namespace EurosolarReporting
{
    partial class maintainance
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(maintainance));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.txtdate = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.txtInstallBookingDate = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.lblmaintainaceno = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.txtMtceCost = new Telerik.Reporting.TextBox();
            this.txtMtceDiscount = new Telerik.Reporting.TextBox();
            this.txtMtceBalance = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox33.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(54)))), ((int)(((byte)(93)))));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Calibri";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "Payment Methods";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.073897123336792D), Telerik.Reporting.Drawing.Unit.Cm(2.1322917938232422D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox16.Value = "Customer Input  :";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.926101684570313D), Telerik.Reporting.Drawing.Unit.Cm(2.1322917938232422D));
            this.textBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox29.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox29.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox29.Value = "=Fields.CustomerInput";
            // 
            // txtdate
            // 
            this.txtdate.Height = Telerik.Reporting.Drawing.Unit.Inch(11.692954063415527D);
            this.txtdate.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.htmlTextBox1,
            this.table8,
            this.table3,
            this.table4,
            this.lblmaintainaceno,
            this.table6,
            this.table1,
            this.textBox37,
            this.table2,
            this.textBox11,
            this.textBox12,
            this.textBox15});
            this.txtdate.Name = "txtdate";
            this.txtdate.Style.Font.Name = "Calibri";
            this.txtdate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0236220359802246D), Telerik.Reporting.Drawing.Unit.Inch(10.708661079406738D));
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.0344724655151367D), Telerik.Reporting.Drawing.Unit.Inch(0.29999980330467224D));
            this.htmlTextBox1.Style.Font.Name = "Calibri";
            this.htmlTextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.htmlTextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.htmlTextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.htmlTextBox1.Value = resources.GetString("htmlTextBox1.Value");
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4668436050415039D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.9189281463623047D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.8016872406005859D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8125410079956055D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.30052483081817627D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox127);
            this.table8.Body.SetCellContent(1, 0, this.textBox126);
            this.table8.Body.SetCellContent(0, 3, this.textBox78);
            this.table8.Body.SetCellContent(1, 3, this.txtInstallBookingDate);
            this.table8.Body.SetCellContent(0, 2, this.textBox1);
            this.table8.Body.SetCellContent(1, 2, this.textBox3);
            this.table8.Body.SetCellContent(0, 1, this.textBox4);
            this.table8.Body.SetCellContent(1, 1, this.textBox5);
            this.table8.Body.SetCellContent(2, 0, this.textBox6);
            this.table8.Body.SetCellContent(2, 1, this.textBox7);
            this.table8.Body.SetCellContent(2, 2, this.textBox8);
            this.table8.Body.SetCellContent(2, 3, this.textBox9);
            tableGroup1.Name = "tableGroup13";
            tableGroup2.Name = "group5";
            tableGroup3.Name = "group4";
            tableGroup4.Name = "tableGroup14";
            this.table8.ColumnGroups.Add(tableGroup1);
            this.table8.ColumnGroups.Add(tableGroup2);
            this.table8.ColumnGroups.Add(tableGroup3);
            this.table8.ColumnGroups.Add(tableGroup4);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox127,
            this.textBox4,
            this.textBox1,
            this.textBox78,
            this.textBox126,
            this.textBox5,
            this.textBox3,
            this.txtInstallBookingDate,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(8.9999980926513672D));
            this.table8.Name = "table8";
            tableGroup6.Name = "group28";
            tableGroup7.Name = "group29";
            tableGroup8.Name = "group6";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ChildGroups.Add(tableGroup7);
            tableGroup5.ChildGroups.Add(tableGroup8);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup7";
            this.table8.RowGroups.Add(tableGroup5);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(2.2899999618530273D));
            this.table8.Style.Font.Name = "Calibri";
            this.table8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4668436050415039D), Telerik.Reporting.Drawing.Unit.Inch(0.30052483081817627D));
            this.textBox127.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox127.Style.Font.Name = "Calibri";
            this.textBox127.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.Value = "ROOF TYPE";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4668436050415039D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.textBox126.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox126.Style.Font.Name = "Calibri";
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "HOUSE ROOF";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8125410079956055D), Telerik.Reporting.Drawing.Unit.Inch(0.30052483081817627D));
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.Font.Italic = true;
            this.textBox78.Style.Font.Name = "Calibri";
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.Value = "=Fields.RoofAngle";
            // 
            // txtInstallBookingDate
            // 
            this.txtInstallBookingDate.Name = "txtInstallBookingDate";
            this.txtInstallBookingDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8125410079956055D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.txtInstallBookingDate.Style.Font.Bold = true;
            this.txtInstallBookingDate.Style.Font.Italic = true;
            this.txtInstallBookingDate.Style.Font.Name = "Calibri";
            this.txtInstallBookingDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtInstallBookingDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtInstallBookingDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInstallBookingDate.Value = "=Fields.InstallBookingDate";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8016872406005859D), Telerik.Reporting.Drawing.Unit.Inch(0.30052483081817627D));
            this.textBox1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            this.textBox1.Value = "ROOF SLOPE";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8016872406005859D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox3.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "INSTALL DATE";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9189281463623047D), Telerik.Reporting.Drawing.Unit.Inch(0.30052483081817627D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Italic = true;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "=Fields.RoofType";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9189281463623047D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Italic = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "=Fields.HouseType";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4668436050415039D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.textBox6.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.StyleName = "";
            this.textBox6.Value = "ASSIGNED TO";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9189281463623047D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Italic = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "=Fields.installername";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8016872406005859D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8125410079956055D), Telerik.Reporting.Drawing.Unit.Inch(0.30052497982978821D));
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5400006771087646D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7520651817321777D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox84);
            this.table3.Body.SetCellContent(0, 1, this.textBox86);
            this.table3.Body.SetCellContent(3, 0, this.textBox83);
            this.table3.Body.SetCellContent(3, 1, this.textBox85);
            this.table3.Body.SetCellContent(2, 0, this.textBox89);
            this.table3.Body.SetCellContent(2, 1, this.textBox90);
            this.table3.Body.SetCellContent(1, 0, this.textBox92);
            this.table3.Body.SetCellContent(1, 1, this.textBox93);
            tableGroup9.Name = "tableGroup4";
            tableGroup10.Name = "tableGroup5";
            this.table3.ColumnGroups.Add(tableGroup9);
            this.table3.ColumnGroups.Add(tableGroup10);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox84,
            this.textBox86,
            this.textBox92,
            this.textBox93,
            this.textBox89,
            this.textBox90,
            this.textBox83,
            this.textBox85});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(4.5D));
            this.table3.Name = "table3";
            tableGroup12.Name = "group9";
            tableGroup13.Name = "group12";
            tableGroup14.Name = "group11";
            tableGroup15.Name = "group10";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.ChildGroups.Add(tableGroup13);
            tableGroup11.ChildGroups.Add(tableGroup14);
            tableGroup11.ChildGroups.Add(tableGroup15);
            tableGroup11.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup11.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup11);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2920656204223633D), Telerik.Reporting.Drawing.Unit.Cm(3.0479998588562012D));
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5400004386901855D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox84.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox84.Style.Font.Name = "Calibri";
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox84.Value = "NAME";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7520649433135986D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox86.Style.Font.Name = "Calibri";
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox86.Value = "=Fields.Contact";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5400004386901855D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox83.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox83.Style.Font.Name = "Calibri";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox83.StyleName = "";
            this.textBox83.Value = "EMAIL";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7520649433135986D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox85.Style.Font.Name = "Calibri";
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox85.StyleName = "";
            this.textBox85.Value = "=Fields.ContEmail";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5400009155273438D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox89.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox89.Style.Font.Name = "Calibri";
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox89.StyleName = "";
            this.textBox89.Value = "PHONE";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7520654201507568D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox90.Style.Font.Name = "Calibri";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox90.StyleName = "";
            this.textBox90.Value = "=Fields.Customer";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5400006771087646D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox92.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox92.Style.Font.Name = "Calibri";
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox92.StyleName = "";
            this.textBox92.Value = "MOBILE";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7520651817321777D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox93.Style.Font.Name = "Calibri";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox93.StyleName = "";
            this.textBox93.Value = "=Fields.CustPhone";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5352902412414551D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.6762518882751465D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox2);
            this.table4.Body.SetCellContent(0, 1, this.textBox17);
            this.table4.Body.SetCellContent(3, 0, this.textBox19);
            this.table4.Body.SetCellContent(3, 1, this.textBox21);
            this.table4.Body.SetCellContent(2, 0, this.textBox22);
            this.table4.Body.SetCellContent(2, 1, this.textBox23);
            this.table4.Body.SetCellContent(1, 0, this.textBox24);
            this.table4.Body.SetCellContent(1, 1, this.textBox25);
            tableGroup16.Name = "tableGroup4";
            tableGroup17.Name = "tableGroup5";
            this.table4.ColumnGroups.Add(tableGroup16);
            this.table4.ColumnGroups.Add(tableGroup17);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox17,
            this.textBox24,
            this.textBox25,
            this.textBox22,
            this.textBox23,
            this.textBox19,
            this.textBox21});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(4.5D));
            this.table4.Name = "table4";
            tableGroup19.Name = "group9";
            tableGroup20.Name = "group12";
            tableGroup21.Name = "group11";
            tableGroup22.Name = "group10";
            tableGroup18.ChildGroups.Add(tableGroup19);
            tableGroup18.ChildGroups.Add(tableGroup20);
            tableGroup18.ChildGroups.Add(tableGroup21);
            tableGroup18.ChildGroups.Add(tableGroup22);
            tableGroup18.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup18.Name = "detailTableGroup2";
            this.table4.RowGroups.Add(tableGroup18);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2115421295166016D), Telerik.Reporting.Drawing.Unit.Cm(3.0479998588562012D));
            this.table4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5352902412414551D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox2.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox2.Value = "ADDRESS";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6762518882751465D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox17.Value = "= TrimStart(Fields.InstallAddress)";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5352902412414551D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox19.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox19.StyleName = "";
            this.textBox19.Value = "POST CODE";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6762518882751465D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox21.Style.Font.Name = "Calibri";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox21.StyleName = "";
            this.textBox21.Value = "=Fields.InstallPostCode";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5352902412414551D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox22.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox22.StyleName = "";
            this.textBox22.Value = "STATE";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6762518882751465D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox23.StyleName = "";
            this.textBox23.Value = "=Fields.InstallState";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5352902412414551D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox24.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox24.Style.Font.Name = "Calibri";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox24.StyleName = "";
            this.textBox24.Value = "SUBURB ";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6762518882751465D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox25.Style.Font.Name = "Calibri";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox25.StyleName = "";
            this.textBox25.Value = "=Fields.InstallCity";
            // 
            // lblmaintainaceno
            // 
            this.lblmaintainaceno.KeepTogether = false;
            this.lblmaintainaceno.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.70866137742996216D), Telerik.Reporting.Drawing.Unit.Inch(0.27559056878089905D));
            this.lblmaintainaceno.Name = "lblmaintainaceno";
            this.lblmaintainaceno.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2708721160888672D), Telerik.Reporting.Drawing.Unit.Inch(0.59992128610610962D));
            this.lblmaintainaceno.Style.Color = System.Drawing.SystemColors.Desktop;
            this.lblmaintainaceno.Style.Font.Bold = true;
            this.lblmaintainaceno.Style.Font.Name = "Calibri";
            this.lblmaintainaceno.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(25D);
            this.lblmaintainaceno.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblmaintainaceno.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.lblmaintainaceno.TextWrap = true;
            this.lblmaintainaceno.Value = "";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.2015371322631836D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D)));
            this.table6.Body.SetCellContent(0, 1, this.textBox26);
            this.table6.Body.SetCellContent(0, 4, this.textBox27);
            this.table6.Body.SetCellContent(0, 2, this.textBox28);
            this.table6.Body.SetCellContent(0, 0, this.textBox32);
            this.table6.Body.SetCellContent(0, 3, this.textBox10);
            this.table6.Body.SetCellContent(1, 0, this.txtMtceCost);
            this.table6.Body.SetCellContent(1, 1, this.txtMtceDiscount);
            this.table6.Body.SetCellContent(1, 2, this.txtMtceBalance);
            this.table6.Body.SetCellContent(1, 3, this.textBox13);
            this.table6.Body.SetCellContent(1, 4, this.textBox14);
            tableGroup24.Name = "group1";
            tableGroup25.Name = "tableGroup1";
            tableGroup26.Name = "group";
            tableGroup27.Name = "group3";
            tableGroup28.Name = "tableGroup2";
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.ChildGroups.Add(tableGroup25);
            tableGroup23.ChildGroups.Add(tableGroup26);
            tableGroup23.ChildGroups.Add(tableGroup27);
            tableGroup23.ChildGroups.Add(tableGroup28);
            tableGroup23.Name = "tableGroup";
            tableGroup23.ReportItem = this.textBox33;
            this.table6.ColumnGroups.Add(tableGroup23);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox32,
            this.textBox26,
            this.textBox28,
            this.textBox10,
            this.textBox27,
            this.txtMtceCost,
            this.txtMtceDiscount,
            this.txtMtceBalance,
            this.textBox13,
            this.textBox14,
            this.textBox33});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(4.6456694602966309D));
            this.table6.Name = "table6";
            tableGroup30.Name = "group2";
            tableGroup31.Name = "group7";
            tableGroup29.ChildGroups.Add(tableGroup30);
            tableGroup29.ChildGroups.Add(tableGroup31);
            tableGroup29.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup29.Name = "detailTableGroup";
            this.table6.RowGroups.Add(tableGroup29);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Inch(0.89999997615814209D));
            this.table6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.800000011920929D);
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.Color = System.Drawing.Color.Black;
            this.textBox26.Style.Font.Bold = false;
            this.textBox26.Style.Font.Name = "Calibri";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "Amount Discount";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox27.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.Style.Color = System.Drawing.Color.Black;
            this.textBox27.Style.Font.Bold = false;
            this.textBox27.Style.Font.Name = "Calibri";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "Received By";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox28.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox28.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox28.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox28.Style.Color = System.Drawing.Color.Black;
            this.textBox28.Style.Font.Bold = false;
            this.textBox28.Style.Font.Name = "Calibri";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "Balance";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2015371322631836D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.Color = System.Drawing.Color.Black;
            this.textBox32.Style.Font.Bold = false;
            this.textBox32.Style.Font.Name = "Calibri";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "Amount";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.Font.Bold = false;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "Paid By";
            // 
            // txtMtceCost
            // 
            this.txtMtceCost.Name = "txtMtceCost";
            this.txtMtceCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2015371322631836D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.txtMtceCost.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMtceCost.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceCost.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceCost.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceCost.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceCost.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceCost.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceCost.Style.Font.Bold = false;
            this.txtMtceCost.Style.Font.Name = "Calibri";
            this.txtMtceCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtMtceCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtMtceCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMtceCost.StyleName = "";
            this.txtMtceCost.Value = "=Fields.MtceCost";
            // 
            // txtMtceDiscount
            // 
            this.txtMtceDiscount.Name = "txtMtceDiscount";
            this.txtMtceDiscount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.txtMtceDiscount.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMtceDiscount.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceDiscount.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceDiscount.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceDiscount.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceDiscount.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceDiscount.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceDiscount.Style.Font.Bold = false;
            this.txtMtceDiscount.Style.Font.Name = "Calibri";
            this.txtMtceDiscount.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtMtceDiscount.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtMtceDiscount.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMtceDiscount.StyleName = "";
            this.txtMtceDiscount.Value = "=Fields.MtceDiscount";
            // 
            // txtMtceBalance
            // 
            this.txtMtceBalance.Name = "txtMtceBalance";
            this.txtMtceBalance.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.txtMtceBalance.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMtceBalance.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceBalance.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceBalance.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceBalance.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceBalance.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceBalance.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.txtMtceBalance.Style.Font.Bold = false;
            this.txtMtceBalance.Style.Font.Name = "Calibri";
            this.txtMtceBalance.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtMtceBalance.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtMtceBalance.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMtceBalance.StyleName = "";
            this.txtMtceBalance.Value = "=Fields.MtceBalance";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox13.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox13.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox13.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox13.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox13.Style.Font.Bold = false;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "=Fields.FPTransType";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1996157169342041D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox14.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox14.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox14.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox14.Style.Font.Bold = false;
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "=Fields.MtceRecByName";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0738973617553711D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(12.926101684570313D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.9762499332427979D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(2.0820839405059814D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4206255674362183D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox18);
            this.table1.Body.SetCellContent(0, 1, this.textBox30);
            this.table1.Body.SetCellContent(1, 0, this.textBox31);
            this.table1.Body.SetCellContent(1, 1, this.textBox34);
            this.table1.Body.SetCellContent(2, 0, this.textBox35);
            this.table1.Body.SetCellContent(2, 1, this.textBox36);
            tableGroup32.Name = "tableGroup3";
            tableGroup32.ReportItem = this.textBox16;
            tableGroup33.Name = "tableGroup6";
            tableGroup33.ReportItem = this.textBox29;
            this.table1.ColumnGroups.Add(tableGroup32);
            this.table1.ColumnGroups.Add(tableGroup33);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox30,
            this.textBox31,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox16,
            this.textBox29});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(14.699995994567871D));
            this.table1.Name = "table1";
            tableGroup35.Name = "group8";
            tableGroup36.Name = "group13";
            tableGroup37.Name = "group14";
            tableGroup34.ChildGroups.Add(tableGroup35);
            tableGroup34.ChildGroups.Add(tableGroup36);
            tableGroup34.ChildGroups.Add(tableGroup37);
            tableGroup34.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup34.Name = "detailTableGroup1";
            this.table1.RowGroups.Add(tableGroup34);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(7.6112508773803711D));
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.073897123336792D), Telerik.Reporting.Drawing.Unit.Cm(1.9762504100799561D));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox18.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox18.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox18.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox18.Value = "Fault Identified  :";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.926101684570313D), Telerik.Reporting.Drawing.Unit.Cm(1.9762504100799561D));
            this.textBox30.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox30.Value = "=Fields.FaultIdentified";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.073897123336792D), Telerik.Reporting.Drawing.Unit.Cm(2.0820837020874023D));
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox31.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox31.StyleName = "";
            this.textBox31.Value = "Action Required  :";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.926101684570313D), Telerik.Reporting.Drawing.Unit.Cm(2.0820837020874023D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox34.StyleName = "";
            this.textBox34.Value = "=Fields.ActionRequired";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.073897123336792D), Telerik.Reporting.Drawing.Unit.Cm(1.4206253290176392D));
            this.textBox35.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox35.StyleName = "";
            this.textBox35.Value = "Work Done  :";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.926101684570313D), Telerik.Reporting.Drawing.Unit.Cm(1.4206253290176392D));
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox36.StyleName = "";
            this.textBox36.Value = "=Fields.WorkDone";
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(23D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(1.2999999523162842D));
            this.textBox37.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "I Nilesh Dhimar confirm that the details above are correct and the Arise Solar As" +
    "sistant has resolved and\r\nserviced the solar system in regards to the details in" +
    " Customer Input.";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.179471492767334D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5857901573181152D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.4615852832794189D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7731521129608154D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox39);
            this.table2.Body.SetCellContent(0, 1, this.textBox41);
            this.table2.Body.SetCellContent(0, 3, this.textBox43);
            this.table2.Body.SetCellContent(0, 2, this.textBox38);
            tableGroup38.Name = "tableGroup7";
            tableGroup39.Name = "tableGroup8";
            tableGroup40.Name = "group15";
            tableGroup41.Name = "tableGroup9";
            this.table2.ColumnGroups.Add(tableGroup38);
            this.table2.ColumnGroups.Add(tableGroup39);
            this.table2.ColumnGroups.Add(tableGroup40);
            this.table2.ColumnGroups.Add(tableGroup41);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox39,
            this.textBox41,
            this.textBox38,
            this.textBox43});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.6002800464630127D), Telerik.Reporting.Drawing.Unit.Cm(25.100000381469727D));
            this.table2.Name = "table2";
            tableGroup42.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup42.Name = "detailTableGroup3";
            this.table2.RowGroups.Add(tableGroup42);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1794719696044922D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox39.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "Customer Signature:";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5857906341552734D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox41.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7731525897979736D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox43.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.461585521697998D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox38.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "Dated:";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox11.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox11.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox11.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox11.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox11.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(54)))), ((int)(((byte)(93)))));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "CONTACT DETAILS";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(8.1999998092651367D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(54)))), ((int)(((byte)(93)))));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "SYSTEM DETAILS";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(54)))), ((int)(((byte)(93)))));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "SITE DETAILS";
            // 
            // maintainance
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtdate});
            this.Name = "Maintenance";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.Font.Name = "open sp";
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8.2676773071289062D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection txtdate;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox lblmaintainaceno;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox txtInstallBookingDate;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox txtMtceCost;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox txtMtceDiscount;
        private Telerik.Reporting.TextBox txtMtceBalance;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox15;
    }
}