﻿using MeghMailUtility;
using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CloudUploader
{
    public class Program
    {
        //Timer _tm = null;
        static void Main(string[] args)
        {
            //Timer timer = new Timer(2000);
            //timer.Elapsed += Timer_Elapsed;
            //timer.Start();
            //while (true)
            //{
            //    // Infinite loop.
            //}

            UploadPDFFile();
            UploadNearMapPDFFile();
            UploadMPPDFFile();
            UploadEBPDFFile();
            UploadPDPDFFile();
            UploadPRPDFFile();
            UploadBeatQuotePDFFile();
            UploadSQPDFFile();
        }
        public static bool UploadPDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "quotedoc";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\quotedoc\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername+"/"+filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                
            }
            //HttpContext.Current.Response.End();
            return true;
        }
        public static bool UploadNearMapPDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "NearMap";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\NearMap\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername + "/" + filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            //HttpContext.Current.Response.End();
            return true;
        }
        public static bool UploadMPPDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "MP";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\MP\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername + "/" + filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            //HttpContext.Current.Response.End();
            return true;
        }
        public static bool UploadEBPDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "EB";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\EB\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername + "/" + filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            //HttpContext.Current.Response.End();
            return true;
        }
        public static bool UploadPDPDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "PD";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\PD\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername + "/" + filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            //HttpContext.Current.Response.End();
            return true;
        }
        public static bool UploadPRPDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "PR";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\PR\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername + "/" + filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            //HttpContext.Current.Response.End();
            return true;
        }
        public static bool UploadBeatQuotePDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "BeatQuote";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\BeatQuote\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername + "/" + filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            //HttpContext.Current.Response.End();
            return true;
        }
        public static bool UploadSQPDFFile()
        {
            try
            {
                GC.Collect();
                string username = "nitinrpatel";
                string api_key = "96b6504f256b49cbb2a1feae33b32dd3";
                string chosenContainer = "arisesolar";
                //chosenContainer = Path.Combine(chosenContainer, "quotedoc");
                //string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
                var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
                var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
                string foldername = "SQ";// DateTime.Now.Day.ToString()+" - "+DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string filePath = @"C:\inetpub\wwwroot\portal.arisesolar.com.au\userfiles\SQ\";
                string[] filePaths = Directory.GetFiles(filePath, "*.*");
                foreach (string file in filePaths)
                {
                    using (FileStream stream = System.IO.File.OpenRead(file))
                    {
                        //HttpContext.Current.Response.Write(stream);
                        string filename = Path.GetFileName(file);
                        cloudFilesProvider.CreateObject(chosenContainer, stream, foldername + "/" + filename);
                        stream.Flush();
                        stream.Close();

                        //===Delete file
                        System.IO.FileInfo backupfile = new System.IO.FileInfo(file);
                        bool isfileUploaded = backupfile.Exists;
                        if (isfileUploaded)
                        {
                            GC.Collect();
                            try
                            {
                                backupfile.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            //HttpContext.Current.Response.End();
            return true;
        }
        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Use SignalTime.
            
        }
    }
}
