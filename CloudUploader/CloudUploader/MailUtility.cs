using System;
using System.Net.Mail;

/// <summary>
/// Summary description for MailUtility
/// </summary>

namespace MeghMailUtility
{
    public class Mail
    {
        public Mail()
        {
            _cc = null;
            _bcc = null;
            _subject = "";
            _body = "";
            _isHtml = true;
            _isAuthentification = false;
            _mailType = TypeMail.NetMail;
            _addressSeparator = new char[] { ';' };
            _attachment = "";
        }
        private string _to;

        public string To
        {
            get { return _to; }
            set { _to = value; }
        }
        private string _cc;

        public string Cc
        {
            get { return _cc; }
            set { _cc = value; }
        }
        private string _from;

        public string From
        {
            get { return _from; }
            set { _from = value; }
        }
        private string _bcc;

        public string Bcc
        {
            get { return _bcc; }
            set { _bcc = value; }
        }
        private char[] _addressSeparator;

        public char AddressSeparator
        {
            set { _addressSeparator = new char[] { value }; }
            get { return _addressSeparator[0]; }
        }

        private string _subject;

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        private string _body;

        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }
        private bool _isHtml;

        public bool IsHtml
        {
            get { return _isHtml; }
            set { _isHtml = value; }
        }
        public enum TypeMail
        {
            NetMail = 0,
            WebMail = 1
        }
        private TypeMail _mailType;

        public TypeMail MailType
        {
            get { return _mailType; }
            set { _mailType = value; }
        }
        private bool _isAuthentification;

        public bool IsAuthentification
        {
            get { return _isAuthentification; }
            set { _isAuthentification = value; }
        }
        private string _attachment;
        public string Attachment
        {
            get { return _attachment; }
            set { _attachment = value; }
        }

        public static bool SendMail(Mail objMail)
        {
            try
            {
                string strSMTPServer = "smtp.gmail.com";
                System.Net.Mail.MailMessage objMessage = new System.Net.Mail.MailMessage();
                objMessage.To.Add(objMail.To);
                objMessage.From = new MailAddress(objMail.From);

                if (objMail.Cc != null && objMail.Cc != "")
                {
                    foreach (string mailItem in objMail.Cc.Split(objMail._addressSeparator))
                        objMessage.CC.Add(mailItem);
                }
                if (objMail.Bcc != null && objMail.Bcc != "")
                {
                    foreach (string mailItem in objMail.Bcc.Split(objMail._addressSeparator))
                        objMessage.Bcc.Add(mailItem);
                }
                objMessage.Subject = objMail.Subject;
                objMessage.Body = objMail.Body;
                objMessage.IsBodyHtml = objMail.IsHtml;
                System.Net.Mail.SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = strSMTPServer;

                smtpClient.EnableSsl = true;
                if (smtpClient.EnableSsl)
                    smtpClient.Port = Convert.ToInt32(587);
                else
                    smtpClient.Port = 25;
                if (objMail.IsAuthentification)
                {
                    System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                    myMailCredential.UserName = "megherrorlog@gmail.com";
                    myMailCredential.Password = "MeghTechGoogle@2017$ErrorLog";
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = myMailCredential;
                }
                smtpClient.Send(objMessage);

                return true;
            }
            catch (Exception ex)
            {
                // catch exception code
                //throw ex;
                return false;
            }
        }
    }
}