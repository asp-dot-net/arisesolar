﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;

public partial class Signature : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int startindex;
    protected static int lastpageindex;
    protected void Page_Load(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        String Token = Request.QueryString["rt"];
        pagetitle.Text = "AriseSolar | Signature";

        int TokenExist = Clstbl_TokenData.tbl_TokenDataExistByToken_ProjectID(Token, ProjectID);

        SttblProjects st1 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st3 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        String contactid = st1.ContactID;
        String InstallerID = st1.Installer;
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(contactid);
        Sttbl_TokenData stk = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

        //Response.Write(Request.PhysicalApplicationPath);
        //Response.End();

        if (!IsPostBack)
        {
            if (TokenExist == 0)
            {
                panelNotComplete.Visible = false;
                PanelPageNotExist.Visible = true;
            }
            //int ProjectIDSigned = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID(ProjectID);

            int ProjectIDSigned = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, stk.QDocNo);
            if (ProjectIDSigned == 1)
            {
                panelNotComplete.Visible = false;
                panelExist.Visible = true;
            }


            string IsExpired = stk.IsExpired;
            if (IsExpired == "True")
            {
                panelNotComplete.Visible = false;
                panelExist.Visible = true;
                lblCustName4.Text = st2.ContFirst + " " + st2.ContLast;
            }
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            custompageIndex = 1;
            // divnopage.Visible = false;
            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);        
        }

        lblCustName.Text = st2.ContFirst + " " + st2.ContLast;
        lblInstallerName.Text = st1.SalesRepName;
        lblRetailer.Text = "ARISE SOLAR PTY LTD";
        lblCustAdd.Text = st1.InstallAddress + "," + st1.InstallCity + "," + st1.InstallState + "-" + st1.InstallPostCode;
        //lblInstalledDate.Text = st1.InstallCompleted;

        lblPanelBrand.Text = st1.PanelBrand;
        lblPanelModel.Text = st1.PanelModel;
        lblInverterBrand.Text = st1.InverterBrand;
        lblInverterModel.Text = st1.InverterModel;
        lblSystemOutput.Text = st1.SystemCapKW;
        lblPanelNo.Text = st1.NumberPanels;
        lblSTC.Text = st1.STCNumber;
        lblHType.Text = st1.HouseType;
        lblRType.Text = st1.RoofType;
        lblRAngle.Text = st1.RoofAngle;
        lblNMI.Text = st1.NMINumber;
        lblDistributor.Text = st1.ElecDistributor;
        lblRetailer2.Text = st1.ElecRetailer;
        lblMeterUpgrade.Text = st1.meterupgrade;
        lblMeterPhase.Text = st1.MeterPhase;
        //lblPrice.Text = st1.TotalQuotePrice;

        lblPanelName.Text = st1.PanelBrandName;
        lblInverterName.Text = st1.InverterDetailsName;

        try
        {
            if (!string.IsNullOrEmpty(st1.TotalQuotePrice))
            {
                decimal Price1 = decimal.Parse(st1.TotalQuotePrice);               
                lblPrice.Text = Price1.ToString("0.00");
            }
            else
            {
                lblPrice.Text = "0.00";
            }
        }
        catch
        {
            lblPrice.Text = "0.00";
        }
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        if (dtCount.Rows.Count > 0)
        {
            try {
				 if (dtCount.Rows[0]["InvoicePayTotalWithOutBakVerified"].ToString()!= "")
                {

                    //decimal DepositPaid1 = decimal.Parse(dtCount.Rows[0]["InvoicePayTotal"].ToString());               
                    //lblPrice.Text = DepositPaid1.ToString("0.00");

                    //decimal BalanceToPay1 = Convert.ToDecimal(st1.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());                   
                    //lblPrice.Text = BalanceToPay1.ToString("0.00");
                    decimal DepositPaid1 = decimal.Parse(dtCount.Rows[0]["InvoicePayTotalWithOutBakVerified"].ToString());
                    lblDPaid.Text = DepositPaid1.ToString("0.00");

                    decimal BalanceToPay1 = Convert.ToDecimal(st1.TotalQuotePrice) - DepositPaid1;
                    lblBalToPay.Text = BalanceToPay1.ToString("0.00");
                }
                else
                {
                    lblDPaid.Text = "0.00";
                    lblBalToPay.Text = "0.00";
                }
               // lblDPaid.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
               // Decimal BalanceToPay = Convert.ToDecimal(st1.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
               // lblBalToPay.Text = Convert.ToString(BalanceToPay);
            }
            catch { }
        }
        else
        {
            lblDPaid.Text = "";
            lblBalToPay.Text = "";
        }

        if(st1.FinanceWithID=="1")
        {
            lblPOption.Text = "Cash";
        }
        else
        {
            lblPOption.Text = "Finance With "+ st1.FinanceWith + " with "+ st3.FinanceWithDeposit + "% for " + st1.PaymentType;
        }
       
        lblCustName2.Text = st2.ContFirst + " " + st2.ContLast;
        lblCustName3.Text = st2.ContFirst + " " + st2.ContLast;
        lblCustName4.Text = st2.ContFirst + " " + st2.ContLast;
        lblTodayDate.Text = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Today));
        lblCustEmailID.Text = st2.ContEmail;
        // lblIPAdd2.Text = lblIPAdd.Text;

        lblNoOfInverter.Text = st1.inverterqty;
    }

    [WebMethod]
    //string username, string subj, string desc
    public static string InsertData(string value, String ProjectID, String Token, String PhysicalRootLocation, String Latitude, String Longitude, String IPAddress, String Country, String Region, String City, String Postal)
    {
        string msg = string.Empty;
        //msg = value;
        SttblProjects st1 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        String contactid = st1.ContactID;
        String InstallerID = st1.Installer;
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(contactid);
        SttblEmployees st3 = ClstblEmployees.tblEmployees_SelectByEmployeeID(st1.SalesRep);
        DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        SttblProjects2 st4 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        int TokenExist = Clstbl_TokenData.tbl_TokenDataExistByToken_ProjectID(Token, ProjectID);

        //Sttbl_TokenData st = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);
        //String QFileName = st.QDocNo+ "Quotation.pdf";
        //SiteConfiguration.DeletePDFFile("quotedoc", QFileName);

        if (TokenExist == 1)
        {
            Sttbl_TokenData st = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);
            if (st.IsExpired == "False")
            {
                try
                {
                    String FileName = ProjectID + "_" + Token + "_" + "Owner.jpg";
                    byte[] bytIn = null;
                    value = value.Replace("data:image/png;base64,", "");
                    bytIn = Convert.FromBase64String(value);
                    //var path = Request.PhysicalApplicationPath + "userfiles\\" + "Signature" + "\\" + FileName;

                    var path = PhysicalRootLocation + "userfiles\\" + "Signature" + "\\" + FileName;
                    FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
                    fs.Write(bytIn, 0, bytIn.Length);
                    fs.Close();

                    SiteConfiguration.UploadPDFFile("Signature", FileName);
                   // SiteConfiguration.deleteimage(FileName, "Signature");

                    String CustName = st2.ContFirst + " " + st2.ContLast;
                    String SignedDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
                    String latitude = Latitude;
                    String longitude = Longitude;
                    String IP_Address = IPAddress;
                    String CustEmail = st2.ContEmail;

                    Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);
                    string Qdocno = sttok.QDocNo;
                    try
                    {
                        int success = Clstbl_SignatureLog.tbl_tbl_SignatureLog_Insert(CustName, SignedDate, latitude, longitude, IP_Address, CustEmail, Token, ProjectID, path, Qdocno);
                        Clstbl_TokenData.tbl_TokenData_Update_IsExpired(Token, ProjectID, "True");
                       
                        if (success > 0)
                        {
                            bool Update = Clstbl_SignatureLog.tbl_tbl_SignatureLog_Update(success.ToString(), Country, Region, City, Postal);

                            msg = "success";
                            //String QFileName = st.QDocNo + "Quotation.pdf";
                            //SiteConfiguration.DeletePDFFile("quotedoc", QFileName);
                            //Telerik_reports.generate_quatation(ProjectID, Qdocno);
                            if (st4.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                            {
                                Telerik_reports.generate_Quote(ProjectID, Qdocno);
                            }
                            else
                            {
                                Telerik_reports.generate_QuoteCash(ProjectID, Qdocno);
                            }

                            #region UpdateSignedQuote
                            if (!string.IsNullOrEmpty(st1.SQ))
                            {
                                SiteConfiguration.DeletePDFFile("SQ", st1.SQ);
                                //SiteConfiguration.DeletePDFFile("SQtest", st1.SQ);
                            }

                            string SQ = ProjectID + "_" + Qdocno + "_" + Token + "Quotation.pdf";
                            //string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SQtest\\", SQ);

                            //var stream = new WebClient().OpenRead("http://b2848392e002ee33fd61-5c566850018ebfb871276c1331018ded.r57.cf2.rackcdn.com/quotedoctest/"+Qdocno+"Quotation.pdf");
                            ////FileStream sourceFile = new FileStream(input, FileMode.Open); //Open streamer
                            //BinaryReader binReader = new BinaryReader(stream);
                            //byte[] fb = new byte[stream.Length]; //create byte array of size file
                            //for (long i = 0; i < stream.Length; i++)
                            //    fb[i] = binReader.ReadByte(); //read until done
                            //stream.Close(); //dispose streamer
                            //binReader.Close(); //dispose reader                   
                            //FileStream fs1 = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
                            //fs.Write(fb, 0,fb.Length);
                            //fs.Close();

                            ///fuSQ.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/SQ/") + SQ);
                            bool sucSQ = ClstblProjects.tblProjects_UpdateSQ_SignedQuoteByProjectID(ProjectID, SQ, "true", SignedDate);

                            if(sucSQ)
                            {
                                #region SendMail
                                SiteConfiguration.deleteimage(FileName, "Signature");
                                TextWriter txtWriter = new StringWriter() as TextWriter;
                                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                                string from = stU.from;
                                String Subject = "Signature Done- " + ConfigurationManager.AppSettings["SiteName"].ToString();

                                string body = string.Empty;
                                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/mailtemplate/SignatureCompletionMail.aspx")))
                                {
                                    body = reader.ReadToEnd();
                                }
                                body = body.Replace("{SalesRep}", st1.SalesRepName); //replacing the required things  
                                body = body.Replace("{CustName}", st2.ContFirst + " " + st2.ContLast);
                                body = body.Replace("{ProjectNumber}", st1.ProjectNumber);
                                body = body.Replace("{year}", Convert.ToString(DateTime.Now.Year));
                                body = body.Replace("{sitename}", ConfigurationManager.AppSettings["SiteName"].ToString());

                                Utilities.SendMail(from, st3.EmpEmail, Subject, body);
                                //Utilities.SendMail(from, "deep@meghtechnologies.com", Subject, body);
                                try
                                {
                                }
                                catch
                                {
                                }

                                #endregion
                            }

                            #endregion
                        }
                        else
                        {
                            msg = "Error";
                        }
                    }
                    catch
                    {
                    }
                }
                catch
                {
                    msg = "Error";
                }
            }
            else
            {
                msg = "Error(Token Expired).";
            }
        }
        else
        {
            msg = "Error(Token does not exist).";

        }

        return msg;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

}