﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_TaxInvoiceAttach : System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        string ProjectID = Request.QueryString["proid"];

        if (!IsPostBack)
        {
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts st3 = ClstblContacts.tblContacts_SelectByContactID(st2.ContactID);
            lblCustName.Text = st3.ContFirst+" "+st3.ContLast;
            lblProjectNumber.Text =st2.ProjectNumber;
            lblSiteName.Text = ConfigurationManager.AppSettings["SiteName"].ToString() + "."; 
            lblSiteName2.Text = ConfigurationManager.AppSettings["SiteName"].ToString();
            lblyear.Text = Convert.ToString(DateTime.Now.Year);
        }

        //emailtop.ImageUrl = SiteURL + "userfiles/emailtop/" + st.emailtop;
        //emailbottom.ImageUrl = SiteURL + "userfiles/emailbottom/" + st.emailbottom;
    }
}