﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_AcnoSignLinkMail : System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        string ProjectID = Request.QueryString["proid"];
        string Token = Request.QueryString["rt"];

        if (!IsPostBack)
        {
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts st3 = ClstblContacts.tblContacts_SelectByContactID(st2.ContactID);
            Sttbl_TokenData stk = Clstbl_TokenData.tbl_TokenDataAcno_SelectByToken_ProjectID(Token, ProjectID);
            lblCustName.Text = st3.ContFirst + " " + st3.ContLast;
            lblQDocNo.Text = stk.QDocNo;

            if(Request.QueryString["p"].ToString() == "EC")
            {
                lblLink.HRef = SiteURL + "ExportControlSign.aspx?proid=" + ProjectID + "&rt=" + Token;
                lblLink.InnerText = SiteURL + "ExportControlSign.aspx?proid=" + ProjectID + "&rt=" + Token;
                lblTitle.Text = "Export Control Acknowledgement";
            }
            else
            {
                lblLink.HRef = SiteURL + "FeedInTariffSign.aspx?proid=" + ProjectID + "&rt=" + Token;
                lblLink.InnerText = SiteURL + "FeedInTariffSign.aspx?proid=" + ProjectID + "&rt=" + Token;
                lblTitle.Text = "Feed In Tariff Acknowledgement";
            }
            
            lblSiteName.Text = ConfigurationManager.AppSettings["SiteName"].ToString();
            lblSiteName2.Text = ConfigurationManager.AppSettings["SiteName"].ToString();
            lblyear.Text = Convert.ToString(DateTime.Now.Year);
        }

        //emailtop.ImageUrl = SiteURL + "userfiles/emailtop/" + st.emailtop;
        //emailbottom.ImageUrl = SiteURL + "userfiles/emailbottom/" + st.emailbottom;
    }
}