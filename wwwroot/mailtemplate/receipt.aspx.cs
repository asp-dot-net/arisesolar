﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class mailtemplate_receipt : System.Web.UI.Page
{
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            string ProjectID = Request.QueryString["id"];  //  "-2146260946"; 
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);

            lblReceiptDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToString()));
            lblContact.Text = stCont.ContFirst + " " + stCont.ContLast;
            lblContMobile.Text = stCont.ContMobile;
            lblCustPhone.Text = stCust.CustPhone;
            lblContEmail.Text = stCont.ContEmail;
            //lblPostalAddress.Text = stCust.PostalAddress;
            //lblPostalAddress2.Text = stCust.PostalCity + ", " + stCust.PostalState + ", " + stCust.PostalPostCode;
            lblInstallAddress.Text = stPro.InstallAddress;
            lblInstallAddress2.Text = stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;

            lblInvoiceNumber.Text = stPro.InvoiceNumber;
            try
            {
                lblInvoiceSent.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
            }
            catch { }

            try
            {
                lblTotalQuotePrice.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            }
            catch { }

            DataTable dt = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
            if (dt.Rows.Count > 0)
            {
                rptInvoice.DataSource = dt;
                rptInvoice.DataBind();
            }

            DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                try
                {
                    lblBalanceOwing.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
                    lblBalanceOwing1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
                }
                catch { }
            }
        }
    }
}