﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_quotation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ProjectID = "217746";
        //Request.QueryString["id"];

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 stPro2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        //SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        string CompanyID = stPro.CustomerID;//"707335";
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CompanyID);

        SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvoiceID(stPro.InvoiceNumber);

        lblprojectno.Text = stPro.ProjectNumber;
        lblcontactname.Text = stCust.Customer;
        lblphonenumber.Text = stCust.CustPhone;
        lblemail.Text = stCont.ContEmail;
        lblstreetname.Text = stCust.street_name;
        lblstreetcity.Text = stCust.StreetCity;
        lblstate.Text = stCust.StreetState;
        lblpostcode.Text = stCust.StreetPostCode;


        lblsite_streetname.Text = stPro.street_name;
        lblsite_streetcity.Text = stPro.InstallCity;
        lblsite_state.Text = stPro.InstallState;
        lblsite_postcode.Text = stPro.InstallPostCode;
        #region newlychange
        lblmeter.Text = stPro.MeterPhase;
        lblswitchupgrade.Text = stPro.meterupgrade;
        #endregion
        lblPanelDetails.Text = stPro.PanelDetails;
        lblPanelDetails1.Text = stPro.PanelDetails;


        //if (stPro.PanelBrandID != "")
        //{
        //    lblPanelDetails.Text = stPro.NumberPanels + " X " + stPro.PanelBrandName;
        //}
        //if (stPro.PanelBrandID != "" && stPro.InverterDetailsID != "")
        //{
        //    lblPanelDetails.Text = stPro.NumberPanels + " X " + stPro.PanelBrandName + " Panels with " + stPro.InverterDetailsName + " Inverter.";
        //}
        //if (stPro.PanelBrandID != "" && stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "")
        //{
        //    lblPanelDetails.Text = stPro.NumberPanels + " X " + stPro.PanelBrandName + " Panels with " + stPro.InverterDetailsName + " Inverter. Plus Second Inverter" + stPro.SecondInverterDetails;
        //}


        if (stPro.InverterDetailsID != "")
        {
            lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "")
        {
            lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "")
        {
            lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
        }

        if (stPro.InverterDetailsID != "")
        {
            lblInverterDetails1.Text = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "")
        {
            lblInverterDetails1.Text = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "")
        {
            lblInverterDetails1.Text = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
        }
        //st.ServiceValue.ToString()

        if(stPro.ServiceValue!=string.Empty|| stPro.ServiceValue != "")
        {
            lblbasiccost.Text = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost.Text = "0";
        }
        lbltax.Text = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG)+ Convert.ToDecimal(stPro.VarAsbestos)+ Convert.ToDecimal(stPro.VarSplitSystem)+ Convert.ToDecimal(stPro.VarCherryPicker)+ Convert.ToDecimal(stPro.VarTravelTime)+ Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax.Text==string.Empty)
        {
            lbltax.Text = "0";
        }
        try
        {
            lblfullretail.Text = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost.Text) + Convert.ToDecimal(lbltax.Text)).ToString());
        }
        catch { }

        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbllessdis2.Text = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbllessdis2.Text = "0";
        }

        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lbllessstc.Text = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lbllessstc.Text = "0";
        }
        //if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        //{
        //    lbllessstc.Text = stPro.RECRebate.ToString();
        //}
        //else
        //{
        //    lbllessstc.Text = "0";
        //}
        try
        {
            string cost= (Convert.ToDecimal(lblfullretail.Text) - Convert.ToDecimal(lbllessdis2.Text) - Convert.ToDecimal(lbllessstc.Text)).ToString();
            //String.Format("${0:#.##}", x)
            txttotalcost.Text = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }
        // lbldepositeamount
        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldepositeamount.Text = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldepositeamount.Text = "0";
        }

        try
        {
            lbltotallessdepo.Text = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(txttotalcost.Text) - Convert.ToDecimal(lbldepositeamount.Text)).ToString());
        }
        catch { }

        lblInstallAddress.Text = stPro.InstallAddress;
        lblInstallCity.Text = stPro.InstallCity;
        lblInstallState.Text = stPro.InstallState;
        lblInstallPostCode.Text = stPro.InstallPostCode;

        lblElecRetailer.Text = stPro.ElecRetailer;
        lblElecDistributor.Text = stPro.ElecDistributor;

        try
        {
            lblVarRoofType.Text = stPro.RoofType;
        }
        catch { }

        try
        {
            lblVarRoofAngle.Text = stPro.RoofAngle;
        }
        catch { }

        try
        {
            lblVarHouseType.Text = stPro.HouseType;

        }
        catch { }
        lblMeterPhase.Text = stPro.MeterPhase;
        try
        {
            lbllessdis2.Text = SiteConfiguration.ChangeCurrency_Val1(st.AdvanceLessAmount);
        }
        catch { }

        lblMeterPhase.Text = stPro.MeterPhase;
        lblSystemCapKW.Text = stPro.SystemCapKW;
        lblSystemCapKW1.Text = stPro.SystemCapKW;
        lblcustnmae.Text = stPro.Customer;
        lblgetdate.Text = DateTime.Now.ToString(("dd-MMM-yyyy"));
    }
}