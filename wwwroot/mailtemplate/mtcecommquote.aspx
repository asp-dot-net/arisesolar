﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mtcecommquote.aspx.cs" Inherits="mailtemplate_mtcecommquote" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }

        .pdfimages ul li {
            width: 318px;
            text-align: center;
            display: block;
            float: left;
            margin: 0px 0px 20px;
        }

        .pdfimages ul {
            width: 100%;
            float: left;
            margin: 0px;
            padding: 0px;
        }

        .terms tr td {
            font-size: 11px;
            text-align: justify;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%;">
            <div style="width: 960px; margin: 0px auto;">
                <div class="middle">
                    <div style="text-align: center;">
                    </div>
                    <div style="border-right: 1px solid #ccc; border-left: 1px solid #ccc; border-bottom: 1px solid #ccc; padding: 0px 0px 0px 0px; margin-bottom: 10px;">
                        <h3 style="text-align: center; padding: 5px 10px; background: #CCC; font-family: 'OpenSansLight', sans-serif; margin: 0px;">Customer Number:
                        <asp:Label ID="lblCompanyNumber" runat="server" Text="CompanyNumber"></asp:Label></h3>
                    </div>
                    <div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #ccc; border-collapse: collapse; border-top: 1px solid #ccc;">
                            <tr>
                                <td width="14%" style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Manual Quote No: </strong>
                                </td>
                                <td width="9%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblManualQuoteNumber" runat="server"></asp:Label>
                                </td>
                                <td width="33%" align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Sales Rep:</strong>
                                </td>
                                <td width="22%" style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblSalesRep" runat="server"></asp:Label>
                                </td>
                                <td width="22%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Project Number: </strong>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <asp:Label ID="lblProjectNumber" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Mobile:</strong>
                                </td>
                                <td colspan="2" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblEmpMobile" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" style="border-left: solid 1px #ccc; margin-top: 0px;">
                            <tr>
                                <td width="21%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; background: #ccc; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Customer Details</strong>
                                </td>
                                <td width="35%" align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                                <td width="18%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; background: #CCC; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Installation Address</strong>
                                </td>
                                <td width="32%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">First Name:
                                </td>
                                <td align="left" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblContFirst" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">Site Address:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">
                                    <asp:Label ID="lblInstallAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Last Name:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <asp:Label ID="lblContLast" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">City/Suburb:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblInstallCity" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">Phone:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">
                                    <asp:Label ID="lblCustPhone" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">State:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblInstallState" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Mobile:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblContMobile" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Post Code:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblInstallPostCode" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Email Address:
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblContEmail" runat="server"></asp:Label>
                                </td>
                                <td style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                                <td style="border-bottom: solid 1px #ccc; border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #ccc; margin-top: 0px;">
                            <tr>
                                <td width="15%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; background: #CCC; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Installation Details</strong>
                                </td>
                                <td colspan="5" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">House Type:
                                </td>
                                <td width="25%" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblHouseType" runat="server" Text="HouseType"></asp:Label>
                                </td>
                                <td width="15%" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Meter Phase:
                                </td>
                                <td colspan="2" width="23%" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblMeterPhase" runat="server"></asp:Label>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">Roof Type:
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblRoofType" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Split System:
                                </td>
                                <td align="left" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <asp:Label ID="lblSplitSystem" runat="server"></asp:Label>
                                </td>
                                <td width="11%" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">North/West:<asp:Label ID="lblPanelConfigNW" runat="server"></asp:Label>
                                </td>
                                <td width="11%" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Other:<asp:Label ID="lblPanelConfigOTH" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Roof Angle:
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblRoofAngle" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Cherry Picker:
                                </td>
                                <td colspan="2" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblCherryPicker" runat="server"></asp:Label>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Meter Install:
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblMeterIncluded" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Travel (KM):
                                </td>
                                <td colspan="2" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">
                                    <asp:Label ID="lblTravelTime" runat="server"></asp:Label>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Asbestos:
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblAsbestos" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Elec. Distributor:
                                </td>
                                <td colspan="2" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblElecDistributor" runat="server"></asp:Label>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Meter Space:
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblMeterSpace" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Elec. Retailer:
                                </td>
                                <td colspan="2" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblElecRetailer" runat="server"></asp:Label>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Meter Upgrade:
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblMeterUG" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Other Variation:
                                </td>
                                <td colspan="2" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblVariationOther" runat="server"></asp:Label>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Off Peak:
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblOffPeak" runat="server"></asp:Label>
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"></td>
                                <td colspan="2" align="left" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"></td>
                                <td style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">NMI:
                                </td>
                                <td style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <asp:Label ID="lblNMINumber" runat="server"></asp:Label>
                                </td>
                                <td colspan="4" style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">No. of STC applicable to this property &nbsp;<asp:Label ID="lblSTCAddress" runat="server"></asp:Label>&nbsp;is
                                    <b>
                                        <asp:Label ID="lblSTCNo" runat="server"></asp:Label></b>&nbsp;STC
                                </td>
                                <%--<td colspan="3" align="left" style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"></td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                </td>--%>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #ccc; margin-top: 0px;">
                            <tr>
                                <td width="29%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; background: #CCC; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>System Details</strong>
                                </td>
                                <td colspan="4" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong id="Strong1" runat="server" visible="false">Supply and Install Complete Solar System:</strong>
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"
                                    align="right">
                                    <strong>
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Literal ID="litServiceValue" runat="server"></asp:Literal>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"
                                    rowspan="2" colspan="2">
                                    <strong>Panel Details:</strong><br />
                                    <asp:Label ID="lblPanelDetails" runat="server"></asp:Label>
                                </td>
                                <td width="21%" align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">House Type:
                                </td>
                                <td width="17%" align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarHouseType" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Roof Type:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarRoofType" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"
                                    rowspan="2" colspan="2">
                                    <strong>Inverter Details:</strong><br />
                                    <asp:Label ID="lblInverterDetails" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Roof Angle:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarRoofAngle" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Meter Upgrade:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarMeterUG" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"
                                    rowspan="2" valign="top" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong id="tdQuotationNotes" runat="server">Quotation Notes:</strong><br />
                                    <asp:Label ID="lblQuotationNotes" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Asbestos:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarAsbestos" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>

                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Split System:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarSplitSystem" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"
                                    rowspan="2" colspan="2">
                                    <strong id="tdFaultIdentified" runat="server">Fault Identified:</strong><br />
                                    <asp:Label ID="lblFaultIdentified" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">Cherry Picker:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarCherryPicker" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Travel Cost:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarTravelTime" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"
                                    rowspan="2" colspan="2">
                                    <strong id="tdActionsRequired" runat="server">Actions Required:</strong><br />
                                    <asp:Label ID="lblActionsRequired" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Other Variation:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarOther" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>

                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Meter Installation:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblVarMeterInstall" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"
                                    rowspan="2" colspan="2">
                                    <strong id="tdWorkDone" runat="server">Work Done:</strong><br />
                                    <asp:Label ID="lblWorkDone" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">Special Discount:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblSpecialDiscount" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <%-- <tr>
                                <td align="right" style="border-right: solid 1px #ccc; border-left: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Less Govt Rebate:</strong>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblRECRebate" runat="server"></asp:Label>
                                </td>
                            </tr>--%>
                            <tr>
                                <td align="right" style="border-left: solid 1px #ccc; border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Total Cost:</strong>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblTotalQuotePrice" runat="server"></asp:Label></strong>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td1" colspan="2" rowspan="1" valign="top" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;" runat="server">
                                    <strong id="tdPromoNotes" runat="server" visible="false">Promo Notes</strong><br>
                                    <asp:Label ID="lblPromoNotes" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="border-left: solid 1px #ccc; border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">Deposit Required:
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblDepositRequired" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;"
                                    colspan="2">
                                    <strong>
                                        <asp:Label ID="lblFinanceStatus" runat="server"></asp:Label></strong>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>Balance Due After Deposit:</strong>
                                </td>
                                <td align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 14px;">
                                    <strong>
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" /><asp:Label ID="lblBalanceToPay" runat="server"></asp:Label></strong>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #ccc; border-top: 1px solid #ccc; margin-top: 15px;">
                            <tr>
                                <td colspan="4" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Payment Schedule: </strong>10% advance deposit payable with order and balance
                                payment on the day of installation or prior.<br>
                                    Note: This quotation is calculated based upon the solar credit grant. Customer agrees
                                that he did not receive any solar grant in the past and also agrees to transfer
                                STCs to Euro Solar or Euro Solar Agent. All products and services are supplied subject
                                to Euro Solar terms and conditions attached.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-right: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>*Upgrade of Meter Box</strong> (if needed) will be negotiated and charged
                                by our Electrician directly. The cost of the replacement of your current electric
                                meter has to be borne by you, which can be legal charges imposed by the Electricity
                                Distributor. Customer agrees to pay the balance amount of the system plus any additional
                                costs towards the system installation. All units of model panels are manufactured in china
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>This quotation is valid for 7 days from issue date.</strong>
                                </td>
                                <td colspan="2" align="right" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">Licence: QLD - 74095, VIC – 22163, TAS - 1300870
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #ccc; margin-top: 0px;">
                            <tr>
                                <td colspan="6" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <h2 style="margin: 0px; padding: 2px 0px; font-size: 16px;">
                                        <strong>Payment Options</strong></h2>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding5px; background: #CCC; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>EFT / Bank Transfer</strong>
                                </td>
                                <td width="10%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">&nbsp;
                                </td>
                                <td width="18%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; background: #CCC; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Credit Card</strong>
                                </td>
                                <td width="16%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">&nbsp;
                                </td>
                                <td width="18%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; background: #CCC; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Cheque / MO</strong>
                                </td>
                                <td width="18%" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">A/c Name: Euro Solar<br />
                                    BSB: 034037 A/c No. 250324<br />
                                    Bank Name: WESTPAC<br />
                                    Please Mention Your Project No as Ref.
                                </td>
                                <td colspan="2" style="border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">(Master / Visa cards)<br />
                                    with 1.5% surcharge
                                </td>
                                <td colspan="2" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">Cheque payable to &quot;Euro Solar&quot; and mail it to<br />
                                    PO BOX 570, ARCHERFIELD, QLD 4108.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Please sign the form below or Pay Deposit if you wish to proceed with this quotation.</strong>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #ccc; margin-top: 0px;">
                            <tr>
                                <td colspan="4" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">I agree to proceed with this quotation / contract and by paying the Deposit I acknowledge
                                that I am aware of and accept the terms and conditions of the contract stated above
                                and overleaf.
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">&nbsp;
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">&nbsp;
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">&nbsp;
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">Customer’s Name
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">Customer’s Signature
                                </td>
                                <td style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc; border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">Date
                                </td>
                            </tr>
                        </table>
                        <%--<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="margin-top: 20px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/clean_energy.jpg") %>" width="120px" />
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Queensland<br>
                                    </strong>P O Box 570,
                                <br>
                                    Archerfield, QLD, 4108
                                </td>
                                <td valign="top" style="border-collapse: collapse; padding: 2px; vertical-align: middle; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Victoria<br>
                                    </strong>126/45 Gilby Road,
                                <br>
                                    Mount Waverley, VIC, 3149
                                </td>
                                <td style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                                    <strong>Tasmania<br>
                                    </strong>4A Swanston Road,<br>
                                    Waverley, TAS, 7250
                                </td>
                                <td style="padding-top: 20px;" id="tdQLDimage1" runat="server">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/roarlogo.jpg") %>" width="120px" />
                                </td>
                            </tr>
                        </table>--%>
                        <%--<div style="height: 40px;">
                        </div>--%>
                        <div style="clear: both; page-break-before: always;">
                        </div>
                        <%-- 
                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 0px 0px 0px 10px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/eurosolarlogo.png") %>"
                                width="124" height="100" />
                        </div>--%>
                        <h3 style="text-align: center; font-family: 'OpenSansLight', sans-serif; margin: 5px;">Meter Box Photos</h3>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 5px;">
                            <tr>
                                <td colspan="4" style="border-collapse: collapse; padding: 2px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">To avoid delay in the installation process please provide us your meter box and/or
                                switch board photo. If you are not able to provide the photo please select one of
                                the image which looks similar to your meter box.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="padding: 2px; margin-top: 5px; font-family: 'OpenSansLight', sans-serif; font-size: 12px;">
                                    <span style="border-bottom: 1px solid #ccc; border-collapse: collapse;">Meter box/ Switch
                                    board requirements for solar system installation:</span>
                                    <ul style="margin-top: 2px; padding-top: 2px; margin-bottom: 2px; padding-bottom: 2px; margin-left: 0px; padding-left: 10px;">
                                        <li>Meters and Switches must be in metal box</li>
                                        <li>The back board behind the Fuses and Meter must be Fire resistant</li>
                                        <li>No Visible Wiring</li>
                                        <li>The Switch board is in the same premises where Solar system needs to be installed
                                        – if Not, Please advise your sales representative OR call 1300 959013</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        <div style="width: 100%; margin-top: 20px;" class="pdfimages">
                            <ul>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img1.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img2.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img3.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img4.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img5.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img6.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img8.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img9.jpg") %>" />
                                </li>
                                <li>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdfimage/img7.jpg") %>" />
                                </li>
                            </ul>
                            <div style="clear: both;">
                            </div>
                        </div>
                        <div style="clear: both; page-break-before: always;">
                        </div>
                        <div id="divQLD" runat="server" visible="false">
                            <h3 style="text-align: center; width: 100%; font-family: 'OpenSansLight', sans-serif; margin: 5px;">P & N PTY LTD T/A ARISESOLAR TERMS & CONDITIONS<br>
                                Agreement</h3>
                            <div style="clear: both">
                            </div>
                            <table width="48%" style="float: left;" border="0" cellspacing="0" cellpadding="0"
                                class="terms">
                                <tr>
                                    <td width="3%" valign="top" style="padding-top: 5px;">
                                        <strong>1</strong>
                                    </td>
                                    <td width="97%" style="padding-top: 5px;">
                                        <strong>Customer Declarations:</strong><br>
                                        You declare that<br>
                                        1.1 You are over the legal age of 18 years<br>
                                        1.2 You are one of the registered owners of the property at the installation address
                                    and your name is on the title deed of the Installation Address<br>
                                        1.3 You have never received or have never been approved for any rebate, financial
                                    assistance solar credit or small scale technology certificate (STCs) for small generation
                                    solar power system at the Installation Address.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>2</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Payment & STC's:</strong><br>
                                        2.1. EURO SOLAR must have the final payment prior to OR on the day of the installation
                                    by Credit Card or Cheque. If you prefer Electronic Fund transfer (EFT), the transaction
                                    must be completed 48 Hours prior to installation and you must provide us with written
                                    proof of the transfer. The customer must provide Credit Card details/cheque/ EFT
                                    Receipt to Installer prior to the Installation. Installation will not be carried
                                    out unless payment has been received by us.<br>
                                        2.2 You are agreeing to pay EURO SOLAR the STC's as part payment for your system.
                                    The STC's will be paid directly to EURO SOLAR or a EURO SOLAR Agent. If the Office
                                    of Clean Energy Regulator (http://ret.cleanenergyregulator.gov.au/) determines you
                                    are not eligible to receive STC's, and therefore EURO SOLAR is unable to receive
                                    the STC's as part payment, you will be liable to pay EURO SOLAR the value of the
                                    STC's, as determined by market rates.<br>
                                        2.3 If you are not eligible for STC's, or if you wish to claim the STC's incentive
                                    yourself, the complete payment (including trading costs of the STC's) of the system
                                    is due before installation.<br />
                                        2.4 You acknowledge that if you breach any conditions of the STC's incentive regulations,
                                    you may be financially liable to the Office of Clean Energy Regulator. (http://ret.cleanenergyregulator.gov.au/).
                                    If you commit any breach of the Incentive Regulation, you acknowledge that EURO
                                    SOLAR will not be liable to you.<br>
                                        2.5 EURO SOLAR or a EURO SOLAR Agent will arrange for the complete documentation
                                    and processing of the sale of STC's. You acknowledge that the price of STCs is governed
                                    by market movements and the REC guidelines. If the market price falls below a certain
                                    price, EURO SOLAR may decide to delay the installation until the price rises to
                                    a higher level. EURO SOLAR may also refuse to carry out installation and refund
                                    your complete deposit. If the market price follow below the certain price, as determined
                                    by EURO SOLAR at its absolute discretion.<br>
                                        2.6 If you fail to pay any amount that is due and payable under this agreement,
                                    EURO SOLAR will be entitled to charge interest on the unpaid amount at the current
                                    Reserve Bank target cash rate plus 2%. You will also have to pay EURO SOLAR any
                                    costs associated with the recovery of such unpaid amounts.<br>
                                        2.7 Title and ownership of the solar system will vets in you upon EURO SOLAR receiving
                                    complete payment from you for the cost of the solar system.<br>
                                        2.8 Failure to pay the complete amount may result in EURO SOLAR taking legal action
                                    against you and it will void all warranties.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>3</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Authority of access to property</strong><br>
                                        3.1. You authorise EURO SOLAR and its contractors, employees and installers. Full
                                    access to the property at all reasonable times to carry out all work associated
                                    with the installation of your solar system including site Inspections, the signing
                                    of required paperwork, the delivery and installation of the PV Solar system, and
                                    connection to the grid.<br>
                                        3.2 Your co-operation is required to enable site inspections/installation to occur
                                    at the earliest possible time which is convenient to EURO SOLAR.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>4</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Liabilities and Risk</strong><br>
                                        4.1 The ownership and insurance risk of the PV solar system passes to you upon installation
                                    and EURO SOLAR receiving full payment for the solar system. It is your responsibility
                                    to ensure that your property insurance adequately covers the cost of your PV solar
                                    system.<br>
                                        4.2 You acknowledge EURO SOLAR accepts no liability or responsibility for your STC's
                                    incentive as administered by the Office of Clean Energy Regulator (http://ret.cleanenergyregulator.gov.au/).<br>
                                        4.3 You acknowledge EURO SOLAR accepts no liability or responsibility for your STC's
                                    Bonus, also known as the "feed in tariff incentive" as administered by the relevant
                                    State or Territory government.<br>
                                        4.4 EURO SOLAR accepts no responsibility for any damage or loss caused to your property
                                    by the installer which has not been caused by the installer's negligence. All EURO
                                    SOLAR installers are subcontractors who are required by EURO SOLAR to have appropriate
                                    third party damage insurance, under relevant state and territory laws. EURO SOLAR
                                    will work with you and the installer to rectify any damage caused to your property
                                    by the installer's negligence.<br>
                                        4.5 You acknowledge that EURO SOLAR will not be responsible for any damage caused
                                    to old and brittle roofing tiles that may be cracked or damaged during installation.<br>
                                        4.6 EURO SOLAR accepts no responsibility for any additional costs associated you're
                                    your need to obtain an upgrade of your existing meter box upgrade or the installation
                                    of a new gross/net. EURO SOLAR will not be liable for any unexpected costs which
                                    may arise in relation to the removal and handling of asbestos at your property in
                                    relation to the installation of the solar system.<br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <strong>5</strong>
                                    </td>
                                    <td>
                                        <strong>Meter</strong><br>
                                        5.1 EURO SOLAR quote does not include the costs of any wiring or any associated
                                    work required to connect your inverter to a meter box which is remotely located
                                    from the inverter.
                                    </td>
                                </tr>
                            </table>
                            <table width="48%" style="float: right; margin-top: 10px;" border="0" cellspacing="0"
                                cellpadding="0" class="terms">
                                <tr>
                                    <td width="3%">&nbsp;
                                    </td>
                                    <td width="97%">(i.e. where the meter box is not attached to the same structure as the inverter
                                    such as the house or the garage.) In the event that the meter box is located remotely
                                    from the inverter location and such costs are not provided for in the relevant EURO
                                    SOLAR quote, you will be required to arrange and pay for the performance of this
                                    electrical work separately from the installation of the solar system.<br>
                                        5.2 Addition minimum charges may be applicable such as $ 200 to split the system
                                    in more than one row, $400 for horizontally fixing array, and extra charges for
                                    meter box upgrade, if applicable.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>6</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Delivery and Installation</strong><br>
                                        6.1 EURO SOLAR will make every reasonable effort to install your system in a timely
                                    manner. However, we will not be bound to meeting estimated or proposed delivery,
                                    installation or system completion dates as we have no control over for example,
                                    worldwide materials availability, peaks in demand created by changes in government
                                    legislation, inclement weather and/or other forms of force majeure. Delayed installation
                                    or grid connection is not a valid reason for claiming a refund or compensation from
                                    EURO SOLAR. Please note that no responsibility for the delay in installation is
                                    acceptable if customer details (including installation site) are incorrect.<br>
                                        6.2 Delays in installation are not grounds for cancellation and EURO SOLAR is not
                                    liable to you for any perceived loss as a result of these delays.<br>
                                        6.3 If difficulties with site access are encountered that were not notified to EURO
                                    SOLAR at the time of quote and/or offered by EURO SOLAR to the Customer, additional
                                    costs incurred in ensuring the safety of our installers may be payable by the Customer.<br>
                                        6.4 Before the installation can commence, and modifications to your roof take place,
                                    you are required to arrange payment with the scheduling department for the balance
                                    payment of the system and that the goods remain the property of EURO SOLAR until
                                    your payment or finance payment, if applicable, is received.<br>
                                        6.5 A Home owner must be present during installation to sign the mandatory declaration
                                    assigning the STCs to EURO SOLAR, as per the renewable Energy Act (2000). Should
                                    the installer arrive on the agreed date and the home owner is not present, a rescheduling
                                    fee of $200.00 will apply and the installation will be delayed.<br>
                                        6.6 If you have any preference regarding panel/Inverter placement, you are required
                                    to inform the installer when they arrive on property.<br>
                                        6.7 If you are not present at the property during the time of installation, you
                                    are required to authorise an adult to be present at the property to sign all document
                                    and assume complete responsibility.<br>
                                        6.8 Upon signing the agreement, any requests for modifications to the material/equipment
                                    will incur an admin charge of $200. A new quote will be generated for any changes
                                    required.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>7</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Privacy Policy</strong><br>
                                        7.1 You agree to provide EURO SOLAR with whatever personal information is required
                                    for the efficient functioning of EURO SOLAR on your behalf, in particular for the
                                    accurate completion of the paperwork for the STC's incentive (selling the STCs)
                                    and network connection to the grid.<br>
                                        7.2. EURO SOLAR will provide your information to its contractors, employees and
                                    installers only as required to effectively perform their duties.<br>
                                        7.3 EURO SOLAR will provide your information, on your behalf to the relevant bodies
                                    for the processing the STC's incentive (selling the STCs), to the electricity distributor
                                    for connecting your PV Solar system to the grid and if required to your electricity
                                    retailer.<br>
                                        7.4 Unless otherwise agreed with you, EURO SOLAR will not provide your personal
                                    information to any third parties other than those mentioned above.<br>
                                        7.5 You must sign all necessary documents on the date of installation for the performance
                                    of all party's obligations under this agreement.<br>
                                        7.6 EURO SOLAR will not sell your personal information under any circumstances.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>8</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Product Warranties</strong><br>
                                        The system comes with up to 10 years installation warranty from EURO SOLAR in conjunction
                                    with the manufacturer's warranty. Warranty booklet will be presented to you at installation
                                    which outlines that the solar system requires servicing every two years by a licenced
                                    solar installer. EURO SOLAR does not provide any kind of warranties on the Monitoring
                                    system which comes with the inverter
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>9</strong>
                                    </td>
                                    <td>
                                        <strong>Cooling off Period</strong><br>
                                        You understand that under Australian Consumer Law and relevant State Building Acts
                                    you are entitled to a cooling-off period of ten (10) working days – where required.
                                    All notifications must be received by EURO SOLAR in writing via email, fax or post.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>10</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>&nbsp; Finance</strong><br>
                                        Financing is not available on advertised specials.<br>
                                        Terms and Condition of repayment of finance and interest will be provided by the
                                    Finance Company.
                                    </td>
                                    <tr>
                                        <td style="padding-top: 5px;" valign="top">
                                            <strong>11</strong>
                                        </td>
                                        <td style="padding-top: 5px;">
                                            <strong>&nbsp;Termination</strong><br>
                                            11.1 EURO SOLAR may terminate this contract with you if either of the following
                                        occurs:<br>
                                            11.1.1 You do not abide by the terms and conditions.<br>
                                            11.1.2 There are delays in the EURO SOLAR process causing supplier prices to increase;
                                        in which case full deposit will be refunded.<br>
                                            11.1.3 EURO SOLAR has full authority to cancel the installation if installation
                                        can't be possible. Customer will receive full refund of any deposit by cheque or
                                        other method.<br>
                                            11.1.4 You may terminate this agreement or contract within the first 10 days of
                                        quote date. You may be eligible to get a refund of deposit after deducting administration
                                        charges. We normally procure the material or equipment required within two weeks
                                        of receiving the signed copy of the contract. Customers would have to pay material
                                        cost if the job is cancelled after two week of receiving the signed copy of contract.
                                        </td>
                                    </tr>
                            </table>
                            <div style="clear: both">
                            </div>
                            <table style="margin-top: 5px;">
                                <tr>
                                    <td align="center" style="font-family: 'OpenSansLight', sans-serif; width: 70%; margin: 0px auto 0px auto;">This agreement is made between EURO SOLAR (95 130 845 199) ("company") and
                                    <asp:Label ID="lblFooterCust1" runat="server"></asp:Label>
                                        at
                                    <asp:Label ID="lblFooterAdd1" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table style="float: left; width: 45%;">
                                <tr>
                                    <td width="38%;">
                                        <strong>Customer Signature:</strong>
                                    </td>
                                    <td style="border-bottom: 1px solid #ccc;"></td>
                                </tr>
                            </table>
                            <table style="float: right; width: 45%;">
                                <tr>
                                    <td width="10%;">
                                        <strong>Dated:</strong>
                                    </td>
                                    <td style="border-bottom: 1px solid #ccc;"></td>
                                </tr>
                            </table>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div id="divTAS" runat="server" visible="false">
                            <h3 style="text-align: center; width: 85%; font-family: 'OpenSansLight', sans-serif; margin: 5px;">P & N PTY LTD T/A ARISESOLAR TERMS & CONDITIONS<br>
                                Agreement</h3>
                            <div style="clear: both">
                            </div>
                            <table width="48%" style="float: left; margin-top: 10px;" border="0" cellspacing="0"
                                cellpadding="0" class="terms">
                                <tr>
                                    <td width="3%" valign="top">
                                        <strong>1</strong>
                                    </td>
                                    <td width="97%">
                                        <strong>Customer Declarations:</strong><br>
                                        You declare that<br>
                                        1.1 You are over the legal age of 18 years<br>
                                        1.2 You are one of the registered owners of the property at the installation address
                                    and your name is on the title deed of the Installation Address
                                    <br>
                                        1.3 You have never received or have never been approved for any rebate, financial
                                    assistance solar credit or small scale technology certificate (STCs) for small generation
                                    solar power system at the Installation Address.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>2</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Payment & STC's:</strong><br>
                                        2.1. EURO SOLAR must have the final payment prior to OR on the day of the installation
                                    by Credit Card or Cheque. If you prefer Electronic Fund transfer (EFT), the transaction
                                    must be completed 48 Hours prior to installation and you must provide us with written
                                    proof of the transfer. The customer must provide Credit Card details/cheque/ EFT
                                    Receipt to Installer prior to the Installation. Installation will not be carried
                                    out unless payment has been received by us.
                                    <br>
                                        2.2 You are agreeing to pay EURO SOLAR the STC's as part payment for your system.
                                    The STC's will be paid directly to EURO SOLAR or a EURO SOLAR Agent. If the Office
                                    of Clean Energy Regulator (http://ret.cleanenergyregulator.gov.au/) determines you
                                    are not eligible to receive STC's, and therefore EURO SOLAR is unable to receive
                                    the STC's as part payment, you will be liable to pay EURO SOLAR the value of the
                                    STC's, as determined by market rates.<br>
                                        2.3 If you are not eligible for STC's, or if you wish to claim the STC's incentive
                                    yourself, the complete payment (including trading costs of the STC's) of the system
                                    is due before installation.<br>
                                        2.4 You acknowledge that if you breach any conditions of the STC's incentive regulations,
                                    you may be financially liable to the Office of Clean Energy Regulator. (http://ret.cleanenergyregulator.gov.au/).
                                    If you commit any breach of the Incentive Regulation, you acknowledge that EURO
                                    SOLAR will not be liable to you.<br>
                                        2.5 EURO SOLAR or a EURO SOLAR Agent will arrange for the complete documentation
                                    and processing of the sale of STC's. You acknowledge that the price of STCs is governed
                                    by market movements and the REC guidelines. If the market price falls below a certain
                                    price, EURO SOLAR may decide to delay the installation until the price rises to
                                    a higher level. EURO SOLAR may also refuse to carry out installation and refund
                                    your complete deposit. If the market price follow below the certain price, as determined
                                    by EURO SOLAR at its absolute discretion.<br>
                                        2.6 If you fail to pay any amount that is due and payable under this agreement,
                                    EURO SOLAR will be entitled to charge interest on the unpaid amount at the current
                                    Reserve Bank target cash rate plus 2%. You will also have to pay EURO SOLAR any
                                    costs associated with the recovery of such unpaid amounts.<br>
                                        2.7 Title and ownership of the solar system will vets in you upon EURO SOLAR receiving
                                    complete payment from you for the cost of the solar system.<br>
                                        2.8 Failure to pay the complete amount may result in EURO SOLAR taking legal action
                                    against you and it will void all warranties.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>3</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Authority of access to property</strong><br>
                                        3.1. You authorise EURO SOLAR and its contractors, employees and installers. Full
                                    access to the property at all reasonable times to carry out all work associated
                                    with the installation of your solar system including site Inspections, the signing
                                    of required paperwork, the delivery and installation of the PV Solar system, and
                                    connection to the grid.<br>
                                        3.2 Your co-operation is required to enable site inspections/installation to occur
                                    at the earliest possible time which is convenient to EURO SOLAR.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>4</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Liabilities and Risk</strong><br>
                                        4.1 The ownership and insurance risk of the PV solar system passes to you upon installation
                                    and EURO SOLAR receiving full payment for the solar system. It is your responsibility
                                    to ensure that your property insurance adequately covers the cost of your PV solar
                                    system.<br>
                                        4.2 You acknowledge EURO SOLAR accepts no liability or responsibility for your STC's
                                    incentive as administered by the Office of Clean Energy Regulator (http://ret.cleanenergyregulator.gov.au/).<br>
                                        4.3 You acknowledge EURO SOLAR accepts no liability or responsibility for your STC's
                                    Bonus, also known as the "feed in tariff incentive" as administered by the relevant
                                    State or Territory government.<br>
                                        4.4 EURO SOLAR accepts no responsibility for any damage or loss caused to your property
                                    by the installer which has not been caused by the installer's negligence. All EURO
                                    SOLAR installers are subcontractors who are required by EURO SOLAR to have appropriate
                                    third party damage insurance, under relevant state and territory laws. EURO SOLAR
                                    will work with you and the installer to rectify any damage caused to your property
                                    by the installer's negligence.<br>
                                        4.5 You acknowledge that EURO SOLAR will not be responsible for any damage caused
                                    to old and brittle roofing tiles that may be cracked or damaged during installation.<br>
                                        4.6 EURO SOLAR accepts no responsibility for any additional costs associated you're
                                    your need to obtain an upgrade of your existing meter box upgrade or the installation
                                    of a new gross/net. EURO SOLAR will not be liable for any unexpected costs which
                                    may arise in relation to the removal and handling of asbestos at your property in
                                    relation to the installation of the solar system.<br>
                                        4.7 If customer is connected with any Electric Hot water/Slab Heating/Climate Saver/Off
                                    Peak Meter then customers need to arrange wiring work at their own cost, including
                                    installation of timer/contactors/any amps switches to control the system. Euro Solar
                                    is not responsible to any costing for that and any delay comes during your grid
                                    connection process
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <strong>5</strong>
                                    </td>
                                    <td>
                                        <strong>Meter</strong><br>
                                        5.1 EURO SOLAR quote does not include the costs of any wiring or any associated
                                    work required to connect your inverter to a meter box which is remotely located
                                    from the inverter.
                                    </td>
                                </tr>
                            </table>
                            <table width="48%" style="float: right; margin-top: 10px;" border="0" cellspacing="0"
                                cellpadding="0" class="terms">
                                <tr>
                                    <td width="3%">&nbsp;
                                    </td>
                                    <td width="97%">(i.e. where the meter box is not attached to the same structure as the inverter
                                    such as the house or the garage.) In the event that the meter box is located remotely
                                    from the inverter location and such costs are not provided for in the relevant EURO
                                    SOLAR quote, you will be required to arrange and pay for the performance of this
                                    electrical work separately from the installation of the solar system.<br>
                                        5.2 Addition minimum charges may be applicable such as $ 200 to split the system
                                    in more than one row, $400 for horizontally fixing array, and extra charges for
                                    meter box upgrade, if applicable.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>6</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Delivery and Installation</strong><br>
                                        6.1 EURO SOLAR will make every reasonable effort to install your system in a timely
                                    manner. However, we will not be bound to meeting estimated or proposed delivery,
                                    installation or system completion dates as we have no control over for example,
                                    worldwide materials availability, peaks in demand created by changes in government
                                    legislation, inclement weather and/or other forms of force majeure. Delayed installation
                                    or grid connection is not a valid reason for claiming a refund or compensation from
                                    EURO SOLAR. Please note that no responsibility for the delay in installation is
                                    acceptable if customer details (including installation site) are incorrect.<br>
                                        6.2 Delays in installation are not grounds for cancellation and EURO SOLAR is not
                                    liable to you for any perceived loss as a result of these delays.<br>
                                        6.3 If difficulties with site access are encountered that were not notified to EURO
                                    SOLAR at the time of quote and/or offered by EURO SOLAR to the Customer, additional
                                    costs incurred in ensuring the safety of our installers may be payable by the Customer.<br>
                                        6.4 Before the installation can commence, and modifications to your roof take place,
                                    you are required to arrange payment with the scheduling department for the balance
                                    payment of the system and that the goods remain the property of EURO SOLAR until
                                    your payment or finance payment, if applicable, is received.<br>
                                        6.5 A Home owner must be present during installation to sign the mandatory declaration
                                    assigning the STCs to EURO SOLAR, as per the renewable Energy Act (2000). Should
                                    the installer arrive on the agreed date and the home owner is not present, a rescheduling
                                    fee of $200.00 will apply and the installation will be delayed.<br>
                                        6.6 If you have any preference regarding panel/Inverter placement, you are required
                                    to inform the installer when they arrive on property.<br>
                                        6.7 If you are not present at the property during the time of installation, you
                                    are required to authorise an adult to be present at the property to sign all document
                                    and assume complete responsibility.<br>
                                        6.8 Upon signing the agreement, any requests for modifications to the material/equipment
                                    will incur an admin charge of $200. A new quote will be generated for any changes
                                    required.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>7</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Privacy Policy</strong><br>
                                        7.1 You agree to provide EURO SOLAR with whatever personal information is required
                                    for the efficient functioning of EURO SOLAR on your behalf, in particular for the
                                    accurate completion of the paperwork for the STC's incentive (selling the STCs)
                                    and network connection to the grid.<br>
                                        7.2. EURO SOLAR will provide your information to its contractors, employees and
                                    installers only as required to effectively perform their duties.<br>
                                        7.3 EURO SOLAR will provide your information, on your behalf to the relevant bodies
                                    for the processing the STC's incentive (selling the STCs), to the electricity distributor
                                    for connecting your PV Solar system to the grid and if required to your electricity
                                    retailer.<br>
                                        7.4 Unless otherwise agreed with you, EURO SOLAR will not provide your personal
                                    information to any third parties other than those mentioned above.<br>
                                        7.5 You must sign all necessary documents on the date of installation for the performance
                                    of all party's obligations under this agreement.<br>
                                        7.6 EURO SOLAR will not sell your personal information under any circumstances.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>8</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Product Warranties</strong><br>
                                        The system comes with up to 10 years installation warranty from EURO SOLAR in conjunction
                                    with the manufacturer's warranty. Warranty booklet will be presented to you at installation
                                    which outlines that the solar system requires servicing every two years by a licenced
                                    solar installer. EURO SOLAR does not provide any kind of warranties on the Monitoring
                                    system which comes with the inverter
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>9</strong>
                                    </td>
                                    <td>
                                        <strong>Cooling off Period</strong><br>
                                        You understand that under Australian Consumer Law and relevant State Building Acts
                                    you are entitled to a cooling-off period of ten (10) working days – where required.
                                    All notifications must be received by EURO SOLAR in writing via email, fax or post.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>10</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>&nbsp; Finance</strong><br>
                                        Financing is not available on advertised specials.<br>
                                        Terms and Condition of repayment of finance and interest will be provided by the
                                    Finance Company.
                                    </td>
                                    <tr>
                                        <td style="padding-top: 5px;" valign="top">
                                            <strong>11</strong>
                                        </td>
                                        <td style="padding-top: 5px;">
                                            <strong>&nbsp;Termination</strong><br>
                                            11.1 EURO SOLAR may terminate this contract with you if either of the following
                                        occurs:<br>
                                            11.1.1 You do not abide by the terms and conditions.<br>
                                            11.1.2 There are delays in the EURO SOLAR process causing supplier prices to increase;
                                        in which case full deposit will be refunded.<br>
                                            11.1.3 EURO SOLAR has full authority to cancel the installation if installation
                                        can't be possible. Customer will receive full refund of any deposit by cheque or
                                        other method.<br>
                                            11.1.4 You may terminate this agreement or contract within the first 10 days of
                                        quote date. You may be eligible to get a refund of deposit after deducting administration
                                        charges. We normally procure the material or equipment required within two weeks
                                        of receiving the signed copy of the contract. Customers would have to pay material
                                        cost if the job is cancelled after two week of receiving the signed copy of contract.
                                        </td>
                                    </tr>
                            </table>
                            <div style="clear: both">
                            </div>
                            <table style="margin-top: 5px;">
                                <tr>
                                    <td align="center" style="font-family: 'OpenSansLight', sans-serif; width: 70%; margin: 0px auto 0px auto;">This agreement is made between EURO SOLAR (95 130 845 199) ("company") and
                                    <asp:Label ID="lblFooterCust2" runat="server"></asp:Label>
                                        at
                                    <asp:Label ID="lblFooterAdd2" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table style="float: left; width: 45%;">
                                <tr>
                                    <td width="38%;">
                                        <strong>Customer Signature:</strong>
                                    </td>
                                    <td style="border-bottom: 1px solid #ccc;"></td>
                                </tr>
                            </table>
                            <table style="float: right; width: 45%;">
                                <tr>
                                    <td width="10%;">
                                        <strong>Dated:</strong>
                                    </td>
                                    <td style="border-bottom: 1px solid #ccc;"></td>
                                </tr>
                            </table>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div id="divVIC" runat="server" visible="false">
                            <h3 style="text-align: center; width: 100%; font-family: 'OpenSansLight', sans-serif; margin: 5px;">P & N PTY LTD T/A ARISESOLAR TERMS & CONDITIONS<br />
                                Agreement</h3>
                            <div style="clear: both">
                            </div>
                            <table width="48%" style="float: left;" border="0" cellspacing="0" cellpadding="0"
                                class="terms">
                                <tr>
                                    <td width="3%" valign="top">
                                        <strong>1</strong>
                                    </td>
                                    <td width="97%">
                                        <strong>Customer Declarations:</strong><br>
                                        You declare that<br>
                                        1.1 You are over the legal age of 18 years<br>
                                        1.2 You are one of the registered owners of the property at the installation address
                                    and your name is on the title deed of the Installation Address<br>
                                        1.3 You have never received or have never been approved for any rebate, financial
                                    assistance solar credit or small scale technology certificate (STCs) for small generation
                                    solar power system at the Installation Address.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>2</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Payment & STC's:</strong><br>
                                        2.1. EURO SOLAR must have the final payment prior to OR on the day of the installation
                                    by Credit Card or Cheque. If you prefer Electronic Fund transfer (EFT), the transaction
                                    must be completed 48 Hours prior to installation and you must provide us with written
                                    proof of the transfer. The customer must provide Credit Card details/cheque/ EFT
                                    Receipt to Installer prior to the Installation. Installation will not be carried
                                    out unless payment has been received by us.<br>
                                        2.2 You are agreeing to pay EURO SOLAR the STC's as part payment for your system.
                                    The STC's will be paid directly to EURO SOLAR or a EURO SOLAR Agent. If the Office
                                    of Clean Energy Regulator (http://ret.cleanenergyregulator.gov.au/) determines you
                                    are not eligible to receive STC's, and therefore EURO SOLAR is unable to receive
                                    the STC's as part payment, you will be liable to pay EURO SOLAR the value of the
                                    STC's, as determined by market rates.<br>
                                        2.3 If you are not eligible for STC's, or if you wish to claim the STC's incentive
                                    yourself, the complete payment (including trading costs of the STC's) of the system
                                    is due before installation.<br>
                                        2.4 You acknowledge that if you breach any conditions of the STC's incentive regulations,
                                    you may be financially liable to the Office of Clean Energy Regulator. (http://ret.cleanenergyregulator.gov.au/).
                                    If you commit any breach of the Incentive Regulation, you acknowledge that EURO
                                    SOLAR will not be liable to you.<br>
                                        2.5 EURO SOLAR or a EURO SOLAR Agent will arrange for the complete documentation
                                    and processing of the sale of STC's. You acknowledge that the price of STCs is governed
                                    by market movements and the REC guidelines. If the market price falls below a certain
                                    price, EURO SOLAR may decide to delay the installation until the price rises to
                                    a higher level. EURO SOLAR may also refuse to carry out installation and refund
                                    your complete deposit. If the market price follow below the certain price, as determined
                                    by EURO SOLAR at its absolute discretion.<br>
                                        2.6 If you fail to pay any amount that is due and payable under this agreement,
                                    EURO SOLAR will be entitled to charge interest on the unpaid amount at the current
                                    Reserve Bank target cash rate plus 2%. You will also have to pay EURO SOLAR any
                                    costs associated with the recovery of such unpaid amounts.<br>
                                        2.7 Title and ownership of the solar system will vets in you upon EURO SOLAR receiving
                                    complete payment from you for the cost of the solar system.<br>
                                        2.8 Failure to pay the complete amount may result in EURO SOLAR taking legal action
                                    against you and it will void all warranties.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>3</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Authority of access to property</strong><br>
                                        3.1. You authorise EURO SOLAR and its contractors, employees and installers. Full
                                    access to the property at all reasonable times to carry out all work associated
                                    with the installation of your solar system including site Inspections, the signing
                                    of required paperwork, the delivery and installation of the PV Solar system, and
                                    connection to the grid.<br>
                                        3.2 Your co-operation is required to enable site inspections/installation to occur
                                    at the earliest possible time which is convenient to EURO SOLAR.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>4</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Liabilities and Risk</strong><br>
                                        4.1 The ownership and insurance risk of the PV solar system passes to you upon installation
                                    and EURO SOLAR receiving full payment for the solar system. It is your responsibility
                                    to ensure that your property insurance adequately covers the cost of your PV solar
                                    system.<br>
                                        4.2 You acknowledge EURO SOLAR accepts no liability or responsibility for your STC's
                                    incentive as administered by the Office of Clean Energy Regulator (http://ret.cleanenergyregulator.gov.au/).<br>
                                        4.3 You acknowledge EURO SOLAR accepts no liability or responsibility for your STC's
                                    Bonus, also known as the "feed in tariff incentive" as administered by the relevant
                                    State or Territory government.<br>
                                        4.4 EURO SOLAR accepts no responsibility for any damage or loss caused to your property
                                    by the installer which has not been caused by the installer's negligence. All EURO
                                    SOLAR installers are subcontractors who are required by EURO SOLAR to have appropriate
                                    third party damage insurance, under relevant state and territory laws. EURO SOLAR
                                    will work with you and the installer to rectify any damage caused to your property
                                    by the installer's negligence.<br>
                                        4.5 You acknowledge that EURO SOLAR will not be responsible for any damage caused
                                    to old and brittle roofing tiles that may be cracked or damaged during installation.<br>
                                        4.6 EURO SOLAR accepts no responsibility for any additional costs associated you're
                                    your need to obtain an upgrade of your existing meter box upgrade or the installation
                                    of a new gross/net. EURO SOLAR will not be liable for any unexpected costs which
                                    may arise in relation to the removal and handling of asbestos at your property in
                                    relation to the installation of the solar system.<br>
                                        4.7 If customer is connected with any Electric Hot water/Slab Heating/Climate Saver/Off
                                    Peak Meter then customers need to arrange wiring work at their own cost, including
                                    installation of timer/contactors/any amps switches to control the system. Euro Solar
                                    is not responsible to any costing for that and any delay comes during your grid
                                    connection process
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <strong>5</strong>
                                    </td>
                                    <td>
                                        <strong>Meter</strong><br>
                                        5.1 EURO SOLAR quote does not include the costs of any wiring or any associated
                                    work required to connect your inverter to a meter box which is remotely located
                                    from the inverter. (i.e. where the meter box is not attached to the same structure
                                    as the inverter such as the house or the garage.)
                                    </td>
                                </tr>
                            </table>
                            <table width="48%" style="float: right; margin-top: 10px;" border="0" cellspacing="0"
                                cellpadding="0" class="terms">
                                <tr>
                                    <td width="3%">&nbsp;
                                    </td>
                                    <td width="97%">In the event that the meter box is located remotely from the inverter location and
                                    such costs are not provided for in the relevant EURO SOLAR quote, you will be required
                                    to arrange and pay for the performance of this electrical work separately from the
                                    installation of the solar system.<br>
                                        5.2 Addition minimum charges may be applicable such as $ 200 to split the system
                                    in more than one row, $400 for horizontally fixing array, and extra charges for
                                    meter box upgrade, if applicable.<br>
                                        5.3 EURO SOLAR quote does not include the costs of reprogramming or reconfiguration
                                    of meter from your power distributor or electricity retailer. Also, EURO SOLAR is
                                    not responsible for any cost occurring to replace your meter or truck appointment
                                    during the grid connection process.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>6</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Delivery and Installation</strong><br>
                                        6.1 EURO SOLAR will make every reasonable effort to install your system in a timely
                                    manner. However, we will not be bound to meeting estimated or proposed delivery,
                                    installation or system completion dates as we have no control over for example,
                                    worldwide materials availability, peaks in demand created by changes in government
                                    legislation, inclement weather and/or other forms of force majeure. Delayed installation
                                    or grid connection is not a valid reason for claiming a refund or compensation from
                                    EURO SOLAR. Please note that no responsibility for the delay in installation is
                                    acceptable if customer details (including installation site) are incorrect.<br>
                                        6.2 Delays in installation are not grounds for cancellation and EURO SOLAR is not
                                    liable to you for any perceived loss as a result of these delays.<br>
                                        6.3 If difficulties with site access are encountered that were not notified to EURO
                                    SOLAR at the time of quote and/or offered by EURO SOLAR to the Customer, additional
                                    costs incurred in ensuring the safety of our installers may be payable by the Customer.<br>
                                        6.4 Before the installation can commence, and modifications to your roof take place,
                                    you are required to arrange payment with the scheduling department for the balance
                                    payment of the system and that the goods remain the property of EURO SOLAR until
                                    your payment or finance payment, if applicable, is received.<br>
                                        6.5 A Home owner must be present during installation to sign the mandatory declaration
                                    assigning the STCs to EURO SOLAR, as per the renewable Energy Act (2000). Should
                                    the installer arrive on the agreed date and the home owner is not present, a rescheduling
                                    fee of $200.00 will apply and the installation will be delayed.<br>
                                        6.6 If you have any preference regarding panel/Inverter placement, you are required
                                    to inform the installer when they arrive on property.<br>
                                        6.7 If you are not present at the property during the time of installation, you
                                    are required to authorise an adult to be present at the property to sign all document
                                    and assume complete responsibility.<br>
                                        6.8 Upon signing the agreement, any requests for modifications to the material/equipment
                                    will incur an admin charge of $200. A new quote will be generated for any changes
                                    required.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>7</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Privacy Policy</strong><br>
                                        7.1 You agree to provide EURO SOLAR with whatever personal information is required
                                    for the efficient functioning of EURO SOLAR on your behalf, in particular for the
                                    accurate completion of the paperwork for the STC's incentive (selling the STCs)
                                    and network connection to the grid.<br>
                                        7.2. EURO SOLAR will provide your information to its contractors, employees and
                                    installers only as required to effectively perform their duties.<br>
                                        7.3 EURO SOLAR will provide your information, on your behalf to the relevant bodies
                                    for the processing the STC's incentive (selling the STCs), to the electricity distributor
                                    for connecting your PV Solar system to the grid and if required to your electricity
                                    retailer.<br>
                                        7.4 Unless otherwise agreed with you, EURO SOLAR will not provide your personal
                                    information to any third parties other than those mentioned above.<br>
                                        7.5 You must sign all necessary documents on the date of installation for the performance
                                    of all party's obligations under this agreement.<br>
                                        7.6 EURO SOLAR will not sell your personal information under any circumstances.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>8</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>Product Warranties</strong><br>
                                        The system comes with up to 10 years installation warranty from EURO SOLAR in conjunction
                                    with the manufacturer's warranty. Warranty booklet will be presented to you at installation
                                    which outlines that the solar system requires servicing every two years by a licenced
                                    solar installer. EURO SOLAR does not provide any kind of warranties on the Monitoring
                                    system which comes with the inverter
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>9</strong>
                                    </td>
                                    <td>
                                        <strong>Cooling off Period</strong><br>
                                        You understand that under Australian Consumer Law and relevant State Building Acts
                                    you are entitled to a cooling-off period of ten (10) working days – where required.
                                    All notifications must be received by EURO SOLAR in writing via email, fax or post.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>10</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>&nbsp; Finance</strong><br>
                                        Financing is not available on advertised specials.<br>
                                        Terms and Condition of repayment of finance and interest will be provided by the
                                    Finance Company.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 4px;"></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" valign="top">
                                        <strong>11</strong>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <strong>&nbsp;Termination</strong><br>
                                        11.1 EURO SOLAR may terminate this contract with you if either of the following
                                    occurs:<br>
                                        11.1.1 You do not abide by the terms and conditions.<br>
                                        11.1.2 There are delays in the EURO SOLAR process causing supplier prices to increase;
                                    in which case full deposit will be refunded.<br>
                                        11.1.3 EURO SOLAR has full authority to cancel the installation if installation
                                    can't be possible. Customer will receive full refund of any deposit by cheque or
                                    other method.<br>
                                        11.1.4 You may terminate this agreement or contract within the first 10 days of
                                    quote date. You may be eligible to get a refund of deposit after deducting administration
                                    charges. We normally procure the material or equipment required within two weeks
                                    of receiving the signed copy of the contract. Customers would have to pay material
                                    cost if the job is cancelled after two week of receiving the signed copy of contract.
                                    </td>
                                </tr>
                            </table>
                            <div style="clear: both">
                            </div>
                            <table style="margin-top: 5px;">
                                <tr>
                                    <td align="center" style="font-family: 'OpenSansLight', sans-serif; width: 70%; margin: 0px auto 0px auto;">This agreement is made between EURO SOLAR (95 130 845 199) ("company") and
                                    <asp:Label ID="lblFooterCust3" runat="server"></asp:Label>
                                        at
                                    <asp:Label ID="lblFooterAdd3" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div style="clear: both">
                                <br />
                            </div>
                            <table style="float: left; width: 45%;">
                                <tr>
                                    <td width="38%;">
                                        <strong>Customer Signature:</strong>
                                    </td>
                                    <td style="border-bottom: 1px solid #ccc;"></td>
                                </tr>
                            </table>
                            <table style="float: right; width: 45%;">
                                <tr>
                                    <td width="10%;">
                                        <strong>Dated:</strong>
                                    </td>
                                    <td style="border-bottom: 1px solid #ccc;"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
