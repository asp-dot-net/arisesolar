﻿using System;
using System.IO;

public partial class mailtemplate_quatationdesign : System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            string ProjectID =Request.QueryString["id"];
            //string ProjectID = "-2146260946";
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblProjects2 stPro2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
          
            SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);

            //if (stPro.InstallState == "QLD")
            //{
            //    divQLD.Visible = true;
            //}
            //else
            //{
            //    if (stPro.InstallState == "VIC")
            //    {
            //        divVIC.Visible = true;
            //    }
            //    else
            //    {
            //        if (stPro.InstallState == "TAS")
            //        {
            //            divTAS.Visible = true;
            //        }
            //    }
            //}
            lblDate.Text ="Dated :"+ string.Format("{0:dd MMM yyyy}", DateTime.Now.AddHours(14));
        //    lblDate1.Text = string.Format("{0:dd MMM yyyy}", DateTime.Now.AddHours(14));
          
            //lblCompanyNumber.Text = stCust.CompanyNumber;
            //lblManualQuoteNumber.Text = stPro.ManualQuoteNumber;
            lblSalesRep.Text = stPro.SalesRepName;
            lblProjectNumber.Text = "Project Number: "+stPro.ProjectNumber;
            //lblProjectNumber1.Text = "Project Number: <br/>" + stPro.ProjectNumber;
            //lblEmpMobile.Text = stEmp.EmpMobile;
            //lblContPrimary.Text = stCont.ContFirst + " " + stCont.ContLast;
            //if (stCont.ContFirst2 != string.Empty)
            //{
            //    tdSC1.Visible = true;
            //    lblContSecondary.Visible = true;
            //    lblContSecondary.Text = stCont.ContFirst2 + " " + stCont.ContLast2;
            //}
            //else
            //{
            //    tdSC1.Visible = false;
            //    lblContSecondary.Visible = false;
            //}
            lblContFirst.Text = stCont.ContFirst;
            lblContLast.Text = stCont.ContLast;
            lblCustPhone.Text = stCust.CustPhone;
            lblContMobile.Text = stCont.ContMobile;
            lblContEmail.Text = stCont.ContEmail;
            //Response.Write(stPro.InstallAddress + "==" + stPro.InstallCity);
            //Response.End();

            if (stEmp.EmpMobile != string.Empty)
            {
                lblContMobile1.Text = "Mobile: " + stEmp.EmpMobile;
                lblContPhone.Text = "/ Phone: " + stEmp.EmpPhone;
            }
            if (stEmp.EmpMobile == string.Empty && stEmp.EmpPhone != string.Empty)
            {
                lblContPhone.Text = "Phone: " + stEmp.EmpPhone;
            }
            
            lblInstallAddress.Text = stPro.InstallAddress;
            lblInstallCity.Text = stPro.InstallCity;
            lblInstallState.Text = stPro.InstallState;
            lblInstallPostCode.Text = stPro.InstallPostCode;
            lblHouseType.Text = stPro.HouseType;
            lblRoofType.Text = stPro.RoofType;
            lblRoofAngle.Text = stPro.RoofAngle;

            if (stPro.MeterIncluded == "True")
            {
                lblMeterIncluded.Text = "Yes";
            }
            else
            {
                lblMeterIncluded.Text = "N/A";
            }

            if (stPro.Asbestoss == "True")
            {
                lblAsbestos.Text = "Yes";
            }
            else
            {
                lblAsbestos.Text = "N/A";
            }
            if (stPro.MeterEnoughSpace == "True")
            {
                lblMeterSpace.Text = "Yes";
            }
            else
            {
                lblMeterSpace.Text = "N/A";
            }
            if (stPro.MeterUG == "True")
            {
                lblMeterUG.Text = "Yes";
            }
            else
            {
                lblMeterUG.Text = "N/A";
            }
            if (stPro.OffPeak == "True")
            {
                lblOffPeak.Text = "Yes";
            }
            else
            {
                lblOffPeak.Text = "N/A";
            }
            //lblOffPeak.Text = stPro.MeterNumber2;

            if (stPro.CherryPicker == "True")
            {
                lblCherryPicker.Text = "Yes";
            }
            else
            {
                lblCherryPicker.Text = "N/A";
            }
            if (stPro.SplitSystem == "True")
            {
                //lblSplitSystem.Text = "Yes";
            }
            else
            {
                //lblSplitSystem.Text = "N/A";
            }

            lblNMINumber.Text = stPro.NMINumber;
            lblMeterPhase.Text = stPro.MeterPhase;

            lblTravelTime.Text = stPro.TravelTime;
            lblElecDistributor.Text = stPro.ElecDistributor;
            lblElecRetailer.Text = stPro.ElecRetailer;

            try
            {
                lblVariationOther.Text = stPro.VariationOther;// SiteConfiguration.ChangeCurrencyVal(stPro.VariationOther);
            }
            catch { }
            try
            {
                lblVarHouseType.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarHouseType);
            }
            catch { }
            try
            {
                lblVarRoofType.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarRoofType);
            }
            catch { }
            try
            {
                lblVarRoofAngle.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarRoofAngle);
            }
            catch { }
            try
            {
                lblVarMeterUG.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarMeterUG);
            }
            catch { }
            try
            {
                //lblVarAsbestos.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarAsbestos);
            }
            catch { }
            try
            {
                //lblVarSplitSystem.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarSplitSystem);
            }
            catch { }
            try
            {
                //lblVarCherryPicker.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarCherryPicker);
            }
            catch { }
            try
            {
                //lblVarTravelTime.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarTravelTime);
            }
            catch { }
            try
            {
                lblVarOther.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarOther);
            }
            catch { }
            try
            {
                lblVarMeterInstall.Text = SiteConfiguration.ChangeCurrencyVal(stPro.VarMeterInstallation);
            }
            catch { }
            try
            {
                lblSpecialDiscount.Text = SiteConfiguration.ChangeCurrencyVal(stPro.SpecialDiscount);
            }
            catch { }
            decimal rebate = 0;
            try
            {
                lblRECRebate.Text = SiteConfiguration.ChangeCurrencyVal(stPro.RECRebate);
                rebate = Convert.ToDecimal(stPro.RECRebate);
            }
            catch { }
            decimal basesystemcost = 0;
            try
            {
                basesystemcost = Convert.ToDecimal(stPro.ServiceValue);
            }
            catch { }

            try
            {
                lblTotalQuotePrice.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
                lblTotalQuotePrice1.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            }
            catch { }
            try
            {
                lblDepositRequired.Text = SiteConfiguration.ChangeCurrencyVal(stPro.DepositRequired);
            }
            catch { }
            decimal totalcost = basesystemcost + rebate;
            //decimal totalcost = basesystemcost;
            litServiceValue.Text = SiteConfiguration.ChangeCurrencyVal(totalcost.ToString());
            lblPanelConfigNW.Text = stPro.PanelConfigNW;
            lblPanelConfigOTH.Text = stPro.PanelConfigOTH;

            lblPanelDetails.Text = stPro.PanelDetails;
            //if (stPro.PanelBrandID != "")
            //{
            //    lblPanelDetails.Text = stPro.NumberPanels + "X" + stPro.PanelBrandName;
            //}
            //if (stPro.PanelBrandID != "" && stPro.InverterDetailsID != "")
            //{
            //    lblPanelDetails.Text = stPro.NumberPanels + "X" + stPro.PanelBrandName + " Panels with " + stPro.InverterDetailsName + " Inverter.";
            //}
            //if (stPro.PanelBrandID != "" && stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "")
            //{
            //    lblPanelDetails.Text = stPro.NumberPanels + "X" + stPro.PanelBrandName + " Panels with " + stPro.InverterDetailsName + " Inverter. Plus Second Inverter" + stPro.SecondInverterDetails;
            //}


            if (stPro.InverterDetailsID != "")
            {
                lblInverterDetails.Text = stPro.inverterqty +" X "+ stPro.InverterDetailsName + " KW Inverter";
                //lblInverterDetails.Text = "One " + stPro.InverterDetailsName + " KW Inverter";
            }
            if (stPro.SecondInverterDetailsID != "")
            {
                lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
                //lblInverterDetails.Text = "One " + stPro.SecondInverterDetails + " KW Inverter";
            }
            if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "")
            {
                lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus "+ stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
                //lblInverterDetails.Text = "One " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.SecondInverterDetails + " KW Inverter.";
            }
            //lblInverterDetails.Text = stPro.InverterDetails;

            //lblFooterCust1.Text = stCust.Customer;
            //lblFooterCust2.Text = stCust.Customer;
            //lblFooterCust3.Text = stCust.Customer;
            //lblFooterAdd1.Text = stCust.StreetCity + ", " + stCust.StreetState + ", " + stCust.StreetPostCode;
            //lblFooterAdd2.Text = stCust.StreetCity + ", " + stCust.StreetState + ", " + stCust.StreetPostCode;
            //lblFooterAdd3.Text = stCust.StreetCity + ", " + stCust.StreetState + ", " + stCust.StreetPostCode;
            //lblAddress.Text = stCust.PostalAddress;
            //lblcustname.Text = stCust.Customer;
            //lblStreetAddress.Text = stCust.StreetAddress;
            //lblStreetCity.Text = stCust.StreetCity;

            //lblStreetState.Text = stCust.StreetState;
            //lblStreetPostCoade.Text = stCust.StreetPostCode;
            
          
            if (stPro.ProjectNotes != string.Empty)
            {
                lblQuotationNotes.Visible = true;
                //tdQuotationNotes.Visible = true;
                lblQuotationNotes.Text = stPro.ProjectNotes.Replace("\n", "<br/>");
            }
            else
            {
                //tdQuotationNotes.Visible = false;
                lblQuotationNotes.Text = "";
                lblQuotationNotes.Visible = true;
            }
            if (stPro2.Promo3 == "True")
            {
                //tdPromoNotes.Visible = true;
                lblPromoNotes.Visible = true;
                lblPromoNotes.Text = stPro2.PromoText;
            }

            if (stPro2.Promo1ID != string.Empty)
            {
                //tdPromoNotes.Visible = true;
                lblPromoNotes.Visible = true;
                lblPromoNotes.Text = stPro2.PromoOffer1;
            }
            if (stPro2.Promo2ID != string.Empty)
            {
                //tdPromoNotes.Visible = true;
                lblPromoNotes.Visible = true;
                lblPromoNotes.Text = stPro2.PromoOffer2;
            }
            if (stPro2.Promo1ID != string.Empty && stPro2.Promo2ID != string.Empty)
            {
                //tdPromoNotes.Visible = true;
                lblPromoNotes.Visible = true;
                lblPromoNotes.Text = stPro2.PromoOffer1 + " + " + stPro2.PromoOffer2;
            }
            if (stPro2.PromoText != string.Empty)
            {
                lblPromoNotes.Visible = true;
                lblPromoNotes.Text = stPro2.PromoText;
            }
            if (stPro2.Promo1ID != string.Empty && stPro2.Promo2ID != string.Empty && stPro2.PromoText != string.Empty)
            {
                //tdPromoNotes.Visible = true;
                lblPromoNotes.Visible = true;
                lblPromoNotes.Text = stPro2.PromoOffer1 + " + " + stPro2.PromoOffer2 + " + " + stPro2.PromoText;
            }
            try
            {
                decimal balcost = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(stPro.DepositRequired);
                lblBalanceToPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));
            }
            catch { }

            if (stPro.FinanceWithID == "1" || stPro.FinanceWithID == string.Empty)
            {
                lblFinanceStatus.Text = "";
            }
            else
            {
                if (stPro.FinanceWithID != string.Empty)
                {
                    lblFinanceStatus.Text = "Payment option to be organized with " + stPro.FinanceWith;
                }
                if (stPro.FinanceWithID != string.Empty && stPro2.FinanceWithDepositID != string.Empty)
                {
                    lblFinanceStatus.Text += " with " + stPro2.FinanceWithDeposit + " % ";
                }
                if (stPro.FinanceWithID != string.Empty && stPro2.FinanceWithDepositID != string.Empty && stPro.PaymentTypeID != string.Empty)
                {
                    lblFinanceStatus.Text += " for " + stPro.PaymentType + ".";
                }
            }
            //lblSTCAddress.Text = stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + " - " + stPro.InstallPostCode;
            //lblSTCNo.Text = stPro.STCNumber;
        }
    }
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
}