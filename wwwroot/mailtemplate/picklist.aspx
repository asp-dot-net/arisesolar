﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="picklist.aspx.cs" Inherits="mailtemplate_picklist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }

        .pdfimages ul li {
            width: 318px;
            text-align: center;
            display: block;
            float: left;
            margin: 0px 0px 20px;
        }

        .pdfimages ul {
            width: 100%;
            float: left;
            margin: 0px;
            padding: 0px;
        }

        .terms tr td {
            font-size: 11px;
        }
    </style>
</head>
<body style="margin: 0px; padding: 0px;">
    <form id="form1" runat="server">
        <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;">
            <tr>
                <td style="padding: 20px 0px 10px 0px">
                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 0px 0px 20px 0px; margin: 0px; text-align: center;"><strong>STOCK ALLOCATION PICK-LIST</strong></p>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000;"><strong>Installation:&nbsp<asp:Label ID="lblProject" runat="server"></asp:Label></strong></td>
                            <td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000;"><strong>
                                <asp:Label ID="lblInstallBookingDate" runat="server"></asp:Label></strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border: #333333 solid 1px; padding: 10px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Installer Name:&nbsp<asp:Label ID="lblInstaller" runat="server"></asp:Label></strong></td>
                                        <td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Installer Mobile:&nbsp<asp:Label ID="lblInstallerMobile" runat="server"></asp:Label></strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding: 8px 0px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Customer Name:&nbsp<asp:Label ID="lblCustomer" runat="server"></asp:Label></strong></td>
                                        <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Phone:&nbsp<asp:Label ID="lblCustPhone" runat="server"></asp:Label></strong></td>
                                        <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Mob:&nbsp<asp:Label ID="lblCustMobile" runat="server"></asp:Label></strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>House Type:&nbsp<asp:Label ID="lblHouseType" runat="server"></asp:Label></strong></td>
                                        <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Roof Type:&nbsp<asp:Label ID="lblRoofType" runat="server"></asp:Label></strong></td>
                                        <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Angle:&nbsp<asp:Label ID="lblRoofAngle" runat="server"></asp:Label></strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding: 8px 0px 0px 0px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Manual Quote:&nbsp<asp:Label ID="lblManualQuoteNumber" runat="server"></asp:Label></strong></td>
                                        <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000000;"><strong>Project No.:&nbsp<asp:Label ID="lblProjectNumber1" runat="server"></asp:Label></strong></td>
                                        <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #000000;"><strong>*&nbsp<asp:Label ID="lblProjectNumber2" runat="server"></asp:Label>*</strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding: 8px 0px 0px 0px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Store:&nbsp<asp:Label ID="lblStockAllocationStore" runat="server"></asp:Label></strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" style="padding: 7px 0px 20px 0px;" height="534px">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="30" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Qty</strong></td>
                            <td width="130" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Item</strong></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Model</strong></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Description</strong></td>
                            <td width="50" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Done</strong></td>
                        </tr>
                        <tr id="trPanel" runat="server">
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">
                                <asp:Literal ID="lblNumberPanels" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">
                                <asp:Literal ID="lblPanelBrandName" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">
                                <asp:Literal ID="lblPanelModel" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">
                                <asp:Literal ID="lblPanDesc" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC" align="center"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <tr id="trInv1" runat="server">
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px;">1</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px;">
                                <asp:Literal ID="lblInvName1" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px;">
                                <asp:Literal ID="lblInvModel1" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px;">
                                <asp:Literal ID="lblInvDesc1" runat="server"></asp:Literal></td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px;"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <tr id="trInv2" runat="server">
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">1</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">
                                <asp:Literal ID="lblInvName2" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">
                                <asp:Literal ID="lblInvModel2" runat="server"></asp:Literal></td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC">
                                <asp:Literal ID="lblInvDesc2" runat="server"></asp:Literal></td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 5px; background: #ECECEC"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <%--<tr>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">32</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">Adjistable Rear Le</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">GS-AD-RL-15/30</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">&nbsp;</td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">2</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">BSG CB Enclosure4</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">32 A 1000V</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">&nbsp;</td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">2</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">BSG Circuit Breaker</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">25 A1000</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">&nbsp;</td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">1</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">Circuit Breaker AC</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">4HA 32A</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">&nbsp;</td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">5</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">Earthing Ling</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">Earthing Ling</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">&nbsp;</td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">32</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">End Clamp Kit 35</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">GS-EC-F35</td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">&nbsp;</td>
                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                        </tr>--%>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>

                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><b>_____</b></td>
                            <td colspan="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><b>KW Electrical Kit</b></td>

                        </tr>
                        <tr>
                            <b>
                                <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><b>_____</b></td>
                                <td colspan="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><b>Rail</b></td>
                            </b>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border-top: #000000 solid 3px;">
                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000000; padding: 10px 0px 10px 0px; margin: 0px;"><strong>Installer Notes:&nbsp;<asp:Label ID="lblInstallerNotes" runat="server"></asp:Label></strong></p>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="93"><strong>PICKED By:</strong></td>
                            <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                            <td width="20px">&nbsp;</td>
                            <td width="50px"><strong>Date:</strong></td>
                            <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" height="10px"></td>
                        </tr>
                        <tr>
                            <td><strong>Collected By:</strong></td>
                            <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                            <td width="20px">&nbsp;</td>
                            <td><strong>Date:</strong></td>
                            <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                        </tr>
                    </table>
                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 20px 0px 10px 0px; margin: 0px; text-align: center;"><strong>Printed:&nbsp;<asp:Label ID="lblPrinted" runat="server"></asp:Label></strong></p>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
            </tr>
              <tr>
                            <td colspan="12">
                                <div style='clear: both; page-break-before: always;'> </div>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;">
                                    <tr>
                                        <td style="padding: 20px 0px 10px 0px">
                                           
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr><td colspan="2"> <p style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 0px 0px 20px 0px; margin: 0px; text-align: center;"><strong>STOCK ALLOCATION RETURN FORM</strong></p></td></tr>
                                                <tr>
                                                    <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000;"><strong>Installation:&nbsp
                          <asp:Label ID="lblProject1" runat="server"></asp:Label>
                                                    </strong></td>
                                                    <td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000;"><strong>
                                                        <asp:Label ID="lblInstallBookingDatePL" runat="server"></asp:Label>
                                                    </strong></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: #333333 solid 1px; padding: 10px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Installer Name:&nbsp
                                <asp:Label ID="lblInstaller1" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Installer Mobile:&nbsp
                                <asp:Label ID="lblInstallerMobile1" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding: 8px 0px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Customer Name:&nbsp
                                <asp:Label ID="lblCustomer1" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Phone:&nbsp
                                <asp:Label ID="lblCustPhone1" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Mob:&nbsp
                                <asp:Label ID="lblCustMobile1" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>House Type:&nbsp
                                <asp:Label ID="lblHouseType1" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Roof Type:&nbsp
                                <asp:Label ID="lblRoofType1" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Angle:&nbsp
                                <asp:Label ID="lblRoofAngle1" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding: 8px 0px 0px 0px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Manual Quote:&nbsp
                                <asp:Label ID="lblManualQuoteNumber1" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000000;"><strong>Project No.:&nbsp
                                <asp:Label ID="lblProjectNumber1PL" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #000000;"><strong>*&nbsp
                                <asp:Label ID="lblProjectNumber2PL" runat="server"></asp:Label>
                                                                    *</strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding: 8px 0px 0px 0px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Store:&nbsp
                                <asp:Label ID="lblStockAllocationStore1" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="padding: 7px 0px 20px 0px;" height="534px">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="30" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Qty</strong></td>
                                                    <td width="130" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Item</strong></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Model</strong></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Description</strong></td>
                                                    <td width="50" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Return Item</strong></td>
                                                </tr>
                                                <tr id="trPanel1" runat="server">
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblPanelBrandName1" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblPanDesc1" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC" align="center"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                                                </tr>
                                                <tr id="trInv11" runat="server">
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">1</td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">
                                                        <asp:Literal ID="lblInvName11" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">
                                                        <asp:Literal ID="lblInvModel11" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">
                                                        <asp:Literal ID="lblInvDesc11" runat="server"></asp:Literal></td>
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                                                </tr>
                                                <tr id="trInv21" runat="server">
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">1</td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblInvName21" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblInvModel21" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblInvDesc21" runat="server"></asp:Literal></td>
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: #000000 solid 3px;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000000; padding: 10px 0px 10px 0px; margin: 0px;">
                                                <strong>Installer Notes:&nbsp;
                      <asp:Label ID="lblInstallerNotes1" runat="server"></asp:Label>
                                                </strong>
                                            </p>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="93"><strong>PICKED By:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                    <td width="20px">&nbsp;</td>
                                                    <td width="50px"><strong>Date:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="10px"></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Collected By:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                    <td width="20px">&nbsp;</td>
                                                    <td><strong>Date:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                </tr>
                                            </table>
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 20px 0px 10px 0px; margin: 0px; text-align: center;">
                                                <strong>Printed:&nbsp;
                      <asp:Label ID="lblPrinted1" runat="server"></asp:Label>
                                                </strong>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
        </table>
    </form>
</body>
</html>
