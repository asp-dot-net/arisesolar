﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="quotation.aspx.cs" Inherits="mailtemplate_quotation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Untitled Document</title>
    <style>
        .bg_left1 {
            background: url(images/bg1.png) repeat left top;
        }

        .hd_h1 {
            border-bottom: #17375E solid 3px;
            line-height: 34px;
            font-size: 35px;
            font-weight: 600;
            font-family: Calibri;
            padding: 0px 0px 10px 0px;
            margin: 0px 0px 40px 0px;
        }

        .tital_hd {
            font-size: 15px;
            font-weight: bold;
            padding-left: 7px;
            padding-right: 7px;
            border-bottom: #17375E solid 2px;
        }

        .table_brd {
        }

        .table1 tr td:first-child {
            background: #fff;
            width: 106px;
            color: #595959;
            font-size: 14px;
            padding-left: 10px;
        }

        .table1 tr:first-child td {
            padding-top: 20px;
        }

        .table1 tr:last-child td:first-child {
            background: none;
            padding: 0px;
        }

        .table1 tr td, .table2 tr td {
            padding: 0px 10px 20px 20px;
            vertical-align: top;
            font-size: 15px;
            line-height: 26px;
        }

        .table1 tr:last-child td, .table1 tr.last_td td, .table2 tr:last-child td {
            padding-bottom: 0px;
        }

        .fntsmall {
            font-size: 12px;
            text-align: right;
        }

        .step1 {
            position: relative;
            z-index: 1;
        }

        .step2 {
            position: relative;
            z-index: 2;
            margin-top: -202px;
        }

        .step3 {
            position: relative;
            z-index: 3;
        }

        .step4 {
            margin-top: -230px;
        }

        .height20 {
            height: 40px;
        }

        .hd_bg {
            background: url(images/bg1.png) repeat left top;
            display: block;
            -webkit-border-radius: 0px 0px 50px 0px;
            -moz-border-radius: 0px 0px 50px 0px;
            border-radius: 0px 0px 50px 0px;
            padding: 44px 50px 25px 20px;
        }

            .hd_bg .hd_h1 {
                margin-bottom: 0px;
            }

        .box2 {
            background: url(images/bg1.png) repeat left top;
            min-height: 200px;
            -webkit-border-radius: 0px 50px 0px 50px;
            -moz-border-radius: 0px 50px 0px 50px;
            border-radius: 0px 50px 0px 50px;
        }

        .paddbox2 {
            padding: 23px 20px 0px 20px;
        }

        .table2 tr td {
            padding-bottom: 0px;
        }

            .table2 tr td:first-child {
                padding-left: 0px;
                width: 107px;
            }

            .table2 tr td:last-child {
                font-weight: 400;
            }

        .italic_fnt tr td:last-child {
            font-style: italic !important;
        }

        .table3 tr td:first-child {
            text-align: left;
            width: 112px;
        }

        .table3 tr:first-child td {
            padding-top: 10px;
        }

        .table3 tr td {
            padding-bottom: 5px;
        }

        .box_cut {
            background: #fff;
            -webkit-border-radius: 0px 0px 0px 50px;
            -moz-border-radius: 0px 0px 0px 50px;
            border-radius: 0px 0px 0px 50px;
            height: 20px;
        }

        .dubl_line {
            line-height: 20px !important;
        }

        .margngtop10 {
            margin-top: 10px;
            text-align: center;
        }

        .text_normal {
            font-style: normal;
        }

        .box5 {
            background: url(images/bg1.png) repeat left top;
            -webkit-border-radius: 0px 0px 0px 50px;
            -moz-border-radius: 0px 0px 0px 50px;
            border-radius: 0px 0px 0px 50px;
        }

        .paddbox3 {
            padding: 20px 20px 40px 20px;
        }

        .fnt14 {
            font-size: 13px;
        }

        .color_59 {
            color: #595959;
        }

        .bgimg {
            background: url(images/img_footer.png) no-repeat right bottom;
            padding-bottom: 40px;
        }

        .box3 {
            background: url(images/bg1.png) repeat left top;
            -webkit-border-radius: 0px 40px 0px 40px;
            -moz-border-radius: 0px 40px 0px 40px;
            border-radius: 0px 40px 0px 40px;
            padding: 20px 7px;
            font-size: 12px;
        }

        .table_bb {
            margin: 0px 8px;
        }

            .table_bb tr td h3 {
                font-size: 18px;
                color: #0077FF;
                margin: 0px;
                padding: 0px;
            }

            .table_bb tr td {
                padding: 15px 0px;
                vertical-align: top;
            }

        .box4 {
            background: url(images/bg1.png) repeat left top;
            -webkit-border-radius: 40px 0px 40px 0px;
            -moz-border-radius: 40px 0px 40px 0px;
            border-radius: 40px 0px 40px 0px;
            padding: 20px 7px;
        }

        .tital_hd.text_right {
            text-align: right;
        }

        .box6 {
            background: url(images/bg1.png) repeat left top;
            -webkit-border-radius: 0px 0px 40px 0px;
            -moz-border-radius: 0px 0px 40px 0px;
            border-radius: 0px 0px 40px 0px;
            padding: 20px 10px;
            font-size: 11px;
        }

        h6 {
            font-size: 14px;
            color: #0077FF;
            font-weight: 400;
            margin: 0px;
            padding: 0px 0px 3px 0px;
        }

        .table_aa tr td {
            padding-bottom: 5px;
        }

        .table_aa tr:last-child td {
            padding-bottom: 0px;
        }

        .table_aa tr:first-child td {
            color: #59595D;
        }

        .marginbtm10 {
            margin-bottom: 7px !important;
        }

        .marginbtm5 {
            margin-bottom: 5px !important;
        }

        .paddnone_p {
            margin: 0px;
            padding: 0px;
        }

        .table_brd_btm {
            border-bottom: #17375E solid 2px;
            padding-bottom: 2px;
        }

        .brd_btm_1 {
            border-bottom: #17375E solid 1px;
        }

        .table_brd_btm tr td {
            padding: 2px 0px;
        }

        .fnt10 {
            margin: 0px;
            padding: 0px;
            font-size: 9px;
            line-height: 9px;
        }

        .box_li ol {
            margin: 0px;
            padding: 5px 0px 3px 17px;
        }

        .box_li li {
            list-style: lower-alpha;
            padding-left: 10px;
            padding-bottom: 3px;
        }

        .divpadd {
            padding: 0px 0px 0px 10px;
            font-size: 13px;
        }

        .paddleftli {
            padding-left: 25px;
        }

        .linka {
            text-decoration: none;
            color: #0077FF;
        }

        .paddbtm20 {
            padding-bottom: 20px;
        }

        .div_sing_area {
            padding: 30px 0px 20px 0px;
        }

        .colorfnt {
            color: #595959;
        }
    </style>
</head>

<body style="margin: 0px; padding: 0px;">
    <table width="935" align="center" cellpadding="0" cellspacing="0" border="0" style="background: url(images/bg_main1.jpg) no-repeat left bottom #4A6AC1; font-family: Calibri;">
        <tr>
            <td>
                <div class="step1">
                    <table width="782" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="422" class="bg_left1" valign="top">
                                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="padding: 44px 18px 90px 18px;">
                                                        <h1 class="hd_h1">Your Arise <br> Solar System<br>
                                                           </h1>
                                                        <div class="tital_hd">PROJECT DETAILS</div>
                                                        <br />
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd table1">
                                                            <tr>
                                                                <td align="right">PROJECT NUMBER</td>
                                                                <td>
                                                                    <asp:Label ID="lblprojectno" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">CONTACT NAME</td>
                                                                <td>
                                                                    <asp:Label ID="lblcontactname" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">PHONE NUMBER</td>
                                                                <td>
                                                                    <asp:Label ID="lblphonenumber" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">EMAIL ADDRESS</td>
                                                                <td>
                                                                    <asp:Label ID="lblemail" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                          
                                                            <tr class="last_td">
                                                                <td align="right">POSTAL ADDRESS</td>
                                                                <td>
                                                                    <asp:Label ID="lblstreetname" runat="server" Text=""></asp:Label><br>
                                                                    <asp:Label ID="lblstreetcity" runat="server" Text=""></asp:Label>, 
                                                                    <asp:Label ID="lblstate" runat="server" Text=""></asp:Label>, 
                                                                    <asp:Label ID="lblpostcode" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <img src="images/bg_img.png" /></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                        <div style="height: 40px;"></div>
                                                        <div class="tital_hd">PROJECT OVERVIEW</div>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd table1">
                                                            <tr>
                                                                <td align="right">SITE ADDRESS</td>
                                                                <td>
                                                                    <asp:Label ID="lblsite_streetname" runat="server" Text=""></asp:Label><br>
                                                                    <asp:Label ID="lblsite_streetcity" runat="server" Text=""></asp:Label>,
                                                                    <asp:Label ID="lblsite_state" runat="server" Text=""></asp:Label>,
                                                                   <asp:Label ID="lblsite_postcode" runat="server" Text=""></asp:Label>,
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">SYSTEM</td>
                                                                <td><strong><asp:Label ID="lblSystemCapKW1" runat="server"></asp:Label> KW</strong><span style="display: block; padding: 0px 0px 0px 10px">
                                                                    Panels:
                                                                    <asp:Label ID="lblPanelDetails" runat="server"></asp:Label><br>
                                                                    Inverter:
                                                                    <asp:Label ID="lblInverterDetails" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                          <br />
                                                            <br />

                                                             <tr>
                                                                <td align="right"></td>
                                                                <td><br>
                                                                    <br>
                                                                    <br />
                                                                    <br />

                                                                    </td>
                                                            </tr>
                                                            <tr class="last_td">
                                                                <td align="right"></td>
                                                                <td><br>
                                                                    <br>
                                                                    <br />
                                                                    <br />
                                                                    <br />
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <img src="images/bg_img.png" /></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                        <p class="fntsmall">
                                                            *Production figures based upon CEC provided yearly production<br>
                                                            average for the area. Savings estimates based upon<br>
                                                            assumed solar energy self-consumption<br>
                                                            to feed-in ratio compared against<br>
                                                            standard<br>
                                                            residential tariffs.
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" style="padding-top: 32px;">
                                            <table width="100%" align="right" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <table width="330" align="right" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td align="right" style="font-size: 18px; color: #fff; line-height: 22px; letter-spacing: 0px;"><%--<strong>www.arisesolar.com.au<br>
                                                                                <span style="font-size: 26px; letter-spacing: 2px;">1300 387 676</span></strong>--%></td>
                                                                            <td width="30"></td>
                                                                            <td width="300">
                                                                                <img src="<%=Request.PhysicalApplicationPath+"/images/logoarise.png" %>" alt="" style="max-width: 100%" /></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="padding-top: 30px;">
                                                                   <%-- <img src="images/img_1.png" alt="" width="304" />--%>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="step2">
                    <div>
                        <img src="images/bg_img1.png" alt="" width="935" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class="height20"></div>
    <table width="935" align="center" cellpadding="0" cellspacing="0" border="0" style="background: url(images/bg_main1a.jpg) no-repeat left bottom #4A6AC1; font-family: Calibri;">
        <tr>
            <td>
                <div class="step3">
                    <table width="798" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="317" valign="top">
                                            <div class="hd_bg">
                                                <h1 class="hd_h1">Solar<br>
                                                    Quotation</h1>
                                            </div>
                                            <br />
                                        </td>
                                        <td valign="top" style="padding-top: 32px;">
                                            <table width="100%" align="right" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <table width="328" align="right" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td align="right" style="font-size: 18px; color: #fff; line-height: 22px; letter-spacing: 0px;"><%--<strong>www.arisesolar.com.au<br>
                                                                    <span style="font-size: 26px; letter-spacing: 2px;">1300 387 676</span></strong>--%></td>
                                                                <td width="30"></td>
                                                                <td width="170">
                                                                    <img src="images/logoarise.png" alt="" style="max-width: 100%" /></td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="15"></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="box2 paddbox2">
                                    <div class="tital_hd">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table2 italic_fnt">
                                            <tr>
                                                <td>SITE DETAILS</td>
                                                <td>
                                                    <asp:Label ID="lblInstallAddress" runat="server"></asp:Label>,<br>
                                                    <asp:Label ID="lblInstallCity" runat="server"></asp:Label>,
                            <asp:Label ID="lblInstallState" runat="server"></asp:Label>
                                                    –
                            <asp:Label ID="lblInstallPostCode" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="453" valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd table1 table3 italic_fnt">
                                                    <tr>
                                                        <td align="right">ELEC. RETAILER</td>
                                                        <td><asp:Label ID="lblElecRetailer" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">ELEC. DISTRIBUTOR</td>
                                                        <td><asp:Label ID="lblElecDistributor" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr class="last_td">
                                                        <td align="right" class="dubl_line">COMPLIANCE & SAFETY</td>
                                                        <td>Meter Phase : <asp:Label ID="lblmeter" runat="server"></asp:Label><br />
                                                            Switchboard Upgrades : <asp:Label ID="lblswitchupgrade" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr class="last_td">
                                                        <td align="right" height="17"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="box_cut"></div>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd table1 table3 italic_fnt">
                                                    <tr>
                                                        <td align="right">PHASES</td>
                                                        <td><asp:Label ID="lblMeterPhase" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">ROOF TYPE</td>
                                                        <td><asp:Label ID="lblVarRoofType" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">ROOF PITCH</td>
                                                        <td><asp:Label ID="lblVarRoofAngle" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr class="last_td">
                                                        <td align="right">HOUSE TYPE</td>
                                                        <td><asp:Label ID="lblVarHouseType" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="box_cut"></div>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                    </table>
                                    <div class="tital_hd">SYSTEM & INSTALLATION DETAILS</div>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="bgimg">
                                        <tr>
                                            <td width="453" valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd table1 table3 italic_fnt">
                                                    <tr>
                                                        <td align="right">SIZE</td>
                                                        <td><strong><asp:Label ID="lblSystemCapKW" runat="server"></asp:Label> KW</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">PANELS<div class="margngtop10">
                                                            <img src="images/logo_1.png" alt="" runat="server" visible="false">
                                                        </div>
                                                        </td>
                                                        <td><strong><asp:Label ID="lblPanelDetails1" runat="server"></asp:Label></strong><br>
                                                            <div class="text_normal">
                                                                - High Efficiency Solar PV Panels<br>
                                                                - Weather Resistant Design
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr class="last_td">
                                                        <td align="right" height="13"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">INVERTER<div class="margngtop10">
                                                            <img src="images/logo_2.png" alt="" runat="server" visible="false">
                                                        </div>
                                                        </td>
                                                        <td><strong><asp:Label ID="lblInverterDetails1" runat="server"></asp:Label></strong><br>
                                                            <div class="text_normal">
                                                                - Dual MPPT Solar Inverter<br>
                                                                - Weather Resistant, IP65 Rater Design
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr class="last_td">
                                                        <td align="right" height="13"></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">INSTALLATION<div class="margngtop10">
                                                           <img src="images/logoarise.png" alt="" style="max-width: 80%" />
                                                        </div>
                                                        </td>
                                                        <td><strong class="text_normal"><i>…Through</i> Arise Solar</strong><br>
                                                            <div class="text_normal">
                                                                - High Efficiency Solar PV Panels<br>
                                                                - Weather Resistant Design
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="box_cut"></div>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <div class="box5 paddbox3">
                                                    <table>
                                                        <tr>
                                                            <td class="color_59">EXTRAS, UPGRADES, & OTHER</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="fnt14">
                                                                <div>
                                                                    - Tilt Frames for Optimum Pitch & Performance<br>
                                                                    - Free External Wi-Fi Plug-in
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="height: 30px;"></div>
                                                <div class="tital_hd">PROJECT NOTES</div>
                                                <div class="box5 paddbox3">
                                                    <table>
                                                        <tr>
                                                            <td class="fnt14">
                                                                <div>- House under construction, installation in 2 – 3 months</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="100"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <br />
                                <div style="height: 30px;"></div>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="280" valign="top" style="padding-left: 55px;">
                                            <div class="box3">
                                                <div class="tital_hd">PAYMENT DETAILS</div>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_bb">
                                                    <tr>
                                                        <td width="43" align="left">
                                                            <img src="images/img_1.jpg" alt=""></td>
                                                        <td>
                                                            <h3>25 Years</h3>
                                                            Panel Performance Warranty</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <img src="images/img_2.jpg" alt=""></td>
                                                        <td>
                                                            <h3>10 Years</h3>
                                                            Panel Manufacturer’s Warranty</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <img src="images/img_3.png" alt=""></td>
                                                        <td>
                                                            <h3>10 Years</h3>
                                                            Installation Workmanship Warranty</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <img src="images/img_4.png" alt=""></td>
                                                        <td>
                                                            <h3>5 Years</h3>
                                                            Inverter Manufacturer’s Warranty</td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                        <td valign="top" style="padding-left: 15px;">
                                            <div class="box4">
                                                <div class="tital_hd text_right">PAYMENT DETAILS</div>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td width="137" valign="top">
                                                            <div class="box6">
                                                                <h6>BANK TRANSFER</h6>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_aa marginbtm10">
                                                                    <tr>
                                                                        <td width="65">BANK</td>
                                                                        <td>Westpac</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>BSB</td>
                                                                        <td>034-001</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>ACC. NO.</td>
                                                                        <td>504046</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>ACC. NAME</td>
                                                                        <td>Arise Solar</td>
                                                                    </tr>
                                                                </table>
                                                                <h6>CREDIT CARD</h6>
                                                                <p class="paddnone_p marginbtm5">Balance payments via Credit Card will incur a 1% surcharge.</p>
                                                                <h6>CHEQUE</h6>
                                                                <p class="paddnone_p marginbtm5">Made payable to Arise Solar & mailed to:</p>
                                                                <p class="paddnone_p"><i>Level 6/140 Creek Street, Brisbane QLD 4000</i></p>
                                                            </div>
                                                        </td>
                                                        <td valign="top" class="fnt14">
                                                            <table width="95%" cellpadding="0" cellspacing="0" border="0" align="right">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd_btm">
                                                                            <tr>
                                                                                <td width="160">System & Installation</td>
                                                                                <td align="right" style="padding-right:40px">&nbsp;&nbsp;&nbsp; <asp:Label runat="server" ID="lblbasiccost"></asp:Label> </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Site Requirements</td>
                                                                                <td align="right" style="padding-right:40px">+  N/A </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Compliance & Safety</td>
                                                                                <td align="right" style="padding-right:40px">+  N/A </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Extras, Upgrades, & Other</td>
                                                                                <td align="right" style="padding-right:40px"><asp:Label runat="server" ID="lbltax"></asp:Label> </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd_btm brd_btm_1">
                                                                            <tr>
                                                                                <td width="160"><strong>Full Retail Price</strong></td>
                                                                                <td align="right" style="padding-right:40px"><asp:Label runat="server" ID="lblfullretail"></asp:Label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Less Discounts2</td>
                                                                                <td align="right" style="padding-right:40px">- <asp:Label ID="lbllessdis2" runat="server"></asp:Label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Less STC Incentive3</td>
                                                                                <td align="right" style="padding-right:40px">- <asp:Label ID="lbllessstc" runat="server"></asp:Label></td>
                                                                            </tr>

                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table_brd_btm">
                                                                            <tr>
                                                                                <td width="160"><strong>Total Cost <i>inc. GST</i></strong></td>
                                                                                <td align="right" style="color: #0077FF; padding-right:40px;">{ $<asp:Label runat="server" ID="txttotalcost"></asp:Label> }</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Deposit Amount</td>
                                                                                <td align="right" style="padding-right:40px"><asp:Label ID="lbldepositeamount" runat="server"></asp:Label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Total less Deposit</td>
                                                                                <td align="right" style="padding-right:40px"><asp:Label ID="lbltotallessdepo" runat="server"></asp:Label></td>
                                                                            </tr>

                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" style="padding-top: 15px;">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <div style="color: #595959; font-size: 17px;">ALSO AVAILABLE…</div>
                                                                                    <p class="fnt10">
                                                                                        * Subject to approval, fee & charges<br>
                                                                                        &nbsp;may apply. Inquire for information.
                                                                                    </p>
                                                                                </td>
                                                                                <td valign="top" width="130">
                                                                                    <img src="images/logo_small.png" alt="" style="max-width: 100%;"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="30"></td>
                        </tr>
                    </table>
                </div>
                <br />
            </td>
        </tr>
    </table>
    <div class="height20"></div>
    <table width="935" align="center" cellpadding="0" cellspacing="0" border="0" style="background: url(images/bg_main1a.jpg) no-repeat left bottom #4A6AC1; font-family: Calibri;">
        <tr>
            <td>
                <div class="step3">
                    <table width="798" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="317" valign="top">
                                            <div class="hd_bg">
                                                <h1 class="hd_h1">Terms &<br>
                                                    Conditions</h1>
                                            </div>
                                        </td>
                                        <td valign="top" style="padding-top: 32px;">
                                            <table width="100%" align="right" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <table width="328" align="right" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td align="right" style="font-size: 18px; color: #fff; line-height: 22px; letter-spacing: 0px;"><%--<strong>www.arisesolar.com.au<br>
                                                                    <span style="font-size: 26px; letter-spacing: 2px;">1300 387 676</span></strong>--%></td>
                                                                <td width="30"></td>
                                                                <td width="170">
                                                                    <img src="images/logoarise.png" alt="" style="max-width: 100%" /></td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="15"></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="box2 paddbox2 box_li paddbtm30">
                                    <h6>1. CUSTOMER DECLARATION</h6>
                                    <div class="divpadd">
                                        <div class="paddleftli">By signing this agreement you acknowledge that:</div>
                                        <ol>
                                            <li>You are over the legal age of 18 years.</li>
                                            <li>You are one of the registered owners of the property at the Installation Address & your name is on the Title Deed of this property.</li>
                                            <li>You have never before received or been approved for any Rebate, Financial Assistance, Solar Credit, or Small-Scale Technology Certificate (STC’s) for a Solar PV Micro Embedded Generator (MEG) System at the Installation Address.</li>
                                            <li>Where applicable, you authorised ARISE SOLAR to conduct such credit or financial background checks on yourself, or any guarantors, as deemed necessary by ARISE SOLAR. You acknowledge that ARISE SOLAR may disclose information pertaining to such checks to the relevant Credit Reporting or Recovery body, in accordance with the Privacy Act 1988 (CTH).</li>
                                        </ol>
                                    </div>
                                    <h6>2. JURISDICTION</h6>
                                    <div class="divpadd">
                                        <ol>
                                            <li>This agreement is deemed to have been made in Queensland & shall be interpreted in accordance with the relevant legislation. All parties submit to the exclusive jurisdiction of the Courts of Queensland.</li>
                                        </ol>
                                    </div>
                                    <h6>3. PAYMENT & STC’S</h6>
                                    <div class="divpadd">
                                        <ol>
                                            <li>ARISE SOLAR must receive full payment prior to or on the day of the installation via Credit Card (CC), Cheque (CHQ), or Electronic Funds Transfer (EFT). ARISE SOLAR withholds the right to postpone or cancel your installation if payment is not received as stipulated below. For CC payments, card details must be provided to ARISE SOLAR directly (a 1% surcharge applies to Balance Payments via CC). CHQ payments must be payable to Arise Solar & posted to PO BOX 570, Archerfield QLD 4108, or provided to our installer on the day of installation. EFT payments must be completed 48 hours prior to the date of installation unless otherwise stated & you must provide us with written proof of the transfer.</li>
                                            <li>You agree to pay ARISE SOLAR the STC's for the installation as part-payment for your system. The STC's will be paid directly to ARISE SOLAR or an authorised ARISE SOLAR Agent. If the Office of Clean Energy Regulator determines you are not eligible to receive STC's, & therefore ARISE SOLAR is unable to receive the STC's, you will be liable to pay ARISE SOLAR the value of the STC's as determined by market rates (see: <a href="http://www.cleanenergyregulator.gov.au" target="_blank" class="linka">http://www.cleanenergyregulator.gov.au</a>).</li>
                                            <li>If you are not eligible for STC's, or if you wish to claim the STC's yourself, you must notify Arise Solar in writing & the complete payment for the system (including trading costs of the STC’s) is due prior to installation.</li>
                                            <li>You acknowledge that if you breach any conditions of the STC Regulations, you may be financially liable to the Office of Clean Energy Regulator. If you commit any breach of the STC Regulations, you acknowledge that ARISE SOLAR will not be liable to you or the Office of Clean Energy Regulator on your behalf.</li>
                                            <li>ARISE SOLAR, or an authorised ARISE SOLAR Agent, will arrange for the complete documentation & processing of the sale of the STC's. You acknowledge that the price of STC’s is governed by market movements & the Renewable Energy Certificates (REC’s) guidelines. If the market price of STC’s falls below a certain point, as determined by ARISE SOLAR at its absolute discretion, ARISE SOLAR may decide to delay the installation until the price rises, or cancel your installation & refund your complete deposit.</li>
                                            <li>Title & ownership of the solar system will transfer to you only upon installation & ARISE SOLAR receiving full payment from you for the solar system.</li>
                                            <li>If you fail to pay any amount that is due & payable under this agreement, ARISE SOLAR will be entitled to charge interest on the unpaid amount at the current Reserve Bank target cash rate plus 2%. You will also have to pay ARISE SOLAR any costs associated with the recovery of such unpaid amounts.</li>
                                            <li>Failure to pay the complete amount may result in ARISE SOLAR taking legal action against you & will void all warranties offered in conjunction with this system & installation.</li>
                                        </ol>
                                    </div>
                                    <h6>4. AUTHORITY OF ACCESS TO PROPERTY</h6>
                                    <div class="divpadd">
                                        <ol>
                                            <li>You authorise ARISE SOLAR & its Contractors, Employees, & Installers full access to the property at all reasonable times to carry out work associated with the installation of your solar system, including Site Inspections, the Signing of Paperwork, the Delivery & Installation of the Solar PV System, & Grid Connection.</li>
                                            <li>Your co-operation is required to enable such works to occur at the earliest possible time which is convenient to ARISE SOLAR & the customer.</li>
                                            <li>In event that you, the customer, breach this agreement, including but not limited to failure to pay the complete amount owing under this agreement, you authorise ARISE SOLAR or any person/s authorised by ARISE SOLAR in writing, upon giving reasonable notice, to enter the property at the installation site, or location where the goods are reasonably believed to be held, for the purpose of examining or recovering the goods. You also agree to indemnify ARISE SOLAR for the costs of removal, enforcement, & legal action.</li>
                                        </ol>
                                    </div>
                                    <h6>5. LIABILITIES & RISK</h6>
                                    <div class="divpadd">
                                        <div class="paddleftli">By signing this agreement you acknowledge that:</div>
                                        <ol>
                                            <li>ARISE SOLAR accepts no responsibility for insurance or insurance risk of the solar system upon installation. It is your responsibility to ensure that your property insurance adequately covers your Solar PV System.</li>
                                            <li>You acknowledge ARISE SOLAR accepts no liability or responsibility for your Feed-in-Tariff (FiT), as administered by the relevant Government body.</li>
                                            <li>ARISE SOLAR accepts no responsibility for any damage or loss caused to your property by the installer which has not been caused by the installer's negligence. All ARISE SOLAR installers are contractors who are required by ARISE SOLAR & the relevant legislations to have appropriate third party insurance including damage insurance. ARISE SOLAR will work with you & the installer to rectify any damage caused to your property as a result of installer negligence.</li>
                                            <li>You acknowledge that ARISE SOLAR will not be responsible for any damage caused to old & brittle roofing tiles that may be cracked or damaged during installation.</li>
                                            <li>Unless otherwise stated, ARISE SOLAR accepts no responsibility or liability for additional costs or delays in relation to your need to obtain an upgrade of your meter box, sub-board, or the installation of a new meter; the removal & handling of asbestos at your property in relation to the installation of the Solar PV System; or the installation of timers, contactors, switches, or other wiring & work in relation to Electric Hot Water, Slab Heating, Climate Saver, or Off-Peak meters.</li>
                                        </ol>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="30"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <div class="height20"></div>
    <table width="935" align="center" cellpadding="0" cellspacing="0" border="0" style="background: url(images/bg_main1a.jpg) no-repeat left bottom #4A6AC1; font-family: Calibri;">
        <tr>
            <td>
                <div class="step3">
                    <table width="798" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="317" valign="top">
                                            <div class="hd_bg">
                                                <h1 class="hd_h1">Terms &<br>
                                                    Conditions</h1>
                                            </div>
                                        </td>
                                        <td valign="top" style="padding-top: 32px;">
                                            <table width="100%" align="right" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <table width="328" align="right" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td align="right" style="font-size: 18px; color: #fff; line-height: 22px; letter-spacing: 0px;"><%--<strong>www.arisesolar.com.au<br>
                                                                    <span style="font-size: 26px; letter-spacing: 2px;">1300 387 676</span></strong>--%></td>
                                                                <td width="30"></td>
                                                                <td width="170">
                                                                    <img src="images/logoarise.png" alt="" style="max-width: 100%" /></td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="15"></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="box2 paddbox2 box_li">
                                    <div class="divpadd">f.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ARISE SOLAR accepts no responsibility or liability for delays in installation if any information provided by the customer is incorrect.</div>
                                    <h6>6. METERS</h6>
                                    <div class="divpadd">
                                        <ol>
                                            <li>In the event that the meter box is located remotely from the inverter installation location, & such costs are not provided for in the relevant ARISE SOLAR quotation, the customer will be required to arrange & pay for the additional work.</li>
                                            <li>ARISE SOLAR’s quote does not include additional costs in relation to reconfiguration of your meter as part of the Grid Connection Process which comes from your Power Distributor or Electricity Retailer.</li>
                                        </ol>
                                    </div>
                                    <h6>7. DELIVERY & INSTALLATION</h6>
                                    <div class="divpadd">
                                        <ol>
                                            <li>ARISE SOLAR will make every reasonable effort to install your system in a timely manner; however ARISE SOLAR will not be bound to meet estimated or proposed delivery, installation, or system completion dates resulting from issues which ARISE SOLAR has no control over including but not limited to, limitations on worldwide material availability, peaks in demand created by changes in Government legislation, inclement weather, & other forms of force majeure. Delays in Installation or Grid Connection are not valid reasons for claiming a refund or compensation from ARISE SOLAR.</li>
                                            <li>Unless otherwise stated additional costs may be applicable for extra work required on the day of installation such as splitting of the system into more than one bank, horizontal fixing of panels, & extra charges for meter box upgrades where required.</li>
                                            <li>Delays in installation are not grounds for cancellation & ARISE SOLAR is not liable to you for any perceived loss as a result of these delays.</li>
                                            <li>If difficulties with site access are encountered which ARISE SOLAR was not made aware of prior to the day of installation, the additional costs incurred may be payable by the customer.</li>
                                            <li>Before the installation can commence, & modifications to your roof take place, you are required to arrange payment with the scheduling department for the balance payment of the system. All goods & products remain the property of ARISE SOLAR until your payment or, if applicable, finance payment is received.</li>
                                            <li>A Home Owner or otherwise Authorised Person must be present during installation to sign the mandatory declaration assigning the STC’s to ARISE SOLAR, as per the Renewable Energy Act (2000). Should the installer arrive on the agreed date & a Home Owner or otherwise Authorised Person is not present, a rescheduling fee of $200.0 will apply & the installation will be delayed.</li>
                                            <li>Upon signing the agreement, any requests for modifications to the material/equipment may incur an administration charge of $200.0. A new quote will be generated for any changes required.</li>
                                        </ol>
                                    </div>
                                    <h6>8. PRIVACY POLICY</h6>
                                    <div class="divpadd">
                                        <ol>
                                            <li>You agree to provide ARISE SOLAR with whatever personal information is required for the efficient functioning of ARISE SOLAR on your behalf, in particular with relation to the processing of STC’s & your Grid Connection.</li>
                                            <li>ARISE SOLAR will only provide your information to its contractors, employees, & installers as is required to effectively perform the required work.</li>
                                            <li>ARISE SOLAR will only provide your information, on your behalf, to the relevant bodies (for the processing the STC's), & the Electricity Distributor & Retailer (for connecting your Solar PV System to the grid).</li>
                                            <li>Unless otherwise stated & agreed upon, ARISE SOLAR will not provide your personal information to any third parties other than those mentioned above.</li>
                                            <li>On the day of installation you must sign all necessary documents as is required for the fulfilment of all parties’ obligations under this agreement.</li>
                                            <li>ARISE SOLAR will not sell your personal information under any circumstances.</li>
                                        </ol>
                                    </div>
                                    <h6>9. PRODUCT WARRANTIES</h6>
                                    <div class="divpadd">
                                        <ol>
                                            <li>The Solar PV System comes with up to 10 Years Installation Warranty from ARISE SOLAR in conjunction with the Manufacturer's Warranty. A Warranty Booklet will be presented to you at installation which also outlines that the Solar PV System requires servicing every two years by a licenced Solar Installer. ARISE SOLAR does not provide warranty on monitoring systems which may come with the inverter.</li>
                                            <li>Finance</li>
                                            <li>Financing is not available on advertised or promotional specials. Terms & Conditions for Repayment or Finance Options, including charges or interest, will be provided in full by the relevant company or entity prior to the customer entering into any such agreement.
                                                <li>Termination</li>
                                                <li>ARISE SOLAR may choose to terminate this agreement & your installation if you do not abide by these Terms & Conditions or if there are delays in the installation process or supplier prices increase & ARISE SOLAR is unable to proceed with the installation as per this agreement; in which case the full deposit will be refunded via CHQ or EFT (unless otherwise stated).</li>
                                                <li>You understand that under Australian Consumer Law & the relevant legislation you are entitled to a cooling-off period of ten (10) working days, where applicable. A Cancellation Request must be received by ARISE SOLAR in writing via email, fax, or post within this period.</li>
                                                <li>If a Cancellation Request is received in this time, you may be eligible for a refund of your deposit less any administration charges or costs incurred during that time such as applications or work requests. Termination after the ten (10) days may result in the loss of the full deposit paid. Furthermore, as we normally procure the required materials within two weeks, or fourteen (14) days, of receiving a signed copy of this agreement, the customer may be required to pay material costs if the job is cancelled after this period.</li>
                                        </ol>
                                    </div>
                                    <div class="div_sing_area">
                                        <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td valign="top">
                                                    <div style="padding-left: 15px;">
                                                        <div class="hd_titla"><strong>ACCEPTANCE</strong></div>
                                                        <p class="p_padd">By signing I below, I hereby accept this Quotation & Contract. I also acknowledge that I am aware of, & agree to, the Terms & Conditions stated above & overleaf.</p>
                                                    </div>
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="table_boader" style="margin-left: 5px;">
                                                        <tr>
                                                            <td style="padding-top: 19px; padding-left: 3px;"><span class="colorfnt">SIGNATURE</span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" width="46%" style="padding-left: 20px; padding-top: 22px;">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" class="table_boader">
                                                        <tr>
                                                            <td><span class="colorfnt">NAME : <asp:Label runat="server" ID="lblcustnmae"></asp:Label></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-top: 30px;"><span class="colorfnt">DATE : <asp:Label runat="server" ID="lblgetdate"></asp:Label></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>



                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="30"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
<style>
    .table_boader tr td {
        border-bottom: #7F7F7F solid 2px;
        font-size: 13px;
    }

    .hd_titla {
        font-size: 15px;
        color: #17365D;
    }

    .p_padd {
        padding: 0px;
        margin: 0px;
        font-size: 12px;
    }
</style>
