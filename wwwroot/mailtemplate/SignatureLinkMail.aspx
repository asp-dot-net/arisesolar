﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignatureLinkMail.aspx.cs" Inherits="mailtemplate_SignatureLinkMail" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />   
</head>
<body>
    <div class="brdemail">
        <table cellpadding="0" cellspacing="0" width="600px" style="border: 1px solid #293245; margin: 0px; auto; display: block; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
            <tr>
                <td style="padding:10px; border-bottom:1px solid #ccc;">
                </td>
            </tr>
            <tr>
                <td>
                    <table id="mytable" border="0" cellpadding="5" align="center" cellspacing="0" style="border-collapse: collapse; width: 600px;">
                        <tr>
                            <td colspan="2" style="background-color: #293245; color: #ffffff; padding: 5px 15px;">
                                <b>Details as follow :</b>
                            </td>
                        </tr>
                        <tr class="" style="padding: 5px 10px; display: block;">
                            <td colspan="2">
                                Hello <b><asp:label runat="server" id="lblCustName"></asp:label></b>,<br />
                                <br />
                                Your solar company has requested your signature for the recent solar installation that you have completed.
                                Please visit the link to sign or contact your solar company to verify for DocNo: <asp:label runat="server" id="lblQDocNo"/>

                                <a id="lblLink" runat="server" />
                                <%--<asp:label runat="server" id="lblLink"></asp:label>--%>


                            </td>
                        </tr>
                        <tr class="" style="padding: 5px 10px; display: block;">
                            <td colspan="2">
                                Thanks and Regards,
                                <br /><asp:label runat="server" id="lblSiteName"></asp:label>
                            </td>
                       
                        </tr>
                         
            <tr runat="server">
                <td style="background: #293245; color: #ffffff; text-align: center; vertical-align: middle; height: 55px; font-family: Verdana, Geneva, sans-serif; font-size: 12px;">Copyrights &copy; <asp:label runat="server" id="lblyear"></asp:label> <asp:label runat="server" id="lblSiteName2"></asp:label>. All rights reserved.
                </td>
            </tr>
                        
        </table>
                    </td>
                </tr>
            </table>
    </div>
</body>
</html>
