﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="welcomeletter.aspx.cs" Inherits="mailtemplate_welcomeletter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }

        @font-face {
            font-family: 'amaticbold';
            src: url('../fonts/amatic-bold.eot');
            src: url('../fonts/amatic-bold.eot?#iefix') format('embedded-opentype'), url('../fonts/amatic-bold.woff2') format('woff2'), url('../fonts/amatic-bold.woff') format('woff'), url('../fonts/amatic-bold.ttf') format('truetype'), url('../fonts/amatic-bold.svg#amaticbold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'bobregular';
            src: url('../fonts/bob.eot');
            src: url('../fonts/bob.eot?#iefix') format('embedded-opentype'), url('../fonts/bob.woff2') format('woff2'), url('../fonts/bob.woff') format('woff'), url('../fonts/bob.ttf') format('truetype'), url('../fonts/bob.svg#bobregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'mission_scriptregular';
            src: url('../fonts/mission-script.eot');
            src: url('../fonts/mission-script.eot?#iefix') format('embedded-opentype'), url('../fonts/mission-script.woff2') format('woff2'), url('../fonts/mission-script.woff') format('woff'), url('../fonts/mission-script.ttf') format('truetype'), url('../fonts/mission-script.svg#mission_scriptregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'museo300';
            src: url('../fonts/museo300-regular.eot');
            src: url('../fonts/museo300-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo300-regular.woff2') format('woff2'), url('../fonts/museo300-regular.woff') format('woff'), url('../fonts/museo300-regular.ttf') format('truetype'), url('../fonts/museo300-regular.svg#museo300') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'museo500';
            src: url('../fonts/museo500-regular.eot');
            src: url('../fonts/museo500-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo500-regular.woff2') format('woff2'), url('../fonts/museo500-regular.woff') format('woff'), url('../fonts/museo500-regular.ttf') format('truetype'), url('../fonts/museo500-regular.svg#museo500') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'museo700';
            src: url('../fonts/museo700-regular.eot');
            src: url('../fonts/museo700-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo700-regular.woff2') format('woff2'), url('../fonts/museo700-regular.woff') format('woff'), url('../fonts/museo700-regular.ttf') format('truetype'), url('../fonts/museo700-regular.svg#museo700') format('svg');
            font-weight: normal;
            font-style: normal;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <!-- ========== Strat static html ========== -->
        <div style="width: 960px;">
            <div style="margin: 60px!important;">
                <div style="position: relative;">
                    <div class="toparea" style="font-family: Arial, Helvetica, sans-serif; padding-bottom: 30px;">
                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 20px 0px 0px 10px; width: 30%;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                width="124" height="100" />
                        </div>
                        <div style="float: left; text-align: center; padding: 5px 10px 0px 0px; width: 33%;"><%--<img src="../images/wecome_img.jpg" width="202" height="134" alt="" />--%></div>
                        <div style="float: right; text-align: right; padding: 5px 10px 0px 0px; width: 30%;">
                            <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 21px; color: #00b0f0; font-weight: 400;">
                                <span style="font-size: 20px; font-weight: 600; line-height: 24px;">THINK GREEN SOLAR<br />
                                    1300 484 784 </span><br>
                                <span style="font-size: 18px;"><a href="mailto:info@thinkgreensolar.com.au">info@thinkgreensolar.com.au</a><br />
                                    <a href="http://www.thinkgreensolar.com.au/" target="_blank">www.thinkgreensolar.com.au</a></span>
                            </p>
                        </div>
                        <div style="float: none; clear: both"></div>
                    </div>
                </div>
                <div>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 30px 0px;"><strong>Dear</strong>
                        <asp:Label ID="lblContact" runat="server"></asp:Label></p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;">Thank you for choosing THINK GREEN SOLAR as your Solar Installation Specialists. We're confident that you'll be more than satisfied by the quality of our services and products.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;">The information enclosed will help you make the most of our services. In some cases the next process may vary or require more time, however most installation will involve the following steps.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;"><strong style="color: #00b0f0; font-weight: 600;">Step1: </strong>Now that we've received your deposit, we'll need you to sign and returned the second page of your quote, along with a copy of your power bill (all pages). This allows us to accurately complete the paperwork that goes into getting your solar system installed and connected to the grid.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;">
                        <strong style="color: #00b0f0; font-weight: 600;">Step2: </strong>Once we've received these documents, we'll submit all relevant approval paperwork where required.<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*if application is necessary, the Power Distributor will respond with approximately 17 business days
                    </p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;"><strong style="color: #00b0f0; font-weight: 600;">Step3: </strong>Our friendly Installation Department will then contact you to arrange a suitable date & time for the installation.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;"><strong style="color: #00b0f0; font-weight: 600;">Step4: </strong>On the specified date, Our CEC Accredited Installer will perform the installation.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;"><strong style="color: #00b0f0; font-weight: 600;">Step5: </strong>If any paperwork is still outstanding our installer will finalise this with you on the day. This includes retrieving a signed copy of your quote if it has not already been provided.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;"><strong style="color: #00b0f0; font-weight: 600;">Step6: </strong>Our teams then organise for the <strong><em>Independent safety inspection</em></strong> which can take up to 28 days. The inspector will ensure the system has been safely installed and meets the current regulation. The inspector will then turn the system on if it compliant.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 20px 0px;"><strong style="color: #00b0f0; font-weight: 600;">Step7: </strong>Once deemed safe, the inspector will provide our teams with a <strong><em>Certificate of Electrical Safety</em></strong>. At this stage your system will operating in conjunction with your property's electricity usage, however it won't be feeding excess solar energy into the grid.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 70px 0px;"><strong style="color: #00b0f0; font-weight: 600;">Step8: </strong>In order to allow the grid feed-in, we submit all paperwork to the Energy Retailer. The retailer will then contact you to arrange your feed-in-tariff. Once agreed upon, a meter reconfiguration will be organised through the Energy Distributor which allows for energy to be drawn from and fed into the grid. After this your system is online and fully functioning.</p>
                    <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 400px; color: #000; margin: 0px; padding: 0px 0px 0px 0px;">Again, thank you for choosing THINK GREEN SOLAR.</p>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div style="position: relative;">
                    <div style="border-top: #000 solid 3px; margin-top: 30px; padding-top:5px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="25%" align="center" valign="middle">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo1.jpg") %>" width="130" height="113" />
                                </td>
                                <%--<td width="25%" align="center" valign="middle">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo2.jpg") %>" width="117" height="113" />--%>
                                </td>
                                <td width="25%" align="center" valign="middle">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo3.jpg") %>" width="168" height="113" />
                                </td>
                                <td width="25%" align="center" valign="middle">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo4.jpg") %>" width="84" height="113" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ========== End static html ========== -->
        <%-- <br>
  <br>
  <br>
  <div style="width: 100%; font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">
    <div style="width: 960px; margin: 0px auto; font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">--%>
        <%--<div style="font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">
                    <div style="float: left; font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">
                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/DocLogonew.jpg") %>" width="164" height="142" />
                    </div>
                </div>--%>
        <%--<div style="clear: both; font-family: 'OpenSansLight' , sans-serif; font-size: 18px;"></div>
      <div style="font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">
        <div style="font-family: 'OpenSansLight' , sans-serif; font-size: 18px;"> <br />
          <p>
            <asp:Label ID="lblContact" runat="server"></asp:Label>
            ,<br />
            <asp:Label ID="lblPostalAddress1" runat="server"></asp:Label>
            <br />
            <asp:Label ID="lblPostalAddress2" runat="server"></asp:Label>
          </p>
          <br />
          <p> Dear
            <asp:Label ID="lblContact2" runat="server"></asp:Label>
            , </p>
          <p> Thank you for choosing EURO SOLAR for your Solar System. We are confident that you
            will be very satisfied with the services that we offer. </p>
          <p> The information enclosed will help you make the most of our services. If you have
            questions, please contact us. Your Sales Person,
            <asp:Label ID="lblSalesRep" runat="server"></asp:Label>
            , can be reached at 1300
            959 013. Your Quotation/Contract No is
            <asp:Label ID="lblQuotationNumber" runat="server"></asp:Label>
            . When you call, please
            have it handy so that we can help in more efficient way. </p>
          <p style="padding-bottom: 0px; margin-bottom: 6px; padding-top: 10px;"> 
            <!--You are 3 Step away from the installing your solar system at your property.--> 
            <strong>What Happens Next :</strong> </p>
          <br />
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 0px #999; border-top: 0px solid #999;">
            <tr>
              <td width="100%" align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Step1:</strong> Please send back the signed first & last page of the quotation as well as a copy of your quarterly Power Bill (all pages). This is so we can correctly complete your paperwork & get you connected to the grid as soon as possible. You can send these documents via fax [07 3319 6197], email, or post. </td>
            </tr>
            <tr>
              <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Step2:</strong> Once we’ve received all documents specified in Step 1, we will apply for your relevant Pre-Approval. </td>
            </tr>
            <tr>
              <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Step3:</strong> Your Power Distributors will respond to the application within approximately 14 business days. </td>
            </tr>
            <tr>
              <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Step4:</strong> Our friendly Installation Department will contact you to arrange a suitable date & time for installation. </td>
            </tr>
            <tr>
              <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Step5:</strong> Our CEC Accredited Installer will install your system. </td>
            </tr>
            <tr>
              <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Step6:</strong> Once the installation is complete, the customer signs all relevant paperwork. This is then returned immediately to the installer. </td>
            </tr>
            <tr>
              <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Step7:</strong> The installer then organizes the Independent Safety Inspector visit [this may take up to 28 days] which ensures the system is safe and up to regulation standards. At this point the inspector will turn your system on. </td>
            </tr>
          </table>
          <br />
          <p>Afterwards the Inspector will provide a Certificate of Electrical Safety to the installer who will then give it to us. From this point your system will be fully functional in powering your house but it won’t be feeding back any excess power to the grid.</p>
          <p>Finally, in order to allow your system to feed power back into the grid, there are a few final steps.</p>
          <ol style="margin: 0px; padding: 0px 20px 20px 20px;">
            <li style="padding: 0px 0px 6px 0px;">Once we receive all your completed paperwork, we send it off to the electricity company.</li>
            <li style="padding: 0px 0px 6px 0px;">Following this, you will get a phone call or letter from your electricity company detailing the new feed in tariff rates (your 8c tariff if applicable). </li>
            <li style="padding: 0px 0px 6px 0px;">Once you agree to their tariffs they can raise a service order with your distributor who then connects you to the grid. (3 weeks – this process takes the longest).</li>
          </ol>
          <br />
          <p style="padding-top: 5px; margin-top: 0px;">Again, thank you for choosing EURO SOLAR.</p>
          <%--<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 0px #999; border-top: 3px solid #333;">
                            <tr>
                                <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Queensland</strong></td>
                                <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Victoria</strong></td>
                                <td rowspan="2" align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;">
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_loages.jpg") %>" width="74" height="61" /></td>
                                <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;"><strong>Tasmania</strong></td>
                            </tr>
                            <tr>
                                <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;">P O Box 570,<br />
                                    Archerfield, QLD, 4108</td>
                                <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;">126/45 Gilby Road,<br />
                                    Mount Waverley, VIC, 3149</td>
                                <td align="left" style="border-right: solid 0px #999; border-bottom: solid 0px #999; border-collapse: collapse; padding: 5px;">4A Swanston Road,<br />
                                    Waverley, TAS, 7250</td>
                            </tr>
                        </table>--%>
        <%--</div>
      </div>
    </div>
  </div>--%>
    </form>
</body>
</html>
