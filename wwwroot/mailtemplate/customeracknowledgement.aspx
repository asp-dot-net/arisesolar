﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customeracknowledgement.aspx.cs" Inherits="mailtemplate_customeracknowledgement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
</head>
<body>
<form id="form1" runat="server">
  <div>
    <div style="font-size: 11px; width: 100%;">
      <div style="font-size: 11px; width: 960px; margin: 0px auto;">
        <div style="border: #000 solid 1px;">
          <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;"> <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                width="124" height="100" /> </div>
          <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** Arise Solar Pty Ltd**<br />
              T/A THINK GREEN SOLAR</h2>
            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;"> P O Box 570, Archerfield, QLD,4108<br />
              ABN: 95 130 845 199 Pho: 1300 959 013<br />
              Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;">infoqld@thinkgreensolar.com.au</a> </p>
          </div>
          <div style="float: left; text-align: center; padding: 8px;">
            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;">STC Assignment Form<br />
              PV Solar</h2>
          </div>
          <div style="float: none; clear: both;"></div>
        </div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 11px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
          <tr>
            <td colspan="7" align="center" valign="top" bgcolor="#d6d6d6" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px; padding-left: 150px;"><strong>Customer Acknowledgement</strong></td>
            <td colspan="5" align="center" style="border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px 8px 5px 10px; background: #d6d6d6;"><strong>Customer No :
              <asp:Label ID="lblcustno2" runat="server"></asp:Label>
              </strong></td>
          </tr>
          <tr>
            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; line-height: 20px;"><strong>I,
              <asp:Label ID="lblContact" runat="server"></asp:Label>
              <br />
              Of,
              <asp:Label ID="lblInstallAddressComplete" runat="server"></asp:Label>
              </strong> <br />
              As the &quot;<strong>Customer</strong>&quot; I do hereby acknowledge that I have:
              <ul style="font-size: 11px; margin-left: 20px; padding:0px; margin-top:5px; margin-bottom:5px;">
                <li>Received a copy of my invoice and User Manuals.</li>
                <li>Been made aware by the installer of the best location for my solar panels and inverters
                  to be installed and have agreed with the location of the installation.</li>
                <li>Satisfied with the condition that installer has left the premises after completion
                  of the work.</li>
              </ul>
              <strong>I also confirm that:</strong>
              <ul style="font-size: 11px; margin-left: -15px; margin-top:5px; margin-bottom:5px;">
                <li>I am completely satisfied with the Solar Power system I have been provided with,
                  including (but not limited to) the installation, positioning and location of the
                  solar panels and inverters.</li>
                <li>I am aware of and understand that once the system has been installed, it will inspected
                  and signed for by an independent inspector, and both the Certificate of Electrical
                  Safety and Electrical Work Request (EWR) will be sent to my electricity company
                  for processing of the Smart Meter Installation or reconfiguration (if I already
                  have a smart meter at my premises). </li>
                <li>I am aware that any damage caused by turning the system on before the inspection
                  has been completed is not covered by the warranty.</li>
                <li>I am aware that any claims for alleged damages caused by the installation of my
                  system are to be made within Five (5) business days from the date of installation.</li>
                <li>I received the Signage Board from the Installer and Happy to Display Outside my
                  House.</li>
                <li>I agree to publish my feedback for review purpose.</li>
                <li><strong>If I am a Certegy Ezi Pay customer I, the undersigned state that:</strong>
                  <ul style="font-size: 11px; margin-left: -15px; margin-top:5px; margin-bottom:5px;">
                    <li>I am the owner of the property or dwelling to which the goods are being installed</li>
                    <li>I understand it is not the responsibility of the merchant to connect my solar power
                      unit/s to the electricity grid and I must commence repayments to Certegy upon completion
                      of the installation of the solar power unit/s by the merchant.</li>
                    <li>The construction of services provided to my property or dwelling from the above
                      company merchant: has been completed to my satisfaction.</li>
                    <li>I am happy to commence my repayments to Certegy.</li>
                  </ul>
                </li>
              </ul>
              <br />
              Job satisfactorily completed and installed on (date) _________________ </td>
          </tr>
          <tr>
            <td colspan="12" align="center" valign="top" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">QUALITY OF SERVICE (1=POOR, 2=AVERAGE, 3=GOOD, 4=VERY GOOD) </td>
          </tr>
          <tr>
            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">Overall Impression </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">1 </td>
            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">2 </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">3 </td>
            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">4 </td>
          </tr>
          <tr>
            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">Services / Technical Support </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">1 </td>
            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">2 </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">3 </td>
            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">4 </td>
          </tr>
          <tr>
            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">Installation </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">1 </td>
            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">2 </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">3 </td>
            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">4 </td>
          </tr>
          <tr>
            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">Your Experience </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">1 </td>
            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">2 </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">3 </td>
            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">4 </td>
          </tr>
          <tr>
            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">Personal View : </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">1 </td>
            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">2 </td>
            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">3 </td>
            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">4 </td>
          </tr>
          <tr>
            <td colspan="12" align="left" valign="top" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="12" align="left" bgcolor="#d6d6d6" valign="top" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;"><strong>Installation Sketch</strong></td>
          </tr>
          <tr>
            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px; height: 300px;">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="12" align="left" valign="top" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;">Signed (&quot;The Customer&quot;, or authorised agent): </td>
          </tr>
          <tr>
            <td colspan="3" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px; padding-top: 20px;">Name_____________________ </td>
            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px; padding-top: 20px;">Signature_______________________ </td>
            <td colspan="6" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px; padding-top: 20px;">Date:____________________ </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</form>
</body>
</html>
