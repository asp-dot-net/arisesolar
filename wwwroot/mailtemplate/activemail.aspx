﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="activemail.aspx.cs" Inherits="mailtemplate_activemail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width='600' border="0" align='left' cellpadding='0' cellspacing='1' style='color: #666; font-size: 14px;'>
                <tr>
                    <td>
                        <asp:Image ID="emailtop" runat="server" />
                    </td>
                </tr>
                <tr bgcolor='#ffffff'>
                    <td style="border: 1px solid #c17709;">
                        <table id="mytable" width='600' border="0" align='left' cellpadding='5' cellspacing='1'
                            style='color: #666; font-size: 14px;'>
                            <tr>
                                <td style="background-color: #d4d4d4; padding: 5px; color: #000;">
                                    <b>Dear <%= Request["Customer"] %></b>
                                </td>
                            </tr>
                            <tr>
                                <td>Dear Customer, Thank you for choosing Arise Solar and we are committed to provide better service to our customers . We have received your all documents and we are going to organise approval from your distributor. You will hear from our installation team for installation  date and time up to 4 weeks. If you have any query please call us on 1300274737.
And press 2
                                </td>
                            </tr>
                            <tr>
                                <td>Thanks,<br />
                                    Installation Team<br />
                                    ARISE SOLAR
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Image ID="emailbottom" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
