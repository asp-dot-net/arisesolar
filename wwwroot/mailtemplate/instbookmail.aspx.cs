﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_instbookmail : System.Web.UI.Page
{
    protected string SiteURL;
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        SiteURL = SiteURL + (SiteURL.EndsWith("/") ? "" : "/");
        if(!string.IsNullOrEmpty(Request.QueryString["nearmap"]))
        {
            image2323.Visible = true;
        //image2323.ImageUrl = pdfURL + "Nearmapphoto/" + Request.QueryString["nearmap"];
        image2323.ImageUrl = SiteConfiguration.GetDocumnetPath("Nearmapphoto", Request.QueryString["nearmap"]);
        }
        //emailtop.ImageUrl = SiteURL + "/userfiles/emailtop/" + st.emailtop;
        emailbottom.ImageUrl = SiteURL + "/userfiles/emailbottom/" + st.emailbottom;
    }
}