﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;

public partial class FeedInTariffSign : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int startindex;
    protected static int lastpageindex;

    protected void Page_Load(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        String Token = Request.QueryString["rt"];
        pagetitle.Text = "AriseSolar | Feed In Tariff Signature";

        int TokenExist = Clstbl_TokenData.tbl_TokenDataAcnoExistByToken_ProjectID(Token, ProjectID);
        SttblProjects st1 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //SttblProjects2 st3 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        string contactid = st1.ContactID;
        string InstallerID = st1.Installer;
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(contactid);
        Sttbl_TokenData stk = Clstbl_TokenData.tbl_TokenDataAcno_SelectByToken_ProjectID(Token, ProjectID);

        if (!IsPostBack)
        {
            if (TokenExist == 0)
            {
                panelNotComplete.Visible = false;
                PanelPageNotExist.Visible = true;
            }

            int ProjectIDSigned = Clstbl_SignatureLog.tbl_SignatureLogAcnoExistByProjectID_DocNo(ProjectID, stk.QDocNo);
            if (ProjectIDSigned == 1)
            {
                panelNotComplete.Visible = false;
                panelExist.Visible = true;
            }

            string IsExpired = stk.IsExpired;
            if (IsExpired == "True")
            {
                panelNotComplete.Visible = false;
                panelExist.Visible = true;
                lblCustName4.Text = st2.ContFirst + " " + st2.ContLast;
            }
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            custompageIndex = 1;
        }

        lblCustName.Text = st2.ContFirst + " " + st2.ContLast;
        lblName.Text = st2.ContFirst + " " + st2.ContLast;
        lblMobileNo.Text = st2.ContMobile;
        lblEmail.Text = st2.ContEmail;
        //lblExportControlLimitKw.Text = st1.KwExport;
        lblInstallerName.Text = st1.SalesRepName;
        lblRetailer.Text = "ARISE SOLAR PTY LTD";
        lblCustAdd.Text = st1.InstallAddress + "," + st1.InstallCity + "," + st1.InstallState + "-" + st1.InstallPostCode;

        lblCustName3.Text = st2.ContFirst + " " + st2.ContLast;
        lblCustName4.Text = st2.ContFirst + " " + st2.ContLast;
        lblTodayDate.Text = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Today));
        lblCustEmailID.Text = st2.ContEmail;
        // lblIPAdd2.Text = lblIPAdd.Text;

    }

    [WebMethod]
    public static string InsertData(string value, String ProjectID, String Token, String PhysicalRootLocation, String Latitude, String Longitude, String IPAddress, String Country, String Region, String City, String Postal, String Yes, String No)
    {
        string msg = string.Empty;

        SttblProjects st1 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        String contactid = st1.ContactID;
        String InstallerID = st1.Installer;
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(contactid);
        SttblEmployees st3 = ClstblEmployees.tblEmployees_SelectByEmployeeID(st1.SalesRep);
        DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        SttblProjects2 st4 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);

        int TokenExist = Clstbl_TokenData.tbl_TokenDataAcnoExistByToken_ProjectID(Token, ProjectID);
        if (TokenExist == 1)
        {
            Sttbl_TokenData st = Clstbl_TokenData.tbl_TokenDataAcno_SelectByToken_ProjectID(Token, ProjectID);
            if (st.IsExpired == "False")
            {
                try
                {
                    String FileName = ProjectID + "_" + Token + "_" + "FeedInTariff_Owner.jpg";
                    byte[] bytIn = null;
                    value = value.Replace("data:image/png;base64,", "");
                    bytIn = Convert.FromBase64String(value);
                    //var path = Request.PhysicalApplicationPath + "userfiles\\" + "Signature" + "\\" + FileName;

                    var path = PhysicalRootLocation + "userfiles\\" + "Signature" + "\\" + FileName;
                    FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
                    fs.Write(bytIn, 0, bytIn.Length);
                    fs.Close();

                    SiteConfiguration.UploadPDFFile("Signature", FileName);
                    // SiteConfiguration.deleteimage(FileName, "Signature");

                    String CustName = st2.ContFirst + " " + st2.ContLast;
                    String SignedDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
                    String latitude = Latitude;
                    String longitude = Longitude;
                    String IP_Address = IPAddress;
                    String CustEmail = st2.ContEmail;

                    Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenDataAcno_SelectByToken_ProjectID(Token, ProjectID);
                    string Docno = sttok.QDocNo;
                    try
                    {
                        int success = Clstbl_SignatureLog.tbl_SignatureLogAcno_Insert(CustName, SignedDate, latitude, longitude, IP_Address, CustEmail, Token, ProjectID, path, Docno);
                        Clstbl_TokenData.tbl_TokenDataAcno_Update_IsExpired(Token, ProjectID, "True");

                        if (success > 0)
                        {
                            bool Update = Clstbl_SignatureLog.tbl_SignatureLogAcno_Update(success.ToString(), Country, Region, City, Postal);
                            bool ysNo = ClstblProjects.tblProjectAcknowledgement_UpdateYesNo(Docno, Yes, No);

                            bool Sign = Clstbl_TokenData.tbl_TokenDataAcno_Update_SignFlag(Token, ProjectID, "True");

                            msg = "success";
                            //String QFileName = st.QDocNo + "Quotation.pdf";
                            //SiteConfiguration.DeletePDFFile("quotedoc", QFileName);
                            //Telerik_reports.generate_quatation(ProjectID, Qdocno);
                            //if (st4.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                            //{
                            //    Telerik_reports.generate_Quote(ProjectID, Docno);
                            //}
                            //else
                            //{
                            //    Telerik_reports.generate_QuoteCash(ProjectID, Docno);
                            //}

                            #region UpdateSignedQuote
                            if (success > 0)
                            {
                                #region SendMail
                                //SiteConfiguration.deleteimage(FileName, "Signature");
                                TextWriter txtWriter = new StringWriter() as TextWriter;
                                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                                string from = stU.from;
                                String Subject = "Signature Done- " + ConfigurationManager.AppSettings["SiteName"].ToString();

                                string body = string.Empty;
                                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/mailtemplate/SignatureCompletionMail.aspx")))
                                {
                                    body = reader.ReadToEnd();
                                }
                                body = body.Replace("{SalesRep}", st1.SalesRepName); //replacing the required things  
                                body = body.Replace("{CustName}", st2.ContFirst + " " + st2.ContLast);
                                body = body.Replace("{ProjectNumber}", st1.ProjectNumber);
                                body = body.Replace("{year}", Convert.ToString(DateTime.Now.Year));
                                body = body.Replace("{sitename}", ConfigurationManager.AppSettings["SiteName"].ToString());

                                Utilities.SendMail(from, st3.EmpEmail, Subject, body);
                                //Utilities.SendMail(from, "deep@meghtechnologies.com", Subject, body);
                                try
                                {
                                }
                                catch
                                {
                                }

                                #endregion
                            }

                            #endregion
                        }
                        else
                        {
                            msg = "Error";
                        }
                    }
                    catch
                    {
                    }
                }
                catch
                {
                    msg = "Error";
                }
            }
            else
            {
                msg = "Error(Token Expired).";
            }
        }
        else
        {
            msg = "Error(Token does not exist).";

        }

        return msg;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }
}