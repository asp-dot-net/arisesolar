<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InvoicePayments.ascx.cs"
    Inherits="includes_InvoicePayments" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register Src="~/payway/EnterCCDetails.ascx" TagPrefix="uc1" TagName="EnterCCDetails" %>--%>

<style>
    .overlay {
        position: absolute;
        background-color: #696969;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 350px;
        opacity: 0.8;
        -moz-opacity: 0.8;
        filter: alpha(opacity=80);
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        z-index: 10000;
    }
</style>
<style>
    .modalbackground {
        background-color: Gray;
        opacity: 0.5;
        filter: Alpha(opacity=50);
    }

    .modalpopup {
        background-color: white;
        padding: 6px 6px 6px 6px;
    }

    table.formtable h3 {
        color: #800000;
        font-size: 16px;
    }
</style>
<style type="text/css">
    .modalPopup {
        background-color: #696969;
        filter: alpha(opacity=40);
        opacity: 0.7;
        xindex: -1;
    }

    .z_index_loader {
        z-index: 998 !important;
    }
</style>


<asp:UpdatePanel ID="UpdatePanel11" runat="server">
    <ContentTemplate>
        
<style>
    table.table tr td, table.table tr th { 
        overflow:visible;
    }
</style>

<script>
    function ComfirmDelete(event, ctl) {
        alert("Done");
        event.preventDefault();
        var defaultAction = $(ctl).prop("href");

        swal({
            title: "Are you sure you want to delete this Record?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true,
            closeOnCancel: true
        },

            function (isConfirm) {
                if (isConfirm) {
                    // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    eval(defaultAction);

                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    return true;
                } else {
                    // swal("Cancelled", "Your imaginary file is safe :)", "error");
                    return false;
                }
            });
    }
</script>
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            }

            function pageLoadedpro() {

                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();

            }
        </script>

        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            function pageLoaded() {
                $(".myvalcomp").select2({
                    //placeholder: "select",
                    allowclear: true
                });
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.redreq').click(function () {
                    formValidate();
                });
                //$('.i-checks').iCheck({
                //    checkboxClass: 'icheckbox_square-green',
                //    radioClass: 'iradio_square-green'
                //});
            }
        </script>
        <div class="statuspopup">
            <div class="paddbtm10">

                <asp:Button ID="btnInvPay" runat="server" OnClick="btnInvPay_Click1" CssClass="btneditinvoice martopzero"
                    Text="Edit Invoice Payment" CausesValidation="false" />

                <%--<asp:ImageButton ID="btnInvPay" runat="server" ImageUrl="~/images/new/btm_edit_invices.png"
                    CausesValidation="false" OnClick="btnInvPay_Click" />--%>
                <asp:Button ID="btnNULLInvPay" Style="display: none;" runat="server" />
            </div>
        </div>
        <cc1:ModalPopupExtender ID="ModalPopupExtenderInvPay" runat="server" BackgroundCssClass="modalbackground z_index_loader"
            DropShadow="false" PopupControlID="divAddInvPay" TargetControlID="btnNULLInvPay"
            CancelControlID="btnClose">
        </cc1:ModalPopupExtender>
        <div id="divAddInvPay" runat="server" style="display: none;" class="modal_popup">
            <div class="modal-content modaltable">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div style="float: right">
                        <button id="btnClose" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" onclick="btnClose_Click">Close</button>
                    </div>
                    <h3 class="modal-title" id="myModalLabel">Invoice Details -
                        <asp:Literal runat="server" ID="ltcustdetail"></asp:Literal></h3>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body" style="width: 1100px; height: 575px; overflow: scroll;">
                        <div class="formainline">

                            <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                <%--   <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanel11">
                                    <ProgressTemplate>
                                        <div class="overlay">--%>
                                <%-- <asp:Image ID="imgloading" ImageUrl="~/admin/images/loading.gif" AlternateText="Processing"
                                                    runat="server" />--%>
                                <%-- </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                                <div class="">
                                    <div class="col-md-6">
                                        <span class="invoicenum"><strong>Invoice Number :
                                                <asp:Label ID="lblProjectNumber" runat="server"></asp:Label>
                                        </strong></span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="unpaidbill" align="right">
                                            <table border="0" cellspacing="0" cellpadding="0" align="right">
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="btnPrintReceipt" runat="server" CssClass="btn btn-primary btn-xs" CausesValidation="false"
                                                            OnClick="btnPrintReceipt_Click" ValidationGroup="print"><i class="fa fa-print"></i> Print</asp:LinkButton>

                                                        <%--  <asp:LinkButton ID="btnPrintReceipt" runat="server" Width="20" Height="19" class="btn btn-primary btn-xs"
                                                                OnClick="btnPrintReceipt_Click" CausesValidation="false" ValidationGroup="print" ><i class="fa fa-print"></i></asp:LinkButton>--%>
                                                        <%-- <a href="" class="btn btn-primary btn-xs" OnClick="btnPrintReceipt_Click"><i class="fa fa-print"></i> Print</a>--%>
                                                    </td>
                                                    <td style="padding-left: 10px;" valign="top">
                                                        <span class="amtvaluebox editpopup btn-xs">
                                                            <strong>
                                                                <asp:Label ID="lblStatus" runat="server" CssClass="uppaidtext" Style="padding-top: 0px;"></asp:Label>
                                                            </strong>
                                                        </span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Invoice&nbsp;Notes </label>
                                            </span><span>
                                                <asp:TextBox ID="txtInvoiceNotes" runat="server" class="form-control" TextMode="MultiLine" Height="180px"
                                                    CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="haftdiv">
                                            <div class="form-group ">
                                                <span class="name">
                                                    <label class="control-label">Total Cost </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtTotalCostNew" Enabled="false" class="form-control" runat="server" CssClass="form-control"></asp:TextBox>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group ">
                                                <span class="name">
                                                    <label class="control-label">Less Rebate </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtLessRebate" runat="server" CssClass="form-control" OnTextChanged="txtLessRebate_TextChanged"
                                                        AutoPostBack="true"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtLessRebate"
                                                        Display="Dynamic" ErrorMessage="Please enter a number" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group ">
                                                <span class="name">
                                                    <label class="control-label">Total Cost </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtTotalCost" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Paid to Date </label>
                                            </span><span>
                                                <asp:TextBox ID="txtPaidDate" runat="server" Enabled="false" Text="0" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Bal Owing </label>
                                            </span><span>
                                                <asp:TextBox ID="txtBalOwing" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group" runat="server" id="divsysdetail">
                                            <span class="name ">
                                                <label class="control-label" style="width: 100%!important;">System Details </label>
                                            </span>
                                            <span>
                                                <%-- <asp:Literal runat="server" ID="ltsysdetail"  ></asp:Literal>--%>
                                                <asp:TextBox ID="ltsysdetail" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-12">
                                        <div class="table-round invoicetable">
                                            <table style="width: 100%" class="table tablebrd table-bordered gridcss spcaldate" style="margin-bottom: 10px;">
                                                <thead>
                                                    <tr class="gridheader">
                                                        <th>Pay&nbsp;Date </th>
                                                        <th>Total&nbsp;Paid </th>
                                                        <th>GST </th>
                                                        <th width="200px">Pay&nbsp;By </th>
                                                        <th>S/Chg </th>
                                                        <th>Rec&nbsp;By </th>
                                                        <th>Updated&nbsp;By </th>
                                                        <th class="center-text">Delete </th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="rptPayment" runat="server" OnItemDataBound="rptPayment_OnItemDataBound"
                                                    OnItemCommand="rptPayment_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 150px">
                                                                <asp:HiddenField ID="hndIsVerified" runat="server" Value='<%#Eval("IsVerified") %>' />
                                                                <asp:HiddenField ID="hndid" runat="server" Value='<%#Eval("InvoicePaymentID") %>' />
                                                                <asp:HiddenField ID="hndRowNumber" runat="server" Value='<%#Eval("RowNumber") %>' />
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                             <%--<asp:TextBox ID="txtInvoicePayDate" runat="server" Width="90px" Text='<%#Eval("InvoicePayDate") %>'></asp:TextBox></td>
                                                                            <td style="margin-top: 10px; vertical-align: middle">
                                                                                <asp:ImageButton ID="Image19" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                                <cc1:CalendarExtender ID="CalendarExtender19" runat="server" PopupButtonID="Image19"
                                                                                    TargetControlID="txtInvoicePayDate" Format="dd/MM/yyyy">
                                                                                </cc1:CalendarExtender>
                                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender19" runat="server" Mask="99/99/9999"
                                                                                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                    MaskType="date" TargetControlID="txtInvoicePayDate" CultureName="en-GB">
                                                                                </cc1:MaskedEditExtender>--%>
                                                                            <div class="input-group date datetimepicker1">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="txtInvoicePayDate" runat="server" ClientIDMode="Static" class="form-control" Width="110px" Text='<%#Eval("InvoicePayDate") %>'>
                                                                                </asp:TextBox>
                                                                            </div>


                                                                            <%--<asp:CompareValidator ID="CompareValidatorPayDate" runat="server" ErrorMessage="Invalid Date"
                                                                                    ControlToValidate="txtInvoicePayDate" Operator="GreaterThanEqual" Display="Dynamic" Type="Date"></asp:CompareValidator>--%>
                                                                            <%-- <asp:CustomValidator ClientValidationFunction="CompareDates" ID="cvTodate" runat="server"
                        ErrorMessage="Invalid Date" ControlToValidate="txtInvoicePayDate"></asp:CustomValidator>
                                                                            --%>

                                                                            <asp:HiddenField ID="hiddenSentDate" runat="server" />
                                                                            <asp:CompareValidator ID="CompareValidator1" runat="server"
                                                                                ControlToValidate="txtInvoicePayDate"
                                                                                ErrorMessage="Invalid Date" Operator="GreaterThanEqual" SetFocusOnError="True" Display="Dynamic"></asp:CompareValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtInvoicePayTotal" runat="server" class="form-control modaltextbox" Width="100px" Text='<%#Eval("InvoicePayTotal","{0:0.00}") %>'
                                                                    OnTextChanged="txtInvoicePayTotal_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtInvoicePayTotal"
                                                                    Display="Dynamic" ErrorMessage="Please enter a number" ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="save2"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtInvoicePayGST" runat="server" class="form-control modaltextbox" Enabled="false" Width="100px" Text='<%#Eval("InvoicePayGST","{0:0.00}") %>'></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                                                    ControlToValidate="txtInvoicePayGST" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="save2"></asp:RegularExpressionValidator></td>
                                                            <td style="width: 200px;">
                                                                <asp:HiddenField ID="hndInvoicePayMethodID" runat="server" Value='<%#Eval("InvoicePayMethodID") %>' />
                                                                <asp:DropDownList ID="ddlInvoicePayMethodID" Width="100px" AutoPostBack="true" CssClass="myvalcomp" OnSelectedIndexChanged="ddlInvoicePayMethodID_SelectedIndexChanged"
                                                                    runat="server" AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>


                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPayBy" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="ddlInvoicePayMethodID" Display="Dynamic" ValidationGroup="save2" InitialValue=""></asp:RequiredFieldValidator>
                                                                <div style="height: 5px"></div>
                                                                <asp:TextBox ID="txtReceiptNumber" runat="server" Visible="false" class="form-control modaltextbox" Width="100px" Text='<%#Eval("ReceiptNumber") %>'></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtReceiptNumber"
                                                                    WatermarkText="Receipt No" />

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required."
                                                                    ControlToValidate="txtReceiptNumber" ValidationGroup="save2" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidatorPayBy" runat="server" ErrorMessage="This value is required."
                                                                        ControlToValidate="ddlInvoicePayMethodID" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCCSurcharge" runat="server" class="form-control modaltextbox" Text='<%#Eval("CCSurcharge","{0:0.00}") %>'
                                                                    Enabled="false" Width="100px"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtCCSurcharge"
                                                                    Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$" ValidationGroup="save2"></asp:RegularExpressionValidator></td>
                                                            <td style="width: 150px;">
                                                                <asp:HiddenField ID="hndRecBy" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                                <asp:DropDownList ID="ddlRecBy" runat="server" AppendDataBoundItems="true" Width="120px" CssClass="myvalcomp"
                                                                    Enabled="false">
                                                                </asp:DropDownList></td>
                                                            <td>
                                                                <asp:Label ID="lblUpdateBy" runat="server" Text='<%#Eval("UpdatedName") %>'></asp:Label></td>
                                                            <td class="center-text">

                                                                <asp:LinkButton ID="lbtnDelete" runat="server" Visible='<%#Eval("UpdatedName").ToString()==""?true:false%>' CommandName="Delete" CssClass="btn btn-danger btn-xs" CausesValidation="false" 
                                                                    CommandArgument='<%#Eval("InvoicePaymentID")%>' data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                                                <i class="fa fa-trash"></i> Delete
                                                                </asp:LinkButton>




                                                                <%-- <asp:ImageButton ID="lbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("InvoicePaymentID") %>'
                                                                        OnClientClick="return confirm('Are you sure you want to delete This Record?  ');" CausesValidation="false" data-placement="top"
                                                                        data-toggle="tooltip" data-original-title="Delete" ImageUrl="~/admin/images/icons/icon_delet.png"></asp:ImageButton>--%>

                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <tr>
                                                    <td colspan="8" align="right">
                                                        <asp:Button ID="btnAddRow" runat="server" Text="Add" OnClick="btnAddRow_Click" CssClass="btn btn-primary redreq btnaddicon"
                                                            CausesValidation="false" /></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div></br></div>

                                        <div style="text-align: center;">
                                            <asp:Button CssClass="btn btn-primary redreq  savewhiteicon btnsaveicon" ID="btnSave" runat="server" Text="Save" CausesValidation="true"
                                                OnClick="btnSave_Click" ValidationGroup="save2" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="HiddenField1" runat="server" />

        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalpopupbackground"
            CancelControlID="ibtnCancelActive" DropShadow="true" PopupControlID="divAddActive"
            OkControlID="btnOKActive" TargetControlID="btnNULLActive">
        </cc1:ModalPopupExtender>
        <div id="divAddActive" runat="server">
            <div class="modal-dialog" style="width: 360px;">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">

                        <div style="float: right">
                            <asp:LinkButton ID="ibtnCancelActive" CausesValidation="false"
                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="ibtnCancelActive_Click1">
                   Close
                            </asp:LinkButton>
                        </div>
                        <h4 class="modal-title" id="H1">Pay Invoice</h4>
                    </div>
                    <%-- <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                    summary="">
                    <tbody>
                        <tr>
                            <td>--%>
                    <asp:HiddenField ID="hdnorderamount" runat="server" />
                    <asp:HiddenField ID="hdnpronum" runat="server" />

                    <div>

                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                           
                        </div>
                    </div>

                    <%--</td>
                        </tr>
                        <tr class="odd">
                            <td style="text-align: right; padding: 0px 0px 10px 0px;">--%>

                    <asp:Button ID="btnOKActive" Style="display: none; visible: false;" runat="server"
                        CssClass="btn" Text=" OK " />
                    <%--  </td>
                        </tr>
                    </tbody>
                </table>--%>
                </div>

            </div>
        </div>


    <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>
    <asp:HiddenField ID="hdndelete" runat="server" />
        <asp:Button ID="btnNULLActive" Style="display: none;" runat="server" />

        </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnPrintReceipt" />
        <asp:PostBackTrigger ControlID="btnSave" />
        <%--<asp:PostBackTrigger ControlID="ddlInvoicePayMethodID" />--%>
        <%--<asp:PostBackTrigger ControlID="btnAddRow" />--%>
    </Triggers>
</asp:UpdatePanel>

    
