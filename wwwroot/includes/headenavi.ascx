﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="headenavi.ascx.cs" Inherits="includes_headenavi" %>
<%--<div class="clearfix wrapper dk nav-user hidden-xs">--%>
<%--<div class="dropdown">--%>
<%--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="thumb avatar pull-left m-r">--%>
<%--<img src="images/a0.png" class="dker" alt="...">--%>
<%-- <asp:Image runat="server" ID="imguser" ImageUrl="~/images/crm.png" />
        <br />--%>
<%--<asp:Image runat="server" ID="Image1" ImageUrl="~/images/crm_img.jpg" />--%>
<%--<i class="on md b-black"></i>--%><%--</span><span class="hidden-nav-xs clearone"><span class="block m-t-xs"><strong class="font-bold text-lt">--%><%--<asp:Label ID="lblusername" runat="server"></asp:Label>--%><%--Customer Relationship Management</strong>--%> <%--<b class="caret"></b>--%><%--</span>--%><%--<span class="text-muted text-xs block">Art Director</span>--%> <%--</span></a>--%>
<%--<ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><span class="arrow top hidden-nav-xs"></span><a href="#">Settings</a> </li>
            <li><a href="profile.html">Profile</a> </li>
            <li><a href="#"><span class="badge bg-danger pull-right">3</span> Notifications </a></li>
            <li><a href="docs.html">Help</a> </li>
            <li class="divider"></li>
            <li><a href="modal.lockme.html" data-toggle="ajaxModal">Logout</a> </li>
        </ul>--%>
<%--</div>--%>
<%--</div>--%>
<nav class="nav-primary hidden-xs">
    <ul class="nav nav-main" data-ride="collapse">
        <li id="lidashboard" runat="server"><a href="<%=SiteURL%>admin/adminfiles/dashboard.aspx" class="auto"><i class="i icon dashboard">
            <!-- <img src="<%=SiteURL%>images/icon_dashboard.png" width="18" height="9">-->
        </i><span>Dashboard</span> </a></li>
        <li id="limaster" runat="server" visible="false"><a class="auto"><i class="i icon master">
            <!--<img src="<%=SiteURL%>images/icon_master.png">-->
        </i><span>Master</span> <span class="pull-right text-muted"><i class="i i-circle-sm-o text"></i><i class="i i-circle-sm text-active"></i></span></a>
            <ul class="nav dk  always-visible" id="subnavmain">
                <li id="licompanylocations" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/companylocations.aspx" class="auto"><i class="i i-dot"></i><span>Company Locations</span> </a></li>
                <li id="licustsource" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/custsource.aspx" class="auto"><i class="i i-dot"></i><span>Company Source</span> </a></li>
                <li id="licustsourcesub" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/custsourcesub.aspx" class="auto"><i class="i i-dot"></i><span>Company Source Sub</span> </a></li>
                <li id="licusttype" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/CustType.aspx" class="auto"><i class="i i-dot"></i><span>Company Type</span> </a></li>
                <li id="lielecdistributor" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/elecdistributor.aspx" class="auto"><i class="i i-dot"></i><span>Elec Distributor</span> </a></li>
                <li id="lielecretailer" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/elecretailer.aspx" class="auto"><i class="i i-dot"></i><span>Elec Retailer</span> </a></li>
                <li id="liemployee" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/employee.aspx" class="auto"><i class="i i-dot"></i><span>Employee</span> </a></li>
                <li id="lifinancewith" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/FinanceWith.aspx" class="auto"><i class="i i-dot"></i><span>Finance With</span> </a></li>
                <li id="lihousetype" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/housetype.aspx" class="auto"><i class="i i-dot"></i><span>House Type</span> </a></li>
                <li id="lileadcancelreason" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/leadcancelreason.aspx" class="auto"><i class="i i-dot"></i><span>Lead Cancel Reason</span> </a></li>
                <li id="limtcereason" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/mtcereason.aspx" class="auto"><i class="i i-dot"></i><span>Mtce Reasons</span> </a></li>
                <li id="limtcereasonsub" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/mtcereasonsub.aspx" class="auto"><i class="i i-dot"></i><span>Mtce Reasons Sub</span> </a></li>
                <li id="liprojectcancel" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/projectcancel.aspx" class="auto"><i class="i i-dot"></i><span>Project Cancel</span> </a></li>
                <li id="liprojectonhold" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/projectonhold.aspx" class="auto"><i class="i i-dot"></i><span>Project On-Hold</span> </a></li>
                <li id="liprojectstatus" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/projectstatus.aspx" class="auto"><i class="i i-dot"></i><span>Project Status</span> </a></li>
                <li id="liprojecttrackers" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/projecttrackers.aspx" class="auto"><i class="i i-dot"></i><span>Project Trackers</span> </a></li>
                <li id="liprojecttypes" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/projecttypes.aspx" class="auto"><i class="i i-dot"></i><span>Project Types</span> </a></li>
                <li id="liprojectvariations" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/projectvariations.aspx" class="auto"><i class="i i-dot"></i><span>Project Variations</span> </a></li>
                <li id="lipromotiontype" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/promotiontype.aspx" class="auto"><i class="i i-dot"></i><span>Promotion Type</span> </a></li>
                <li id="liprospectcategory" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/prospectcategory.aspx" class="auto"><i class="i i-dot"></i><span>Prospect Categories</span> </a></li>
                <li id="liroofangles" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/roofangles.aspx" class="auto"><i class="i i-dot"></i><span>Roof Angles</span> </a></li>
                <li id="liroottye" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/roottye.aspx" class="auto"><i class="i i-dot"></i><span>Root Type</span> </a></li>
                <li id="lisalesteams" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/salesteams.aspx" class="auto"><i class="i i-dot"></i><span>Sales Team</span> </a></li>
                <li id="listockcategory" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/StockCategory.aspx" class="auto"><i class="i i-dot"></i><span>Stock Category</span> </a></li>
                <li id="litransactiontypes" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/transactiontypes.aspx" class="auto"><i class="i i-dot"></i><span>Transaction Types</span> </a></li>
                <li id="lipaymenttype" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/paymenttype.aspx" class="auto"><i class="i i-dot"></i><span>Finance Payment Type</span> </a></li>
                <li id="liinvcommontype" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/invcommontype.aspx" class="auto"><i class="i i-dot"></i><span>Invoice Type</span> </a></li>
                <li id="lipostcodes" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/postcodes.aspx" class="auto"><i class="i i-dot"></i><span>Post Codes</span> </a></li>
                <li id="licustinstusers" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/custinstusers.aspx" class="auto"><i class="i i-dot"></i><span>Cust/Inst Users</span> </a></li>
                <li id="linewsletter" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/newsletter.aspx" class="auto"><i class="i i-dot"></i><span>News Letter</span> </a></li>
                <li id="lilogintracker" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/logintracker.aspx" class="auto"><i class="i i-dot"></i><span>Login Tracker</span> </a></li>
                <li id="lileftempprojects" runat="server"><a href="<%=SiteURL%>admin/adminfiles/company/leftempprojects.aspx" class="auto"><i class="i i-dot"></i><span>Left Emp Projects</span> </a></li>
                <li id="lilteamtime" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/lteamtime.aspx" class="auto"><i class="i i-dot"></i><span>L-Team App Time</span> </a></li>
                <li id="lirefundoptions" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/refundoptions.aspx" class="auto"><i class="i i-dot"></i><span>Refund Options</span> </a></li>
                <li id="liupdateformbayId" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/updateformbayId.aspx" class="auto"><i class="i i-dot"></i><span>Update Formbay Id</span> </a></li>
                <li id="lipickup" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/pickup.aspx" class="auto"><i class="i i-dot"></i><span>PickUp / Sent Through</span> </a></li>
                <li id="liunittype" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/unittype.aspx" class="auto"><i class="i i-dot"></i><span>Unit Type</span> </a></li>
                <li id="listreettype" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/streettype.aspx" class="auto"><i class="i i-dot"></i><span>Street Type</span> </a></li>
                <li id="listreetname" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/streetname.aspx" class="auto"><i class="i i-dot"></i><span>Street Name</span> </a></li>
                <li id="lipromooffer" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/promooffer.aspx" class="auto"><i class="i i-dot"></i><span>Promo Offer</span> </a></li>

            </ul>
        </li>
        <%--<li id="licompany" runat="server"><a href="<%=SiteURL%>admin/adminfiles/company/company.aspx" class="auto"><i class="i icon company"></i><span>Company</span> </a></li>--%>
        <li id="licompany" runat="server"><a runat="server" id="acompany" class="auto"><i class="i icon company"></i><span>Company</span> </a></li>
        <li id="licontacts" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/contacts.aspx" class="auto"><i class="i icon contact">
            <!--<img src="<%=SiteURL%>images/icon_contacts.png">-->
        </i><span>Contacts</span> </a></li>
        <li id="lipromosend" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/promo/promosend.aspx" class="auto"><i class="i icon spromo">
            <!--<img src="<%=SiteURL%>images/icon_smspromo.png">-->
        </i><span>Promo</span> </a></li>
        <li id="lileadtracker" runat="server" visible="false"><a id="hrefleadtracker" runat="server" class="auto"><i class="i icon tracker">
            <!--<img src="<%=SiteURL%>images/icon_tracker.png">-->
        </i><span>Tracker</span>  <span class="pull-right text-muted"><i class="i i-circle-sm-o text"></i><i class="i i-circle-sm text-active"></i></span></a>
            <ul class="nav dk always-visible">
                <li id="lileadtrackersub" runat="server"><a href="<%=SiteURL%>admin/adminfiles/company/leadtracker.aspx" class="auto"><i class="i i-dot"></i><span>Lead Tracker</span> </a></li>
                <li id="lilteamtracker" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/company/lteamtracker.aspx" class="auto"><i class="i i-dot"></i><span>Team Lead Tracker</span> </a></li>
                <li id="listctracker" runat="server"><a id="hrefstctracker" runat="server" class="auto"><i class="i i-dot"></i><span>STC Tracker</span> </a></li>
                <li id="liinstinvoice" runat="server"><a href="<%=SiteURL%>admin/adminfiles/invoice/instinvoice.aspx" class="auto"><i class="i i-dot"></i><span>Inst Invoice</span> </a></li>
                <li id="lisalesinvoice" runat="server"><a href="<%=SiteURL%>admin/adminfiles/invoice/salesinvoice.aspx" class="auto"><i class="i i-dot"></i><span>Sales Invoice</span> </a></li>
                <li id="liinstalltracker" runat="server"><a href="<%=SiteURL%>admin/adminfiles/company/installbookingtracker.aspx" class="auto"><i class="i i-dot"></i><span>Install Booking Tracker</span> </a></li>
                <li id="liaccrefund" runat="server"><a href="<%=SiteURL%>admin/adminfiles/master/accrefund.aspx" class="auto"><i class="i i-dot"></i><span>Refund</span> </a></li>
                <li id="liformbaydocsreport" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/formbaydocsreport.aspx" class="auto"><i class="i i-dot"></i><span>FormBay</span> </a></li>
                <li id="lipromotracker" runat="server"><a href="<%=SiteURL%>admin/adminfiles/company/promotracker.aspx" class="auto"><i class="i i-dot"></i><span>Promo Tracker</span> </a></li>
            </ul>
        </li>

        <li id="liProjectsView" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/view/project.aspx" class="auto"><i class="i icon projects1">
            <!--<img src="<%=SiteURL%>images/icons/icon_project.png">-->
        </i><span>Projects</span> </a></li>
        <li id="liinstallation" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/installation/installations.aspx" class="auto"><i class="i icon installation">
            <!--<img src="<%=SiteURL%>images/icons/icon_installation.png">-->
        </i><span>Installation</span> </a></li>
        <li id="liinstavailable" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/installation/instavailable.aspx" class="auto"><i class="i icon unavailabili">
            <!--<img src="<%=SiteURL%>images/unavailabilitylist.png">-->
        </i><span>UnAvailability List</span> </a></li>
        <li id="liinvoicesissued" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/invoice/invoicesissued.aspx" class="auto"><i class="i icon invoiceissue">
            <!--<img src="<%=SiteURL%>images/icons/icon_invoiceissue.png">-->
        </i><span>Invoices Issued</span> </a></li>
        <li id="liinvoicespaid" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/invoice/invoicespaid.aspx" class="auto"><i class="i icon invoicepaid">
            <!--<img src="<%=SiteURL%>images/icons/icon_invoicepaid.png">-->
        </i><span>Invoices Paid</span> </a></li>
        <li id="listock" runat="server" visible="false"><a class="auto"><i class="i icon stock">
            <!--<img src="<%=SiteURL%>images/icons/icon_stock.png">-->
        </i><span>Stock Item</span> <span class="pull-right text-muted"><i class="i i-circle-sm-o text"></i><i class="i i-circle-sm text-active"></i></span></a>
            <ul class="nav dk always-visible">
                <li id="listockitem" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stockitem.aspx" class="auto"><i class="i i-dot"></i><span>Stock Item</span> </a></li>
                <li id="listocktransfer" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stocktransfer.aspx" class="auto"><i class="i i-dot"></i><span>Stock Transfer</span> </a></li>
                <li id="listockorder" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stockorder.aspx" class="auto"><i class="i i-dot"></i><span>Stock Order</span> </a></li>
                <li id="listockdeduct" runat="server"><a href="<%=SiteURL%>admin/adminfiles/stock/stockdeduct.aspx" class="auto"><i class="i i-dot"></i><span>Stock Deduct</span> </a></li>
                <li id="listockusage" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/stockusage.aspx" class="auto"><i class="i i-dot"></i><span>Stock Usage</span> </a></li>
                <li id="listocktransferreport" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/stocktransferreportdetail.aspx" class="auto"><i class="i i-dot"></i><span>Stock Transfer Report</span> </a></li>
                <li id="listockorderreport" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/stockorderreport.aspx" class="auto"><i class="i i-dot"></i><span>Stock Order Report</span> </a></li>
                <li id="listockreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/stockreport.aspx" class="auto"><i class="i i-dot"></i><span>Stock Report</span> </a></li>

            </ul>
        </li>
        <li id="liCompanyView" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/view/company.aspx" class="auto"><i class="i icon leadtracker">
            <!--<img src="<%=SiteURL%>images/icon_leadtracker.png">-->
        </i><span>Customer</span> </a></li>
        <li id="liFollowUpsView" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/view/followups.aspx" class="auto"><i class="i icon folloup">
            <!--<img src="<%=SiteURL%>images/icons/icon_folloup.png">-->
        </i><span>Follow-Ups</span> </a></li>
        <%-- <li id="listctracker" runat="server" visible="false"><a id="hrefstctracker" runat="server" class="auto"><i class="i icon">
            <img src="<%=SiteURL%>images/icon_tracker.png"></i> <span>Tracker</span> </a>
            <ul class="nav dk">
                <li><a href="<%=SiteURL%>admin/adminfiles/invoice/instinvoice.aspx" class="auto"><i class="i i-dot"></i><span>Inst Invoice</span> </a></li>
                <li><a href="<%=SiteURL%>admin/adminfiles/invoice/salesinvoice.aspx" class="auto"><i class="i i-dot"></i><span>Sales Invoice</span> </a></li>
            </ul>
        </li>--%>
        <li id="liUploadLead" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/upload/lead.aspx" class="auto"><i class="i icon leadtracker">
            <!--<img src="<%=SiteURL%>images/icon_leadtracker.png">-->
        </i><span>Lead</span> </a></li>
        <li id="licustomerdetail" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/customer/custproject.aspx" class="auto"><i class="i icon mcte">
            <!--<img src="<%=SiteURL%>images/icon_leadtracker.png">-->
        </i><span>Customer Detail</span> </a></li>
        <li id="licasualmtce" runat="server" visible="false"><a class="auto"><i class="i icon mcte">
            <!--<img src="<%=SiteURL%>images/icon_mcte.png">-->
        </i><span>Mtce</span>  <span class="pull-right text-muted"><i class="i i-circle-sm-o text"></i><i class="i i-circle-sm text-active"></i></span></a>
            <ul class="nav dk always-visible">
                <li id="limtcecall" runat="server"><a href="<%=SiteURL%>admin/adminfiles/company/mtcecall.aspx" class="auto"><i class="i i-dot"></i><span>Maintenance</span> </a></li>
                <li id="licasualmtcesub" runat="server"><a href="<%=SiteURL%>admin/adminfiles/company/casualmtce.aspx" class="auto"><i class="i i-dot"></i><span>Casual Maintenance</span> </a></li>
            </ul>
        </li>
        <li id="liReports" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/salesreport.aspx" class="auto"><i class="i icon report">
            <!--<img src="<%=SiteURL%>images/icons/icon_report.png">-->
        </i><span>Report</span>  <span class="pull-right text-muted"><i class="i i-circle-sm-o text"></i><i class="i i-circle-sm text-active"></i></span></a>
            <ul id="ulreports" runat="server" class="nav dk default always-visible">
                <li id="linoinstalldate" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/noinstalldate.aspx" class="auto"><i class="i i-dot"></i><span>NoInstall Date</span> </a></li>
                <li id="liinstalldate" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/installdate.aspx" class="auto"><i class="i i-dot"></i><span>Install Date</span> </a></li>
                <li id="lipanelscount" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/panelscount.aspx" class="auto"><i class="i i-dot"></i><span>Panel Graph</span> </a></li>
                <li id="liaccountreceive" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/accountreceive.aspx" class="auto"><i class="i i-dot"></i><span>Account Receive</span> </a></li>
                <li id="lipaymentstatus" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/paymentstatus.aspx" class="auto"><i class="i i-dot"></i><span>Payment Status</span> </a></li>
                <li id="liweeklyreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/weeklyreport.aspx" class="auto"><i class="i i-dot"></i><span>Weekly Report</span> </a></li>
                <li id="lileadassignreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/leadassignreport.aspx" class="auto"><i class="i i-dot"></i><span>Lead Assign Report</span> </a></li>

                <li id="lioutstandingreport" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/outstandingreport.aspx" class="auto"><i class="i i-dot"></i><span>Out Standing Report</span> </a></li>

                <li id="lileadtrackreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/leadtrackreport.aspx" class="auto"><i class="i i-dot"></i><span>Lead Track Report</span> </a></li>
                <li id="lipanelstock" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/panelstock.aspx" class="auto"><i class="i i-dot"></i><span>Panel Stock Report</span> </a></li>
                <li id="lirooftypecount" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/rooftypecount.aspx" class="auto"><i class="i i-dot"></i><span>Roof Type Report</span> </a></li>
                <li id="licapkwcount" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/capkwcount.aspx" class="auto"><i class="i i-dot"></i><span>SystemCapKW Report</span> </a></li>
                <li id="listatustrack" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/statustrack.aspx" class="auto"><i class="i i-dot"></i><span>Status Track Report</span> </a></li>
                <li id="liinvoicesalesreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/invoicesalesreport.aspx" class="auto"><i class="i i-dot"></i><span>Invoice Sales Report</span> </a></li>
                <li id="liinstdocsreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/instdocsreport.aspx" class="auto"><i class="i i-dot"></i><span>Installer Docs</span> </a></li>
                <li id="liactivereport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/activereport.aspx" class="auto"><i class="i i-dot"></i><span>Active Report</span> </a></li>
                <li id="lipaninvreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/paninvreport.aspx" class="auto"><i class="i i-dot"></i><span>Panel Inv. Report</span> </a></li>
                <%--<li id="liformbaydocsreport" runat="server"><a href="<%=SiteURL%>admin/adminfiles/reports/formbaydocsreport.aspx" class="auto"><i class="i i-dot"></i><span>Formbay Docs Report</span> </a></li>--%>
                <li id="liorderinstalledreport" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/reports/orderinstalledreport.aspx" class="auto"><i class="i i-dot"></i><span>Order Installed Report</span> </a></li>
            </ul>
        </li>
        <li id="listocklistinst" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/stock/stocklist.aspx" class="auto"><i class="i icon leadtracker">
            <!-- <img src="<%=SiteURL%>images/icon_leadtracker.png">-->
        </i><span>Stock List</span> </a></li>
        <li id="lilteamcalendar" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/company/lteamcalendar.aspx" class="auto"><i class="i icon team">
            <!--<img src="<%=SiteURL%>images/icons/icon_team.png">-->
        </i><span>Team Calendar</span> </a></li>
        <li id="liinstallercalendar" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/installation/installercalendar.aspx" class="auto"><i class="i icon installation1">
            <!--<img src="<%=SiteURL%>images/icons/icon_installation.png">-->
        </i><span>Installer Calendar</span> </a></li>

        <%-- <li id="li1" runat="server" visible="false"><a href="<%=SiteURL%>admin/adminfiles/contacts/allcontacts.aspx" class="auto"><i class="i icon contact">
            <!--<img src="<%=SiteURL%>images/icon_contacts.png">-->
        </i><span>Contacts</span> </a></li>--%>
    </ul>
    <div class="line dk hidden-nav-xs"></div>
    <%-- <div class="text-muted text-xs hidden-nav-xs padder m-t-sm m-b-sm">Contacts</div>--%>
    <ul class="nav">
        <li id="li1" runat="server"><a href="<%=SiteURL%>admin/adminfiles/contacts/allcontacts.aspx" class="auto"><i class="i i-circle-sm text-info-dk"></i><span>Contacts</span> </a></li>
    </ul>
    <ul class="nav brdnonebtm">
        <%-- <li><a href="#"><i class="i i-circle-sm text-danger-dk"></i><span>Activity Stream</span> </a></li>
        <li><a href="conversation.html"><i class="i i-circle-sm text-info-dk"></i><span>Conversation</span> </a></li>
        <li><a href="calendar.html"><i class="i i-circle-sm text-warning-dk"></i><span>Calendar</span> </a></li>
        <li><a href="#"><i class="i i-circle-sm text-muted-dk"></i><span>My Task</span> </a></li>--%>
    </ul>
    <div class="navicopy">
        <span class="copytext iconmegh">&copy; 2014 Euro Solar. All rights reserved.<br>
            Designed by: <a href="http://www.meghtechnologies.com/" target="_blank">&nbsp;</a></span>
    </div>
</nav>
