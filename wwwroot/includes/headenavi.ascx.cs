﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class includes_headenavi : System.Web.UI.UserControl
{
    protected string SiteURL;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stuser = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stuser.EmployeeID;
        string date = string.Format("{0:mm/dd/yyyy}", DateTime.Now.AddHours(14).ToShortDateString());
        //if (!Roles.IsUserInRole("Installer"))
        //{
        //    //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        //    if (stuser.EmpImage != string.Empty)
        //    {
        //        imguser.ImageUrl = "~/userfiles/EmployeeProfile/" + stuser.EmpImage;
        //    }
        //    else
        //    {
        //        imguser.ImageUrl = "~/images/anynomous.png";
        //    }
        //}
        //lblusername.Text = stuser.EmpFirst + " " + stuser.EmpLast;

        if (Roles.IsUserInRole("SalesRep"))
        {
            ///dtfollwoup = ClstblCustInfo.tblCustInfo_SelectByDate(date, EmployeeID);
        }
        if (Roles.IsUserInRole("Administrator"))
        {
            EmployeeID = "";
            liformbaydocsreport.Visible = true;
            lipromotracker.Visible = true;
            lioutstandingreport.Visible = true;
            lipromosend.Visible = true;
            listockorderreport.Visible = true;
            liorderinstalledreport.Visible = true;
            lilteamtracker.Visible = true;
        }

        //DataTable dtfollwoup = ClstblCustInfo.tblCustInfo_SelectByDate(date, date, EmployeeID, "", "", "", "");
        //if (dtfollwoup.Rows.Count > 0)
        //{
        //    lblFollowupcount.Text = dtfollwoup.Rows.Count.ToString();
        //    lblFollowupcount.Visible = true;
        //}
        //else
        //{
        //    lblFollowupcount.Visible = false;
        //}

        if (Roles.IsUserInRole("Administrator"))
        {
            limaster.Visible = true;
            liinstallation.Visible = true;
            listock.Visible = true;
            liProjectsView.Visible = true;
            //liFollowUpsView.Visible = true;
            liinvoicesissued.Visible = true;
            liinvoicespaid.Visible = true;
            liUploadLead.Visible = true;
            licompany.Visible = true;

            listctracker.Visible = true;

            licasualmtce.Visible = true;
            liReports.Visible = true;
            lileadtracker.Visible = true;
            lilteamcalendar.Visible = true;
            liinstallercalendar.Visible = true;
            listocktransferreport.Visible = true;
            lioutstandingreport.Visible = true;
        }
        if (Roles.IsUserInRole("Finance"))
        {
            //licompany.Visible = false;
            //licontacts.Visible = false;
            //listock.Visible = false;
            liProjectsView.Visible = true;

        }
        if (Roles.IsUserInRole("SalesRep"))
        {
            liProjectsView.Visible = true;
            //liFollowUpsView.Visible = true;
            lileadtracker.Visible = true;
            lileadtrackersub.Visible = true;
            lilteamtracker.Visible = false;
            listctracker.Visible = false;
            liinstinvoice.Visible = false;
            lisalesinvoice.Visible = false;
            liinstalltracker.Visible = false;
            liaccrefund.Visible = false;
            //liformbay.Visible = false;

            liReports.Visible = true;
            ulreports.Attributes.Add("class", "nav dk always-visible");
            liactivereport.Visible = true;
            linoinstalldate.Visible = false;
            liinstalldate.Visible = false;
            lipanelscount.Visible = false;
            liaccountreceive.Visible = false;
            lipaymentstatus.Visible = false;
            liweeklyreport.Visible = false;
            lileadassignreport.Visible = false;
            listockreport.Visible = false;
            lileadtrackreport.Visible = false;
            lipanelstock.Visible = false;
            lirooftypecount.Visible = false;
            licapkwcount.Visible = false;
            listatustrack.Visible = false;
            liinvoicesalesreport.Visible = false;
            liinstdocsreport.Visible = false;
            lipaninvreport.Visible = false;
            liformbaydocsreport.Visible = false;
        }
        if (Roles.IsUserInRole("DSalesRep"))
        {
            liProjectsView.Visible = true;
            //liFollowUpsView.Visible = true;
        }
        if (Roles.IsUserInRole("Sales Manager"))
        {
            liProjectsView.Visible = true;
            //liFollowUpsView.Visible = true;
            lileadtracker.Visible = true;
            lileadtrackersub.Visible = true;
            lilteamtracker.Visible = false;
            listctracker.Visible = false;
            liinstinvoice.Visible = false;
            lisalesinvoice.Visible = false;
            liinstalltracker.Visible = false;
            liaccrefund.Visible = true;
            //liformbay.Visible = false;

            liReports.Visible = true;
            ulreports.Attributes.Add("class", "nav dk always-visible");
            liactivereport.Visible = true;
            linoinstalldate.Visible = false;
            liinstalldate.Visible = false;
            lipanelscount.Visible = false;
            liaccountreceive.Visible = false;
            lipaymentstatus.Visible = false;
            liweeklyreport.Visible = false;
            lileadassignreport.Visible = false;
            listockreport.Visible = false;
            lileadtrackreport.Visible = false;
            lipanelstock.Visible = false;
            lirooftypecount.Visible = false;
            licapkwcount.Visible = false;
            listatustrack.Visible = false;
            liinvoicesalesreport.Visible = false;
            liinstdocsreport.Visible = false;
            lipaninvreport.Visible = false;
            liformbaydocsreport.Visible = false;
        }
        if (Roles.IsUserInRole("DSales Manager"))
        {
            liProjectsView.Visible = true;
            //liFollowUpsView.Visible = true;
            listctracker.Visible = false;
            lileadtracker.Visible = true;
            lileadtrackersub.Visible = true;
            lilteamtracker.Visible = false;
            liinstinvoice.Visible = false;
            lisalesinvoice.Visible = false;
            liinstalltracker.Visible = false;
            liaccrefund.Visible = true;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            liinstallation.Visible = true;
            listctracker.Visible = true;
            licompany.Visible = false;
            liProjectsView.Visible = false;
            listocklistinst.Visible = true;
            licontacts.Visible = false;
            liinstavailable.Visible = true;
        }
        if (Roles.IsUserInRole("CompanyManager"))
        {
            licompany.Visible = true;
            liProjectsView.Visible = true;
            licontacts.Visible = true;
            lileadtracker.Visible = true;
            lileadtrackersub.Visible = true;
            lilteamtracker.Visible = false;
            listctracker.Visible = false;
            liinstinvoice.Visible = false;
            lisalesinvoice.Visible = false;
            liinstalltracker.Visible = false;
            liaccrefund.Visible = false;
            liformbaydocsreport.Visible = false;
        }
        if (Roles.IsUserInRole("Installation Manager"))
        {
            liProjectsView.Visible = true;
            liinstallation.Visible = true;
            liinvoicesissued.Visible = true;
            liinvoicespaid.Visible = true;
            listock.Visible = true;
            //liFollowUpsView.Visible = true;
            licasualmtce.Visible = true;
            listctracker.Visible = true;
            liReports.Visible = true;

            lileadtracker.Visible = true;
            lileadtrackersub.Visible = false;
            lilteamtracker.Visible = false;
            //listctracker.Visible = false;
            liinstinvoice.Visible = true;
            lisalesinvoice.Visible = false;
            liinstalltracker.Visible = true;
            liaccrefund.Visible = true;
            //liformbay.Visible = false;
            liformbaydocsreport.Visible = true;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            licompany.Visible = true;
            licontacts.Visible = true;
            liProjectsView.Visible = true;
            liinstallation.Visible = true;
            //liFollowUpsView.Visible = true;
            lileadtracker.Visible = true;
            lileadtrackersub.Visible = false;
            lilteamtracker.Visible = false;
            listctracker.Visible = false;
            liinstinvoice.Visible = false;
            lisalesinvoice.Visible = false;
            liinstalltracker.Visible = true;
            liaccrefund.Visible = false;
            //liformbay.Visible = false;
            liinstallercalendar.Visible = true;
            liformbaydocsreport.Visible = true;
            listock.Visible = true;
            listockitem.Visible = false;
            listocktransfer.Visible = false;
            listockorder.Visible = false;
            listockusage.Visible = false;
            listocktransferreport.Visible = false;
            listockorderreport.Visible = false;
            listockreport.Visible = false;
            listockdeduct.Visible = true;

        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            licompany.Visible = true;
            licontacts.Visible = true;
            liProjectsView.Visible = true;
            liinstallation.Visible = true;
            //liFollowUpsView.Visible = true;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            licompany.Visible = true;
            licontacts.Visible = true;
            liProjectsView.Visible = true;
            //liFollowUpsView.Visible = true;
            liinvoicesissued.Visible = true;
            liinvoicespaid.Visible = true;
            liformbaydocsreport.Visible = true;
        }
        if (Roles.IsUserInRole("Maintenance"))
        {
            licompany.Visible = true;
            licontacts.Visible = true;
            liProjectsView.Visible = true;
            liinstallation.Visible = true;
            licasualmtce.Visible = true;
        }
        if (Roles.IsUserInRole("STC"))
        {
            licompany.Visible = true;
            licontacts.Visible = true;
            liProjectsView.Visible = true;
            //liFollowUpsView.Visible = true;
            listctracker.Visible = true;

            lileadtracker.Visible = true;
            lileadtrackersub.Visible = false;
            lilteamtracker.Visible = false;
            listctracker.Visible = true;
            liinstinvoice.Visible = false;
            lisalesinvoice.Visible = false;
            liinstalltracker.Visible = false;
            liaccrefund.Visible = false;
            //liformbay.Visible = false;
        }
        if (Roles.IsUserInRole("Verification"))
        {
            licompany.Visible = true;
            licontacts.Visible = true;
            liProjectsView.Visible = true;
            liinvoicesissued.Visible = true;
            liinvoicespaid.Visible = true;
            liinstallation.Visible = true;
            listock.Visible = true;
            listctracker.Visible = true;
        }
        if (Roles.IsUserInRole("Accountant"))
        {
            lioutstandingreport.Visible = true;
            licompany.Visible = true;
            licontacts.Visible = true;
            liProjectsView.Visible = true;
            liinstallation.Visible = true;

            listocktransferreport.Visible = true;
            listockorderreport.Visible = true;

            lileadtracker.Visible = true;
            lileadtrackersub.Visible = false;
            lilteamtracker.Visible = false;
            listctracker.Visible = true;
            liinstinvoice.Visible = true;
            lisalesinvoice.Visible = true;
            liinstalltracker.Visible = false;
            liaccrefund.Visible = true;
            //liformbay.Visible = false;

            liReports.Visible = true;
            ulreports.Attributes.Add("class", "nav dk always-visible");
            liactivereport.Visible = false;
            linoinstalldate.Visible = false;
            liinstalldate.Visible = false;
            lipanelscount.Visible = false;
            liaccountreceive.Visible = false;
            lipaymentstatus.Visible = false;
            liweeklyreport.Visible = false;
            lileadassignreport.Visible = false;
            listockreport.Visible = true;
            lileadtrackreport.Visible = false;
            lipanelstock.Visible = false;
            lirooftypecount.Visible = false;
            licapkwcount.Visible = false;
            listatustrack.Visible = false;
            liinvoicesalesreport.Visible = true;
            liinstdocsreport.Visible = false;
            lipaninvreport.Visible = false;

            liformbaydocsreport.Visible = true;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            licompany.Visible = false;
            licontacts.Visible = false;
            liProjectsView.Visible = false;
            licustomerdetail.Visible = true;
        }
        if (Roles.IsUserInRole("WarehouseManager"))
        {
            listock.Visible = true;
            licontacts.Visible = true;
            licompany.Visible = true;
            liinstallation.Visible = true;
            listockorder.Visible = false;
            listocktransferreport.Visible = true;
            listockreport.Visible = true;
        }
        if (Roles.IsUserInRole("Purchase Manager"))
        {
            listock.Visible = true;
            licontacts.Visible = true;
            licompany.Visible = true;
            liReports.Visible = true;
            liactivereport.Visible = false;
            linoinstalldate.Visible = false;
            liinstalldate.Visible = false;
            lipanelscount.Visible = false;
            liaccountreceive.Visible = false;
            lipaymentstatus.Visible = false;
            liweeklyreport.Visible = false;
            lileadassignreport.Visible = false;
            listockreport.Visible = true;
            lileadtrackreport.Visible = false;
            lipanelstock.Visible = false;
            lirooftypecount.Visible = false;
            licapkwcount.Visible = false;
            listatustrack.Visible = false;
            liinvoicesalesreport.Visible = false;
            liinstdocsreport.Visible = false;
            lipaninvreport.Visible = false;
            liformbaydocsreport.Visible = false;
            lipaninvreport.Visible = true;
            listockorderreport.Visible = true;
        }
        if (stuser.SalesTeamID != string.Empty)
        {
            if (Roles.IsUserInRole("Lead Manager"))
            {
                liProjectsView.Visible = true;
                listctracker.Visible = false;
                //liFollowUpsView.Visible = false;
                lileadtracker.Visible = true;
                lileadtrackersub.Visible = false;
                lilteamtracker.Visible = true;
                listctracker.Visible = false;
                liinstinvoice.Visible = false;
                lisalesinvoice.Visible = false;
                liinstalltracker.Visible = false;
                liaccrefund.Visible = false;
                //liformbay.Visible = false;

                lilteamcalendar.Visible = true;
            }
            if (Roles.IsUserInRole("leadgen"))
            {
                lidashboard.Visible = true;
                licompany.Visible = false;
                licontacts.Visible = false;
                liProjectsView.Visible = false;
            }
        }
        else
        {
            if (Roles.IsUserInRole("Administrator"))
            {
                hrefleadtracker.HRef = SiteURL + "admin/adminfiles/company/leadtracker.aspx";
            }
        }

        if (Roles.IsUserInRole("leadgen") || Roles.IsUserInRole("Lead Manager"))
        {
            licompany.Visible = true;
            acompany.HRef = SiteURL + "admin/adminfiles/company/Ecompany.aspx";
        }
        else
        {
            acompany.HRef = SiteURL + "admin/adminfiles/company/company.aspx";
        }

        if (Roles.IsUserInRole("DSalesRep"))
        {
            liProjectsView.Visible = true;
        }

        if (Roles.IsUserInRole("Installer"))
        {
            hrefstctracker.HRef = SiteURL + "admin/adminfiles/invoice/instinvoice.aspx";
        }
        else
        {
            hrefstctracker.HRef = SiteURL + "admin/adminfiles/company/stctracker.aspx";
        }
        if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("Customer"))
        {

        }
        else
        {
            if (stuser.username != string.Empty)
            {
                if (stuser.username.ToLower() == "karen")
                {
                    licompany.Visible = false;
                    licontacts.Visible = false;
                }
            }
        }
        if (stuser.username != string.Empty)//SubAdministrator
        {
               // Roles.CreateRole("SubAdministrator");
                if (Roles.IsUserInRole("SubAdministrator"))
                {
                //if (stuser.username.ToLower() == "rvala" || stuser.username.ToLower() == "dharmik")
                //{

                //licompanylocations.Visible = false;
                //licustsource.Visible = false;
                //licustsourcesub.Visible = false;
                //lielecdistributor.Visible = false;
                //lielecretailer.Visible = false;
                //liemployee.Visible = false;
                //lihousetype.Visible = false;
                //lifinancewith.Visible = false;
                //licusttype.Visible = false;
                //limaster.Visible = true;
                //lipromosend.Visible = false;
                //lileadcancelreason.Visible = false;
                //limtcereason.Visible = false;
                //lilteamtracker.Visible = false;
                //liprojecttypes.Visible = false;
                //listctracker.Visible = false;
                //liinstinvoice.Visible = false;
                //lisalesinvoice.Visible = false;
                //liprojectcancel.Visible = false;
                //liprospectcategory.Visible = false;
                //liroofangles.Visible = false;
                //liprojecttrackers.Visible = false;
                //liinstalltracker.Visible = false;
                //lipaymenttype.Visible = false;
                //licustinstusers.Visible = false;
                //liprojectstatus.Visible = false;
                //liaccrefund.Visible = false;
                //litransactiontypes.Visible = false;
                //lisalesteams.Visible = false;
                //liformbaydocsreport.Visible = false;
                //liprojectonhold.Visible = false;
                //liinstallation.Visible = false;
                //liinvoicesissued.Visible = false;
                //lipromotiontype.Visible = false;
                //liinvoicespaid.Visible = false;
                //liprojectvariations.Visible = false;
                //listock.Visible = false;
                //limtcereasonsub.Visible = false;
                //licasualmtce.Visible = false;
                //liReports.Visible = false;
                //listockcategory.Visible = false;
                //liroottye.Visible = false;
                //lilteamcalendar.Visible = false;
                //liinstallercalendar.Visible = false;
                //lipromooffer.Visible = false;
                //li1.Visible = false;
                //liinvcommontype.Visible = false;
                //lipostcodes.Visible = false;
                //linewsletter.Visible = false;
                //lilogintracker.Visible = false;
                //lilteamtime.Visible = false;
                //lirefundoptions.Visible = false;
                //liupdateformbayId.Visible = false;
                //lipickup.Visible = false;
                //listreettype.Visible = true;
                //listreetname.Visible = true;
                //lileftempprojects.Visible = true;
                //liunittype.Visible = true;

                licompanylocations.Visible = false;
                licustsource.Visible = false;
                licustsourcesub.Visible = false;
                lielecdistributor.Visible = false;
                lielecretailer.Visible = false;
                liemployee.Visible = false;
                lihousetype.Visible = false;
                lifinancewith.Visible = false;
                licusttype.Visible = false;
                limaster.Visible = true;
                lipromosend.Visible = false;
                lileadcancelreason.Visible = false;
                limtcereason.Visible = false;
                lilteamtracker.Visible = false;
                liprojecttypes.Visible = false;
                listctracker.Visible = false;
                liinstinvoice.Visible = false;
                lisalesinvoice.Visible = false;
                liprojectcancel.Visible = false;
                liprospectcategory.Visible = false;
                liroofangles.Visible = false;
                liprojecttrackers.Visible = false;
                liinstalltracker.Visible = false;
               // ligridconnectiontracker.Visible = false;
                lipaymenttype.Visible = false;
                licustinstusers.Visible = false;
                liprojectstatus.Visible = false;
                liaccrefund.Visible = false;
                litransactiontypes.Visible = false;
                lisalesteams.Visible = false;
                liformbaydocsreport.Visible = false;
                liprojectonhold.Visible = false;
                liinstallation.Visible = false;
                liinvoicesissued.Visible = false;
                lipromotiontype.Visible = false;
                liinvoicespaid.Visible = false;
                liprojectvariations.Visible = false;
                listock.Visible = false;
                limtcereasonsub.Visible = false;
                licasualmtce.Visible = false;
                liReports.Visible = false;
                listockcategory.Visible = false;
                liroottye.Visible = false;
                lilteamcalendar.Visible = false;
                liinstallercalendar.Visible = false;
                lipromooffer.Visible = false;
                li1.Visible = false;
                liinvcommontype.Visible = false;
                lipostcodes.Visible = true;
                linewsletter.Visible = false;
                lilogintracker.Visible = false;
                lilteamtime.Visible = false;
                lirefundoptions.Visible = false;
                liupdateformbayId.Visible = true;
                lipickup.Visible = false;
                listreettype.Visible = true;
                listreetname.Visible = true;
                lileftempprojects.Visible = true;
                liunittype.Visible = true;
            }
               // }
            
        }


        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery).ToLower();
        string foldername = string.Empty;
       // Utilities.GetPageFolderName(Request.Url.PathAndQuery).ToLower();


        if (pagename == "dashboard" || pagename == "mydashboard")
        {
            lidashboard.Attributes.Add("class", "active");
        }
        else if (pagename == "company")
        {
            licompany.Attributes.Add("class", "active");
        }
        else if ((pagename == "contacts") || (pagename == "promotracker"))
        {
            licontacts.Attributes.Add("class", "active");
        }
        else if (pagename == "leadtracker" || pagename == "lteamtracker" || pagename == "stctracker" || pagename == "instinvoice" || pagename == "salesinvoice" || pagename == "pvdstatus" || pagename == "installbookingtracker" || pagename == "accrefund" || pagename == "formbay")
        {
            lileadtracker.Attributes.Add("class", "active");
        }
        else if (pagename == "installations" || pagename == "printinstallation")
        {
            liinstallation.Attributes.Add("class", "active");
        }
        else if (pagename == "invoicesissued")
        {
            liinvoicesissued.Attributes.Add("class", "active");
        }
        else if (pagename == "invoicespaid")
        {
            liinvoicespaid.Attributes.Add("class", "active");
        }
        else if ((pagename == "project") && (foldername == "view"))
        {
            liProjectsView.Attributes.Add("class", "active");
        }
        else if (pagename == "stockitem" || pagename == "stockorder" || pagename == "stocktransfer" || pagename == "additemqty" || pagename == "stockdeduct" || pagename == "stockusage" || pagename == "order" || pagename == "transfer" || pagename == "usage")
        {
            listock.Attributes.Add("class", "active");
        }
        else if ((pagename == "company") && (foldername == "view"))
        {
            liCompanyView.Attributes.Add("class", "active");
        }
        else if (pagename == "followups")
        {
            liFollowUpsView.Attributes.Add("class", "active");
        }
        else if (pagename == "stctracker" || pagename == "pvdstatus" || pagename == "instinvoice" || pagename == "salesinvoice")
        {
            listctracker.Attributes.Add("class", "active");
        }
        else if (pagename == "lead")
        {
            liUploadLead.Attributes.Add("class", "active");
        }
        else if (((foldername == "master") && ((pagename == "companylocations") || (pagename == "custsource") || (pagename == "custsourcesub") || (pagename == "custtype") || (pagename == "elecdistributor") || (pagename == "elecretailer") || (pagename == "employee") || (pagename == "empdetails") || (pagename == "emppermissions") || (pagename == "empreferences") || (pagename == "emptitle") || (pagename == "financewith") || (pagename == "housetype") || (pagename == "leadcancelreason") || (pagename == "mtcereason") || (pagename == "mtcereasonsub") || (pagename == "projectcancel") || (pagename == "projectonhold") || (pagename == "projectstatus") || (pagename == "projecttrackers") || (pagename == "projecttypes") || (pagename == "projectvariations") || (pagename == "promotiontype") || (pagename == "prospectcategory") || (pagename == "roofangles") || (pagename == "roottye") || (pagename == "salesteams") || (pagename == "stockcategory") || (pagename == "transactiontypes") || (pagename == "paymenttype") || (pagename == "invcommontype") || (pagename == "postcodes") || (pagename == "custinstusers") || (pagename == "newsletter") || (pagename == "logintracker") || (pagename == "lteamtime"))) || (pagename == "leftempprojects") || (pagename == "refundoptions"))
        {
            limaster.Attributes.Add("class", "active");
        }
        else if ((pagename == "casualmtce") || (pagename == "mtcecall") || (pagename == "invcommon") || (pagename == "updateinvcomm"))
        {
            licasualmtce.Attributes.Add("class", "active");
        }
        else if ((pagename == "salesreport") || (pagename == "noinstalldate") || (pagename == "installdate") || (pagename == "panelscount") || (pagename == "accountreceive") || (pagename == "paymentstatus") || (pagename == "weeklyreport") || (pagename == "leadassignreport") || (pagename == "leadtrackreport") || (pagename == "stockreport") || (pagename == "panelstock") || (pagename == "rooftypecount") || (pagename == "capkwcount") || (pagename == "statustrack") || (pagename == "invoicesalesreport") || (pagename == "instdocsreport") || (pagename == "paninvreport") || (pagename == "activereport"))
        {
            liReports.Attributes.Add("class", "active");
        }
        else if (pagename == "stocklist")
        {
            listocklistinst.Attributes.Add("class", "active");
        }
        else if (pagename == "customerdetail" || pagename == "custproject")
        {
            licustomerdetail.Attributes.Add("class", "active");
        }
        else if (pagename == "instavailable")
        {
            liinstavailable.Attributes.Add("class", "active");
        }
        else if (pagename == "lteamcalendar")
        {
            lilteamcalendar.Attributes.Add("class", "active");
        }
        else if (pagename == "installercalendar")
        {
            liinstallercalendar.Attributes.Add("class", "active");
        }


        if (pagename == "companylocations")
        {
            licompanylocations.Attributes.Add("class", "selected");
        }
        if (pagename == "custsource")
        {
            licustsource.Attributes.Add("class", "selected");
        }
        if (pagename == "custsourcesub")
        {
            licustsourcesub.Attributes.Add("class", "selected");
        }
        if (pagename == "custtype")
        {
            licusttype.Attributes.Add("class", "selected");
        }
        if (pagename == "elecdistributor")
        {
            lielecdistributor.Attributes.Add("class", "selected");
        }
        if (pagename == "elecretailer")
        {
            lielecretailer.Attributes.Add("class", "selected");
        }
        if (pagename == "employee" || pagename == "empdetails" || pagename == "emppermissions" || pagename == "empreferences")
        {
            liemployee.Attributes.Add("class", "selected");
        }
        if (pagename == "financewith")
        {
            lifinancewith.Attributes.Add("class", "selected");
        }
        if (pagename == "pickup")
        {
            lipickup.Attributes.Add("class", "selected");
        }
        if (pagename == "unittype")
        {
            liunittype.Attributes.Add("class", "selected");
        }
        if (pagename == "streettype")
        {
            listreettype.Attributes.Add("class", "selected");
        }
        if (pagename == "promooffer")
        {
            lipromooffer.Attributes.Add("class", "selected");
        }
        if (pagename == "housetype")
        {
            lihousetype.Attributes.Add("class", "selected");
        }
        if (pagename == "leadcancelreason")
        {
            lileadcancelreason.Attributes.Add("class", "selected");
        }
        if (pagename == "mtcereason")
        {
            limtcereason.Attributes.Add("class", "selected");
        }
        if (pagename == "mtcereasonsub")
        {
            limtcereasonsub.Attributes.Add("class", "selected");
        }
        if (pagename == "projectcancel")
        {
            liprojectcancel.Attributes.Add("class", "selected");
        }
        if (pagename == "projectonhold")
        {
            liprojectonhold.Attributes.Add("class", "selected");
        }
        if (pagename == "projectstatus")
        {
            liprojectstatus.Attributes.Add("class", "selected");
        }
        if (pagename == "projecttrackers")
        {
            liprojecttrackers.Attributes.Add("class", "selected");
        }
        if (pagename == "projecttypes")
        {
            liprojecttypes.Attributes.Add("class", "selected");
        }
        if (pagename == "projectvariations")
        {
            liprojectvariations.Attributes.Add("class", "selected");
        }
        if (pagename == "promotiontype")
        {
            lipromotiontype.Attributes.Add("class", "selected");
        }
        if (pagename == "prospectcategory")
        {
            liprospectcategory.Attributes.Add("class", "selected");
        }
        if (pagename == "roofangles")
        {
            liroofangles.Attributes.Add("class", "selected");
        }
        if (pagename == "roottye")
        {
            liroottye.Attributes.Add("class", "selected");
        }
        if (pagename == "salesteams")
        {
            lisalesteams.Attributes.Add("class", "selected");
        }
        if (pagename == "stockcategory")
        {
            listockcategory.Attributes.Add("class", "selected");
        }
        if (pagename == "transactiontypes")
        {
            litransactiontypes.Attributes.Add("class", "selected");
        }
        if (pagename == "paymenttype")
        {
            lipaymenttype.Attributes.Add("class", "selected");
        }
        if (pagename == "invcommontype")
        {
            liinvcommontype.Attributes.Add("class", "selected");
        }
        if (pagename == "postcodes")
        {
            lipostcodes.Attributes.Add("class", "selected");
        }
        if (pagename == "custinstusers")
        {
            licustinstusers.Attributes.Add("class", "selected");
        }
        if (pagename == "newsletter")
        {
            linewsletter.Attributes.Add("class", "selected");
        }
        if (pagename == "logintracker")
        {
            lilogintracker.Attributes.Add("class", "selected");
        }
        if (pagename == "leftempprojects")
        {
            lileftempprojects.Attributes.Add("class", "selected");
        }
        if (pagename == "lteamtime")
        {
            lilteamtime.Attributes.Add("class", "selected");
        }
        if (pagename == "refundoptions")
        {
            lirefundoptions.Attributes.Add("class", "selected");
        }
        if (pagename == "updateformbayId")
        {
            liupdateformbayId.Attributes.Add("class", "selected");
        }
        if (pagename == "accrefund")
        {
            liaccrefund.Attributes.Add("class", "selected");
        }
        if (pagename == "leadtracker")
        {
            lileadtrackersub.Attributes.Add("class", "selected");
        }
        if (pagename == "lteamtracker")
        {
            lilteamtracker.Attributes.Add("class", "selected");
        }
        if (pagename == "stctracker")
        {
            listctracker.Attributes.Add("class", "selected");
        }
        if (pagename == "instinvoice")
        {
            liinstinvoice.Attributes.Add("class", "selected");
        }
        if (pagename == "salesinvoice")
        {
            lisalesinvoice.Attributes.Add("class", "selected");
        }
        if (pagename == "installbookingtracker")
        {
            liinstalltracker.Attributes.Add("class", "selected");
        }
        if (pagename == "stockitem")
        {
            listockitem.Attributes.Add("class", "selected");
        }
        if (pagename == "stocktransfer")
        {
            listocktransfer.Attributes.Add("class", "selected");
        }
        if (pagename == "stockorder")
        {
            listockorder.Attributes.Add("class", "selected");
        }
        if (pagename == "stockdeduct")
        {
            listockdeduct.Attributes.Add("class", "selected");
        }
        if (pagename == "stockusage")
        {
            listockusage.Attributes.Add("class", "selected");
        }

        if (pagename == "noinstalldate")
        {
            linoinstalldate.Attributes.Add("class", "selected");
        }
        if (pagename == "installdate")
        {
            liinstalldate.Attributes.Add("class", "selected");
        }
        if (pagename == "panelscount")
        {
            lipanelscount.Attributes.Add("class", "selected");
        }
        if (pagename == "accountreceive")
        {
            liaccountreceive.Attributes.Add("class", "selected");
        }
        if (pagename == "paymentstatus")
        {
            lipaymentstatus.Attributes.Add("class", "selected");
        }
        if (pagename == "weeklyreport")
        {
            liweeklyreport.Attributes.Add("class", "selected");
        }
        if (pagename == "leadassignreport")
        {
            lileadassignreport.Attributes.Add("class", "selected");
        }
        if (pagename == "stockreport")
        {
            listockreport.Attributes.Add("class", "selected");
        }
        if (pagename == "stocktransferreport")
        {
            listocktransferreport.Attributes.Add("class", "selected");
        }

        if (pagename == "leadtrackreport")
        {
            lileadtrackreport.Attributes.Add("class", "selected");
        }
        if (pagename == "panelstock")
        {
            lipanelstock.Attributes.Add("class", "selected");
        }
        if (pagename == "rooftypecount")
        {
            lirooftypecount.Attributes.Add("class", "selected");
        }
        if (pagename == "capkwcount")
        {
            licapkwcount.Attributes.Add("class", "selected");
        }
        if (pagename == "statustrack")
        {
            listatustrack.Attributes.Add("class", "selected");
        }
        if (pagename == "invoicesalesreport")
        {
            liinvoicesalesreport.Attributes.Add("class", "selected");
        }
        if (pagename == "instdocsreport")
        {
            liinstdocsreport.Attributes.Add("class", "selected");
        }
        if (pagename == "activereport")
        {
            liactivereport.Attributes.Add("class", "selected");
        }
        if (pagename == "paninvreport")
        {
            lipaninvreport.Attributes.Add("class", "selected");
        }
        if (pagename == "formbaydocsreport")
        {
            liformbaydocsreport.Attributes.Add("class", "selected");
        }
        if (pagename == "promotracker")
        {
            lipromotracker.Attributes.Add("class", "selected");
        }

        if (pagename == "mtcecall")
        {
            limtcecall.Attributes.Add("class", "selected");
        }
        if (pagename == "casualmtce")
        {
            licasualmtcesub.Attributes.Add("class", "selected");
        }
        if (pagename == "formbaydocsreport")
        {
            liformbaydocsreport.Attributes.Add("class", "selected");
        }
        if (pagename == "outstandingreport")
        {
            lioutstandingreport.Attributes.Add("class", "selected");
        }
        if ((pagename == "promosend"))
        {
            lipromosend.Attributes.Add("class", "active");
        }




        DataTable dtemp = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(EmployeeID);
        if (dtemp.Rows.Count > 0)
        {
            foreach (DataRow dr in dtemp.Rows)
            {
                if (dr["SalesTeamID"].ToString() == "2" || dr["SalesTeamID"].ToString() == "5" || dr["SalesTeamID"].ToString() == "7" || dr["SalesTeamID"].ToString() == "8")
                {
                    li1.Visible = true;
                }
                else
                {
                    li1.Visible = false;
                }
            }
        }

    }
}