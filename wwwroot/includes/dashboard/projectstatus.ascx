﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectstatus.ascx.cs" Inherits="includes_dashboard_projectstatus" %>
<%@ Register Src="~/includes/dashboard/projectstatus1.ascx" TagPrefix="uc1" TagName="projectstatus1" %>
<%@ Register Src="~/includes/dashboard/projectstatus2.ascx" TagPrefix="uc1" TagName="projectstatus2" %>

<div class="col-md-6">
    <div class="panlenew">
        <section class="panel backnone2">
            <%--<div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-sm btn-rounded btn-default dropdown-toggle" data-toggle="dropdown"><span class="dropdown-label">Week</span> <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-select">
                            <li><a href="#">
                                <input type="radio" name="b">
                                Month</a></li>
                            <li><a href="#">
                                <input type="radio" name="b">
                                Week</a></li>
                            <li><a href="#"><input type="radio" name="b">Day</a></li>
                        </ul>
                    </div>
                    <a class="btn btn-default btn-icon btn-rounded btn-sm marginleft3" href="#">Go</a>
                </div>
                <a href="#">Project Status</a>
            </div>--%>
            
            <div class="panel-body" id="divMain" runat="server">
                <uc1:projectstatus1 runat="server" ID="projectstatus1" />
            </div>
            <div class="panel-body" id="divInst" runat="server">
                <uc1:projectstatus2 runat="server" ID="projectstatus2" />
            </div>
        </section>
    </div>
</div>

