﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_installer : System.Web.UI.UserControl
{
    protected string DepRecAmount;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtState = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptState.DataSource = dtState;
            rptState.DataBind();

            rptStateHid.DataSource = dtState;
            rptStateHid.DataBind();
            rptStateHid2.DataSource = dtState;
            rptStateHid2.DataBind();

            BindTodayInst();
            BindMtceInst();
            BindPendingInst();
        }
    }

    public void BindTodayInst()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblContacts st = ClstblContacts.tblContacts_StructByUserId(userid);
        string Installer = "0";
        if (Roles.IsUserInRole("Installer"))
        {
            Installer = st.ContactID;
        }
        else
        {
            Installer = "0";
        }
        divTodaysInst.Visible = true;
        if (!string.IsNullOrEmpty(Installer))
        {
            DataTable dt = ClsDashboard.tblInstallBookings_SelectToday(Installer);
            
            if (dt.Rows.Count == 0)
            {
                divTodaysInst.Visible = false;
            }
            else
            {
                divTodaysInst.Visible = true;
                GridViewTodayInst.DataSource = dt;
                GridViewTodayInst.DataBind();
            }
        }
    }
    protected void GridViewTodayInst_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewTodayInst.PageIndex = e.NewPageIndex;
        BindTodayInst();
    }
    protected void GridViewTodayInst_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {
            string[] strids = e.CommandArgument.ToString().Split('|');
            string projectid = strids[0];
            string companyid = strids[1];
            Profile.eurosolar.companyid = companyid;
            Profile.eurosolar.projectid = projectid;
            Profile.eurosolar.contactid = "";
            if (e.CommandName == "viewproject")
            {
                Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
            }
        }

        if (e.CommandName == "Done")
        {
            hndProjectID.Value = e.CommandArgument.ToString();
            ModalPopupExtenderDocs.Show();

            ListItem item4 = new ListItem();
            item4.Text = "Select";
            item4.Value = "";
            ddlPatmentMethod.Items.Clear();
            ddlPatmentMethod.Items.Add(item4);

            ddlPatmentMethod.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
            ddlPatmentMethod.DataValueField = "FPTransTypeID";
            ddlPatmentMethod.DataTextField = "FPTransType";
            ddlPatmentMethod.DataMember = "FPTransType";
            ddlPatmentMethod.DataBind();
        }
    }
    protected void rptStateHid_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblContacts st = ClstblContacts.tblContacts_StructByUserId(userid);

            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptDone = (Repeater)e.Item.FindControl("rptDone");

            string Installer = "0";
            if (Roles.IsUserInRole("Installer"))
            {
                Installer = st.ContactID;
            }
            else
            {
                Installer = "0";
            }
            //Response.Write(Installer);
            //Response.End();
            if (!string.IsNullOrEmpty(Installer))
            {
                DataTable dt = ClsDashboard.tblProjects_Installer_Done(hndState.Value, Installer);
                if (dt.Rows.Count > 0)
                {
                    rptDone.DataSource = dt;
                    rptDone.DataBind();
                }
            }
        }
    }
    protected void rptStateHid2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblContacts st = ClstblContacts.tblContacts_StructByUserId(userid);

            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptRemining = (Repeater)e.Item.FindControl("rptRemining");

            string Installer = "0";
            if (Roles.IsUserInRole("Installer"))
            {
                Installer = st.ContactID;
            }
            else
            {
                Installer = "0";
            }
            if (!string.IsNullOrEmpty(Installer))
            {
                DataTable dt2 = ClsDashboard.tblProjects_Installer_Remaining(hndState.Value, Installer);
                rptRemining.DataSource = dt2;
                rptRemining.DataBind();
            }
        }
    }

    public void BindMtceInst()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblContacts st = ClstblContacts.tblContacts_StructByUserId(userid);
        string Installer = "0";
        if (Roles.IsUserInRole("Installer"))
        {
            Installer = st.ContactID;
        }
        else
        {
            Installer = "0";
        }
        divMtceInst.Visible = true;
        if (!string.IsNullOrEmpty(Installer))
        {
            DataTable dt = ClsDashboard.tblProjectMaintenance_SelectToday(Installer);
            if (dt.Rows.Count > 0)
            {
                divMtceInst.Visible = true;
                GridViewMtce.DataSource = dt;
                GridViewMtce.DataBind();
            }
            else
            {
                divMtceInst.Visible = false;
            }
            DataTable dtCount = ClsDashboard.tblProjects_Installer_Amount(Installer);
            lblTotalPrice.Text = dtCount.Rows[0]["Total"].ToString();
            lblReceived.Text = dtCount.Rows[0]["DepRec"].ToString();
            lblOwing.Text = dtCount.Rows[0]["Owing"].ToString();
        }

    }
    protected void GridViewMtce_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {
            string[] strids = e.CommandArgument.ToString().Split('|');
            string projectid = strids[0];
            string companyid = strids[1];
            Profile.eurosolar.companyid = companyid;
            Profile.eurosolar.projectid = projectid;
            Profile.eurosolar.contactid = "";
            if (e.CommandName == "viewproject")
            {
                Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
            }
        }
        if (e.CommandName == "viewproject")
        {

        }
    }
    protected void GridViewMtce_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewMtce.PageIndex = e.NewPageIndex;
        BindMtceInst();
    }
    protected void ibtnUpdate_Click(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        string from = st.from;

        if (hndProjectID.Value != string.Empty)
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(hndProjectID.Value);
            bool success = ClsProjectSale.tblProjects_UpdateInstDocs(hndProjectID.Value, Convert.ToString(chkSTCFormSign.Checked), Convert.ToString(chkSerialNumbers.Checked), Convert.ToString(chkQuotationForm.Checked), Convert.ToString(chkCustomerAck.Checked), Convert.ToString(chkComplianceCerti.Checked), Convert.ToString(chkCustomerAccept.Checked), txtEWRNumber.Text, ddlPatmentMethod.SelectedValue);
            if (success)
            {
                TextWriter txtWriter = new StringWriter() as TextWriter;
                string subjectC = "Email from EURO SOLAR";
                Server.Execute("~/mailtemplate/doneinstdocs.aspx?ProjectNumber=" + stPro.ProjectNumber + "&Address=" + stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + " - " + stPro.InstallPostCode + "&STCFormSign=" + Convert.ToString(chkSTCFormSign.Checked) + "&SerialNumbers=" + Convert.ToString(chkSerialNumbers.Checked) + "&QuotationForm=" + Convert.ToString(chkQuotationForm.Checked) + "&CustomerAck=" + Convert.ToString(chkCustomerAck.Checked) + "&ComplianceCerti=" + Convert.ToString(chkComplianceCerti.Checked) + "&CustomerAccept=" + Convert.ToString(chkCustomerAccept.Checked) + "&EWRNumber=" + txtEWRNumber.Text.Trim() + "&PatmentMethod=" + ddlPatmentMethod.SelectedItem, txtWriter);
                try
                {
                    if (stPro.InstallState == "QLD")
                    {
                        Utilities.SendMail(from, "payments@eurosolar.com.au", subjectC, txtWriter.ToString(), "nishet@eurosolar.com.au");
                    }
                    if (stPro.InstallState == "VIC")
                    {
                        Utilities.SendMail(from, "soniyaa@eurosolar.com.au", subjectC, txtWriter.ToString(), "d.maharaj@eurosolar.com.au");
                    }
                    if (stPro.InstallState == "TAS")
                    {
                        Utilities.SendMail(from, "nishet@eurosolar.com.au", subjectC, txtWriter.ToString());
                    }
                }
                catch { }
            }
        }
    }

    public void BindPendingInst()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblContacts st = ClstblContacts.tblContacts_StructByUserId(userid);
        string Installer = "0";
        if (Roles.IsUserInRole("Installer"))
        {
            Installer = st.ContactID;
        }
        else
        {
            Installer = "0";
        }
        divPendingInst.Visible = true;
        if (!string.IsNullOrEmpty(Installer))
        {
            DataTable dt = ClsDashboard.tblInstallBookings_SelectPending(Installer);
            if (dt.Rows.Count > 0)
            {
                divPendingInst.Visible = true;
                grdPendingInst.DataSource = dt;
                grdPendingInst.DataBind();
            }
            else
            {
                divPendingInst.Visible = false;
            }

            DataTable dtCount = ClsDashboard.tblProjects_Installer_Amount(Installer);
            lblTotalPrice.Text = dtCount.Rows[0]["Total"].ToString();
            lblReceived.Text = dtCount.Rows[0]["DepRec"].ToString();
            lblOwing.Text = dtCount.Rows[0]["Owing"].ToString();


            DataTable dt1 = ClsDashboard.tblInstallBookings_SelectPendingBookingDate(Installer);
            if (dt1.Rows.Count > 0)
            {
                divpendinglist.Visible = true;
                Gridpendinglist.DataSource = dt1;
                Gridpendinglist.DataBind();
            }
            else
            {
                divpendinglist.Visible = false;
            }
        }
    }
    protected void grdPendingInst_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Done")
        {
            hndProjectID.Value = e.CommandArgument.ToString();
            ModalPopupExtenderDocs.Show();

            ListItem item4 = new ListItem();
            item4.Text = "Select";
            item4.Value = "";
            ddlPatmentMethod.Items.Clear();
            ddlPatmentMethod.Items.Add(item4);

            ddlPatmentMethod.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
            ddlPatmentMethod.DataValueField = "FPTransTypeID";
            ddlPatmentMethod.DataTextField = "FPTransType";
            ddlPatmentMethod.DataMember = "FPTransType";
            ddlPatmentMethod.DataBind();

            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(e.CommandArgument.ToString());
            try
            {
                chkSTCFormSign.Checked = Convert.ToBoolean(st.STCFormSign);
            }
            catch
            {
            }
            try
            {
                chkSerialNumbers.Checked = Convert.ToBoolean(st.SerialNumbers);
            }
            catch
            {
            }
            try
            {
                chkQuotationForm.Checked = Convert.ToBoolean(st.QuotationForm);
            }
            catch
            {
            }
            try
            {
                chkCustomerAck.Checked = Convert.ToBoolean(st.CustomerAck);
            }
            catch
            {
            }
            try
            {
                chkComplianceCerti.Checked = Convert.ToBoolean(st.ComplianceCerti);
            }
            catch
            {
            }
            try
            {
                chkCustomerAccept.Checked = Convert.ToBoolean(st.CustomerAccept);
            }
            catch
            {
            }


            txtEWRNumber.Text = st.EWRNumber;
            try
            {
                ddlPatmentMethod.SelectedValue = st.PatmentMethod;
            }
            catch { }
        }
    }
    protected void grdPendingInst_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdPendingInst.PageIndex = e.NewPageIndex;
        BindPendingInst();
    }
    protected void Gridpendinglist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Gridpendinglist.PageIndex = e.NewPageIndex;
        BindPendingInst();
    }
    protected void Gridpendinglist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "insbookingdate")
        {
            string projectid = e.CommandArgument.ToString();
            hndprojectID1.Value = projectid;
            ModalPopupExtender1.Show();
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        string ProjectID = hndprojectID1.Value;
        string InstallBookingDate = txtInstallBookingDate.Text.Trim();
        string InstallDays = txtInstallDays.Text;
        string InstallAM1 = Convert.ToString(rblAM1.Checked);
        string InstallAM2 = Convert.ToString(rblAM2.Checked);
        string InstallPM1 = Convert.ToString(rblPM1.Checked);
        string InstallPM2 = Convert.ToString(rblPM2.Checked);

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        ClsProjectSale.tblInstallBookings_Delete(ProjectID);

        if (txtInstallBookingDate.Text.Trim() != string.Empty && (rblAM1.Checked == true || rblAM2.Checked == true || rblPM1.Checked == true || rblPM2.Checked == true))
        {
            int InstallDay = 1;
            if (txtInstallDays.Text != string.Empty)
            {
                InstallDay = Convert.ToInt32(txtInstallDays.Text);
            }
            DateTime InstallDate = Convert.ToDateTime(txtInstallBookingDate.Text.Trim());
            for (int i = 0; i < InstallDay; i++)
            {
                string AMPM = "";
                if (rblAM1.Checked == true)
                {
                    AMPM = "AM1";
                    DateTime InstallDate1 = InstallDate.AddDays(i);
                    int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                }
                if (rblAM2.Checked == true)
                {
                    AMPM = "AM2";
                    DateTime InstallDate1 = InstallDate.AddDays(i);
                    int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                }
                if (rblPM1.Checked == true)
                {
                    AMPM = "PM1";
                    DateTime InstallDate1 = InstallDate.AddDays(i);
                    int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                }
                if (rblPM2.Checked == true)
                {
                    AMPM = "PM2";
                    DateTime InstallDate1 = InstallDate.AddDays(i);
                    int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                }
            }
            bool sucPreInst = false;
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string UpdatedBy = stEmp.EmployeeID;


            sucPreInst = ClsProjectSale.tblProjects_UpdatePreinstallation(ProjectID, InstallBookingDate, InstallDays, InstallAM1, InstallAM2, InstallPM1, InstallPM2);

            /* -------------------- Job Booked -------------------- */
            if (txtInstallBookingDate.Text.Trim() != string.Empty)
            {
                if (stPro.ProjectStatusID == "3")
                {
                    DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "11");
                    if (dtStatus.Rows.Count > 0)
                    {
                    }
                    else
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, stPro.EmployeeID, stPro.NumberPanels);
                    }
                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
                }
            }
        }

        /* -------------------- Installer Detail Mail -------------------- */

        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        string from = st.from;

        string Name = "";
        string curuser = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Installer"))
        {
            SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(curuser);
            Name = stCont.ContFirst + " " + stCont.ContLast;
        }
        else
        {
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(curuser);
            Name = stEmp.EmpFirst + " " + stEmp.EmpLast;
        }

        TextWriter txtWriter = new StringWriter() as TextWriter;
        string subjectC = Name + " Booked Installation for " + stPro.ProjectNumber;
        Server.Execute("~/mailtemplate/installdetail.aspx?ProjectNumber=" + stPro.ProjectNumber + "&Name=" + Name + "&Date=" + txtInstallBookingDate.Text.Trim(), txtWriter);
        try
        {
            if (stPro.InstallState == "QLD" || stPro.InstallState == "TAS")
            {
                Utilities.SendMail(from, "service.qld@eurosolar.com.au", subjectC, txtWriter.ToString(), "install.qld@eurosolar.com.au ; payments@eurosolar.com.au");
            }
            if (stPro.InstallState == "VIC")
            {
                Utilities.SendMail(from, "tarak.shah@eurosolar.com.au", subjectC, txtWriter.ToString(), "soniyaa@eurosolar.com.au ; solarconnection@eurosolar.com.au");
            }
        }
        catch { }

        txtInstallBookingDate.Text = String.Empty;
        txtInstallDays.Text = String.Empty;
        rblAM1.Checked = false;
        rblAM2.Checked = false;
        rblPM1.Checked = false;
        rblPM2.Checked = false;

        BindPendingInst();
    }
}