﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="postinstaller.ascx.cs" Inherits="includes_dashboard_postinstaller" %>
<div class="row">
    <div class="col-md-6">
        <div class="panel-heading">
            Pre Installer Count
        </div>
        <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss">
            <tr>
                <th>&nbsp;</th>
                <asp:Repeater ID="rptState" runat="server">
                    <ItemTemplate>
                        <th>
                            <%#Eval("State") %>
                        </th>
                    </ItemTemplate>
                </asp:Repeater>
            </tr>
            <tr>
                <td>Total</td>
                <asp:Repeater ID="rptStateHid" runat="server" OnItemDataBound="rptStateHid_ItemDataBound">
                    <ItemTemplate>
                        <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                        <asp:Repeater ID="rptDone" runat="server">
                            <ItemTemplate>
                                <td>
                                    <%#Eval("total") %>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </tr>
        </table>
    </div>
</div>
