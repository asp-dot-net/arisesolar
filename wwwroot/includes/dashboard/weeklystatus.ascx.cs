﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_weeklystatus : System.Web.UI.UserControl
{
    protected string jschart1;
    protected string endValue;
    protected string SiteURL;



    protected void Page_Load(object sender, EventArgs e)
    {
        //TextBox txt = this.Parent.FindControl("txttest") as TextBox;
        ////u can set ur value here
        //string idw = txt.Text;

        StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st1.siteurl;
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Lead Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                if (Roles.IsUserInRole("Sales Manager"))
                {
                    divemployee.Visible = true;
                    divemployee2.Visible = false;
                }
                else
                {
                    divemployee.Visible = false;
                    divemployee2.Visible = true;
                }
                if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
                {
                    string SalesTeam = "";
                    DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }


                    if (SalesTeam != string.Empty)
                    {
                        ddlFollowUpEmployees.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                    }
                }
                //else
                //{
                //    ddlFollowUpEmployees.DataSource = ClstblEmployees.tblEmployees_SelectASC();
                //}

                ddlFollowUpEmployees.DataMember = "fullname";
                ddlFollowUpEmployees.DataTextField = "fullname";
                ddlFollowUpEmployees.DataValueField = "EmployeeID";
                ddlFollowUpEmployees.DataBind();
                BindWeeklyStatus();
                BindStatus();
            }
        }
    }

    public void BindStatus()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = "0";
        if (ddlFollowUpEmployees.SelectedValue != "")
        {
            if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
            {
                EmployeeID = "0";
            }
            else
            {
                EmployeeID = ddlFollowUpEmployees.SelectedValue;
            }
        }
        else
        {
            EmployeeID = st.EmployeeID;
        }
        DataTable dt = ClstblProjects.tblProjects_CountAmount(EmployeeID);
        if (dt.Rows.Count > 0)
        {

            litTotal.Text = dt.Rows[0]["Total"].ToString();
            litDepRec.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["DepRec"].ToString());
            litActive.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["Active"].ToString());
        }
    }
    public void BindWeeklyStatus()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = "0";

        if (ddlFollowUpEmployees.SelectedValue != "")
        {
            if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
            {
                EmployeeID = "0";
            }
            else
            {
                EmployeeID = st.EmployeeID;
            }
        }
        else
        {
            EmployeeID = st.EmployeeID;
        }
        DataTable dt = ClstblProjects.tblProjects_WeeklyTarget(EmployeeID);
        if (dt.Rows.Count > 0)
        {
            lblTarget.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["Target"].ToString());
            lblAchieve.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["Achieve"].ToString());
            lblBalance.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["Balance"].ToString());
            lblLastWeekAchievements.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["LastWeek"].ToString());

            string chartvalue = "";
            foreach (DataRow row in dt.Rows)
            {
                chartvalue = lblTarget.Text + "," + lblAchieve.Text + "," + lblBalance.Text + "," + lblLastWeekAchievements.Text;
                endValue = lblTarget.Text;
            }
            jschart1 = chartvalue;
        }
    }
    protected void btnFollowupgo_Click(object sender, EventArgs e)
    {
        BindStatus();
        BindScript();
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(UpdateLead, this.GetType(), "MyAction", "doMyAction();", true);
    }

}