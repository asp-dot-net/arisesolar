﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_leadcount : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Lead Manager"))
            {
                if (Roles.IsUserInRole("Sales Manager"))
                {
                    divemployee.Visible = true;
                    divemployee2.Visible = false;
                }
                else
                {
                    divemployee.Visible = false;
                    divemployee2.Visible = true;
                }
                if (Roles.IsUserInRole("PreInstaller"))
                {
                    divPreInst.Visible = true;
                    divMain.Visible = false;
                }
                if (Roles.IsUserInRole("PostInstaller"))
                {
                    divPostInst.Visible = true;
                    divMain.Visible = false;
                }
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
                {
                    string SalesTeam = "";
                    DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st1.EmployeeID);
                    if (dt_empsale.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }


                    if (SalesTeam != string.Empty)
                    {
                        ddlFollowUpEmployees.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                        ddlFollowUpEmployees.DataMember = "fullname";
                        ddlFollowUpEmployees.DataTextField = "fullname";
                        ddlFollowUpEmployees.DataValueField = "EmployeeID";
                        ddlFollowUpEmployees.DataBind();
                    }
                }
                BindCountLead();
            }
        }
    }

    public void BindCountLead()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = "0";
        if (ddlFollowUpEmployees.SelectedValue != "")
        {
            if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
            {
                EmployeeID = "0";
            }
            else
            {
                //EmployeeID = st.EmployeeID;
                EmployeeID = ddlFollowUpEmployees.SelectedValue;
            }
        }
        else
        {
            EmployeeID = st.EmployeeID;
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep"))
        {
            //Response.Write(EmployeeID);
            //Response.End();
            DataTable dt = ClstblCustomers.tblCustomers_CountLeadStatus(EmployeeID);
            if (dt.Rows.Count > 0)
            {
                //litNewLead.Text = dt.Rows[0]["NewLead"].ToString();
                litUnhandledLead.Text = dt.Rows[0]["UnLead"].ToString();
                litProspect.Text = dt.Rows[0]["Prospect"].ToString();
                litCustomer.Text = dt.Rows[0]["Customer"].ToString();

                litDepRec.Text = dt.Rows[0]["DepRec"].ToString();
                litActive.Text = dt.Rows[0]["Active"].ToString();
                litJobBooked.Text = dt.Rows[0]["JobBooked"].ToString();
                litOnHold.Text = dt.Rows[0]["OnHold"].ToString();
                litJobBookedPost.Text = dt.Rows[0]["JobBooked"].ToString();
                litInstComplete.Text = dt.Rows[0]["InstComplete"].ToString();
                litPaperRec.Text = dt.Rows[0]["PaperRec"].ToString();
                litDueAmount.Text = dt.Rows[0]["DueAmount"].ToString();
                liTotal.Text = (Convert.ToDecimal(dt.Rows[0]["UnLead"].ToString()) + Convert.ToDecimal(dt.Rows[0]["Prospect"].ToString()) + Convert.ToDecimal(dt.Rows[0]["Customer"].ToString())).ToString();
            }
        }
    }

    protected void btnFollowupgo_Click(object sender, EventArgs e)
    {
        BindCountLead();
        BindScript();
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(UpdateLead, this.GetType(), "MyAction", "doMyAction();", true);
    }
    //protected void btnGo_Click(object sender, EventArgs e)
    //{
    //    BindCountLead();
    //}
}