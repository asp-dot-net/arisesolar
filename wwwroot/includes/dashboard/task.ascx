﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="task.ascx.cs" Inherits="includes_dashboard_task" %>
<div class="col-md-6">
    <div class="panlenew taskpanel">
        <section class="panel">
            <%--<div class="panel-heading"><span class="badge bg-warning bg-red pull-right">10</span> <a href="#">Task</a> </div>--%>
            <div class="panel-body">
                <h4><a href="#">Task</a><span class="badge bg-warning bg-red pull-right">10</span></h4>
                <ul class="taskli bglightbule fnt12">
                    <li class="task orengline">
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span></span></li>
                    <li class="task greenline">
                        <div>Fusce suscipit ex nibh, sit amet egestas justo ullamcorper at. </div>
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span></span></li>
                    <li class="task buleline">
                        <div>Fusce imperdiet quis turpis vel sodales. </div>
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span></span></li>
                    <li class="task pinkline">
                        <div>Integer vel odio efficitur, ornare purus id, gravida ante. </div>
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span></span></li>
                    <li class="task skybuleline">
                        <div>Donec commodo arcu non massa pretium bibendum. </div>
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span></span></li>
                    <li class="task blackline">
                        <div>Fusce non felis a risus aliquam scelerisque non a orci. </div>
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <span></span></li>
                </ul>
            </div>
        </section>
    </div>
</div>
