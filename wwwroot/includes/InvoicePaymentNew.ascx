﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InvoicePaymentNew.ascx.cs" Inherits="includes_InvoicePaymentNew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style>
    .overlay {
        position: absolute;
        background-color: #696969;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 350px;
        opacity: 0.8;
        -moz-opacity: 0.8;
        filter: alpha(opacity=80);
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
        z-index: 10000;
    }
</style>
<style>
    .modalbackground {
        background-color: Gray;
        opacity: 0.5;
        filter: Alpha(opacity=50);
    }

    .modalpopup {
        background-color: white;
        padding: 6px 6px 6px 6px;
    }

    table.formtable h3 {
        color: #800000;
        font-size: 16px;
    }
</style>
<style type="text/css">
    .modalPopup {
        background-color: #696969;
        filter: alpha(opacity=40);
        opacity: 0.7;
        xindex: -1;
    }

    .z_index_loader {
        z-index: 998 !important;
    }
</style>


<asp:UpdatePanel ID="UpdatePanel11" runat="server">
    <ContentTemplate>

        <style>
            table.table tr td, table.table tr th {
                overflow: visible;
            }
        </style>

        <script>
            function ComfirmDelete(event, ctl) {
                alert("Done");
                event.preventDefault();
                var defaultAction = $(ctl).prop("href");

                swal({
                    title: "Are you sure you want to delete this Record?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },

                    function (isConfirm) {
                        if (isConfirm) {
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            eval(defaultAction);

                            //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            return true;
                        } else {
                            // swal("Cancelled", "Your imaginary file is safe :)", "error");
                            return false;
                        }
                    });
            }
        </script>
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            }

            function pageLoadedpro() {

                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();

            }
        </script>

        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            function pageLoaded() {
                $(".myvalcomp").select2({
                    //placeholder: "select",
                    allowclear: true
                });
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.redreq').click(function () {
                    formValidate();
                });
                //$('.i-checks').iCheck({
                //    checkboxClass: 'icheckbox_square-green',
                //    radioClass: 'iradio_square-green'
                //});
            }
        </script>
        <div class="statuspopup">
            <div class="paddbtm10">

                <asp:Button ID="btnInvPay" runat="server" OnClick="btnInvPay_Click1" CssClass="btneditinvoice martopzero"
                    Text="Edit Invoice Payment" CausesValidation="false" />

                <asp:Button ID="btnNULLInvPay" Style="display: none;" runat="server" />
                <asp:HiddenField ID="hndProjectId" runat="server" />
            </div>
        </div>
        <cc1:ModalPopupExtender ID="ModalPopupExtenderInvPay" runat="server" BackgroundCssClass="modalbackground z_index_loader"
            DropShadow="false" PopupControlID="divAddInvPay" TargetControlID="btnNULLInvPay"
            CancelControlID="btnClose">
        </cc1:ModalPopupExtender>
        <div id="divAddInvPay" runat="server" style="display: none;" class="modal_popup">
            <div class="modal-content modaltable">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div style="float: right">
                        <button id="btnClose" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                    </div>
                    <h3 class="modal-title" id="myModalLabel">Invoice Details -
                        <asp:Literal runat="server" ID="ltcustdetail"></asp:Literal></h3>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body" style="width: 1100px; height: 575px; overflow: scroll;">
                        <div class="formainline">

                            <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                <%--   <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanel11">
                                    <ProgressTemplate>
                                        <div class="overlay">--%>
                                <%-- <asp:Image ID="imgloading" ImageUrl="~/admin/images/loading.gif" AlternateText="Processing"
                                                    runat="server" />--%>
                                <%-- </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                                <div class="">
                                    <div class="col-md-6">
                                        <span class="invoicenum"><strong>Invoice Number :
                                                <asp:Label ID="lblProjectNumber" runat="server"></asp:Label>
                                        </strong></span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="unpaidbill" align="right">
                                            <table border="0" cellspacing="0" cellpadding="0" align="right">
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="btnPrintReceipt" runat="server" CssClass="btn btn-primary btn-xs" CausesValidation="false"
                                                            OnClick="btnPrintReceipt_Click" ValidationGroup="print"><i class="fa fa-print"></i> Print</asp:LinkButton>

                                                        <%--  <asp:LinkButton ID="btnPrintReceipt" runat="server" Width="20" Height="19" class="btn btn-primary btn-xs"
                                                                OnClick="btnPrintReceipt_Click" CausesValidation="false" ValidationGroup="print" ><i class="fa fa-print"></i></asp:LinkButton>--%>
                                                        <%-- <a href="" class="btn btn-primary btn-xs" OnClick="btnPrintReceipt_Click"><i class="fa fa-print"></i> Print</a>--%>
                                                    </td>
                                                    <td style="padding-left: 10px;" valign="top">
                                                        <span class="amtvaluebox editpopup btn-xs">
                                                            <strong>
                                                                <asp:Label ID="lblStatus" runat="server" CssClass="uppaidtext" Style="padding-top: 0px;"></asp:Label>
                                                            </strong>
                                                        </span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Invoice&nbsp;Notes </label>
                                            </span><span>
                                                <asp:TextBox ID="txtInvoiceNotes" runat="server" class="form-control" TextMode="MultiLine" Height="180px"
                                                    CssClass="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtInvoiceNotes" FilterMode="InvalidChars" FilterType="Custom"
                                                    InvalidChars="'" >
                                                </cc1:FilteredTextBoxExtender>
                                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtInvoiceNotes" FilterMode="InvalidChars" FilterType="Custom"
                                                    InvalidChars='"' >
                                                </cc1:FilteredTextBoxExtender>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="haftdiv">
                                            <div class="form-group ">
                                                <span class="name">
                                                    <label class="control-label">Total Cost </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtTotalCostNew" Enabled="false" class="form-control" runat="server" CssClass="form-control"></asp:TextBox>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group ">
                                                <span class="name">
                                                    <label class="control-label">Less Rebate </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtLessRebate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtLessRebate"
                                                        Display="Dynamic" ErrorMessage="Please enter a number" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group ">
                                                <span class="name">
                                                    <label class="control-label">Total Cost </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtTotalCost" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Paid to Date </label>
                                            </span><span>
                                                <asp:TextBox ID="txtPaidDate" runat="server" Enabled="false" Text="0" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Bal Owing </label>
                                            </span><span>
                                                <asp:TextBox ID="txtBalOwing" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group" runat="server" id="divsysdetail">
                                            <span class="name ">
                                                <label class="control-label" style="width: 100%!important;">System Details </label>
                                            </span>
                                            <span>
                                                <asp:TextBox ID="ltsysdetail" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-12">
                                        <div class="table-round invoicetable">
                                            <table style="width: 100%; margin-bottom: 10px;" class="table tablebrd table-bordered gridcss spcaldate">
                                                <thead>
                                                    <tr class="gridheader">
                                                        <th class="center-text">Pay Date</th>
                                                        <th class="center-text">Total Paid</th>
                                                        <th class="center-text">GST</th>
                                                        <th class="center-text">Pay By</th>
                                                        <th class="center-text">Receipt No</th>
                                                        <th class="center-text">S/Chg</th>
                                                        <th class="center-text">Rec By</th>
                                                        <th class="center-text">Updated By</th>
                                                        <th class="center-text">Bank Verified</th>
                                                        <th class="center-text">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater runat="server" ID="rptPaymentDetails" OnItemCommand="rptPaymentDetails_ItemCommand">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td class="center-text"><%#Eval("InvoicePayDate1") %></td>
                                                                <td class="center-text"><%#Eval("InvoicePayTotal") %></td>
                                                                <td class="center-text"><%#Eval("InvoicePayGST") %></td>
                                                                <td class="center-text"><%#Eval("FPTransType") %></td>
                                                                <td class="center-text"><%#Eval("ReceiptNumber") %></td>
                                                                <td class="center-text"><%#Eval("CCSurcharge") %></td>
                                                                <td class="center-text"><%#Eval("RecBy") %></td>
                                                                <td class="center-text"><%#Eval("UpdatedBy") %></td>
                                                                <td class="center-text"><%#Eval("bankVerified") %></td>
                                                                <td class="center-text">
                                                                    <asp:LinkButton ID="lbtnDelete" runat="server" Visible='<%# Roles.IsUserInRole("Administrator") ? true:false %>' CommandName="Delete" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                                        CommandArgument='<%#Eval("InvoicePaymentID") + ";" + Eval("deleteFlag") %>' data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                                      <i class="fa fa-trash"></i> Delete
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div style="text-align: center;">
                                        <asp:Button CssClass="btn btn-primary redreq  savewhiteicon btnsaveicon" ID="btnSave" runat="server" Text="Save" CausesValidation="true"
                                            OnClick="btnSave_Click" ValidationGroup="save2" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" class="btn btn-danger" OnClick="lnkdelete_Click">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>
        <asp:HiddenField ID="hdndelete" runat="server" />
        <asp:HiddenField ID="hndInvoiceID" runat="server" />
        <asp:HiddenField ID="hndInvoiceDeleteType" runat="server" />
        <asp:Button ID="btnNULLActive" Style="display: none;" runat="server" />

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnPrintReceipt" />
        <%--<asp:PostBackTrigger ControlID="ddlInvoicePayMethodID" />--%>
        <%--<asp:PostBackTrigger ControlID="btnAddRow" />--%>
    </Triggers>
</asp:UpdatePanel>
