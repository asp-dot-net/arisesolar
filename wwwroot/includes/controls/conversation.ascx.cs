﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_controls_conversation : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>doMyAction();</script>");

            if (Roles.IsUserInRole("Administrator"))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }
            //if (Roles.IsUserInRole("PostInstaller"))
            //{
            //    divSalesRep.Visible = false;
            //}

            //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            //if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            //{
            //    string SalesTeam = "";
            //    DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            //    if (dt_empsale.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt_empsale.Rows)
            //        {
            //            SalesTeam += dr["SalesTeamID"].ToString() + ",";
            //        }
            //        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            //    }


            //    if (SalesTeam != string.Empty)
            //    {
            //        ddlEmployees.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            //    }
            //}
            //else
            {

            }
            ddlEmployees.Items.Clear();
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            try
            {
                ddlEmployees.SelectedValue = st.EmployeeID;

                ddlEmployees.DataSource = ClstblEmployees.tblEmployees_SelectASC();
                ddlEmployees.DataMember = "fullname";
                ddlEmployees.DataTextField = "fullname";
                ddlEmployees.DataValueField = "EmployeeID";
                ddlEmployees.DataBind();
            }
            catch
            {
            }
        }
    }
    public void BindData()
    {
        DataTable dt = ClstblConversation.tblConversation_Select(Request.QueryString["compid"]);
        if (dt.Rows.Count > 0)
        {
            //Response.Write(dt.Rows.Count);
            //Response.End();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void btnAddDetail_Click(object sender, EventArgs e)
    {
        string CustomerID = Request.QueryString["compid"];
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;
        string EmployeeConv=ddlEmployees.SelectedValue;

        int success3 = ClstblConversation.tblConversation_Insert(CustomerID, EmployeeID, txtnotes.Text);
        
        string readflag = "0";
        if (stEmp.EmployeeID == ddlEmployees.SelectedValue)
        {
            readflag = "1";
        }
        bool sucess1 = ClstblConversation.tblConversation_EmployeeConv_Update(success3.ToString(), EmployeeConv, readflag);
        if (Convert.ToString(success3) != string.Empty)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
            
            txtnotes.Text = string.Empty;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
        BindData();
        //SiteConfiguration.GetNotifications(ddlEmployees.SelectedValue);
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtnotes.Text = string.Empty;
       // ddlEmployees.SelectedValue = "0";
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>doMyAction();</script>");
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindData();
      
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblConversation.tblConversation_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetAdd1();
            //PanSuccess.Visible = true;

        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }

        PanAddUpdate.Visible = false;
        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindData();

    }
}