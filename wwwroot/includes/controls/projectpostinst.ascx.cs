using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Configuration;
using System.IO;
//using FormbayClientApi;

public partial class includes_controls_projectpostinst : System.Web.UI.UserControl
{
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        //string ProjectID = Request.QueryString["proid"];

        //SttblProjects st = ClstblProjects.tblProjects_Select ByProjectID(ProjectID);
        //if (!string.IsNullOrEmpty(st.InstallBookingDate))
        //{
        //    cmpNextDate.Visible = false;
        //    cmpNextDate.ValueToCompare = DateTime.Now.ToShortDateString();
        //}
        //else
        //{
        //    cmpNextDate.Visible = false;
        //}
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {

                if (st2.ProjectStatusID == "3")
                {
                    btnUpdatePostInst.Visible = false;
                }
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                if (st2.DepositReceived != string.Empty)
                {
                    btnUpdatePostInst.Visible = false;
                }
            }
            if (!IsPostBack)
            {
                BindInstallStatus();
            }
            if (st2.ProjectTypeID == "8")//btnUpdateQuote
            {
                sscomplete.Visible = true;
            }
            else
            {
                sscomplete.Visible = false;
            }
        }
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    public void BindProjectPostInst()
    {
        cmpNextDate.ValueToCompare = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddYears(-10)));

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);

            if ((Roles.IsUserInRole("Sales Manager")))
            {
                PanAddUpdate.Enabled = false;
            }
            if (!string.IsNullOrEmpty(st.InstallBookingDate))
            {
                cmpNextDate.Visible = true;
                cmpNextDate.ValueToCompare = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(st.InstallBookingDate));
            }
            else
            {
                cmpNextDate.Visible = false;
            }
            if (Roles.IsUserInRole("Administrator"))
            {
                panelcomcerti.Visible = true;
                // divAddUpdate.Visible = true;
            }
            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                //divAddUpdate.Visible = true;
                panelcomcerti.Visible = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = true;
                panelcomcerti.Visible = true;
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = true;
                PanelEdit.Enabled = true;
                Panel1.Enabled = false;
                Panel2.Enabled = false;
                Panel3.Enabled = false;
            }
            if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("SalesRep"))
            {
                ddlSTCCheckedBy.Enabled = false;
                ddlInstallVerifiedBy.Enabled = false;
            }

            BindDropDown();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            if (st.InvoiceNumber != string.Empty)
            {
                //InvoicePayments1.Visible = true;
            }
            else
            {
                //InvoicePayments1.Visible = false;
            }
            if (st.ProjectStatusID == "5")
            {
                RequiredFieldValidatorIC.Visible = true;
            }
            else
            {
                RequiredFieldValidatorIC.Visible = false;
            }

            if (st.ProjectStatusID != "9")
            {
                if (st.ProjectStatusID == "2" || st.ProjectStatusID == "8")
                {
                    divActive.Visible = true;
                    PanAddUpdate.Visible = false;
                }
                else
                {
                    if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
                    {
                        divActive.Visible = false;
                        PanAddUpdate.Visible = true;

                        string CEmpType = stEmpC.EmpType;
                        //string CSalesTeamID = stEmpC.SalesTeamID;
                        string CSalesTeamID = "";
                        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                        if (dt_empsale.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt_empsale.Rows)
                            {
                                CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                            }
                            CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                        }

                        if (Request.QueryString["proid"] != string.Empty)
                        {
                            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                            string EmpType = stEmp.EmpType;
                            //string SalesTeamID = stEmp.SalesTeamID;
                            string SalesTeam = "";
                            DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                            if (dt_empsale1.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt_empsale1.Rows)
                                {
                                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                                }
                                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                            }

                            if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                            {
                                PanAddUpdate.Enabled = true;
                            }
                            else
                            {
                                PanAddUpdate.Enabled = false;
                            }
                        }
                    }
                    if (st.FinanceWithID == string.Empty || st.FinanceWithID == "1")
                    {
                        PanAddUpdate.Visible = true;
                        DivDocNotVerified.Visible = false;
                    }
                    else
                    {
                        if (st.DocumentVerified == "True")
                        {
                            PanAddUpdate.Visible = true;
                            DivDocNotVerified.Visible = false;
                        }
                        else
                        {
                            PanAddUpdate.Visible = false;
                            DivDocNotVerified.Visible = true;
                        }
                    }
                }
            }
            try
            {
                txtsscompletedate.Text = Convert.ToDateTime(st.SSCompleteDate).ToShortDateString();
            }
            catch { }

            try
            {
                txtInstallCompleted.Text = Convert.ToDateTime(st.InstallCompleted).ToShortDateString();
            }
            catch { }
            try
            {
                txtInstallDocsReceived.Text = Convert.ToDateTime(st.InstallDocsReceived).ToShortDateString();
            }
            catch { }
            try
            {
                txtCertificateIssued.Text = Convert.ToDateTime(st.CertificateIssued).ToShortDateString();
            }
            catch { }
            try
            {
                txtInvoiceSent.Text = Convert.ToDateTime(st.InvoiceSent).ToShortDateString();
            }
            catch { }
            try
            {
                txtMeterAppliedDate.Text = Convert.ToDateTime(st.MeterAppliedDate).ToShortDateString();
            }
            catch { }
            try
            {
                txtMeterAppliedTime.Text = st.MeterAppliedTime;
            }
            catch { }

            try
            {
                ddlcomcerti.SelectedValue = st.ComplainceCertificate;

            }
            catch { }

            try
            {
                if (st.InstallVerifiedBy == string.Empty)
                {
                    ddlInstallVerifiedBy.SelectedValue = stEmpC.EmployeeID;
                }
                else
                {
                    ddlInstallVerifiedBy.SelectedValue = st.InstallVerifiedBy;
                }
            }
            catch
            {
            }
            chkSTCFormsDone.Checked = Convert.ToBoolean(st.STCFormsDone);
            try
            {
                if (st.STCCheckedBy == string.Empty)
                {
                    ddlSTCCheckedBy.SelectedValue = stEmpC.EmployeeID;
                }
                else
                {
                    ddlSTCCheckedBy.SelectedValue = st.STCCheckedBy;
                }
            }
            catch
            {
            }

            txtInverterSerial.Text = st.InverterSerial;
            txtSecondInverterSerial.Text = st.SecondInverterSerial;

            try
            {
                if (st.InvoiceStatusID == "2")
                {
                    txtInvoiceFU.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.InvoiceFU));
                }
            }
            catch { }

            chkReceiptSent.Checked = Convert.ToBoolean(st.ReceiptSent);
            txtMeterAppliedMethod.Text = st.MeterAppliedMethod;
            txtMeterAppliedRef.Text = st.MeterAppliedRef;

            DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != "")
            {
                decimal paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                decimal balown = Convert.ToDecimal(st.TotalQuotePrice) - Convert.ToDecimal(paiddate);
                try
                {
                    txtBalanceOwing.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
                }
                catch { }
            }

            chkProjectIncomplete.Checked = Convert.ToBoolean(st2.ProjectIncomplete);
            chkNoDocs.Checked = Convert.ToBoolean(st2.NoDocs);
            chkDocsDone.Checked = Convert.ToBoolean(st2.DocsDone);
            txtInspectorName.Text = st2.InspectorName;
            try
            {
                txtInspectionDate.Text = Convert.ToDateTime(st2.InspectionDate).ToShortDateString();
            }
            catch
            {
            }
            if (st.ProjectStatusID == "3" || st.ProjectStatusID == "4" || st.ProjectStatusID == "8")
            {

            }

            chkSTCFormSaved.Checked = Convert.ToBoolean(st.STCFormSaved);
            if (st.ST != string.Empty)
            {
                divST.Visible = true;
                lblST.Visible = true;
                lblST.Text = st.ST;
                //lblST.NavigateUrl = "~/userfiles/ST/" + st.ST;
                //lblST.NavigateUrl = pdfURL + "ST/" + st.ST;
                lblST.NavigateUrl = SiteConfiguration.GetDocumnetPath("ST", st.ST);
                RequiredFieldValidatorST.Visible = false;
            }
            else
            {
                divST.Visible = false;
                lblST.Visible = false;
            }

            chkCertificateSaved.Checked = Convert.ToBoolean(st.CertificateSaved);
            if (st.CE != string.Empty)
            {
                divCE.Visible = true;
                lblCE.Visible = true;
                lblCE.Text = st.CE;
                //lblCE.NavigateUrl = "~/userfiles/CE/" + st.CE;
                //lblCE.NavigateUrl = pdfURL + "CE/" + st.CE;
                lblCE.NavigateUrl = SiteConfiguration.GetDocumnetPath("CE", st.CE);
                RequiredFieldValidatorCE.Visible = false;
            }
            else
            {
                divCE.Visible = false;
                lblCE.Visible = false;
            }

            DataTable dtSydney = ClstblProjects.tblPostCodes_SelectSydney();
            if (dtSydney.Rows.Count > 0)
            {
                string Suburb = dtSydney.Rows[0]["Suburb"].ToString();
                if (Suburb == st.InstallCity)
                {
                    divMeterApplication.Visible = true;
                }
                else
                {
                    divMeterApplication.Visible = false;
                }
            }

            if (st2.ProjectIncomplete != string.Empty && st2.ProjectInCompReason != string.Empty)
            {
                divProjectInCompReason.Visible = true;
                txtProjectInCompReason.Text = st2.ProjectInCompReason;
            }
            else
            {
                divProjectInCompReason.Visible = false;
            }

            DataTable dt = ClstblProjects.tbl_StockPickUpDetail_SelectByProjectID(ProjectID);
            if (dt.Rows.Count > 0)
            {
                string pickupdate = dt.Rows[0]["PickUpDate"].ToString();
                if (string.IsNullOrEmpty(pickupdate))
                {
                    txtpickupdate.Text = string.Empty;
                }
                else
                {
                    txtpickupdate.Text = Convert.ToDateTime(dt.Rows[0]["PickUpDate"]).ToShortDateString();
                }
                if (!string.IsNullOrEmpty(dt.Rows[0]["InstallerNameID"].ToString()))
                {
                    ddlInstaller.SelectedValue = dt.Rows[0]["InstallerNameID"].ToString();
                }
                else
                {
                    BindDropDown();
                }
                txtPickupNote.Text = dt.Rows[0]["PickUpNote"].ToString();

                if (!string.IsNullOrEmpty(dt.Rows[0]["PickUpDate"].ToString()) && !string.IsNullOrEmpty(dt.Rows[0]["InstallerNameID"].ToString()))
                {
                    txtInstallCompleted.ReadOnly = true;//Changes on 04-/08/20 false to true
                }
                else
                {
                    txtInstallCompleted.ReadOnly = false;
                }
                if (!string.IsNullOrEmpty(st.InstallBookingDate.ToString()))
                {
                    txtInstallCompleted.ReadOnly = false;
                }
                else
                {
                    txtInstallCompleted.ReadOnly = true;
                }
            }
            DataTable dt1 = ClstblProjects.tbl_StockPickUpReturnDetail_SelectByProjectID(ProjectID);
            if (dt1.Rows.Count > 0)
            {
                string pickupreturndate = dt1.Rows[0]["PickUpReturnDate"].ToString();
                if (string.IsNullOrEmpty(pickupreturndate))
                {
                    txtPickupReturnDate.Text = string.Empty;
                }
                else
                {
                    txtPickupReturnDate.Text = Convert.ToDateTime(dt1.Rows[0]["PickUpReturnDate"]).ToShortDateString();
                }
                txtPickupReturnNote.Text = dt1.Rows[0]["PickUpReturnNote"].ToString();
                if (!string.IsNullOrEmpty(dt1.Rows[0]["InstallerID"].ToString()))
                {
                    ddlInstaller2.SelectedValue = dt1.Rows[0]["InstallerID"].ToString();
                }
                else
                {
                    BindDropDown();
                }
                //ddlInstaller2.SelectedValue = dt1.Rows[0]["InstallerID"].ToString();
            }

            DataTable dtPickList = ClstblProjects.tblPickUpChangeReason_SelectByPId(Request.QueryString["proid"]);
            if (dtPickList.Rows.Count > 0)
            {
                //txtPickupNote.Text = dtPickList.Rows[0]["Notes"].ToString();
                txtPickupNote.Text = dtPickList.Rows[0]["PickUpNote"].ToString();
                
                if (string.IsNullOrEmpty(dtPickList.Rows[0]["PickUpDate"].ToString()))
                {
                    txtpickupdate.Text = string.Empty;
                }
                else
                {
                    txtpickupdate.Text = Convert.ToDateTime(dtPickList.Rows[0]["PickUpDate"]).ToShortDateString();
                }
                divChangePickUp.Visible = true;
                rptChangePickUp.DataSource = dtPickList;
                rptChangePickUp.DataBind();
            }
            else
            {
                divChangePickUp.Visible = false;
            }
        }
    }

    public void BindDropDown()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        ListItem item = new ListItem();
        item.Value = "";
        item.Text = "Select";
        ddlInstallVerifiedBy.Items.Clear();
        ddlInstallVerifiedBy.Items.Add(item);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            string SalesTeam = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlInstallVerifiedBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlInstallVerifiedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        //ddlInstallVerifiedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlInstallVerifiedBy.DataValueField = "EmployeeID";
        ddlInstallVerifiedBy.DataMember = "fullname";
        ddlInstallVerifiedBy.DataTextField = "fullname";
        ddlInstallVerifiedBy.DataBind();

        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlSTCCheckedBy.Items.Clear();
        ddlSTCCheckedBy.Items.Add(item2);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            string SalesTeam = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }

            if (SalesTeam != string.Empty)
            {
                ddlSTCCheckedBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSTCCheckedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        //ddlSTCCheckedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlSTCCheckedBy.DataValueField = "EmployeeID";
        ddlSTCCheckedBy.DataMember = "fullname";
        ddlSTCCheckedBy.DataTextField = "fullname";
        ddlSTCCheckedBy.DataBind();

        ListItem item3 = new ListItem();
        item3.Text = "Installer Name";
        item3.Value = "";
        ddlInstaller.Items.Clear();
        ddlInstaller.Items.Add(item3);
        //   Response.Write(date1+"="+ date2);
        ddlInstaller.DataSource = ClstblContacts.tblContacts_GetInstallers();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Installer";
        item4.Value = "";
        ddlInstaller2.Items.Clear();
        ddlInstaller2.Items.Add(item4);
        //   Response.Write(date1+"="+ date2);
        ddlInstaller2.DataSource = ClstblContacts.tblContacts_GetInstallers();
        ddlInstaller2.DataValueField = "ContactID";
        ddlInstaller2.DataTextField = "Contact";
        ddlInstaller2.DataMember = "Contact";
        ddlInstaller2.DataBind();
    }

    protected void UpdateFunction()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            string installationBookingDate = st2.InstallBookingDate;
            //if (!string.IsNullOrEmpty(st2.InstallBookingDate))
            //{
            string ProjectID = Request.QueryString["proid"];
            string InstallCompleted = txtInstallCompleted.Text.Trim();
            string InstallDocsReceived = txtInstallDocsReceived.Text.Trim();
            string InstallVerifiedBy = ddlInstallVerifiedBy.SelectedValue;
            string STCFormsDone = Convert.ToString(chkSTCFormsDone.Checked);
            string STCCheckedBy = ddlSTCCheckedBy.SelectedValue;
            string STCFormSaved = Convert.ToString(chkSTCFormSaved.Checked);
            string CertificateIssued = txtCertificateIssued.Text.Trim();
            string CertificateSaved = Convert.ToString(chkCertificateSaved.Checked);
            string InverterSerial = txtInverterSerial.Text;
            string SecondInverterSerial = txtSecondInverterSerial.Text;
            string InvoiceSent = txtInvoiceSent.Text.Trim();
            string ReceiptSent = Convert.ToString(chkReceiptSent.Checked);

            string MeterAppliedDate = txtMeterAppliedDate.Text.Trim();
            //string MeterAppliedTime = txtMeterAppliedTime.Text.Trim();
            string MeterAppliedTime = "";
            string MeterAppliedMethod = txtMeterAppliedMethod.Text;
            string MeterAppliedRef = txtMeterAppliedRef.Text;
            int complaincecertificate = Convert.ToInt32(ddlcomcerti.SelectedValue);
            // HttpContext.Current.Response.Write(complaincecertificate);
            //HttpContext.Current.Response.End();

            string StockPickupDate = txtpickupdate.Text;
            string InstallerName = ddlInstaller.SelectedItem.Text;
            string InstallerNameID = ddlInstaller.SelectedItem.Value;

            string StockPickupReturnDate = txtPickupReturnDate.Text;
            string StockPickupReturnNote = txtPickupReturnNote.Text;
            string Installer = ddlInstaller2.SelectedItem.Text;
            string InstallerID = ddlInstaller2.SelectedItem.Value;

            String UpdatedDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));

            string ProjectIncomplete = Convert.ToString(chkProjectIncomplete.Checked);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string UpdatedBy = stEmp.EmployeeID;

            string ST = "";
            string CE = "";
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (chkSTCFormSaved.Checked == true)
            {
                if (fuST.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("ST", st.ST);
                    ST = ProjectID + fuST.FileName;
                    fuST.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\ST\\") + ST);
                    bool sucST = ClsProjectSale.tblProjects_UpdateST(ProjectID, ST);
                    SiteConfiguration.UploadPDFFile("ST", ST);
                    SiteConfiguration.deleteimage(ST, "ST");
                }
                RequiredFieldValidatorST.Visible = true;
            }
            else
            {
                SiteConfiguration.DeletePDFFile("ST", st.ST);

                bool sucMP = ClsProjectSale.tblProjects_UpdateST(ProjectID, "");
                RequiredFieldValidatorST.Visible = false;
            }
            if (chkCertificateSaved.Checked == true)
            {
                if (fuCE.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("CE", st.CE);
                    CE = ProjectID + fuCE.FileName;
                    fuCE.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\CE\\") + CE);
                    bool sucCE = ClsProjectSale.tblProjects_UpdateCE(ProjectID, CE);
                    SiteConfiguration.UploadPDFFile("CE", CE);
                    SiteConfiguration.deleteimage(CE, "CE");
                }
                RequiredFieldValidatorCE.Visible = true;
            }
            else
            {
                SiteConfiguration.DeletePDFFile("CE", st.CE);
                bool sucCE = ClsProjectSale.tblProjects_UpdateCE(ProjectID, "");
                RequiredFieldValidatorCE.Visible = false;
            }

            if (st.Installer != string.Empty)
            {
                DataTable dtComp = ClsProjectSale.tblProjects_InstCompCount(st.Installer);
                {
                    if (Convert.ToDecimal(dtComp.Rows[0]["Total"].ToString()) <= 3)
                    {
                        InstallCompleted = txtInstallCompleted.Text.Trim();
                    }
                    else
                    {
                        InstallCompleted = string.Empty;
                        divInstComp.Visible = true;
                    }
                }
            }


            DataTable dt = ClstblProjects.tbl_StockPickUpDetail_SelectByProjectID(ProjectID);
            if (dt.Rows.Count > 0)
            {
                if (string.IsNullOrEmpty(InstallerNameID))
                {
                    InstallerName = string.Empty;
                }
                string StockPickupDateSaved = dt.Rows[0]["PickupDate"].ToString();
                if (!string.IsNullOrEmpty(StockPickupDateSaved))
                {
                    StockPickupDateSaved = Convert.ToDateTime(StockPickupDateSaved).ToShortDateString();
                }
                string InstallerNameIDSaved = dt.Rows[0]["InstallerNameID"].ToString();
                string StockPickUpNoteSaved = dt.Rows[0]["PickUpNote"].ToString();
                if (StockPickupDateSaved != StockPickupDate || InstallerNameIDSaved != InstallerNameID || StockPickUpNoteSaved != txtPickupNote.Text)
                {
                    bool suc = ClstblProjects.tbl_StockPickUpDetail_UpdateByProjectID(ProjectID, st2.ProjectNumber, StockPickupDate, InstallerNameID, InstallerName, txtPickupNote.Text, UpdatedDate, UpdatedBy);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(StockPickupDate) || !string.IsNullOrEmpty(InstallerNameID) || !string.IsNullOrEmpty(txtPickupNote.Text))
                {
                    if (string.IsNullOrEmpty(InstallerNameID))
                    {
                        InstallerName = string.Empty;
                    }
                    //this line is used when there is no ProjectID in the table.
                    int success = ClstblProjects.tbl_StockPickUpDetail_Insert(ProjectID, st2.ProjectNumber, StockPickupDate, InstallerNameID, InstallerName, txtPickupNote.Text, UpdatedDate, UpdatedBy);
                }
            }
            //}


            DataTable dt1 = ClstblProjects.tbl_StockPickUpReturnDetail_SelectByProjectID(ProjectID);
            if (dt1.Rows.Count > 0)
            {
                if (string.IsNullOrEmpty(InstallerID))
                {
                    Installer = string.Empty;
                }
                string StockPickupReturnDateSaved = dt1.Rows[0]["PickupReturnDate"].ToString();
                if (!string.IsNullOrEmpty(StockPickupReturnDateSaved))
                {
                    StockPickupReturnDateSaved = Convert.ToDateTime(StockPickupReturnDateSaved).ToShortDateString();
                }
                string StockPickupReturnNoteSaved = dt1.Rows[0]["PickUpReturnNote"].ToString();
                string InstallerIDSaved = dt1.Rows[0]["InstallerID"].ToString();
                if (StockPickupReturnDateSaved != StockPickupReturnDate || StockPickupReturnNoteSaved != StockPickupReturnNote || InstallerIDSaved != InstallerID)
                {
                    bool suc = ClstblProjects.tbl_StockPickUpReturnDetail_UpdateByProjectID(ProjectID, st2.ProjectNumber, StockPickupReturnDate, StockPickupReturnNote, InstallerID, Installer, UpdatedDate, UpdatedBy);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(StockPickupReturnDate) || !string.IsNullOrEmpty(InstallerID) || !string.IsNullOrEmpty(StockPickupReturnDate))
                {
                    if (string.IsNullOrEmpty(InstallerID))
                    {
                        Installer = string.Empty;
                    }
                    int success = ClstblProjects.tbl_StockPickUpReturnDetail_Insert(ProjectID, st2.ProjectNumber, StockPickupReturnDate, StockPickupReturnNote, InstallerID, Installer, UpdatedDate, UpdatedBy);
                }
            }


            if (st.ProjectStatusID == "5" || st.ProjectStatusID == "10" || st.ProjectStatusID == "13" || st.ProjectStatusID == "14" || st.ProjectStatusID == "12" || st.ProjectStatusID == "11")
            {

                bool sucPostInst = ClsProjectSale.tblProjects_UpdatePostInst(ProjectID, InstallCompleted, InstallDocsReceived, InstallVerifiedBy, STCFormsDone, STCCheckedBy, STCFormSaved, CertificateIssued, CertificateSaved, InverterSerial, SecondInverterSerial, InvoiceSent, ReceiptSent, MeterAppliedDate, MeterAppliedTime, MeterAppliedMethod, MeterAppliedRef, UpdatedBy, complaincecertificate);
                bool s2 = ClstblProjects.tbl_PickListLog_Update_InstallCompleteDate(ProjectID, "1", InstallCompleted);
                if (sucPostInst)
                {
                    SetAdd1();
                    //PanSuccess.Visible = true;
                }
                else
                {
                    SetError1();
                    //PanError.Visible = true;
                }
            }
            //else //--20 / 2 / 19
            //{                    
            //    divStockAssign.Visible = true;
            //    txtInstallCompleted.Text = string.Empty;
            //}
            SttblProjects stmntc = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            if (stmntc.ProjectStatusID != "15")
            {
                if (st.ProjectStatusID != "13")
                {
                    if (st.ProjectStatusID != "5")
                    {
                        if (txtInstallCompleted.Text.Trim() != string.Empty && st.ProjectStatusID == "12")/* ----------------- Project Inst Complete ----------------- */
                        {
                            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "10");
                            if (dtStatus.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "10");
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("10", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                            }
                        }
                        else if (!string.IsNullOrEmpty(txtpickupdate.Text) && !string.IsNullOrEmpty(ddlInstaller.SelectedValue))/* ----------------- Stock Assigned ----------------- */
                        {
                            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "11");
                            if (dtStatus.Rows.Count > 0)
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("12", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "12");
                                // when staus is job booked and pickupdate and installer is selected 
                                //status should get updated to Stock assigned.
                            }
                        }
                        else if (txtInstallDocsReceived.Text.Trim() != string.Empty) /* -------------- Paperwork Received ------------ */
                        {
                            //if (st.ProjectStatusID == "10")
                            {
                                DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "13");
                                if (dtStatus.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("13", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "13");
                            }
                        }
                        else if (txtInvoiceFU.Text.Trim() != string.Empty)
                        {
                            if (st.STCApplied != string.Empty)
                            {
                                DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "14");
                                if (dtStatus.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("14", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "14");
                            }
                        } /* -------------------- STC / Invoice Paid -------------------- */
                        else
                        {
                            if (st2.STCApplied == string.Empty && st2.InvoiceFU == string.Empty)
                            {
                                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "13");
                            }
                            if (st.ProjectStatusID == "14" || st.ProjectStatusID == "5")
                            {
                                RequiredFieldValidatorPR.Visible = true;
                            }
                            else
                            {
                                if (txtInstallCompleted.Text.Trim() != string.Empty && txtInstallDocsReceived.Text.Trim() == string.Empty)
                                {
                                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "10");
                                }
                            }
                        }
                    }
                }
            }

            bool suc2 = ClsProjectSale.tblProjects2_UpdatePostInst(ProjectID, ProjectIncomplete, txtProjectInCompReason.Text);
            ClsProjectSale.tblProjects2_UpdatePostInstInspection(ProjectID, txtInspectorName.Text, txtInspectionDate.Text);
            //}
            //else
            //{
            //    divinstbookdate.Visible = true;

            //}
        }
        installcomplatemethod();
        BindProjectPostInst();
        BindProjects();
    }

    protected void btnUpdatePostInst_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        DataTable dt = ClstblProjects.tbl_StockPickUpDetail_SelectByProjectID(ProjectID);
        if (dt.Rows.Count > 0)
        {
            string InstallerId = dt.Rows[0]["InstallerNameID"].ToString();
            string pickupdate = dt.Rows[0]["PickUpDate"].ToString();
            string PickUpNote = dt.Rows[0]["PickUpNote"].ToString();
            if (!string.IsNullOrEmpty(InstallerId) && !string.IsNullOrEmpty(InstallerId) && !string.IsNullOrEmpty(InstallerId))
            {
                DateTime myDateTime = new DateTime();
                myDateTime = Convert.ToDateTime(pickupdate);
                string myString_new = myDateTime.ToString("dd/MM/yyyy");
                if (myString_new != txtpickupdate.Text || InstallerId != ddlInstaller.SelectedValue || PickUpNote != txtPickupNote.Text)
                {
                    ddlreason.SelectedValue = "0";
                    txtNotes2.Text = string.Empty;
                    modalpickupreason.Show();
                }
                else
                {
                    UpdateFunction();
                }
            }
            else
            {
                UpdateFunction();
            }
        }
        else//this is done because intially projectid and no. where not dumbed in tbl_StockPickUpDetail
        {
            UpdateFunction();
        }

    }
    public void installcomplatemethod()
    {
        string ProjectID = Request.QueryString["proid"];
        //if (txtInstallCompleted.Text.Trim() == string.Empty) --------20/2/19
        //{
        //    //ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
        //}
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (txtInstallCompleted.Text.Trim() == string.Empty && st2.StockDeductDate != string.Empty)//12
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "12");
        }

        if (txtInstallCompleted.Text.Trim() == string.Empty && st2.StockDeductDate != string.Empty && txtInstallDocsReceived.Text.Trim() == string.Empty)//12
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "12");
        }


    }
    protected void chkSTCFormSaved_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (chkSTCFormSaved.Checked == true)
        {
            divST.Visible = true;
            RequiredFieldValidatorST.Visible = true;
        }
        else
        {
            divST.Visible = false;
            RequiredFieldValidatorST.Visible = false;
        }
    }

    protected void chkCertificateSaved_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (chkCertificateSaved.Checked == true)
        {
            divCE.Visible = true;
            RequiredFieldValidatorCE.Visible = true;
        }
        else
        {
            divCE.Visible = false;
            RequiredFieldValidatorCE.Visible = false;
        }
    }
    protected void chkProjectIncomplete_CheckedChanged(object sender, EventArgs e)
    {
        if (chkProjectIncomplete.Checked == true)
        {
            divProjectInCompReason.Visible = true;
        }
        else
        {
            divProjectInCompReason.Visible = false;
        }
    }

    public void BindInstallStatus()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];

            //SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Profile.eurosolar.projectid);
            //string subdomain = ConfigurationManager.AppSettings["subdomain"].ToString();
            string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
            string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
            string username = ConfigurationManager.AppSettings["username"].ToString();
            string password = ConfigurationManager.AppSettings["password"].ToString();

            //  FormbayClient c = new FormbayClient(subdomain, clientid, clientSecret, username, password);
            //  Job job = new Job();
            DataTable dt = ClstblProjects.tblProjects_FormbayJobbyProjectID(ProjectID);
            if (dt.Rows.Count > 0)
            {
                lblinstall_status.Visible = true;
                btninstallstatus.Visible = true;
                lnkgetpanelnumbers.Visible = true;

                SttblProjects st_pro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                //job = c.GetJob(Convert.ToInt32(st_pro.FormbayId));

                //if (job.installation_status != string.Empty)
                //{
                //    try
                //    {
                //        SttblInstallationStatus st_install = ClstblInstallationStatus.tblInstallationStatus_SelectByInstallationStatusID(job.installation_status);
                //        lblinstall_status.Text = st_install.InstallationStatusName;
                //    }
                //    catch
                //    {
                //    }
                //}
            }
            else
            {
                lblinstall_status.Visible = false;
                btninstallstatus.Visible = false;
                lnkgetpanelnumbers.Visible = false;
            }
        }
    }
    protected void btninstallstatus_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];

            string subdomain = ConfigurationManager.AppSettings["subdomain"].ToString();
            string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
            string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
            string username = ConfigurationManager.AppSettings["username"].ToString();
            string password = ConfigurationManager.AppSettings["password"].ToString();

            //FormbayClient c = new FormbayClient(subdomain, clientid, clientSecret, username, password);
            //Job job = new Job();
            //DataTable dt = ClstblProjects.tblProjects_FormbayJobbyProjectID(ProjectID);
            //if (dt.Rows.Count > 0)
            //{
            //    SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            //    job = c.GetJob(Convert.ToInt32(st2.FormbayId));
            //    ClstblProjects.tblProjects_UpdateInstallationStatus(ProjectID, job.installation_status);
            //}
            BindInstallStatus();
        }
    }
    protected void lnkgetpanelnumbers_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(Profile.eurosolar.projectid))
        //{
        //    string subdomain = ConfigurationManager.AppSettings["subdomain"].ToString();
        //    string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
        //    string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
        //    string username = ConfigurationManager.AppSettings["username"].ToString();
        //    string password = ConfigurationManager.AppSettings["password"].ToString();
        //    FormbayClient c = new FormbayClient(subdomain, clientid, clientSecret, username, password);

        //    ModalPopupExtender1.Show();
        //    div_popup.Visible = true;
        //    Job job = new Job();
        //    SttblProjects st_pro = ClstblProjects.tblProjects_SelectByProjectID(Profile.eurosolar.projectid);
        //    job = c.GetJob(Convert.ToInt32(st_pro.FormbayId));

        //    string val = "";
        //    List<object> list = job.serialnumbersname;
        //    foreach (string prime in list)
        //    {
        //        val = val + prime + "<br/>";
        //    }
        //    lbl.Text = val;

        //}
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {

        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        string installationBookingDate = st2.InstallBookingDate;
        string ProjectID = Request.QueryString["proid"];

        string sscompletedate = txtsscompletedate.Text.Trim();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        bool sucQuote1 = false;

        //string ST = "";
        //string CE = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        if (sscompletedate != string.Empty)
        {
            sucQuote1 = ClsProjectSale.tblProjects_UpdateQuote_SSCompleteDate(ProjectID, sscompletedate);

            if (sucQuote1)
            {
                if (st.ProjectStatusID == "22")
                {

                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("23", ProjectID, stEmp.EmployeeID, st.NumberPanels);

                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "23");
                }
            }
        }
        if (sucQuote1)
        {
            PanSuccess.Visible = true;

        }
        else
        {
            PanError.Visible = true;
        }

        BindProjectPostInst();
        BindProjects();
    }

    protected void lnksendmessage_Click(object sender, EventArgs e)
    {
        try
        {

            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

            #region SendMail
            if (!String.IsNullOrEmpty(st.InvoiceSent))
            {
                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                String Subject = "Arise Solar Tax Invoice";

                Server.Execute("~/mailtemplate/TaxInvoiceAttach.aspx?proid=" + ProjectID, txtWriter);

                //if (st.ProjectTypeID == "8")
                //{
                //    Telerik_reports.generate_mtceinvoice(ProjectID, "Save");
                //}
                //else
                //{
                Telerik_reports.generate_Taxinvoice(ProjectID, "Save");
                //}
                string FileName = st.ProjectNumber + "_TaxInvoice.pdf";

                string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SavePDF2\\", FileName);

                Utilities.SendMailWithAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), fullPath, FileName);

                //Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), fullPath, FileName);

                SiteConfiguration.deleteimage(FileName, "SavePDF2");

                MsgSendSuccess();
            }
            else
            {
                MsgError("Please create invoice first.");
            }
            #endregion
        }
        catch (Exception exc)
        {
            SetError1();
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void MsgSendSuccess()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunMsgSendSuccess();", true);
    }

    public void MsgSuccess(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyGreenfun('{0}');", msg), true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }


    protected void ButtonReason_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        DataTable dt = ClstblProjects.tbl_StockPickUpDetail_SelectByProjectID(ProjectID);
        string myString_new = "";
        string InstallerId = "";
        string PickUpNote = "";
        string pickupdate = "";
        string updatedate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        if (dt.Rows.Count > 0)
        {
            pickupdate = dt.Rows[0]["PickUpDate"].ToString();
            DateTime myDateTime = new DateTime();
            myDateTime = Convert.ToDateTime(pickupdate);
            InstallerId = dt.Rows[0]["InstallerNameID"].ToString();
            PickUpNote = dt.Rows[0]["PickUpNote"].ToString();
            myString_new = myDateTime.ToString("dd/MM/yyyy");
        }
        string projectnumber = st.ProjectNumber;
        string reasonid = ddlreason.SelectedValue;
        string reason = ddlreason.SelectedItem.Text;
        string reasonnote = txtNotes2.Text;
        int sucTrack = ClstblProjects.tblPickUpChangeReason_Insert(ProjectID, projectnumber, reasonid, reason, reasonnote, pickupdate, InstallerId, PickUpNote, UpdatedBy, updatedate);
        UpdateFunction();
        modalpickupreason.Hide();
    }

    protected void ddlreason_SelectedIndexChanged(object sender, EventArgs e)
    {
        modalpickupreason.Show();
    }
}