<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectpreinst.ascx.cs"
    Inherits="includes_controls_projectpreinst" %>

<style>
    .ui-autocomplete-loading {
        background: white url("../../../images/indicator.gif") right center no-repeat;
    }

    .hide-modal {
        width: 250px;
        position: absolute;
        margin: 0 auto;
        right: 0;
        left: 0;
        bottom: 20px;
        z-index: 9999;
    }
    /*.selected_row {
            background-color: #A1DCF2 !important;
        }

        .modal-body .select2-container {
            z-index: 999999999999999 !important;
        }*/
    .set_with1 .btn-danger[disabled] {
        width: 89px;
        padding-left: 24px;
    }

    .dnone{
        display:none!important;
    }
</style>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
<asp:UpdatePanel ID="updatestockpanel" runat="server">
    <ContentTemplate>
        <script>
            function openModal() {
                $('[id*=modal_danger]').modal('show');
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>
<script src="../../assets/js/jquery.min.js"></script>
<script>

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedpreinst);
    function pageLoadedpreinst() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
        $(".myval1").select2({
            minimumResultsForSearch: -1
        });

        $('#<%=btnUpdatePreInst.ClientID %>').click(function (e) {
            formValidate();
        });
        $('#<%=Button1.ClientID %>').click(function (e) {
            // alert("1");
            formValidate();
        });

        $('#<%=btnSubmit.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
            <%--    $('#<%=picklistdiv.ClientID %>').hide();
                //$('.loading-container').css('display', 'block');
                if (count == 1) {
                    MyfunSuccess();
                    count++;
                }
                //alert("h1");--%>
            }
            else {
                formValidate();
                //alert("h2");
            }

        });

        {
            // NOTE: this block is to get scroll dynamically for more than 3 record as row's height won't be
            //be of fixed value.
            var row_count1 = $("#tblPL").find('tr').length;
            var table_height = 0;
            //alert(table_height);
            for (var i = 0; i <= 3; i++) {
                table_height = table_height + $("#tblPL tr").eq(i).height();
            }
            if (row_count1 > 3) {
                $('#<%=divpl.ClientID %>').attr("style", "overflow-y: scroll; height: " + table_height + "px !important;");
            }
            else {
                $('#<%=divpl.ClientID %>').attr("style", "");
            }
        }

        $('#<%=btnDownload.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
                $('#<%=picklistdiv.ClientID %>').hide();
                $('#<%=btnPickList.ClientID%>').removeAttr("disabled");
                //alert("h1");
            }
            else {
                formValidate();
                //alert("h2");
            }
        });

    }
    function getParsedAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "jsonp",
            data: {
                s: 'address',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data != null) {
                    document.getElementById("<%= txtInstallAddress.ClientID %>").value = data.StreetLine;
                    document.getElementById("<%= txtInstallPostCode.ClientID %>").value = data.Postcode;
                    document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                    document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                    document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                    document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                    document.getElementById("<%= txtInstallCity.ClientID %>").value = data.Suburb;
                    document.getElementById("<%= txtInstallState.ClientID %>").value = data.State;
                    document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                    document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                }
                validateProjectAddress();
            }
        });
    }
    function validateProjectAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "json",
            data: {
                s: 'validate',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data == true) {
                    document.getElementById("validaddressid").style.display = "block";
                    document.getElementById("invalidaddressid").style.display = "none";

                    document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                }
                else {
                    document.getElementById("validaddressid").style.display = "none";
                    document.getElementById("invalidaddressid").style.display = "block";
                    document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                }
            }
        });
    }
    function ChkFun1(source, args) {
        validateProjectAddress();
        var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

        if (elem == '1') {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }

    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }

</script>



<script type="text/javascript">
    var count = 1;

    $(document).ready(function () {
        //HighlightControlToValidate();
        $('.redreq1').click(function () {
            form_Validate();
        });
        // $('.loading-container').css('display', 'none');
    });


    function doMyAction() {

        $('#<%=btnUpdatePreInst.ClientID %>').click(function (e) {
            formValidate();
        });

        $('#<%=Button1.ClientID %>').click(function (e) {
            // alert("2");
            formValidate();
        });

        $('#<%=btnSubmit.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
              <%--  $('#<%=picklistdiv.ClientID %>').hide();
                //$('.loading-container').css('display', 'block');
                if (count == 1) {
                    MyfunSuccess();
                    count++;
                }
                //alert("h1");--%>
            }
            else {
                formValidate();
                //alert("h2");
            }

        });

        $('#<%=btnDownload.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
                $('#<%=picklistdiv.ClientID %>').hide();
                $('#<%=btnPickList.ClientID%>').removeAttr("disabled");
                //alert("h1");
            }
            else {
                formValidate();
                //alert("h2");
            }
        });

    }


    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function HighlightControlToValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                $('#' + Page_Validators[i].controltovalidate).blur(function () {
                    var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
                    if (validatorctrl != null && !validatorctrl.isvalid) {
                        $(This).css("border-color", "#FF5F5F");
                    }
                    else {
                        $(This).css("border-color", "#B5B5B5");
                    }
                });
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }

</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea stcpage popuoformbox">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Literal ID="litmessage" runat="server" Text="Transaction Failed."></asp:Literal></strong>
                        </div>
                        <div class="alert alert-danger" id="DivInstalldetails" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Please enter installation plan data.</strong>
                        </div>
                        <div class="alert alert-danger" id="DivDocNotVerified" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Your documents are not verified.</strong>
                        </div>
                        <div class="alert alert-danger" id="divActive" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Your Project is not Active.</strong>
                        </div>
                        <div class="alert alert-danger" id="divVicAppRefRebate" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Please enter Vic Rebate App Ref.</strong>
                        </div>
                        <div class="alert alert-success" id="divformbaymsg" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;<asp:Label runat="server" ID="lblformbaymsg"></asp:Label></strong>
                        </div>
                        <div class="alert alert-danger" id="divinverter" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Picklist Can not be Generated For this  Inverter.</strong>
                        </div>
                        <div class="alert alert-danger" id="divpanel" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Picklist Can not be Generated For this Panel.</strong>
                        </div>
                        <div class="alert alert-danger" id="divinstalldate" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Your install date should be less then one month to generate picklist</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <%--<div class="panel panel-default">
                            <div class="panel-body">--%>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group">
                                    <span></span>
                                    <div class=" checkbox-info checkbox">
                                        <span class="fistname">
                                            <asp:Label ID="lbltesting" runat="server"></asp:Label>
                                            <label for="<%=chkElecDistOK.ClientID %>" class="control-label">
                                                <asp:CheckBox ID="chkElecDistOK" runat="server" OnCheckedChanged="chkElecDistOK_CheckedChanged" AutoPostBack="true" />
                                                <span class="text">Elec Dist OK  &nbsp;</span>
                                            </label>
                                        </span>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group" id="divElecDistApproved" runat="server" visible="false">
                                    <span class="name">
                                        <asp:Label ID="Label23" runat="server" class="control-label">
                                            Elec Dist Approved</asp:Label></span>
                                    <div class="input-group date datetimepicker1">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                        <asp:TextBox ID="txtElecDistApproved" runat="server" class="form-control" Width="125px">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEDA" runat="server" ErrorMessage=""
                                            Visible="false" ValidationGroup="preinst" ControlToValidate="txtElecDistApproved"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="name">
                                        <asp:Label ID="Label2" runat="server" class="control-label">
                                           Install</asp:Label></span>
                                    <div class="input-group datevalidation date datetimepicker1">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                        <asp:TextBox ID="txtInstallBookingDate" runat="server" AutoPostBack="true" OnTextChanged="txtInstallBookingDate_TextChanged1" class="form-control" Width="125px">
                                        </asp:TextBox>
                                        <%--   <asp:RangeValidator ID="DateRange" ValidationGroup="preinst" class="datecmp"
                                            runat="server" ControlToValidate="txtInstallBookingDate"
                                            Type="Date" ErrorMessage="Install Date Is not Greater Than Lic Expire date."></asp:RangeValidator>--%>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorIBDate" runat="server" ControlToValidate="txtInstallBookingDate" CssClass=""
                                            ValidationGroup="preinst" ErrorMessage="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                    </div>
                                </div>


                                <%--   <asp:Button ID="btnRemoveInst" runat="server" OnClick="btnRemoveInst_Click" Text="Remove Install" CssClass="btn btn-info" Visible="false" />--%>

                                <asp:LinkButton ID="btnRemoveInst" Style="width: 160px" runat="server" OnClick="btnRemoveInst_Click" OnClientClick="yes" CausesValidation="false" class="btn btn-labeled btn-danger">
                                        <i class="btn-label fa fa-trash"></i>Remove Install</asp:LinkButton>

                                <div class="form-group row">
                                    <span class="name paddtop2px col-md-12" style="margin: 15px 0px;">
                                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                            <tbody>
                                                <tr>
                                                    <td width="5">AM </td>
                                                    <td width="10">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=rblAM1.ClientID %>">
                                                                    <asp:CheckBox ID="rblAM1" runat="server" />
                                                                    <span class="text"></span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td width="50">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=rblAM2.ClientID %>">
                                                                    <asp:CheckBox ID="rblAM2" runat="server" />
                                                                    <span class="text"></span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td width="5">PM </td>
                                                    <td width="10">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=rblPM1.ClientID %>">
                                                                    <asp:CheckBox ID="rblPM1" runat="server" />
                                                                    <span class="text"></span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td width="10">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=rblPM2.ClientID %>">
                                                                    <asp:CheckBox ID="rblPM2" runat="server" />
                                                                    <span class="text"></span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>



                                    </span>
                                    <%--      <span class="col-md-12">PM
                                        <asp:CheckBox ID="rblPM1" runat="server" />
                                        <label for="<%=rblPM1.ClientID %>">
                                            <span></span>
                                        </label>
                                        <asp:CheckBox ID="rblPM2" runat="server" />
                                        <label for="<%=rblPM2.ClientID %>">
                                            <span></span>
                                        </label>
                                    </span>--%>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <span class="name" style="width: 45px; float: left; padding-top: 6px;"></span>
                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <label class="control-label">
                                                        Days
                                                    </label>
                                                    <span style="width: 65px; margin-top: -5px;">
                                                        <asp:TextBox ID="txtInstallDays" runat="server" Width="50px" Text="1" MaxLength="10" CssClass="form-control wd50" AutoPostBack="true" OnTextChanged="txtInstallDays_TextChanged"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtInstallDays"
                                                            ValidationGroup="preinst" Display="Dynamic" ErrorMessage="Please enter a number"
                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        <%--   <asp:RangeValidator ID="rangevalEditQty" runat="server" ErrorMessage="Please enter valid day"
                                                            ValidationGroup="preinst" ControlToValidate="txtInstallDays" Type="Integer" MinimumValue="1"
                                                            MaximumValue="356" Display="Dynamic"></asp:RangeValidator>--%>
                                                    </span>
                                                </td>
                                                <td runat="server">
                                                    <div style="width: 100%; display: inline-block; margin-top: 28px">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=chkquickform.ClientID %>" class="control-label">
                                                                    <asp:CheckBox ID="chkquickform" runat="server" />
                                                                    <span class="text">Quick Form   </span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                </td>

                                                <td runat="server" visible="false">
                                                    <div style="width: 100%; display: inline-block; margin-top: 28px">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=cbkIsFormBay.ClientID %>" class="control-label">
                                                                    <asp:CheckBox ID="cbkIsFormBay" runat="server" />
                                                                    <span class="text">IsFormBay   </span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </track>
                                    <div class="clear">
                                    </div>
                                </div>

                                <div class="form-group ">

                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                        <tbody>
                                            <tr>
                                                <td valign="top" style="width: 175px">
                                                    <span class="dateimg">

                                                        <asp:ImageButton ID="btnCreateSTCForm" runat="server" ImageUrl="../../admin/images/btn_create_stc_form.png"
                                                            CausesValidation="false" OnClick="btnCreateSTCForm_Click" Visible="false" />
                                                    </span>

                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="panelPickListBtn" Visible="false">
                                                        <span class="dateimg">
                                                            <asp:ImageButton ID="btnPickList" runat="server" OnClick="btnPickList_Click"
                                                                Text="PickList" CausesValidation="false"
                                                                ImageUrl="../../admin/images/btn_picklist.png" Visible="false" />
                                                        </span>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <span class="dateimg"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <caption>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                        </caption>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="col-md-4">
                                <div class="form-group dateimgarea wdth200">
                                    <span class="name disblock">
                                        <asp:label class="control-label"  id="lblloc" runat="server">
                                            Stock Allocation Store
                                        </asp:label>
                                    </span>
                                    <div class="drpValidate">
                                        <asp:DropDownList ID="ddlStockAllocationStore" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlStockAllocationStore" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>

                                    </div>

                                    <div class="clear">
                                    </div>
                                </div>
                                <asp:UpdatePanel runat="server" ID="upddl" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group wdth200" id="Installer" runat="server">
                                            <span class="name disblock">
                                                <asp:label class="control-label" id="lblInstaller" runat="server">
                                                    Installer
                                                </asp:label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlInstaller_SelectedIndexChanged"
                                                    AutoPostBack="true" aria-controls="DataTables_Table_0" class="myval">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddlInstaller" Display="Dynamic" ValidationGroup="preinst" InitialValue="">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <span style="float: left; width: 100px; padding-left: 10px;">
                                                <asp:Button ID="btnCustNotes" runat="server" class="btn btn-primary" Text="Cust Notes"
                                                    Visible="false" Style="float: left" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlInstaller" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="form-group wdth200" id="Designer" runat="server">
                                    <span class="name disblock">
                                        <asp:label class="control-label" id="lbldesign" runat="server">
                                            Designer
                                        </asp:label>
                                    </span>
                                    <div class="drpValidate">
                                        <asp:DropDownList ID="ddlDesigner" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <span style="float: left; width: 100px; padding-left: 10px;">
                                        <asp:Button ID="btnComplianceCert" runat="server" class="btn btn-primary" Text="Compliance Cert"
                                            Visible="false" />
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group wdth200">
                                    <span class="name disblock">
                                        <asp:label class="control-label" id="lblec" runat="server">
                                            Electrician
                                        </asp:label>
                                    </span>
                                    <div class="drpValidate">
                                        <asp:DropDownList ID="ddlElectrician" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group wdth200">
                                    <span class="name">
                                        <label class="control-label">
                                            Installer Notes
                                        </label>
                                    </span><span>
                                        <asp:TextBox ID="txtInstallerNotes" runat="server" TextMode="MultiLine" Height="70px"
                                            CssClass="form-control"></asp:TextBox>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span class="name">
                                        <label class="control-label">
                                            Install Site
                                        </label>
                                    </span><span class="fullspan">
                                        <div class="marginbtm10" runat="server" visible="false">
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                            <asp:TextBox ID="txtInstallAddressline" runat="server" MaxLength="200" CssClass="InstallAddress  form-control" onblur="getParsedAddress();"></asp:TextBox>
                                            <asp:CustomValidator ID="custompreadd" runat="server"
                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="preinst" Enabled="false"
                                                ClientValidationFunction="ChkFun1"></asp:CustomValidator>
                                        </div>
                                        <div class="marginbtm10">
                                            <asp:HiddenField ID="hndaddress" runat="server" />
                                            <asp:TextBox ID="txtInstallAddress" runat="server" MaxLength="200" CssClass="InstallAddress  form-control"></asp:TextBox>
                                        </div>
                                        <asp:HiddenField ID="hndstreetno" runat="server" />
                                        <asp:HiddenField ID="hndstreetname" runat="server" />
                                        <asp:HiddenField ID="hndstreettype" runat="server" />
                                        <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                        <asp:HiddenField ID="hndunittype" runat="server" />
                                        <asp:HiddenField ID="hndunitno" runat="server" />
                                        <div id="validaddressid" style="display: none">
                                            <img src="../../../images/check.png" alt="check">Address is valid.
                                        </div>
                                        <div id="invalidaddressid" style="display: none">
                                            <img src="../../../images/x.png" alt="cross">
                                            Address is invalid.
                                        </div>
                                        <div class="onelindiv marginbtm10 row">
                                            <span class="col-sm-6">
                                                <asp:TextBox ID="txtformbayUnitNo" runat="server" placeholder="Unit No" CssClass="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required."
                                                                        ControlToValidate="txtformbayUnitNo" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>--%>
                                            </span>
                                            <span class="col-sm-6">
                                                <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                                        ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>--%>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="onelindiv marginbtm10 row">
                                            <span class="col-md-3 col-sm-3">
                                                <asp:TextBox ID="txtformbayStreetNo" runat="server" placeholder="Street No" CssClass="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This value is required."
                                                                        ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>--%>
                                            </span>
                                            <span class="col-md-4 col-sm-4 padleftzero">
                                                <asp:TextBox ID="txtformbaystreetname" runat="server" placeholder="Street Name" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required."
                                                    ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                    ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="detail"
                                                    ClientValidationFunction="validateformbaystreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                <div id="Divvalidstreetname1" style="display: none">
                                                    <img src="../../../images/check.png" alt="check">Address is valid.
                                                </div>
                                                <div id="DivInvalidstreetname1" style="display: none">
                                                    <img src="../../../images/x.png" alt="cross">
                                                    Address is invalid.
                                                </div>
                                            </span>

                                            <span class="col-md-5 col-sm-5 padleftzero">
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Street Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>

                                                </div>
                                                <div class="clear">
                                                </div>
                                            </span>
                                        </div>
                                        <div class="marginbtm10 row">
                                            <span class="col-sm-6 col-md-6">
                                                <asp:TextBox ID="txtInstallCity" runat="server" CssClass="form-control" OnTextChanged="txtInstallCity_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <cc1:AutoCompleteExtender ID="AutoComplete_pre" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtInstallCity" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                </cc1:AutoCompleteExtender>

                                            </span>
                                            <span class="col-sm-3 col-md-3 padleftzero">
                                                <asp:TextBox ID="txtInstallState" runat="server" Enabled="false" MaxLength="200"
                                                    CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <span class="col-sm-3 col-md-3 padleftzero">
                                                <asp:TextBox ID="txtInstallPostCode" runat="server" Enabled="false"
                                                    CssClass="form-control"></asp:TextBox>
                                            </span>
                                        </div>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <span class="name col-md-6">
                                        <%--<asp:ImageButton ID="btnCustAddress" runat="server"
                                            OnClick="btnCustAddress_Click" ImageUrl="~/images/user_cust_address_btn1.png" OnClientClick="yes"
                                            CausesValidation="false" />--%>

                                        <asp:LinkButton ID="btnCustAddress" Style="width: 160px" runat="server" OnClientClick="yes" CausesValidation="false" class="btn btn-labeled btn-darkorange" Visible="false">
                                        <i class="btn-label fa fa-street-view"></i>Use Cust. Add</asp:LinkButton>
                                    </span>



                                    <%--<asp:Button CssClass="btn btn-primary savewhiteicon" ID="btnUpdateAddress" runat="server" OnClick="btnUpdateAddress_Click"
                                        Text="Update Address" />--%>
                                    <span class="name col-md-5">
                                        <%--<asp:ImageButton ID="btnUpdateAddress" runat="server"
                                            OnClick="btnUpdateAddress_Click" OnClientClick="yes" ImageUrl="~/images/icon_updateaddress.png" />--%>

                                        <asp:LinkButton ID="btnUpdateAddress" Style="width: 150px" runat="server" OnClientClick="yes" OnClick="btnUpdateAddress_Click" CausesValidation="false" class="btn btn-labeled btn-primary" Visible="false">
                                        <i class="btn-label fa fa-map-marker"></i>Update Address
                                        </asp:LinkButton>
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                                          
                                <div class="form-group " style="display: none;">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Cust/Inst Notify
                                        </label>
                                    </span><span>
                                        <asp:DropDownList ID="ddlSendMail" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="1">Customer-Install Confirm</asp:ListItem>
                                            <asp:ListItem Value="2">Installer-Install Confirm</asp:ListItem>
                                            <asp:ListItem Value="3">Installer-Cancel Install</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <span class="name">
                                        <div class="row">
                                            <div class="col-md-3" style="display:none">
                                                <asp:Button ID="btnMove" runat="server" OnClick="btnMove_Click" CssClass="btn btn-primary" Text="Move" Visible="false" />
                                            </div>
                                            <div class="col-md-7" style="display:none">
                                                <asp:DropDownList ID="ddlMoveProject" runat="server" Visible="false" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Project</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="display:none">
                                                <asp:TextBox ID="txtInstallendDate2" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-group ">
                                    <span class="name">
                                        <label class="control-label">
                                            System Details
                                        </label>
                                    </span><span>
                                        <asp:HiddenField ID="hbdSystemDetails" runat="server" />
                                        <asp:TextBox ID="txtSystemDetails" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>     
                        <div class="form-group row">
                                    <div class="col-md-6">
                                        <span class="name ">
                                            <label class="control-label">
                                                Panel
                                            </label>
                                        </span>

                                        <span>
                                            <asp:TextBox ID="txtnoofpanel" runat="server" CssClass="form-control" Enabled="false" Visible="false"></asp:TextBox>
                                        </span>
                                        <span class="name ">
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                                L
                                            </label>
                                            <label class="control-label" style="width: 50px;display:none; text-align: center;">
                                             <%--   J--%>P
                                            </label>
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                             APL
                                            </label>
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                             SMPL
                                            </label>
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                                W
                                            </label>
                                            <label class="control-label" style="width: 70px; text-align: center;">
                                                T
                                            </label>
                                            <label class="control-label" style="width: 70px; text-align: center;">
                                                O
                                            </label>
                                            <label class="control-label" style="width: 70px; text-align: center;">
                                                ETA
                                            </label>
                                             <label class="control-label" style="width: 50px; text-align: center;">
                                                M
                                            </label>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <asp:Label ID="lblPl" runat="server" Width="50px" CssClass="form-control"></asp:Label>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:none;" >
                                            <asp:Label ID="lblPj" runat="server" Width="50px" CssClass="form-control"></asp:Label>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <%--<asp:Label ID="lblApl" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                            <asp:LinkButton ID="lblApl" runat="server" OnClick="lblApl_Click" Width="50px" CssClass="form-control"></asp:LinkButton>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                           <%-- <asp:Label ID="lblSMPL" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                            <asp:LinkButton runat="server" ID="lblSMPL" Width="50px" CssClass="form-control" OnClick="lblSMPL_Click"></asp:LinkButton>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <%--<asp:Label ID="lblPW" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                         <asp:LinkButton runat="server" ID="lblPW" Width="50px" CssClass="form-control" OnClick="lblPW_Click"></asp:LinkButton>
                                        </span>
                                        <span style="width: 70px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <asp:Label ID="lblPt" runat="server" Width="65px" CssClass="form-control"></asp:Label>
                                        </span>
                                        <span style="width: 70px; text-align: center; margin-bottom:5px; display:inline-block;">
                                          <%--<asp:Label ID="lblPO" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                          <asp:LinkButton runat="server" ID="lblPO" Width="70px" CssClass="form-control" OnClick="lblPO_Click"></asp:LinkButton>
                                        </span>
                                        <span style="width: 70px; text-align: center; margin-bottom:5px; display:inline-block;">
                                         <%--<asp:Label ID="lblIo" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                           <asp:LinkButton runat="server" ID="lnketap" Width="70px" CssClass="form-control"> </asp:LinkButton>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <asp:Label ID="lblPm" runat="server" Width="50px" CssClass="form-control"></asp:Label>                                           
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="name">
                                            <label class="control-label">
                                                Inverter
                                            </label>
                                        </span>

                                        <span>
                                            <asp:TextBox ID="txtnoofinverter" runat="server" CssClass="form-control" Enabled="false" Visible="false"></asp:TextBox>
                                        </span>
                                        <span class="name ">
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                                L
                                            </label>
                                            <label class="control-label" style="width: 50px; text-align: center;display:none">
                                                <%--J--%>P
                                            </label>
                                              <label class="control-label" style="width: 50px; text-align: center;">
                                                APL
                                            </label>
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                                SMPL
                                            </label>
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                                W
                                            </label>
                                            <label class="control-label" style="width: 70px; text-align: center;">
                                                T
                                            </label>
                                            <label class="control-label" style="width: 70px; text-align: center;">
                                                O
                                            </label>
                                            <label class="control-label" style="width: 70px; text-align: center;">
                                                ETA
                                            </label>
                                            <label class="control-label" style="width: 50px; text-align: center;">
                                                M
                                            </label>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <asp:Label ID="lblIl" runat="server" Width="50px" CssClass="form-control"></asp:Label>
                                        </span>
                                        <span style="width: 50px; text-align: center; margin-bottom:5px; display:none;">
                                            <asp:Label ID="lblIj" runat="server" Width="50px" CssClass="form-control"></asp:Label>
                                        </span>
                                         <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <%--<asp:Label ID="lblAriseInv" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                             <asp:LinkButton ID="lblAriseInv" runat="server" Width="50px" OnClick="lblAriseInv_Click" CssClass="form-control"></asp:LinkButton>
                                        </span>
                                          <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <%--<asp:Label ID="lblSmInv" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                              <asp:LinkButton ID="lblSmInv" runat="server" Width="50px" OnClick="lblSmInv_Click" CssClass="form-control"></asp:LinkButton>
                                        </span>
                                         <span style="width: 50px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <%--<asp:Label ID="lblIW" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                            <asp:LinkButton runat="server" ID="lblIW" Width="50px" CssClass="form-control" OnClick="lblIW_Click"></asp:LinkButton>
                                        </span>
                                        <span style="width: 70px; text-align: center; margin-bottom:5px; display:inline-block;">
                                            <asp:Label ID="lblIt" runat="server" Width="65px" CssClass="form-control"></asp:Label>
                                        </span>
                                        <span style="width: 70px; text-align: center; margin-bottom:5px; display:inline-block;">
                                         <%--<asp:Label ID="lblIo" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                           <asp:LinkButton runat="server" ID="lblIo" Width="70px" CssClass="form-control" OnClick="lblIo_Click"></asp:LinkButton>
                                        </span>
                                         <span style="width: 70px; text-align: center; margin-bottom:5px; display:inline-block;">
                                         <%--<asp:Label ID="lblIo" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                           <asp:LinkButton runat="server" ID="lnketaI" Width="70px" CssClass="form-control"></asp:LinkButton>
                                        </span>
                                         <span style="width:50px; text-align: center;">
                                            <asp:Label ID="lblIm" runat="server" Width="50px" CssClass="form-control"></asp:Label>                                           
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    
                                </div>
                        <div class="row" id="divPickList" runat="server" visible="false">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5">
                                            <h5>Pick List Log</h5>
                                        </div>
                                    </div>
                                    <div class="row finalgrid">
                                        <div class="col-md-12" runat="server" id="divpl">
                                            <table width="100%" cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%; text-align: center;">ID
                                                        </th>
                                                        <th style="width: 5%; text-align: center;">Picklist Type
                                                        </th>
                                                        <th style="width: 5%; text-align: center;">Generated Date 
                                                        </th>
                                                        <%--PickList Generated Date--%>
                                                        <th style="width: 5%; text-align: center">Install Date
                                                        </th>
                                                        <%--InstallationBooked Date--%>
                                                        <th style="width: 10%; text-align: center">Installer Name  
                                                        </th>
                                                        <th style="width: 20%; text-align: center">System Details  
                                                        </th>
                                                        <th style="width: 20%; text-align: center">Reason
                                                        </th>
                                                        <%-- <th style="width: 20%; text-align: center">Note
                                                        </th>--%>
                                                        <th style="width: 5%; text-align: center">CreatedBy
                                                        </th>
                                                        <th style="width: 7%; text-align: center">Deduct On
                                                        </th>
                                                        <th style="text-align: center">&nbsp;
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:HiddenField ID="hndInstallerId" runat="server" />
                                                <asp:Repeater ID="rptPickList" runat="server" OnItemCommand="rptPickList_ItemCommand" OnItemDataBound="rptPickList_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 5%;">
                                                                <%#Eval("ID")%>
                                                            </td>
                                                            <td style="width: 5%;">
                                                                <%#Eval("pcktype")%>
                                                            </td>
                                                            <td style="width: 5%;">
                                                                <asp:HiddenField ID="hndpicklistid" runat="server" Value='<%#Eval("ID")%>' />
                                                                <%#Eval("PickListDateTime", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            <%-- <td style="width: 5%;">
                                                                <%#Eval("InstallerID")%>
                                                            </td>--%>
                                                            <td style="width: 5%;">
                                                                <%#Eval("InstallBookedDate", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            <td style="width: 10%; word-break: break-word;">
                                                                <%#Eval("InstallerName")%>
                                                            </td>
                                                            <td style="width: 20%;">
                                                                <%--<%#Eval("PanelQty")+" X " + Eval("Panel") + " Plus " + Eval("InverterQty") + " X " + Eval("Inverter")%>--%>
                                                                <%--<%#Eval("SystemDetail")%>--%>
                                                                <asp:Label ID="lblstytemdetail" runat="server" Text='<%#Eval("SystemDetail")%>'></asp:Label>
                                                            </td>
                                                            <td style="width: 20%;">
                                                                <%#Eval("Reason")%>
                                                            </td>
                                                            <%--  <td style="width: 20%;">
                                                                <%#Eval("Note")%>
                                                            </td>--%>
                                                            <td style="width: 5%;">
                                                                <%--    <%#Eval("EmpNicName")%>--%>
                                                                <asp:HiddenField ID="hdnEmpId" runat="server" Value='<%#Eval("createdby")%>' />
                                                                <asp:Label ID="lblEmpName" Width="70px" runat="server"></asp:Label>
                                                            </td>
                                                            <td style="width: 7%;">
                                                                <%#Eval("DeductOn", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            <%-- <td style="width: 10%;">
                                                              <asp:HiddenField ID="hndPickLIstItemId" runat="server" Value='<%# Eval("PicklistitemId") %>' />
                                                            </td>--%>
                                                            <td>
                                                                <asp:LinkButton ID="btnUpdatepicklist" Style="width: 70px" runat="server" OnClick="btnUpdatepicklist_Click" CommandName="Edit" CommandArgument='<%#Eval("Id")+","+ Eval("InstallerID") %>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                Edit
                                                                </asp:LinkButton>&nbsp;&nbsp;
                                                                <asp:LinkButton ID="btnwallet" Style="width: 70px" runat="server" OnClick="btnwallet_Click" CommandName="wallet" CommandArgument='<%#Eval("Id")+","+ Eval("InstallerID")%>' CausesValidation="false" class="btn btn-labeled btn-primary" Visible="false">
                                                                Wallet
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnpicklistDelete" Style="width: 70px" runat="server" CommandName="delete" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                Delete
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnNotes" Style="width: 70px" runat="server" CommandName="notes" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                Notes
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="picklistdiv" runat="server" visible="false">
                            <div class="col-md-4">
                                <div class="form-group wdth200">
                                    <span class="name">
                                        <label class="control-label">
                                            Reason for generating PickList again:
                                        </label>
                                    </span><span>
                                        <asp:TextBox ID="txtreason" runat="server" TextMode="MultiLine" Height="70px"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtreason"
                                            Display="Dynamic" ValidationGroup="picklist"></asp:RequiredFieldValidator>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group wdth200">
                                    <span class="name">
                                        <label class="control-label">
                                            Note for generating PickList again:
                                        </label>
                                    </span><span>
                                        <asp:TextBox ID="txtnote" runat="server" TextMode="MultiLine" Height="70px"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass=""
                                            ValidationGroup="picklist" ControlToValidate="txtnote"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="padding-top: 40px;">
                                    <asp:Button class="btn btn-primary" ID="btnSubmit" runat="server" OnClick="btnSubmit_Click"
                                        CausesValidation="true" ValidationGroup="picklist" Text="Submit" />
                                    <%--<asp:HyperLink ID="hypDownload" Visible="false"  runat="server" Text="Download PickList">            
                                    </asp:HyperLink>--%>
                                    <a class="btn btn-info" runat="server" id="btnDownload" onserverclick="btnDownload_Click" style="display: none;"><i class="fa fa-download" style="font-size: 12px;"></i>Download PickList</a>
                                    <asp:HiddenField runat="server" ID="hdnPickListLogID" />
                                    <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                        CausesValidation="false" Text="Cancel" />
                                </div>
                            </div>
                        </div>
                        <%--  </div>
                        </div>--%>
                        <div class="row" id="divAddUpdate" runat="server">
                            <div class="col-md-12 text-center">
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="btnUpdatePreInst" runat="server" OnClick="btnUpdatePreInst_Click"
                                    CausesValidation="true" ValidationGroup="preinst" Text="Save" />
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="btngreenbot" runat="server" OnClick="GreenBotSave"
                                    CausesValidation="true" ValidationGroup="preinst" Text="Greenbot Save" />
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnformbay1" Visible="false" CausesValidation="true" ValidationGroup="preinst" runat="server" OnClick="btnformbay1_Click" Text="Formbay" />
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnquickfrom" runat="server" OnClick="btnquickfrom_Click" Text="QuickForm" />
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnquickfromUpdate" runat="server" OnClick="btnquickfromUpdate_Click" Text="QuickForm Update" />
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="btnQuckFormnew" runat="server" OnClick="btnQuckFormnew_Click"
                                    CausesValidation="true" Text="BridghSelect" Visible="false" />
                                <a class="btn btn-info" runat="server" id="btnDownloadPickList" onserverclick="btnDownloadPickList_Click" visible="false"><i class="fa fa-download" style="font-size: 12px;"></i>Download PickList</a>
                           
                               <asp:Button ID="btnaddnew" runat="server" CausesValidation="false" CssClass="btn btn-info redreq dnone" 
                                   OnClick="btnaddnew_Click1" Text="New PickList" Visible="false" />
                                                   
                            
                            </div>

                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>


        <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData1">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div_popup" class="modal_popup" align="" style="width: 500px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="lbltitle"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                    Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H1">Update Address</h3>
                                        </div>

                                        <%--   <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                            <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                        </asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class="col-md-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="">
                                                            First Name 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtUpdateFirstName" runat="server" MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Last Name 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtUpdateLastName"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-sm-12">
                                                <div class="col-sm-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Mobile 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtContMobile"
                                                            ValidationGroup="updateaddress" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>

                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Phone 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtCustPhone"
                                                            ValidationGroup="updateaddress" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                            ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>

                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 form-group" id="divEmail" runat="server">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Email 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                            ValidationGroup="updateaddress" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>

                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-sm-12">
                                                <div class=" col-sm-6 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Unit No 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtUnitNo"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Unit Type 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:DropDownList ID="ddlUnitType" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-sm-12">
                                                <div class="col-sm-3 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Street No 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtstreetno"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Street Name 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtStreetName"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Street Type 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:DropDownList ID="ddlstreetType" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="col-sm-4 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            City 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtCity"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            State 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtState"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class=" col-sm-4 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Post Code 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtpostcode"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <%-- <div class="col-sm-12">--%>

                                            <div class=" col-sm-12 center-text form-group">
                                                <span class="">
                                                    <label class="">
                                                        &nbsp;
                                                    </label>
                                                </span><span class="">
                                                    <asp:Button class="btn btn-primary btnsaveicon" ID="btnUpdateAddressData" runat="server" OnClick="btnUpdateAddressData_Click"
                                                        CausesValidation="true" Text="Save" />

                                                </span>

                                                <div class="clear">
                                                </div>
                                            </div>
                                            <%-- </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:Button ID="Button2" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
            OkControlID="btnOKAddress" TargetControlID="Button2">
        </cc1:ModalPopupExtender>
        <div id="divAddressCheck" runat="server" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close</button>
                        </div>
                        <h4 class="modal-title" id="H4">Duplicate Address</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                <tbody>
                                    <tr align="center">
                                        <td>
                                            <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="tablescrolldiv">
                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                    <asp:GridView ID="rptaddress" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptaddress_PageIndexChanging"
                                        PagerStyle-CssClass="gridpagination" AllowSorting="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Customers">
                                                <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate><%# Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:Button ID="Button15" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender5" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="div10" CancelControlID="Button18"
            OkControlID="btnOKAddress" TargetControlID="Button15">
        </cc1:ModalPopupExtender>
        <div id="div10" runat="server" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="Button18" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        
                        </div>
                        <h4 class="modal-title" id="H4"></h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                <tbody>
                                    <tr align="center">
                                        <td>
                                            <asp:Label ID="lblmsg" runat="server">
                                            </asp:Label>
                                            <h3 class="noline marbtmzero"> </h3>
                                        </td>
                                         
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:Button ID="btnNULLStock" Style="display: none;" runat="server" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="hdnclosepopup" runat="server" />
        <!-- Region For Stock New Changes Pick List log--->
        <asp:Button ID="Button5" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender4" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divStock" TargetControlID="Button5"
            CancelControlID="btnCancelStock">
        </cc1:ModalPopupExtender>
        <div id="divStock" class="modal_popup" runat="server" style="display: none">
            <div class="modal-dialog" style="width: 1000px">
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="btnCancelStock" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        </div>
                        <h4 class="modal-title" id="H2">Add PickList Stock Item</h4>
                    </div>
                    <div class="finaladdupdate printorder">
                        <div id="Div1" runat="server" visible="true">
                            <div class="alert alert-danger" id="divminStockModel4" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock,please Remove Item</strong>
                                </div>
                            <div class="alert alert-danger" id="DivPanel1" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock For Panel,please Remove Item</strong>
                                </div>
                            <div class="alert alert-danger" id="DivInverter1" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock  For Inverter,please Remove Item</strong>
                                </div>

                    </div>
                            <div class="graybgarea" style="padding: 10px 15px;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group wdth200">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Installer
                                                        </label>
                                                    </span>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlinstall2" runat="server" CausesValidation="false" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlinstall2_SelectedIndexChanged1"
                                                            AutoPostBack="true" aria-controls="DataTables_Table_0" class="myval">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlInstaller" Display="Dynamic" ValidationGroup="preinst" InitialValue="">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                    <span style="float: left; width: 100px; padding-left: 10px;">
                                                        <asp:Button ID="Button12" runat="server" class="btn btn-primary" Text="Cust Notes"
                                                            Visible="false" Style="float: left" />
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlinstall2" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group wdth200">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Designer
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddldesign2" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <span style="float: left; width: 100px; padding-left: 10px;">
                                                <asp:Button ID="Button13" runat="server" class="btn btn-primary" Text="Compliance Cert"
                                                    Visible="false" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group wdth200">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Electrician
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlec2" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="position: relative">
                                        <asp:Repeater ID="rptattribute" runat="server" OnItemDataBound="rptattribute_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <span class="name disblock">
                                                            <label>
                                                                Stock Category
                                                            </label>
                                                        </span><span>
                                                            <asp:HiddenField ID="hdnStockCategoryId" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                            <div class="drpValidate">
                                                                <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                    ValidationGroup="Add" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <span class="name disblock">
                                                            <label>
                                                                Stock Item</label>
                                                        </span><span>
                                                            <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                            <div class="drpValidate">
                                                                <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged2" AutoPostBack="true"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                    ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <%--      <div class="form-group col-md-2">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    System Details</label>
                                                            </span><span style="width: 90%">
                                                                <asp:HiddenField ID="hdnSysDetails" runat="server" Value='<%# Eval("SysDetails") %>' />
                                                                <asp:TextBox ID="txtSysDetails" runat="server" ReadOnly="true"  CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>--%>
                                                    <div class="form-group col-md-1">
                                                        <span class="name disblock">
                                                            <label>
                                                                Quantity</label>
                                                        </span><span style="width: 60%">
                                                            <asp:HiddenField ID="hndQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                            <asp:HiddenField ID="hndqty1" runat="server" Value='<%# Eval("Qty") %>' />
                                                            <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-1">
                                                        <span class="name disblock">
                                                            <label>
                                                                Wal Qty</label>
                                                        </span>
                                                        <span style="width: 60%">
                                                            <asp:TextBox ID="txtwallateQty" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group" style="padding-top: 5px;">
                                                            <div class="padtop15">
                                                                <span class="name">
                                                                    <%--<asp:HiddenField ID="hdnStockOrderItemID" runat="server" Value='<%#Eval("StockOrderItemID") %>' />--%>
                                                                    <asp:CheckBox ID="chkdelete" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <br />
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:Button ID="litremove" runat="server" OnClick="litremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" /><br />

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div style="position: absolute; top: 0px; right: 160px;">
                                            <div class="form-group" style="margin-top: 23px;">
                                                <asp:Button ID="btnAddRow" runat="server" Text="Add" OnClick="btnAddRow_Click" CssClass="btn btn-info redreq"
                                                    CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Reason for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Width="290px" Height="80px"
                                                    CssClass="form-control"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="" ControlToValidate="TextBox1"
                                            Display="Dynamic" ValidationGroup="picklistagain"></asp:RequiredFieldValidator>--%>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Note for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="TextBox2" TextMode="MultiLine" runat="server" Width="290px" Height="80px"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBox2"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="picklistagain" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Install Date:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="TextBox5" runat="server" class="form-control" Width="125px" Enabled="false"></asp:TextBox>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: left; padding-left: 0px; padding-top: 3px;">
                                            <asp:Button ID="btnAdd" runat="server" Text="Save" OnClick="btnAdd_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <!--  End Region For Stock New Changes--->
        <!-- Region For Stock New Changes Pick List log--->
        <asp:Button ID="Button6" Style="display: none;" runat="server" />
        <asp:HiddenField ID="hdnPickId" runat="server" />
        <asp:HiddenField ID="hndAccreditation" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderPickListAgain" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="div3" TargetControlID="Button6"
            CancelControlID="Button7">
        </cc1:ModalPopupExtender>
        <div id="div3" class="modal_popup" runat="server" style="display: none">
            <div class="modal-dialog" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="Button7" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        </div>
                        <h4 class="modal-title" id="H3">Add PickList Stock Item Again</h4>
                    </div>
                    <div class="alert alert-danger" id="divMinStock" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock,please Remove Item</strong>
                    </div>
                    <div class="alert alert-danger" id="divpnl" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock for Panel,please Remove Item</strong>
                    </div>
                    <div class="alert alert-danger" id="divin" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock For Inverter,please Remove Item</strong>
                    </div>
                    <div class="finaladdupdate printorder">

                        <div id="Div4" runat="server" visible="true">
                            <div class="graybgarea" style="padding: 10px 15px;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group wdth200">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Installer
                                                        </label>
                                                    </span>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlInstaller1" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlInstaller1_SelectedIndexChanged"
                                                            AutoPostBack="true" aria-controls="DataTables_Table_0" class="myval">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlInstaller1" Display="Dynamic" ValidationGroup="picklistagain" InitialValue="">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                    <span style="float: left; width: 100px; padding-left: 10px;">
                                                        <asp:Button ID="Button10" runat="server" class="btn btn-primary" Text="Cust Notes"
                                                            Visible="false" Style="float: left" />
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlInstaller1" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group wdth200">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Designer
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddldesigner1" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <span style="float: left; width: 100px; padding-left: 10px;">
                                                <asp:Button ID="Button11" runat="server" class="btn btn-primary" Text="Compliance Cert"
                                                    Visible="false" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group wdth200">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Electrician
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlec" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="position: relative">
                                        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <span class="name disblock">
                                                            <label>
                                                                Stock Category
                                                            </label>
                                                        </span><span>
                                                            <asp:HiddenField ID="hdnStockCategoryId" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                            <%--<asp:HiddenField ID="HiddenField4" runat="server" Value='<%# Eval("PicklstItemId") %>'/> --%>
                                                            <%--<asp:HiddenField ID="hdnPickListItemId" runat="server" Value='<%# Eval("PicklistitemId") %>' />--%>
                                                            <div class="drpValidate">
                                                                <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged1"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                    ValidationGroup="picklistagain" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <span class="name disblock">
                                                            <label>
                                                                Stock Item</label>
                                                        </span><span>
                                                            <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                            <asp:HiddenField ID="HiddenField3" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                            <div class="drpValidate">
                                                                <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged1" AutoPostBack="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                    ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="picklistagain"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-1">
                                                        <span class="name disblock">
                                                            <label>
                                                                Quantity</label>
                                                        </span><span style="width: 60%">
                                                            <asp:HiddenField ID="hdbQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                            <asp:HiddenField ID="hndqtynew" runat="server" Value='<%# Eval("Qty") %>' />
                                                            <asp:TextBox ID="txtQuantity" ValidationGroup="picklistagain" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-1">
                                                        <span class="name disblock">
                                                            <label>
                                                                Wal Qty</label>
                                                        </span>
                                                        <span style="width: 60%">
                                                            <asp:TextBox ID="txtwallateQty" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="padtop15">
                                                                <span class="name set_with1" style="padding-top: 5px;">
                                                                    <%--<asp:HiddenField ID="hdnStockOrderItemID" runat="server" Value='<%#Eval("StockOrderItemID") %>' />--%>
                                                                    <asp:CheckBox ID="chkdelete" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <br />
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:HiddenField ID="hndPckListItemId" runat="server" Value='<%#Eval("PicklstItemId")%>' />
                                                                    <asp:Button ID="litremove" runat="server" OnClick="litremove_Click1" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" /><br />

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div style="position: absolute; top: 0px; right: 160px;">
                                            <div class="form-group" style="margin-top: 23px;">
                                                <asp:Button ID="btnAddRowagain" runat="server" Text="Add" OnClick="btnAddRowagain_Click" CssClass="btn btn-info redreq"
                                                    CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Reason for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="txtpicklistreason" runat="server" TextMode="MultiLine" Width="290px" Height="80px"
                                                    CssClass="form-control"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtpicklistreason"
                                            Display="Dynamic" ValidationGroup="picklistagain"></asp:RequiredFieldValidator>--%>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Note for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="txtpicklistnote" TextMode="MultiLine" runat="server" Width="290px" Height="80px"></asp:TextBox>

                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Install Date:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="TextBox6" runat="server" class="form-control" Width="125px" Enabled="false"></asp:TextBox>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>



                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: left; padding-top: 3px;">
                                            <asp:Button ID="btnaddagain" runat="server" Text="Save" OnClick="btnaddagain_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="true" ValidationGroup="picklistagain" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: left; padding-top: 3px;">
                                            <asp:Button ID="btnupdate" runat="server" Autopostback="true" Text="Update" OnClick="btnupdate_Click" Visible="false" CssClass="btn btn-info redreq"
                                                CausesValidation="false" ValidationGroup="picklistagain" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  End Region For Stock New Changes--->

        <!-- Region For Insert Into Wallet--->
        <asp:Button ID="Button8" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderwallet" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divWallet" TargetControlID="Button8"
            CancelControlID="Button9">
        </cc1:ModalPopupExtender>
        <div id="divWallet" class="modal_popup" runat="server" style="display: none">
            <div class="modal-dialog" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="Button9" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        </div>
                        <h4 class="modal-title" id="H3">Insert Into Wallat</h4>
                    </div>
                    <div class="finaladdupdate printorder">
                        <div id="Div6" runat="server" visible="true">
                            <div class="graybgarea" style="padding: 10px 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Repeater ID="repwallet" runat="server" OnItemDataBound="repwallet_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Category
                                                                </label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockCategoryId" runat="server" Value='<%# Eval("StockCategoryID") %>' />

                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged1"
                                                                        AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                        ValidationGroup="picklistagain" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Item</label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="picklistagain"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div>
                                                            <div class="form-group col-md-3">
                                                                <div>
                                                                    <span class="name disblock">
                                                                        <label>
                                                                            P.Qty</label>
                                                                    </span><span style="width: 60%">
                                                                        <asp:HiddenField ID="hdbQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                                        <asp:HiddenField ID="hndqtynew" runat="server" Value='<%# Eval("Qty") %>' />
                                                                        <asp:TextBox ID="txtQuantity" ValidationGroup="picklistagain" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <div class="form-group">
                                                                    <div>
                                                                        <span class="name">
                                                                            <label>
                                                                                Out Qty</label>
                                                                        </span><span style="width: 100%">
                                                                            <asp:HiddenField ID="hnddiiffqty" runat="server" Value='<%#Convert.ToInt32(Eval("InstallQty")) %>' />
                                                                            <asp:HiddenField ID="hndInstallaQty" runat="server" Value='<%# Convert.ToInt32(Eval("InstallQty")) %>' />
                                                                            <asp:TextBox ID="txtdiffqty" Enabled="false" ValidationGroup="picklistagain" Text='<%#Convert.ToInt32(Eval("InstallQty")) %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <div class="form-group">
                                                                    <div>
                                                                        <span class="name">
                                                                            <label>
                                                                                Install Qty</label>
                                                                        </span><span style="width: 100%">
                                                                            <asp:TextBox ID="txtinstallQty" ValidationGroup="picklistagain" Text="" runat="server" CssClass="form-control" OnTextChanged="txtinstallQty_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <div class="form-group">
                                                                    <div>
                                                                        <span class="name">
                                                                            <label>
                                                                                Wallet Qty</label>
                                                                        </span><span style="width: 100%">
                                                                            <asp:TextBox ID="txtwalletqty" Enabled="false" AutoPostBack="true" ValidationGroup="picklistagain" runat="server" onkeypress="return isNumberKey(event)" OnTextChanged="txtwalletqty_TextChanged" CssClass="form-control"></asp:TextBox>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-top: 23px;">
                                            <asp:Button ID="Btnaddmore" runat="server" Text="Add" OnClick="Btnaddmore_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="col-md-12 form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Reason for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Width="290px" Height="80px"
                                                    CssClass="form-control"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtpicklistreason"
                                            Display="Dynamic" ValidationGroup="picklistagain"></asp:RequiredFieldValidator>--%>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-12 form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Note for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="TextBox4" TextMode="MultiLine" runat="server" Width="290px" Height="80px" CssClass="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtpicklistnote"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="picklistagain" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: left; padding-left: 15px; padding-top: 3px;">
                                            <asp:Button ID="btnInserwallet" runat="server" Autopostback="true" Text="Add Wallet" OnClick="btnInserwallet_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="false" ValidationGroup="picklistagain" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  End Region For Stock New Changes--->

        <asp:Button ID="Button3" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="modalstockupdate" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="LinkButton1" DropShadow="false" PopupControlID="div2" TargetControlID="Button3">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div2" class="modal_popup" align="" style="width: 350px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="Label1"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="LinkButton1" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                                 Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H145">Stock Update</h3>
                                        </div>

                                        <%--   <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                            <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                        </asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Stock Status 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:DropDownList ID="ddlstockstatus" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select myval" AutoPostBack="true" OnSelectedIndexChanged="ddlstockstatus_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">Return Stock</asp:ListItem>
                                                            <asp:ListItem Value="2">Job Cancelled</asp:ListItem>
                                                            <asp:ListItem Value="3">Stock Swapped</asp:ListItem>
                                                            <asp:ListItem Value="4">Stock with Int.</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlstockstatus" Display="Dynamic" ValidationGroup="stockupdate" InitialValue=""> </asp:RequiredFieldValidator>
                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Date 
                                                            </label>
                                                        </span><span class="">
                                                            <div class="input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtDate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate"
                                                                    ErrorMessage="" CssClass="reqerror" ValidationGroup="stockupdate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Notes
                                                            </label>
                                                        </span><span class="">
                                                            <asp:TextBox ID="txtNotes" TextMode="MultiLine" runat="server" Width="290px" Height="80px"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="reqvaltxtNotesName" runat="server" ControlToValidate="txtNotes"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="stockupdate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div runat="server" class="col-sm-12" visible="false" id="txtprono">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Project Number
                                                            </label>
                                                        </span><span class="">
                                                            <asp:TextBox ID="txtProjNo" runat="server" class="form-control"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtProjNo">
                                                            </cc1:FilteredTextBoxExtender>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtProjNo"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="stockupdate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class=" col-sm-12 center-text form-group">
                                                    <span class="">
                                                        <label class="">
                                                            &nbsp;
                                                        </label>
                                                    </span><span class="">
                                                        <asp:Button CausesValidation="true" ValidationGroup="stockupdate" class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="Button1" runat="server" OnClick="Button1_Click"
                                                            Text="Save" />

                                                    </span>
                                                    <asp:Button CausesValidation="true" ValidationGroup="stockupdate" class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="Button4" runat="server" OnClick="Button4_Click"
                                                        Text="Save" />
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="Button21" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender8" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="Div14" DropShadow="false" CancelControlID="LinkButton10" OkControlID="btnOKMobile" TargetControlID="Button21">
        </cc1:ModalPopupExtender>
        <div id="Div14" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Picklist For Draft</div>
                    <label id="Label13" runat="server"></label>
                    <div class="modal-body ">Want To Generate PickList For Draft?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="LnkUpdate" runat="server" CausesValidation="false"  OnClick="LnkUpdate_Click" class="btn btn-danger">OK</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton10" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>


        <asp:Button ID="Button20" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender7" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="Div13" DropShadow="false" CancelControlID="LinkButton7" OkControlID="btnOKMobile" TargetControlID="Button19">
        </cc1:ModalPopupExtender>
        <div id="Div13" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Picklist For Draft</div>
                    <label id="Label12" runat="server"></label>
                    <div class="modal-body ">Want To Generate PickList For Draft?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="false"  OnClick="LinkButton6_Click" class="btn btn-danger">OK</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton8" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:Button ID="Button19" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender6" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="Div12" DropShadow="false" CancelControlID="LinkButton7" OkControlID="btnOKMobile" TargetControlID="Button19">
        </cc1:ModalPopupExtender>
        <div id="Div12" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Picklist For Draft</div>
                    <label id="Label11" runat="server"></label>
                    <div class="modal-body ">Want To Generate PickList For Draft?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkDraft" runat="server" CausesValidation="false"  OnClick="lnkDraft_Click" class="btn btn-danger">OK</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton7" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>
        <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hndpicklistid" runat="server" />

        <!--End Danger Modal Templates-->
        <%-- </div>
        </div>--%>

        <!--Modal Templates For Picklist Notes-->

        <asp:Button ID="Button14" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderNotes" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="LinkButton1" DropShadow="false" PopupControlID="divnotes" TargetControlID="Button14">
        </cc1:ModalPopupExtender>
        <div runat="server" id="divnotes" class="modal_popup" align="" style="width: 700px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="Label3"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="LinkButton2" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                                 Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H145">Notes</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Notes
                                                            </label>
                                                        </span>
                                                        <span class="left-text">
                                                            <asp:TextBox ID="txtpicknote" TextMode="MultiLine" runat="server" Width="400px" Height="80px"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtpicknote"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="note" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" col-sm-12 center-text form-group">
                                                    <span class="">
                                                        <label class="">
                                                            &nbsp;
                                                        </label>
                                                    </span><span class="">
                                                        <asp:Button CausesValidation="true" ValidationGroup="note" class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="btnnote" runat="server" OnClick="btnnote_Click"
                                                            Text="Save" />
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class=" col-sm-12 center-text form-group">
                                                    <div class="row finalgrid">
                                                        <div class="col-md-12" runat="server" id="div5">
                                                            <table width="100%" cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 10%; text-align: center;">ID
                                                                        </th>
                                                                        <th style="width: 70%; text-align: center; text-align: left;">Notes
                                                                        </th>
                                                                        <th style="width: 20%; text-align: center; text-align: left;"></th>
                                                                    </tr>
                                                                </thead>
                                                                <asp:Repeater ID="rptPicklistNotes" runat="server" OnItemCommand="rptPicklistNotes_ItemCommand">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label runat="server" ID="lblid" Text='<%#Eval("id")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 70%; text-align: left;">
                                                                                <asp:Label runat="server" ID="Label4" Text='<%#Eval("Notes")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; text-align: center;">
                                                                                <asp:LinkButton runat="server" ID="btndelete" Text="Delete" CssClass="btn btn-labeled btn-primary" Style="width: 70px; text-align: center;" CommandName="delete" CommandArgument='<%#Eval("id")%>'></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--End Danger Modal Templates-->

        <!--Modal Templates For Order Detail-->

        <asp:Button ID="Button89" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderOrder" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="LinkButton1" DropShadow="false" PopupControlID="divOrderDetail" TargetControlID="Button89">
        </cc1:ModalPopupExtender>
        <div runat="server" id="divOrderDetail" class="modal_popup" align="" style="width: 700px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="Label5"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="LinkButton3" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                                 Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H146">
                                                <asp:Label runat="server" ID="lblname"></asp:Label></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class=" col-sm-12 center-text form-group">
                                                    <div class="row finalgrid">
                                                        <div class="col-md-12" runat="server" id="div8">
                                                            <table width="100%" cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 25%; text-align: center;">InvNo/Wid
                                                                        </th>
                                                                        <th style="width: 50%; text-align: center; text-align: left;">Order Item
                                                                        </th>
                                                                        <th style="width: 25%; text-align: center; text-align: left;">Quantity
                                                                        </th>
                                                                       <th style="width: 25%; text-align: center; text-align: left;">Company Name
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <asp:Repeater ID="rptOrderDetil" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label runat="server" ID="lblStockOrderID" Text='<%#Eval("InvoiceNo")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 70%; text-align: left;">
                                                                                <asp:Label runat="server" ID="lblStockOrderItem" Text='<%#Eval("StockOrderItem")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; text-align: center;">
                                                                                <asp:Label runat="server" ID="lblOrderQuantity" Text='<%#Eval("OrderQuantity")%>'></asp:Label>
                                                                            </td>
                                                                            <%--<td style="width: 20%; text-align: center;">
                                                                                <asp:Label runat="server" ID="Label6" Text='<%#Eval("ExpDelivery","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                            </td>--%>
                                                                            
                                                                           <td style="width: 20%; text-align: center;">
                                                                                <asp:Label runat="server" ID="Label6" Text='<%#Eval("WhCust")%>'></asp:Label>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--End Danger Modal Templates-->

        <!--Modal Templates For Order Detail-->


        <!--End Danger Modal Templates-->

        <!--Modal Templates For Order Detail-->

        <%--<asp:Button ID="Button16" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="LinkButton1" DropShadow="false" PopupControlID="div7" TargetControlID="Button16">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div7" class="modal_popup" align="" style="width: 700px;">--%>
        <asp:Button ID="Button16" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="LinkButton1" DropShadow="false" PopupControlID="div7" TargetControlID="Button16">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div7" class="modal_popup" align="" style="width: 1500px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="Label7"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="LinkButton4" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                                 Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H146">
                                                <asp:Label runat="server" ID="Label8" Text="PickUp Details"></asp:Label></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;" id="Data" runat="server">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class=" col-sm-12 center-text form-group">
                                                    <div class="row finalgrid">
                                        <div class="col-md-12" runat="server" id="div9">
                                            <table width="100%" cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%; text-align: center;">ID
                                                        </th>
                                                          <th style="width: 5%; text-align: center;">Project Number 
                                                        </th>
                                                        <th style="width: 5%; text-align: center;">Generated Date 
                                                        </th>
                                                        <%--PickList Generated Date--%>
                                                        <th style="width: 5%; text-align: center">Install Date
                                                        </th>
                                                        <%--InstallationBooked Date--%>
                                                        <th style="width: 10%; text-align: center">Installer Name  
                                                        </th>
                                                        <th style="width: 20%; text-align: center">System Details  
                                                        </th>
                                                        <th style="width: 20%; text-align: center">Reason
                                                        </th>
                                                        <%-- <th style="width: 20%; text-align: center">Note
                                                        </th>--%>
                                                        <th style="width: 5%; text-align: center">CreatedBy
                                                        </th>
                                                        <%--<th style="width: 7%; text-align: center">Deduct On
                                                        </th>--%>
                                                        <th style="text-align: center">&nbsp;
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:HiddenField ID="HiddenField4" runat="server" />
                                                <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="Repeater2_ItemCommand" OnItemDataBound="Repeater2_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 5%;">
                                                                <%#Eval("ID")%>
                                                            </td>
                                                            <td style="width: 5%;">
                                                                <%#Eval("ProjectNumber")%>
                                                            </td>

                                                            <td style="width: 5%;">
                                                                <asp:HiddenField ID="hndpicklistid" runat="server" Value='<%#Eval("ID")%>' />
                                                                <%#Eval("PickListDateTime", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            <%-- <td style="width: 5%;">
                                                                <%#Eval("InstallerID")%>
                                                            </td>--%>
                                                            <td style="width: 5%;">
                                                                <%#Eval("InstallBookedDate", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            <td style="width: 10%; word-break: break-word;">
                                                                <%#Eval("InstallerName")%>
                                                            </td>
                                                            <td style="width: 20%;">
                                                                <%--<%#Eval("PanelQty")+" X " + Eval("Panel") + " Plus " + Eval("InverterQty") + " X " + Eval("Inverter")%>--%>
                                                                <%--<%#Eval("SystemDetail")%>--%>
                                                                <asp:Label ID="lblstytemdetail" runat="server"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%;">
                                                                <%#Eval("Reason")%>
                                                            </td>
                                                            <%--  <td style="width: 20%;">
                                                                <%#Eval("Note")%>
                                                            </td>--%>
                                                            <td style="width: 5%;">
                                                                <%--    <%#Eval("EmpNicName")%>--%>
                                                                <asp:HiddenField ID="hdnEmpId" runat="server" Value='<%#Eval("createdby")%>' />
                                                                 <asp:HiddenField ID="hdnCompanyid" runat="server" Value='<%#Eval("Companyid")%>' />
                                                                <asp:Label ID="lblEmpName" Width="70px" runat="server"></asp:Label>
                                                            </td>
                                                            <%--<td style="width: 7%;">
                                                                <%#Eval("DeductOn", "{0:dd/MM/yyyy}")%>
                                                            </td>--%>
                                                            <%-- <td style="width: 10%;">
                                                              <asp:HiddenField ID="hndPickLIstItemId" runat="server" Value='<%# Eval("PicklistitemId") %>' />
                                                            </td>--%>
                                                            <td>
                                                                <asp:LinkButton ID="btnUpdatepicklist" Style="width: 70px" Visible="false" runat="server" OnClick="btnUpdatepicklist_Click" CommandName="Edit" CommandArgument='<%#Eval("Id")+","+ Eval("InstallerID") %>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                Edit
                                                                </asp:LinkButton>&nbsp;&nbsp;
                                                                <asp:LinkButton ID="btnwallet" Style="width: 70px" runat="server"  OnClick="btnwallet_Click" CommandName="wallet" CommandArgument='<%#Eval("Id")+","+ Eval("InstallerID")%>' CausesValidation="false" class="btn btn-labeled btn-primary" Visible="false">
                                                                Wallet
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnpicklistDelete" Style="width: 70px" runat="server" CommandName="delete" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                Delete
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnNotes" Style="width: 70px" runat="server" Visible="false" CommandName="notes" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                Notes
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--End Danger Modal Templates-->
        <asp:Button ID="Button17" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="LinkButton1" DropShadow="false" PopupControlID="divOrStockderDetail" TargetControlID="Button17">
        </cc1:ModalPopupExtender>
        <div runat="server" id="divOrStockderDetail" class="modal_popup" align="" style="width: 500px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="Label9"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="LinkButton5" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                                 Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H146">
                                                <asp:Label runat="server" ID="Label10"></asp:Label></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class=" col-sm-12 center-text form-group">
                                                    <div class="row finalgrid">
                                                        <div class="col-md-12" runat="server" id="div11">
                                                            <table width="100%" cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 25%; text-align: center;">OrderID
                                                                        </th>
                                                                        <th style="width: 50%; text-align: center; text-align: left;">Order Item
                                                                        </th>
                                                                        <th style="width: 25%; text-align: center; text-align: left;">Quantity
                                                                        </th>
                                                                       <th style="width: 25%; text-align: center; text-align: left;">Exp.Delivery
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <asp:Repeater ID="Repeater3" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="width: 10%;">
                                                                                <asp:Label runat="server" ID="lblStockOrderID" Text='<%#Eval("StockOrderID")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 70%; text-align: left;">
                                                                                <asp:Label runat="server" ID="lblStockOrderItem" Text='<%#Eval("StockOrderItem")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; text-align: center;">
                                                                                <asp:Label runat="server" ID="lblOrderQuantity" Text='<%#Eval("OrderQuantity")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; text-align: center;">
                                                                                <asp:Label runat="server" ID="Label6" Text='<%#Eval("ExpDelivery","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        
        <style>
            .modalbackground {
                background-color: Gray;
                opacity: 0.5;
                filter: Alpha(opacity=50);
            }

            .modalpopup {
                background-color: white;
                padding: 6px 6px 6px 6px;
            }

            table.formtable h3 {
                color: #800000;
                font-size: 16px;
            }
        </style>
        <style type="text/css">
            .modalPopup {
                background-color: #696969;
                filter: alpha(opacity=40);
                opacity: 0.7;
                z-index: -1;
            }
        </style>
        <script>
            function validateformbaystreetAddress(source, args) {
                var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
                $.ajax({
                    type: "POST",
                    //action:"continue.aspx",
                    url: "company.aspx/Getstreetname",
                    data: "{'streetname':'" + streetname + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == true) {
                            document.getElementById("Divvalidstreetname1").style.display = "block";
                            document.getElementById("DivInvalidstreetname1").style.display = "none";
                        }
                        else {
                            document.getElementById("Divvalidstreetname1").style.display = "none";
                            document.getElementById("DivInvalidstreetname1").style.display = "block";
                        }
                    }
                });
            }

        <%--    function btnSumbit_ClientClick() {
                //alert("fstst");
                formValidate();
                //$('#<%=picklistdiv.ClientID %>').hide();
            }--%>

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpreinst);
            function pageLoadedpreinst() {
                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });
            }

        </script>


        <script>

</script>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpdatePreInst" />
        <asp:PostBackTrigger ControlID="btnPickList" />
        <asp:PostBackTrigger ControlID="btnDownload" />
        <asp:PostBackTrigger ControlID="btnRemoveInst" />
        <asp:PostBackTrigger ControlID="btnMove" />
        <asp:PostBackTrigger ControlID="btnCreateSTCForm" />
        <asp:PostBackTrigger ControlID="btnformbay1" />
        <asp:PostBackTrigger ControlID="btngreenbot" />
        <asp:PostBackTrigger ControlID="btnInserwallet" />
        <asp:PostBackTrigger ControlID="btnUpdateAddressData" />
        <asp:PostBackTrigger ControlID="btnaddnew" />
        <asp:PostBackTrigger ControlID="btnDownloadPickList" />

  <asp:PostBackTrigger ControlID="lnkDraft" />
        <%--  <asp:AsyncPostBackTrigger ControlID="updatepanelgrid" />--%>
        <%--<asp:PostBackTrigger ControlID="btnQuckFormnew" />--%>
    </Triggers>
</asp:UpdatePanel>
