﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_controls_InvoiceDetails : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnOpenInvoice_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.ProjectTypeID == "8")
        {
            Telerik_reports.generate_mtceinvoice(ProjectID, "Download");
        }
        else
        {
            Telerik_reports.generate_Taxinvoice(ProjectID, "Download");
        }
        //BindScript();
    }

    protected void btnPrintReceipt_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        if (ProjectID != string.Empty)
        {
            Telerik_reports.generate_receipt(ProjectID);
        }
        //BindScript();
    }

}