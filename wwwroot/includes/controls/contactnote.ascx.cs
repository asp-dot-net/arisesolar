﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
 
public partial class includes_controls_contactnote : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        HidePanels();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>doMyAction();</script>");
    }
    public void BindContactNote()
    {
        //HidePanels();
        divrightbtn.Visible = true;
        PanGrid.Visible = false;
        ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        ddlSelectRecords.DataBind();
        BindGrid(0);

        if ((Roles.IsUserInRole("Accountant")) || (Roles.IsUserInRole("BookInstallation")) || (Roles.IsUserInRole("Installer")) || (Roles.IsUserInRole("Maintenance")) || (Roles.IsUserInRole("PostInstaller")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Verification")))
        {
            divrightbtn.Visible = false;
        }
    }

    protected DataTable GetGridData()
    {
        string id = "";
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string NoteSetBy = st.EmployeeID;

        if (!string.IsNullOrEmpty(Request.QueryString["contid"]))
        {
            id = Request.QueryString["contid"];
        }

        DataTable dt = ClstblContNotes.tblContNotes_SelectByContactID(id, NoteSetBy);


        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ContactID = Request.QueryString["contid"];
        SttblContacts stcontact = ClstblContacts.tblContacts_SelectByContactID(ContactID);
        string EmployeeID = stcontact.EmployeeID;
        string ContNote = txtnote.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string NoteSetBy = st.EmployeeID;

        int success = ClstblContNotes.tblContNotes_Insert(ContactID, EmployeeID, NoteSetBy, ContNote);
        //--- do not chage this code start------
        if (Convert.ToString(success) != "")
        {
            SetAdd();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
        divrightbtn.Visible = true;

        //--- do not chage this code end------
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
      
    }

    protected void ddlcategorysearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(pancontactnote,this.GetType(),"MyAction","doMyAction();",true);
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(pancontactnote,this.GetType(),"MyAction","doMyAction();",true);
        txtnote.Focus();
        InitAdd();
        divrightbtn.Visible = true;

        PanSearch.Visible = false;
        PanGrid.Visible = false;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(pancontactnote,
                                             this.GetType(),
                                             "MyAction",
                                             "doMyAction();",
                                             true);
        Reset();
        PanAddUpdate.Visible = false;
        lnkAdd.Visible = true;
        PanSearch.Visible = true;
        PanGrid.Visible = true;
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetAdd()
    {
        Reset();
        HidePanels();
        PanSearch.Visible = true;
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        btnAdd.Visible = true;
        btnReset.Visible = true;
        //   btnCancel.Visible = false;

    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = true;
        btnReset.Visible = true;
        // btnCancel.Visible = true;
        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = false;

        btnReset.Visible = false;
        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        divrightbtn.Visible = true;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
        PanAddUpdate.Visible = false;
        PanSearch.Visible = true;
        PanGrid.Visible = true;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtnote.Text = string.Empty;
        divrightbtn.Visible = true;
        PanSearch.Visible = false;
        PanGrid.Visible = false;
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblContNotes.tblContNotes_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        Reset();
        PanAddUpdate.Visible = false;
        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        //--- do not chage this code end------
    }
}