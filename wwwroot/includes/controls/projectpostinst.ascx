<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectpostinst.ascx.cs"
    Inherits="includes_controls_projectpostinst" %>
<%@ Register Src="../InvoicePayments.ascx" TagName="InvoicePayments" TagPrefix="uc1" %>

<script type="text/javascript">
    $(document).ready(function () {
        //HighlightControlToValidate();

    });

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadeddetail);
    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
    prm.add_beginRequest(beginrequesthandler);
    // raised after an asynchronous postback is finished and control has been returned to the browser.
    prm.add_endRequest(endrequesthandler);

    function beginrequesthandler(sender, args) {
        //shows the modal popup - the update progress

    }
    function endrequesthandler(sender, args) {
        //hide the modal popup - the update progress

        //if (args.get_error() != undefined) {
        //    args.set_errorhandled(true);
        //}

    }
    function pageLoadeddetail() {
        $('#<%=btnUpdatePostInst.ClientID %>').click(function () {
            formValidate();
        });


        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });

              {
            // NOTE: this block is to get scroll dynamically for more than 3 record as row's height won't be
            //be of fixed value.
            var row_count1 = $("#tblreasontochange").find('tr').length;
            var table_height = 0;
            //alert(table_height);
            for (var i = 0; i <= 2; i++) {
                table_height = table_height + $("#tblreasontochange tr").eq(i).height();
            }
            if (row_count1 > 2) {
                $('#<%=divreasontochange.ClientID %>').attr("style", "overflow-y: scroll; height: " + table_height + "px !important;");
            }
            else {
                $('#<%=divreasontochange.ClientID %>').attr("style", "");
            }
        }
    }

    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "red");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {Time
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea stcpage">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                        <div class="alert alert-danger" id="DivDocNotVerified" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Your documents are not verified.</strong>
                        </div>
                        <div class="alert alert-danger" id="divActive" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Your Project is not Active.</strong>
                        </div>
                        <div class="alert alert-danger" id="divStockAssign" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Stock is not assigned.</strong>
                        </div>
                        <div class="alert alert-danger" id="divInstComp" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Installer has already three jobs.</strong>
                        </div>
                        <div class="alert alert-danger" id="divinstbookdate" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Please Enter Installation Booking Date.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Panel ID="Panel5" runat="server">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Stock Pickup Detail</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group wdth211">
                                                            <span class="name">
                                                                <asp:Label ID="Label14" runat="server" class="control-label">
                                                            Pickup Date</asp:Label></span>
                                                            <div class="input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtpickupdate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" ControlToValidate="txtpickupdate"
                                                                    Display="Dynamic" ValidationGroup="postinst"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group wdth230">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Installer Name
                                                                </label>
                                                            </span>
                                                            <span style="width: 200px;">
                                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                                    aria-controls="DataTables_Table_0" class="myval">
                                                                    <asp:ListItem Value="0">Installer Name</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" ControlToValidate="ddlInstaller"
                                                                    InitialValue="" Display="Dynamic" ValidationGroup="postinst"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group wdth200">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Pickup Note
                                                                </label>
                                                            </span><span style="width: 200px;">
                                                                <asp:TextBox ID="txtPickupNote" runat="server" TextMode="MultiLine" Height="70px"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group wdth211">
                                                            <span class="name">
                                                                <asp:Label ID="Label15" runat="server" class="control-label">
                                                            Pickup Return Date</asp:Label></span>
                                                            <div class="input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtPickupReturnDate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group wdth230">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Installer
                                                                </label>
                                                            </span><span style="width: 200px;">
                                                                <asp:DropDownList ID="ddlInstaller2" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" class="myval">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="" ControlToValidate="ddlOption"
                                                            InitialValue="" Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group wdth200">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Pickup Return Note
                                                                </label>
                                                            </span><span style="width: 200px;">
                                                                <asp:TextBox ID="txtPickupReturnNote" runat="server" TextMode="MultiLine" Height="70px"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="divChangePickUp" runat="server" style="margin-top:5px;">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <%--<div class="row">
                                                                <div class="col-md-5 col-sm-5">
                                                                    <h5> Log</h5>
                                                                </div>
                                                            </div>--%>
                                                            <div class="row finalgrid">
                                                                <div class="col-md-12" runat="server" id="divreasontochange">
                                                                    <table width="100%" cellpadding="0" id="tblreasontochange" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 15%; text-align: center">PickUpDate
                                                                                </th>
                                                                                <th style="width: 35%; text-align: center">Reason
                                                                                </th>
                                                                                <th style="width: 35%; text-align: center">Note
                                                                                </th>
                                                                                <th style="width: 15%; text-align: center">CreatedBy
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <asp:Repeater ID="rptChangePickUp" runat="server">
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td style="width: 15%;">
                                                                                        <%#Eval("PickUpDate", "{0:dd/MM/yyyy}")%>
                                                                                    </td>
                                                                                    <td style="width: 35%;">
                                                                                        <%#Eval("Reason")%>
                                                                                    </td>
                                                                                    <td style="width: 35%;">
                                                                                        <%#Eval("Notes")%>
                                                                                    </td>
                                                                                    <td style="width: 15%;">
                                                                                        <%#Eval("EmpNicName")%>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="Panel1" runat="server">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Installation detail</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group wdth211">
                                                            <span class="name">
                                                                <asp:Label ID="Label23" runat="server" class="control-label">
                                                            Install Completed</asp:Label></span>
                                                            <div class="input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtInstallCompleted" runat="server" class="form-control" ReadOnly="true">
                                                                </asp:TextBox>
                                                                <asp:CompareValidator ID="cmpNextDate" runat="server" ControlToValidate="txtInstallCompleted" Visible="false"
                                                                    ErrorMessage="Installation EndDate cannot be less than Installation Booking Date date" Operator="GreaterThanEqual"
                                                                    Type="Date" Display="Dynamic"></asp:CompareValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorIC" runat="server" ErrorMessage="*" Visible="false"
                                                                    ValidationGroup="postinst" SetFocusOnError="true" ControlToValidate="txtInstallCompleted" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <asp:Label runat="server" ID="lblinstall_status"></asp:Label>
                                                                <asp:Button runat="server" ID="btninstallstatus" class="btn btn-primary" Text="Get Status" OnClick="btninstallstatus_Click" />
                                                                <asp:LinkButton runat="server" ID="lnkgetpanelnumbers" OnClick="lnkgetpanelnumbers_Click" Text="Show Series"></asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group wdth211" runat="server" visible="false">
                                                            <span class="name">
                                                                <asp:Label ID="Label2" runat="server" class="control-label">
                                                        Install Docs Rec</asp:Label></span>
                                                            <div class="input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtInstallDocsReceived" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPR" runat="server" ErrorMessage="This value is required." Visible="false"
                                                                    ValidationGroup="postinst" ControlToValidate="txtInstallDocsReceived" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>


                                                        <div class="form-group wdth211" runat="server" visible="false">
                                                            <span class="name">
                                                                <asp:Label ID="Label3" runat="server" class="control-label">
                                                         Certificate Issued</asp:Label></span>
                                                            <div class="input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtCertificateIssued" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <%--<div class="form-group dateimgarea">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Certificate Issued
                                                            </label>
                                                        </span><span class="dateimg">
                                                            <asp:TextBox ID="txtCertificateIssued" runat="server" Width="100px" CssClass="form-control"></asp:TextBox>
                                                            <asp:ImageButton ID="Image25" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                            <cc1:CalendarExtender ID="CalendarExtender25" runat="server" PopupButtonID="Image25"
                                                                TargetControlID="txtCertificateIssued" Format="dd/MM/yyyy">
                                                            </cc1:CalendarExtender>
                                                            <asp:RegularExpressionValidator ValidationGroup="postinst" ControlToValidate="txtCertificateIssued"
                                                                ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>--%>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group" style="margin-top: 30px; margin-bottom: 22px;">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkSTCFormsDone.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkSTCFormsDone" runat="server" />
                                                                        <span class="text">STC Checked  &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>

                                                        <%--<div class="form-group chkwidth">
                                                        <span class="name"></span><span>
                                                            <asp:CheckBox ID="chkSTCFormsDone" runat="server" />
                                                            <label for="<%=chkSTCFormsDone.ClientID %>">
                                                                <span></span>
                                                            </label>
                                                            <label class="control-label">
                                                                STC Checked
                                                            </label>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>--%>
                                                        <div class="form-group wdth210" runat="server" visible="false">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    By
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <asp:DropDownList ID="ddlSTCCheckedBy" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group " id="divProjectInCompReason" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Incomplete Reason
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtProjectInCompReason" runat="server" TextMode="MultiLine" Width="250px" Height="50px"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group wdth210" runat="server" visible="false">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Install Verified By
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlInstallVerifiedBy" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" class="myval">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">



                                                        <div class="form-group" style="margin-top: 30px; margin-bottom: 54px;">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkProjectIncomplete.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkProjectIncomplete" runat="server" OnCheckedChanged="chkProjectIncomplete_CheckedChanged" AutoPostBack="true" />
                                                                        <span class="text">Project Incomplete  &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>

                                                            <div class="clear">
                                                            </div>
                                                        </div>


                                                        <div class="form-group" style="margin-top: 30px; margin-bottom: 50px;" runat="server" visible="false">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkNoDocs.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkNoDocs" runat="server" />
                                                                        <span class="text">No Docs  &nbsp;</span>
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>


                                                        <div class="form-group" style="margin-top: 30px; margin-bottom: 22px;" runat="server" visible="false">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkDocsDone.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkDocsDone" runat="server" />
                                                                        <span class="text">Docs Done  &nbsp;</span>
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div id="sscomplete" runat="server" visible="false">
                                                            <div class="form-group wdth211">
                                                                <span class="name">
                                                                    <asp:Label ID="Label12" runat="server" class="control-label">
                                                            SS Complete Date</asp:Label></span>
                                                                <div class="input-group date datetimepicker1">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtsscompletedate" runat="server" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <span>
                                                                <asp:Button CausesValidation="false" class="btn btn-primary savewhiteicon" ID="btnSave" runat="server" OnClick="btnSave_Click" ValidationGroup="quote" Text="Solar Service Complete" />
                                                                <div class="clear">
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="Panel4" runat="server" Visible="false">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <!-- <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption"></span>
                                        </div>-->
                                            <div class="widget-body">
                                                <div class="row">
                                                    <asp:Panel ID="PanelEdit" runat="server">
                                                        <div class="col-md-12 spicalpaddbtmnone">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group " style="margin-bottom: 0px!important">
                                                                        <span class="name disblockcss"></span><span>
                                                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                                <tr>
                                                                                    <td valign="top" width="10">

                                                                                        <div class="form-group">
                                                                                            <span></span>
                                                                                            <div class=" checkbox-info checkbox">
                                                                                                <span class="fistname">
                                                                                                    <label for="<%=chkSTCFormSaved.ClientID %>" class="control-label">
                                                                                                        <asp:CheckBox ID="chkSTCFormSaved" runat="server" AutoPostBack="true" OnCheckedChanged="chkSTCFormSaved_CheckedChanged" />
                                                                                                        <span class="text">STC Form Saved &nbsp;</span>
                                                                                                    </label>
                                                                                                </span>
                                                                                            </div>
                                                                                            <div class="clear">
                                                                                            </div>
                                                                                        </div>


                                                                                        <%--  <asp:CheckBox ID="chkSTCFormSaved" runat="server" AutoPostBack="true" OnCheckedChanged="chkSTCFormSaved_CheckedChanged" />
                                                                                <label for="<%=chkSTCFormSaved.ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                              
                                                                                <label class="control-label">
                                                                                    STC Form Saved
                                                                                </label>--%>
                                                                                    </td>
                                                                                    <td valign="top" width="10" class="paddtable1" id="divST" runat="server" visible="false">
                                                                                        <asp:HyperLink ID="lblST" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                                        <span>
                                                                                            <asp:FileUpload ID="fuST" runat="server" />
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorST" runat="server" ErrorMessage="*"
                                                                                                ValidationGroup="postinst" SetFocusOnError="true" ControlToValidate="fuST" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </span>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-bottom: 0px!important">
                                                                        <span class="name disblockcss"></span><span>
                                                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                                <tr>
                                                                                    <td valign="top" width="10" class="paddtoptable1">
                                                                                        <div class="form-group">
                                                                                            <span></span>
                                                                                            <div class=" checkbox-info checkbox">
                                                                                                <span class="fistname">
                                                                                                    <label for="<%=chkCertificateSaved.ClientID %>" class="control-label">
                                                                                                        <asp:CheckBox ID="chkCertificateSaved" runat="server" AutoPostBack="true" OnCheckedChanged="chkCertificateSaved_CheckedChanged" />
                                                                                                        <span class="text">Certificate Saved &nbsp;</span>
                                                                                                    </label>
                                                                                                </span>
                                                                                            </div>
                                                                                            <div class="clear">
                                                                                            </div>
                                                                                        </div>
                                                                                        <%-- <asp:CheckBox ID="chkCertificateSaved" runat="server" AutoPostBack="true" OnCheckedChanged="chkCertificateSaved_CheckedChanged" />
                                                                                <label for="<%=chkCertificateSaved.ClientID %>">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="control-label">
                                                                                    Certificate Saved
                                                                                </label>--%>
                                                                                    </td>
                                                                                    <td valign="top" width="10" class="paddtable1" id="divCE" runat="server" visible="false">
                                                                                        <asp:HyperLink ID="lblCE" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                                        <asp:FileUpload ID="fuCE" runat="server" />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCE" runat="server" ErrorMessage="*"
                                                                                            ValidationGroup="postinst" SetFocusOnError="true" ControlToValidate="fuCE" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </span>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </asp:Panel>
                                                    <asp:Panel ID="Panel2" runat="server">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group wdth210" style="margin-bottom: 0px!important">
                                                                            <span class="name">
                                                                                <label class="control-label">
                                                                                    Inv Serial No
                                                                                </label>
                                                                            </span><span>
                                                                                <asp:TextBox ID="txtInverterSerial" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group wdth210" style="margin-bottom: 0px!important">
                                                                            <span class="name">
                                                                                <label class="control-label">
                                                                                    2nd Inv
                                                                                </label>
                                                                            </span><span>
                                                                                <asp:TextBox ID="txtSecondInverterSerial" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="Panel3" runat="server">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Payment Status</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">


                                                    <div class="form-group col-sm-2 wdth190">
                                                        <span class="name">
                                                            <asp:Label ID="Label4" runat="server" class="control-label">
                                                  Invoice Sent</asp:Label></span>
                                                        <div class="input-group date datetimepicker1">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtInvoiceSent" runat="server" class="form-control">
                                                            </asp:TextBox>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>


                                                    <div class="form-group col-md-2 wdth190">
                                                        <div class="name">
                                                            <asp:Label ID="Label6" runat="server" class="control-label">
                                                                    Balance Owing
                                                            </asp:Label></span>
                                                        </div>
                                                        <div class="dateimg">
                                                            <asp:TextBox ID="txtBalanceOwing" runat="server" CssClass="form-control"
                                                                Enabled="false"></asp:TextBox>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group dateimgarea col-md-2 wdth190">
                                                        <span class="name">
                                                            <asp:Label ID="Label7" runat="server" class="control-label">
                                                                    Invoice Fully Paid
                                                            </asp:Label></span>
                                                        </span><span class="dateimg">
                                                            <asp:TextBox ID="txtInvoiceFU" runat="server" CssClass="form-control"
                                                                Enabled="false"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-2" style="padding-top: 18px; width: 140px !important;">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=chkReceiptSent.ClientID %>" class="control-label">
                                                                    <asp:CheckBox ID="chkReceiptSent" runat="server" />
                                                                    <span class="text">Receipt Sent  &nbsp;</span>
                                                                </label>
                                                            </span>
                                                        </div>

                                                        <div class="clear">
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-2 wdth190">
                                                        <asp:LinkButton ID="lnksendmessage" Style="width: 163px; margin-top: 25px;" runat="server" CausesValidation="false" class="btn btn-labeled btn-success" OnClick="lnksendmessage_Click">
                                                                    <i class="glyphicon glyphicon-envelope"></i>Send Invoice
                                                        </asp:LinkButton>
                                                    </div>

                                                    <div class="form-group col-md-2 wdth190">
                                                        <div class="form-group postinstbox ">
                                                            <span style="text-align: center;" class="proinstdate">
                                                                <uc1:InvoicePayments ID="InvoicePayments1" runat="server" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Meter Application Detail</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div>

                                                        <div class="form-group col-md-3 wdth211" style="display:none">
                                                            <span class="name">
                                                                <asp:Label ID="Label1" runat="server" class="control-label">
                                                        Time</asp:Label></span>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtMeterAppliedTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtenderST" runat="server" TargetControlID="txtMeterAppliedTime" Mask="99:99:99"
                                                                    MessageValidatorTip="true" MaskType="Time">
                                                                </cc1:MaskedEditExtender>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="form-group col-md-3 wdth211">
                                                        <span class="name">
                                                            <asp:Label ID="Label8" runat="server" class="control-label">
                                                                Meter Apply Ref
                                                            </asp:Label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtMeterAppliedRef" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3 wdth211">
                                                        <span class="name">
                                                            <asp:Label ID="Label9" runat="server" class="control-label">
                                                                Inspector Name
                                                            </asp:Label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtInspectorName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-3 wdth211">
                                                        <span class="name">
                                                            <asp:Label ID="Label5" runat="server" class="control-label">
                                                     Inspection Date</asp:Label></span>
                                                        <div class="input-group date datetimepicker1">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtInspectionDate" runat="server" class="form-control">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <asp:Panel runat="server" ID="panelcomcerti" Visible="false">
                                                        <div class="form-group col-md-3 wdth211">
                                                            <span class="name">
                                                                <asp:Label ID="Label13" runat="server" class="control-label">
                                                     Compliance Certificate</asp:Label></span>
                                                            <asp:DropDownList ID="ddlcomcerti" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row" id="divMeterApplication" runat="server" visible="false">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="widget flat radius-bordered borderone">
                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                        <span class="widget-caption">Meter Application</span>
                                                    </div>
                                                    <div class="widget-body">
                                                        <div>
                                                            <div class="form-group dateimgarea col-md-3">
                                                                <span class="name">

                                                                    <asp:Label ID="Label10" runat="server" class="control-label">
                                                                    Meter Applied
                                                                    </asp:Label>
                                                                </span><span class="dateimg">
                                                                    <asp:TextBox ID="txtMeterAppliedDate" runat="server" Width="100px" CssClass="form-control"></asp:TextBox>
                                                                    <asp:ImageButton ID="Image23" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                    <cc1:CalendarExtender ID="CalendarExtender23" runat="server" PopupButtonID="Image23"
                                                                        TargetControlID="txtMeterAppliedDate" Format="dd/MM/yyyy">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:RegularExpressionValidator ValidationGroup="postinst" ControlToValidate="txtMeterAppliedDate"
                                                                        ID="RegularExpressionValidator5" runat="server" ErrorMessage="Enter valid date"
                                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <span class="name">

                                                                    <asp:Label ID="Label11" runat="server" class="control-label">
                                                                    Apply Method
                                                                    </asp:Label>
                                                                </span><span style="width: 200px;">
                                                                    <asp:TextBox ID="txtMeterAppliedMethod" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </asp:Panel>
                            </div>
                        </div>
                        <div class="row" id="divAddUpdate" runat="server">
                            <div class="col-md-12 text-center">
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdatePostInst" runat="server" OnClick="btnUpdatePostInst_Click"
                                    ValidationGroup="postinst" Text="Save" CausesValidation="true" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>
        <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData1">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div_popup" class="modalpopup modal_popup" align="center" style="width: 400px;">
            <div class="popupdiv">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading pad15">
                                <asp:Label runat="server" ID="lbltitle" Text="Series"></asp:Label>
                                <div class="panel-tools">
                                    <div class="closbtn">
                                        <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                            <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body formareapop" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group dateimgarea">
                                            <asp:Label runat="server" ID="lbl"></asp:Label>
                                            <div class="clear">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <asp:Button ID="Button33" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="modalpickupreason" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="lnkbtn1" DropShadow="false" PopupControlID="divreason" TargetControlID="Button33">
        </cc1:ModalPopupExtender>
        <div runat="server" id="divreason" class="modal_popup" align="" style="width: 350px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="Label16"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="lnkbtn1" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                    Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H145">Stock Update</h3>
                                        </div>

                                        <%--   <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                            <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                        </asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Stock Status 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:DropDownList ID="ddlreason" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select myval" AutoPostBack="true" OnSelectedIndexChanged="ddlreason_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">Reason 1</asp:ListItem>
                                                            <asp:ListItem Value="2">Reason 2</asp:ListItem>
                                                            <asp:ListItem Value="3">Reason 3</asp:ListItem>
                                                            <asp:ListItem Value="4">Reason 4</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlreason" Display="Dynamic" ValidationGroup="returnreason" InitialValue="0"> </asp:RequiredFieldValidator>
                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Notes
                                                            </label>
                                                        </span><span class="">
                                                            <asp:TextBox ID="txtNotes2" TextMode="MultiLine" runat="server" Width="290px" Height="80px"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="reqvaltxtNotesName2" runat="server" ControlToValidate="txtNotes2"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="returnreason" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class=" col-sm-12 center-text form-group">
                                                    <span class="">
                                                        <label class="">
                                                            &nbsp;
                                                        </label>
                                                    </span><span class="">
                                                        <asp:Button CausesValidation="true" ValidationGroup="returnreason" class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="ButtonReason" runat="server" OnClick="ButtonReason_Click"
                                                            Text="Save" />

                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </ContentTemplate>
    <Triggers>
        <%-- <asp:PostBackTrigger ControlID="btnUpdatePostInst" />
        <asp:PostBackTrigger ControlID="InvoicePayments1" />--%>
    </Triggers>
</asp:UpdatePanel>
