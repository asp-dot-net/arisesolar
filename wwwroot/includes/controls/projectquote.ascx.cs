using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.IO;
using System.Configuration;
using System.Web.UI;
//using Telerik.Reporting.Services.WebApi;
using Google.Apis.Sheets.v4;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4.Data;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using PdfiumViewer;

public partial class includes_controls_projectquote : System.Web.UI.UserControl
{
    static DataView dv;
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];


    Telerik_reports Telerik_reports;
    //Google Sheet 
    //For Local
    //static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    //static readonly string ApplicationName = "Sync tblWebDownload";
    //static readonly string sheet = "Sheet1";
    //static readonly string SpreadsheetId = "1tNnJ0g2KWSIFoOx8BDmDnbLrnTZMODrlop-ab-RRN-I";
    //static SheetsService service;

    //For Live
    static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    static readonly string ApplicationName = "AriseSolar WebDownloadData";
    static readonly string sheet = "Sheet1";
    static readonly string SpreadsheetId = "19NxOhA3Z1srODefkdzVbb0NJr2bHwJ8roS7rrmk3bMM";
    static SheetsService service;
    //Gogle Sheet

    protected static string Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        //string Role = "";
       


        PanSuccess.Visible = false;
        PanError.Visible = false;
        // InvoicePayments1.Visible = true;
        string ProjectID = Request.QueryString["proid"];
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {
                if (st2.ProjectStatusID == "3")
                {
                    btnUpdateQuote.Visible = false;
                }
            }
            if (st2.ProjectTypeID == "8")
            {
                divmtcepaperwork.Visible = true;
            }
            else
            {
                divmtcepaperwork.Visible = false;
            }
            if (st2.ProjectTypeID == "8")//btnUpdateQuote
            {
                ssactive.Visible = true;
            }
            else
            {
                ssactive.Visible = false;
            }
        }
    }
    //public void MsgError(string msg)
    //{
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    //}
    public void BindProjectQuote()
    {
        
        BindScript();
        if (Roles.IsUserInRole("Sales Manager"))
        {
            hndRole.Value = "Sales Manager";
        }
        else if (Roles.IsUserInRole("Administrator"))
        {
            hndRole.Value = "Administrator";
        }
        else if(Roles.IsUserInRole("DSales Manager"))
        {
            hndRole.Value = "DSales Manager";
        }
        else if (Roles.IsUserInRole("Installation Manager"))
        {
            hndRole.Value = "Installation Manager";
        }
        else
        {
            hndRole.Value = "Hide";
        }

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);       
        if (st.ProjectStatusID == "8")
        {
            cvTodate.Visible = false;
        }
        if (Roles.IsUserInRole("Administrator"))
        {
            txtInvoiceNumber.Enabled = true;
            txtInvoiceDoc.Enabled = true;
            txtInvoiceSent.Enabled = true;
            divDepRec.Enabled = true;
            //btnCreateNewQoute.Visible = true;
        }
        else
        {
            txtInvoiceNumber.Enabled = false;
            txtInvoiceDoc.Enabled = false;
            txtInvoiceSent.Enabled = false;
            //btnCreateNewQoute.Visible = false;
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            // ImageDepRec.Visible = true;
            txtDepositReceived.Enabled = true;
        }
        else
        {
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PreInstaller"))
            {
                if (st2.FinanceWithDepositID == "1")
                {
                    txtDepositReceived.Enabled = true;
                    //  ImageDepRec.Visible = true;

                    if (st.InvoiceNumber == string.Empty)
                    {
                        //   ImageDepRec.Visible = false;
                        txtDepositReceived.Enabled = false;
                    }
                    else
                    {
                        txtDepositReceived.Enabled = true;
                        //   ImageDepRec.Visible = true;
                    }
                }
                else
                {
                    DataTable dtDR = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
                    if (dtDR.Rows.Count > 0)
                    {
                        txtDepositReceived.Enabled = true;
                        //   ImageDepRec.Visible = true;
                    }
                    else
                    {
                        //   ImageDepRec.Visible = false;
                        txtDepositReceived.Enabled = false;
                    }
                }
            }
            else
            {
                //ImageDepRec.Visible = false;
                txtDepositReceived.Enabled = false;
            }
        }

        if (st.ProjectStatusID == "3")
        {
            RequiredFieldValidatorDepRec.Visible = true;
        }
        else
        {
            RequiredFieldValidatorDepRec.Visible = false;
        }
        if (st.ProjectStatusID == "10" || st.ProjectStatusID == "5")
        {
            RequiredFieldValidatorAD.Visible = true;
        }
        else
        {
            RequiredFieldValidatorAD.Visible = false;
        }

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {
            if (st.ProjectStatusID == "5")
            {

                PanAddUpdate.Enabled = false;
            }
        }
        if (Roles.IsUserInRole("Finance"))
        {
            PanAddUpdate.Enabled = false;
        }
        DataTable dt_inv1 = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
        if (dt_inv1.Rows.Count > 0)
        {
            if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
            {

                if (dt_inv1.Rows[0]["InvoicePayDate"].ToString() != string.Empty)
                {
                    hiddenExpiryDate.Value = Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]).AddDays(-1).ToString();//System.DateTime.Now.AddDays(-1).ToString();

                    DateTime dtInvPay = Convert.ToDateTime(hiddenExpiryDate.Value);
                    //CompareValidatorDepRec.ValueToCompare = DateTime.Today.Date.ToString("dd/MM/yyyy"); //string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]));
                if(txtDepositReceived.Text!=null && txtDepositReceived.Text!="")
                    {
                        DateTime dtDeprec= Convert.ToDateTime(txtDepositReceived.Text);
                        if(dtDeprec < dtInvPay)
                        {
                            MsgError("Deposit Date Canot be less then First Dep. Date");
                        }
                    }
                }
            }

        }
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("DSales Manager"))
        {
            //CompareValidatorActiveDate.Visible = false;
            //CompareValidatorActiveDate.ValueToCompare = txtDepositReceived.Text;
            //CompareValidatorDepRec.Visible = false;
            if (txtDepositReceived.Text != string.Empty)
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString();
                    hdndeprec.Value = Convert.ToDateTime(txtDepositReceived.Text).AddDays(-1).ToString();
                }
            }
            else
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    hdndeprec.Value = DateTime.Now.AddDays(-1).ToString();
                }
            }
        }
        else
        {
            //CompareValidatorActiveDate.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            //CompareValidatorActiveDate.ValueToCompare = txtDepositReceived.Text;
            if (txtDepositReceived.Text != string.Empty)
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString();
                    hdndeprec.Value = Convert.ToDateTime(txtDepositReceived.Text).AddDays(-1).ToString();
                }
            }
            else
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    hdndeprec.Value = DateTime.Now.AddDays(-1).ToString();
                }
            }
            if (st.DepositReceived == string.Empty)
            {
                //CompareValidatorDepRec.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            }
        }

        if (Roles.IsUserInRole("Accountant"))
        {
            PanAddUpdate.Enabled = true;
            Panel2.Enabled = true;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Maintenance"))
        {
            PanAddUpdate.Enabled = true;
        }
        if (Roles.IsUserInRole("SalesRep"))
        {
        }
        if (Roles.IsUserInRole("STC"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            PanAddUpdate.Enabled = true;
            PanelMain.Enabled = true;
            Panel1.Enabled = false;
            Panel2.Enabled = true;
            Panel31.Enabled = false;
            Panel32.Enabled = false;
            Panel4.Enabled = false;
            Panel5.Enabled = false;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            PanAddUpdate.Enabled = true;
            divDepRec.Enabled = false;
            PanActiveDate.Enabled = false;
        }
        if (Roles.IsUserInRole("Installation Manager"))
        {
            if (st.ProjectStatusID == "3")
            {
                chkSignedQuote.Enabled = false;
                txtDepositReceived.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("SalesRep") && (st.ProjectStatusID == "3" || st.ProjectStatusID == "5"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            //string CSalesTeamID = stEmpC.SalesTeamID;
            string CSalesTeamID = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                }
                CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
            }

            if (Request.QueryString["proid"] != string.Empty)
            {
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                string EmpType = stEmp.EmpType;
                //string SalesTeamID = stEmp.SalesTeamID;
                string SalesTeam = "";
                DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                if (dt_empsale1.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale1.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }

                if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                {
                    PanAddUpdate.Enabled = true;
                }
                else
                {
                    PanAddUpdate.Enabled = false;
                }
            }
        }
        string ProjectID = Request.QueryString["proid"];

        BindQuote();
        BindInvoice();

        if (st.InvoiceNumber == string.Empty && st.InvoiceDoc == string.Empty)
        {
            btnCreateInvoice.Visible = true;
            btnOpenInvoice.Visible = false;
            //InvoicePayments1.Visible = false;
        }
        else
        {
            btnCreateInvoice.Visible = false;
            btnOpenInvoice.Visible = true;
            //InvoicePayments1.Visible = true;

        }
        try
        {
            //Response.Write(Convert.ToDateTime(st.QuoteSent).ToShortDateString());
            //Response.End();
            txtQuoteSent.Text = Convert.ToDateTime(st.QuoteSent).ToShortDateString();
        }
        catch
        {
        }
        try
        {
            txtssdate.Text = Convert.ToDateTime(st.SSActiveDate).ToShortDateString();
        }
        catch { }
        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Administrator"))
        {
            txtQuoteSent.Enabled = true;
            PanQuoteSent.Enabled = true;
        }

        try
        {
            txtQuoteAcceptedQuote.Text = Convert.ToDateTime(st.QuoteAccepted).ToShortDateString();
        }
        catch { }
        txtInvoiceDoc.Text = st.InvoiceDoc;
        txtInvoiceNumber.Text = st.InvoiceNumber;

        if (st.InvoiceSent != string.Empty)
        {
            try
            {
                txtInvoiceSent.Text = Convert.ToDateTime(st.InvoiceSent).ToShortDateString();
            }
            catch { }
        }
        else
        {
            DataTable dtInvDate = ClstblInvoicePayments.tblInvoicePayments_SelectTop1ByProjectID(ProjectID);
            if (dtInvDate.Rows.Count > 0)
            {
                try
                {
                    txtInvoiceSent.Text = Convert.ToDateTime(dtInvDate.Rows[0]["InvoicePayDate"].ToString()).ToShortDateString();
                }
                catch { }
            }
        }
        try
        {
            txtDepositReceived.Text = Convert.ToDateTime(st.DepositReceived).ToShortDateString();
        }
        catch { }

        chkSignedQuote.Checked = Convert.ToBoolean(st.SignedQuote);
        if (st.SQ != string.Empty)
        {
            lblSQ.Visible = true;
            lblSQ.Text = st.SQ;
            //lblSQ.NavigateUrl = "~/userfiles/SQ/" + st.SQ;
            //lblSQ.NavigateUrl = pdfURL + "SQ/" + st.SQ;
            lblSQ.NavigateUrl = SiteConfiguration.GetDocumnetPath("SQ", st.SQ);
            //   RequiredFieldValidatorSQ.Visible = false;
        }
        else
        {
            lblSQ.Visible = false;
        }

        chkMeterBoxPhotosSaved.Checked = Convert.ToBoolean(st.MeterBoxPhotosSaved);

        if (st.MP != string.Empty)
        {
            lblMP.Visible = true;
            lblMP.Text = st.MP;
            //lblMP.NavigateUrl = "~/userfiles/MP/" + st.MP;
            //lblMP.NavigateUrl = pdfURL + "MP/" + st.MP;
            lblMP.NavigateUrl = SiteConfiguration.GetDocumnetPath("MP", st.MP);
            // RequiredFieldValidatorMP.Visible = false;
        }
        else
        {
            lblMP.Visible = false;
        }
        chkElecBillSaved.Checked = Convert.ToBoolean(st.ElecBillSaved);

        if (st.EB != string.Empty)
        {
            lblEB.Visible = true;
            lblEB.Text = st.EB;
            //lblEB.NavigateUrl = "~/userfiles/EB/" + st.EB;
            //lblEB.NavigateUrl = pdfURL + "EB/" + st.EB;
            lblEB.NavigateUrl = SiteConfiguration.GetDocumnetPath("EB", st.EB);
            // RequiredFieldValidatorEB.Visible = false;
        }
        else
        {
            lblEB.Visible = false;
        }
        if (st.beatquote != string.Empty)
        {
            chkbeatquote.Checked = Convert.ToBoolean(st.beatquote);
        }
        if (st.beatquotedoc != string.Empty)
        {
            lblbeat.Visible = true;
            lblbeat.Text = st.beatquotedoc;
            //lblbeat.NavigateUrl = pdfURL + "BeatQuote/" + st.beatquotedoc;
            lblbeat.NavigateUrl = SiteConfiguration.GetDocumnetPath("BeatQuote", st.beatquotedoc);
            //  RequiredFieldValidatorbeat.Visible = false;
        }
        else
        {
            lblbeat.Visible = false;
        }

        if (st.nearmap != string.Empty)
        {
            chknearmap.Checked = Convert.ToBoolean(st.nearmap);
        }

        if (st.nearmapdoc != string.Empty)
        {
            lblnearmap1.Visible = true;
            lblnearmap1.Text = st.nearmapdoc;
            //lblnearmap1.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc;
            lblnearmap1.NavigateUrl = SiteConfiguration.GetDocumnetPath("NearMap", st.nearmapdoc);

        }
        if (st.nearmapdoc1 != string.Empty)
        {
            lblnearmap2.Visible = true;
            lblnearmap2.Text = st.nearmapdoc1;
            //lblnearmap2.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc1;
            lblnearmap2.NavigateUrl = SiteConfiguration.GetDocumnetPath("NearMap", st.nearmapdoc1);

        }
        if (st.nearmapdoc2 != string.Empty)
        {
            lblnearmap3.Visible = true;
            lblnearmap3.Text = st.nearmapdoc2;
            //lblnearmap3.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc2;
            lblnearmap3.NavigateUrl = SiteConfiguration.GetDocumnetPath("NearMap", st.nearmapdoc2);

        }


        //if (st.nearmap != string.Empty)
        //{
        //    if (chknearmap.Checked = Convert.ToBoolean(st.nearmap))
        //    {
        //        Spanmap.Visible = true;
        //        Spanmap1.Visible = true;
        //        Spanmap2.Visible = true;
        //        if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
        //        {
        //            RequiredFieldValidatormap.Visible = false;
        //            RequiredFieldValidator1.Visible = false;
        //            RequiredFieldValidator2.Visible = false;
        //            //RequiredFieldValidatormap.Enabled = false;
        //            //RequiredFieldValidator1.Enabled = false;
        //            //RequiredFieldValidator2.Enabled = false;
        //            //RequiredFieldValidatormap.IsValid = true;
        //            //RequiredFieldValidator1.IsValid = true;
        //            //RequiredFieldValidator2.IsValid = true;

        //            //RequiredFieldValidatormap.Enabled = false;
        //            //RequiredFieldValidator1.Enabled = false;
        //            //RequiredFieldValidator2.Enabled = false;
        //        }
        //        else
        //        {
        //            RequiredFieldValidatormap.Visible = true;
        //            RequiredFieldValidator1.Visible = true;
        //            RequiredFieldValidator2.Visible = true;
        //            //RequiredFieldValidatormap.Enabled = true;
        //            //RequiredFieldValidator1.Enabled = true;
        //            //RequiredFieldValidator2.Enabled = true;
        //        }
        //    }
        //    else
        //    {
        //        Spanmap.Visible = false;
        //        Spanmap1.Visible = false;
        //        Spanmap2.Visible = false;
        //        if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
        //        {
        //            RequiredFieldValidatormap.Visible = false;
        //            RequiredFieldValidator1.Visible = false;
        //            RequiredFieldValidator2.Visible = false;
        //            //RequiredFieldValidatormap.Enabled = false;
        //            //RequiredFieldValidator1.Enabled = false;
        //            //RequiredFieldValidator2.Enabled = false;
        //            //RequiredFieldValidatormap.IsValid = true;
        //            //RequiredFieldValidator1.IsValid = true;
        //            //RequiredFieldValidator2.IsValid = true;

        //            //RequiredFieldValidatormap.Enabled = false;
        //            //RequiredFieldValidator1.Enabled = false;
        //            //RequiredFieldValidator2.Enabled = false;
        //        }
        //        else
        //        {
        //            RequiredFieldValidatormap.Visible = true;
        //            RequiredFieldValidator1.Visible = true;
        //            RequiredFieldValidator2.Visible = true;
        //            //RequiredFieldValidatormap.Enabled = true;
        //            //RequiredFieldValidator1.Enabled = true;
        //            //RequiredFieldValidator2.Enabled = true;
        //        }
        //    }
        //}
        //else
        //{
        //    if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
        //    {
        //        RequiredFieldValidatormap.Visible = false;
        //        RequiredFieldValidator1.Visible = false;
        //        RequiredFieldValidator2.Visible = false;
        //        //RequiredFieldValidatormap.Enabled = false;
        //        //RequiredFieldValidator1.Enabled = false;
        //        //RequiredFieldValidator2.Enabled = false;
        //        //RequiredFieldValidatormap.IsValid = true;
        //        //RequiredFieldValidator1.IsValid = true;
        //        //RequiredFieldValidator2.IsValid = true;

        //        //RequiredFieldValidatormap.Enabled = false;
        //        //RequiredFieldValidator1.Enabled = false;
        //        //RequiredFieldValidator2.Enabled = false;
        //    }
        //    else
        //    {
        //        RequiredFieldValidatormap.Visible = true;
        //        RequiredFieldValidator1.Visible = true;
        //        RequiredFieldValidator2.Visible = true;
        //        //RequiredFieldValidatormap.Enabled = true;
        //        //RequiredFieldValidator1.Enabled = true;
        //        //RequiredFieldValidator2.Enabled = true;
        //    }
        //}
        if (st.nearmapdoc != string.Empty)
        {
            lblnearmap.Visible = true;
            lblnearmap.Text = st.PdfFileNaame;
            //lblnearmap.NavigateUrl = pdfURL + "NearMap/" + st.PdfFileNaame;
            lblnearmap.NavigateUrl = SiteConfiguration.GetDocumnetPath("NearMap", st.PdfFileNaame);
            //RequiredFieldValidatormap.Visible = false;
            //if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
            // {
            //     RequiredFieldValidatormap.Visible = false;
            // }
            // else
            // {
            //     RequiredFieldValidatormap.Visible = true;
            // }
        }
        else
        {
            //if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
            //{
            //    RequiredFieldValidatormap.Visible = false;
            //}
            //else
            //{
            //    RequiredFieldValidatormap.Visible = true;
            //}
            ////RequiredFieldValidatormap.Visible = true;
            lblnearmap.Visible = false;
        }

        if (st.nearmapdoc1 != string.Empty)
        {
            //lblnearmap1.Visible = true;
            //lblnearmap1.Text = st.nearmapdoc1;
            //lblnearmap1.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc1;
            //RequiredFieldValidatormap.Visible = false;
            //RequiredFieldValidator1.Visible = false;
        }
        else
        {
            //if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
            //{
            //    RequiredFieldValidator1.Visible = false;
            //}
            //else
            //{
            //    RequiredFieldValidator1.Visible = true;
            //}
           // lblnearmap1.Visible = false;
        }

        //if (st.nearmapdoc2 != string.Empty)
        //{
        //    lblnearmap2.Visible = true;
        //    lblnearmap2.Text = st.nearmapdoc2;
        //    lblnearmap2.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc2;
        //    //RequiredFieldValidator2.Visible = false;
        //}
        //else
        //{
        //    lblnearmap2.Visible = false;
        //    //if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
        //    //{
        //    //    RequiredFieldValidator2.Visible = false;
        //    //}
        //    //else
        //    //{
        //    //    RequiredFieldValidator2.Visible = true;
        //    //}
        //}
        //if (st.nearmapdoc != string.Empty)
        //{
        //    lblnearmap.Visible = true;
        //    lblnearmap.Text = st.nearmapdoc;
        //    lblnearmap.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc;
        //    //RequiredFieldValidatormap.Visible = false;
        //}
        //else
        //{
        //    lblnearmap.Visible = false;
        //}

        chkProposedDesignSaved.Checked = Convert.ToBoolean(st.ProposedDesignSaved);
        if (st.PD != string.Empty)
        {
            lblPD.Visible = true;
            lblPD.Text = st.PD;
            //lblPD.NavigateUrl = "~/userfiles/PD/" + st.PD;
            //lblPD.NavigateUrl = pdfURL + "PD/" + st.PD;
            lblPD.NavigateUrl = SiteConfiguration.GetDocumnetPath("PD", st.PD);
            // RequiredFieldValidatorPD.Visible = false;
        }
        else
        {
            lblPD.Visible = false;
        }
        chkPaymentReceipt.Checked = Convert.ToBoolean(st.PaymentReceipt);
        if (st.PR != string.Empty)
        {
            lblPR.Visible = true;
            lblPR.Text = st.PR;
            //lblPR.NavigateUrl = "~/userfiles/PR/" + st.PR;
            //lblPR.NavigateUrl = pdfURL + "PR/" + st.PR;
            lblPR.NavigateUrl = SiteConfiguration.GetDocumnetPath("PR", st.PR);
            // RequiredFieldValidatorPR.Visible = false;
        }
        else
        {
            lblPR.Visible = false;
        }


        if (st.PanelBrandID != "")
        {
            txtPanelDetails.Text = st.NumberPanels + " X " + st.PanelOutput + " Watt " + st.PanelBrandName + " Panels.(" + st.PanelModel + ")";
        }
        if (st.InverterDetailsID != "")
        {
            txtInverterDetails.Text = "One " + st.InverterDetailsName + " KW Inverter. Plus " + st.SecondInverterDetails + " KW Inverter.";
        }
        if (st.SignedQuote == "True")
        {
            divSQ.Visible = true;
        }
        else
        {
            divSQ.Visible = false;
        }
        if (st.MeterBoxPhotosSaved == "True")
        {
            // divMP.Visible = true;

        }
        else
        {
            // divMP.Visible = false;
        }
        if (st.beatquote == "True")
        {
            //Spanbeat.Visible = true;
        }
        else
        {
            //Spanbeat.Visible = false;
        }
        if (st.ElecBillSaved == "True")
        {
            //divEB.Visible = true;
        }
        else
        {
            // divEB.Visible = false;
        }
        if (st.ProposedDesignSaved == "True")
        {
            //  divPD.Visible = true;
        }
        else
        {
            //divPD.Visible = false;
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            txtActiveDate.Enabled = true;
            PanActiveDate.Enabled = true;
        }
        else
        {
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller")))
            {
                if (st.ElecDistributorID == "12")
                {
                    if (chkSignedQuote.Checked == true && txtDepositReceived.Text != string.Empty && txtQuoteAcceptedQuote.Text != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.meterupgrade != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            txtActiveDate.Enabled = true;
                            PanActiveDate.Enabled = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                txtActiveDate.Enabled = true;
                                PanActiveDate.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        txtActiveDate.Enabled = false;
                        PanActiveDate.Enabled = false;
                    }
                }
                else if (st.ElecDistributorID == "13")
                {
                    if (chkSignedQuote.Checked == true && txtDepositReceived.Text != string.Empty && txtQuoteAcceptedQuote.Text != string.Empty && st.RegPlanNo != string.Empty && st.LotNumber != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.meterupgrade != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            txtActiveDate.Enabled = true;
                            PanActiveDate.Enabled = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                txtActiveDate.Enabled = true;
                                PanActiveDate.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        txtActiveDate.Enabled = false;
                        PanActiveDate.Enabled = false;
                    }
                }
                else
                {
                    if (chkSignedQuote.Checked == true && txtDepositReceived.Text != string.Empty && txtQuoteAcceptedQuote.Text != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            txtActiveDate.Enabled = true;
                            PanActiveDate.Enabled = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                txtActiveDate.Enabled = true;
                                PanActiveDate.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        txtActiveDate.Enabled = false;
                        PanActiveDate.Enabled = false;
                    }
                }
            }
            else
            {
                txtActiveDate.Enabled = false;
                PanActiveDate.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            txtDepositReceived.Enabled = true;
            //  ImageDepRec.Visible = true;
        }
        else
        {
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager"))
            {
                if (st.HouseType != string.Empty && st.RoofTypeID != string.Empty && st.ElecDistributorID != string.Empty)
                {
                    txtDepositReceived.Enabled = true;
                    //  ImageDepRec.Visible = true;
                }
                else
                {
                    txtDepositReceived.Enabled = false;
                    // ImageDepRec.Visible = false;
                }
            }
        }
        try
        {
            txtActiveDate.Text = Convert.ToDateTime(st.ActiveDate).ToShortDateString();
        }
        catch { }
        try
        {
            //Response.Write(Convert.ToBoolean(st.readyactive.ToString()).ToString());
            //Response.End();
            if (st.readyactive.ToString() == "1")
            {
                chkactive.Checked = true;
            }
            else
            {
                chkactive.Checked = false;
            }
            //Convert.ToBoolean(st.readyactive.ToString());
        }
        catch { }
        DataTable dt_inv_recpt = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
        try
        {
            if (dt_inv_recpt.Rows[0]["ReceiptNumber"].ToString() != string.Empty)
            {
                divrecept.Visible = true;
                lblreceipt.Text = dt_inv_recpt.Rows[0]["ReceiptNumber"].ToString();
            }

        }
        catch { }
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        try
        {
            lblTotalPaidAmount.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
        }
        catch { }

        string totalprice = st.TotalQuotePrice;
        decimal TotalPrice = 0;
        if (totalprice != string.Empty)
        {
            TotalPrice = Convert.ToDecimal(totalprice);
        }
        decimal totalpay = 0;
        if (dtCount.Rows.Count > 0)
        {
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                totalpay = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            }
        }
        if (TotalPrice == 0)
        {
            lblpaymentstatus.Text = " ";
        }
        else
        {
            if (TotalPrice - totalpay > 0)
            {
                lblpaymentstatus.Text = "Owing";
            }
            else
            {
                lblpaymentstatus.Text = "Fully Paid";
            }
        }

        if (st.InvoiceNumber != string.Empty)
        {
            Panel31.Enabled = true;
            Panel32.Enabled = true;
            Panel33.Enabled = true;
        }
        else
        {
            Panel31.Enabled = false;
            Panel32.Enabled = false;
            Panel33.Enabled = false;
        }
        DataTable dt_inv = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
        if (dt_inv.Rows.Count > 0)
        {
            Panel31.Enabled = true;
            Panel32.Enabled = true;
        }
        else
        {
            Panel31.Enabled = false;
            Panel32.Enabled = false;
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PostInstaller"))
        {
            btnOpenInvoice.Enabled = true;
            imgbtnPerforma.Visible = true;
        }
        else
        {
            btnOpenInvoice.Enabled = true;
            imgbtnPerforma.Visible = false;
        }
        //if (Roles.IsUserInRole("Administrator"))
        //{
        //    PanActiveDate.Enabled = true;
        //    divDepRec.Enabled = true;

        //}
        //else
        //{
        //    PanActiveDate.Enabled = false;
        //    divDepRec.Enabled = false;
        //    if (st.ActiveDate == string.Empty)
        //    {
        //        txtActiveDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        //    }
        //    if (st.DepositReceived == string.Empty)
        //    {
        //        txtDepositReceived.Text = DateTime.Now.AddHours(14).ToShortDateString();
        //    }
        //}
        if (st.ProjectStatusID == "15")
        {
            //DepositReceived date Enable
            divDepRec.Enabled = true;
            txtDepositReceived.Enabled = true;
            Panel31.Enabled = true;
            txtInvoiceNumber.Enabled = false;

            //Active date Enable
            PanActiveDate.Enabled = true;
            txtActiveDate.Enabled = true;
            Panel32.Enabled = true;
            txtInvoiceDoc.Enabled = false;
            
        }
        if (Roles.IsUserInRole("Administrator"))
        {
            btnCreateNewQoute.Visible = true;
        }
        else
        {
            if (st.nearmapdoc != null && st.nearmapdoc != "" || st.nearmapdoc1 != null && st.nearmapdoc1 != "" || st.nearmapdoc2 != null && st.nearmapdoc2 != "")
            {
                //btnCreateNewQoute.Enabled = true;
                //btnCreateNewQoute.Attributes.Add("enabled", "enabled");
                btnCreateNewQoute.Visible = true;
            }
            else
            {
                //btnCreateNewQoute.Enabled = false;
                //btnCreateNewQoute.Attributes.Add("disabled", "disabled");
                btnCreateNewQoute.Visible = false;
            }
        }
        BindScript();

    }
    public void BindData()
    {
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        lblSQ.Visible = true;
        lblSQ.Text = st.SQ;
        //lblSQ.NavigateUrl = "~/userfiles/SQ/" + st.SQ;
        //lblSQ.NavigateUrl = pdfURL + "SQ/" + st.SQ;
        lblSQ.NavigateUrl = SiteConfiguration.GetDocumnetPath("SQ", st.SQ);

        lblMP.Visible = true;
        lblMP.Text = st.MP;
        //lblMP.NavigateUrl = "~/userfiles/MP/" + st.MP;
        //lblMP.NavigateUrl = pdfURL + "MP/" + st.MP;
        lblMP.NavigateUrl = SiteConfiguration.GetDocumnetPath("MP", st.MP);

        lblEB.Visible = true;
        lblEB.Text = st.EB;
        //lblEB.NavigateUrl = "~/userfiles/EB/" + st.EB;
        //lblEB.NavigateUrl = pdfURL + "EB/" + st.EB;
        lblEB.NavigateUrl = SiteConfiguration.GetDocumnetPath("EB", st.EB);

        lblPD.Visible = true;
        lblPD.Text = st.PD;
        //lblPD.NavigateUrl = "~/userfiles/PD/" + st.PD;
        //lblPD.NavigateUrl = pdfURL + "PD/" + st.PD;
        lblPD.NavigateUrl = SiteConfiguration.GetDocumnetPath("PD", st.PD);

        lblPR.Visible = true;
        lblPR.Text = st.PR;
        //lblPR.NavigateUrl = "~/userfiles/PR/" + st.PR;
        //lblPR.NavigateUrl = pdfURL + "PR/" + st.PR;
        lblPR.NavigateUrl = SiteConfiguration.GetDocumnetPath("PR", st.PR);

        lblbeat.Visible = true;
        lblbeat.Text = st.beatquotedoc;
        //lblbeat.NavigateUrl = pdfURL + "BeatQuote/" + st.beatquotedoc;
        lblbeat.NavigateUrl = SiteConfiguration.GetDocumnetPath("BeatQuote", st.beatquotedoc);
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    protected void btnUpdateQuote_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string CompanyId = Request.QueryString["compid"];

        string QuoteAccepted = txtQuoteAcceptedQuote.Text.Trim();
        string SignedQuote = Convert.ToString(chkSignedQuote.Checked);
        string InvoiceNumber = txtInvoiceNumber.Text;
        string InvoiceSent = txtInvoiceSent.Text.Trim();
        string DepositReceived = txtDepositReceived.Text.Trim();
        string InvoiceDoc = txtInvoiceDoc.Text;
        //string WelcomeLetterDone = Convert.ToString(chkWelcomeLetterDone.Checked);
        string MeterBoxPhotosSaved = Convert.ToString(chkMeterBoxPhotosSaved.Checked);
        //Response.Write("grgersh");
        //Response.End();
        string ElecBillSaved = Convert.ToString(chkElecBillSaved.Checked);
        string ProposedDesignSaved = Convert.ToString(chkProposedDesignSaved.Checked);
        string PaymentReceipt = Convert.ToString(chkPaymentReceipt.Checked);
        string nearmap = Convert.ToString(chknearmap.Checked);

        string ActiveDate = txtActiveDate.Text.Trim();

        string SQ = "";
        string MP = "";
        string EB = "";
        string PD = "";
        string PR = "";
        string beat = "";
        string map = "";

        DataTable dt_inv1 = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
        {
            if (dt_inv1.Rows.Count > 0)
            {


                if (dt_inv1.Rows[0]["InvoicePayDate"].ToString() != string.Empty)
                {
                    hiddenExpiryDate.Value = Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]).AddDays(-1).ToString();//System.DateTime.Now.AddDays(-1).ToString();

                    DateTime dtInvPay = Convert.ToDateTime(hiddenExpiryDate.Value);
                    //CompareValidatorDepRec.ValueToCompare = DateTime.Today.Date.ToString("dd/MM/yyyy"); //string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]));
                    if (txtDepositReceived.Text != null && txtDepositReceived.Text != "")
                    {
                        DateTime dtDeprec = Convert.ToDateTime(txtDepositReceived.Text);
                        if (dtDeprec < dtInvPay)
                        {
                            MsgError("Deposit Date Canot be less then First Dep. Date");
                            return;
                        }
                    }
                }
            }
        
        }
        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
        {
           //if(txtActiveDate.Text!=null && txtActiveDate.Text !="" && txtDepositReceived.Text != null && txtDepositReceived.Text != "")
           // {
           //     DateTime activeDate = Convert.ToDateTime(txtActiveDate.Text);
           //     DateTime dtDeprec = Convert.ToDateTime(txtDepositReceived.Text);
           //     if (activeDate <= dtDeprec)
           //     {
           //         MsgError("Active Date Canot be less then  Dep.Rec Date");
           //         return;
           //     }
           // }
            if (txtActiveDate.Text != null && txtActiveDate.Text != "")
            {
               // DateTime ActiveDate1 = Convert.ToDateTime(txtActiveDate.Text);
               // DateTime dtToday = DateTime.Now.AddHours(14);
                string dtToday= Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();
                string ActDt = Convert.ToDateTime(txtActiveDate.Text).ToShortDateString();
                if (Convert.ToDateTime(ActDt) < Convert.ToDateTime(dtToday))
                {
                    MsgError("Active date should be less then today date..");
                    return;
                }
            }

        }
       
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //===================================================================
        if (chkactive.Checked)
        {
            bool sucyactive = ClstblProjects.tblProjects_Updatereadyactive(ProjectID, chkactive.Checked.ToString());
        }

        //GoogleSheet Code By Kiran 07-09-2019
        SttblCustomers stcomp = ClstblCustomers.tblCustomers_SelectByCustomerID(CompanyId);

        if (txtDepositReceived.Text != string.Empty)
        {
            if (string.IsNullOrEmpty(st.DepositReceived))
            {
                if (!string.IsNullOrEmpty(stcomp.gclid))
                {
                    string str = stcomp.gclid;
                    //if (str.Contains("?gclid="))
                    //{
                    //    int start = str.IndexOf("?gclid=") + ("?gclid=").Length;
                    //    string value = str.Substring(start);
                    //bool updategcid = ClstblCustomers.tblCustomers_Updategclid(Convert.ToString(success), value);



                    //GoogleSheet Code Start
                    try
                    {
                        GoogleCredential credential;
                        //Reading Credentials File...
                        //For Local
                        //string filepath = HttpContext.Current.Server.MapPath("app_client_secret.json");

                        //For Live
                        string filepath = HttpContext.Current.Server.MapPath("AriseSolar-692588a0321d.json");
                        using (var stream = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                        {
                            credential = GoogleCredential.FromStream(stream)
                                .CreateScoped(Scopes);
                        }

                        //Creating Google Sheets API service...
                        service = new SheetsService(new BaseClientService.Initializer()
                        {
                            HttpClientInitializer = credential,
                            ApplicationName = ApplicationName,
                        });

                        List<object> list = null;
                        //var range = $"{sheet}!A:S";
                        string range = "A:F";
                        var valueRange = new ValueRange();
                        DateTime dt = Convert.ToDateTime(txtDepositReceived.Text);
                        list = new List<object>() { str, "Offline Sales FINAL", dt.ToString("MM/dd/yyyy hh:mm:ss"), st.TotalQuotePrice, "", "" };

                        valueRange.Values = new List<IList<object>> { list };
                        var appendRequest = service.Spreadsheets.Values.Append(valueRange, SpreadsheetId, range);
                        appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
                        var appendReponse = appendRequest.Execute();

                    }
                    catch (Exception ex)
                    {

                    }
                    //GoogleSheet Code End


                    // }
                }
            }
        }

        //tblProjects_Updatereadyactive
        //=====================================================================
        /* ----------------------- Deposite Rec Status ----------------------- */
        //Mtn-New Inquiry than cant change Status Updated change by Kiran on 01-10-2019
        SttblProjects stmntc = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (stmntc.ProjectStatusID != "15")
        {
            if (st.ActiveDate == string.Empty)
            {
                if (txtDepositReceived.Text != string.Empty)
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                    if (dtStatusDR.Rows.Count > 0)
                    {
                    }
                    else
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    }
                    //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                    {                        
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                        ClstblCustomers.tblCustomers_UpdateCustType("3", Request.QueryString["compid"]);
                    }
                }
                else
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "2");
                    if (dtStatusDR.Rows.Count > 0)
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    }
                    //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                    {
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "2");
                    }
                }
            }
        }
        /* -------------------------------------------------------------- */

        if (chkSignedQuote.Checked == true)
        {
            // Response.Write("hii");
            //if (st.SQ == string.Empty)
            //{
            //    RequiredFieldValidatorSQ.Visible = true;
            //}
            //else
            //{
            //    RequiredFieldValidatorSQ.Visible = false;
            //}

            if (fuSQ.HasFile)
            {
                SiteConfiguration.DeletePDFFile("SQ", st.SQ);
                SQ = ProjectID + fuSQ.FileName;
                fuSQ.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/SQ/") + SQ);
                bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
                SiteConfiguration.UploadPDFFile("SQ", SQ);
                SiteConfiguration.deleteimage(SQ, "SQ");

            }
            //else
            //{
            //    bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, st.SQ);
            //}
        }
        else
        {
            SiteConfiguration.DeletePDFFile("SQ", st.SQ);
            bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, "");

            //RequiredFieldValidatorSQ.Visible = false;
        }

        if (chkMeterBoxPhotosSaved.Checked == true)
        {
            // RequiredFieldValidatorMP.Visible = true;
            if (fuMP.HasFile)
            {
                SiteConfiguration.DeletePDFFile("MP", st.MP);
                MP = ProjectID + fuMP.FileName;
                fuMP.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\MP\\") + MP);
                bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, MP);
                SiteConfiguration.UploadPDFFile("MP", MP);
                SiteConfiguration.deleteimage(MP, "MP");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("MP", st.MP);
            bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, "");
            // RequiredFieldValidatorMP.Visible = false;
        }
        if (chkElecBillSaved.Checked == true)
        {
            //RequiredFieldValidatorEB.Visible = true;
            if (fuEB.HasFile)
            {
                SiteConfiguration.DeletePDFFile("EB", st.EB);
                EB = ProjectID + fuEB.FileName;
                fuEB.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\EB\\") + EB);
                bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, EB);
                SiteConfiguration.UploadPDFFile("EB", EB);
                SiteConfiguration.deleteimage(EB, "EB");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("EB", st.EB);
            bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, "");
            // RequiredFieldValidatorEB.Visible = false;
        }
        if (chkProposedDesignSaved.Checked == true)
        {
            // RequiredFieldValidatorPD.Visible = true;
            if (fuPD.HasFile)
            {
                SiteConfiguration.DeletePDFFile("PD", st.PD);
                PD = ProjectID + fuPD.FileName;
                fuPD.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PD\\") + PD);
                bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, PD);
                SiteConfiguration.UploadPDFFile("PD", PD);
                SiteConfiguration.deleteimage(PD, "PD");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("PD", st.PD);
            bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, "");
            // RequiredFieldValidatorPD.Visible = false;
        }

        if (chkPaymentReceipt.Checked == true)
        {
            // RequiredFieldValidatorPR.Visible = true;
            if (fuPR.HasFile)
            {
                SiteConfiguration.DeletePDFFile("PR", st.PR);
                PR = ProjectID + fuPR.FileName;
                fuPR.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PR\\") + PR);
                bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, PR);
                SiteConfiguration.UploadPDFFile("PR", PR);
                SiteConfiguration.deleteimage(PR, "PR");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("PR", st.PR);
            bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, "");
            // RequiredFieldValidatorPR.Visible = false;
        }
        if (chkbeatquote.Checked == true)
        {
            // RequiredFieldValidatorbeat.Visible = true;
            if (fubeat.HasFile)
            {
                SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
                beat = ProjectID + fubeat.FileName;
                fubeat.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\BeatQuote\\") + beat);
                bool sucbeat = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, beat);
                SiteConfiguration.UploadPDFFile("BeatQuote", beat);
                SiteConfiguration.deleteimage(beat, "BeatQuote");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
            bool sucMP = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, "");
            // RequiredFieldValidatorbeat.Visible = false;
        }

        if (chknearmap.Checked == true)
        {
            //pdfURL = "http://b2848392e002ee33fd61-5c566850018ebfb871276c1331018ded.r57.cf2.rackcdn.com/";
            //RequiredFieldValidatormap.Visible = true;
            if (funearmap.HasFile)
            {
                bool s1 = ClsProjectSale.tblProjects_UpdatenearmapPdf(ProjectID, "");
                bool sucma8 = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
               bool sucmp6565 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, "");
                bool sucmap28952 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");
                
                SiteConfiguration.DeletePDFFile("NearMap", st.PdfFileNaame);
                //map = "1" + ProjectID + funearmap.FileName;
                map = funearmap.FileName;               
                //Response.Write(map);
                //Response.End();
                string path = Request.PhysicalApplicationPath + "\\userfiles\\NearMap\\";
                funearmap.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\NearMap\\") + map);
                string PdfFilePath = Request.PhysicalApplicationPath + "\\userfiles\\NearMap\\" + map;
                if (File.Exists(PdfFilePath))
                {
                    int NearMapId = ClsProjectSale.tblNearMapImages_Insert(ProjectID, st.ProjectNumber, map, DateTime.Now.AddHours(14).ToString());
                    bool s12 = ClsProjectSale.tblProjects_UpdatenearmapPdf(ProjectID, map);
                    SiteConfiguration.UploadPDFFile("NearMap", map);
                    using (var document = PdfDocument.Load(PdfFilePath))
                    {
                        var pageCount = document.PageCount;

                        for (int i = 0; i < pageCount; i++)
                        {
                            var dpi =100;

                            using (var image = document.Render(i, dpi, dpi, PdfRenderFlags.CorrectFromDpi))
                            {
                                var encoder = ImageCodecInfo.GetImageEncoders()
                                    .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                                var encParams = new EncoderParameters(1);
                                encParams.Param[0] = new EncoderParameter(
                                    System.Drawing.Imaging.Encoder.Quality, 100L);
                                // image.Save(projectDir + "/output_" + i + ".jpg", encoder, encParams);
                                //string filname = ProjectID + "Nearmap" + i + ".jpg";
                                if (i == 0)
                                {
                                    string filname = "";
                                    if (st.nearmapdoc != null && st.nearmapdoc != "")
                                    {
                                        bool sucmap34 = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");

                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                        SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
                                        //DeleteFileFromFolder(st.nearmapdoc);
                                        // bool sucma = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
                                        // bool sucmp1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, "");
                                        //bool sucmap2 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");
                                    }
                                    else
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";

                                    }
                                    //string filname = ProjectID + "Nearmap_" + i + ".jpg";
                                    image.Save(path + "/" + filname, encoder, encParams);
                                    //   image.Save(PdfFilePath + ProjectID + "Nearmap" + i + ".jpg", encoder, encParams);

                                    bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, filname);
                                    bool sucmap2 = ClsProjectSale.tblNearMapImages_UpdateImage1(NearMapId.ToString(), filname);
                                    SiteConfiguration.UploadPDFFile("NearMap", filname);
                                    //SiteConfiguration.deleteimage("NearMap", filname);
                                    SiteConfiguration.deleteimage(filname, "NearMap");

                                }
                                if (i == 1)
                                {
                                    string filname = "";
                                    if (st.nearmapdoc1 != null && st.nearmapdoc1 != "")
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";

                                        SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc1);
                                        // DeleteFileFromFolder(st.nearmapdoc1);
                                        // bool sucma = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
                                        bool sucmp1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, "");
                                        //bool sucmap2 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");
                                    }
                                    else
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                    }

                                    //string filname = ProjectID + "Nearmap_" + i + ".jpg";
                                    image.Save(path + "/" + filname, encoder, encParams);
                                    bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, filname);
                                    bool sucmap2 = ClsProjectSale.tblNearMapImages_UpdateImage2(NearMapId.ToString(), filname);
                                    SiteConfiguration.UploadPDFFile("NearMap", filname);
                                    // SiteConfiguration.deleteimage("NearMap",filname);
                                    SiteConfiguration.deleteimage(filname, "NearMap");
                                }
                                if (i == 2)
                                {
                                    string filname = "";
                                    if (st.nearmapdoc2 != null && st.nearmapdoc2 != "")
                                    {

                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                        SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc2);
                                        //DeleteFileFromFolder(st.nearmapdoc2);
                                        // bool sucma = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
                                        bool sucmp1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");

                                    }
                                    else
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                    }

                                    // string filname = ProjectID + "Nearmap_" + i + ".jpg";
                                    image.Save(path + "/" + filname, encoder, encParams);
                                    bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, filname);
                                    bool sucmap2 = ClsProjectSale.tblNearMapImages_UpdateImage3(NearMapId.ToString(), filname);
                                    SiteConfiguration.UploadPDFFile("NearMap", filname);
                                    //SiteConfiguration.deleteimage("NearMap", filname);
                                    SiteConfiguration.deleteimage(filname, "NearMap");
                                }
                            }
                        }

                        //SiteConfiguration.deleteimage(map, "NearMap");
                    }
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        // Console.WriteLine(ex.Message);
                    }
                    SiteConfiguration.deleteimage(map, "NearMap");
                }
                BindData();
                //bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, map);
                //bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, map);

                //SiteConfiguration.UploadPDFFile("NearMap", map);
                //SiteConfiguration.deleteimage(map, "NearMap");
            }

            //if (funearmap1.HasFile)
            //{
            //    SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc1);
            //    map = "2" + ProjectID + funearmap1.FileName;

            //    //Response.Write(map);
            //    //Response.End();
            //    funearmap1.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\NearMap\\") + map);
            //    //bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, map);
            //    bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, map);

            //    SiteConfiguration.UploadPDFFile("NearMap", map);
            //    SiteConfiguration.deleteimage(map, "NearMap");
            //}

            //if (funearmap2.HasFile)
            //{
            //    SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc2);
            //    map = "3" + ProjectID + funearmap2.FileName;

            //    //Response.Write(map);
            //    //Response.End();
            //    funearmap2.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\NearMap\\") + map);
            //    //bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, map);
            //    bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, map);

            //    SiteConfiguration.UploadPDFFile("NearMap", map);
            //    SiteConfiguration.deleteimage(map, "NearMap");
            //}
        }
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
        //    SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc1);//Second file
        //    SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc2);
        //    //bool sucMP = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
        //    bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, "");
        //    bool sucmap2 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");

        //    //RequiredFieldValidatormap.Visible = false;
        //}
        else
        {
            bool s1 = ClsProjectSale.tblProjects_UpdatenearmapPdf(ProjectID, "");
            bool sucma8 = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
            bool sucmp6565 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, "");
            bool sucmap28952 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");
        }
        string UpdatedBy = stEmp.EmployeeID;
        bool sucQuote = ClsProjectSale.tblProjects_UpdateQuote(ProjectID, QuoteAccepted, SignedQuote, InvoiceNumber, InvoiceSent, DepositReceived, InvoiceDoc, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, UpdatedBy, PaymentReceipt, ActiveDate);
        bool succ_beatquote = ClsProjectSale.tblProjects_Updatebeatquote(ProjectID, chkbeatquote.Checked.ToString());
        bool succ_nearmap = ClsProjectSale.tblProjects_UpdateQuotenearmapcheck(ProjectID, chknearmap.Checked.ToString());
        string mtcepaperwork = "";
        if (FUmtcepaperwork.HasFile)
        {
            try
            {
                SiteConfiguration.DeletePDFFile("MtcePaperWork", st.mtcepaperwork);
            }
            catch
            {
            }

            mtcepaperwork = ProjectID + FUmtcepaperwork.FileName;
            FUmtcepaperwork.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/MtcePaperWork/") + mtcepaperwork);
            bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, mtcepaperwork);
            SiteConfiguration.UploadPDFFile("MtcePaperWork", mtcepaperwork);
            SiteConfiguration.deleteimage(mtcepaperwork, "MtcePaperWork");
        }
        else
        {
            bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, st.mtcepaperwork);
        }
        /* ----------------------- Active Status ----------------------- */
        TextWriter txtWriter = new StringWriter() as TextWriter;
        StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");

        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        //Response.Write(st.ContactID);
        //Response.End();
        string from = stU.from;
        string mailto = stCont.ContEmail;
        string subject = "Thank you for choosing Arisesolar";
        //==========for test==============
        ////Server.Execute("~/mailtemplete/activemail.aspx?Customer=" + st.Customer, txtWriter);
        ////try
        ////{

        ////  //  Response.Write(from + "======" + mailto + "======" + subject + "======" + txtWriter.ToString());
        ////    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());

        ////}
        ////catch
        ////{
        ////}
        // Response.End();
        //=============================
        if (stmntc.ProjectStatusID != "15")
        {
            if (st.InstallCompleted == string.Empty)
            {
                if (txtActiveDate.Text != string.Empty)
                {
                    if (st.SurveyCerti == "1")
                    {
                        if (st.CertiApprove == "1")
                        {
                            DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                            {
                                if (dtStatusDR.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                {
                                    if (st.ProjectStatusID != "11")
                                    {
                                        if (st.InstallState == "VIC")
                                        {
                                            if (!string.IsNullOrEmpty(st.VicAppReference))
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        else
                                        {

                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                    //try
                                    //{
                                    //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    //    //   Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                    //}
                                    //catch
                                    //{
                                    //}
                                }
                            }
                            else
                            {
                                if (st.DocumentVerified == "True")
                                {
                                    if (dtStatusDR.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                    }
                                    if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                    {
                                        if (st.ProjectStatusID != "11")
                                        {
                                            if (st.InstallState == "VIC")
                                            {
                                                if (!string.IsNullOrEmpty(st.VicAppReference))
                                                {
                                                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                                }
                                            }
                                            else
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                        //try
                                        //{
                                        //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                        //    //  Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                        //}
                                        //catch
                                        //{
                                        //}
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            if (dtStatusDR.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                            }
                            if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                            {
                                if (st.ProjectStatusID != "11")
                                {
                                    if (st.InstallState == "VIC")
                                    {
                                        if (!string.IsNullOrEmpty(st.VicAppReference))
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    else
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                //try
                                //{
                                //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                //    // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                //}
                                //catch
                                //{
                                //}
                            }
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                if (dtStatusDR.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                {
                                    if (st.ProjectStatusID == "2" || st.ProjectStatusID == "8")//Changes on 4/7/2020 By Nilesh Add st.ProjectStatusID != "12"
                                    {
                                        if (st.InstallState == "VIC")
                                        {
                                            if (!string.IsNullOrEmpty(st.VicAppReference))
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        //else(st.ProjectStatusID != "11" && st.ProjectStatusID != "12")//Changes on 4/7/2020 By Nilesh Add
                                        //{
                                        //    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        //}
                                        else 
                                        {
                                            if (st.ProjectStatusID == "2" || st.ProjectStatusID == "8")//Changes on 4/7/2020 By Nilesh Add
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                    }
                                    //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                    //try
                                    //{
                                    //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    //    // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                    //}
                                    //catch(Exception ex)
                                    //{

                                    //}
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (txtDepositReceived.Text != string.Empty)
                    {
                        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                        if (dtStatusDR.Rows.Count > 0)
                        {
                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                        }
                        //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                        {
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                        }
                    }
                }
            }
        }

        /* ------------------------------------------------------------- */
        //============put by roshni==================///
        //if (st.SignedQuote != string.Empty && st.DepositReceived != string.Empty && st.QuoteAccepted != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.DocumentVerified != string.Empty)
        //{
        //    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //}


        //==================================


        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Rep"))
        {
            try
            {
                ClstblProjects.tblProjects_UpdateQuoteSentDate(ProjectID, txtQuoteSent.Text);
            }
            catch(Exception ex)
            {
            }
        }

        if (sucQuote)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }
        BindScript();
        BindData();
        BindProjectQuote();
        activemethod();
        BindProjects();
    }
    public void activemethod()//put by roshni
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if ((chkSignedQuote.Checked.ToString() == false.ToString() || st.QuoteAccepted == string.Empty || st.ActiveDate == string.Empty) && st.DepositReceived != string.Empty)
        {
            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
        }
        if (st.ActiveDate == string.Empty && st.DepositReceived == string.Empty)
        {
            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "2");
        }
    }
    protected void chkSignedQuote_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (chkSignedQuote.Checked == true)
        {
            divSQ.Visible = true;
            if (st.SQ == string.Empty)
            {
                //RequiredFieldValidatorSQ.Visible = true;
            }
            else
            {
                // RequiredFieldValidatorSQ.Visible = false;
            }
        }
        else
        {
            divSQ.Visible = false;
            //RequiredFieldValidatorSQ.Visible = false;
        }
        BindScript();
    }
    protected void chkMeterBoxPhotosSaved_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (chkMeterBoxPhotosSaved.Checked == true)
        {
            // divMP.Visible = true;

        }
        else
        {
            // divMP.Visible = false;
        }
    }
    protected void chkElecBillSaved_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;

        if (chkElecBillSaved.Checked == true)
        {
            //divEB.Visible = true;
        }
        else
        {
            //  divEB.Visible = false;
        }
    }
    protected void chkProposedDesignSaved_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (chkProposedDesignSaved.Checked == true)
        {
            // divPD.Visible = true;
        }
        else
        {
            //  divPD.Visible = false;
        }
    }

    public void BindQuote()
    {
        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        if (dtQuote.Rows.Count > 0)
        {
            divQuotes2.Visible = true;
            divQuotes.Visible = true;
            rptQuote.DataSource = dtQuote;
            rptQuote.DataBind();
            if (dtQuote.Rows.Count > 5)
            {
                divquo.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divquo.Attributes.Add("style", "");
            }


        }
        else
        {
            divQuotes2.Visible = false;
            divQuotes.Visible = false;
        }
    }
    protected void btnCreateQuote_Click(object sender, EventArgs e)
    {
        string QuoteID = string.Empty;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string ProjectNumber = st.ProjectNumber;
        //DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectTop1(ProjectID);
        DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectByProjectID(ProjectID);
        if (dtTop1.Rows.Count > 0)
        {
        }
        else
        {
            bool sucQoute = ClstblProjects.tblProjects_UpdateQuoteSent(ProjectID);
        }

        BindProjectQuote();

        int success = ClstblProjects.tblProjectQuotes_Insert(ProjectID, ProjectNumber, EmployeeID);
        string QuoteDoc = "Quotation.pdf";

        if (Convert.ToString(success) != "")
        {
            QuoteID = Convert.ToString(success);
            QuoteDoc = Convert.ToString(success) + QuoteDoc;
            bool suc = ClstblProjects.tblProjectQuotes_UpdateProjectQuoteDoc(Convert.ToString(success), QuoteDoc);
        }

        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
        TextWriter txtWriter = new StringWriter() as TextWriter;

        if (st.ProjectTypeID == "8")
        {
            if (stCust.ResCom == "2")
            {
                Server.Execute("~/mailtemplate/mtcecommquote.aspx?id=" + ProjectID, txtWriter);
            }
            else
            {
                Server.Execute("~/mailtemplate/mtcequote.aspx?id=" + ProjectID, txtWriter);
            }
        }
        else
        {
            if (stCust.ResCom == "2")
            {
                Telerik_reports.generate_quatation(ProjectID, QuoteID);
                //Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);
            }
            else
            {


                Telerik_reports.generate_quatation(ProjectID, QuoteID);
                // Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);//quatationnew
                //Server.Execute("~/mailtemplate/receipt.aspx?id=" + ProjectID, txtWriter);//quatationdesign
            }
        }

        String htmlText = txtWriter.ToString();

        //HTMLExportToPDF(htmlText, ProjectID + "Quote.pdf");
        // SavePDF(htmlText, QuoteDoc);
        BindQuote();
    }

    public void SavePDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        try
        {
            string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
            string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
            // create an API client instance
            pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
            // convert a web page and write the generated PDF to a memory stream
            MemoryStream Stream = new MemoryStream();

            //client.setVerticalMargin("66pt");
            client.setVerticalMargin("0pt");
            client.setHorizontalMargin("0pt");

            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/pdfheader.aspx", txtWriter);

            //TextWriter txtWriter2 = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/pdffooter.aspx?id" + Request.QueryString["proid"], txtWriter2);
            //Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);
         
            //string HeaderHtml = txtWriter.ToString();
            //string FooterHtml = txtWriter2.ToString();

            //client.setHeaderHtml(HeaderHtml);
            //client.setFooterHtml(FooterHtml);
            //client.setHeaderFooterPageExcludeList("1,-1");




            //--------------------------------------O N L I N E--------------------------------------------------
            FileStream fs = new FileStream(Request.PhysicalApplicationPath + "\\userfiles\\quotedoc\\" + filename, FileMode.Create);

            client.convertHtml(HTML, fs);
            fs.Flush();
            fs.Close();
            SiteConfiguration.UploadPDFFile("quotedoc", filename);
            SiteConfiguration.deleteimage(filename, "quotedoc");
            //------------------------------------------------------------------------------------------------------


            //FileStream fs = new FileStream(Request.PhysicalApplicationPath + "\\userfiles\\quotedoctest\\" + filename, FileMode.Create);

            //client.convertHtml(HTML, fs);
            //fs.Flush();
            //fs.Close();
            //SiteConfiguration.UploadPDFFile("quotedoctest", filename);
            //SiteConfiguration.deleteimage(filename, "quotedoctest");

        }
        catch (pdfcrowd.Error why)
        {
            //Response.Write(why.ToString());
        }
    }

    public void BindInvoice()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        txtInvoiceNumber.Text = st.InvoiceNumber;
        txtInvoiceDoc.Text = st.InvoiceDoc;
    }
    protected void btnCreateInvoice_Click(object sender, EventArgs e)
    {

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //Response.Write(ProjectID);
        //Response.End();
        string InvoiceDoc = ProjectID + "Quotation.pdf";
        if (ProjectID != "")
        {
            bool suc = ClstblProjects.tblProjects_UpdateInvoiceDoc(ProjectID, st.ProjectNumber);
            if (suc)
            {
                btnCreateInvoice.Visible = false;
                btnOpenInvoice.Visible = true;
                        
            }
            else
            {
                btnCreateInvoice.Visible = true;
                btnOpenInvoice.Visible = false;
            }
        }
        BindInvoice();
        BindProjectQuote();
    }
    protected void btnOpenInvoice_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.ProjectTypeID == "8")
        {
            Telerik_reports.generate_mtceinvoice(ProjectID, "Download");
        }
        else
        {
            Telerik_reports.generate_Taxinvoice(ProjectID, "Download");
        }
        BindProjectQuote();
        BindInvoice();
        BindScript();
    }

    protected void btnWelcomeDocs_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        bool suc = ClstblProjects.tblProjects_UpdateWelcomeLetter(ProjectID);
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        TextWriter txtWriter = new StringWriter() as TextWriter;
        //try
        //{
        if (stPro.InstallState == "VIC")
        {
            Telerik_reports.generate_welcomelettervic(ProjectID);
            //Server.Execute("~/mailtemplate/welcomelettervic.aspx?id=" + ProjectID, txtWriter);
        }
        else
        {
            Telerik_reports.generate_welcomeletter(ProjectID);
            //Server.Execute("~/mailtemplate/welcomeletter.aspx?id=" + ProjectID, txtWriter);
        }
        //}
        //catch { }
        // String htmlText = txtWriter.ToString();
        //Response.Write(htmlText);
        //HTMLExportToPDF(htmlText, ProjectID + "WelcomeLetter.pdf");
    }
    public void HTMLExportToPDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        try
        {
            string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
            string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
            // create an API client instance
            pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
            // convert a web page and write the generated PDF to a memory stream
            MemoryStream Stream = new MemoryStream();
            //client.setVerticalMargin("0pt");

            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/pdfheader.aspx", txtWriter);

            //TextWriter txtWriter2 = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

            //string HeaderHtml = txtWriter.ToString();
            //string FooterHtml = txtWriter2.ToString();

            //client.setHeaderHtml(HeaderHtml);
            //client.setFooterHtml(FooterHtml);
            client.setVerticalMargin("0pt");
            client.setHorizontalMargin("0pt");
            //client.setPageWidth("960pt");
            //client.setPageHeight("1365pt");
            client.convertHtml(HTML, Stream);

            // set HTTP response headers
            Response.Clear();
            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Cache-Control", "no-cache");
            Response.AddHeader("Accept-Ranges", "none");
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            // send the generated PDF
            Stream.WriteTo(Response.OutputStream);
            Stream.Close();
            Response.Flush();
            Response.End();
        }
        catch (pdfcrowd.Error why)
        {
            //Response.Write(why.ToString());
            //Response.End();
        }
    }
    //protected void btnCheckActive_Click(object sender, EventArgs e)
    //{
    //    string ProjectID = Request.QueryString["proid"];
    //    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    if (st.SignedQuote != "False")
    //    {
    //        lblSignedQuote.Text = "Signed Quote: Yes";
    //    }
    //    else
    //    {
    //        lblSignedQuote.Text = "Signed Quote: No";
    //    }
    //    if (st.ElecDistributorID == string.Empty)
    //    {
    //        lblEleDest.Text = "Please select Elec Distributor.";
    //    }
    //    else
    //    {
    //        lblEleDest.Visible = false;
    //    }
    //    if (st.ElecDistributorID == "12")
    //    {
    //        if (st.ElecDistApprovelRef != string.Empty)
    //        {
    //            lblApprovalRef.Text = "Approval Ref: Yes";
    //        }
    //        else
    //        {
    //            lblApprovalRef.Text = "Approval Ref: No";
    //        }
    //        if (st.NMINumber != string.Empty)
    //        {
    //            lblNMINumber.Text = "NMI Number: Yes";
    //        }
    //        else
    //        {
    //            lblNMINumber.Text = "NMI Number: No";
    //        }

    //        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
    //        {
    //            lblDocumentVerified.Visible = false;
    //        }
    //        else
    //        {
    //            lblDocumentVerified.Visible = true;
    //            if (st.DocumentVerified == "True")
    //            {
    //                lblDocumentVerified.Text = "Document Verification: Yes";
    //            }
    //            else
    //            {
    //                lblDocumentVerified.Text = "Document Verification: No";
    //            }
    //        }
    //        lblApprovalRef.Visible = true;
    //        lblNMINumber.Visible = true;
    //    }
    //    else if (st.ElecDistributorID == "13")
    //    {
    //        if (st.RegPlanNo != string.Empty)
    //        {
    //            lblRegPlanNo.Text = "Reg Plan No: Yes";
    //        }
    //        else
    //        {
    //            lblRegPlanNo.Text = "Reg Plan No: No";
    //        }
    //        if (st.LotNumber != string.Empty)
    //        {
    //            lblLotNum.Text = "LotNumber: Yes";
    //        }
    //        else
    //        {
    //            lblLotNum.Text = "LotNumber: No";
    //        }
    //        if (st.ElecDistApprovelRef != string.Empty)
    //        {
    //            lblApprovalRef.Text = "Approval Ref: Yes";
    //        }
    //        else
    //        {
    //            lblApprovalRef.Text = "Approval Ref: No";
    //        }
    //        if (st.NMINumber != string.Empty)
    //        {
    //            lblNMINumber.Text = "NMI Number: Yes";
    //        }
    //        else
    //        {
    //            lblNMINumber.Text = "NMI Number: No";
    //        }

    //        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
    //        {
    //            lblDocumentVerified.Visible = false;
    //        }
    //        else
    //        {
    //            lblDocumentVerified.Visible = true;
    //            if (st.DocumentVerified == "True")
    //            {
    //                lblDocumentVerified.Text = "Document Verification: Yes";
    //            }
    //            else
    //            {
    //                lblDocumentVerified.Text = "Document Verification: No";
    //            }
    //        }
    //        lblRegPlanNo.Visible = true;
    //        lblLotNum.Visible = true;
    //        lblApprovalRef.Visible = true;
    //        lblNMINumber.Visible = true;
    //    }
    //    else
    //    {
    //        if (st.ElecDistApprovelRef != string.Empty)
    //        {
    //            lblApprovalRef.Text = "Approval Ref: Yes";
    //        }
    //        else
    //        {
    //            lblApprovalRef.Text = "Approval Ref: No";
    //        }
    //        if (st.NMINumber != string.Empty)
    //        {
    //            lblNMINumber.Text = "NMI Number: Yes";
    //        }
    //        else
    //        {
    //            lblNMINumber.Text = "NMI Number: No";
    //        }

    //        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
    //        {
    //            lblDocumentVerified.Visible = false;
    //        }
    //        else
    //        {
    //            lblDocumentVerified.Visible = true;
    //            if (st.DocumentVerified == "True")
    //            {
    //                lblDocumentVerified.Text = "Document Verification: Yes";
    //            }
    //            else
    //            {
    //                lblDocumentVerified.Text = "Document Verification: No";
    //            }
    //        }
    //        lblRegPlanNo.Visible = false;
    //        lblLotNum.Visible = false;
    //        lblApprovalRef.Visible = true;
    //        lblNMINumber.Visible = true;
    //    }
    //    //else
    //    //{
    //    //    lblRegPlanNo.Visible = false;
    //    //    lblLotNum.Visible = false;
    //    //    lblApprovalRef.Visible = false;
    //    //    lblNMINumber.Visible = false;
    //    //}

    //    if (st.DepositReceived != string.Empty)
    //    {
    //        lblDepositeRec.Text = "Deposit Rec: Yes";
    //    }
    //    else
    //    {
    //        lblDepositeRec.Text = "Deposit Rec: No";
    //    }
    //    if (st.QuoteAccepted != string.Empty)
    //    {
    //        lblQuoteAccepted.Text = "Quote Accepted: Yes";
    //    }
    //    else
    //    {
    //        lblQuoteAccepted.Text = "Quote Accepted: No";
    //    }
    //    //if (st.SQ != "" && st.SignedQuote != "False" && st.DepositReceived != "" && st.RegPlanNo != "" && st.LotNumber != "" && st.ElecDistApprovelRef != "" && st.NMINumber != "")
    //    if (st.ProjectStatusID == "3" || st.ProjectStatusID == "5")
    //    {
    //        lblActive.Text = "Project is Active";
    //    }
    //    else
    //    {
    //        lblActive.Text = "Project is not Active";
    //    }
    //    ModalPopupExtender2.Show();
    //}
    //protected void ibtnCancelActive_Onclick(object sender, EventArgs e)
    //{
    //    ModalPopupExtender2.Hide();
    //}
    protected void chkPaymentReceipt_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (chkPaymentReceipt.Checked == true)
        {
            //divPR.Visible = true;
        }
        else
        {
            // divPR.Visible = false;
        }
    }
    protected void rptQuote_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndProjectQuoteID = (HiddenField)e.Item.FindControl("hndProjectQuoteID");
            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc");
            LinkButton btnSendAMail = (LinkButton)e.Item.FindControl("btnSendAMail");
            Image Image2 = (Image)e.Item.FindControl("Image2");
            //Label lblsignchked = (Label)e.Item.FindControl("lblsignchked");
            //Label lblsignUnChked = (Label)e.Item.FindControl("lblsignUnChked");
            // System.Web.UI.WebControls.CheckBox chkSerialNo = (System.Web.UI.WebControls.CheckBox)e.Item.FindControl("chkSerialNo");


            SttblProjectQuotes st = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteID(hndProjectQuoteID.Value);

            int Signed = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, st.ProjectQuoteDoc);
            if (Signed == 1)
            {
                //lblsignchked.Visible = true;
                //lblsignUnChked.Visible = false;               
                btnSendAMail.Visible = true;
                btnSendAMail.PostBackUrl = "~/admin/adminfiles/company/company.aspx?m=pro&compid=" + Request.QueryString["compid"] + "&proid=" + Request.QueryString["proid"];
                Image2.Visible = true;

            }
            else
            {
                //lblsignchked.Visible = false;
                //lblsignUnChked.Visible = true;
                btnSendAMail.Visible = false;
                Image2.Visible = false;
            }

            //hypDoc.NavigateUrl = pdfURL + "quotedoc/" + st.QuoteDoc;
            hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("quotedoc", st.QuoteDoc);
        }
    }
    protected void chkbeatquote_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (chkbeatquote.Checked == true)
        {
            //Spanbeat.Visible = true;
        }
        else
        {
            //Spanbeat.Visible = false;
        }
    }
    protected void imgbtnPerforma_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        TextWriter txtWriter = new StringWriter() as TextWriter;
        //try
        //{
        if (st.ProjectTypeID == "8")
        {
            Telerik_reports.generate_performamtceinvoice(ProjectID);
            //Server.Execute("~/mailtemplate/performamtceinvoice.aspx?id=" + ProjectID, txtWriter);
        }
        else
        {
            Telerik_reports.generate_performainvoice(ProjectID);
            // Server.Execute("~/mailtemplate/performainvoice.aspx?id=" + ProjectID, txtWriter);
        }

        //}
        //catch { }
        // String htmlText = txtWriter.ToString();
        // HTMLExportToPDF(htmlText, ProjectID + "PerformaInvoice.pdf");

        BindProjectQuote();
        BindInvoice();
    }

    protected void chknearmap_CheckedChanged(object sender, EventArgs e)
    {

        PanAddUpdate.Visible = true;
        if (chknearmap.Checked == true)
        {
            //Spanmap.Visible = true;
        }
        else
        {
            //Spanmap.Visible = false;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string ssactivedate = txtssdate.Text.Trim();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        bool sucQuote1 = false;

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (ssactivedate != string.Empty)
        {
            sucQuote1 = ClsProjectSale.tblProjects_UpdateQuote_SSActiveDate(ProjectID, ssactivedate);

            if (sucQuote1)
            {
                if (txtssdate.Text != string.Empty)
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("22", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "22");

                }
            }
        }
        BindProjects();
        if (sucQuote1)
        {
            PanSuccess.Visible = true;
        }
        else
        {
            PanError.Visible = true;
        }
        BindData();
        BindProjectQuote();

    }

    protected void lnksendmessage_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        string ProjectNumber = st.ProjectNumber;
        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        if (dtQuote.Rows.Count > 0)
        {
            string Quotepdfno = dtQuote.Rows[0]["ProjectQuoteDoc"].ToString();


            int success = Clstbl_TokenData.tbl_TokenData_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, Quotepdfno, SignFlag);

            if (success > 0)
            {
              

                #region Send Sms
                SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);


                if (st2.ContMobile.Substring(0, 1) == "0")
                {
                    st2.ContMobile = st2.ContMobile.Substring(1);
                }
                Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenData_SelectById(success.ToString());
                st2.ContMobile = "+61" + st2.ContMobile;
                string mobileno = st2.ContMobile;      // "+919979156818";  //contactno;
                string OwnerName = st2.ContFirst + " " + st2.ContLast;
                string messagetext = "Hello " + OwnerName + ",\nPlease open this URL to upload your signature on DocNo: " + sttok.QDocNo + ":\n" + Siteurl + "Signature.aspx?proid=" + ProjectID + "&rt=" + Token + "\n\n Arise Solar.";

                if (mobileno != string.Empty && messagetext != string.Empty)
                {
                    string AccountSid = "AC991f3f103f6a5f608278c1e283f139a1";
                    string AuthToken = "be9c00d8443b360082ab02d3d78969b2";
                    var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);//919879111739  +61451831980
                    var message = twilio.SendMessage("+61418671214", mobileno, messagetext);
                    //var message = twilio.SendMessage("+61418671214", "+61402262170", messagetext);
                    //Response.Write(message.Sid);

                    if (message.RestException == null)
                    {
                        
                        // status = "success";
                        MsgSendSuccess();
                    }
                    else
                    {
                       // string Errormsg=messa
                        // status = "error";
                        SetError1();
                    }

                }

                #endregion


            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }



    }


    protected void rptQuote_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.CommandName.ToString() == "MailAttachedPDf")
        {
            string QuoteId = e.CommandArgument.ToString();

            #region SendMail

            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "Arise Solar Signed Quotation PDF";

            Server.Execute("~/mailtemplate/QuotationAttachedMail.aspx?proid=" + ProjectID, txtWriter);

            SttblProjectQuotes st = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteID(QuoteId);
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st2.ContactID);

            string PDFName = st2.ProjectNumber + "_" + st.QuoteDoc;

            //string filepath = pdfURL + "quotedoc/" + st.QuoteDoc;
            string filepath = SiteConfiguration.GetDocumnetPath("quotedoc", st.QuoteDoc);
            // Utilities.SendMailWithAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName);

            //string filepath = pdfURL + "quotedoc/" + st.QuoteDoc;
            Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath, PDFName);
            MsgSendSuccess();
            try
            {
            }
            catch
            {
            }

            #endregion

        }
    }

    protected void lnksendbrochures_Click(object sender, EventArgs e)
    {
        try
        {
            string ProjectID = Request.QueryString["proid"];

            #region SendMail
            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "AriseSolar Quote";

            Server.Execute("~/mailtemplate/BrochureAttachedMail.aspx?proid=" + ProjectID, txtWriter);

            string[] filepath = new string[10];
            string[] PDFName = new string[10];
            int FileCount = 0;
            int exist = 0;

            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

            if (!string.IsNullOrEmpty(st.PanelBrandID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.PanelBrandID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        exist++;
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.InverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        exist++;
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.SecondInverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.SecondInverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.ThirdInverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }

            int QuoteDocNo = ClstblProjects.tblProjectQuotes_SelectTOPDocNoByProjectID(ProjectID);
            if (QuoteDocNo > 0)
            {
                try
                {
                    exist++;
                    PDFName[FileCount] = st.ProjectNumber + "_Quotation.pdf";
                    //filepath[FileCount] = pdfURL + "quotedoc/" + QuoteDocNo + "Quotation.pdf";
                    filepath[FileCount] = SiteConfiguration.GetDocumnetPath("quotedoc", QuoteDocNo + "Quotation.pdf");
                    FileCount++;
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }

            }

            if (exist == 3)//It sends mail only when one panel,one inverter and quote all 3 pdfs are available.
            {
                if (!string.IsNullOrEmpty(st.SalesRep))
                {
                    SttblEmployees stemploy = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.SalesRep);
                    Utilities.SendMailWithMultipleAttachmentWithReplyList(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName, FileCount, stemploy.EmpEmail, stemploy.EmpFirst + " " + stemploy.EmpLast);
                }
                else
                {
                    Utilities.SendMailWithMultipleAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName, FileCount);
                }
                //Utilities.SendMailWithMultipleAttachmentWithReplyList(from, "kiran.sawant@meghtechnologies.com", Subject, txtWriter.ToString(), filepath, PDFName, FileCount, "kiran.sawant@meghtechnologies.com", "Kiran Sawant");
                MsgSendSuccess();
                //Console.WriteLine("Hello");
            }
            else
            {
                if (QuoteDocNo <= 0)
                {
                    MsgInfo("Please generate Quotation first.");
                }
                else
                {
                    MsgBrochureNotAvaialble();
                }
            }

            #endregion
        }
        catch (Exception ex)
        {
            MsgInfo("Mail is not send.");
        }
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "checkopen();", true);
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void MsgSendSuccess()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunMsgSendSuccess();", true);
    }
    public void MsgBrochureNotAvaialble()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunBrochuresDoesNotExist();", true);
    }

    public void MsgSuccess(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyGreenfun('{0}');", msg), true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void MsgInfo(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyBluefun('{0}');", msg), true);
    }  

    protected void btnCreateNewQoute_Click1(object sender, EventArgs e)
    {
        //string ProjectID = Request.QueryString["proid"];
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //if (st.ProjectTypeID == "8")
        //{
        //    Telerik_reports.generate_Quote(ProjectID, QuoteID);    
        //}
        //else
        //{
        //    Telerik_reports.generate_Quote(ProjectID, QuoteID);
        //}
        //BindProjectQuote();
        //BindScript();
        string QuoteID = string.Empty;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;
        
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string ProjectNumber = st.ProjectNumber;
        //DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectTop1(ProjectID);
        DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectByProjectID(ProjectID);

        DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);

        if (dtTop1.Rows.Count > 0)
        {
        }
        else
        {
            bool sucQoute = ClstblProjects.tblProjects_UpdateQuoteSent(ProjectID);
        }

        BindProjectQuote();

        int success = ClstblProjects.tblProjectQuotes_Insert(ProjectID, ProjectNumber, EmployeeID);
        string QuoteDoc = "Quotation.pdf";

        if (Convert.ToString(success) != "")
        {
            QuoteID = Convert.ToString(success);
            QuoteDoc = Convert.ToString(success) + QuoteDoc;
            bool suc = ClstblProjects.tblProjectQuotes_UpdateProjectQuoteDoc(Convert.ToString(success), QuoteDoc);
        }

        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
        TextWriter txtWriter = new StringWriter() as TextWriter;

        if (st.ProjectTypeID == "8")
        {
            if (stCust.ResCom == "2")
            {
                Server.Execute("~/mailtemplate/mtcecommquote.aspx?id=" + ProjectID, txtWriter);
            }
            else
            {
                Server.Execute("~/mailtemplate/mtcequote.aspx?id=" + ProjectID, txtWriter);
            }
        }
        else
        {
            if (stCust.ResCom == "2")
            {
                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                {
                    Telerik_reports.generate_Quote(ProjectID, QuoteID);
                }
                else
                {
                  //  Telerik_reports.generate_Quote(ProjectID, QuoteID);//Remove Before Uploadd
                    Telerik_reports.generate_QuoteCash(ProjectID, QuoteID);
                }
                //Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);
            }
            else
            {


                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                {
                    Telerik_reports.generate_Quote(ProjectID, QuoteID);
                }
                else 
                {
                   // Telerik_reports.generate_Quote(ProjectID, QuoteID);//Remove Before Uploadd
                    Telerik_reports.generate_QuoteCash(ProjectID, QuoteID);
                }
                // Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);//quatationnew
                //Server.Execute("~/mailtemplate/receipt.aspx?id=" + ProjectID, txtWriter);//quatationdesign
            }
        }

        String htmlText = txtWriter.ToString();

        //HTMLExportToPDF(htmlText, ProjectID + "Quote.pdf");
        // SavePDF(htmlText, QuoteDoc);
        BindQuote();
    }
    public void DeleteFileFromFolder(string StrFilename)
    {
        System.IO.FileInfo image = new System.IO.FileInfo(HttpContext.Current.Server.MapPath("\\userfiles\\" + "NearMap" + "\\") + StrFilename);
        //string url = pdfURL + "NearMap/"+StrFilename;
        string url = SiteConfiguration.GetDocumnetPath("NearMap", StrFilename);
        string physicalPath = Server.MapPath(Path.GetFileName(url));
        string fileName = Path.GetFileName(url);

        string fullPath = Server.MapPath(fileName);

        File.Delete(fullPath);
        //string fullpath = url + StrFilename;
        //FileInfo userphoto = new FileInfo(MapPath(fullpath));
        //if (userphoto.Exists)
        //{
        //    File.Delete(MapPath(fullpath));
        //}
        //string strFileFullPath = Request.PhysicalApplicationPath + "\\userfiles\\NearMap\\" + StrFilename;

        //string strPhysicalFolder = Server.MapPath("..\\");

        //string strFileFullPath = strPhysicalFolder + StrFilename;



    }

    protected void chknearmap_CheckedChanged1(object sender, EventArgs e)
    {
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (chknearmap.Checked)
        {
            //Spanmap.Visible = true;
            //Spanmap1.Visible = true;
            //Spanmap2.Visible = true;
            if ((Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator")))
            {
                //RequiredFieldValidatormap.IsValid = true;
                //RequiredFieldValidator1.IsValid = true;
                //RequiredFieldValidator2.IsValid = true;

                //RequiredFieldValidatormap.Enabled = false;
                //RequiredFieldValidator1.Enabled = false;
                //RequiredFieldValidator2.Enabled = false;

                //RequiredFieldValidatormap.Visible = false;
                //RequiredFieldValidator1.Visible = false;
                //RequiredFieldValidator2.Visible = false;

            }

            else
            {
                //if (st.nearmapdoc != null && st.nearmapdoc != "")
                //{
                //    RequiredFieldValidatormap.Visible = false;
                //}
                //else
                //{
                //    RequiredFieldValidatormap.Visible = true;
                //}
                //if (st.nearmapdoc1 != null && st.nearmapdoc1 != "")
                //{
                //    RequiredFieldValidator1.Visible = false;
                //}
                //else
                //{
                //    RequiredFieldValidator1.Visible = true;
                //}
                //if (st.nearmapdoc2 != null && st.nearmapdoc2 != "")
                //{
                //    RequiredFieldValidator2.Visible = false;
                //}
                //else
                //{
                //    RequiredFieldValidator2.Visible = true;

                //}
                //RequiredFieldValidatormap.Enabled = true;
                //RequiredFieldValidator1.Enabled = true;
                //RequiredFieldValidator2.Enabled = true;
            }
        }
        else
        {
            //Spanmap.Visible = false;
            //Spanmap1.Visible = false;
            //Spanmap2.Visible = false;
        }
    }
    //}

    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        string ProjectNumber = st.ProjectNumber;
        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        if (dtQuote.Rows.Count > 0)
        {
            string Quotepdfno = dtQuote.Rows[0]["ProjectQuoteDoc"].ToString();


            int success = Clstbl_TokenData.tbl_TokenData_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, Quotepdfno, SignFlag);

            if (success > 0)
            {
                #region SendMail

                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                String Subject = "Signature Request - " + ConfigurationManager.AppSettings["SiteName"].ToString();

                Server.Execute("~/mailtemplate/SignatureLinkMail.aspx?proid=" + ProjectID + "&rt=" + Token, txtWriter);

              Utilities.SendMail(from, stContact.ContEmail, Subject, txtWriter.ToString());

                  //Utilities.SendMail(from, "nilesh.rai@meghtechnologies.com", Subject, txtWriter.ToString());

                //string filepath = "http://localhost:57341//userfiles//quotedoctest//6558Quotation.pdf";
                //Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath);
                try
                {
                    MsgSendSuccess();
                }
                catch(Exception ex)
                {
                }

                #endregion

               


            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }


    }
}
