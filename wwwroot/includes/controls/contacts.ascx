<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="contacts.ascx.cs" Inherits="includes_controls_contacts" %>

<%@ Register Src="contactdetail.ascx" TagName="contactdetail" TagPrefix="uc1" %>
<%@ Register Src="contactnote.ascx" TagName="contactnote" TagPrefix="uc2" %>
<%--<%@ Register src="promo.ascx" tagname="promo" tagprefix="uc3" %>--%>
<%@ Register Src="contactinfo.ascx" TagName="contactinfo" TagPrefix="uc4" %>
<%@ Register Src="contactaccount.ascx" TagName="contactaccount" TagPrefix="uc5" %>

<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }
</style>
<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<style>
    .contactsarea {
        position: relative;
    }
</style>

<script>
    function ComfirmDelete(event, ctl) {
        event.preventDefault();
        var defaultAction = $(ctl).prop("href");

        swal({
            title: "Are you sure you want to delete this Record?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true,
            closeOnCancel: true
        },

            function (isConfirm) {
                if (isConfirm) {
                    // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    eval(defaultAction);

                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    return true;
                } else {
                    // swal("Cancelled", "Your imaginary file is safe :)", "error");
                    return false;
                }
            });
    }
</script>

<script>
    $(document).ready(function () {
        //  alert('kjdhk');

        $('#<%=btnAdd.ClientID %>').click(function () {
            formValidate();
        });
        $('#<%=btnUpdate.ClientID %>').click(function () {
            formValidate();
        });

    });
    function doMyAction() {
        $('#<%=btnAdd.ClientID %>').click(function () {
            formValidate();
        });
        $('#<%=btnUpdate.ClientID %>').click(function () {
            formValidate();
        });
        loader();
    }
    function loader() {
        $(window).load(function () {
        });
    }

</script>
<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>

        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedcontacttab);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);
            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');
            }
            function endrequesthandler(sender, args) {
                $('.loading-container').css('display', 'none');
                $(".myvalcont").select2({
                    //placeholder: "select",
                    allowclear: true
                });

            }
            function pageLoadedcontacttab() {


            }
        </script>

        <section class="row m-b-md" id="pancontacts" runat="server">

            <div class="col-sm-12">
                <div class="contactsarea">
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate" Visible="false">

                        <div class="widget flat radius-bordered borderone">
                            <div class="widget-header bordered-bottom bordered-blue">
                                <span class="widget-caption">
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>Contact</span>
                            </div>
                            <div class="widget-body">
                                <div class="lightgraybgarea">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group" id="title" runat="server" visible="false">
                                                    <asp:Label ID="lblFirstName" runat="server" class="col-sm-2 control-label">
                                                <strong>Title</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtContTitle" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                        <br />
                                                    </div>
                                                </div>

                                                <div class="form-group spicaldivin marginbtm15" id="divMrMs" runat="server">
                                                    <asp:Label ID="Label37" runat="server" class="col-sm-12">
                                                 Primary Contact</asp:Label>
                                                    <div class="col-sm-12">
                                                        <div class="row marginbtm15">

                                                            <div class="col-md-4">
                                                                <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                    placeholder="Salutation"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                    placeholder="First Name"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="contact" Style="width: 100%!important;"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                    placeholder="Last Name"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="" Style="width: 100%!important;"
                                                                    ControlToValidate="txtContLast" Display="Dynamic" ValidationGroup="contact"></asp:RequiredFieldValidator>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="form-group spicaldivin " id="div1" runat="server">
                                                    <asp:Label ID="Label1" runat="server" class="col-sm-12">
                                              Secondary Contact</asp:Label>
                                                    <div class="col-sm-12">
                                                        <div class="row marginbtm15">
                                                            <div class="col-md-4">
                                                                <asp:TextBox ID="txtContMr2" runat="server" MaxLength="10" CssClass="form-control"
                                                                    placeholder="Salutation"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:TextBox ID="txtContFirst2" runat="server" MaxLength="30" CssClass="form-control"
                                                                    placeholder="First Name"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:TextBox ID="txtContLast2" runat="server" MaxLength="40" CssClass="form-control"
                                                                    placeholder="Last Name"></asp:TextBox>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" class="col-sm-12">
                                                Mobile</asp:Label>
                                                    <div class="col-sm-12 marginbtm15">
                                                        <asp:TextBox ID="txtContMobile" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtContMobile"
                                                            ForeColor="Red" ValidationGroup="contact" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                        <br />
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <asp:Label ID="Label3" runat="server" class="col-sm-12">
                                                Email</asp:Label>
                                                    <div class="col-sm-12 marginbtm15">
                                                        <asp:TextBox ID="txtContEmail" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContEmail"
                                                            ValidationGroup="contact" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                            ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                        </asp:RegularExpressionValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label4" runat="server" class="col-sm-12">
                                              Notes</asp:Label>
                                                    <div class="col-sm-12 marginbtm15">
                                                        <asp:TextBox ID="txtContNotes" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <div class="col-sm-8 col-sm-offset-5">
                                                    <asp:Button CssClass="btn btn-primary  redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                        Text="Add" ValidationGroup="contact" />
                                                    <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                        Text="Save" Visible="false" />
                                                    <asp:Button CssClass="btn btn-default btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                        CausesValidation="false" Text="Reset" />
                                                    <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                        CausesValidation="false" Text="Cancel" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                    <div class=" paddleftright10" id="PanSearch" runat="server">
                        <%--<div class="leftarea1">
                        <div class="showenteries">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>Show</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" CssClass="myval1">
                                        </asp:DropDownList></td>
                                    <td>entries</td>
                                </tr>
                            </table>
                        </div>
                    </div>--%>
                        <div>
                            <div id="divrightbtn" runat="server" style="text-align: right;">
                                <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" CssClass="btn btn-default purple"
                                    OnClick="lnkAdd_Click"><i class="fa fa-plus"></i>Add</asp:LinkButton>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="messesgarea">
                        <div class="alert alert-success" style="margin-bottom: 20px;" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                    </div>
                    <div class="contactbottomarea finalgrid">
                        <div class="tablescrolldiv">
                            <div class="tableblack tableminpadd">
                                <div class="table-responsive" id="PanGrid" runat="server">
                                    <asp:GridView ID="GridView1" DataKeyNames="ContactID" runat="server" AllowSorting="true" OnSorting="GridView1_Sorting"
                                        OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnPageIndexChanging="GridView1_PageIndexChanging" AutoGenerateColumns="false"
                                        CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndContactID" runat="server" Value='<%#Eval("ContactID") %>' />
                                                    <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerID") %>' />
                                                    <%#Eval("ContFirst")%>
                                                    <%#Eval("ContLast")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Company" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="180px">
                                                <ItemTemplate>
                                                    <%#Eval("Customer")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Street" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px">
                                                <ItemTemplate>
                                                    <%#Eval("StreetAddress")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Suburb" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                <ItemTemplate>
                                                    <%#Eval("StreetCity")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="P/Cd" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                                                <ItemTemplate>
                                                    <%#Eval("StreetPostCode")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Land Line" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                                <ItemTemplate>
                                                    <%#Eval("ContMobile")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn btn-primary btn-xs"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=c&compid="+Eval("CustomerID")+"&contid="+ Eval("ContactID") %>'>
                                           <%-- <img src="<%=SiteURL%>/images/icon_detail.png" />--%> <i class="fa fa-link"></i>Detail </asp:HyperLink>
                                                    
                                                        <asp:HyperLink ID="gvbtnDetail2" runat="server" data-toggle="tooltip" data-placement="top" Visible="false" data-original-title="Detail" CssClass="btn-primary btn btn-xs"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/ECompany.aspx?m=c&compid="+Eval("CustomerID")+"&contid="+Eval("ContactID") %>'>
                                          <%--  <img src="<%=SiteURL%>/images/icon_detail.png" />--%>
                                            <i class="fa fa-link"></i>Detail
                                                    </asp:HyperLink>
                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("ContactID")%>' Visible='<%#(  (Convert.ToString(Eval("ContactID"))==Convert.ToString(Eval("FirstContact"))) || (Convert.ToString(Eval("myfollowup"))=="1"))?false:true %>' data-dismiss="modal" data-backdrop="true"
                                                        CssClass="btn btn-danger btn-xs" CausesValidation="false"  data-original-title="Delete" data-toggle="tooltip" data-placement="top">
                                               <i class="fa fa-trash"></i> Delete</asp:LinkButton>


                                                </ItemTemplate>
                                                <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </section>

        <div class="row nopadding" id="divcontacttab" runat="server" visible="false" style="margin-top: 20px;">
            <div class="col-md-12">
                <div class="tabintab companytab">
                    <div class="panel panel-default">
                        <div class="panel-heading printpage" style="padding-bottom: 12px;">
                            <span style="padding-top: 8px; display: inline-block;">
                                <asp:Literal ID="ltcontactname" runat="server" Text="jigar"></asp:Literal>
                            </span>
                            <div class="panel-tools pull-right">
                                <asp:LinkButton ID="lnkclose" runat="server" CssClass="btn btn-maroon" CausesValidation="false"
                                    OnClick="lnkclose_Click"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="panel-body" style="padding: 15px 15px;">
                            <cc1:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                AutoPostBack="true">
                                <cc1:TabPanel ID="TabSummary" runat="server" HeaderText="Detail">
                                    <ContentTemplate>
                                        <uc1:contactdetail ID="contactdetail2" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <%--  <cc1:TabPanel ID="TabContact" runat="server" HeaderText="Notes" Visible="false">
                                    <ContentTemplate>
                                        
                                        <uc2:contactnote ID="contactnote2" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>--%>
                                <%-- <cc1:TabPanel ID="TabProject" runat="server" HeaderText="Promo">
                            <ContentTemplate>
                               <uc3:promo ID="promo2" runat="server" />
                            </ContentTemplate>
                        </cc1:TabPanel>--%>
                                <cc1:TabPanel ID="TabInfo" runat="server" HeaderText="Info">
                                    <ContentTemplate>
                                        <uc4:contactinfo ID="contactinfo2" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel ID="TabAccount" runat="server" HeaderText="User Account">
                                    <ContentTemplate>
                                        <uc5:contactaccount ID="contactaccount2" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                            </cc1:TabContainer>





                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hdndelete" runat="server" />
        <!--End Danger Modal Templates-->

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="TabContainer2" />
    </Triggers>
</asp:UpdatePanel>


