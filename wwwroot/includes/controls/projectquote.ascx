<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectquote.ascx.cs"
    Inherits="includes_controls_projectquote" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/InvoicePayments.ascx" TagName="InvoicePayments" TagPrefix="uc1" %>
<style type="text/css">
    body {
        font-size: 11px;
    }

    .selected_row {
        background-color: #A1DCF2 !important;
    }
</style>
<script type="text/javascript">
    function HideShow() {
       var RoleName = document.getElementById('<%= hndRole.ClientID%>').value;
        //alert(RoleName);

        var valName6 = document.getElementById("<%=RequiredFieldValidatormap.ClientID%>");
        <%--var valName7 = document.getElementById("<%=RequiredFieldValidator1.ClientID%>");
        var valName8 = document.getElementById("<%=RequiredFieldValidator2.ClientID%>");--%>

        if ($("#<%= chknearmap.ClientID %>").is(':checked') == true) {
            // alert("true");
            $("#Spanmap").attr("style", "display:block");
            //$("#Spanmap1").attr("style", "display:block");
            //$("#Spanmap2").attr("style", "display:block");

            var hndnearmap = document.getElementById('<%= lblnearmap.ClientID%>');
           <%-- var hndnearmap1 = document.getElementById('<%= lblnearmap1.ClientID%>');
            var hndnearmap2 = document.getElementById('<%= lblnearmap2.ClientID%>');--%>

            //alert(hndnearmap +"<br/>"+hndnearmap1);

            //ValidatorEnable(valName, false);
            if (RoleName == "Administrator" || RoleName == "Sales Manager" || RoleName == "DSales Manager" || RoleName == "Installation Manager") {
                //alert("In Condition " + RoleName);

                ValidatorEnable(valName6, false);
                //ValidatorEnable(valName7, false);
                //ValidatorEnable(valName8, false);
            }
            else {
                if (hndnearmap == null) {
                    ValidatorEnable(valName6, true);
                }
                else {
                    ValidatorEnable(valName6, false);
                }
                //if (hndnearmap1 == null) {
                //    ValidatorEnable(valName7, true);
                //}
                //else {
                //    ValidatorEnable(valName7, false);
                //}
                //if (hndnearmap2 == null) {
                //    ValidatorEnable(valName8, true);
                //}
                //else {
                //    ValidatorEnable(valName8, false);
                //}
                
                //ValidatorEnable(valName8, true);
            }
        }
        else {
            $("#Spanmap").attr("style", "display:none");
            $("#Spanmap1").attr("style", "display:none");
            $("#Spanmap2").attr("style", "display:none");
            //ValidatorEnable(valName, false);
            ValidatorEnable(valName6, false);
                //ValidatorEnable(valName7, false);
                //ValidatorEnable(valName8, false);
        }


        
    }
</script>
<script>

    function ShowProgressWithoutValidation() {           
                    $('.modalbackground').css('background-color', 'unset');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    $('.loading-container').css('display', 'block');     
        }

      <%--   $('form').on("click", 'a', function () {
                //String a = ;                
               // alert("c");
                if (<%=IsPostBack.ToString().ToLower()%>) {
                    alert("in2");
                    ShowProgress();
             }        
             else {
                    alert("Not postback");
                    $('.loading-container').css('display', 'none');  
                    //lnksendmessage,lnksendbrochures,btnSendAMail
                }
     });--%>

       $('form').on("click", '.addPreloader', function () {                      
                // alert("c");
                    ShowProgressWithoutValidation();
     });

</script>

<script type="text/javascript">
    function checkopen() {

        var valName = document.getElementById("<%=RequiredFieldValidatorMP.ClientID%>");
        var valName1 = document.getElementById("<%=RequiredFieldValidatorEB.ClientID%>");
        var valName2 = document.getElementById("<%=RequiredFieldValidatorPD.ClientID%>");
        var valName3 = document.getElementById("<%=RequiredFieldValidatorPR.ClientID%>");
        var valName4 = document.getElementById("<%=RequiredFieldValidatorbeat.ClientID%>");
     <%--   var valName5 = document.getElementById("<%=RequiredFieldValidatormap.ClientID%>");--%>



        if ($("#<%= chkMeterBoxPhotosSaved.ClientID %>").is(':checked') == true) {
            // alert("true");
            $("#<%= divMP.ClientID %>").attr("style", "display:block");
            ValidatorEnable(valName, false);

        }
        else {
            // alert("false");
            $("#<%= divMP.ClientID %>").attr("style", "display:none");
            ValidatorEnable(valName, false);
        }
        if ($("#<%= chkElecBillSaved.ClientID %>").is(':checked') == true) {
            $("#divEB").attr("style", "display:block");
            ValidatorEnable(valName1, false);
        }
        else {
            $("#divEB").attr("style", "display:none");
            ValidatorEnable(valName1, false);
        }
        if ($("#<%= chkProposedDesignSaved.ClientID %>").is(':checked') == true) {
            $("#divPD").attr("style", "display:block");
            ValidatorEnable(valName2, false);
        }
        else {
            $("#divPD").attr("style", "display:none");
            ValidatorEnable(valName2, false);
        }
        if ($("#<%= chkPaymentReceipt.ClientID %>").is(':checked') == true) {
            $("#divPR").attr("style", "display:block");
            ValidatorEnable(valName3, false);
        }
        else {
            $("#divPR").attr("style", "display:none");
            ValidatorEnable(valName3, false);
        }
        if ($("#<%= chkbeatquote.ClientID %>").is(':checked') == true) {
            $("#Spanbeat").attr("style", "display:block");
            ValidatorEnable(valName4, false);
        }
        else {
            $("#Spanbeat").attr("style", "display:none");
            ValidatorEnable(valName4, false);
        }
        HideShow();
        <%--if ($("#<%= chknearmap.ClientID %>").is(':checked') == true) {
            //$("#Spanmap").attr("style", "display:block");
           //ValidatorEnable(valName5, false);
        }
        else {
            $("#Spanmap").attr("style", "display:none");
            //ValidatorEnable(valName5, false);
        }--%>
    }
</script>
<script>
    function fileChange() {
        var valName = document.getElementById("<%=RequiredFieldValidatorMP.ClientID%>");
        if ($("#<%= chkMeterBoxPhotosSaved.ClientID %>").is(':checked') == true) {
            $("#<%= divMP.ClientID %>").attr("style", "display:block");
            ValidatorEnable(valName, true);

        }
        else {
            $("#<%= divMP.ClientID %>").attr("style", "display:none");
            ValidatorEnable(valName, false);
        }
    }
    function fileChange1() {
        var valName = document.getElementById("<%=RequiredFieldValidatorEB.ClientID%>");
        if ($("#<%= chkElecBillSaved.ClientID %>").is(':checked') == true) {
            $("#divEB").attr("style", "display:block");
            ValidatorEnable(valName, true);
        }
        else {
            $("#divEB").attr("style", "display:none");
            ValidatorEnable(valName, false);
        }
    }

    function fileChange2() {
        var valName = document.getElementById("<%=RequiredFieldValidatorPD.ClientID%>");
        if ($("#<%= chkProposedDesignSaved.ClientID %>").is(':checked') == true) {
            $("#divPD").attr("style", "display:block");
            ValidatorEnable(valName, true);
        }
        else {
            $("#divPD").attr("style", "display:none");
            ValidatorEnable(valName, false);
        }
    }
    function fileChange3() {
        var valName = document.getElementById("<%=RequiredFieldValidatorPR.ClientID%>");
        if ($("#<%= chkPaymentReceipt.ClientID %>").is(':checked') == true) {
            $("#divPR").attr("style", "display:block");
            ValidatorEnable(valName, true);
        }
        else {
            $("#divPR").attr("style", "display:none");
            ValidatorEnable(valName, false);
        }
    }
    function fileChange4() {
        var valName = document.getElementById("<%=RequiredFieldValidatorbeat.ClientID%>");
        if ($("#<%= chkbeatquote.ClientID %>").is(':checked') == true) {
            $("#Spanbeat").attr("style", "display:block");
            ValidatorEnable(valName, true);
        }
        else {
            $("#Spanbeat").attr("style", "display:none");
            ValidatorEnable(valName, false);
        }
    }
    <%--function fileChange5() {
        var valName = document.getElementById("<%=RequiredFieldValidatormap.ClientID%>");
        if ($("#<%= chknearmap.ClientID %>").is(':checked') == true) {
            $("#Spanmap").attr("style", "display:block");
            ValidatorEnable(valName, true);
        }
        else {
            $("#Spanmap").attr("style", "display:none");
            ValidatorEnable(valName, false);

        }
    }--%>
    //---------------------------------------------Deep Comment
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //checkopen();
        //------------------------------------deep comment end
        // alert("fbdgbgb");
        doMyAction();
        HideShow();
    });

    function doMyAction() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        //  HighlightControlToValidate();
        $('#<%=btnUpdateQuote.ClientID %>').click(function () {
            formValidate();
        });
        // CompareDates();
        // CompareDatesActive();
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    }
    function CompareDates(source, args) {
        var str1 = document.getElementById("<%= txtDepositReceived.ClientID %>").value;
        //var str2 = document.getElementById("txtToDate").value;
        var str2 = document.getElementById("<%= hiddenExpiryDate.ClientID %>").value;
        var dt1 = parseInt(str1.substring(0, 2), 10);
        var mon1 = parseInt(str1.substring(3, 5), 10);
        var yr1 = parseInt(str1.substring(6, 10), 10);
        var dt2 = parseInt(str2.substring(0, 2), 10);
        var mon2 = parseInt(str2.substring(3, 5), 10);
        var yr2 = parseInt(str2.substring(6, 10), 10);
        var date1 = new Date(yr1, mon1, dt1);
        var date2 = new Date(yr2, mon2, dt2);
        var t1 = date1.toString();//this becuase it was not able to compare equal to dates.
        var t2 = date2.toString();
        //alert(str1 + "-----" + dt1 + "-----" + mon1 + "-----"+yr1+"-----" + date1);
        //alert(str2+"-----"+dt2+"-----"+mon2+"-----"+date2);

        if ((date2 < date1) || (d2 == d1)) {
           // alert("Submitting ...1");
            args.c = true;
        } else {
             // alert("To date cannot be greater than from date:1");            
            args.IsValid = false;
            //alert(args.IsValid);
        }
    }
    function CompareDatesActive(source, args) {
        var str1 = document.getElementById("<%= txtActiveDate.ClientID %>").value;
        // var str1 = "06/02/2019";
        //var str2 = document.getElementById("txtToDate").value;
        var str2 = document.getElementById("<%= hdndeprec.ClientID %>").value; //roshni
        //var str2 = "06/02/2019";
        var dt1 = parseInt(str1.substring(0, 2), 10);
        var mon1 = parseInt(str1.substring(3, 5), 10);
        var yr1 = parseInt(str1.substring(6, 10), 10);
        var dt2 = parseInt(str2.substring(0, 2), 10);
        var mon2 = parseInt(str2.substring(3, 5), 10);
        var yr2 = parseInt(str2.substring(6, 10), 10);
        var date1 = new Date(yr1, mon1, dt1);
        var date2 = new Date(yr2, mon2, dt2);
        //alert(str1 + "-----" + dt1 + "-----" + mon1 + "-----"+yr1+"-----" + date1);
        //alert(str2 + "-----" + dt2 + "-----" + mon2 + "-----" + date2);

        var d1 = date1.toString();//this becuase it was not able to compare equal to dates.
        var d2 = date2.toString();

        if ((date2 < date1) || (d2 == d1)) {
            // alert("Submitting ...2");           
            args.IsValid = true;
         }
         else {
             // alert("To date cannot be greater than from date2");
            args.IsValid = false;
         }
    }
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>


<asp:UpdatePanel runat="server" ID="updatepanelgrid">
    <ContentTemplate>

        <asp:HiddenField runat="server" ID ="hndRole"/>
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <div class="quateapage">
                        <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel1" runat="server">
                                        <div>
                                            <div class="widget flat radius-bordered borderone">
                                                <div class="widget-header bordered-bottom bordered-blue">
                                                    <span class="widget-caption">Quote Detail</span>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-6" style="padding-left: 0px;">
                                                            <div>
                                                                <asp:Panel runat="server" ID="PanQuoteSent" Enabled="false">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <span class="name">
                                                                                <asp:Label ID="Label2" runat="server" class="  control-label">
                                                First Quote Sent</asp:Label></span>
                                                                            <div class="input-group date datetimepicker1 ">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="txtQuoteSent" runat="server" class="form-control" Width="125px">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label3" runat="server" class="  control-label">
                                                Quote Accepted</asp:Label></span>
                                                                    <div class="input-group date datetimepicker1 ">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtQuoteAcceptedQuote" runat="server" class="form-control" Width="125px">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group dateimgarea fileuploadmain col-md-6" style="margin-bottom: 0px;">
                                                                <span>
                                                                    <span>
                                                                        <label for="<%=chkSignedQuote.ClientID %>">
                                                                            <asp:CheckBox ID="chkSignedQuote" runat="server" AutoPostBack="true" OnCheckedChanged="chkSignedQuote_CheckedChanged" />
                                                                            <span class="text">Signed Quote Stored</span>
                                                                        </label>

                                                                    </span>
                                                                    <span id="divSQ" runat="server" visible="false">
                                                                        <asp:HyperLink ID="lblSQ" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="dateimg">
                                                                            <span class="file-input btn btn-azure btn-file">

                                                                                <asp:FileUpload ID="fuSQ" runat="server" />
                                                                            </span>
                                                                        </span>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorSQ" runat="server" ErrorMessage="This value is required." ValidationGroup="quote" ControlToValidate="fuSQ" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                    </span>

                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12" style="padding-left: 0px;">
                                                                <div class="form-group col-md-4">
                                                                    <asp:LinkButton ID="lnksendmessage" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false" class="btn btn-labeled btn-success addPreloader" OnClick="lnksendmessage_Click">
                                                                    <i class="glyphicon glyphicon-envelope"></i>  Cust`s Sign Via SMS
                                                                    </asp:LinkButton>
                                                                </div>
                                                                 <div class="form-group col-md-4">
                                                                    <asp:LinkButton ID="lnkEmail" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false" class="btn btn-labeled btn-success addPreloader" OnClick="lnkEmail_Click">
                                                                    <i class="glyphicon glyphicon-envelope"></i>Cust`s Sign Via Email
                                                                    </asp:LinkButton>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <asp:LinkButton ID="lnksendbrochures" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false" class="btn btn-labeled btn-success addPreloader" OnClick="lnksendbrochures_Click">
                                                                    <i class="glyphicon glyphicon-envelope"></i>  Send Quote
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group dateimgarea fileuploadmain col-md-6" runat="server" style="margin-left: -15px;" id="divmtcepaperwork">
                                                                <span><span>
                                                                    <label>
                                                                        Mtce Paper Work
                                                                    </label>
                                                                </span>
                                                                    <span id="Span1" runat="server">
                                                                        <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="FUmtcepaperwork" runat="server" />
                                                                        </span>
                                                                    </span></span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-md-5 col-sm-5" id="divQuotes2" runat="server" visible="false">
                                                                    <h5>Quotes</h5>
                                                                </div>
                                                                <div class="col-md-7 col-sm-7" style="text-align: right;">
                                                                    <%--<asp:ImageButton ID="btnCreateQuote" runat="server" ImageUrl="~/images/btn_create_new_quote.png" OnClick="btnCreateQuote_Click" CausesValidation="false" />--%>
                                                                    <asp:LinkButton ID="btnCreateQuote" Style="width: 163px; display:none;" OnClick="btnCreateQuote_Click" runat="server" CausesValidation="false" class="btn btn-labeled btn-success">
                                                                    <i class="btn-label fa fa-file-pdf-o"></i>Create Quote
                                                                    </asp:LinkButton>                                                                  
                                                                    <asp:LinkButton ID="btnCreateNewQoute" OnClick="btnCreateNewQoute_Click1" runat="server" class="btn btn-labeled btn-success" CausesValidation="false"><i class="btn-label fa fa-file-pdf-o"></i>Quote</asp:LinkButton>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="row finalgrid" id="divQuotes" runat="server" visible="false">
                                                                <div class="col-md-12" runat="server" id="divquo">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 25%; text-align: center">Quote Date
                                                                                </th>
                                                                                <th style="width: 20%; text-align: center">Doc No.
                                                                                </th>
                                                                                <th style="width: 15%; text-align: center">Signed
                                                                                </th>
                                                                                <th style="width: 20%; text-align: center">Document
                                                                                </th>
                                                                                <th style="width: 20%; text-align: center">Email
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <asp:Repeater ID="rptQuote" runat="server" OnItemDataBound="rptQuote_ItemDataBound" OnItemCommand="rptQuote_ItemCommand">
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td style="width: 25%; text-align: center">
                                                                                        <%#Eval("ProjectQuoteDate", "{0:ddd - dd MMM yyyy}")%>
                                                                                    </td>
                                                                                    <td style="width: 20%; text-align: center">
                                                                                        <%#Eval("ProjectQuoteDoc")%>
                                                                                    </td>
                                                                                    <td style="width: 15%; text-align: center;">
                                                                                        <%-- <label for='<%# Container.FindControl("chkSerialNo").ClientID  %>' runat="server" id="lblchk123" >
                                                                                            <asp:CheckBox ID="chkSerialNo" runat="server" />
                                                                                            <span class="text">&nbsp;</span>
                                                                                        </label>--%>
                                                                                        <%--<asp:Label runat="server" ID="lblsignchked" Visible="false"><i class="fa fa-check-square" style="width:20px;height:20px;"></i></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblsignUnChked"><i class="fa fa-square"></i></asp:Label>--%>
                                                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/check.png" Visible="false" />
                                                                                    </td>
                                                                                    <td style="width: 25%; text-align: center;">
                                                                                        <asp:HiddenField ID="hndProjectQuoteID" runat="server" Value='<%#Eval("ProjectQuoteID") %>' />
                                                                                        <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                                            <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/icon_document_downalod.png" />
                                                                                        </asp:HyperLink>
                                                                                    </td>
                                                                                    <td style="width: 20%; text-align: center">
                                                                                        <asp:LinkButton ID="btnSendAMail" runat="server" Visible="false" CommandName="MailAttachedPDf" CommandArgument='<%#Eval("ProjectQuoteID")%>' CssClass="addPreloader" >
                                                                                               <i class="fa fa-envelope-o fa-lg"></i>
                                                                                           <%--<span class="typcn typcn-mail" ></span>--%>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6" id="ssactive" runat="server" visible="false">
                                                            <div class="form-group">
                                                                <span class="name">
                                                                    <asp:Label ID="Label4" runat="server" class="  control-label">
                                                SS Active Date</asp:Label></span>
                                                                <div class="input-group date datetimepicker1 ">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtssdate" runat="server" class="form-control" Width="125px">
                                                                    </asp:TextBox>
                                                                </div>
                                                                <br />
                                                                <asp:Button class="btn btn-primary savewhiteicon" ID="btnSave" runat="server" OnClick="btnSave_Click" CausesValidation="false" Text="Solar Service Active" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="projectquote" runat="server" visible="false">
                                            <div class="widget flat radius-bordered borderone">
                                                <div class="widget-header bordered-bottom bordered-blue">
                                                    <span class="widget-caption">System Detail</span>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group ">
                                                                <span class="name" style="width: 140px;">
                                                                    <label class="control-label">
                                                                        Panel Details
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtPanelDetails" Enabled="false" runat="server" CssClass="form-control"> </asp:TextBox>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group ">
                                                                <span class="name" style="width: 140px;">
                                                                    <label class="control-label">
                                                                        Inverter Details
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtInverterDetails" Enabled="false" runat="server" CssClass="form-control"> </asp:TextBox>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Invoice Detail</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <asp:Panel ID="PanelMain" runat="server">
                                                                <div class="form-group">
                                                                    <span class="dateimg">
                                                                        <span>
                                                                            <%--<asp:Button ID="btnCreateInvoice" runat="server" OnClick="btnCreateInvoice_Click" CssClass="createinvoice martopzero" Text="Create Invoice" CausesValidation="false" />--%>
                                                                            <asp:LinkButton ID="btnCreateInvoice" Style="width: 163px" runat="server" OnClick="btnCreateInvoice_Click" CausesValidation="false" class="btn btn-labeled btn-darkorange">
                                        <i class="btn-label fa fa-file-text"></i>Create Invoice

                                                                            </asp:LinkButton>

                                                                        </span>
                                                                        <span>

                                                                            <asp:ImageButton ID="btnOpenInvoice" runat="server" OnClick="btnOpenInvoice_Click"
                                                                                ImageUrl="../../images/btn_openinvoice.png" CausesValidation="false" />
                                                                         
                                                                        </span>
                                                                    </span>
                                                                    </span>
                                                            <div class="clear">
                                                            </div>
                                                                </div>
                                                            </asp:Panel>

                                                            <asp:Panel ID="Panel31" runat="server" CssClass="padleftzero">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Invoice No
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtInvoiceNumber" runat="server" Enabled="false" CssClass="form-control" Style="width: 163px"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtInvoiceNumber"
                                                                            ValidationGroup="quote" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="divDepRec" runat="server">
                                                                    <div class="form-group">
                                                                        <span class="name">
                                                                            <asp:Label ID="Label5" runat="server" class="  control-label">
                                                                                Deposit Rec</asp:Label></span>

                                                                        <div class="input-group date datetimepicker1 ">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtDepositReceived" runat="server" class="form-control" Width="125px">
                                                                            </asp:TextBox>
                                                                        </div>

                                                                        <asp:RegularExpressionValidator ValidationGroup="quote" ControlToValidate="txtDepositReceived" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date" Display="Dynamic"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        <asp:CustomValidator ClientValidationFunction="CompareDates" ID="cvTodate" runat="server" ErrorMessage="Invalid End Date" ControlToValidate="txtDepositReceived" Display="Dynamic"></asp:CustomValidator>
                                                                        <asp:HiddenField ID="hiddenExpiryDate" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDepRec" runat="server" ErrorMessage="This value is required." CssClass="comperror" Visible="false"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="txtDepositReceived" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </div>

                                                                </asp:Panel>
                                                            </asp:Panel>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-3 editinvoicebox">
                                                        <asp:Panel ID="Panel2" runat="server">
                                                            <div class="form-group" style="margin-bottom: 10px; height: 39px;">
                                                                <span class="dateimg" style="width: 163px">
                                                                    <uc1:InvoicePayments ID="InvoicePayments1" runat="server" />
                                                                </span>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="Panel32" runat="server">
                                                            <div class="form-group">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Invoice Doc
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtInvoiceDoc" MaxLength="10" Enabled="false" runat="server" Style="width: 163px" CssClass="form-control"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                                        ValidationGroup="quote" ControlToValidate="txtInvoiceDoc" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <asp:Panel ID="PanActiveDate" runat="server">

                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label6" runat="server" class="  control-label">
                                                Active Date</asp:Label></span>
                                                                    <div class="input-group date datetimepicker1 ">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtActiveDate" runat="server" class="form-control" Width="125px">
                                                                        </asp:TextBox>
                                                                        <asp:CustomValidator ClientValidationFunction="CompareDatesActive" ID="CustomValidator1" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Invalid End Date" ControlToValidate="txtActiveDate"></asp:CustomValidator>
                                                                        <asp:HiddenField ID="hdndeprec" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorAD" runat="server" ErrorMessage="*" Visible="false"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="txtActiveDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>

                                                            </asp:Panel>
                                                        </asp:Panel>
                                                    </div>



                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <asp:Panel ID="Panel4" runat="server">
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-bottom: 11px !important;">
                                                                        <%-- <asp:ImageButton ID="btnCheckActive" runat="server" ImageUrl="~/images/new/btn_check_active.png" Visible="false"
                                                                        CausesValidation="false" OnClick="btnCheckActive_Click" />--%>
                                                                        <asp:ImageButton ID="imgbtnPerforma" runat="server" OnClick="imgbtnPerforma_Click"
                                                                            ImageUrl="~/images/proformainvoice.png" CausesValidation="false" />
                                                                        <%--<asp:LinkButton ID="imgbtnPerforma" style="width:140px" runat="server"  OnClick="imgbtnPerforma_Click" CausesValidation="false"  class="btn btn-labeled btn-sky">
                                        <i class="btn-label fa fa-file-o"></i>Performa Invoice
                                                                    </asp:LinkButton>--%>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div style="height: 49px;">
                                                                        <%--<asp:ImageButton ID="btnWelcomeDocs" runat="server" ImageUrl="~/images/btn_welcomes.png"
                                                                            CausesValidation="false" OnClick="btnWelcomeDocs_Click" />--%>

                                                                        <asp:LinkButton ID="btnWelcomeDocs" Style="width: 163px" runat="server" OnClick="btnWelcomeDocs_Click" CausesValidation="false" class="btn btn-labeled btn-primary">
                                        <i class="btn-label fa fa-info"></i>Welcome Letter
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="row">
                                                            <asp:Panel runat="server" ID="Panel33">
                                                                <%--<div class="form-group dateimgarea">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Invoice Sent
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtInvoiceSent" runat="server" Width="125px" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                <asp:ImageButton ID="Image13" runat="server" Enabled="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                <cc1:CalendarExtender ID="CalendarExtender13" runat="server" PopupButtonID="Image13"
                                                                    TargetControlID="txtInvoiceSent" Format="dd/MM/yyyy">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ValidationGroup="quote" ControlToValidate="txtInvoiceSent" ID="RegularExpressionValidator3" runat="server" ErrorMessage="Enter valid date"
                                                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>--%>
                                                                <%--InvoiceSent--%>
                                                                <div class="form-group col-md-12">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label1" runat="server" class="  control-label">
                                                Invoice Sent</asp:Label></span>
                                                                    <div class="input-group date datetimepicker1">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtInvoiceSent" runat="server" class="form-control" Width="125px">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Total Paid Amount:
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 130px;">
                                                                        <b>
                                                                            <asp:Label ID="lblTotalPaidAmount" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <span class="name disblock">
                                                                        <label class="control-label" style="width: 80px">
                                                                            Payment Status:
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 130px;">
                                                                        <b>
                                                                            <asp:Label ID="lblpaymentstatus" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4" id="divrecept" runat="server" visible="false">
                                                                    <span class="name disblock">
                                                                        <label class="control-label" style="width: 50px">
                                                                            Receipt No:
                                                                        </label>
                                                                    </span><span class="amtvaluebox">
                                                                        <b>
                                                                            <asp:Label ID="lblreceipt" runat="server" Style="width: 130px;"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--<div class="form-group">
                                                            <div class="row">
                                                                
                                                                <div class="col-md-3">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Total Paid Amount:
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 100px;">
                                                                        <b>
                                                                            <asp:Label ID="lblTotalPaidAmount" Width="100px" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Payment Status:
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 100px;">
                                                                        <b>
                                                                            <asp:Label ID="lblpaymentstatus" Width="100px" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3" id="divrecept" runat="server" visible="false">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Receipt No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 100px;">
                                                                        <b>
                                                                            <asp:Label ID="lblreceipt" Width="100px" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>--%>
                                                    </div>




                                                </div>
                                                <div class="form_group">
                                                    <div style="width: 100%; display: inline-block; margin-top: 28px" runat="server" enabled="false">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=chkactive.ClientID %>" class="control-label">
                                                                    <asp:CheckBox ID="chkactive" runat="server" />
                                                                    <span class="text">Ready to Active   </span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Panel ID="Panel5" runat="server">
                                        <div>
                                            <div class="widget flat radius-bordered borderone">
                                                <div class="widget-header bordered-bottom bordered-blue">
                                                    <span class="widget-caption">Document Upload Detail</span>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkMeterBoxPhotosSaved.ClientID %>">
                                                                            <asp:CheckBox ID="chkMeterBoxPhotosSaved" runat="server" onclick="fileChange()" />
                                                                            <span class="text">Meter Photos Saved  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span runat="server" class="floatleft" id="divMP" style="display: none">
                                                                        <asp:HyperLink ID="lblMP" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuMP" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorMP" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuMP" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkElecBillSaved.ClientID %>">
                                                                            <asp:CheckBox ID="chkElecBillSaved" runat="server" onclick="fileChange1()" />
                                                                            <span class="text">Electricity Bill Saved  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="divEB" style="display: none">
                                                                        <asp:HyperLink ID="lblEB" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuEB" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEB" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuEB" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkProposedDesignSaved.ClientID %>">
                                                                            <asp:CheckBox ID="chkProposedDesignSaved" runat="server" onclick="fileChange2()" />
                                                                            <span class="text">Prop Design Saved  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="divPD" style="display: none">
                                                                        <asp:HyperLink ID="lblPD" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuPD" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPD" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuPD" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkPaymentReceipt.ClientID %>">
                                                                            <asp:CheckBox ID="chkPaymentReceipt" runat="server" onclick="fileChange3()" />
                                                                            <span class="text">Payment Receipt  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="divPR" style="display: none">
                                                                        <asp:HyperLink ID="lblPR" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuPR" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPR" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuPR" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkbeatquote.ClientID %>">
                                                                            <asp:CheckBox ID="chkbeatquote" runat="server" onclick="fileChange4()" />
                                                                            <span class="text">Beat Quote  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="Spanbeat" style="display: none">
                                                                        <asp:HyperLink ID="lblbeat" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fubeat" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorbeat" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fubeat" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chknearmap.ClientID %>">
                                                                            <asp:CheckBox ID="chknearmap" runat="server" onclick="HideShow()" />
                                                                            <%--<asp:CheckBox ID="CheckBox1" runat="server" oncheckedchanged="chknearmap_CheckedChanged1" />--%>
                                                                            <span class="text">NearMap Photo  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="Spanmap" style="display: none">
                                                                    <%--<span id="Span2" visible="false" runat="server">--%>
                                                                        <asp:HiddenField runat="server" ID="hndnearmap" />
                                                                        <asp:HyperLink ID="lblnearmap" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="funearmap" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatormap" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="funearmap" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <asp:HyperLink ID="lblnearmap1" Visible="false" runat="server" Target="_blank"></asp:HyperLink>
                                                                    <asp:HyperLink ID="lblnearmap2" Visible="false" runat="server" Target="_blank"></asp:HyperLink>
                                                                    <asp:HyperLink ID="lblnearmap3" Visible="false" runat="server" Target="_blank"></asp:HyperLink>
                                                                  <%--  <span id="Spanmap1" style="display: none">
                                                                        
                                                                        <asp:HiddenField runat="server" ID="hndnearmap1" />
                                                                        <asp:HyperLink ID="lblnearmap1" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="funearmap1" runat="server"/>
                                                                        </span>
                                                                        <asp:Label ID="lbl2" runat="server"  ForeColor="Red" Visible="false"></asp:Label>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote"  SetFocusOnError="true"  ControlToValidate="funearmap1" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>--%>
                                                                  <%--  <span id="Spanmap2" style="display: none"> 
                                                                       
                                                                        <asp:HiddenField runat="server" ID="hndnearmap2" />
                                                                        <asp:HyperLink ID="lblnearmap2" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="funearmap2" runat="server"/>
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="funearmap2" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                         <asp:Label ID="lbl3" runat="server"  ForeColor="Red" Visible="false"></asp:Label>
                                                                    </span>--%>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                            &nbsp; 
                                        </div>
                                        <div class="row" id="divAddUpdate" runat="server">
                                            <div class="col-md-12 textcenterbutton center-text">
                                                <div>
                                                    <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateQuote" runat="server" OnClick="btnUpdateQuote_Click" ValidationGroup="quote" Text="Save" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </section>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpdateQuote" />
        <asp:PostBackTrigger ControlID="btnOpenInvoice" />
        <asp:PostBackTrigger ControlID="imgbtnPerforma" />
        <asp:PostBackTrigger ControlID="btnCreateQuote" />
        <asp:PostBackTrigger ControlID="btnWelcomeDocs" />
        <asp:PostBackTrigger ControlID="InvoicePayments1" />
        <asp:PostBackTrigger ControlID="lnksendmessage" />
        <asp:PostBackTrigger ControlID="lnksendbrochures" />
        <asp:PostBackTrigger ControlID="btnCreateNewQoute" />      
        <%--<asp:PostBackTrigger ControlID="btnSendAMail" />--%>
    </Triggers>
</asp:UpdatePanel>

