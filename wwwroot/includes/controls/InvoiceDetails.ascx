﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InvoiceDetails.ascx.cs" Inherits="includes_controls_InvoiceDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class="widget flat radius-bordered borderone">
   
    <div class="widget-body">
        <asp:Panel runat="server" ID="PanInvoiceDetails">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Total Cost </label>
                        </span><span>
                            <asp:TextBox ID="txtTotalCostInvoiceInstall" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                        </span>
                        <div class="clear"></div>
                    </div>

                </div>

                <div class="col-md-3">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Total Paid </label>
                        </span><span>
                            <asp:TextBox ID="txtPaidDateInstalled" runat="server" Enabled="false" Text="0" CssClass="form-control"></asp:TextBox>
                        </span>
                        <div class="clear"></div>
                    </div>

                </div>

                <div class="col-md-3">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Bal Owing </label>
                        </span><span>
                            <asp:TextBox ID="txtBalOwingInstall" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                        </span>
                        <div class="clear"></div>
                    </div>

                </div>

                <div class="col-md-3">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Payment Status </label>
                        </span><span class="amtvaluebox">
                            <b>
                                <asp:Label ID="lblPaymentStatusInstall" runat="server"></asp:Label></b>
                        </span>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <span class="name">
                            <label class="control-label">Pay Date </label>
                        </span>
                        <div class="input-group date datetimepicker1 ">
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                            <asp:TextBox ID="txtInvoicePayDateInstall" runat="server" class="form-control">
                            </asp:TextBox>
                            <cc1:filteredtextboxextender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayDateInstall" runat="server"
                                ValidChars="/">
                            </cc1:filteredtextboxextender>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Total Paid </label>
                        </span><span><%--AutoPostBack="true" OnTextChanged="txtInvoicePayTotal_TextChanged"--%>
                            <asp:TextBox ID="txtInvoicePayTotalInstall" runat="server" class="form-control modaltextbox"
                                onchange="InvoicePayTotalChange(this.value)"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtInvoicePayTotal"
                                Display="Dynamic" ErrorMessage="Please enter a number" ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="InvDetailsInstall"></asp:RegularExpressionValidator>
                            <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayTotalInstall" runat="server"
                                validchars=".,-">
                                                                                                                    </cc1:FilteredTextBoxExtender>

                            <asp:TextBox ID="TextBox2" runat="server" class="form-control modaltextbox dnone"></asp:TextBox>
                        </span>
                        <div class="clear"></div>
                    </div>

                </div>

                <div class="col-md-1">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">GST </label>
                        </span><span>
                            <asp:TextBox ID="txtInvoicePayGSTInstall" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                ControlToValidate="txtInvoicePayGSTInstall" Display="Dynamic" ErrorMessage="Please enter a number"
                                ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="InvDetails"></asp:RegularExpressionValidator>
                            <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayGSTInstall" runat="server"
                                ValidChars=".">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                        </span>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="col-md-1">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Pay By </label>
                        </span><span><%--OnSelectedIndexChanged="ddlInvoicePayMethodID_SelectedIndexChanged" AutoPostBack="true" --%>
                            <asp:DropDownList ID="ddlInvoicePayMethodIDInstall" Width="100px" CssClass="myval"
                                runat="server" AppendDataBoundItems="true" onchange="InvoicePayMethodIDChange(this.value)">
                                <asp:ListItem Value="">Select</asp:ListItem>
                            </asp:DropDownList>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPayBy" runat="server" ErrorMessage="" CssClass=""
                                ControlToValidate="ddlInvoicePayMethodIDInstall" Display="Dynamic" ValidationGroup="InvDetails" InitialValue=""></asp:RequiredFieldValidator>
                            <div style="height: 5px"></div>
                        </span>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="col-md-2" id="DivReceiptNoInstall" runat="server" style="display: none">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Receipt No. </label>
                        </span><span>
                            <asp:TextBox ID="txtReceiptNumberInstall" runat="server" class="form-control modaltextbox"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender id="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtReceiptNumberInstall"
                                WatermarkText="Receipt No" />

                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                                                                            ControlToValidate="txtReceiptNumber" ValidationGroup="InvDetails" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        </span>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="col-md-1">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">S/Chg </label>
                        </span><span>
                            <asp:TextBox ID="txtCCSurchargeInstall" runat="server" class="form-control modaltextbox"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtCCSurchargeInstall"
                                Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$" ValidationGroup="InvDetails"></asp:RegularExpressionValidator>
                            <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtCCSurchargeInstall" runat="server"
                                ValidChars=".">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                        </span>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group ">
                        <span class="name">
                            <label class="control-label">Rec By </label>
                        </span><span>
                            <asp:DropDownList ID="ddlRecByInstall" runat="server" AppendDataBoundItems="true" Width="120px" CssClass="myval"
                                Enabled="false">
                                <asp:ListItem Value="">Select</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="col-md-1" style="margin-top: 23px!important;">
                    <div class="form-group ">
                        <asp:HiddenField ID="hndModeInstall" runat="server" Value="" />
                        <asp:HiddenField ID="hndInvoicePaymentIDInstall" runat="server" />
                        <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                            Text="Save" ID="btnInvoiceSaveInstall" ValidationGroup="InvDetails" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="row finalgrid" id="divPaymentDetailsInstall" runat="server" visible="false">
                        <div class="col-md-12" runat="server" id="divPaymentInstall">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                <thead>
                                    <tr>
                                        <th style="width: 15%; text-align: center">Pay Date</th>
                                        <th style="width: 15%; text-align: center">Total Pay</th>
                                        <th style="width: 10%; text-align: center">GST</th>
                                        <th style="width: 15%; text-align: center">Pay By</th>
                                        <th style="text-align: center">Receipt No</th>
                                        <th style="width: 10%; text-align: center">S/Chg</th>
                                        <th style="width: 15%; text-align: center">Rec By</th>
                                        <th style="width: 15%; text-align: center">Verified By</th>
                                        <th style="width: 25%; text-align: center">Action</th>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="rptPaymentDetailsInstall" runat="server" OnItemCommand="rptPaymentDetails_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 15%; text-align: center">
                                                <%# Eval("InvoicePayDate", "{0:dd-MMM-yyyy}") %>
                                            </td>

                                            <td style="width: 15%; text-align: center;">
                                                <%# Eval("InvoicePayTotal", "{0:0.00}") %>
                                            </td>

                                            <td style="width: 10%; text-align: center;">
                                                <%# Eval("InvoicePayGST", "{0:0.00}") %>
                                            </td>

                                            <td style="width: 15%; text-align: center;">
                                                <%# Eval("InvoicePayMethod") %>
                                            </td>

                                            <td style="text-align: center;">
                                                <%# Eval("ReceiptNumber") %>
                                            </td>

                                            <td style="width: 10%; text-align: center;">
                                                <%# Eval("CCSurcharge", "{0:0.00}") %>
                                            </td>

                                            <td style="width: 15%; text-align: center;">
                                                <%# Eval("RecBy", "{0:0.00}") %>
                                            </td>

                                            <td style="width: 15%; text-align: center;">
                                                <%# Eval("VerifiedByName", "{0:0.00}") %>
                                            </td>

                                            <td style="width: 25%; text-align: center;">
                                                <asp:LinkButton ID="btnEdit" runat="server" Visible='<%#Eval("VerifiedByName").ToString()==""?true:false%>' CommandName="Edit" CssClass="btn-primary btn btn-xs" CausesValidation="false"
                                                    CommandArgument='<%#Eval("InvoicePaymentID")%>'>
                                                                                                                                    <i class="fa fa-pen"></i> Edit
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbtnDelete" runat="server" Visible='<%#Eval("VerifiedByName").ToString()==""?true:false%>' CommandName="Delete" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                    CommandArgument='<%#Eval("InvoicePaymentID")%>'>
                                                                                                                                    <i class="fa fa-trash"></i> Delete
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</div>
