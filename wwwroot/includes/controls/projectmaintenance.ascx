﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectmaintenance.ascx.cs" Inherits="includes_controls_projectmaintenance" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/InvoicePayments.ascx" TagName="InvoicePayments" TagPrefix="uc1" %>
<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>

        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedMTCE);
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);
            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                //$('.loading-container').css('display', 'block');
            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
                // $('.loading-container').css('display', 'none');
            }
            function pageLoadedMTCE() {
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                //  $('.loading-container').css('display', 'none');
                $(".myvalMTCE").select2({
                    //placeholder: "select",
                    allowclear: true
                });
                //$('.i-checks').iCheck({
                //    checkboxClass: 'icheckbox_square-green',
                //    radioClass: 'iradio_square-green'
                //});
            }
        </script>
        <script>
            function ComfirmDelete(event, ctl) {
                event.preventDefault();
                var defaultAction = $(ctl).prop("href");
                swal({
                    title: "Are you sure you want to delete this Record?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                  function (isConfirm) {
                      if (isConfirm) {
                          // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                          eval(defaultAction);
                          //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                          return true;
                      } else {
                          // swal("Cancelled", "Your imaginary file is safe :)", "error");
                          return false;
                      }
                  });
            }
        </script>
        <script type="text/javascript">
    $(document).ready(function () {
        //HighlightControlToValidate();

    });

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadeddetail);
    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
    prm.add_beginRequest(beginrequesthandler);
    // raised after an asynchronous postback is finished and control has been returned to the browser.
    prm.add_endRequest(endrequesthandler);

    function beginrequesthandler(sender, args) {
        //shows the modal popup - the update progress

    }
    function endrequesthandler(sender, args) {
        //hide the modal popup - the update progress

        //if (args.get_error() != undefined) {
        //    args.set_errorhandled(true);
        //}

    }
    function pageLoadeddetail() {
        $('#<%=ibtnUpdateDetail.ClientID %>').click(function () {
            formValidate();
        });


        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
           
    }

    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "red");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea">
                <div class="contactsarea">
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel-body">
                                    <div class="paddall">
                                        <div class="row">
                                            <asp:Panel ID="Panel1" runat="server" CssClass="PanAddUpdate">
                                                <div class="widget flat radius-bordered borderone">
                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                        <span class="widget-caption">Maintenance Call Details</span>
                                                    </div>
                                                    <div class="widget-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group wdth230">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label23" runat="server" class="control-label">
                                                                        Call Date</asp:Label></span>
                                                                    <div class="input-group date datetimepicker1">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtOpenDate" runat="server" class="form-control">
                                                                        </asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage=""
                                                            ControlToValidate="txtOpenDate" Display="Dynamic" ValidationGroup="req"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label19" runat="server" class="name disblock control-label">
                                                Call Reason:</asp:Label></span>
                                                                    <div class="firstselect row">
                                                                        <div class="col-md-6">
                                                                            <div class="wdth230">
                                                                                <asp:DropDownList ID="ddlProjectMtceReasonID" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myvalMTCE">
                                                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="wdth230">
                                                                                <asp:DropDownList ID="ddlProjectMtceReasonSubID" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myvalMTCE">
                                                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlProjectMtceReasonSubID" Display="Dynamic" ValidationGroup="req"></asp:RequiredFieldValidator>
                                                                            </div>
                                                                        </div>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="* Required" CssClass="reqerror"
                                                            ControlToValidate="ddlLocation" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group spicalcontact col-md-6">
                                                                        <span class="name">
                                                                            <label class="control-label">Customer: </label>
                                                                        </span><span class="paddtop5 valuebox wdth230">
                                                                            <asp:Label ID="lblCustomer" Width="200px" runat="server"></asp:Label>
                                                                        </span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                    <div class="form-group spicalcontact col-md-6">
                                                                        <span class="name">
                                                                            <label class="control-label">Project : </label>
                                                                        </span><span class="paddtop5 valuebox wdth230">
                                                                            <asp:Label ID="lblProject" runat="server"></asp:Label>
                                                                        </span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group spicalcontact wdth230">
                                                                    <span class="name">
                                                                        <label class="control-label">New</label>
                                                                    </span><span>
                                                                        <asp:TextBox runat="server" ID="txtCurrentPhoneContact" CssClass="form-control" TextMode="MultiLine" Height="80px" Width="950"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group dateimgarea checkboxmain3">
                                                                    <span class="name paddtop9none">
                                                                        <label for="<%=chkWarranty.ClientID %>">
                                                                            <asp:CheckBox ID="chkWarranty" runat="server" />
                                                                            <span class="text">&nbsp;Warranty</span>
                                                                        </label>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <br />
                                                                <div class="form-group wdth230" style="padding-top: 12px;">
                                                                    <span class="name">
                                                                        <label class="control-label">Employee </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlEmployee" runat="server" Width="200px" AppendDataBoundItems="true"
                                                                            CssClass="myvalMTCE">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <span class="name">
                                                                            <label class="control-label">Contact : </label>
                                                                        </span><span class="paddtop5 valuebox wdth230">
                                                                            <asp:Label ID="lblContact" runat="server"></asp:Label>
                                                                        </span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                    <div class="form-group spicalcontact col-md-6">
                                                                        <span class="name">
                                                                            <label class="control-label">Mobile </label>
                                                                        </span><span class="paddtop5 valuebox wdth230">
                                                                            <asp:Label ID="lblMobile" runat="server"></asp:Label>
                                                                        </span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget flat radius-bordered borderone">
                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                        <span class="widget-caption">Installation Details</span>
                                                    </div>
                                                    <div class="widget-body">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Install Date </label>
                                                                    </span><span class="paddtop9 valuebox wdth230">
                                                                        <asp:Label ID="lblInstallDate" runat="server"></asp:Label>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">System </label>
                                                                    </span><span class="paddtop9 valuebox wdth230">
                                                                        <asp:Label ID="lblSystem" runat="server"></asp:Label>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Sales Rep </label>
                                                                    </span><span class="paddtop9 valuebox wdth230">
                                                                        <asp:Label ID="lblSalesRep" runat="server"></asp:Label>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">&nbsp;
                                                                    </span><span class="paddtop9">&nbsp;
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Installer </label>
                                                                    </span><span class="paddtop9 valuebox wdth230">
                                                                        <asp:Label ID="lblInstaller" runat="server"></asp:Label>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Meter Elec </label>
                                                                    </span><span class="paddtop9 valuebox wdth230">
                                                                        <asp:Label ID="lblMeterElec" runat="server"></asp:Label>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget flat radius-bordered borderone">
                                                    <div class="widget-body">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group wdth230">
                                                                    <span class="name">
                                                                        <label class="control-label">Tech Assigned to this Job </label>
                                                                    </span>
                                                                    <asp:DropDownList ID="ddlInstallerAssigned" runat="server" CssClass="myvalMTCE"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select </asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group wdth230">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label2" runat="server" class="name disblock  control-label">
                                                Prop Fix Date</asp:Label></span>
                                                                    <div class="input-group date datetimepicker1">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtPropResolutionDate" runat="server" class="form-control">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group" style="width: 245px">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">Mtce Cost </label>
                                                                    </span><span class="dollarsign">
                                                                        <asp:TextBox ID="txtMtceCost" runat="server" MaxLength="20" CssClass="form-control floatleft wdth230" Width=""></asp:TextBox>
                                                                        <span class="doller">$</span>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                                            ControlToValidate="txtMtceCost" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group" style="width: 245px">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">Service Cost </label>
                                                                    </span><span class="dollarsign">
                                                                        <asp:TextBox ID="txtServiceCost" runat="server" MaxLength="20" CssClass="form-control floatleft" Width="230"></asp:TextBox>
                                                                        <span class="doller">$</span>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                                            ControlToValidate="txtServiceCost" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group wdth230">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label1" runat="server" class="name disblock  control-label">
                                               Fixed</asp:Label></span>
                                                                    <div class="input-group date datetimepicker1">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtCompletionDate" runat="server" class="form-control">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group marginspicleftright" style="width: 245px">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">Discount </label>
                                                                    </span><span class="dollarsign">
                                                                        <asp:TextBox ID="txtMtceDiscount" runat="server" AutoPostBack="true" CssClass="form-control floatleft" MaxLength="15" OnTextChanged="txtMtceDiscount_TextChanged" Width="230"></asp:TextBox>
                                                                        <span class="doller">$</span>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMtceDiscount"
                                                                            Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group" style="width: 245px">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">Balance </label>
                                                                    </span><span class="dollarsign">
                                                                        <asp:TextBox ID="txtMtceBalance" runat="server" MaxLength="15" Enabled="false" CssClass="form-control floatleft wdth230"></asp:TextBox>
                                                                        <span class="doller">$</span>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <div class="form-group" style="margin-bottom: 10px; height: 39px;">
                                                                            <span class="dateimg">
                                                                                <uc1:InvoicePayments ID="InvoicePayments1" runat="server" />
                                                                            </span>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="col-md-4">
                                                    <div style="text-align: center;">
                                                        <asp:Button class="btn btn-primary addwhiteicon redreq btnaddicon" ID="btnAddPanel" runat="server" OnClick="btnAddPanel_Click"
                                                            Text="Add Panel" CausesValidation="true" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div style="text-align: center;">
                                                        <asp:Button class="btn btn-primary addwhiteicon redreq btnaddicon" ID="btnAddInverter" runat="server" OnClick="btnAddInverter_Click"
                                                            Text="Add Inverter" CausesValidation="true" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group center-text">
                                            <span class="name"></span><span class="dateimg">
                                                <asp:Button ID="ibtnUpdateDetail" runat="server" Text="save" OnClick="ibtnUpdateDetail_Click"
                                                    CssClass="btn btn-primary btnsaveicon" ValidationGroup="req"/>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>
        <%-- Add Panel--%>
        <cc1:ModalPopupExtender ID="ModalPopupExtenderAddPanel" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelNotes" DropShadow="false" PopupControlID="divAddPanel" TargetControlID="btnNULLNotes">
        </cc1:ModalPopupExtender>
        <div id="divAddPanel" runat="server" style="display: none" class="modal_popup">
            <div class="modal-dialog modal-lg" style="width: 1100px">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                         <div style="float: right">
                        <button id="ibtnCancelNotes" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                        <h3 class="modal-title" id="myModalLabel">
                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                            Add Panel</h3>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div id="divPanelDetail" runat="server">
                                <div class="widget flat radius-bordered borderone">
                                    <div class="widget-header bordered-bottom bordered-blue">
                                        <span class="widget-caption">Select Panels</span>
                                    </div>
                                    <div class="widget-body">
                                        <asp:Panel runat="server" ID="Pan1">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group selpen">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Select Panel
                                                            </label>
                                                        </span><span>
                                                            <asp:Label ID="labl" runat="server"></asp:Label>
                                                            <asp:DropDownList ID="ddlPanel" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPanel_SelectedIndexChanged" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                No. of Panels
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtNoOfPanels" runat="server" MaxLength="10" CssClass="form-control"
                                                                AutoPostBack="true" OnTextChanged="txtNoOfPanels_TextChanged"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                                ControlToValidate="txtNoOfPanels" Display="Dynamic" ErrorMessage="Number Only"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                System Capacity
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtSystemCapacity" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Brand 
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtPanelBrand" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Watts/Panel 
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtWatts" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Model
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtPanelModel" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Rebate
                                                            </label>
                                                            <br />
                                                        </span><span class="pricespan">
                                                            <asp:TextBox ID="txtRebate" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRebate"
                                                                Display="Dynamic" ErrorMessage="Number Only"
                                                                ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                STC Mult
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtSTCMult" runat="server" Text="1" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Zone Rt
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtZoneRt" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                    <asp:Button ID="ibtnEditPanel" runat="server" Text="Update" OnClick="ibtnEditPanel_Click"
                                        CssClass="btn btn-primary savewhiteicon btnsaveicon" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnNULLNotes" Style="display: none;" runat="server" />
        <asp:Button ID="Button2" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderAddInverter" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divAddInverter" CancelControlID="ibtnCancel"
            TargetControlID="Button2">
        </cc1:ModalPopupExtender>
        <div id="divAddInverter" runat="server" class="modal_popup" style="display: none; width: auto">
            <div class="modal-dialog modal-lg" style="width: 1100px">
                <div class="modal-content">
                    <div class="modal-header">
                         <div style="float: right">
                        <button id="ibtnCancel" runat="server" onclick="btnCancle_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close </button>
                             </div>
                        <h4 class="modal-title" id="H4">Add Inverter</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div id="divInverterDetail" runat="server">
                                <div class="widget flat radius-bordered borderone">
                                    <div class="widget-header bordered-bottom bordered-blue">
                                        <span class="widget-caption">Select Inverter</span>
                                    </div>
                                    <div class="widget-body">
                                        <asp:Panel runat="server" ID="Pan2">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group selpen row">
                                                        <div class="col-md-9">
                                                            <span class="name disblock ">
                                                                <label class="control-label">
                                                                    Select Inverter
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlInverter" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    OnSelectedIndexChanged="ddlInverter_SelectedIndexChanged" AutoPostBack="true"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Qty1
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtqty" runat="server" MaxLength="50" CssClass="form-control alignright" Width="50"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Brand
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtInverterBrand" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Series
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtSeries" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group selpen row">
                                                        <div class="col-md-9">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    2nd Inverter
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlInverter2" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    OnSelectedIndexChanged="ddlInverter2_SelectedIndexChanged" AutoPostBack="true"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Qty2
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtqty2" runat="server" MaxLength="50" CssClass="form-control alignright" Width="50"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <div class="row">
                                                            <div class="col-md-3 ">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        STC
                                                                    </label>
                                                                </span>
                                                                <span class="spicalspanwith">
                                                                    <div class="first1">
                                                                        <asp:TextBox ID="txtSTCNo" runat="server" Enabled="false" MaxLength="50" CssClass="form-control"
                                                                            Width="50"></asp:TextBox>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="second1">
                                                                    <label class="control-label">
                                                                        Value
                                                                    </label>
                                                                </div>
                                                                <div class="thrid1">
                                                                    <asp:TextBox ID="txtSTCValue" runat="server" Enabled="false" MaxLength="50" CssClass="form-control alignright floatleft dolarsingn" Width="80"></asp:TextBox>
                                                                </div>
                                                                <div class="four1">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Inverter Output
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtInverterOutput" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Model
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtInverterModel" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group selpen row">
                                                        <div class="col-md-9">
                                                            <span class="name disblock ">
                                                                <label class="control-label">
                                                                    3rd Inverter
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlInverter3" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Qty3
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtqty3" runat="server" MaxLength="50" CssClass="form-control alignright" Width="50"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="displaynone768">
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                </label>
                                                            </span><span></span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                </label>
                                                            </span><span></span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="form-group selpen">
                                                        <div style="height: 50px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                    <asp:Button ID="ibtnEditInverter" runat="server" Text="Update" OnClick="ibtnEditInverter_Click"
                                        CssClass="btn btn-primary savewhiteicon btnsaveicon" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
    </Triggers>
</asp:UpdatePanel>
