﻿<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectstc.ascx.cs" Inherits="includes_controls_projectstc" %>
<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedSTC);
    function pageLoadedSTC() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });

    }
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>


        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea stcpage">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <div class="salepage">
                        <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">STC Status</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <asp:Label ID="Label2" runat="server" class="  control-label">
                                               STC Upload Date</asp:Label></span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtSTCUploaded" runat="server" class="form-control" Width="200px">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <%--   <div class="form-group dateimgarea">
                                                <span class="name">
                                                    <label class="control-label">
                                                        
                                                    </label>
                                                </span><span class="dateimg">
                                                    <asp:TextBox ID="txtSTCUploaded" runat="server" Width="150px" CssClass="form-control"></asp:TextBox>
                                                    <asp:ImageButton ID="Image27" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                    <cc1:CalendarExtender ID="CalendarExtender27" runat="server" PopupButtonID="Image27"
                                                        TargetControlID="txtSTCUploaded" Format="dd/MM/yyyy">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator ValidationGroup="stc" ControlToValidate="txtSTCUploaded" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>--%>

                                                        <div class="form-group">
                                                            <span class="name">
                                                                <asp:Label ID="Label1" runat="server" class="  control-label">
                                            STC Applied Date</asp:Label></span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtSTCApplied" runat="server" class="form-control" Width="200px">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>



                                                        <%-- <div class="form-group dateimgarea">
                                                <span class="name">
                                                    <label class="control-label">
                                                       
                                                    </label>
                                                </span><span class="dateimg">
                                                    <asp:TextBox ID="txtSTCApplied" runat="server" Width="150px" CssClass="form-control"></asp:TextBox>
                                                    <asp:ImageButton ID="Image28" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                    <cc1:CalendarExtender ID="CalendarExtender28" runat="server" PopupButtonID="Image28"
                                                        TargetControlID="txtSTCApplied" Format="dd/MM/yyyy">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator ValidationGroup="stc" ControlToValidate="txtSTCApplied" ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter valid date"
                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>--%>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    STC Upload Number
                                                                </label>
                                                            </span><span style="width: 200px;">
                                                                <asp:TextBox ID="txtSTCUploadNumber" runat="server" CssClass="form-control" MaxLength="10" Width="240px"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server" ValidationGroup="stc"
                                                                    ControlToValidate="txtSTCUploadNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    PVD Number
                                                                </label>
                                                            </span><span style="width: 200px;">
                                                                <asp:TextBox ID="txtPVDNumber" runat="server" CssClass="form-control" Width="240px"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group" style="width: 240px;">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    PVD Status
                                                                </label>
                                                            </span><span style="width: 300px;">
                                                                <asp:DropDownList ID="ddlPVDStatusID" runat="server" AppendDataBoundItems="true" Width="420px"
                                                                    aria-controls="DataTables_Table_0" class="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="width: 240px;">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    QuickForm ID
                                                                </label>
                                                            </span><span style="width: 200px;">
                                                                <asp:TextBox ID="txtformbayid" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                      <div class="col-md-11">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    STC Notes:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSTCNotes" CssClass="form-control" runat="server" Width="100%" Height="78px" TextMode="MultiLine"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 textcenterbutton center-text form-group">
                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateSTC" runat="server" OnClick="btnUpdateSTC_Click"
                                                    Text="Save" CausesValidation="true" ValidationGroup="stc" />
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="HiddenField2" runat="server" />
        </section>
    </ContentTemplate>

</asp:UpdatePanel>
