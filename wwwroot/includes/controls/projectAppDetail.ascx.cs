﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Globalization;

public partial class includes_controls_projectAppDetail : System.Web.UI.UserControl
{
    decimal capacity;
    decimal rebate;
    decimal SystemCapacity;
    decimal stcno;
    decimal output;

    protected void Page_Load(object sender, EventArgs e)
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;

        txtOpenDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        //if (ddlPanel.SelectedValue == "")
        //{
        //    txtZoneRt.Text = string.Empty;
        //}
        string ProjectID = Request.QueryString["proid"];
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {
                if (st2.ProjectStatusID == "3")
                {
                    btnUpdateSale.Visible = false;
                }
            }
            //activemethod();
            //if (Roles.IsUserInRole("SalesRep"))
            //{
            //    if (st2.DepositReceived != string.Empty)
            //    {
            //        btnUpdateSale.Visible = false;
            //    }
            //}
            if ((Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("PostInstaller")))
            {
                btnUpdateSale.Visible = true;
                Pan1.Enabled = true;
                Pan2.Enabled = true;
                Pan3.Enabled = true;
                Pan4.Enabled = false;
                Pan5.Enabled = false;
                Pan6.Enabled = false;
                Pan7.Enabled = false;
                Pan8.Enabled = false;
            }
            if ((Roles.IsUserInRole("PreInstaller")))
            {
                Pan4.Enabled = true;
            }
            //  txtqty2.Text = "0";
            //txtqty3.Text = "0";

            ListItem item5 = new ListItem();
            item5.Text = "Select";
            item5.Value = "";
            ddlSalesRep.Items.Clear();
            ddlSalesRep.Items.Add(item5);

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string SalesTeam = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }


                if (SalesTeam != string.Empty)
                {
                    ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                }
            }
            else
            {
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
            }
            //ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectByUserASC(userid);
            ddlSalesRep.DataValueField = "EmployeeID";
            ddlSalesRep.DataTextField = "fullname";
            ddlSalesRep.DataMember = "fullname";
            ddlSalesRep.DataBind();
            try
            {
                ddlSalesRep.SelectedValue = st2.SalesRep;
            }
            catch { }

            if (st2.ProjectTypeID == "8")
            {
                divsupplier.Visible = true;
            }
            else
            {
                divsupplier.Visible = false;
            }
        }
        if (!IsPostBack)
        {
            BindProjectSale();
        }


    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    public void BindProjectSale()
    {
        
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            divMtce.Visible = false;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);


            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            //if (Roles.IsUserInRole("PreInstaller"))
            //{
            //    PanAddUpdate.Enabled = true;
            //}
            //if (Roles.IsUserInRole("PostInstaller"))
            //{
            //    PanAddUpdate.Enabled = false;
            //}
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("SalesRep"))
            {
                ddlEmployee.Enabled = false;
                ddlElecDistAppBy.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }
            BindSaleDropDown();
            if (Roles.IsUserInRole("SalesRep") && (st.ProjectStatusID == "3" || st.ProjectStatusID == "5"))
            {
                PanAddUpdate.Enabled = false;
            }

            try
            {

                if (st.SalesType == string.Empty)
                {
                    rblboth.Checked = true;
                }
                if (st.SalesType == "1")
                {
                    rblonlypanel.Checked = true;
                }
                if (st.SalesType == "2")
                {
                    rblonlyinverter.Checked = true;
                }
                if (st.SalesType == "3")
                {
                    rblboth.Checked = true;
                }
            }
            catch
            {
            }

            try
            {
                //Response.Write(st.PanelBrandID);
                //Response.End();
                ddlPanel.SelectedValue = st.PanelBrandID;
            }
            catch { }

            txtPanelBrand.Text = st.PanelBrand;
            //Response.Write(st.PanelBrand);
            //Response.End();
            txtWatts.Text = st.PanelOutput;
            try
            {
                txtRebate.Text = SiteConfiguration.ChangeCurrencyVal((st.RECRebate).ToString());
            }
            catch { }
            txtSTCMult.Text = st.STCMultiplier;
            txtPanelModel.Text = st.PanelModel;
            txtNoOfPanels.Text = st.NumberPanels;
            txtSystemCapacity.Text = st.SystemCapKW;
            //try
            //{
            ddlInverter.SelectedValue = st.InverterDetailsID;
            //}
            //catch
            //{
            //}
            txtInverterBrand.Text = st.InverterBrand;
            txtSeries.Text = st.InverterSeries;
            try
            {
                ddlInverter2.SelectedValue = st.SecondInverterDetailsID;

                if (st.ThirdInverterDetailsID != "")
                {
                    ddlInverter3.SelectedValue = st.ThirdInverterDetailsID;
                }

            }
            catch
            {
            }
            txtSTCNo.Text = st.STCNumber;

            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                //try
                //{
                txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                //}
                //catch { }
            }

            txtInverterModel.Text = st.InverterModel;
            txtqty.Text = st.inverterqty;
            //Response.Write(st.inverterqty2);
            //Response.End();
            txtqty2.Text = st.inverterqty2;
            txtqty3.Text = st.inverterqty3;


            txtInverterOutput.Text = st.InverterOutput;
            //try
            //{
            ddlElecDist.SelectedValue = st.ElecDistributorID;
            //}
            //catch
            //{
            //}
            txtApprovalRef.Text = st.ElecDistApprovelRef;
            //try
            //{
            ddlElecRetailer.SelectedValue = st.ElecRetailerID;
            //}
            //catch
            //{
            //}
            txtRegPlanNo.Text = st.RegPlanNo;
            //try
            //{
            ddlHouseType.SelectedValue = st.HouseTypeID;
            //}
            //catch
            //{
            //}
            //try
            //{
            ddlRoofType.SelectedValue = st.RoofTypeID;
            //}
            //catch
            //{ }
            //try
            //{
            ddlRoofAngle.SelectedValue = st.RoofAngleID;
            //}
            //catch
            //{ }
            txtLotNum.Text = st.LotNumber;
            txtNMINumber.Text = st.NMINumber;
            txtPeakMeterNo.Text = st.MeterNumber1;
            txtOffPeakMeters.Text = st.MeterNumber2;
            txtMeterPhase.Text = st.MeterPhase;
            try
            {
                ddlmeterupgrade.SelectedValue = st.meterupgrade;
            }
            catch
            {
            }


            chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
            chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);

            if (st.InstallPostCode != "")
            {
                DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                if (dtZoneCode.Rows.Count > 0)
                {

                    string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                    if (dtZoneRt.Rows.Count > 0)
                    {
                        //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                        //Response.End();
                        txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                    }
                }
            }
            try
            {
                txtElecDistApplied.Text = Convert.ToDateTime(st.ElecDistApplied).ToShortDateString();
            }
            catch { }
            ddlElecDistApplyMethod.SelectedValue = st.ElecDistApplyMethod;
            txtElecDistApplySentFrom.Text = st.ElecDistApplySentFrom;

            SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);
            txtSPAInvoiceNumber.Text = st2.SPAInvoiceNumber;
            try
            {
                txtSPAPaid.Text = Convert.ToDateTime(st2.SPAPaid).ToShortDateString();
            }
            catch { }
            ddlSPAPaidBy.SelectedValue = st2.SPAPaidBy;
            try
            {

                txtSPAPaidAmount.Text = SiteConfiguration.ChangeCurrencyVal(st2.SPAPaidAmount);
            }
            catch { }

            ddlSPAPaidMethod.SelectedValue = st2.SPAPaidMethod;
            if (st.ElecDistributor == "14")
            {
                divSPA.Visible = true;
            }
            else
            {
                divSPA.Visible = false;
            }
            if (ddlElecDist.SelectedValue == "19" && st.InstallState == "TAS")
            {
                if (st.SurveyCerti != string.Empty)
                {
                    if (st.SurveyCerti == "1")
                    {
                        rblSurveyCerti.SelectedValue = "1";
                    }
                    if (st.SurveyCerti == "2")
                    {
                        rblSurveyCerti.SelectedValue = "2";
                    }
                    divSurveyCerti.Visible = true;
                }
                else
                {
                    divSurveyCerti.Visible = false;
                }

                if (st.CertiApprove != string.Empty)
                {
                    if (st.CertiApprove == "1")
                    {
                        rblCertiApprove.SelectedValue = "1";
                    }
                    if (st.CertiApprove == "2")
                    {
                        rblCertiApprove.SelectedValue = "2";
                    }
                    divCertiApprove.Visible = true;
                }
                else
                {
                    divCertiApprove.Visible = false;
                }
            }

            if (st.ElecDistAppBy == string.Empty)
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                try
                {
                    ddlElecDistAppBy.SelectedValue = stEmp.EmployeeID;
                }
                catch { }
            }
            else
            {
                try
                {
                    ddlElecDistAppBy.SelectedValue = st.ElecDistAppBy;
                }
                catch { }
            }
            try
            {
                txtElecDistAppDate.Text = Convert.ToDateTime(st.ElecDistAppDate).ToShortDateString();
            }
            catch { }

            if (Roles.IsUserInRole("Installation Manager"))
            {
                if (st.ProjectStatusID == "3")
                {
                    if (st.ElecDistributorID == "12")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                    else if (st.ElecDistributorID == "13")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                        txtLotNum.Enabled = false;
                        txtRegPlanNo.Enabled = false;
                    }
                    else
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                }
            }
            if (st.ProjectTypeID == "8")
            {
                divMtce.Visible = true;
                divPanelDetail.Visible = true;
                divInverterDetail.Visible = true;
                divSitedetails.Visible = false;
                divDistAppDetail.Visible = false;
                divMeterDetails.Visible = false;
            }
            else
            {
                divMtce.Visible = false;
            }
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
            {
                //Panel1.Enabled = false;
                Panel1.Attributes.Add("style", "pointer-events: none;");
            }
            else
            {
                //Panel1.Enabled = true;
                //Panel1.Attributes.Add("", "");
            }
        }
    }

    public void BindSaleDropDown()
    {
        SttblProjects stProj = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProj.CustomerID);

        //String Inverter1ID;

        DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(stProj.InstallState);
        string State = "0";

        if (dtState.Rows.Count > 0)
        {
            State = dtState.Rows[0]["CompanyLocationID"].ToString();
        }

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlPanel.Items.Clear();
        ddlPanel.Items.Add(item1);

        DataTable dtPanel = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            //Response.Write(State);
            //Response.End();
            dtPanel = ClsProjectSale.tblStockItems_SelectPanel_SalesRep(State);
        }
        else
        {

            dtPanel = ClsProjectSale.tblStockItems_SelectPanel(State);
        }

        ddlPanel.DataSource = dtPanel;
        ddlPanel.DataValueField = "StockItemID";
        ddlPanel.DataMember = "StockItem";
        ddlPanel.DataTextField = "StockItem";
        ddlPanel.DataBind();


        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlInverter.Items.Clear();
        ddlInverter.Items.Add(item2);

        DataTable dtInverter = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);            
        }
        else
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter(State);
        }

        ddlInverter.DataSource = dtInverter;
        ddlInverter.DataValueField = "StockItemID";
        ddlInverter.DataMember = "StockItem";
        ddlInverter.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter1ID = stProj.InverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter1ID))
            {
                SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter1ID);
                if (ststockid.SalesTag == "False")
                {
                    ListItem I1 = new ListItem(); // Example List
                    I1.Text = ststockid.StockItem;
                    I1.Value = Inverter1ID;
                    ddlInverter.Items.Add(I1);
                }
            }
        }
        ddlInverter.DataBind();

        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlInverter2.Items.Clear();
        ddlInverter2.Items.Add(item3);

        ddlInverter2.DataSource = dtInverter;
        ddlInverter2.DataValueField = "StockItemID";
        ddlInverter2.DataMember = "StockItem";
        ddlInverter2.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter2ID = stProj.SecondInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter2ID))
            {
                SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter2ID);
                if (ststockid.SalesTag == "False")
                {
                    ListItem I2 = new ListItem(); // Example List
                    I2.Text = ststockid.StockItem;
                    I2.Value = Inverter2ID;
                    ddlInverter2.Items.Add(I2);
                }
            }
        }    
        ddlInverter2.DataBind();


        ListItem item9 = new ListItem();
        item9.Text = "Select";
        item9.Value = "";
        ddlInverter3.Items.Clear();
        ddlInverter3.Items.Add(item9);

        ddlInverter3.DataSource = dtInverter;
        ddlInverter3.DataValueField = "StockItemID";
        ddlInverter3.DataMember = "StockItem";
        ddlInverter3.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter3ID = stProj.ThirdInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter3ID))
            {
                SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter3ID);
                if (ststockid.SalesTag == "False")
                {
                    ListItem I3 = new ListItem(); // Example List
                    I3.Text = ststockid.StockItem;
                    I3.Value = Inverter3ID;
                    ddlInverter3.Items.Add(I3);
                }
            }
        }
        ddlInverter3.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlHouseType.Items.Clear();
        ddlHouseType.Items.Add(item4);

        ddlHouseType.DataSource = ClstblHouseType.tblHouseType_SelectASC();
        ddlHouseType.DataValueField = "HouseTypeID";
        ddlHouseType.DataMember = "HouseType";
        ddlHouseType.DataTextField = "HouseType";
        ddlHouseType.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlRoofType.Items.Clear();
        ddlRoofType.Items.Add(item5);

        ddlRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        ddlRoofType.DataValueField = "RoofTypeID";
        ddlRoofType.DataMember = "RoofType";
        ddlRoofType.DataTextField = "RoofType";
        ddlRoofType.DataBind();

        ListItem item6 = new ListItem();
        item6.Text = "Select";
        item6.Value = "";
        ddlRoofAngle.Items.Clear();
        ddlRoofAngle.Items.Add(item6);

        ddlRoofAngle.DataSource = ClstblRoofAngles.tblRoofAngles_SelectASC();
        ddlRoofAngle.DataValueField = "RoofAngleID";
        ddlRoofAngle.DataMember = "RoofAngle";
        ddlRoofAngle.DataTextField = "RoofAngle";
        ddlRoofAngle.DataBind();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        System.Web.UI.WebControls.ListItem itemPaid = new System.Web.UI.WebControls.ListItem();
        itemPaid.Text = "Select";
        itemPaid.Value = "";
        ddlSPAPaidBy.Items.Clear();
        ddlSPAPaidBy.Items.Add(itemPaid);

        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                if (dr["SalesTeamID"].ToString() != string.Empty)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
            }
            if (SalesTeam != string.Empty)
            {
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {

            if (SalesTeam != string.Empty)
            {
                ddlSPAPaidBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSPAPaidBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        ddlSPAPaidBy.DataValueField = "EmployeeID";
        ddlSPAPaidBy.DataMember = "fullname";
        ddlSPAPaidBy.DataTextField = "fullname";
        ddlSPAPaidBy.DataBind();

        System.Web.UI.WebControls.ListItem itemMethod = new System.Web.UI.WebControls.ListItem();
        itemMethod.Text = "Select";
        itemMethod.Value = "";
        ddlSPAPaidMethod.Items.Clear();
        ddlSPAPaidMethod.Items.Add(itemMethod);

        ddlSPAPaidMethod.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
        ddlSPAPaidMethod.DataValueField = "FPTransTypeID";
        ddlSPAPaidMethod.DataMember = "FPTransType";
        ddlSPAPaidMethod.DataTextField = "FPTransType";
        ddlSPAPaidMethod.DataBind();

        System.Web.UI.WebControls.ListItem item11 = new System.Web.UI.WebControls.ListItem();
        item11.Text = "Select";
        item11.Value = "";
        ddlElecDistApplyMethod.Items.Clear();
        ddlElecDistApplyMethod.Items.Add(item11);

        ddlElecDistApplyMethod.DataSource = ClsProjectSale.tblElecDistApplyMethod_SelectASC();
        ddlElecDistApplyMethod.DataValueField = "ElecDistApplyMethodID";
        ddlElecDistApplyMethod.DataMember = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataTextField = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataBind();


        ddlpickthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlpickthrough.DataValueField = "PickUpID";
        ddlpickthrough.DataMember = "PickUpID";
        ddlpickthrough.DataTextField = "PickUp";
        ddlpickthrough.DataBind();

        ddlsentthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlsentthrough.DataValueField = "PickUpID";
        ddlsentthrough.DataMember = "PickUpID";
        ddlsentthrough.DataTextField = "PickUp";
        ddlsentthrough.DataBind();

        ddlhsentthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlhsentthrough.DataValueField = "PickUpID";
        ddlhsentthrough.DataMember = "PickUpID";
        ddlhsentthrough.DataTextField = "PickUp";
        ddlhsentthrough.DataBind();


        System.Web.UI.WebControls.ListItem item12 = new System.Web.UI.WebControls.ListItem();
        item12.Text = "Select";
        item12.Value = "";
        ddlEmployee.Items.Clear();
        ddlEmployee.Items.Add(item12);

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
            if (SalesTeam != string.Empty)
            {
                ddlEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataMember = "fullname";
            ddlEmployee.DataTextField = "fullname";
            ddlEmployee.DataBind();


            ddlEmployee.SelectedValue = stEmp.EmployeeID;
        }
        catch { }


        System.Web.UI.WebControls.ListItem item13 = new System.Web.UI.WebControls.ListItem();
        item13.Text = "Select";
        item13.Value = "";
        ddlElecDistAppBy.Items.Clear();
        ddlElecDistAppBy.Items.Add(item13);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlElecDistAppBy.DataValueField = "EmployeeID";
            ddlElecDistAppBy.DataMember = "fullname";
            ddlElecDistAppBy.DataTextField = "fullname";
            ddlElecDistAppBy.DataBind();
        }
        catch
        { }


        string EleDist = "";
        if (stProj.InstallState == "NSW")
        {
            EleDist = "select * from tblElecDistributor where NSW=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "SA")
        {
            EleDist = "select * from tblElecDistributor where SA=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "QLD")
        {
            EleDist = "select * from tblElecDistributor where QLD=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "VIC")
        {
            EleDist = "select * from tblElecDistributor where VIC=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "WA")
        {
            EleDist = "select * from tblElecDistributor where WA=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "ACT")
        {
            EleDist = "select * from tblElecDistributor where ACT=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "TAS")
        {
            EleDist = "select * from tblElecDistributor where TAS=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "NT")
        {
            EleDist = "select * from tblElecDistributor where NT=1 order by ElecDistributor asc";
        }

        DataTable dtElecDist = ClsProjectSale.ap_form_element_execute(EleDist);
        if (dtElecDist.Rows.Count > 0)
        {
            ListItem item7 = new ListItem();
            item7.Text = "Select";
            item7.Value = "";
            ddlElecDist.Items.Clear();
            ddlElecDist.Items.Add(item7);

            ddlElecDist.DataSource = dtElecDist;
            ddlElecDist.DataValueField = "ElecDistributorID";
            ddlElecDist.DataMember = "ElecDistributor";
            ddlElecDist.DataTextField = "ElecDistributor";
            ddlElecDist.DataBind();
        }

        string EleRet = "";
        if (stProj.InstallState == "NSW")
        {
            EleRet = "select * from tblElecRetailer where NSW=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "SA")
        {
            EleRet = "select * from tblElecRetailer where SA=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "QLD")
        {
            EleRet = "select * from tblElecRetailer where QLD=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "VIC")
        {
            EleRet = "select * from tblElecRetailer where VIC=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "WA")
        {
            EleRet = "select * from tblElecRetailer where WA=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "ACT")
        {
            EleRet = "select * from tblElecRetailer where ACT=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "TAS")
        {
            EleRet = "select * from tblElecRetailer where TAS=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "NT")
        {
            EleRet = "select * from tblElecRetailer where NT=1 order by ElecRetailer asc";
        }

        DataTable dtElecRet = ClsProjectSale.ap_form_element_execute(EleRet);
        if (dtElecRet.Rows.Count > 0)
        {
            ListItem item8 = new ListItem();
            item8.Text = "Select";
            item8.Value = "";
            ddlElecRetailer.Items.Clear();
            ddlElecRetailer.Items.Add(item8);

            ddlElecRetailer.DataSource = dtElecRet;
            ddlElecRetailer.DataValueField = "ElecRetailerID";
            ddlElecRetailer.DataMember = "ElecRetailer";
            ddlElecRetailer.DataTextField = "ElecRetailer";
            ddlElecRetailer.DataBind();
        }

        ListItem item21 = new ListItem();
        item21.Text = "Select";
        item21.Value = "";
        ddlProjectMtceReasonID.Items.Clear();
        ddlProjectMtceReasonID.Items.Add(item21);

        ddlProjectMtceReasonID.DataSource = ClstblProjectMtceReason.tblProjectMtceReason_SelectASC();
        ddlProjectMtceReasonID.DataValueField = "ProjectMtceReasonID";
        ddlProjectMtceReasonID.DataMember = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataTextField = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataBind();
        try
        {
            ddlProjectMtceReasonID.SelectedValue = "2";
        }
        catch { }


        ListItem item22 = new ListItem();
        item22.Text = "Select";
        item22.Value = "";
        ddlProjectMtceReasonSubID.Items.Clear();
        ddlProjectMtceReasonSubID.Items.Add(item22);

        ddlProjectMtceReasonSubID.DataSource = ClstblProjectMtceReasonSub.tblProjectMtceReasonSub_SelectASC();
        ddlProjectMtceReasonSubID.DataValueField = "ProjectMtceReasonSubID";
        ddlProjectMtceReasonSubID.DataMember = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataTextField = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataBind();

        ddlProjectMtceReasonSubID.SelectedValue = "2";

        ListItem item25 = new ListItem();
        item25.Text = "Select";
        item25.Value = "";
        ddlProjectMtceCallID.Items.Clear();
        ddlProjectMtceCallID.Items.Add(item25);

        ddlProjectMtceCallID.DataSource = ClstblProjects.tblProjectMtceCall_SelectASC();
        ddlProjectMtceCallID.DataValueField = "ProjectMtceCallID";
        ddlProjectMtceCallID.DataMember = "ProjectMtceCall";
        ddlProjectMtceCallID.DataTextField = "ProjectMtceCall";
        ddlProjectMtceCallID.DataBind();

        ddlProjectMtceCallID.SelectedValue = "4";

        ListItem item26 = new ListItem();
        item26.Text = "Select";
        item26.Value = "";
        ddlProjectMtceStatusID.Items.Clear();
        ddlProjectMtceStatusID.Items.Add(item26);

        if (stProj.ProjectTypeID != "8")
        {
            ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectTop4();
        }
        else
        {
            ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectLast4();
        }
        ddlProjectMtceStatusID.DataValueField = "ProjectMtceStatusID";
        ddlProjectMtceStatusID.DataMember = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataTextField = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataBind();

        ddlProjectMtceStatusID.SelectedValue = "2";
    }

    protected void btnUpdateSale_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string salestype = "";
        if (rblonlypanel.Checked == true)
        {
            salestype = "1";
        }
        if (rblonlyinverter.Checked == true)
        {
            salestype = "2";
        }
        if (rblonlyinverter.Checked == true)
        {
            salestype = "3";
        }

        string PanelBrandID = ddlPanel.SelectedValue; //ID
        string PanelBrand = txtPanelBrand.Text;
        string PanelOutput = txtWatts.Text;
        string RECRebate = txtRebate.Text;
        string STCMultiplier = txtSTCMult.Text;
        string STCZoneRating = txtZoneRt.Text;
        string PanelModel = txtPanelModel.Text;
        string NumberPanels = txtNoOfPanels.Text;
        string SystemCapKW = txtSystemCapacity.Text;

        string PanelDetails = "";
        if (ddlPanel.SelectedValue != string.Empty)
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            if (ddlPanel.SelectedValue != "")
            {
                PanelDetails = txtNoOfPanels.Text + " X " + txtWatts.Text + " Watt " + stPanel.StockItem + " Panels. (" + txtPanelModel.Text + ")";
            }
        }

        string InverterDetailsID = ddlInverter.SelectedValue; //Id
        string InverterBrand = txtInverterBrand.Text;
        string InverterSeries = txtSeries.Text;
        string SecondInverterDetailsID = ddlInverter2.SelectedValue;
        string ThirdInverterDetailsID = ddlInverter3.SelectedValue;
        string STCNumber = txtSTCNo.Text;

        DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
        string STCValue = "0";
        if (dtSTCValue.Rows.Count > 0)
        {
            STCValue = dtSTCValue.Rows[0]["STCValue"].ToString();
        }
        string InverterModel = txtInverterModel.Text;
        string inverterqty = txtqty.Text;
        string inverterqty2 = txtqty2.Text;
        string inverterqty3 = txtqty3.Text;
        string InverterOutput = txtInverterOutput.Text;

        string InverterDetails = "";
        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter ";
        }
        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter2.StockItem + " KW Inverter";
        }
        if (ddlInverter.SelectedValue != "" && ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter. Plus " + stInverter2.StockItem + " KW Inverter.";
        }

        string SystemDetails = "";
        if (ddlPanel.SelectedValue != "")
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            SystemDetails = txtNoOfPanels.Text + " X " + stPanel.StockItem;
        }
        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SystemDetails += " + " + stInverter.StockItem;
        }
        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            SystemDetails += " + " + stInverter2.StockItem;
        }

        string ElecDistributorID = ddlElecDist.SelectedValue;
        string ElecDistApprovelRef = txtApprovalRef.Text;
        string ElecRetailerID = ddlElecRetailer.SelectedValue;
        string RegPlanNo = txtRegPlanNo.Text;
        string HouseTypeID = ddlHouseType.SelectedValue;
        string RoofTypeID = ddlRoofType.SelectedValue;
        string RoofAngleID = ddlRoofAngle.SelectedValue;
        string LotNumber = txtLotNum.Text;
        string NMINumber = txtNMINumber.Text;
        string MeterNumber1 = txtPeakMeterNo.Text;
        string MeterNumber2 = txtOffPeakMeters.Text;
        string MeterPhase = txtMeterPhase.Text;
        string MeterEnoughSpace = Convert.ToString(chkEnoughMeterSpace.Checked);
        string OffPeak = Convert.ToString(chkIsSystemOffPeak.Checked);

        string SPAInvoiceNumber = txtSPAInvoiceNumber.Text;
        string SPAPaid = txtSPAPaid.Text.Trim();
        string SPAPaidBy = ddlSPAPaidBy.SelectedValue;
        string SPAPaidAmount = txtSPAPaidAmount.Text;
        string SPAPaidMethod = ddlSPAPaidMethod.SelectedValue;

        string ElecDistApplied = txtElecDistApplied.Text.Trim();
        string ElecDistApplyMethod = ddlElecDistApplyMethod.SelectedValue;
        string ElecDistApplySentFrom = txtElecDistApplySentFrom.Text;

        string FlatPanels = txtFlatPanels.Text.Trim();
        string PitchedPanels = txtPitchedPanels.Text.Trim();
        string SurveyCerti = rblSurveyCerti.SelectedValue;
        string CertiApprove = rblCertiApprove.SelectedValue;
        string ElecDistAppDate = txtElecDistAppDate.Text.Trim();
        string ElecDistAppBy = ddlElecDistAppBy.SelectedValue;

        //Response.Write(stPro.InstallPostCode);
        //Response.End();
        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stPro.InstallPostCode);
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
        }
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        if (txtNoOfPanels.Text != stPro.NumberPanels)
        {
            //bool sucSalePrice = ClsProjectSale.tblProjects_UpdateSalePrice(ProjectID);

            //add by jigar chokshi
            //bool sucSaleTotalQuote = ClsProjectSale.tblProjects_UpdateTotalQuotePrice(ProjectID, "0");
            int sucpanelslog = ClsProjectSale.tblPanelsUser_log_insert(ProjectID, txtNoOfPanels.Text, userid);
        }
        //Response.Write(ddlInverter2.SelectedValue + "==" + SecondInverterDetailsID);
        //Response.End();
        if (SecondInverterDetailsID == "")
        {
            SecondInverterDetailsID = "0";
        }
        if (ThirdInverterDetailsID == "")
        {
            ThirdInverterDetailsID = "0";
        }
        bool successSale = ClsProjectSale.tblProjects_UpdateSale(ProjectID, PanelBrandID, PanelBrand, PanelOutput, RECRebate, STCMultiplier, STCZoneRating, PanelModel, NumberPanels, SystemCapKW, PanelDetails, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, STCValue, InverterModel, InverterOutput, InverterDetails, SystemDetails, ElecDistributorID, ElecDistApprovelRef, ElecRetailerID, RegPlanNo, HouseTypeID, RoofTypeID, RoofAngleID, LotNumber, NMINumber, MeterNumber1, MeterNumber2, MeterPhase, MeterEnoughSpace, OffPeak, UpdatedBy, ElecDistApplied, ElecDistApplyMethod, ElecDistApplySentFrom, FlatPanels, PitchedPanels, SurveyCerti, ElecDistAppDate, ElecDistAppBy, CertiApprove, ThirdInverterDetailsID);
        // add by roshni
        bool successSale_meterupgrade = ClsProjectSale.tblProjects_UpdateSale_meterupgrade(ProjectID, ddlmeterupgrade.SelectedValue);//
                                                                                                                                     //add by jigar chokshi
                                                                                                                                     //Response.Write(SecondInverterDetailsID);


        bool successSale_inverterqty = ClsProjectSale.tblProjects_UpdateSale_inverterqty(ProjectID, inverterqty, inverterqty2, inverterqty3);

        bool sucQuote2 = ClsProjectSale.tblProjects2_UpdateQuote(ProjectID, SPAInvoiceNumber, SPAPaid, SPAPaidBy, SPAPaidAmount, SPAPaidMethod);

        ClsProjectSale.tblProjects_UpdateSalesType(ProjectID, salestype);

        int Tracksuccess = ClstblTrackPanelDetails.tblTrackPanelDetails_Insert(ProjectID, DateTime.Now.ToString(), userid, PanelBrandID, PanelBrand, RECRebate, NumberPanels, PanelOutput, STCMultiplier, SystemCapKW, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, InverterModel, InverterOutput);

        if (ddlthroughtype.SelectedValue != string.Empty)
        {
            if (ddlthroughtype.SelectedValue == "1")
            {
                bool scc_update = ClsProjectSale.tblProjects_Update_InverterReplacementDetail(ProjectID, ddlthroughtype.SelectedValue, txtoldserialno.Text, txtlogdement.Text, txtlogdementdate.Text, txttrackingno.Text, txtnewserialno.Text, txterrormessage.Text, txtfaultydate.Text, ddlpickthrough.SelectedValue, txtdeliveryreceived.Text, txtsentsupplier.Text, ddlsentthrough.SelectedValue, txtinvoicesent.Text, txtinvoiceno.Text, ddlinstallerpickup.SelectedValue);
            }
            if (ddlthroughtype.SelectedValue == "2")
            {
                bool scc_update = ClsProjectSale.tblProjects_Update_InverterReplacementDetail(ProjectID, ddlthroughtype.SelectedValue, htxtoldserialno.Text, txthlogdementno.Text, txthlogdementdate.Text, "", txthnewserialno.Text, txtherrormesage.Text, "", "", txtdeliveryrec.Text, txt_hsentsupplierdate.Text, ddlhsentthrough.SelectedValue, txt_incoicesent_date.Text, txt_invoiceno.Text, "");
                //ddlinstallerpickup,txth_faultyunit,
            }
        }

        if (stPro.ProjectTypeID == "8")
        {
            ClsProjectSale.tblProjectMaintenance_UpdateDetail(ProjectID, txtOpenDate.Text.Trim(), ddlProjectMtceReasonID.SelectedValue, ddlProjectMtceReasonSubID.SelectedValue, ddlProjectMtceCallID.SelectedValue, ddlProjectMtceStatusID.SelectedValue, Convert.ToString(chkWarranty.Checked), txtCustomerInput.Text, txtFaultIdentified.Text, txtActionRequired.Text, txtWorkDone.Text);
        }

        DataTable dt = ClsProjectSale.tblProjects_CheckProjectActiveBefore(ProjectID);
        if (dt.Rows.Count == 0)//It would check if the project had status 'Active' before.It would allow only if status of project was never 'Active'
        {
            activemethod();
        }
        BindProjects();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void activemethod()//put by roshni
    {
        string ProjectID = Request.QueryString["proid"];


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (stPro.ElecDistributorID == "13")
        {

            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                //Response.Write(stPro.DocumentVerified);
                //Response.End();
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    //Response.Write(stPro.DocumentVerified );
                    //Response.End();
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.RegPlanNo == string.Empty || stPro.LotNumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
            }
        }
        else if (stPro.ElecDistributorID == "12")
        {
            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
            }
        }
        else
        {
            if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
            }

            else if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.ActiveDate != string.Empty)
            {

                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");

            }
        }

    }
    protected void ddlPanel_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);


        //   Response.End();
        //  labl.Text = "jepal";

    }

    protected void ddlInverter_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        PanAddUpdate.Visible = true;
        inverteroutput();

    }

    protected void ddlInverter2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        PanAddUpdate.Visible = true;
        inverteroutput();
    }

    protected void ddlElecDist_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        PanAddUpdate.Visible = true;
        if (ddlElecDist.SelectedValue != string.Empty)
        {
            if (ddlElecDist.SelectedValue == "14")
            {
                divSPA.Visible = true;
            }
            else
            {
                divSPA.Visible = false;
            }

            string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (ddlElecDist.SelectedValue == "19" && stPro.InstallState == "TAS")
            {
                divSurveyCerti.Visible = true;
                divCertiApprove.Visible = true;
            }
            else
            {
                divSurveyCerti.Visible = false;
                divCertiApprove.Visible = false;
            }

            //DataTable dtEnergex = ClstblElecDistributor.tblElecDistributor_SelectEnergex();
            //if (dtEnergex.Rows.Count > 0)
            //{
            //    if (ddlElecDist.SelectedValue == dtEnergex.Rows[0]["ElecDistributorID"].ToString())
            //    {
            //        RequiredFieldValidatorApprovalRef.Visible = true;
            //        RequiredFieldValidatorNMINumber.Visible = true;
            //    }
            //    else
            //    {
            //        RequiredFieldValidatorNMINumber.Visible = false;
            //    }
            //}

            //DataTable dtErgon = ClstblElecDistributor.tblElecDistributor_SelectErgon();
            //if (dtErgon.Rows.Count > 0)
            //{
            //    if (ddlElecDist.SelectedValue == dtErgon.Rows[0]["ElecDistributorID"].ToString())
            //    {
            //        RequiredFieldValidatorApprovalRef.Visible = true;
            //        RequiredFieldValidatorElecRetailer.Visible = true;
            //        RequiredFieldValidatorRegPlanNo.Visible = true;
            //    }
            //}
        }
        else
        {
            divSurveyCerti.Visible = false;
            divCertiApprove.Visible = false;
        }
    }

    protected void txtNoOfPanels_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        PanAddUpdate.Visible = true;
        if (txtNoOfPanels.Text != string.Empty)
        {
            capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * Convert.ToDecimal(txtWatts.Text)) / 1000;
            txtSystemCapacity.Text = Convert.ToString(capacity);
            string installbooking = stPro.InstallBookingDate;
            if (installbooking == null || installbooking == "")
            {
                DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                String year = currentdate.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);

                //-------------------------------------------------------------------
                //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //---------------------------------------------------------------------
            }
            else
            {

                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);

                //--------------------------------------------------------------------------
                //string date = Convert.ToDateTime(installbooking).ToShortDateString();
                //DateTime dt1 = Convert.ToDateTime("31/12/2017");
                //DateTime dt2 = Convert.ToDateTime(date);
                //int year = dt2.Year;
                //if (dt2 <= dt1)
                //{
                //    stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 14);
                //}
                //else
                //{
                //    stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //}
                //-----------------------------------------------------------------------------

            }
            //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
            int myInt = (int)Math.Floor(stcno);
            txtSTCNo.Text = Convert.ToString(myInt);
            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            rebate = myInt * Convert.ToDecimal(txtSTCValue.Text);
            txtRebate.Text = Convert.ToString(rebate);
        }
    }
    protected void ddlRoofType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        if (ddlRoofType.SelectedValue != string.Empty)
        {
            if (ddlRoofType.SelectedValue == "8") // Tin Flat
            {
                ddlRoofAngle.Enabled = false;
                ddlRoofAngle.SelectedValue = "";
            }
            else
            {
                ddlRoofAngle.Enabled = true;
            }
            if (ddlRoofType.SelectedValue == "7") // Tin + Tile  
            {
                divFlatPanels.Visible = true;
                divPitchedPanels.Visible = true;
                lblFlatPanels.Text = "Panels on Tin";
                lblPitchedPanels.Text = "Panels on Tile";
            }
            else if (ddlRoofType.SelectedValue == "9") // Tin Pitched + Flat
            {
                divFlatPanels.Visible = true;
                divPitchedPanels.Visible = true;
                lblFlatPanels.Text = "Panels on Flat";
                lblPitchedPanels.Text = "Panels on Pitched";
            }
            else
            {
                divFlatPanels.Visible = false;
                divPitchedPanels.Visible = false;
                lblFlatPanels.Text = "";
                lblPitchedPanels.Text = "";
            }
        }
    }
    protected void ddlthroughtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (ddlthroughtype.SelectedValue == "1")
        {
            divsupplierthrough.Visible = true;
            divinhousethrough.Visible = false;
        }
        else
        {
            divinhousethrough.Visible = true;
            divsupplierthrough.Visible = false;
        }

    }
    protected void ddlPanel_SelectedIndexChanged1(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                 this.GetType(),
        //                                 "MyAction",
        //                                 "doMyAction();",
        //                                 true);
        PanAddUpdate.Visible = true;
        
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st2.InstallPostCode);
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
        }
        if (ddlPanel.SelectedValue != string.Empty)
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            txtPanelBrand.Text = st.StockManufacturer;
            txtPanelModel.Text = st.StockModel;
            txtWatts.Text = st.StockSize;

            if (!string.IsNullOrEmpty(st.StockSize) && !string.IsNullOrEmpty(txtNoOfPanels.Text))
            {
                SystemCapacity = (Convert.ToDecimal(st.StockSize) * Convert.ToDecimal(txtNoOfPanels.Text)) / 1000;
                txtSystemCapacity.Text = Convert.ToString(SystemCapacity);
            }

            txtSTCMult.Text = "1";
            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                try
                {
                    txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                }
                catch { }
            }
            capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * Convert.ToDecimal(txtWatts.Text)) / 1000;
            string installbooking = stPro.InstallBookingDate;
            if (installbooking == null || installbooking == "")
            {
                DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                String year = currentdate.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);

                //-------------------------------------------------------------------
                //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //---------------------------------------------------------------------
            }
            else
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);

                //--------------------------------------------------------------------------
                //string date = Convert.ToDateTime(installbooking).ToShortDateString();
                //DateTime dt1 = Convert.ToDateTime("31/12/2017");
                //DateTime dt2 = Convert.ToDateTime(date);
                //int year = dt2.Year;
                //if (dt2 <= dt1)
                //{
                //    stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 14);
                //}
                //else
                //{
                //    stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //}
                //-----------------------------------------------------------------------------
            }
            //stcno = (Convert.ToDecimal(capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
            //stcno = (Convert.ToDecimal(SystemCapacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
            //int myInt = (int)Math.Round(stcno);
            int myInt = (int)Math.Floor(stcno);
            txtSTCNo.Text = Convert.ToString(myInt);
            rebate = myInt * Convert.ToDecimal(txtSTCValue.Text);
            txtRebate.Text = Convert.ToString(rebate);
        }
        else
        {
            txtPanelBrand.Text = string.Empty;
            txtPanelModel.Text = string.Empty;
            txtWatts.Text = string.Empty;
        }
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        // PanAddUpdate.Visible = true;
        //if (ddlPanel.SelectedValue != string.Empty)
        //{
        //    SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
        //    txtPanelBrand.Text = st.StockManufacturer;
        //    txtPanelModel.Text = st.StockModel;
        //    txtWatts.Text = st.StockSize;

        //    if (!string.IsNullOrEmpty(st.StockSize) && !string.IsNullOrEmpty(txtNoOfPanels.Text))
        //    {
        //        SystemCapacity = (Convert.ToDecimal(st.StockSize) * Convert.ToDecimal(txtNoOfPanels.Text)) / 1000;
        //        txtSystemCapacity.Text = Convert.ToString(SystemCapacity);
        //    }

        //    txtSTCMult.Text = "1";
        //    DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
        //    if (dtSTCValue.Rows.Count > 0)
        //    {
        //        try
        //        {
        //            txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
        //        }
        //        catch { }
        //    }

        //    if (SystemCapacity != 0 && txtZoneRt.Text != string.Empty)
        //    {
        //        stcno = (Convert.ToDecimal(SystemCapacity) * Convert.ToDecimal(txtZoneRt.Text) * 15);
        //        int myInt = (int)Math.Round(stcno);
        //        txtSTCNo.Text = Convert.ToString(myInt);
        //        rebate = myInt * Convert.ToDecimal(txtSTCValue.Text);
        //        txtRebate.Text = Convert.ToString(rebate);
        //    }
        //}
        //else
        //{
        //    txtPanelBrand.Text = string.Empty;
        //    txtPanelModel.Text = string.Empty;
        //    txtWatts.Text = string.Empty;
        //}

    }

    public void inverteroutput()
    {

        //Response.Write(ddlInverter3.SelectedValue + ":<br/>");
        //Response.Write(ddlInverter2.SelectedValue + ":<br/>");
        //Response.Write(ddlInverter.SelectedValue);
        //Response.End();



        try
        {
            if (ddlInverter2.SelectedValue == "")
            {
                ddlInverter2.SelectedValue = "0";
            }
            if (ddlInverter3.SelectedValue == "")
            {
                ddlInverter3.SelectedValue = "0";
            }

            if (ddlInverter.SelectedValue == "")
            {
                ddlInverter.SelectedValue = "0";
            }
        }
        catch { }

        var inv2stocksize = "0";
        try
        {
            SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);

            if (st2.StockSize != string.Empty)
            {
                inv2stocksize = st2.StockSize;
            }
        }
        catch { }

        var inv3stocksize = "0";
        try
        {
            SttblStockItems st3 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter3.SelectedValue);
            if (st3.StockSize != string.Empty)
            {
                inv3stocksize = st3.StockSize;
            }
        }
        catch { }

        SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
        if (ddlInverter.SelectedValue != string.Empty)
        {
            txtInverterBrand.Text = st.StockManufacturer;
            txtInverterModel.Text = st.StockModel;
            txtSeries.Text = st.StockSeries;
        }
        else
        {
            txtInverterBrand.Text = "";
            txtInverterModel.Text = "";
            txtSeries.Text = "";
        }



        var invstocksize = "0";
        if (st.StockSize != string.Empty)
        {
            invstocksize = st.StockSize;
        }
        output = Convert.ToDecimal(inv2stocksize) + Convert.ToDecimal(invstocksize) + Convert.ToDecimal(inv3stocksize);
        txtInverterOutput.Text = Convert.ToString(output);



        //if (ddlInverter2.SelectedValue == "" && ddlInverter.SelectedValue != "" && ddlInverter3.SelectedValue == "")
        //    {
        //        SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
        //        txtInverterOutput.Text = Convert.ToString(st.StockSize);
        //    }

        //    if (ddlInverter.SelectedValue != "")
        //    {
        //        SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
        //        output = Convert.ToDecimal(st.StockSize) + Convert.ToDecimal(st2.StockSize) + Convert.ToDecimal(st3.StockSize);
        //        txtInverterOutput.Text = Convert.ToString(output);
        //    }
        //    else
        //    {
        //        txtInverterOutput.Text = st2.StockSize;
        //    }


    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    protected void ddlInverter3_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        inverteroutput();
    }
}