using System;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Net;

public partial class includes_controls_projectvicdoc : System.Web.UI.UserControl
{
    public includes_controls_projectvicdoc()
        : base()
    {
    }

    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        //{
        //    string ProjectID = Request.QueryString["proid"];
        //    SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //    try
        //    {
        //        if(!string.IsNullOrEmpty(st2.InstallState))
        //        {
        //            if(st2.InstallState=="VIC")
        //            {

        //            }
        //        }
        //    }
        //    catch { }
        //}
        bindEWR();
    }


    public void BindVicDoc()
    {
        panelvicdoc.Visible = true;
    }


    protected void btnCreateSTCForm_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        try
        {
            TextWriter txtWriter = new StringWriter() as TextWriter;
            if (st.ProjectTypeID == "3")
            {
                Telerik_reports.generate_stcug(ProjectID);
            }
            else
            {
                Telerik_reports.generate_STCform(ProjectID);

            }


        }
        catch { }
    }

    protected void btnsolarsystem_Click(object sender, EventArgs e)
    {
        //string ProjectID = "";
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                Telerik_reports.generate_solar_system(Request.QueryString["proid"]);
            }
        }
        catch
        {

        }
    }

    protected void rptInstDocs_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndID = (HiddenField)e.Item.FindControl("hndID");
            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc");

            SttblInstallerExtraDocs st = ClsProjectSale.tblInstallerExtraDocs_SelectByID(hndID.Value);
            // hypDoc.NavigateUrl = "~/userfiles/instotherdocs/" + st.DocName;
            hypDoc.Text = st.DocName;
            //hypDoc.NavigateUrl = pdfURL + "instotherdocs/" + st.DocName;
            hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("instotherdocs", st.DocName);
        }
    }

    protected void btnWelcomeDocs_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        bool suc = ClstblProjects.tblProjects_UpdateWelcomeLetter(ProjectID);
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        TextWriter txtWriter = new StringWriter() as TextWriter;
        //try
        //{
        if (stPro.InstallState == "VIC")
        {
            Telerik_reports.generate_welcomeletter(ProjectID);
            //Server.Execute("~/mailtemplate/welcomelettervic.aspx?id=" + ProjectID, txtWriter);
        }
        else
        {
            Telerik_reports.generate_welcomeletter(ProjectID);
            //Server.Execute("~/mailtemplate/welcomeletter.aspx?id=" + ProjectID, txtWriter);
        }
        //}
        //catch { }
        // String htmlText = txtWriter.ToString();
        //Response.Write(htmlText);
        //HTMLExportToPDF(htmlText, ProjectID + "WelcomeLetter.pdf");
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        // string ID = hdndelete.Value.ToString();
        SttblInstallerExtraDocs stdoc = ClsProjectSale.tblInstallerExtraDocs_SelectByID(ID);
        bool success = SiteConfiguration.DeletePDFFile("instotherdocs", stdoc.DocName);
        if (success)
        {
            bool sucess1 = ClsProjectSale.tblInstallerExtraDocs_Delete(ID);
            if (sucess1)
            {
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }

    }

    protected void btnPrintReceipt_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        if (!string.IsNullOrEmpty(ProjectID))
        {
            Telerik_reports.generate_receipt(ProjectID);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string EWR = "";
        Random r = new Random();
        int num = r.Next();
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (fuST.HasFile)
        {
            string OldEWRFilename = string.Empty;
            DataTable dt = ClstblVicDoc.tbl_ewr_upload_Select();
            if (dt.Rows.Count > 0)
            {
                OldEWRFilename = dt.Rows[0]["ewr_randomfilename"].ToString();
            }
            SiteConfiguration.DeletePDFFile("EWR", OldEWRFilename);
            EWR = num + fuST.FileName;
            fuST.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\EWR\\") + EWR);
            if (hypewr.Text == "")
            {
                bool success = ClstblVicDoc.tbl_ewr_upload_Insert(1, fuST.FileName, num + fuST.FileName, "EWR/" + num + fuST.FileName);
                SetUpload();
            }
            else
            {
                bool updatesuccess = ClstblVicDoc.tbl_ewr_upload_Update(1, fuST.FileName, num + fuST.FileName, "EWR/" + num + fuST.FileName);
                SetUpload();
            }
            SiteConfiguration.UploadPDFFile("EWR", num + fuST.FileName);
            SiteConfiguration.deleteimage(num + fuST.FileName, "EWR");
            
        }
        bindEWR();
    }

    public void bindEWR()
    {
        DataTable dt = ClstblVicDoc.tbl_ewr_upload_Select();
        if (dt.Rows.Count > 0)
        {
            hypewr.Text = dt.Rows[0]["ewr_filename"].ToString();
            // hypewr.InnerText = dt.Rows[0]["ewr_filename"].ToString();
            // hypewr.HRef = pdfURL+dt.Rows[0]["ewr_filepath"].ToString();
            //hypewr.Attributes.Add("download", "download");
            //hypewr.HRef = pdfURL+"EWR/"+dt.Rows[0]["ewr_randomfilename"].ToString();
        }
    }

    protected void btncerti_Click(object sender, ImageClickEventArgs e)
    {
        //string ProjectID = "";
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                Telerik_reports.generate_EWR(Request.QueryString["proid"]);
            }
        }
        catch
        {

        }
    }

    protected void hypewr_Click(object sender, EventArgs e)
    {
        DataTable dt = ClstblVicDoc.tbl_ewr_upload_Select();
        if (dt.Rows.Count > 0)
        {
            //string path = pdfURL + dt.Rows[0]["ewr_filepath"].ToString();
            //System.Net.WebClient webClient = new System.Net.WebClient();
            //byte[] bytes = webClient.DownloadData(path);
            //string fileName = dt.Rows[0]["ewr_randomfilename"].ToString();
            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            //Response.BinaryWrite(bytes);
            //Response.End();
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetUpload()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunUploadSuccess();", true);
    }
}