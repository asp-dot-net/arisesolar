using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Configuration;

public partial class includes_controls_contactinfo : System.Web.UI.UserControl
{
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        //string contid = (Request.QueryString["contid"]);
        //  BindContactInfo();
        HidePanels();
        //SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
        //SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st2.CustomerID);
        ////Response.Write(stCust.CustTypeID);
        ////Response.End();
        //if (stCust.CustTypeID == "11")
        //{
        //    divInst.Visible = true;
        //}
        //else
        //{
        //    divInst.Visible = false;
        //}
    }
    public void HidePanels()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
    }
    public void BindContactInfo()
    {
        //Response.Write("hii");
        //Response.End();
        PanAddUpdate.Enabled = true;
        CheckRecordCount();
        {
            if (!string.IsNullOrEmpty(Request.QueryString["contid"]))
            {
                Reset();
                SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);

                chkDocStoreDone.Checked = Convert.ToBoolean(st2.DocStoreDone);
                if (st2.AL != string.Empty)
                {
                    lblAL.Visible = true;
                    lblAL.Text = st2.AL;
                    //lblAL.NavigateUrl = "~/userfiles/AL/" + st2.AL;
                    //lblAL.NavigateUrl = pdfURL + "AL/" + st2.AL;
                    lblAL.NavigateUrl = SiteConfiguration.GetDocumnetPath("AL", st2.AL);
                    RequiredFieldValidatorAL.Visible = false;
                }
                else
                {
                    lblAL.Visible = false;
                }
                if (st2.DocStoreDone == "True")
                {
                    divAL.Visible = true;
                    if (st2.AL == string.Empty)
                    {
                        RequiredFieldValidatorAL.Visible = false;
                    }
                }
                else
                {
                    divAL.Visible = false;
                }

                chkInstaller.Checked = Convert.ToBoolean(st2.Installer);
                chkDesigner.Checked = Convert.ToBoolean(st2.Designer);
                chkElectrician.Checked = Convert.ToBoolean(st2.Electrician);
                chkMeterElectrician.Checked = Convert.ToBoolean(st2.MeterElectrician);

                txtContNotes.Text = st2.ContNotes;
                try
                {
                    txtContactEntered.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st2.ContactEntered));
                }
                catch { }

                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.ContactEnteredBy);
                txtContactEnteredBy.Text = stEmp.EmpFirst + ' ' + stEmp.EmpLast;
                txtContactID.Text = Request.QueryString["contid"];
                txtElecLicence.Text = st2.ElecLicence;
                txtAccreditation.Text = st2.Accreditation;
                try
                {
                    txtElecLicenceExpires.Text = Convert.ToDateTime(st2.ElecLicenceExpires).ToShortDateString();
                }
                catch { }
                txtRefBSB.Text = st2.RefBSB;
                txtRefAccount.Text = st2.RefAccount;
                //Response.Write("hii");
                //Response.End();
                SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st2.CustomerID);
                if (stCust.CustTypeID == "4")
                {

                    divInst.Visible = true;
                }
                else
                {
                    divInst.Visible = false;
                }
                //Response.Write(txtContactEnteredBy.Text);
                //Response.Write(txtContactID.Text);
                //Response.Write(txtRefBSB.Text);
                //Response.End();
            }
        }
    }

    public void CheckRecordCount()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblContacts.AdmintblContactsGetCount(userid, Request.QueryString["contid"]);
        if (dt.Rows.Count == 0)
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Administrator"))
        {
            PanAddUpdate.Enabled = true;
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            string CSalesTeamID = stEmpC.SalesTeamID;

            if (Request.QueryString["contid"] != string.Empty)
            {
                SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stCont.EmployeeID);
                string EmpType = stEmp.EmpType;
                string SalesTeamID = stEmp.SalesTeamID;

                if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
                {
                    PanAddUpdate.Enabled = true;
                }
                else
                {
                    PanAddUpdate.Enabled = false;
                }
            }
        }
        if (Roles.IsUserInRole("Installer"))
        {
            PanAddUpdate.Enabled = false;
        }
    }

    public void BindData()
    {
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
        lblAL.Visible = true;
        lblAL.Text = st2.AL;
        //lblAL.NavigateUrl = "~/userfiles/AL/" + st2.AL;
        //lblAL.NavigateUrl = pdfURL + "AL/" + st2.AL;
        lblAL.NavigateUrl = SiteConfiguration.GetDocumnetPath("AL", st2.AL);
    }

    protected void btnUpdateDetail_Click(object sender, EventArgs e)
    {
        string ContactID = Request.QueryString["contid"];

        string DocStoreDone = Convert.ToString(chkDocStoreDone.Checked);
        string Installer = Convert.ToString(chkInstaller.Checked);
        string Designer = Convert.ToString(chkDesigner.Checked);
        string Electrician = Convert.ToString(chkElectrician.Checked);
        string MeterElectrician = Convert.ToString(chkMeterElectrician.Checked);

        string ContNotes = txtContNotes.Text;
        string ElecLicence = txtElecLicence.Text;
        string Accreditation = txtAccreditation.Text;
        string ElecLicenceExpires = txtElecLicenceExpires.Text;
        string RefBSB = txtRefBSB.Text;
        string RefAccount = txtRefAccount.Text;
        string AL = "";

        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
        bool sucDetail = ClstblContacts.tblContacts_UpdateInfo(ContactID, ContNotes, ElecLicence, Accreditation, DocStoreDone, ElecLicenceExpires, RefBSB, RefAccount, Installer, Designer, Electrician, MeterElectrician);

        if (chkDocStoreDone.Checked == true)
        {
            if (fuAL.HasFile)
            {
                SiteConfiguration.DeletePDFFile("AL", st2.AL);                
                AL = ContactID + fuAL.FileName;
                fuAL.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\AL\\") + AL);
                bool sucAL = ClstblContacts.tblContacts_UpdateAL(ContactID, AL);
                SiteConfiguration.UploadPDFFile("AL", AL);
                SiteConfiguration.deleteimage(AL, "AL");
            }
            RequiredFieldValidatorAL.Visible = true;
        }
        else
        {
            SiteConfiguration.DeletePDFFile("AL", st2.AL);            
            bool sucAL = ClstblContacts.tblContacts_UpdateAL(ContactID, "");
            RequiredFieldValidatorAL.Visible = false;
        }

        //--- do not chage this code Start------
        if (sucDetail)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
        BindData();

        BindContactInfo();
    }

    protected void chkDocStoreDone_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (chkDocStoreDone.Checked == true)
        {
            divAL.Visible = true;
            RequiredFieldValidatorAL.Visible = true;
        }
        else
        {
            divAL.Visible = false;
            RequiredFieldValidatorAL.Visible = false;
        }
    }

    public void Reset()
    {
        txtContNotes.Text = string.Empty;
        txtContactEntered.Text = string.Empty;
        txtContactEnteredBy.Text = string.Empty;
        txtContactID.Text = string.Empty;
        txtRefBSB.Text = string.Empty;
        txtRefAccount.Text = string.Empty;
        txtElecLicence.Text = string.Empty;
        txtAccreditation.Text = string.Empty;
        txtElecLicenceExpires.Text = string.Empty;

        chkDocStoreDone.Checked = false;
        chkInstaller.Checked = false;
        chkDesigner.Checked = false;
        chkElectrician.Checked = false;
        chkMeterElectrician.Checked = false;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}