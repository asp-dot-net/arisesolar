using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.IO;
using System.Configuration;
using System.Collections.Generic;

public partial class includes_controls_projectelecinv : System.Web.UI.UserControl
{
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {
                if (st2.ProjectStatusID == "3")
                {
                    btnUpdateElecInv.Visible = false;
                }
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                if (st2.DepositReceived != string.Empty)
                {
                    btnUpdateElecInv.Visible = false;
                }
            }
            rangelessamount.MaximumValue = "1000000000000000";
            if (hdnamountsum.Value != string.Empty)
            {
                //Response.Write(rangelessamount.MaximumValue + "==" + rangelessamount.MinimumValue);
                //Response.End();
                rangelessamount.MinimumValue = Convert.ToString(hdnamountsum.Value);

                rangelessamount.ErrorMessage = "Amount Should Be Greater Than " + hdnamountsum.Value + ".";

            }
            else
            {
                rangelessamount.MinimumValue = "0";
            }

            bind_grid();
        }
    }

    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        //Response.Write(dt.Rows.Count);
        //Response.End();
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    public void BindProjectElecInv()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
            string employeeid = st.EmployeeID;
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByEmployeeID(employeeid);

            //if (st.ProjectStatusID != "9")
            //{
            //    if (st.ProjectStatusID == "3" || st.ProjectStatusID == "5" || st.ProjectStatusID == "10")
            //    {
            //        divActive.Visible = false;
            //        PanAddUpdate.Visible = true;
            if ((Roles.IsUserInRole("Sales Manager")))
            {
                PanAddUpdate.Enabled = false;
            }
            if (st.FinanceWithID == string.Empty || st.FinanceWithID == "1")
            {
            }
            else
            {
                if (st.DocumentVerified == "True" || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Administrator"))
                {
                    PanAddUpdate.Visible = true;
                    DivDocNotVerified.Visible = false;
                }
                else
                {
                    PanAddUpdate.Visible = false;
                    DivDocNotVerified.Visible = true;
                }
            }
            //    }
            //    else
            //    {
            //        divActive.Visible = true;
            //        PanAddUpdate.Visible = false;
            //    }
            //}

            if (Roles.IsUserInRole("Administrator"))
            {
                // divAddUpdate.Visible = true;
            }
            //if ((Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Accountant")) || stEmpC.SalesTeamID == "4")

            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("InstallationManager"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                Panel1.Enabled = false;
                Panel2.Enabled = true;
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if ((Roles.IsUserInRole("PostInstaller")))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }

            //if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            //{
            //    string CEmpType = stEmpC.EmpType;
            //    string CSalesTeamID = stEmpC.SalesTeamID;

            //    if (Profile.eurosolar.contactid != string.Empty)
            //    {
            //        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
            //        string EmpType = stEmp.EmpType;
            //        string SalesTeamID = stEmp.SalesTeamID;

            //        if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
            //        {
            //            PanAddUpdate.Enabled = true;
            //        }
            //        else
            //        {
            //            PanAddUpdate.Enabled = false;
            //        }
            //    }
            //}

            BindDropDown();
            BindInvoice();

            if (Roles.IsUserInRole("Accountant"))
            {
                Panel2.Enabled = true;
            }
            txtElevHouseType.Text = st.HouseType;
            txtElevRoofType.Text = st.RoofType;
            txtElevRoofAngle.Text = st.RoofAngle;
            txtTravelDist.Text = st.TravelTime;
            try
            {
                txtElecOtherVariations.Text = SiteConfiguration.ChangeCurrencyVal(st.VarOther);
            }
            catch { }
            if (st.PanelBrandID != "" && st.InverterDetailsID != "")
            {
                txtElecPanelDetails.Text = st.PanelDetails;
                txtElecInverterDetails.Text = st.SystemDetails;
            }
            chkElecSplitSystem.Checked = Convert.ToBoolean(st.SplitSystem);
            chkElecSTCSaved.Checked = Convert.ToBoolean(st.STCFormSaved);
            chkElecCheryPicker.Checked = Convert.ToBoolean(st.CherryPicker);
            chkElecCertSaved.Checked = Convert.ToBoolean(st.CertificateSaved);

            txtInstallerInvNo.Text = st.InstallerInvNo;

            try
            {
                txtSalesCommPaid.Text = Convert.ToDateTime(st.SalesCommPaid).ToShortDateString();
            }
            catch { }
            if (st.Installer != string.Empty)
            {
                try
                {
                    ddlInstaller.SelectedValue = st.Installer;
                }
                catch { }
            }

            try
            {
                txtInstallerAmnt.Text = SiteConfiguration.ChangeCurrencyVal(st.InstallerInvAmnt);
            }
            catch { }
            try
            {
                txtInstallerInvDate.Text = Convert.ToDateTime(st.InstallerInvDate).ToShortDateString();
            }
            catch { }
            try
            {
                txtInstallerPayDate.Text = Convert.ToDateTime(st.InstallerPayDate).ToShortDateString();
            }
            catch { }
            txtElectricianInvoiceNotes.Text = st.ElectricianInvoiceNotes;

            try
            {
                txtBaseCostPrice.Text = SiteConfiguration.ChangeCurrencyVal(st.ServiceValue);
            }
            catch { }

            try
            {
                txtSalesInvDate.Text = Convert.ToDateTime(st2.SalesInvDate).ToShortDateString();
            }
            catch { }
            txtSalesPayNotes.Text = st2.SalesPayNotes;
            try
            {
                txtSalesPayDate.Text = Convert.ToDateTime(st2.SalesPayDate).ToShortDateString();
            }
            catch { }

            if (st2.SalesInvNo == string.Empty)
            {
                //btnCreateTaxInvoice.Visible = true;
                //btnOpenTaxInvoice.Visible = false;
            }
            else
            {
                //btnCreateTaxInvoice.Visible = false;
                //btnOpenTaxInvoice.Visible = true;
            }

            if (st.InvDoc != string.Empty)
            {
                lblInvDoc.Visible = true;
                lblInvDoc.Text = st.InvDoc;
                //lblInvDoc.NavigateUrl = "~/userfiles/InvDocSales/" + st.InvDoc;
                //lblInvDoc.NavigateUrl = pdfURL + "InvDocSales/" + st.InvDoc;
                lblInvDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("InvDocSales", st.InvDoc);
                //RequiredFieldValidatorInvDoc.Visible = false;
            }
            else
            {
                lblInvDoc.Visible = false;
            }

            if (st.InvDocDoor != string.Empty)
            {
                lblInvDocDoor.Visible = true;
                lblInvDocDoor.Text = st.InvDocDoor;
                //lblInvDocDoor.NavigateUrl = "~/userfiles/InvDocDoor/" + st.InvDocDoor;
                //lblInvDocDoor.NavigateUrl = pdfURL + "InvDocDoor/" + st.InvDocDoor;
                lblInvDocDoor.NavigateUrl = SiteConfiguration.GetDocumnetPath("InvDocDoor", st.InvDocDoor);
                //RequiredFieldValidatorInvDocDoor.Visible = false;
            }
            else
            {
                lblInvDocDoor.Visible = false;
            }

            txtExtraWork.Text = st.PreExtraWork;
            try
            {
                txtAmount.Text = SiteConfiguration.ChangeCurrencyVal(st.PreAmount);
            }
            catch { }

            ddlApprovedBy.SelectedValue = st.PreApprovedBy;

            string SalesTeam = "";
            DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(employeeid);
            if (dt_empsale1.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale1.Rows)
                {
                    if (dr["SalesTeamID"].ToString() == "4")
                    {
                        SalesTeam = dr["SalesTeamID"].ToString();
                    }
                }
            }

            if (SalesTeam == "4" || Roles.IsUserInRole("DSales Manager"))
            {
                ListItem item1 = new ListItem();
                item1.Text = "Select";
                item1.Value = "";
                ddlInvType.Items.Clear();
                ddlInvType.Items.Add(item1);

                ddlInvType.DataSource = ClstblInvoiceCommon.tblInvCommonType_SelectTeamC();
                ddlInvType.DataValueField = "InvCommonTypeID";
                ddlInvType.DataMember = "InvCommonType";
                ddlInvType.DataTextField = "InvCommonType";
                ddlInvType.DataBind();
            }
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PostInstaller"))
            {
                ListItem item1 = new ListItem();
                item1.Text = "Select";
                item1.Value = "";
                ddlInvType.Items.Clear();
                ddlInvType.Items.Add(item1);

                ddlInvType.DataSource = ClstblInvoiceCommon.tblInvCommonType_SelectAll();
                ddlInvType.DataValueField = "InvCommonTypeID";
                ddlInvType.DataMember = "InvCommonType";
                ddlInvType.DataTextField = "InvCommonType";
                ddlInvType.DataBind();
            }
            if (Roles.IsUserInRole("Installer"))
            {
                ListItem item1 = new ListItem();
                item1.Text = "Select";
                item1.Value = "";
                ddlInvType.Items.Clear();
                ddlInvType.Items.Add(item1);

                ddlInvType.DataSource = ClstblInvoiceCommon.tblInvCommonType_SelectInst();
                ddlInvType.DataValueField = "InvCommonTypeID";
                ddlInvType.DataMember = "InvCommonType";
                ddlInvType.DataTextField = "InvCommonType";
                ddlInvType.DataBind();

                ddlApprovedBy.Enabled = false;
                txtExtraWork.Enabled = false;
                txtAmount.Enabled = false;

                divInstaller.Visible = true;
                ddlInvType.SelectedValue = "1";

                divSalesComm.Visible = false;
            }
            if (userid == "4673ec49-eb9f-4049-b3ac-61a7a6578c0b")
            {
                PanAddUpdate.Enabled = true;
            }
            BindGrid();
        }
    }
    public void BindDropDown()
    {
        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlApprovedBy.Items.Clear();
        ddlApprovedBy.Items.Add(item5);

        ddlApprovedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlApprovedBy.DataValueField = "EmployeeID";
        ddlApprovedBy.DataMember = "fullname";
        ddlApprovedBy.DataTextField = "fullname";
        ddlApprovedBy.DataBind();

        DataTable dtInst = ClstblContacts.tblContacts_SelectInverter();
        ListItem item2 = new ListItem();
        item2.Text = "Installer";
        item2.Value = "";
        ddlInstaller.Items.Clear();
        ddlInstaller.Items.Add(item2);

        ddlInstaller.DataSource = dtInst;
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataBind();

        //ListItem item3 = new ListItem();
        //item3.Text = "Installer";
        //item3.Value = "";
        //ddlInstallerDD.Items.Clear();
        //ddlInstallerDD.Items.Add(item3);

        //ddlInstallerDD.DataSource = dtInst;
        //ddlInstallerDD.DataValueField = "ContactID";
        //ddlInstallerDD.DataTextField = "Contact";
        //ddlInstallerDD.DataMember = "Contact";
        //ddlInstallerDD.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Installer";
        item4.Value = "";
        ddlInstallerMtce.Items.Clear();
        ddlInstallerMtce.Items.Add(item4);

        ddlInstallerMtce.DataSource = dtInst;
        ddlInstallerMtce.DataValueField = "ContactID";
        ddlInstallerMtce.DataTextField = "Contact";
        ddlInstallerMtce.DataMember = "Contact";
        ddlInstallerMtce.DataBind();
    }
    public void BindData()
    {
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        lblInvDoc.Visible = true;
        lblInvDoc.Text = st.InvDoc;
        //lblInvDoc.NavigateUrl = "~/userfiles/InvDocSales/" + st.InvDoc;
        //lblInvDoc.NavigateUrl = pdfURL + "InvDocSales/" + st.InvDoc;
        lblInvDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("InvDocSales", st.InvDoc);

        lblInvDocDoor.Visible = true;
        lblInvDocDoor.Text = st.InvDocDoor;
        //lblInvDocDoor.NavigateUrl = "~/userfiles/InvDocDoor/" + st.InvDocDoor;
        //lblInvDocDoor.NavigateUrl = pdfURL + "InvDocDoor/" + st.InvDocDoor;
        lblInvDocDoor.NavigateUrl = SiteConfiguration.GetDocumnetPath("InvDocDoor", st.InvDocDoor);
    }
    public void BindGrid()
    {
        DataTable dtInvCom = ClstblInvoiceCommon.tblInvoiceCommon_SelectByProject(Request.QueryString["proid"]);
        if (dtInvCom.Rows.Count > 0)
        {
            grdInvoice.Visible = true;
            GridView1.DataSource = dtInvCom;
            GridView1.DataBind();
        }
        else
        {
            grdInvoice.Visible = false;
        }
    }

    protected void btnUpdateElecInv_Click(object sender, EventArgs e)
    {


        string InstallerInvNo = txtInstallerInvNo.Text;
        string InstallerInvAmnt = txtInstallerAmnt.Text;
        string InstallerInvDate = txtInstallerInvDate.Text.Trim();
        string InstallerPayDate = txtInstallerPayDate.Text.Trim();
        string ElectricianInvoiceNotes = txtElectricianInvoiceNotes.Text;
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string SalesLandingPrice = txtSalesLandingPrice.Text;
        string SalesMarketPrice = txtSalesMarketPrice.Text;
        string SalesInvAmnt = txtSalesInvAmnt.Text;
        string SalesInvNo = txtSalesInvNo.Text;
        string SalesInvDate = txtSalesInvDate.Text.Trim();
        string SalesPayNotes = txtSalesPayNotes.Text;
        string SalesPayDate = txtSalesPayDate.Text.Trim();

        string PreExtraWork = txtExtraWork.Text;
        string PreAmount = txtAmount.Text;
        string PreApprovedBy = ddlApprovedBy.SelectedValue;
        string SalesComm = Convert.ToString(chkSalesComm.Checked);
        string SalesCommPaid = txtSalesCommPaid.Text.Trim();
        string PanelComm = Convert.ToString(chkPanelComm.Checked);

        string InvDoc = "";
        string InvDocDoor = "";
        //===================rangevalidator lessdeduct========================

        //===================End rangevalidator lessdeduct========================
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        decimal ucomm = 0;
        decimal ecomm = 0;

        string SalesTeam = "";
        DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
        if (dt_empsale1.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale1.Rows)
            {
                if (dr["SalesTeamID"].ToString() == "4")
                {
                    SalesTeam = dr["SalesTeamID"].ToString();
                }
            }
        }
        if (SalesTeam == "4")
        {
            if (Convert.ToDecimal(txtBaseCostPrice.Text) >= 0 && Convert.ToDecimal(txtBaseCostPrice.Text) >= 0)
            {
                ucomm = Convert.ToDecimal(txtSalesMarketPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text);
            }
            else
            {
                ucomm = 0;
            }
            if (Convert.ToDecimal(txtBaseCostPrice.Text) >= 0 && Convert.ToDecimal(txtSalesMarketPrice.Text) >= 0)
            {
                ecomm = (Convert.ToDecimal(txtBaseCostPrice.Text) - Convert.ToDecimal(txtSalesMarketPrice.Text)) / 2;
            }
            else
            {
                ecomm = 0;
            }
        }

        string UserCommision = Convert.ToString(ucomm);
        string EuroCommision = Convert.ToString(ecomm);

        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        string UpdatedBy = "";
        if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
        {
            SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(userid);
            UpdatedBy = stCont.ContactID;
        }
        else
        {
            UpdatedBy = stEmpC.EmployeeID;
        }

        if (fuInvDoc.HasFile)
        {
            SiteConfiguration.DeletePDFFile("InvDocSales", st.InvDoc);
            InvDoc = ProjectID + fuInvDoc.FileName;
            fuInvDoc.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/InvDocSales/") + InvDoc);
            bool sucInvDoc = ClsProjectSale.tblProjects_UpdateInvDoc(ProjectID, InvDoc);
            SiteConfiguration.UploadPDFFile("InvDocSales", InvDoc);
            SiteConfiguration.deleteimage(InvDoc, "InvDocSales");
        }
        //else
        //{
        //    bool sucInvDoc = ClsProjectSale.tblProjects_UpdateInvDoc(ProjectID, InvDoc);
        //}

        if (fuInvDocDoor.HasFile)
        {
            SiteConfiguration.DeletePDFFile("InvDocDoor", st.InvDocDoor);
            InvDocDoor = ProjectID + fuInvDocDoor.FileName;

            fuInvDocDoor.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/InvDocDoor/") + InvDocDoor);
            bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(ProjectID, InvDocDoor);
            SiteConfiguration.UploadPDFFile("InvDocDoor", InvDocDoor);
            SiteConfiguration.deleteimage(InvDocDoor, "InvDocDoor");
        }
        //else
        //{
        //    bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(ProjectID, InvDocDoor);
        //}
        BindData();
        bool sucElecInv = ClsProjectSale.tblProjects_UpdateElecInv(ProjectID, InstallerInvNo, InstallerInvAmnt, InstallerInvDate, InstallerPayDate, ElectricianInvoiceNotes, UpdatedBy, PreExtraWork, PreAmount, PreApprovedBy, SalesComm);
        ClsProjectSale.tblProjects_UpdatePanelComm(ProjectID, PanelComm);


        /* ---------------------------- Common Invoice ---------------------------- */

        if (ddlInvType.SelectedValue == "1")
        {

            SttblInvoiceCommon stInv1 = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType("1", ProjectID);
            string inv_no = txtInvNo.Text;
            if (inv_no != string.Empty)
            {

                //int exist1 = ClstblInvoiceCommon.tblInvoiceCommon_ExistsByInvType("1", ProjectID);
                //if (exist1 == 0)
                if (hdnGrid.Value == string.Empty)
                {
                    //lbl.Text = ProjectID + inv_no + InstallerInvAmnt + InstallerInvDate + InstallerInvDate + InstallerPayDate + ElectricianInvoiceNotes;
                    //int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert("1", ProjectID, InstallerInvNo, InstallerInvAmnt, InstallerInvDate, InstallerPayDate, ElectricianInvoiceNotes, UpdatedBy, UpdatedBy, PreExtraWork, PreAmount, PreApprovedBy, ddlInstaller.SelectedValue);
                    int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert("1", ProjectID, inv_no, txtInvAmnt.Text, txtInvDate.Text, txtPayDate.Text, txtNotes.Text, UpdatedBy, UpdatedBy, PreExtraWork, PreAmount, PreApprovedBy, ddlInstaller.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(success.ToString(), txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(success.ToString(), txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDoc.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv1.InvDoc);
                        string InvDocC = Convert.ToString(success) + fuInvDoc.FileName;

                        fuInvDoc.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), "");
                    //}
                }
                else
                {
                    ClstblInvoiceCommon.tblInvoiceCommon_Update(stInv1.InvoiceID, "1", ProjectID, inv_no, txtInvAmnt.Text, txtInvDate.Text, txtPayDate.Text, txtNotes.Text, UpdatedBy, PreExtraWork, PreAmount, PreApprovedBy, ddlInstaller.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(stInv1.InvoiceID, txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(stInv1.InvoiceID, txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDoc.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv1.InvDoc);
                        string InvDocC = stInv1.InvoiceID + fuInvDoc.FileName;

                        fuInvDoc.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv1.InvoiceID, InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv1.InvoiceID, stInv1.InvDoc);
                    //}
                }


            }
        }

        if (ddlInvType.SelectedValue == "2" || ddlInvType.SelectedValue == "7" || ddlInvType.SelectedValue == "8")
        {

            // Response.Write(Convert.ToDecimal(txtBaseCostPrice.Text)+"="+(Convert.ToDecimal(txtSalesLandingPrice.Text)));
            // Response.End();
            if ((Convert.ToDecimal(txtBaseCostPrice.Text) >= 0) && (Convert.ToDecimal(txtSalesLandingPrice.Text) >= 0))
            {

                if (Convert.ToDecimal((Convert.ToDecimal(txtBaseCostPrice.Text)) - (Convert.ToDecimal(txtSalesMarketPrice.Text))) > 0)
                {
                    double amount = (Convert.ToDouble(txtBaseCostPrice.Text) - Convert.ToDouble(txtSalesMarketPrice.Text)) * 0.5;
                    decimal amnt = (Convert.ToDecimal(amount)) + (Convert.ToDecimal(txtSalesMarketPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text));
                    txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
                }
                else
                {
                    decimal amnt = Convert.ToDecimal(txtBaseCostPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text);
                    txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
                }
            }
            string inv_no = txtInvNo.Text;
            SttblInvoiceCommon stInv2 = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType("2", ProjectID);
            if (inv_no != string.Empty)
            {

                //int exist2 = ClstblInvoiceCommon.tblInvoiceCommon_ExistsByInvType(ddlInvType.SelectedValue, ProjectID);
                //if (exist2 == 0)
                if (hdnGrid.Value == string.Empty)
                {

                    //  int success = ClstblInvoiceCommon.tblInvoiceCommon_InsertD2D(ddlInvType.SelectedValue, ProjectID, SalesInvNo, SalesInvAmnt, SalesInvDate, SalesPayDate, SalesPayNotes, UpdatedBy, UpdatedBy, "", "", "", "", txtSalesMarketPrice.Text, txtSalesLandingPrice.Text, txtAdvanceAmount.Text.Trim(), txtAdvanceDate.Text.Trim());
                    int success = ClstblInvoiceCommon.tblInvoiceCommon_InsertD2D(ddlInvType.SelectedValue, ProjectID, inv_no, txtInvAmnt.Text, txtInvDate.Text, txtPayDate.Text, txtNotes.Text, UpdatedBy, UpdatedBy, "", "", "", "", txtSalesMarketPrice.Text, txtSalesLandingPrice.Text, txtAdvanceAmount.Text.Trim(), txtAdvanceDate.Text.Trim());
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(success.ToString(), txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocDoor.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv2.InvDoc);
                        string InvDocDoorC = Convert.ToString(success) + fuInvDocDoor.FileName;

                        fuInvDocDoor.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocDoorC);

                        bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(Convert.ToString(success), InvDocDoorC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocDoorC);
                        SiteConfiguration.deleteimage(InvDocDoorC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(Convert.ToString(success), "");
                    //}
                }
                else
                {

                    //--------------------------chnge hdnfeild replace stInv2.InvoiceID-------------------------------

                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateD2D(hdnGrid.Value, ddlInvType.SelectedValue, ProjectID, inv_no, txtInvAmnt.Text, txtInvDate.Text, txtPayDate.Text, txtNotes.Text, UpdatedBy, "", "", "", "", txtSalesMarketPrice.Text, txtSalesLandingPrice.Text, txtAdvanceAmount.Text.Trim(), txtAdvanceDate.Text.Trim());
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(hdnGrid.Value, txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)//fuInvDocCom repalce to fuInvDocDoor
                    {

                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv2.InvDoc);
                        string InvDocDoorC = hdnGrid.Value + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocDoorC);
                        //Response.Write(hdnGrid.Value + "==" + InvDocDoorC);
                        //Response.End();
                        bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(hdnGrid.Value, InvDocDoorC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocDoorC);
                        SiteConfiguration.deleteimage(InvDocDoorC, "commoninvdoc");
                    }
                    //else
                    //{

                    //    bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(hdnGrid.Value, "");
                    //}
                }
            }
            ClsProjectSale.tblProjects2_UpdateElecInvTeamC(ProjectID, SalesLandingPrice, SalesMarketPrice, SalesInvAmnt, SalesInvNo, SalesInvDate, SalesPayNotes, SalesPayDate, UserCommision, EuroCommision);
        }
        if (ddlInvType.SelectedValue == "1011")
        {

            // Response.Write(Convert.ToDecimal(txtBaseCostPrice.Text)+"="+(Convert.ToDecimal(txtSalesLandingPrice.Text)));
            // Response.End();
            if ((Convert.ToDecimal(txtBaseCostPrice.Text) >= 0) && (Convert.ToDecimal(txtSalesLandingPrice.Text) >= 0))
            {

                if (Convert.ToDecimal((Convert.ToDecimal(txtBaseCostPrice.Text)) - (Convert.ToDecimal(txtSalesMarketPrice.Text))) > 0)
                {
                    double amount = (Convert.ToDouble(txtBaseCostPrice.Text) - Convert.ToDouble(txtSalesMarketPrice.Text)) * 0.5;
                    decimal amnt = (Convert.ToDecimal(amount)) + (Convert.ToDecimal(txtSalesMarketPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text));
                    txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
                }
                else
                {
                    decimal amnt = Convert.ToDecimal(txtBaseCostPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text);
                    txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
                }
            }
            string inv_no = txtInvNo.Text;
            SttblInvoiceCommon stInv2 = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType("2", ProjectID);
            if (inv_no != string.Empty)
            {

                //int exist2 = ClstblInvoiceCommon.tblInvoiceCommon_ExistsByInvType(ddlInvType.SelectedValue, ProjectID);
                //if (exist2 == 0)
                if (hdnGrid.Value == string.Empty)
                {

                    //  int success = ClstblInvoiceCommon.tblInvoiceCommon_InsertD2D(ddlInvType.SelectedValue, ProjectID, SalesInvNo, SalesInvAmnt, SalesInvDate, SalesPayDate, SalesPayNotes, UpdatedBy, UpdatedBy, "", "", "", "", txtSalesMarketPrice.Text, txtSalesLandingPrice.Text, txtAdvanceAmount.Text.Trim(), txtAdvanceDate.Text.Trim());
                    int success = ClstblInvoiceCommon.tblInvoiceCommon_InsertD2D(ddlInvType.SelectedValue, ProjectID, inv_no, txtInvAmnt.Text, txtInvDate.Text, txtPayDate.Text, txtNotes.Text, UpdatedBy, UpdatedBy, "", "", "", "", txtSalesMarketPrice.Text, txtSalesLandingPrice.Text, txtAdvanceAmount.Text.Trim(), txtAdvanceDate.Text.Trim());
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(success.ToString(), txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_Updatemanagecomm(success.ToString(), txtpanelcost.Text, txtflatejob.Text, txtpanelno.Text);
                    if (fuInvDocDoor.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv2.InvDoc);
                        string InvDocDoorC = Convert.ToString(success) + fuInvDocDoor.FileName;

                        fuInvDocDoor.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocDoorC);

                        bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(Convert.ToString(success), InvDocDoorC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocDoorC);
                        SiteConfiguration.deleteimage(InvDocDoorC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(Convert.ToString(success), "");
                    //}
                }
                else
                {

                    //--------------------------chnge hdnfeild replace stInv2.InvoiceID-------------------------------

                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateD2D(hdnGrid.Value, ddlInvType.SelectedValue, ProjectID, inv_no, txtInvAmnt.Text, txtInvDate.Text, txtPayDate.Text, txtNotes.Text, UpdatedBy, "", "", "", "", txtSalesMarketPrice.Text, txtSalesLandingPrice.Text, txtAdvanceAmount.Text.Trim(), txtAdvanceDate.Text.Trim());
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(hdnGrid.Value, txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_Updatemanagecomm(hdnGrid.Value, txtpanelcost.Text, txtflatejob.Text, txtpanelno.Text);
                    //ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(hdnGrid.Value, txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)//fuInvDocCom repalce to fuInvDocDoor
                    {

                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv2.InvDoc);
                        string InvDocDoorC = hdnGrid.Value + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocDoorC);
                        //Response.Write(hdnGrid.Value + "==" + InvDocDoorC);
                        //Response.End();
                        bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(hdnGrid.Value, InvDocDoorC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocDoorC);
                        SiteConfiguration.deleteimage(InvDocDoorC, "commoninvdoc");
                    }
                    //else
                    //{

                    //    bool sucInvDocDoor = ClsProjectSale.tblProjects_UpdateInvDocDoor(hdnGrid.Value, "");
                    //}
                }
            }
            ClsProjectSale.tblProjects2_UpdateElecInvTeamC(ProjectID, SalesLandingPrice, SalesMarketPrice, SalesInvAmnt, SalesInvNo, SalesInvDate, SalesPayNotes, SalesPayDate, UserCommision, EuroCommision);
        }
        if (ddlInvType.SelectedValue == "3")
        {
            SttblInvoiceCommon stInv3 = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType("3", ProjectID);
            if (txtInvNo.Text.Trim() != string.Empty)
            {

                //int exist3 = ClstblInvoiceCommon.tblInvoiceCommon_ExistsByInvType("3", ProjectID);
                //if (exist3 == 0)
                if (hdnGrid.Value == string.Empty)
                {
                    int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert("3", ProjectID, txtInvNo.Text, txtInvAmnt.Text, txtInvDate.Text.Trim(), txtPayDate.Text.Trim(), txtNotes.Text, UpdatedBy, UpdatedBy, "", "", "", ddlInstallerMtce.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(success.ToString(), txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(success.ToString(), txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv3.InvDoc);
                        string InvDocC = Convert.ToString(success) + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), "");
                    //}
                }
                else
                {
                    ClstblInvoiceCommon.tblInvoiceCommon_Update(stInv3.InvoiceID, "3", ProjectID, txtInvNo.Text, txtInvAmnt.Text, txtInvDate.Text.Trim(), txtPayDate.Text.Trim(), txtNotes.Text, UpdatedBy, "", "", "", ddlInstallerMtce.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(stInv3.InvoiceID, txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(stInv3.InvoiceID, txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv3.InvDoc);
                        string InvDocC = stInv3.InvoiceID + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv3.InvoiceID, InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv3.InvoiceID, stInv3.InvDoc);
                    //}
                }
            }
        }

        if (ddlInvType.SelectedValue == "4")
        {
            SttblInvoiceCommon stInv4 = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType("4", ProjectID);
            if (txtInvNo.Text.Trim() != string.Empty)
            {
                //int exist4 = ClstblInvoiceCommon.tblInvoiceCommon_ExistsByInvType("4", ProjectID);
                //if (exist4 == 0)
                if (hdnGrid.Value == string.Empty)
                {
                    int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert("4", ProjectID, txtInvNo.Text, txtInvAmnt.Text, txtInvDate.Text.Trim(), txtPayDate.Text.Trim(), txtNotes.Text, UpdatedBy, UpdatedBy, "", "", "", ddlInstallerMtce.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(success.ToString(), txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(success.ToString(), txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv4.InvDoc);
                        string InvDocC = Convert.ToString(success) + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), "");
                    //}
                }
                else
                {
                    ClstblInvoiceCommon.tblInvoiceCommon_Update(stInv4.InvoiceID, "4", ProjectID, txtInvNo.Text, txtInvAmnt.Text, txtInvDate.Text.Trim(), txtPayDate.Text.Trim(), txtNotes.Text, UpdatedBy, "", "", "", ddlInstallerMtce.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(stInv4.InvoiceID, txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(stInv4.InvoiceID, txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv4.InvDoc);
                        string InvDocC = stInv4.InvoiceID + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv4.InvoiceID, InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv4.InvoiceID, stInv4.InvDoc);
                    //}
                }
            }
        }

        if (ddlInvType.SelectedValue == "6")
        {
            SttblInvoiceCommon stInv4 = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType("6", ProjectID);
            if (txtInvNo.Text.Trim() != string.Empty)
            {
                //int exist6 = ClstblInvoiceCommon.tblInvoiceCommon_ExistsByInvType("6", ProjectID);
                //if (exist6 == 0)
                if (hdnGrid.Value == string.Empty)
                {
                    int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert("6", ProjectID, txtInvNo.Text, txtInvAmnt.Text, txtInvDate.Text.Trim(), txtPayDate.Text.Trim(), txtNotes.Text, UpdatedBy, UpdatedBy, "", "", "", ddlInstallerMtce.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(success.ToString(), txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(success.ToString(), txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv4.InvDoc);
                        string InvDocC = Convert.ToString(success) + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), "");
                    //}
                }
                else
                {
                    ClstblInvoiceCommon.tblInvoiceCommon_Update(stInv4.InvoiceID, "6", ProjectID, txtInvNo.Text, txtInvAmnt.Text, txtInvDate.Text.Trim(), txtPayDate.Text.Trim(), txtNotes.Text, UpdatedBy, "", "", "", ddlInstallerMtce.SelectedValue);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvance(stInv4.InvoiceID, txtAdvanceAmount.Text, txtAdvanceDate.Text);
                    ClstblInvoiceCommon.tblInvoiceCommon_UpdateAdvanceTotal(stInv4.InvoiceID, txtLessDeductedAmount.Text, txtTotalAmount.Text, ddlAdvType.SelectedValue);
                    if (fuInvDocCom.HasFile)
                    {
                        SiteConfiguration.DeletePDFFile("commoninvdoc", stInv4.InvDoc);
                        string InvDocC = stInv4.InvoiceID + fuInvDocCom.FileName;

                        fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                        bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv4.InvoiceID, InvDocC);
                        SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                        SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                    }
                    //else
                    //{
                    //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(stInv4.InvoiceID, stInv4.InvDoc);
                    //}
                }
            }
        }
        if (ddlInvType.SelectedValue == "5")
        {
            SttblInvoiceCommon stInv4 = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType("5", ProjectID);
            if (txtInvNo.Text.Trim() != string.Empty)
            {
                int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert("5", ProjectID, txtInvNo.Text, txtInvAmnt.Text, txtInvDate.Text.Trim(), txtPayDate.Text.Trim(), txtNotes.Text, UpdatedBy, UpdatedBy, "", "", "", ddlInstallerMtce.SelectedValue);
                if (fuInvDocCom.HasFile)
                {
                    SiteConfiguration.DeletePDFFile("commoninvdoc", stInv4.InvDoc);
                    string InvDocC = Convert.ToString(success) + fuInvDocCom.FileName;

                    fuInvDocCom.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDocC);
                    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), InvDocC);
                    SiteConfiguration.UploadPDFFile("commoninvdoc", InvDocC);
                    SiteConfiguration.deleteimage(InvDocC, "commoninvdoc");
                }
                //else
                //{
                //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), "");
                //}
            }
        }
        ClsProjectSale.tblProjects_UpdateSalesCommPaid(ProjectID, SalesCommPaid);
        /* ---------------------------- Common Invoice End ------------------------- */

        complatestatus();
        BindProjects();
        if (sucElecInv)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
        BindGrid();

        divInstaller.Visible = false;
        divDoorToDoor.Visible = false;
        divInspMtce.Visible = false;
        ResetInvoice();
        hdnGrid.Value = "";
    }
    public void complatestatus()
    {

        string ProjectID = Request.QueryString["proid"];

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        /* --------------------------- Complete ---------------------------- */
        //Response.Write(st.SalesCommPaid + "==" + st.PVDStatusID + "==" + st.paydate);
        //Response.End();
        if (st.paydate != string.Empty && st.PVDStatusID == "7" && st.SalesCommPaid != string.Empty)
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
        }
        if (ddlInvType.SelectedValue != "3")
        {
            if(!string.IsNullOrEmpty(st.InstallCompleted))
            {
                if (st.SalesCommPaid == string.Empty && st.PVDStatusID != "7")
                {
                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "14");
                }
            }
        }
        /* ----------------------------------------------------------------- */
        //Response.Write(st.PVDStatusID + "==" + st.SalesCommPaid + "==" + st.InstallerPayDate);
        //Response.End();
        //if (st.PVDStatusID == "7" && st.SalesCommPaid != string.Empty && st.paydate != string.Empty)
        //{

        //    DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "5");

        //    if (dtStatus.Rows.Count > 0)
        //    {
        //    }
        //    else
        //    {
        //        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, stEmp.EmployeeID, st.NumberPanels);

        //    }
        //    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
        //    //Response.Write(suc.ToString());
        //    //Response.End();
        //}
    }
    protected void txtSalesLandingPrice_TextChanged(object sender, EventArgs e)
    {
        if ((Convert.ToDecimal(txtBaseCostPrice.Text) >= 0) && (Convert.ToDecimal(txtSalesLandingPrice.Text) >= 0))
        {
            if (Convert.ToDecimal((Convert.ToDecimal(txtBaseCostPrice.Text)) - (Convert.ToDecimal(txtSalesMarketPrice.Text))) > 0)
            {
                double amount = (Convert.ToDouble(txtBaseCostPrice.Text) - Convert.ToDouble(txtSalesMarketPrice.Text)) * 0.5;
                decimal amnt = (Convert.ToDecimal(amount)) + (Convert.ToDecimal(txtSalesMarketPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text));
                //txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
                if (amnt > 0)
                {
                    divAdvAmount.Visible = true;
                    divAdvDate.Visible = true;
                }
                else
                {
                    //divAdvAmount.Visible = false;
                    //divAdvDate.Visible = false;
                }

                DataTable dt = ClstblInvoiceCommon.tblInvoiceCommon_SelectAdvance(Request.QueryString["proid"]);
                if (dt.Rows[0]["AdvanceAmount"].ToString() != string.Empty)
                {
                    if (Convert.ToDecimal(dt.Rows[0]["AdvanceAmount"].ToString()) > 0)
                    {
                        decimal advance = Convert.ToDecimal(dt.Rows[0]["AdvanceAmount"].ToString());
                        //  txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(Convert.ToDecimal(SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt))) - (advance)));
                    }
                }
            }
            else
            {
                decimal amnt = Convert.ToDecimal(txtBaseCostPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text);
                //txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
                if (amnt > 0)
                {
                    divAdvAmount.Visible = true;
                    divAdvDate.Visible = true;
                }
                else
                {
                    //divAdvAmount.Visible = false;
                    //divAdvDate.Visible = false;
                }
                DataTable dt = ClstblInvoiceCommon.tblInvoiceCommon_SelectAdvance(Request.QueryString["proid"]);
                if (dt.Rows[0]["AdvanceAmount"].ToString() != string.Empty)
                {
                    if (Convert.ToDecimal(dt.Rows[0]["AdvanceAmount"].ToString()) > 0)
                    {
                        decimal advance = Convert.ToDecimal(dt.Rows[0]["AdvanceAmount"].ToString());
                        //   txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(Convert.ToDecimal(SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt))) - (advance)));
                    }
                }
            }
        }
    }
    public void BindInvoice()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        txtSalesInvNo.Text = st2.SalesInvNo;
    }
    //protected void btnCreateTaxInvoice_Click(object sender, EventArgs e)
    //{
    //    string ProjectID = Request.QueryString["proid"];
    //    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    ddlInvType.SelectedValue = "2";
    //    if (st.ManualQuoteNumber != string.Empty)
    //    {
    //        bool suc = ClstblProjects.tblProjects2_UpdateSalesInvNo(ProjectID, st.ManualQuoteNumber);
    //        if (suc)
    //        {
    //            btnCreateTaxInvoice.Visible = false;
    //            btnOpenTaxInvoice.Visible = true;
    //        }
    //        else
    //        {
    //            btnCreateTaxInvoice.Visible = true;
    //            btnOpenTaxInvoice.Visible = false;
    //        }
    //        divNoMQN.Visible = false;
    //    }
    //    else
    //    {
    //        divNoMQN.Visible = true;
    //    }
    //    BindInvoice();
    //    BindProjectElecInv();
    //}
    //protected void btnOpenTaxInvoice_Click(object sender, EventArgs e)
    //{
    //    string ProjectID = Request.QueryString["proid"];

    //    TextWriter txtWriter = new StringWriter() as TextWriter;
    //    Server.Execute("~/mailtemplate/taxinvoice.aspx?id=" + ProjectID, txtWriter);
    //    String htmlText = txtWriter.ToString();
    //    HTMLExportToPDF(htmlText, ProjectID + "TaxInvoice.pdf");

    //    BindProjectElecInv();
    //}

    public void HTMLExportToPDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdfheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    string HeaderHtml = txtWriter.ToString();
        //    string FooterHtml = txtWriter2.ToString();

        //    //client.setHeaderHtml(HeaderHtml);
        //    //client.setFooterHtml(FooterHtml);
        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }
    public void ResetInvoice()
    {
        ddlInvType.SelectedValue = "";

        txtInstallerInvNo.Text = string.Empty;
        txtInstallerInvDate.Text = string.Empty;
        txtInstallerPayDate.Text = string.Empty;
        ddlApprovedBy.SelectedValue = "";
        txtAmount.Text = string.Empty;
        txtElectricianInvoiceNotes.Text = string.Empty;
        txtExtraWork.Text = string.Empty;
        ddlInstaller.SelectedValue = "";

        txtSalesMarketPrice.Text = string.Empty;
        txtSalesLandingPrice.Text = string.Empty;
        txtSalesInvAmnt.Text = string.Empty;
        txtSalesInvNo.Text = string.Empty;
        txtSalesInvDate.Text = string.Empty;
        txtSalesPayNotes.Text = string.Empty;
        txtSalesPayDate.Text = string.Empty;

        txtInvNo.Text = string.Empty;
        txtInvAmnt.Text = string.Empty;
        txtInvDate.Text = string.Empty;
        txtPayDate.Text = string.Empty;
        txtNotes.Text = string.Empty;
        ddlInstallerMtce.SelectedValue = "";
    }
    protected void ddlInvType_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlInvType.SelectedValue != "")
        {

            #region

            //if (ddlInvType.SelectedValue == "1")
            //{
            //    divInstaller.Visible = true;
            //}
            //else
            //{
            //    txtInstallerInvNo.Text = string.Empty;
            //    txtInstallerInvDate.Text = string.Empty;
            //    txtInstallerPayDate.Text = string.Empty;
            //    ddlApprovedBy.SelectedValue = "";
            //    txtAmount.Text = string.Empty;
            //    txtElectricianInvoiceNotes.Text = string.Empty;
            //    txtExtraWork.Text = string.Empty;
            //    ddlInstaller.SelectedValue = "";

            //    divInstaller.Visible = false;
            //}
            //if (ddlInvType.SelectedValue == "2" || ddlInvType.SelectedValue == "7" || ddlInvType.SelectedValue == "8")
            //{
            //    divDoorToDoor.Visible = true;
            //    SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);
            //    if (st2.SalesInvNo != string.Empty)
            //    {
            //        try
            //        {
            //            txtSalesLandingPrice.Text = SiteConfiguration.ChangeCurrencyVal(st2.SalesLandingPrice);
            //        }
            //        catch { }
            //        try
            //        {
            //            txtSalesMarketPrice.Text = SiteConfiguration.ChangeCurrencyVal(st2.SalesMarketPrice);
            //        }
            //        catch { }
            //        try
            //        {
            //            txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(st2.SalesInvAmnt);
            //        }
            //        catch { }
            //        txtSalesInvNo.Text = st2.SalesInvNo;

            //        if (st2.SalesInvAmnt != string.Empty)
            //        {
            //            divAdvAmount.Visible = true;
            //            divAdvDate.Visible = true;
            //        }
            //        DataTable dtAdv = ClstblInvoiceCommon.tblInvoiceCommon_SelectAdvance(Request.QueryString["proid"]);
            //        if (dtAdv.Rows[0]["AdvanceAmount"].ToString() != string.Empty)
            //        {
            //            lblAdvanceAmount.Text = SiteConfiguration.ChangeCurrencyVal(dtAdv.Rows[0]["AdvanceAmount"].ToString());
            //            CompareValidatorAdvance.ValueToCompare = Convert.ToString(Convert.ToDecimal(st2.SalesInvAmnt) - Convert.ToDecimal(lblAdvanceAmount.Text));
            //        }
            //    }
            //}
            //else
            //{
            //    //txtBaseCostPrice.Text = string.Empty;
            //    txtSalesMarketPrice.Text = string.Empty;
            //    txtSalesLandingPrice.Text = string.Empty;
            //    txtSalesInvAmnt.Text = string.Empty;
            //    txtSalesInvNo.Text = string.Empty;
            //    txtSalesInvDate.Text = string.Empty;
            //    txtSalesPayNotes.Text = string.Empty;
            //    txtSalesPayDate.Text = string.Empty;
            //    txtAdvanceAmount.Text = string.Empty;
            //    txtAdvanceDate.Text = string.Empty;

            //    divDoorToDoor.Visible = false;
            //}
            //if (ddlInvType.SelectedValue == "3" || ddlInvType.SelectedValue == "4" || ddlInvType.SelectedValue == "6")
            //{
            //    divInspMtce.Visible = true;
            //}
            //else
            //{
            //    txtInvNo.Text = string.Empty;
            //    txtInvAmnt.Text = string.Empty;
            //    txtInvDate.Text = string.Empty;
            //    txtPayDate.Text = string.Empty;
            //    txtNotes.Text = string.Empty;
            //    ddlInstallerMtce.SelectedValue = "";

            //    divInspMtce.Visible = false;
            //}

            #endregion
            divInspMtce.Visible = true;
            divAdvAmount.Visible = true;
            divAdvDate.Visible = true;
            //if (ddlInvType.SelectedValue == "5")
            //{
            //    lbl.Text = "dhara";
            //}
        }
        else
        {
            divInstaller.Visible = false;
            divDoorToDoor.Visible = false;
            divInspMtce.Visible = false;
        }

        if (ddlInvType.SelectedValue == "1011")
        {
            divpanelcost.Visible = true;
            divflatjob.Visible = true;
            divpanelno.Visible = true;
        }
        else
        {
            divpanelcost.Visible = false;
            divflatjob.Visible = false;
            divpanelno.Visible = false;
        }

        txtAdvanceAmount.Text = string.Empty;
        txtAdvanceDate.Text = string.Empty;
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hypdocument = (HyperLink)e.Row.FindControl("hypdocument");
            HiddenField hndInvoiceID = (HiddenField)e.Row.FindControl("hndInvoiceID");

            Image imgDwn = (Image)e.Row.FindControl("imgDwn");
            SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvoiceID(hndInvoiceID.Value);
            HiddenField hndadvamt = (HiddenField)e.Row.FindControl("hndadvamt");
            HiddenField hndadvancedate = (HiddenField)e.Row.FindControl("hndadvancedate");
            HiddenField hndInvType = (HiddenField)e.Row.FindControl("hndInvType");
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");


            if (Roles.IsUserInRole("Administrator"))
            {
                gvbtnDelete.Visible = true;
            }
            else
            {
                if (hndInvType.Value.ToString() == "2" && hndInvoiceID.Value != string.Empty && hndadvancedate.Value != string.Empty)
                {
                    gvbtnDelete.Visible = false;
                }
            }
            //hypdocument.NavigateUrl = pdfURL + "commoninvdoc/" + st.InvDoc;
            hypdocument.NavigateUrl = SiteConfiguration.GetDocumnetPath("commoninvdoc", st.InvDoc);
            imgDwn.ImageUrl = "~/images/icon_dwn.jpg";
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        hdnGrid.Value = id;

        SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvoiceID(id);
        divAdvAmount.Visible = true;
        divAdvDate.Visible = true;
        divInspMtce.Visible = true;
        ddlInvType.SelectedValue = st.InvType;
        if (ddlInvType.SelectedValue == "1011")
        {
            divpanelcost.Visible = true;
            divflatjob.Visible = true;
            divpanelno.Visible = true;
        }
        else
        {
            divpanelcost.Visible = false;
            divflatjob.Visible = false;
            divpanelno.Visible = false;
        }
        string InvType = st.InvType;
        //if (InvType == "1")
        //{
        divInstaller.Visible = true;
        try
        {
            txtInstallerAmnt.Text = SiteConfiguration.ChangeCurrencyVal(st.InvAmnt);
        }
        catch { }
        try
        {
            txtInstallerInvDate.Text = Convert.ToDateTime(st.InvDate).ToShortDateString();
        }
        catch { }
        try
        {
            txtInstallerPayDate.Text = Convert.ToDateTime(st.PayDate).ToShortDateString();
        }
        catch { }
        try
        {
            txtAmount.Text = SiteConfiguration.ChangeCurrencyVal(st.ExtraAmount);
        }
        catch { }

        txtInstallerInvNo.Text = st.InvNo;
        try
        {
            ddlApprovedBy.SelectedValue = st.ApprovedBy;
        }
        catch { }

        txtElectricianInvoiceNotes.Text = st.Notes;
        txtExtraWork.Text = st.ExtraWork;
        try
        {
            ddlInstaller.SelectedValue = st.Installer;
        }
        catch { }
        if (st.InvDoc != string.Empty)
        {

            lblInvDoc.Visible = true;
            lblInvDoc.Text = st.InvDoc;
            //lblInvDoc.NavigateUrl = "~/userfiles/commoninvdoc/" + st.InvDoc;
            //lblInvDoc.NavigateUrl = pdfURL + "commoninvdoc/" + st.InvDoc;
            lblInvDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("commoninvdoc", st.InvDoc);
        }
        //}
        //else
        //{
        //    divInstaller.Visible = false;
        //}
        //if (InvType == "2")
        //{
        //divDoorToDoor.Visible = true;

        try
        {
            txtSalesLandingPrice.Text = SiteConfiguration.ChangeCurrencyVal(st.LandingPrice);
            txtSalesLandingPrice_TextChanged(sender, e);
        }
        catch { }
        try
        {
            txtSalesMarketPrice.Text = SiteConfiguration.ChangeCurrencyVal(st.MarketPrice);
        }
        catch { }
        try
        {
            txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(st.InvAmnt);
        }
        catch { }

        txtSalesInvNo.Text = st.InvNo;
        try
        {
            txtSalesInvDate.Text = Convert.ToDateTime(st.InvDate).ToShortDateString();
        }
        catch { }

        try
        {
            txtAdvanceAmount.Text = SiteConfiguration.ChangeCurrencyVal(st.AdvanceAmount);
        }
        catch { }
        try
        {
            txtLessDeductedAmount.Text = SiteConfiguration.ChangeCurrencyVal(st.AdvanceLessAmount);

        }
        catch { }
        try
        {
            txtTotalAmount.Text = SiteConfiguration.ChangeCurrencyVal(st.AdvanceTotalAmount);
        }
        catch { }
        try
        {
            ddlAdvType.SelectedValue = (st.AdvanceType);
        }
        catch { }
        try
        {
            txtAdvanceDate.Text = Convert.ToDateTime(st.AdvanceDate).ToShortDateString();
        }
        catch { }
        txtSalesPayNotes.Text = st.Notes;
        try
        {
            txtSalesPayDate.Text = Convert.ToDateTime(st.PayDate).ToShortDateString();
        }
        catch { }

        if (st.InvDoc != string.Empty)
        {


            lblInvDocDoor.Visible = true;
            lblInvDocDoor.Text = st.InvDoc;
            //lblInvDocDoor.NavigateUrl = "~/userfiles/commoninvdoc/" + st.InvDoc;
            //lblInvDocDoor.NavigateUrl = pdfURL + "commoninvdoc/" + st.InvDoc;
            lblInvDocDoor.NavigateUrl = SiteConfiguration.GetDocumnetPath("commoninvdoc", st.InvDoc);
        }
        //}
        //else
        //{
        //    divDoorToDoor.Visible = false;
        //}
        //if (InvType == "3" || InvType == "4")
        //{
        //  divInspMtce.Visible = true;

        txtInvNo.Text = st.InvNo;
        try
        {
            txtInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(st.InvAmnt);
        }
        catch { }
        try
        {
            txtInvDate.Text = Convert.ToDateTime(st.InvDate).ToShortDateString();
        }
        catch { }
        try
        {
            txtPayDate.Text = Convert.ToDateTime(st.PayDate).ToShortDateString();
        }
        catch { }
        txtNotes.Text = st.Notes;
        try
        {
            ddlInstallerMtce.SelectedValue = st.Installer;
        }
        catch { }
        if (st.InvDoc != string.Empty)
        {
            lblInvDocCom.Visible = true;
            lblInvDocCom.Text = st.InvDoc;
            //lblInvDocCom.NavigateUrl = "~/userfiles/commoninvdoc/" + st.InvDoc;
            //lblInvDocCom.NavigateUrl = pdfURL + "commoninvdoc/" + st.InvDoc;
            lblInvDocCom.NavigateUrl = SiteConfiguration.GetDocumnetPath("commoninvdoc", st.InvDoc);
        }

        //}
        //else
        //{
        //    divInspMtce.Visible = false;
        //}


    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid();

    }

    protected void txtSalesMarketPrice_TextChanged(object sender, EventArgs e)
    {
        //if ((Convert.ToDecimal(txtBaseCostPrice.Text) >= 0) && (Convert.ToDecimal(txtSalesLandingPrice.Text) >= 0))
        //{
        //    if (Convert.ToDecimal((Convert.ToDecimal(txtBaseCostPrice.Text)) - (Convert.ToDecimal(txtSalesMarketPrice.Text))) > 0)
        //    {
        //        double amount = (Convert.ToDouble(txtBaseCostPrice.Text) - Convert.ToDouble(txtSalesMarketPrice.Text)) * 0.5;
        //        decimal amnt = (Convert.ToDecimal(amount)) + (Convert.ToDecimal(txtSalesMarketPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text));
        //        txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
        //    }
        //    else
        //    {
        //        decimal amnt = Convert.ToDecimal(txtBaseCostPrice.Text) - Convert.ToDecimal(txtSalesLandingPrice.Text);
        //        txtSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(amnt));
        //    }
        //}
    }
    protected void txtAdvanceAmount_TextChanged(object sender, EventArgs e)
    {
        if (txtAdvanceAmount.Text != string.Empty)
        {
            divInspMtce.Visible = true;
            BindAdvAmount();
        }
    }
    protected void txtLessDeductedAmount_TextChanged(object sender, EventArgs e)
    {
        if (txtLessDeductedAmount.Text != string.Empty && Convert.ToInt32(txtLessDeductedAmount.Text) > 0)
        {
            divInspMtce.Visible = true;

            hdndeductamount.Value = txtLessDeductedAmount.Text;
            rangelessamount.MaximumValue = "1000000000000000";
            if (hdnamountsum.Value != string.Empty)
            {
                //Response.Write(hdnamountsum.Value);
                //Response.End();
                rangelessamount.MinimumValue = Convert.ToString(hdnamountsum.Value);
                rangelessamount.ErrorMessage = "Amount Should Be Greater Than" + hdnamountsum.Value + ".";
            }
            else
            {
                rangelessamount.MinimumValue = "0";
            }
            if (ddlInvType.SelectedValue == "2")
            {
                ModalPopupExtenderprojectdetail.Show();
                txtdeductamount.Text = txtLessDeductedAmount.Text;

                bind_grid();
            }

            BindAdvAmount();
        }
    }
    public void BindAdvAmount()
    {
        decimal total = 0;
        if (txtAdvanceAmount.Text != string.Empty && txtAdvanceDate.Text != string.Empty)
        {
            total = (Convert.ToDecimal(txtInvAmnt.Text)) + (Convert.ToDecimal(txtAdvanceAmount.Text) - Convert.ToDecimal(txtLessDeductedAmount.Text));
        }

        txtTotalAmount.Text = total.ToString();
    }

    protected void lnkprodetail_Click(object sender, EventArgs e)
    {

        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);// hdnGrid.Value
        txtprojno.Text = st2.ProjectNumber;
    }
    protected void btnsavedetail_Click(object sender, EventArgs e)
    {
        //Response.Write(hdnGrid.Value);
        //Response.End();0;-insert and 1;-update

        if (hdnGrid.Value != string.Empty)
        {
            SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvoiceID(hdnGrid.Value);
            if (hdnproamount.Value == string.Empty)
            {
                hdnproamount.Value = "0";
            }
            int exist = ClstblInvoiceCommon.tbl_projectamount_ExistsById(hdnproamount.Value);
            if (exist == 0)
            {
                int succ = ClstblInvoiceCommon.tbl_projectamount_Insert(txtprojno.Text, Request.QueryString["proid"], ddlInvType.SelectedValue, hdnGrid.Value, txtproamount.Text);
            }
            else
            {
                bool succ = ClstblInvoiceCommon.tbl_projectamount_Update(hdnproamount.Value, txtprojno.Text, Request.QueryString["proid"], ddlInvType.SelectedValue, hdnGrid.Value, txtproamount.Text);
            }
        }

        txtprojno.Text = string.Empty;
        txtproamount.Text = string.Empty;

        ModalPopupExtenderprojectdetail.Show();
        bind_grid();
    }
    public void bind_grid()
    {
        //Response.Write(hdndeductamount.Value);
        //Response.End();
        DataTable dt = ClstblInvoiceCommon.tbl_projectamount_Select(Request.QueryString["proid"]);
        rptName.DataSource = dt;
        rptName.DataBind();
        decimal amount = 0;
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                amount += Convert.ToDecimal(dt.Rows[i]["amount"].ToString());
            }

            hdnamountsum.Value = amount.ToString();
        }
        decimal amount2 = 0;
        if (hdndeductamount.Value != string.Empty)
        {
            amount2 = Convert.ToDecimal(hdndeductamount.Value) - amount;
        }

        hdntotalamount.Value = amount.ToString();
        cmpNumbers.MinimumValue = "0";
        if (amount.ToString() != "0")
        {

            cmpNumbers.MaximumValue = Convert.ToString(amount2);
        }
        else
        {
            cmpNumbers.MaximumValue = txtdeductamount.Text;
        }
    }
    protected void rptName_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        ModalPopupExtenderprojectdetail.Show();
        string id = rptName.DataKeys[e.NewSelectedIndex].Value.ToString();
        hdnproamount.Value = id;
        Sttbl_projectamount st = ClstblInvoiceCommon.tbl_projectamount_SelectByInvoiceID(id);
        if (st.projectno != string.Empty)
        {
            txtprojno.Text = st.projectno;
        }
        if (st.amount != string.Empty)
        {
            txtproamount.Text = st.amount;
        }
    }
    protected void rptName_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = rptName.DataKeys[e.RowIndex].Value.ToString();
        hdndelete1.Value = id;
        ModalPopupExtenderDelete.Show();
        bind_grid();

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        bool sucess1 = false;
        bool suc_ess1 = false;

        string id = string.Empty;
        if (hdndelete.Value.ToString() != string.Empty)
        {
            id = hdndelete.Value.ToString();

            SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvoiceID(id);
            //ClsProjectSale.tblProjects2_UpdateElecInvTeamC(st.ProjectID, txtSalesLandingPrice.Text, txtSalesMarketPrice.Text, txtSalesInvAmnt.Text, txtSalesInvNo.Text, txtSalesInvDate.Text, txtSalesPayNotes.Text, txtSalesPayDate.Text, txtUserCommision, EuroCommision);
            ClsProjectSale.tblProjects2_UpdateElecInvTeamC(st.ProjectID, "", "", "", "", "", "", "", "", "");
            sucess1 = ClstblInvoiceCommon.tblInvoiceCommon_Delete(id);
            suc_ess1 = ClstblInvoiceCommon.tbl_projectamount_Delete(id);

        }
        bool suc_ess2 = false;
        if (hdndelete1.Value.ToString() != string.Empty)
        {
            id = hdndelete1.Value.ToString();


            suc_ess2 = ClstblInvoiceCommon.tbl_projectamount_Delete_row(id);
        }

        //--- do not chage this code start------
        if (suc_ess1 || suc_ess2 || sucess1)
        {
            SetAdd1();
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
        //--- do not chage this code end------ 



        GridView1.EditIndex = -1;
        BindGrid();
        bind_grid();
        BindProjectElecInv();

        ModalPopupExtenderprojectdetail.Show();


    }


}