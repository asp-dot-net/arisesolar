﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;
using System.Data;

public partial class includes_controls_projectstc : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            
            if (Roles.IsUserInRole("SalesRep"))
            {
                if (st2.DepositReceived != string.Empty)
                {
                    btnUpdateSTC.Visible = false;
                }
            }

        }
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();

    }
    public void GetSTCByProject(string proid)
    {
        HiddenField2.Value = proid;
        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("STC")))
        {
            PanAddUpdate.Enabled = true;
        }
        else
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Finance"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("InstallationManager"))
        {
            PanAddUpdate.Enabled = true;
        }

        BindDropDown();
        if (!string.IsNullOrEmpty(HiddenField2.Value))
        {
            string ProjectID = HiddenField2.Value;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            if ((Roles.IsUserInRole("Sales Manager")))
            {

                PanAddUpdate.Enabled = false;
            }
            try
            {
                txtSTCUploaded.Text = Convert.ToDateTime(st.STCUploaded).ToShortDateString();
            }
            catch { }

            try
            {
                txtSTCApplied.Text = Convert.ToDateTime(st.STCApplied).ToShortDateString();
            }
            catch { }

            txtSTCUploadNumber.Text = st.STCUploadNumber;
            txtPVDNumber.Text = st.PVDNumber;
            ddlPVDStatusID.SelectedValue = st.PVDStatusID;
            txtformbayid.Text = st.FormbayId;
            txtSTCNotes.Text = st.STCNotes;
           //ClientScriptManager.admin_adminfiles_company_stctracker .
            //var page = Parent as ;
            //page.GetLoginTime();
            //if (Page is stctracker.aspx)
            //{
            //    (Page as TestAspx).GetSomething();
            //} 
        }
    }
    public void BindProjectSTC()
    {
        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("STC")))
        {
            PanAddUpdate.Enabled = true;
        }
        else
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Finance"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("InstallationManager"))
        {
            PanAddUpdate.Enabled = true;
        }

        BindDropDown();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            if ((Roles.IsUserInRole("Sales Manager")))
            {

                PanAddUpdate.Enabled = false;
            }
            try
            {
                txtSTCUploaded.Text = Convert.ToDateTime(st.STCUploaded).ToShortDateString();
            }
            catch { }

            try
            {
                txtSTCApplied.Text = Convert.ToDateTime(st.STCApplied).ToShortDateString();
            }
            catch { }

            txtSTCUploadNumber.Text = st.STCUploadNumber;
            txtPVDNumber.Text = st.PVDNumber;
            ddlPVDStatusID.SelectedValue = st.PVDStatusID;
            txtformbayid.Text = st.FormbayId;
            txtSTCNotes.Text = st.STCNotes;
        }
    }

    public void BindDropDown()
    {
        System.Web.UI.WebControls.ListItem item3 = new System.Web.UI.WebControls.ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlPVDStatusID.Items.Clear();
        ddlPVDStatusID.Items.Add(item3);

        ddlPVDStatusID.DataSource = ClsProjectSale.tblPVDStatus_SelectActive();
        ddlPVDStatusID.DataValueField = "PVDStatusID";
        ddlPVDStatusID.DataMember = "PVDStatus";
        ddlPVDStatusID.DataTextField = "PVDStatus";
        ddlPVDStatusID.DataBind();
    }
    protected void btnUpdateSTC_Click(object sender, EventArgs e)
    {
        string ProjectID = string.Empty;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
           
             ProjectID = Request.QueryString["proid"];
        }
        else{
           
            ProjectID=HiddenField2.Value;
         
        }

      
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string STCUploaded = txtSTCUploaded.Text.Trim();
        string STCApplied = txtSTCApplied.Text.Trim();
        string STCUploadNumber = txtSTCUploadNumber.Text;
        string PVDNumber = txtPVDNumber.Text;
        string PVDStatusID = ddlPVDStatusID.SelectedValue;
        string formbayid = txtformbayid.Text;
        string STCNotes = txtSTCNotes.Text;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        bool sucForms = ClsProjectSale.tblProjects_UpdateSTC(ProjectID, STCUploaded, STCApplied, STCUploadNumber, PVDNumber, PVDStatusID, UpdatedBy);
        bool sucstcnotes = ClsProjectSale.tblProjects_UpdateSTCNotes(ProjectID, STCNotes);
        bool sucForms1 = ClsProjectSale.tblProjects_UpdateSTC_formbayid(ProjectID, formbayid);

        /* -------------------- STC / Invoice Paid -------------------- */

        if (txtSTCApplied.Text.Trim() != string.Empty)
        {
            if (st.InvoiceFU != string.Empty)
            {
                DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "14");
                if (dtStatus.Rows.Count > 0)
                {
                }
                else
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("14", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                }
                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "14");
            }
        }
        /* ------------------------------------------------------------ */
        /* ---------------------- pvdapproved ---------------------- */
        if (ddlPVDStatusID.SelectedValue == "7")
        {
             DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "19");
             if (dtStatus.Rows.Count > 0)
             {
             }
             else
             {
                 int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("19", ProjectID, stEmp.EmployeeID, st.NumberPanels);
             }
             ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "19");
        }
        /* ------------------------------------------------------------ */
        /* ---------------------- Complete ---------------------- */
        if (ddlPVDStatusID.SelectedValue == "7" && st.SalesCommPaid != string.Empty && st.paydate != string.Empty)
        {
            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "5");
            if (dtStatus.Rows.Count > 0)
            {
            }
            else
            {
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            }
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
        }
        /* ------------------------------------------------------ */
        /* ---------------------- paerworkrec back ---------------------- */
        if (txtSTCApplied.Text.Trim() == string.Empty && st.InvoiceFU == string.Empty)
        {
                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "13");
        }
        /* ------------------------------------------------------ */

        BindProjects();
        if (sucForms)
        {
           
            SetAdd1();
            
            //PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}