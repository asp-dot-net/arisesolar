<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectmtce.ascx.cs" Inherits="includes_controls_projectmtce" %>
<%@ Register Src="printmtcedetail.ascx" TagName="printmtcedetail" TagPrefix="uc1" %>
<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }
</style>

<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedMTCE);
    prm.add_beginRequest(beginrequesthandler);
    // raised after an asynchronous postback is finished and control has been returned to the browser.
    prm.add_endRequest(endrequesthandler);
    function beginrequesthandler(sender, args) {
        //shows the modal popup - the update progress
        //$('.loading-container').css('display', 'block');
    }
    function endrequesthandler(sender, args) {
        //hide the modal popup - the update progress
        // $('.loading-container').css('display', 'none');
    }
    function pageLoadedMTCE() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        //  $('.loading-container').css('display', 'none');
        $(".myvalMTCE").select2({
            //placeholder: "select",
            allowclear: true
        });
        //$('.i-checks').iCheck({
        //    checkboxClass: 'icheckbox_square-green',
        //    radioClass: 'iradio_square-green'
        //});
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //HighlightControlToValidate();

    });

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadeddetail);
    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
    prm.add_beginRequest(beginrequesthandler);
    // raised after an asynchronous postback is finished and control has been returned to the browser.
    prm.add_endRequest(endrequesthandler);

    function beginrequesthandler(sender, args) {
        //shows the modal popup - the update progress

    }
    function endrequesthandler(sender, args) {
        //hide the modal popup - the update progress

        //if (args.get_error() != undefined) {
        //    args.set_errorhandled(true);
        //}

    }
    function pageLoadeddetail() {
        $('#<%=btnUpdate.ClientID %>').click(function () {
            formValidate();
        });


        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });

    }

    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "red");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
<%--<script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>--%>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>


        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedMTCE);
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);
            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                //$('.loading-container').css('display', 'block');
            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
                // $('.loading-container').css('display', 'none');
            }
            function pageLoadedMTCE() {
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                //  $('.loading-container').css('display', 'none');
                $(".myvalMTCE").select2({
                    //placeholder: "select",
                    allowclear: true
                });

                //$('.i-checks').iCheck({
                //    checkboxClass: 'icheckbox_square-green',
                //    radioClass: 'iradio_square-green'
                //});
            }
        </script>

        <script>
            function ComfirmDelete(event, ctl) {
                event.preventDefault();
                var defaultAction = $(ctl).prop("href");

                swal({
                    title: "Are you sure you want to delete this Record?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },

                    function (isConfirm) {
                        if (isConfirm) {
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            eval(defaultAction);

                            //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            return true;
                        } else {
                            // swal("Cancelled", "Your imaginary file is safe :)", "error");
                            return false;
                        }
                    });
            }
        </script>
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea stcpage">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false"><i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong></div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong> </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false"><i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong> </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong> </div>
                        <div class="alert alert-danger" id="divActive" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Your Project is not Active.</strong></div>
                    </div>

                    <div class="clear">&nbsp;</div>

                    <div class="contacttoparea">
                        <div class="form-inline">
                            <div class="paddall15 searchfinal Responsive-search" style="margin-bottom: 25px;">
                                <div class="row">
                                    <div class="col-md-2">
                                        <table border="0" cellspacing="0" cellpadding="0" class="showdata">
                                            <tr>
                                                <td>


                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="addcontent" id="divrightbtn" runat="server" style="display: inline-block; float: right;">
                                            <asp:Label ID="hdnmtceid" runat="server" Visible="false"></asp:Label>
                                            <asp:LinkButton ID="lnkmaintainance" runat="server" Visible="false" OnClick="lnkmaintainance_Click" CssClass="martopzero" CausesValidation="false">
                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/admin/images/btn_maintainance.jpg" />
                                            </asp:LinkButton>
                                            </span> 
                                            <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" class="addcontact" CssClass="btn btn-default purple" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i>Add</asp:LinkButton>
                                            <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" OnClick="lnkBack_Click" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="widget flat radius-bordered borderone">
                            <div class="widget-header bordered-bottom bordered-blue">
                                <span class="widget-caption">Maintenance Call Details</span>
                            </div>
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group wdth230">
                                            <span class="name">
                                                <asp:Label ID="Label23" runat="server" class="control-label">
                                              Call Date</asp:Label></span>
                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtOpenDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator15" Controltovalidate="txtOpenDate" Display="Dynamic" errormessage="" ValidationGroup="req" />--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtOpenDate" Display="Dynamic" ValidationGroup="req"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <%--   <div class="form-group dateimgarea">
                                        <span class="name">
                                            <label class="control-label">Call Date </label>
                                        </span><span class="dateimg">
                                            <asp:TextBox ID="txtOpenDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                            <asp:ImageButton ID="Image3" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image3"
                                                TargetControlID="txtOpenDate" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                            <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtOpenDate" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                        </span>
                                        <div class="clear"></div>
                                    </div>--%>

                                        <div class="form-group">
                                            <span class="name">
                                                <asp:Label ID="Label19" runat="server" class="name disblock control-label">
                                                Call Reason:</asp:Label></span>
                                            <div class="firstselect row">
                                                <div class="col-md-6">
                                                    <div class="wdth230">
                                                        <asp:DropDownList ID="ddlProjectMtceReasonID" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalMTCE">
                                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>




                                                <div class="col-md-6">
                                                    <div class="wdth230">
                                                        <asp:DropDownList ID="ddlProjectMtceReasonSubID" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalMTCE">
                                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlProjectMtceReasonSubID" Display="Dynamic" ValidationGroup="req"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="* Required" CssClass="reqerror"
                                                            ControlToValidate="ddlLocation" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>

                                        <%--<div class="form-group row">
                                                <span class="name disblock col-md-12">
                                                    <label class="control-label">Call Reason: </label>
                                                </span><span><span class="firstselect col-md-6">
                                                    <asp:DropDownList ID="ddlProjectMtceReasonID" runat="server" CssClass="form-control search-select"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select </asp:ListItem>
                                                    </asp:DropDownList>
                                                </span><span class="secondselect col-md-6">
                                                    <asp:DropDownList ID="ddlProjectMtceReasonSubID" runat="server" CssClass="form-control search-select"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                </span></span>
                                                <div class="clear"></div>
                                            </div>--%>
                                        <div class="row">
                                            <div class="form-group spicalcontact col-md-6">
                                                <span class="name">
                                                    <label class="control-label">Customer: </label>
                                                </span><span class="paddtop5 valuebox wdth230">
                                                    <asp:Label ID="lblCustomer" Width="200px" runat="server"></asp:Label>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group spicalcontact col-md-6">
                                                <span class="name">
                                                    <label class="control-label">Project : </label>
                                                </span><span class="paddtop5 valuebox wdth230">
                                                    <asp:Label ID="lblProject" runat="server"></asp:Label>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group dateimgarea checkboxmain3">
                                            <br />
                                            <span class="name paddtop9none">
                                                <label for="<%=chkWarranty.ClientID %>">
                                                    <asp:CheckBox ID="chkWarranty" runat="server" />
                                                    <span class="text">&nbsp;Warranty</span>
                                                </label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>


                                        <div class="form-group wdth230" style="padding-top: 12px;">
                                            <span class="name">
                                                <label class="control-label">Employee </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlEmployee" runat="server" Width="200px" AppendDataBoundItems="true"
                                                    CssClass="myvalMTCE">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <span class="name">
                                                    <label class="control-label">Contact : </label>
                                                </span><span class="paddtop5 valuebox wdth230">
                                                    <asp:Label ID="lblContact" runat="server"></asp:Label>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group spicalcontact col-md-6">
                                                <span class="name">
                                                    <label class="control-label">Mobile </label>
                                                </span><span class="paddtop5 valuebox wdth230">
                                                    <asp:Label ID="lblMobile" runat="server"></asp:Label>
                                                </span>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group  ">

                                            <span class="name">
                                                <label class="control-label">New</label>
                                            </span><span>
                                                <asp:TextBox runat="server" ID="txtCurrentPhoneContact" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="widget flat radius-bordered borderone">
                            <div class="widget-header bordered-bottom bordered-blue">
                                <span class="widget-caption">Installation Details</span>
                            </div>
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">Sales Rep </label>
                                            </span><span class="paddtop9 valuebox wdth230">
                                                <asp:Label ID="lblSalesRep" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">Install Date </label>
                                            </span><span class="paddtop9 valuebox wdth230">
                                                <asp:Label ID="lblInstallDate" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">Installer </label>
                                            </span><span class="paddtop9 valuebox wdth230">
                                                <asp:Label ID="lblInstaller" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">Meter Elec </label>
                                            </span><span class="paddtop9 valuebox wdth230">
                                                <asp:Label ID="lblMeterElec" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">System </label>
                                            </span><span class="paddtop9 valuebox">
                                                <asp:Label ID="lblSystem" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="widget flat radius-bordered borderone">
                            <div class="widget-header bordered-bottom bordered-blue">
                                <span class="widget-caption">Work Details</span>
                            </div>
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">Customer's Description of Work to be Done </label>
                                            </span><span class="fullspan">
                                                <asp:TextBox ID="txtCustomerInput" runat="server" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">Description of Fault Identified </label>
                                            </span><span class="fullspan">
                                                <asp:TextBox ID="txtFaultIdentified" runat="server" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">Description of Actions Required to Fix </label>
                                            </span><span class="fullspan">
                                                <asp:TextBox ID="txtActionRequired" runat="server" CssClass="form-control pull-left" Width="405px"></asp:TextBox>
                                                <asp:Button ID="btnCopy" class="btn btn-dark-grey marleft10" runat="server" CausesValidation="false" OnClick="btnCopy_Click" Text="Copy" />
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Tech Assigned to this Job </label>
                                            </span><span class="fullhaft">
                                                <asp:DropDownList ID="ddlInstallerAssigned" runat="server" CssClass="myvalMTCE"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select </asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">Call Method </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlProjectMtceCallID" runat="server" CssClass="myvalMTCE"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value=""> Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group" style="width: 230px">
                                            <span class="name">
                                                <asp:Label ID="Label1" runat="server" class="name disblock  control-label">
                                                Prop Fix Date</asp:Label></span>
                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtPropResolutionDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>




                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="width: 230px">
                                            <span class="name">
                                                <asp:Label ID="Label2" runat="server" class="name disblock  control-label">
                                               Fixed</asp:Label></span>
                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtCompletionDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="display: none">
                                        <div class="form-group" style="width: 240px">
                                            <span class="name">
                                                <label class="control-label">Call Status </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlProjectMtceStatusID" runat="server" CssClass="myvalMTCE"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value=""> Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group" style="width: 245px">
                                            <span class="name disblock">
                                                <label class="control-label">Mtce Cost </label>
                                            </span><span class="dollarsign">
                                                <asp:TextBox ID="txtMtceCost" runat="server" MaxLength="20" CssClass="form-control floatleft" Style="width: 230px"></asp:TextBox>
                                                <span class="doller">$</span>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                    ControlToValidate="txtMtceCost" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group marginspicleftright" style="width: 245px">
                                            <span class="name disblock">
                                                <label class="control-label">Discount </label>
                                            </span><span class="dollarsign">
                                                <asp:TextBox ID="txtMtceDiscount" runat="server" AutoPostBack="true" CssClass="form-control floatleft" MaxLength="15" OnTextChanged="txtMtceDiscount_TextChanged" Style="width: 230px"></asp:TextBox>
                                                <span class="doller">$</span>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMtceDiscount"
                                                    Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="display: none">
                                        <div class="form-group " style="width: 240px">
                                            <span class="name">
                                                <label class="control-label">Paid By </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlFPTransTypeID" runat="server" CssClass="myvalMTCE"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select </asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group" style="width: 245px">
                                            <span class="name disblock">
                                                <label class="control-label">Service Cost </label>
                                            </span><span class="dollarsign">
                                                <asp:TextBox ID="txtServiceCost" runat="server" MaxLength="20" CssClass="form-control floatleft" Style="width: 230px"></asp:TextBox>
                                                <span class="doller">$</span>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                    ControlToValidate="txtServiceCost" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="width: 245px">
                                            <span class="name disblock">
                                                <label class="control-label">Balance </label>
                                            </span><span class="dollarsign">
                                                <asp:TextBox ID="txtMtceBalance" runat="server" MaxLength="15" Enabled="false" CssClass="form-control floatleft" Style="width: 230px"></asp:TextBox>
                                                <span class="doller">$</span>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="display: none">
                                        <div class="form-group" style="width: 240px">
                                            <span class="name">
                                                <label class="control-label">Rec By </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlMtceRecBy" runat="server" CssClass="myvalMTCE"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select </asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Work Done</h5>
                                        <div class="form-group">
                                            <span class="fullspan">
                                                <asp:TextBox ID="txtWorkDone" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 textcenterbutton marbottom20 center-text">
                                        <div class="col-md-12">
                                            <asp:Button class="btn btn-primary addwhiteicon redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="projmtce"
                                                Text="Add" CausesValidation="true" />
                                            <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                Text="Save" CausesValidation="true" ValidationGroup="req" Visible="true" />
                                            <asp:Button class="btn btn-purple resetbutton btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                CausesValidation="false" Text="Reset" />
                                            <asp:Button class="btn btn-dark-grey calcelwhiteicon btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                CausesValidation="false" Text="Cancel" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </asp:Panel>

                    <%--  <div class="contactbottomarea">
                        <div class="tableblack">
                            <div class="table-responsive printpage" id="PanGrid" runat="server">--%>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="finalgrid table-responsive xscroll" id="PanGrid" runat="server">

                                <asp:GridView ID="GridView1" DataKeyNames="ProjectMaintenanceID"
                                    runat="server" OnRowCommand="GridView1_RowCommand" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover table_WrapContent"
                                    OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnPageIndexChanging="GridView1_PageIndexChanging" Style="text-wrap: normal"
                                    AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnSorting="GridView1_Sorting" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="OpenDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" SortExpression="CustomerInput">
                                            <ItemTemplate>
                                                <%#Eval("OpenDate","{0:dd-MMM-yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallerName" ItemStyle-Width="150px">
                                            <ItemTemplate>
                                                <%#Eval("InstallerName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CustomerInput" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="CustomerInput">
                                            <ItemTemplate>
                                                <%#Eval("CustomerInput")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="width100">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="gvbtnDetail" runat="server" CommandName="Detail" CausesValidation="false" CommandArgument='<%# Eval("ProjectMaintenanceID")%>' CssClass="btn btn-primary btn-xs"
                                                    class=" btn-teal tooltips" data-placement="top" data-original-title="Detail View"> <i class="fa fa-link"></i> Detail</asp:LinkButton>

                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CausesValidation="false" CommandArgument='<%#Eval("ProjectMaintenanceID")%>'
                                                    CssClass="btn btn-danger btn-xs">
                                            <i class="fa fa-trash"></i> Delete
                                                </asp:LinkButton>




                                                <%--    <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandName="Select" ImageUrl="~/images/icon_edit.png"
                                                                    CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />--%>
                                            </ItemTemplate>
                                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="row nopadding" runat="server" style="margin-top: 20px;" id="divMtceDetail" visible="false">
                        <div class="col-md-12">
                            <div class="tabintab">
                                <div class="panel panel-default">
                                    <div class="panel-heading printpage">
                                        <asp:Literal ID="liMtceDetail" runat="server">Mtce Detail</asp:Literal>
                                        <div class="panel-tools">
                                            <asp:LinkButton ID="lnkclose" CssClass="btn-link" CausesValidation="false" runat="server"
                                                OnClick="lnkclose_Click"><img src="../../../images/new/back_btn_bule.png" width="13" height="13" /></asp:LinkButton>
                                        </div>
                                    </div>
                                    <uc1:printmtcedetail ID="printmtcedetail1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hdndelete" runat="server" />
        <!--End Danger Modal Templates-->
    </ContentTemplate>
    <Triggers>
        <%--  <asp:PostBackTrigger ControlID="GridView1" />--%>
        <asp:PostBackTrigger ControlID="lnkmaintainance" />

    </Triggers>
</asp:UpdatePanel>
