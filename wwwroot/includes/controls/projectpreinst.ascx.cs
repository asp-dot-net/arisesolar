﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Collections;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Net;
using System.Web.Script.Serialization;
//using FormbayClientApi;

public partial class includes_controls_projectpreinst : System.Web.UI.UserControl
{
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }

    #region CreateJob Model
    public partial class CreateJob
    {
        [JsonProperty("VendorJobId")]
        public int VendorJobId { get; set; }

        [JsonProperty("Arisesolarid")]
        public int Arisesolarid { get; set; }

        [JsonProperty("BasicDetails")]
        public BasicDetails BasicDetails { get; set; }

        [JsonProperty("InstallerView")]
        public InstallerView InstallerView { get; set; }

        [JsonProperty("ElectricianView")]
        public ElectricianView ElectricianView { get; set; }

        [JsonProperty("DesignerView")]
        public DesignerView DesignerView { get; set; }

        [JsonProperty("JobInstallationDetails")]
        public JobInstallationDetails JobInstallationDetails { get; set; }

        [JsonProperty("JobOwnerDetails")]
        public JobOwnerDetails JobOwnerDetails { get; set; }

        [JsonProperty("JobSystemDetails")]
        public JobSystemDetails JobSystemDetails { get; set; }

        [JsonProperty("JobSTCDetails")]
        public JobStcDetails JobStcDetails { get; set; }

        [JsonProperty("panel")]
        public Panel[] Panel { get; set; }

        [JsonProperty("inverter")]
        public Inverter[] Inverter { get; set; }

        [JsonProperty("lstJobNotes")]
        public LstJobNote[] LstJobNotes { get; set; }

        [JsonProperty("lstCustomDetails")]
        public LstCustomDetail[] LstCustomDetails { get; set; }

        public string quickformGuid { get; set; }

    }

    public partial class BasicDetails
    {
        [JsonProperty("JobType")]
        public int JobType { get; set; }
        [JsonProperty("Jobstatus")]
        public int Jobstatus { get; set; }

        [JsonProperty("formid")]
        public int formid { get; set; }

        [JsonProperty("Createddate")]
        public DateTime Createddate { get; set; }

        [JsonProperty("RefNumber")]
        public string RefNumber { get; set; }

        [JsonProperty("JobStage")]
        public int JobStage { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("strInstallationDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime StrInstallationDate { get; set; }

        [JsonProperty("Priority")]
        public int Priority { get; set; }
    }

    public partial class InstallerView
    {
        [JsonProperty("Installerid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }
    }

    public partial class ElectricianView
    {
        [JsonProperty("Electricianid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }
    }

    public partial class DesignerView
    {
        [JsonProperty("Designerid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }
    }

    public partial class Inverter
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Series")]
        public string Series { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }
        [JsonProperty("size")]
        public string size { get; set; }
        [JsonProperty("noofinverter")]
        public int noofinverter { get; set; }
    }

    public partial class JobInstallationDetails
    {
        [JsonProperty("UnitTypeID")]
        public string UnitTypeID { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public string StreetTypeId { get; set; }

        [JsonProperty("Streetaddress")]
        public string Streetaddress { get; set; }


        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("AddressID")]
        public int AddressId { get; set; }

        [JsonProperty("PostalAddressID")]
        public int PostalAddressId { get; set; }

        [JsonProperty("Inst_UnitNo")]
        public string Inst_UnitNo { get; set; }

        [JsonProperty("Inst_UnitType")]
        public string Inst_UnitType { get; set; }

        [JsonProperty("Inst_StreetNo")]
        public string Inst_StreetNo { get; set; }

        [JsonProperty("Inst_StreetName")]
        public string Inst_StreetName { get; set; }


        [JsonProperty("Inst_StreetType")]
        public string Inst_StreetType { get; set; }

        [JsonProperty("Inst_StreetAddress")]
        public string Inst_StreetAddress { get; set; }


        [JsonProperty("Inst_Suburb")]
        public string Inst_Suburb { get; set; }

        [JsonProperty("Inst_State")]
        public string Inst_State { get; set; }

        [JsonProperty("Inst_PostCode")]
        public string Inst_PostCode { get; set; }


        [JsonProperty("PropertyType")]
        public string PropertyType { get; set; }

        [JsonProperty("SingleMultipleStory")]
        public string SingleMultipleStory { get; set; }

        [JsonProperty("Rooftype")]
        public string Rooftype { get; set; }

        [JsonProperty("RegPlanNo")]
        public string RegPlanNo { get; set; }

        [JsonProperty("LotNo")]
        public string LotNo { get; set; }

        [JsonProperty("InstallingNewPanel")]
        public string InstallingNewPanel { get; set; }

        [JsonProperty("ExistingSystem")]
        public bool ExistingSystem { get; set; }

        [JsonProperty("ExistingSystemSize")]
        public string ExistingSystemSize { get; set; }

        [JsonProperty("NoOfPanels")]
        public int NoOfPanels { get; set; }

        [JsonProperty("SystemLocation")]
        public string SystemLocation { get; set; }

        [JsonProperty("AdditionalInstallationInformation")]
        public string AdditionalInstallationInformation { get; set; }

        [JsonProperty("DistributorID")]
        public int DistributorID { get; set; }

        [JsonProperty("ElectricityProviderID")]
        public int ElectricityProviderID { get; set; }

        [JsonProperty("NMI")]
        public string NMI { get; set; }

        [JsonProperty("MeterNumber")]
        public string MeterNumber { get; set; }

        [JsonProperty("PhaseProperty")]
        public string PhaseProperty { get; set; }

    }

    public partial class JobOwnerDetails
    {
        [JsonProperty("OwnerType")]
        public string OwnerType { get; set; }

        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }



        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("UnitTypeID")]
        public string UnitTypeID { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }


        [JsonProperty("StreetTypeID")]
        public string StreetTypeId { get; set; }

        [JsonProperty("StreetAddress")]
        public string StreetAddress { get; set; }


        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }


        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
    }

    public partial class JobStcDetails
    {
        [JsonProperty("TypeOfConnection")]
        public string TypeOfConnection { get; set; }

        [JsonProperty("SystemMountingType")]
        public string SystemMountingType { get; set; }

        [JsonProperty("DeemingPeriod")]
        public string DeemingPeriod { get; set; }

        [JsonProperty("MultipleSGUAddress")]
        public string MultipleSguAddress { get; set; }

        [JsonProperty("Location")]
        public string Location { get; set; }

        //[JsonProperty("panel")]
        //public JobStcDetailsPanelBrand panel { get; set; }

        //[JsonProperty("inverter")]
        //public JobStcDetailsPanelBrand inverter { get; set; }


    }
    public partial class JobStcDetailsPanelBrand
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }
    }

    public partial class JobSystemDetails
    {
        [JsonProperty("SystemModel")]
        public string SystemModel { get; set; }

        [JsonProperty("SystemSize")]
        public decimal SystemSize { get; set; }

        [JsonProperty("Systemtype")]
        public int Systemtype { get; set; }

        [JsonProperty("ConnectionType")]
        public int ConnectionType { get; set; }

        [JsonProperty("MountingType")]
        public int MountingType { get; set; }

        [JsonProperty("SerialNumbers")]
        public string SerialNumbers { get; set; }

        [JsonProperty("NoOfPanel")]
        public int NoOfPanel { get; set; }

        [JsonProperty("InstallationType")]
        public string InstallationType { get; set; }

    }

    public partial class LstCustomDetail
    {
        [JsonProperty("VendorJobCustomFieldId")]
        public long VendorJobCustomFieldId { get; set; }

        [JsonProperty("FieldValue")]
        public string FieldValue { get; set; }

        [JsonProperty("FieldName")]
        public string FieldName { get; set; }
    }

    public partial class LstJobNote
    {
        [JsonProperty("VendorJobNoteId")]
        public string VendorJobNoteId { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }
    }

    public partial class Panel
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }

        [JsonProperty("size")]
        public string size { get; set; }
        [JsonProperty("STC")]
        public string STC { get; set; }
    }
    #endregion
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    protected string SiteURL;
    private object installbookingtracker;
    HiddenField hdnSysDetails = new HiddenField();
    HiddenField hdnQty = new HiddenField();
    HiddenField hiddStockOrderitemID = new HiddenField();
    HiddenField hdnStockCategoryId = new HiddenField();
    HiddenField hdnStockItem = new HiddenField();
    HiddenField InstallerId = new HiddenField();
    DataTable dtAcno = new DataTable();
    decimal capacity;
    decimal rebate;
    decimal SystemCapacity;
    decimal stcno;
    protected void chkElecDistOK_CheckedChanged(object sender, EventArgs e)
    {
        //AjaxControlToolkit.ModalPopupExtender m = this.Parent.FindControl("projectpreinst") as AjaxControlToolkit.ModalPopupExtender;    


        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        string ProjectID = "";

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (stProject.InstallState == "VIC")
            {
                if (!string.IsNullOrEmpty(stProject.VicAppReference))
                {
                    divVicAppRefRebate.Visible = false;
                    PanAddUpdate.Visible = true;
                    if (chkElecDistOK.Checked == true)
                    {
                        //UserControlButtonClicked(sender);
                        divElecDistApproved.Visible = true;
                        if (txtElecDistApproved.Text == string.Empty)
                        {

                            RequiredFieldValidatorEDA.Visible = true;
                        }
                        else
                        {
                            RequiredFieldValidatorEDA.Visible = false;
                        }

                        txtInstallBookingDate.Enabled = true;
                        //  ImageInstallBookingDate.Enabled = true;
                        //rblAM1.Enabled = true;
                        rblAM1.Attributes.Add("onclick", "return true;");
                        rblAM2.Attributes.Add("onclick", "return true;");
                        rblPM1.Attributes.Add("onclick", "return true;");
                        rblPM2.Attributes.Add("onclick", "return true;");
                        //rblAM2.Enabled = true;
                        //rblPM1.Enabled = true;
                        //rblPM2.Enabled = true;
                        txtInstallDays.Enabled = true;
                    }
                    else
                    {
                        divElecDistApproved.Visible = false;
                        RequiredFieldValidatorEDA.Visible = false;

                        txtInstallBookingDate.Enabled = false;
                        //   ImageInstallBookingDate.Enabled = false;
                        rblAM1.Attributes.Add("onclick", "return false;");
                        rblAM2.Attributes.Add("onclick", "return false;");
                        rblPM1.Attributes.Add("onclick", "return false;");
                        rblPM2.Attributes.Add("onclick", "return false;");
                        //rblAM2.Enabled = false;
                        //rblPM1.Enabled = false;
                        //rblPM2.Enabled = false;
                        txtInstallDays.Enabled = false;

                        txtInstallBookingDate.Text = string.Empty;
                        txtElecDistApproved.Text = string.Empty;
                        rblAM1.Checked = false;
                        rblAM2.Checked = false;
                        rblPM1.Checked = false;
                        rblPM2.Checked = false;
                    }
                    BindScript();
                    //UserControlButtonClicked(sender);
                }
                else
                {
                    divVicAppRefRebate.Visible = true;
                    chkElecDistOK.Checked = false;
                    // PanAddUpdate.Visible = false;
                }
            }
            else
            {
                divVicAppRefRebate.Visible = false;
                PanAddUpdate.Visible = true;
                if (chkElecDistOK.Checked == true)
                {
                    //UserControlButtonClicked(sender);
                    divElecDistApproved.Visible = true;
                    if (txtElecDistApproved.Text == string.Empty)
                    {

                        RequiredFieldValidatorEDA.Visible = true;
                    }
                    else
                    {
                        RequiredFieldValidatorEDA.Visible = false;
                    }

                    txtInstallBookingDate.Enabled = true;
                    //  ImageInstallBookingDate.Enabled = true;
                    //rblAM1.Enabled = true;
                    rblAM1.Attributes.Add("onclick", "return true;");
                    rblAM2.Attributes.Add("onclick", "return true;");
                    rblPM1.Attributes.Add("onclick", "return true;");
                    rblPM2.Attributes.Add("onclick", "return true;");
                    //rblAM2.Enabled = true;
                    //rblPM1.Enabled = true;
                    //rblPM2.Enabled = true;
                    txtInstallDays.Enabled = true;
                }
                else
                {
                    divElecDistApproved.Visible = false;
                    RequiredFieldValidatorEDA.Visible = false;

                    txtInstallBookingDate.Enabled = false;
                    //   ImageInstallBookingDate.Enabled = false;
                    rblAM1.Attributes.Add("onclick", "return false;");
                    rblAM2.Attributes.Add("onclick", "return false;");
                    rblPM1.Attributes.Add("onclick", "return false;");
                    rblPM2.Attributes.Add("onclick", "return false;");
                    //rblAM2.Enabled = false;
                    //rblPM1.Enabled = false;
                    //rblPM2.Enabled = false;
                    txtInstallDays.Enabled = false;

                    txtInstallBookingDate.Text = string.Empty;
                    txtElecDistApproved.Text = string.Empty;
                    rblAM1.Checked = false;
                    rblAM2.Checked = false;
                    rblPM1.Checked = false;
                    rblPM2.Checked = false;
                }
                BindScript();
                //UserControlButtonClicked(sender);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //UserControlButtonClicked(sender);
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        //SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        //rvreqby.MinimumValue = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st2.ElecLicenceExpires));
        //rvreqby.MaximumValue = SiteConfiguration.FromSqlDate(DateTime.Now.AddYears(200));
        if (!IsPostBack)
        {
            MaxAttribute = 1;
            BindAddedAttribute();
            bindrepeater();
        }


        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];

        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
            if (st.StockAllocationStore == ddlStockAllocationStore.SelectedValue)
            {
                ListItem item = new ListItem();
                item.Value = "";
                item.Text = "Select";
                ddlStockAllocationStore.Items.Clear();
                ddlStockAllocationStore.Items.Add(item);

                ddlStockAllocationStore.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
                ddlStockAllocationStore.DataValueField = "CompanyLocationID";
                ddlStockAllocationStore.DataTextField = "CompanyLocation";
                ddlStockAllocationStore.DataMember = "CompanyLocation";
                ddlStockAllocationStore.DataBind();
                if (st.StockAllocationStore != string.Empty)
                {

                    try
                    {
                        ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                    }
                    catch { }
                }
                else
                {
                    if (st.InstallState != string.Empty)
                    {
                        DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(st.InstallState);
                        if (dtState.Rows.Count > 0)
                        {
                            ddlStockAllocationStore.SelectedValue = dtState.Rows[0]["CompanyLocationID"].ToString();
                        }
                    }
                }
            }
            #region itemcount
            try
            {
                string nm = "";
                if (st.StockAllocationStore != null && st.StockAllocationStore != "")
                {

                    SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
                    nm = stloc.CompanyLocation;
                }
                else
                {
                    if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                    {
                        SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                        nm = stloc.CompanyLocation;
                    }
                }
                sttblprojectqtypanelinverter stpanelqty = ClstblProjects.tblProjects_SelectByProjectqty(st.ProjectNumber, nm);

                sttblprojectqtypanelinverter stinverterqty = ClstblProjects.tblProjects_SelectByProjectinverterqty(st.ProjectNumber);


                string inverter1 = "0";
                if (!string.IsNullOrEmpty(st.InvertQty1))
                {
                    inverter1 = st.InvertQty1;
                }
                string inverter2 = "0";
                if (!string.IsNullOrEmpty(st.InvertQty2))
                {
                    inverter2 = st.InvertQty2;
                }
                string inverter3 = "0";
                if (!string.IsNullOrEmpty(st.InvertQty3))
                {
                    inverter3 = st.InvertQty3;
                }
                string panelQtu = "0";
                if (!string.IsNullOrEmpty(st.panelQty) || st.panelQty != "")
                {
                    panelQtu = st.panelQty;
                }
                string StockQuantitypanel = "0";
                if (!string.IsNullOrEmpty(stpanelqty.StockQuantitypanel))
                {
                    StockQuantitypanel = stpanelqty.StockQuantitypanel;
                }
                string StockQuantityinverter = "0";
                if (!string.IsNullOrEmpty(stinverterqty.StockQuantityinverter))
                {
                    StockQuantityinverter = stinverterqty.StockQuantityinverter;
                }

                int InverterQty = Convert.ToInt32(inverter1) + Convert.ToInt32(inverter2) + Convert.ToInt32(inverter3);
                txtnoofpanel.Text = StockQuantitypanel + "/" + panelQtu;
                txtnoofinverter.Text = StockQuantityinverter + "/" + Convert.ToString(InverterQty);
                DateTime today = DateTime.Today;

                //Panel Live Qty
                lblPl.Text = stpanelqty.StockQuantitypanel;
                lblPj.Text = st.panelQty;
                //lblPt.Text = Convert.ToString(Convert.ToInt32(StockQuantitypanel) - Convert.ToInt32(panelQtu));

                //Panel Min Qty   
                string panelminQtu = "0";
                if (!string.IsNullOrEmpty(st.PanelBrandID) || st.PanelBrandID != "")
                {
                    // DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.PanelBrandID, st.StockAllocationStore);
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.PanelBrandID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            panelminQtu = dtLive.Rows[0]["MinQty"].ToString();
                        }
                    }
                }
                lblPm.Text = panelminQtu;

                //Inverter Live Qty
                DataTable dtinverter = ClstblProjects.tblProjects_Selectinverterqty(st.ProjectNumber, nm);
                int InverterliveQty1 = 0;
                int InverterliveQty2 = 0;
                int InverterliveQty3 = 0;
                if (dtinverter.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity1"].ToString()))
                    {
                        InverterliveQty1 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity1"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity2"].ToString()))
                    {
                        InverterliveQty2 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity2"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity3"].ToString()))
                    {
                        InverterliveQty3 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity3"].ToString());
                    }
                }


                lblIl.Text = Convert.ToString(InverterliveQty1 + InverterliveQty3 + InverterliveQty3);
                lblIj.Text = Convert.ToString(InverterQty);
                lblIt.Text = Convert.ToString(Convert.ToInt32(InverterliveQty1 + InverterliveQty3 + InverterliveQty3) - Convert.ToInt32(InverterQty));

                //Invert Min Qty
                int InvertminQtu1 = 0;
                int InvertminQtu2 = 0;
                int InvertminQtu3 = 0;
                if (!string.IsNullOrEmpty(st.InverterDetailsID) || st.InverterDetailsID != "")
                {
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.InverterDetailsID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            InvertminQtu1 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) || st.SecondInverterDetailsID != "")
                {
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.SecondInverterDetailsID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            InvertminQtu2 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID) || st.ThirdInverterDetailsID != "")
                {
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.ThirdInverterDetailsID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            InvertminQtu3 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                        }
                    }
                }

                lblIm.Text = Convert.ToString(InvertminQtu1 + InvertminQtu2 + InvertminQtu3);
                //Panel Order Qty
                int panelOrderQty = 0;
                
                if (!string.IsNullOrEmpty(st.PanelBrandID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.PanelBrandID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            panelOrderQty = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblPO.Text = panelOrderQty.ToString();
                int panelOrderQtyETA = 0;
                if (!string.IsNullOrEmpty(st.PanelBrandID) && !string.IsNullOrEmpty(nm))
                {
                   DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(st.PanelBrandID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            panelOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lnketap.Text = panelOrderQtyETA.ToString();

                //if (lblPO.Text == "0")
                //{
                //    lblPO.Enabled = false;
                //}
                //else
                //{
                //    lblPO.Enabled = true;
                //}

                //Invert Order Qty
                int InvertOrderQty1 = 0;
                int InvertOrderQty2 = 0;
                int InvertOrderQty3 = 0;
                if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    // DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.InverterDetailsID, st.StockAllocationStore);
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.InverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.SecondInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.ThirdInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblIo.Text = Convert.ToString(InvertOrderQty1 + InvertOrderQty2 + InvertOrderQty3);

                //Invertr ETA Order Qty
                int InvertETAOrderQty1 = 0;
                int InvertETAOrderQty2 = 0;
                int InvertETAOrderQty3 = 0;
                if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    // DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.InverterDetailsID, st.StockAllocationStore);
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(st.InverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertETAOrderQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(st.SecondInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertETAOrderQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(st.ThirdInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertETAOrderQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lnketaI.Text = Convert.ToString(InvertETAOrderQty1 + InvertETAOrderQty2 + InvertETAOrderQty3);
                //if (lblIo.Text == "0")
                //{
                //    lblIo.Enabled = false;
                //}
                //else
                //{
                //    lblIo.Enabled = true;
                //}

                //Panel WholeSale Qty
                int panelWhOrderQty = 0;
                if (!string.IsNullOrEmpty(st.PanelBrandID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.PanelBrandID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            panelWhOrderQty = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblPW.Text = panelWhOrderQty.ToString();
                //if (lblPW.Text == "0")
                //{
                //    lblPW.Enabled = false;
                //}
                //else
                //{
                //    lblPW.Enabled = false;
                //}
                //Inverter Wholesale

                int InvertOrderwhQty1 = 0;
                int InvertOrderwhQty2 = 0;
                int InvertOrderwhQty3 = 0;
                if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.InverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderwhQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.SecondInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderwhQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.ThirdInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderwhQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblIW.Text = Convert.ToString(InvertOrderwhQty1 + InvertOrderwhQty2 + InvertOrderwhQty3);
                if (lblIW.Text == "0")
                {
                    lblIW.Enabled = false;
                }
                else
                {
                    lblIW.Enabled = false;
                }
                //ArisePanelCount
                if (st.PanelBrand != null && st.PanelBrandID != "")
                {
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.PanelBrandID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                        else
                        {
                            lblApl.Text = "0";

                        }

                    }
                    else
                    {
                        lblApl.Text = "0";

                    }
                }
                else
                {
                    lblApl.Text = "0";
                }
                //SolarMinerPanelCount
                if (st.PanelBrandID != null && st.PanelBrandID != "")
                {
                    // SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.PanelBrandID);
                    DataTable DtSolarMinor = ClstblProjects.GetItemCountompanyWise(st.PanelBrandID, "2", nm);
                    //DataTable DtSolarMinor = ClstblProjects.GetItemCountompanyWise(stockItems.FixStockItemID, "2");
                    if (DtSolarMinor.Rows.Count > 0)
                    {
                        if (DtSolarMinor.Rows[0]["Qty"].ToString() != null && DtSolarMinor.Rows[0]["Qty"].ToString() != "")
                        {
                            lblSMPL.Text = DtSolarMinor.Rows[0]["Qty"].ToString();
                        }
                        else
                        {
                            lblSMPL.Text = "0";

                        }
                    }
                    else
                    {
                        lblSMPL.Text = "0";

                    }
                }
                else
                {
                    lblSMPL.Text = "0";

                }
                //AriseSolar InverterCount
                int inv1 = 0;
                int inv2 = 0;
                int inv3 = 0;
                if (st.InverterDetailsID != null && st.InverterDetailsID != "")
                {

                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.InverterDetailsID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            inv1 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.SecondInverterDetailsID != null && st.SecondInverterDetailsID != "")
                {

                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.SecondInverterDetailsID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            inv2 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.ThirdInverterDetailsID != null && st.ThirdInverterDetailsID != "")
                {

                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.ThirdInverterDetailsID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            inv3 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                lblAriseInv.Text = Convert.ToString(inv1 + inv2 + inv3);

                //SolarMinor InverterCount
                int invSm1 = 0;
                int invSm2 = 0;
                int invSm3 = 0;
                if (st.InverterDetailsID != null && st.InverterDetailsID != "")
                {
                    // SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.InverterDetailsID);
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.InverterDetailsID, "2", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            invSm1 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.SecondInverterDetailsID != null && st.SecondInverterDetailsID != "")
                {
                    //SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.SecondInverterDetailsID);
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.SecondInverterDetailsID, "2", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            invSm2 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.ThirdInverterDetailsID != null && st.ThirdInverterDetailsID != "")
                {
                    //SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.InverterDetailsID);
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.ThirdInverterDetailsID, "2", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            invSm3 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                lblSmInv.Text = Convert.ToString(invSm1 + invSm2 + invSm3);
            }
            catch (Exception edf) { }
            int apl = 0;
            int smpl = 0;
            int wh = 0;
            int livepnl = 0;

            int TotalPnal = 0;
            if (lblApl.Text != null && lblApl.Text != "")
            {
                apl = Convert.ToInt32(lblApl.Text);
            }
            if (lblApl.Text != null && lblApl.Text != "")
            {
                smpl = Convert.ToInt32(lblSMPL.Text);
            }
            if (lblPW.Text != null && lblPW.Text != "")
            {
                wh = Convert.ToInt32(lblPW.Text);
            }
            if (lblPl.Text != null && lblPl.Text != "")
            {
                livepnl = Convert.ToInt32(lblPl.Text);
            }

            try
            {

                int PnalCount = apl + smpl + wh;
                // int livepnl = Convert.ToInt32();
                lblPt.Text = Convert.ToString(livepnl - PnalCount);
                if (lblPt.Text != null && lblPt.Text != "")
                {
                    TotalPnal = Convert.ToInt32(lblPt.Text);
                    if (st.ProjectStatusID != "8" && st.ProjectStatusID != "2")//planned and deprec
                    {
                        if (TotalPnal <= 0)
                        {
                            divpanel.Visible = true;
                        }
                        else
                        {
                            divpanel.Visible = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
            try
            {
                int ariseinv = 0;
                int sminv = 0;
                int whinv = 0;
                int liveinv = 0;
                int ttlinv = 0;
                if (lblAriseInv.Text != null && lblAriseInv.Text != "")
                {
                    ariseinv = Convert.ToInt32(lblAriseInv.Text);
                }
                if (lblSmInv.Text != null && lblSmInv.Text != "")
                {
                    sminv = Convert.ToInt32(lblSmInv.Text);
                }
                if (lblIW.Text != null && lblIW.Text != "")
                {
                    whinv = Convert.ToInt32(lblIW.Text);
                }
                if (lblIl.Text != null && lblIl.Text != "")
                {
                    liveinv = Convert.ToInt32(lblIl.Text);
                }

                int InvCount = ariseinv + sminv + whinv;
                // int LIVEINVCount = ();
                lblIt.Text = Convert.ToString(liveinv - InvCount);
                if (lblIt.Text != null && lblIt.Text != "")
                {
                    ttlinv = Convert.ToInt32(lblIt.Text);
                    if (st.ProjectStatusID != "8" && st.ProjectStatusID != "2")//planned and deprec
                    {
                        if (ttlinv <= 0)
                        {
                            divinverter.Visible = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            { }
            #endregion 
            if (st.InstallEndDate != null && st.InstallEndDate != "")
            {
                if (st.InstallBookingDate != null && st.InstallBookingDate != "")
                {
                    //DateTime InstallDate = Convert.ToDateTime(st.InstallBookingDate);
                    DateTime EndDate = Convert.ToDateTime(st.InstallEndDate);
                    //DateTime TodayDate = Convert.ToDateTime(txtInstallBookingDate.Text);
                    //string TodayDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();
                    string InstallDate = Convert.ToDateTime(st.InstallBookingDate).ToShortDateString();
                    //string EndDate = Convert.ToDateTime(txtInstallBookingDate.Text).AddDays(28).ToShortDateString();
                    //string TodayDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();

                    if (Convert.ToDateTime(InstallDate) <= Convert.ToDateTime(EndDate))
                    {
                        btnPickList.Visible = true;
                        btnaddnew.Visible = true;
                    }
                    else
                    {
                        btnPickList.Visible = false;
                        btnaddnew.Visible = false;
                        divinstalldate.Visible = true;
                    }
                }
            }
            if (st.PanelBrandID != "")
            {
                txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName;

            }
            if (st.PanelBrandID != "" && st.InverterDetailsID != "")
            {
                txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter.";
                //hdnQty.Value = st.NumberPanels.ToString();
                hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                //hdnStockItem.Value = st.StockItemID.ToString();
                hdnStockCategoryId.Value = st.StockCategoryID.ToString();
                //st.
            }
            if (st.PanelBrandID != "" && st.InverterDetailsID != "" && st.SecondInverterDetailsID != "")
            {
                txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter. Plus Second Inverter" + st.SecondInverterDetails;
                hdnQty.Value = st.NumberPanels.ToString();
                hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                hdnStockItem.Value = st.StockItemID.ToString();
                hdnStockCategoryId.Value = st.StockCategoryID.ToString();

            }
            //BindStockItem();

            if (String.IsNullOrEmpty(stProject.InstallBookingDate))
            {
                panelPickListBtn.Enabled = false;

            }
            if (!String.IsNullOrEmpty(stProject.quickformGuid))
            {
                btnquickfrom.Visible = false;
                btnquickfromUpdate.Visible = true;
            }
            else
            {
                btnquickfrom.Visible = true;
                btnquickfromUpdate.Visible = false;
            }
            //Response.Write(Request.QueryString["compid"]);
            //Response.End();
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Maintenance")))
            {
                btnQuckFormnew.Visible = true;
                //btnaddnew.Visible = true;
            }
            else if ((Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("Maintenance")))
            {
                btnaddnew.Visible = false;
            }
            else if ((Roles.IsUserInRole("PreInstaller")))
            {

            }
        }

    }

    //public void BindStockItem()
    //{
    //    DataTable dtStockItem = ClstblStockItems.tblStockItems_SelectAll();
    //    DropDownList ddlStockItem = new DropDownList();
    //    ddlStockItem.DataSource = dtStockItem;
    //    ddlStockItem.DataTextField = "StockItem";
    //    ddlStockItem.DataValueField = "StockItemID";
    //    ddlStockItem.DataBind();

    //}
    public void datevalidator()
    {
        //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
        //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());
    }
    //public void datevalidator1()
    //{
    //    DateRange.MinimumValue = DateTime.Now.Date.AddYears(-1).ToString();
    //    DateRange.MaximumValue = DateTime.Now.Date.AddYears(200).ToString();
    //}
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }


    public void BindProjectPreInst(string proid)
    {

        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        if (proid != string.Empty)
        {

            //string proid = Request.QueryString["proid"];
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                SttblCustomers st1 = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);

                if (st1.isformbayadd != string.Empty)
                {
                    if (Convert.ToBoolean(st1.isformbayadd) == true)
                    {
                        txtInstallAddressline.Enabled = true;
                        custompreadd.Enabled = true;
                        txtInstallAddress.Enabled = false;
                        txtInstallCity.Enabled = false;
                        AutoComplete_pre.Enabled = false;
                    }
                    else if (Convert.ToBoolean(st1.isformbayadd) == false)
                    {
                        txtInstallAddressline.Enabled = false;
                        custompreadd.Enabled = false;
                        txtInstallAddress.Enabled = true;
                        txtInstallCity.Enabled = true;
                        AutoComplete_pre.Enabled = true;
                    }
                }
            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);

            SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
            try
            {
                string nm = "";
                if (st.StockAllocationStore != null && st.StockAllocationStore != "")
                {

                    SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
                    nm = stloc.CompanyLocation;
                }
                else
                {
                    if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                    {
                        SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                        nm = stloc.CompanyLocation;
                    }
                }
                sttblprojectqtypanelinverter stpanelqty = ClstblProjects.tblProjects_SelectByProjectqty(st.ProjectNumber, nm);

                sttblprojectqtypanelinverter stinverterqty = ClstblProjects.tblProjects_SelectByProjectinverterqty(st.ProjectNumber);


                string inverter1 = "0";
                if (!string.IsNullOrEmpty(st.InvertQty1))
                {
                    inverter1 = st.InvertQty1;
                }
                string inverter2 = "0";
                if (!string.IsNullOrEmpty(st.InvertQty2))
                {
                    inverter2 = st.InvertQty2;
                }
                string inverter3 = "0";
                if (!string.IsNullOrEmpty(st.InvertQty3))
                {
                    inverter3 = st.InvertQty3;
                }
                string panelQtu = "0";
                if (!string.IsNullOrEmpty(st.panelQty) || st.panelQty != "")
                {
                    panelQtu = st.panelQty;
                }
                string StockQuantitypanel = "0";
                if (!string.IsNullOrEmpty(stpanelqty.StockQuantitypanel))
                {
                    StockQuantitypanel = stpanelqty.StockQuantitypanel;
                }
                string StockQuantityinverter = "0";
                if (!string.IsNullOrEmpty(stinverterqty.StockQuantityinverter))
                {
                    StockQuantityinverter = stinverterqty.StockQuantityinverter;
                }

                int InverterQty = Convert.ToInt32(inverter1) + Convert.ToInt32(inverter2) + Convert.ToInt32(inverter3);
                txtnoofpanel.Text = StockQuantitypanel + "/" + panelQtu;
                txtnoofinverter.Text = StockQuantityinverter + "/" + Convert.ToString(InverterQty);
                DateTime today = DateTime.Today;

                //Panel Live Qty
                lblPl.Text = stpanelqty.StockQuantitypanel;
                lblPj.Text = st.panelQty;
                //lblPt.Text = Convert.ToString(Convert.ToInt32(StockQuantitypanel) - Convert.ToInt32(panelQtu));

                //Panel Min Qty   
                string panelminQtu = "0";
                if (!string.IsNullOrEmpty(st.PanelBrandID) || st.PanelBrandID != "")
                {
                    // DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.PanelBrandID, st.StockAllocationStore);
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.PanelBrandID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            panelminQtu = dtLive.Rows[0]["MinQty"].ToString();
                        }
                    }
                }
                lblPm.Text = panelminQtu;

                //Inverter Live Qty
                DataTable dtinverter = ClstblProjects.tblProjects_Selectinverterqty(st.ProjectNumber, nm);
                int InverterliveQty1 = 0;
                int InverterliveQty2 = 0;
                int InverterliveQty3 = 0;
                if (dtinverter.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity1"].ToString()))
                    {
                        InverterliveQty1 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity1"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity2"].ToString()))
                    {
                        InverterliveQty2 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity2"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity3"].ToString()))
                    {
                        InverterliveQty3 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity3"].ToString());
                    }
                }
                lblIl.Text = Convert.ToString(InverterliveQty1 + InverterliveQty3 + InverterliveQty3);
                lblIj.Text = Convert.ToString(InverterQty);
                lblIt.Text = Convert.ToString(Convert.ToInt32(InverterliveQty1 + InverterliveQty3 + InverterliveQty3) - Convert.ToInt32(InverterQty));

                //Invert Min Qty
                int InvertminQtu1 = 0;
                int InvertminQtu2 = 0;
                int InvertminQtu3 = 0;
                if (!string.IsNullOrEmpty(st.InverterDetailsID) || st.InverterDetailsID != "")
                {
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.InverterDetailsID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            InvertminQtu1 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) || st.SecondInverterDetailsID != "")
                {
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.SecondInverterDetailsID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            InvertminQtu2 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID) || st.ThirdInverterDetailsID != "")
                {
                    DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.ThirdInverterDetailsID, nm);
                    if (dtLive.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                        {
                            InvertminQtu3 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                        }
                    }
                }

                lblIm.Text = Convert.ToString(InvertminQtu1 + InvertminQtu2 + InvertminQtu3);
                //Panel Order Qty
                int panelOrderQty = 0;
                if (!string.IsNullOrEmpty(st.PanelBrandID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.PanelBrandID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            panelOrderQty = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblPO.Text = panelOrderQty.ToString();
                if (lblPO.Text == "0")
                {
                    lblPO.Enabled = false;
                }
                else
                {
                    lblPO.Enabled = true;
                }

                //Invert Order Qty
                int InvertOrderQty1 = 0;
                int InvertOrderQty2 = 0;
                int InvertOrderQty3 = 0;
                if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    // DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.InverterDetailsID, st.StockAllocationStore);
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.InverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.SecondInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.ThirdInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblIo.Text = Convert.ToString(InvertOrderQty1 + InvertOrderQty2 + InvertOrderQty3);
                if (lblIo.Text == "0")
                {
                    lblIo.Enabled = false;
                }
                else
                {
                    lblIo.Enabled = true;
                }

                //Panel WholeSale Qty
                int panelWhOrderQty = 0;
                if (!string.IsNullOrEmpty(st.PanelBrandID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.PanelBrandID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            panelWhOrderQty = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblPW.Text = panelWhOrderQty.ToString();
                //if (lblPW.Text == "0")
                //{
                //    lblPW.Enabled = false;
                //}
                //else
                //{
                //    lblPW.Enabled = false;
                //}
                //Inverter Wholesale

                int InvertOrderwhQty1 = 0;
                int InvertOrderwhQty2 = 0;
                int InvertOrderwhQty3 = 0;
                if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.InverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderwhQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.SecondInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderwhQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
                {
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(st.ThirdInverterDetailsID, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            InvertOrderwhQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                }
                lblIW.Text = Convert.ToString(InvertOrderwhQty1 + InvertOrderwhQty2 + InvertOrderwhQty3);
                if (lblIW.Text == "0")
                {
                    lblIW.Enabled = false;
                }
                else
                {
                    lblIW.Enabled = true;
                }
                //ArisePanelCount
                if (st.PanelBrand != null && st.PanelBrandID != "")
                {
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.PanelBrandID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                        else
                        {
                            lblApl.Text = "0";

                        }

                    }
                    else
                    {
                        lblApl.Text = "0";

                    }
                }
                else
                {
                    lblApl.Text = "0";
                }
                //SolarMinerPanelCount
                if (st.PanelBrandID != null && st.PanelBrandID != "")
                {
                    // SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.PanelBrandID);
                    DataTable DtSolarMinor = ClstblProjects.GetItemCountompanyWise(st.PanelBrandID, "2", nm);
                    //DataTable DtSolarMinor = ClstblProjects.GetItemCountompanyWise(stockItems.FixStockItemID, "2");
                    if (DtSolarMinor.Rows.Count > 0)
                    {
                        if (DtSolarMinor.Rows[0]["Qty"].ToString() != null && DtSolarMinor.Rows[0]["Qty"].ToString() != "")
                        {
                            lblSMPL.Text = DtSolarMinor.Rows[0]["Qty"].ToString();
                        }
                        else
                        {
                            lblSMPL.Text = "0";

                        }
                    }
                    else
                    {
                        lblSMPL.Text = "0";

                    }
                }
                else
                {
                    lblSMPL.Text = "0";

                }
                //AriseSolar InverterCount
                int inv1 = 0;
                int inv2 = 0;
                int inv3 = 0;
                if (st.InverterDetailsID != null && st.InverterDetailsID != "")
                {

                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.InverterDetailsID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            inv1 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.SecondInverterDetailsID != null && st.SecondInverterDetailsID != "")
                {

                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.SecondInverterDetailsID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            inv2 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.ThirdInverterDetailsID != null && st.ThirdInverterDetailsID != "")
                {

                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.ThirdInverterDetailsID, "1", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            inv3 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                lblAriseInv.Text = Convert.ToString(inv1 + inv2 + inv3);

                //SolarMinor InverterCount
                int invSm1 = 0;
                int invSm2 = 0;
                int invSm3 = 0;
                if (st.InverterDetailsID != null && st.InverterDetailsID != "")
                {
                    // SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.InverterDetailsID);
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.InverterDetailsID, "2", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            invSm1 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.SecondInverterDetailsID != null && st.SecondInverterDetailsID != "")
                {
                    //SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.SecondInverterDetailsID);
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.SecondInverterDetailsID, "2", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            invSm2 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                if (st.ThirdInverterDetailsID != null && st.ThirdInverterDetailsID != "")
                {
                    //SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.InverterDetailsID);
                    DataTable dtArise = ClstblProjects.GetItemCountompanyWise(st.ThirdInverterDetailsID, "2", nm);
                    if (dtArise.Rows.Count > 0)
                    {
                        if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                        {
                            invSm3 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                }
                lblSmInv.Text = Convert.ToString(invSm1 + invSm2 + invSm3);
            }
            catch (Exception e) { }
            int apl = 0;
            int smpl = 0;
            int wh = 0;
            int livepnl = 0;

            int TotalPnal = 0;
            if (lblApl.Text != null && lblApl.Text != "")
            {
                apl = Convert.ToInt32(lblApl.Text);
            }
            if (lblApl.Text != null && lblApl.Text != "")
            {
                smpl = Convert.ToInt32(lblSMPL.Text);
            }
            if (lblPW.Text != null && lblPW.Text != "")
            {
                wh = Convert.ToInt32(lblPW.Text);
            }
            if (lblPl.Text != null && lblPl.Text != "")
            {
                livepnl = Convert.ToInt32(lblPl.Text);
            }

            try
            {

                int PnalCount = apl + smpl + wh;
                // int livepnl = Convert.ToInt32();
                lblPt.Text = Convert.ToString(livepnl - PnalCount);
                if (lblPt.Text != null && lblPt.Text != "")
                {
                    TotalPnal = Convert.ToInt32(lblPt.Text);
                    if (st.ProjectStatusID != "8" && st.ProjectStatusID != "2")//planned and deprec
                    {
                        if (TotalPnal <= 0)
                        {
                            divpanel.Visible = true;
                        }
                        else
                        {
                            divpanel.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            try
            {
                int ariseinv = 0;
                int sminv = 0;
                int whinv = 0;
                int liveinv = 0;
                int ttlinv = 0;
                if (lblAriseInv.Text != null && lblAriseInv.Text != "")
                {
                    ariseinv = Convert.ToInt32(lblAriseInv.Text);
                }
                if (lblSmInv.Text != null && lblSmInv.Text != "")
                {
                    sminv = Convert.ToInt32(lblSmInv.Text);
                }
                if (lblIW.Text != null && lblIW.Text != "")
                {
                    whinv = Convert.ToInt32(lblIW.Text);
                }
                if (lblIl.Text != null && lblIl.Text != "")
                {
                    liveinv = Convert.ToInt32(lblIl.Text);
                }

                int InvCount = ariseinv + sminv + whinv;
                // int LIVEINVCount = ();
                lblIt.Text = Convert.ToString(liveinv - InvCount);
                if (lblIt.Text != null && lblIt.Text != "")
                {
                    ttlinv = Convert.ToInt32(lblIt.Text);
                    if (st.ProjectStatusID != "8" && st.ProjectStatusID != "2")//planned and deprec
                    {
                        if (ttlinv <= 0)
                        {
                            divinverter.Visible = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            { }
            //   DateRange.Type = ValidationDataType.Date;
            //Response.Write(st2.ElecLicenceExpires);
            //Response.End();
            //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
            //// DateRange.MaximumValue = DateTime.Now.ToShortDateString();
            if (!string.IsNullOrEmpty(st2.ElecLicenceExpires))//st2.ElecLicenceExpires != string.Empty)
            {
                //     Response.Write("yes");

                //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(st2.ElecLicenceExpires.ToString());
            }
            else
            {
                //     Response.Write("no");
                //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());
            }
            // Response.End();

            if (!string.IsNullOrEmpty(st2.ElecLicenceExpires))
            {
                SiteConfiguration.FromSqlDate(Convert.ToDateTime(st2.ElecLicenceExpires));
            }

            if (st.QuickForm == "True")
            {

                chkquickform.Checked = true;
            }

            if ((Roles.IsUserInRole("Sales Manager")))
            {
                PanAddUpdate.Enabled = false;
            }
            //if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller"))
            //{
            //    btnUpdateAddress.Visible = true;
            //}
            //else
            //{
            //    btnUpdateAddress.Visible = false;
            //}
            //if (Roles.IsUserInRole("Administrator"))
            //{
            //    // divAddUpdate.Visible = true;
            //}
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }

            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                //PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                //divAddUpdate.Visible = true;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Sales Manager"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
                // PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                //if (proid != string.Empty)
                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }
            BindDropDown();

            if (st.ProjectStatusID != "9")
            {

                if (st.ProjectTypeID == "7")
                {
                    divActive.Visible = false;
                    PanAddUpdate.Visible = true;
                    //Response.Write(st.InstallBookingDate);
                    //Response.End();
                    if (st.InstallBookingDate != string.Empty)
                    {
                        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Installation Manager")))
                        {
                            btnRemoveInst.Visible = true;
                            btnMove.Visible = false;
                            ddlMoveProject.Visible = true;
                        }
                    }
                    else
                    {
                        btnRemoveInst.Visible = false;
                        btnMove.Visible = false;
                        ddlMoveProject.Visible = false;
                    }
                    if (st.FinanceWithID == string.Empty || st.FinanceWithID == "1")
                    {
                        PanAddUpdate.Visible = true;
                        DivDocNotVerified.Visible = false;
                    }
                    else
                    {
                        if (st.DocumentVerified == "True")
                        {
                            PanAddUpdate.Visible = true;
                            DivDocNotVerified.Visible = false;
                        }
                        else
                        {
                            PanAddUpdate.Visible = false;
                            DivDocNotVerified.Visible = true;
                        }
                    }

                    string ProjectID = proid;// Request.QueryString["proid"];
                    if (Convert.ToInt32(st.InstallDays) > 0)
                    {
                        txtInstallDays.Text = st.InstallDays;
                    }
                    chkElecDistOK.Checked = Convert.ToBoolean(st.ElecDistOK);
                    if (st.ElecDistOK == "True")
                    {
                        divElecDistApproved.Visible = true;

                        try
                        {
                            txtElecDistApproved.Text = Convert.ToDateTime(st.ElecDistApproved).ToShortDateString();
                        }
                        catch { }

                        txtInstallBookingDate.Enabled = true;
                        //   ImageInstallBookingDate.Enabled = true;
                        rblAM1.Attributes.Add("onclick", "return true;");
                        rblAM2.Attributes.Add("onclick", "return true;");
                        rblPM1.Attributes.Add("onclick", "return true;");
                        rblPM2.Attributes.Add("onclick", "return true;");
                        //rblAM1.Enabled = true;
                        //rblAM2.Enabled = true;
                        //rblPM1.Enabled = true;
                        //rblPM2.Enabled = true;
                        txtInstallDays.Enabled = true;
                    }
                    else
                    {
                        divElecDistApproved.Visible = false;

                        txtInstallBookingDate.Enabled = false;
                        //  ImageInstallBookingDate.Enabled = false;
                        //rblAM1.Enabled = false;
                        //lblAM1.Attributes.Add("style", "pointer-events: none;");
                        rblAM1.Attributes.Add("onclick", "return false;");
                        rblAM2.Attributes.Add("onclick", "return false;");
                        rblPM1.Attributes.Add("onclick", "return false;");
                        rblPM2.Attributes.Add("onclick", "return false;");
                        //rblAM2.Enabled = false;
                        //rblPM1.Enabled = false;
                        //rblPM2.Enabled = false;
                        txtInstallDays.Enabled = false;
                    }
                    txtInstallerNotes.Text = st.InstallerNotes;
                    try
                    {
                        txtInstallAddressline.Text = st.InstallAddress + ", " + st.InstallCity + " " + st.InstallState + " " + st.InstallPostCode;
                    }
                    catch { }
                    txtInstallAddress.Text = st.InstallAddress;

                    txtformbayUnitNo.Text = st.unit_number;
                    try
                    {
                        ddlformbayunittype.SelectedValue = st.unit_type;
                    }
                    catch { }
                    txtformbayStreetNo.Text = st.street_number;
                    txtformbaystreetname.Text = st.street_name;
                    ddlformbaystreettype.SelectedValue = st.street_type;

                    txtInstallCity.Text = st.InstallCity;
                    txtInstallState.Text = st.InstallState;
                    txtInstallPostCode.Text = st.InstallPostCode;

                    hndstreetname.Value = st.street_name;
                    hndstreetno.Value = st.street_number;
                    hndstreetsuffix.Value = st.street_suffix;
                    hndstreettype.Value = st.street_type;
                    hndunitno.Value = st.unit_number;
                    hndunittype.Value = st.unit_type;
                    hndaddress.Value = st.InstallAddress;

                    if (st.IsFormBay == "2")
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return false;");
                        //cbkIsFormBay.Enabled = false;
                    }
                    else
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return true;");
                        //cbkIsFormBay.Enabled = true;
                        if (st.IsFormBay == "1")
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return true;");
                            //cbkIsFormBay.Checked = true;
                        }
                        else
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return false;");
                            //cbkIsFormBay.Checked = false;
                        }
                    }

                    if (st.PanelBrandID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName;
                        hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter.";
                        hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "" && st.SecondInverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter. Plus Second Inverter" + st.SecondInverterDetails;
                        hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                    }

                    try
                    {
                        ddlInstaller.SelectedValue = st.Installer;
                    }
                    catch { }
                    try
                    {
                        ddlDesigner.SelectedValue = st.Designer;
                    }
                    catch { }
                    try
                    {
                        ddlElectrician.SelectedValue = st.Electrician;
                    }
                    catch { }

                    if (st.StockAllocationStore != string.Empty)
                    {
                        try
                        {
                            ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        }
                        catch { }
                    }
                    else
                    {
                        if (st.InstallState != string.Empty)
                        {
                            DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(st.InstallState);
                            if (dtState.Rows.Count > 0)
                            {
                                ddlStockAllocationStore.SelectedValue = dtState.Rows[0]["CompanyLocationID"].ToString();
                            }
                        }
                    }
                    rblAM1.Checked = Convert.ToBoolean(st.InstallAM1);
                    rblAM2.Checked = Convert.ToBoolean(st.InstallAM2);
                    rblPM1.Checked = Convert.ToBoolean(st.InstallPM1);
                    rblPM2.Checked = Convert.ToBoolean(st.InstallPM2);

                    try
                    {
                        if (!string.IsNullOrEmpty(st.InstallBookingDate))
                        {
                            txtInstallBookingDate.Text = Convert.ToDateTime(st.InstallBookingDate).ToShortDateString();
                            if (st.InstallEndDate != null && st.InstallEndDate != "")
                            {
                                if (txtInstallBookingDate.Text != null && txtInstallBookingDate.Text != "")
                                {
                                    //DateTime InstallDate = Convert.ToDateTime(st.InstallBookingDate);
                                    DateTime EndDate = Convert.ToDateTime(st.InstallEndDate);
                                    //DateTime TodayDate = Convert.ToDateTime(txtInstallBookingDate.Text);
                                    //string TodayDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();
                                    string InstallDate = Convert.ToDateTime(txtInstallBookingDate.Text).ToShortDateString();
                                    //string EndDate = Convert.ToDateTime(txtInstallBookingDate.Text).AddDays(28).ToShortDateString();
                                    //string TodayDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();

                                    if (Convert.ToDateTime(InstallDate) <= Convert.ToDateTime(EndDate))
                                    {
                                        btnPickList.Visible = true;
                                        btnaddnew.Visible = true;
                                    }
                                    else
                                    {
                                        divinstalldate.Visible = true;
                                        btnPickList.Visible = false;
                                        btnaddnew.Visible = false;

                                    }
                                }
                            }

                        }
                        else
                        {
                            txtInstallBookingDate.Text = "";
                        }
                    }
                    catch { }
                }
                else if (st.ProjectStatusID == "2" || st.ProjectStatusID == "8" || st.ProjectStatusID == "4" || st.ProjectStatusID == "20" || st.ProjectStatusID == "6")
                {
                    divActive.Visible = true;
                    PanAddUpdate.Visible = false;
                }
                else
                {
                    divActive.Visible = false;
                    PanAddUpdate.Visible = true;

                    if (st.InstallBookingDate != string.Empty)
                    {
                        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Installation Manager")))
                        {
                            btnRemoveInst.Visible = true;
                            btnMove.Visible = true;
                            ddlMoveProject.Visible = true;
                        }
                    }
                    else
                    {
                        btnRemoveInst.Visible = false;
                        btnMove.Visible = false;
                        ddlMoveProject.Visible = false;
                    }
                    if (st.FinanceWithID == string.Empty || st.FinanceWithID == "1")
                    {
                        PanAddUpdate.Visible = true;
                        DivDocNotVerified.Visible = false;
                    }
                    else
                    {
                        if (st.DocumentVerified == "True")
                        {
                            PanAddUpdate.Visible = true;
                            DivDocNotVerified.Visible = false;
                        }
                        else
                        {
                            PanAddUpdate.Visible = false;
                            DivDocNotVerified.Visible = true;
                        }
                    }

                    string ProjectID = proid;// Request.QueryString["proid"];
                    if (Convert.ToInt32(st.InstallDays) > 0)
                    {
                        txtInstallDays.Text = st.InstallDays;
                    }
                    chkElecDistOK.Checked = Convert.ToBoolean(st.ElecDistOK);
                    if (st.ElecDistOK == "True")
                    {
                        divElecDistApproved.Visible = true;

                        try
                        {
                            txtElecDistApproved.Text = Convert.ToDateTime(st.ElecDistApproved).ToShortDateString();
                        }
                        catch { }

                        txtInstallBookingDate.Enabled = true;
                        //   ImageInstallBookingDate.Enabled = true;
                        rblAM1.Attributes.Add("onclick", "return true;");
                        rblAM2.Attributes.Add("onclick", "return true;");
                        rblPM1.Attributes.Add("onclick", "return true;");
                        rblPM2.Attributes.Add("onclick", "return true;");
                        //rblAM1.Enabled = true;
                        //rblAM2.Enabled = true;
                        //rblPM1.Enabled = true;
                        //rblPM2.Enabled = true;
                        txtInstallDays.Enabled = true;
                    }
                    else
                    {
                        divElecDistApproved.Visible = false;

                        txtInstallBookingDate.Enabled = false;
                        //      ImageInstallBookingDate.Enabled = false;
                        //rblAM1.Enabled = false;
                        //lblAM1.Attributes.Add("style", "pointer-events: none;");
                        rblAM1.Attributes.Add("onclick", "return false;");
                        rblAM2.Attributes.Add("onclick", "return false;");
                        rblPM1.Attributes.Add("onclick", "return false;");
                        rblPM2.Attributes.Add("onclick", "return false;");
                        //rblAM2.Enabled = false;
                        //rblPM1.Enabled = false;
                        //rblPM2.Enabled = false;
                        txtInstallDays.Enabled = false;
                    }
                    txtInstallerNotes.Text = st.InstallerNotes;
                    try
                    {
                        txtInstallAddressline.Text = st.InstallAddress + ", " + st.InstallCity + " " + st.InstallState + " " + st.InstallPostCode;
                    }
                    catch { }
                    txtInstallAddress.Text = st.InstallAddress;
                    txtformbayUnitNo.Text = st.unit_number;
                    try
                    {
                        ddlformbayunittype.SelectedValue = st.unit_type;
                    }
                    catch { }
                    txtformbayStreetNo.Text = st.street_number;
                    txtformbaystreetname.Text = st.street_name;
                    ddlformbaystreettype.SelectedValue = st.street_type;
                    txtInstallCity.Text = st.InstallCity;
                    txtInstallState.Text = st.InstallState;
                    txtInstallPostCode.Text = st.InstallPostCode;

                    hndstreetname.Value = st.street_name;
                    hndstreetno.Value = st.street_number;
                    hndstreetsuffix.Value = st.street_suffix;
                    hndstreettype.Value = st.street_type;
                    hndunitno.Value = st.unit_number;
                    hndunittype.Value = st.unit_type;
                    hndaddress.Value = st.InstallAddress;

                    if (st.IsFormBay == "2")
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return false;");
                        //cbkIsFormBay.Enabled = false;
                    }
                    else
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return true;");
                        //cbkIsFormBay.Enabled = true;
                        if (st.IsFormBay == "1")
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return true;");
                            //cbkIsFormBay.Checked = true;
                        }
                        else
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return false;");
                            //cbkIsFormBay.Checked = false;
                        }
                    }

                    if (st.PanelBrandID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName;
                        // hdnStockItem.Value = st.NumberPanels.ToString();
                        //hdnSysDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter.";
                        //  hdnStockItem.Value = st.NumberPanels.ToString();
                        //hdnSysDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "" && st.SecondInverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter. Plus Second Inverter" + st.SecondInverterDetails;
                        //  hdnStockItem.Value = st.NumberPanels.ToString();
                        // hdnSysDetails.Value = txtSystemDetails.Text.ToString();
                    }

                    try
                    {
                        ddlInstaller.SelectedValue = st.Installer;
                    }
                    catch { }
                    try
                    {
                        ddlDesigner.SelectedValue = st.Designer;
                    }
                    catch { }
                    try
                    {
                        ddlElectrician.SelectedValue = st.Electrician;
                    }
                    catch { }

                    if (st.StockAllocationStore != string.Empty)
                    {
                        try
                        {
                            ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        }
                        catch { }
                    }
                    else
                    {
                        if (st.InstallState != string.Empty)
                        {
                            DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(st.InstallState);
                            if (dtState.Rows.Count > 0)
                            {
                                ddlStockAllocationStore.SelectedValue = dtState.Rows[0]["CompanyLocationID"].ToString();
                            }
                        }
                    }
                    rblAM1.Checked = Convert.ToBoolean(st.InstallAM1);
                    rblAM2.Checked = Convert.ToBoolean(st.InstallAM2);
                    rblPM1.Checked = Convert.ToBoolean(st.InstallPM1);
                    rblPM2.Checked = Convert.ToBoolean(st.InstallPM2);

                    try
                    {
                        txtInstallBookingDate.Text = Convert.ToDateTime(st.InstallBookingDate).ToShortDateString();
                    }
                    catch { }
                }
            }
            if (st.Installer != string.Empty && st.InstallBookingDate != string.Empty)
            {
                if (st.FormbayId != string.Empty)
                {
                    btnformbay1.Enabled = false;
                }
                else
                {
                    btnformbay1.Enabled = true;
                }
            }
            else
            {
                btnformbay1.Enabled = true;
            }
            //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(proid);
            DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(proid, "1");
            if (dtPickList.Rows.Count > 0)
            {
                divPickList.Visible = true;
                rptPickList.DataSource = dtPickList;
                rptPickList.DataBind();
                btnDownloadPickList.Visible = true;
            }
            else
            {
                divPickList.Visible = false;
                btnDownloadPickList.Visible = false;
            }
        }
    }
    public void BindDropDown()
    {
        ListItem item = new ListItem();
        item.Value = "";
        item.Text = "Select";
        ddlStockAllocationStore.Items.Clear();
        ddlStockAllocationStore.Items.Add(item);

        ddlStockAllocationStore.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlStockAllocationStore.DataValueField = "CompanyLocationID";
        ddlStockAllocationStore.DataTextField = "CompanyLocation";
        ddlStockAllocationStore.DataMember = "CompanyLocation";
        ddlStockAllocationStore.DataBind();

        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlElectrician.Items.Clear();
        ddlElectrician.Items.Add(item3);

        ddlElectrician.DataSource = ClstblContacts.tblContacts_SelectElectrician();
        ddlElectrician.DataValueField = "ContactID";
        ddlElectrician.DataTextField = "Contact";
        ddlElectrician.DataMember = "Contact";
        ddlElectrician.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlDesigner.Items.Clear();
        ddlDesigner.Items.Add(item4);

        ddlDesigner.DataSource = ClstblContacts.tblContacts_SelectDesigner();
        ddlDesigner.DataValueField = "ContactID";
        ddlDesigner.DataTextField = "Contact";
        ddlDesigner.DataMember = "Contact";
        ddlDesigner.DataBind();

        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();
        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();

        if (txtInstallBookingDate.Text.Trim() != string.Empty)
        {
            string date1 = txtInstallBookingDate.Text.Trim();
            string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);
            //   Response.Write(date1+"="+ date2);
            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();
        }
        else
        {
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();
        }

        ListItem itemM = new ListItem();
        itemM.Text = "Project";
        itemM.Value = "";
        ddlMoveProject.Items.Clear();
        ddlMoveProject.Items.Add(itemM);

        //string proid = HiddenField2.Value;
        string proid = "";
        if (string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            proid = HiddenField2.Value;
        }
        else
        {
            proid = Request.QueryString["proid"];
        }
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        if (st.PanelBrandID != string.Empty && st.InverterDetailsID != string.Empty)
        {
            ddlMoveProject.DataSource = ClstblProjects.tblProjects_SelectMove(st.PanelBrandID, st.InverterDetailsID);
            ddlMoveProject.DataValueField = "ProjectID";
            ddlMoveProject.DataTextField = "Project";
            ddlMoveProject.DataMember = "Project";
            ddlMoveProject.DataBind();
        }
    }

    protected void btnUpdatePreInst_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
        string ProjectID = "";
        // installbookingtracker.openModal = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {

            //string ProjectID = Request.QueryString["proid"];
            string InstallBookingDate = txtInstallBookingDate.Text.Trim();
            string InstallDays = txtInstallDays.Text;
            string ElecDistOK = Convert.ToString(chkElecDistOK.Checked);
            string InstallerNotes = txtInstallerNotes.Text;
            string ElecDistApproved = txtElecDistApproved.Text.Trim();

            string formbayUnitNo = txtformbayUnitNo.Text;
            string formbayunittype = ddlformbayunittype.SelectedValue;
            string formbayStreetNo = txtformbayStreetNo.Text;
            string formbaystreetname = txtformbaystreetname.Text;
            string formbaystreettype = ddlformbaystreettype.SelectedValue;

            txtInstallAddress.Text = formbayUnitNo + " " + formbayunittype + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
            string InstallAddress = txtInstallAddress.Text;

            string InstallCity = txtInstallCity.Text;
            string InstallState = txtInstallState.Text;
            string InstallPostCode = txtInstallPostCode.Text;
            string Installer = ddlInstaller.SelectedValue;
            string Designer = ddlDesigner.SelectedValue;
            string Electrician = ddlElectrician.SelectedValue;
            string StockAllocationStore = ddlStockAllocationStore.SelectedValue;
            string InstallAM1 = Convert.ToString(rblAM1.Checked);
            string InstallAM2 = Convert.ToString(rblAM2.Checked);
            string InstallPM1 = Convert.ToString(rblPM1.Checked);
            string InstallPM2 = Convert.ToString(rblPM2.Checked);
            string IsFormBay = "";
            string project = InstallCity + "-" + InstallAddress;
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (ddlInstaller.SelectedValue != null && ddlInstaller.SelectedValue != "")
            {
                if (txtInstallBookingDate.Text != null && txtInstallBookingDate.Text != "")
                {
                    DataTable dt = ClsProjectSale.GetInstallerData(ddlInstaller.SelectedValue, txtInstallBookingDate.Text);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count > 2)
                        {
                            string ProjectNumber = "";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                ProjectNumber += dt.Rows[i]["ProjectNumber"].ToString() + ",";
                            }
                            lblmsg.Text = "This Installer " + dt.Rows[0]["InstallerName"].ToString() + " Is Already Booked on " + txtInstallBookingDate.Text + " for Projectnumber " + ProjectNumber.TrimEnd(',');
                            ModalPopupExtender5.Show();
                            return;
                        }
                    }
                }
            }

            //if (stPro.StockAllocationStore != null && stPro.StockAllocationStore != "")
            //{
            //    if (stPro.StockAllocationStore!=ddlStockAllocationStore.SelectedValue)
            //    {
            //        StockAllocationStore = ddlStockAllocationStore.SelectedValue;

            //    }

            //}

            if (txtInstallBookingDate.Text != null && txtInstallBookingDate.Text != "")
            {
                string InstallDate = Convert.ToDateTime(txtInstallBookingDate.Text).ToShortDateString();
                string EndDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).AddDays(28).ToShortDateString();
                bool s1 = ClsProjectSale.tblprojects_UpdateInstallEndDate(ProjectID, EndDate);
            }
            if (cbkIsFormBay.Checked == true)
            {
                IsFormBay = "1";
            }
            else
            {
                IsFormBay = "2";
            }
            string CustId = "";
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                CustId = Request.QueryString["compid"];
            }
            else
            {
                HiddenField hdn_custpopup = this.Parent.FindControl("hdn_custpopup") as HiddenField;
                if (hdn_custpopup.Value != string.Empty)
                {
                    CustId = hdn_custpopup.Value;
                }
            }

            int existcustomer = ClstblCustomers.tblCustomers_ExitsByID_Address(CustId, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
            if (existcustomer > 0)
            {
                PanAddUpdate.Visible = true;
                ModalPopupExtenderAddress.Show();
                //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustId, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
                //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
                // rptaddress.DataSource = dt;
                rptaddress.DataBind();
            }
            else
            {
                string CustID = "";
                if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
                {
                    CustID = Request.QueryString["compid"].ToString();
                }
                else
                {
                    HiddenField hdn_custpopup = this.Parent.FindControl("hdn_custpopup") as HiddenField;
                    if (hdn_custpopup.Value != string.Empty)
                    {
                        CustID = hdn_custpopup.Value;
                    }
                }
                //SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                //bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);
                bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);
                bool succ = ClstblCustomers.tblCustomer_Update_Address(CustID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);

                ClsProjectSale.tblInstallBookings_Delete(ProjectID);
                if (txtInstallBookingDate.Text.Trim() != string.Empty && (rblAM1.Checked == true || rblAM2.Checked == true || rblPM1.Checked == true || rblPM2.Checked == true))
                {
                    int InstallDay = 1;
                    if (txtInstallDays.Text != string.Empty)
                    {
                        InstallDay = Convert.ToInt32(txtInstallDays.Text);
                    }
                    DateTime InstallDate = Convert.ToDateTime(txtInstallBookingDate.Text.Trim());
                    for (int i = 0; i < InstallDay; i++)
                    {
                        string AMPM = "";
                        if (rblAM1.Checked == true)
                        {
                            AMPM = "AM1";
                            DateTime InstallDate1 = InstallDate.AddDays(i);
                            int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                        }
                        if (rblAM2.Checked == true)
                        {
                            AMPM = "AM2";
                            DateTime InstallDate1 = InstallDate.AddDays(i);
                            int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                        }
                        if (rblPM1.Checked == true)
                        {
                            AMPM = "PM1";
                            DateTime InstallDate1 = InstallDate.AddDays(i);
                            int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                        }
                        if (rblPM2.Checked == true)
                        {
                            AMPM = "PM2";
                            DateTime InstallDate1 = InstallDate.AddDays(i);
                            int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(InstallDate1), ProjectID, AMPM, "");
                        }
                    }
                }

                if (txtElecDistApproved.Text == string.Empty)
                {
                    RequiredFieldValidatorEDA.Visible = true;
                }
                else
                {
                    RequiredFieldValidatorEDA.Visible = false;
                }

                bool sucPreInst = false;
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string UpdatedBy = stEmp.EmployeeID;

                /* =========================== Mail =========================== */

                StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = st.from;
                SiteURL = st.siteurl;

                if (ddlSendMail.SelectedValue == "3") // Booking Cancel Mail
                {
                    if (txtInstallBookingDate.Text.Trim() == string.Empty && ddlInstaller.SelectedValue != string.Empty)
                    {
                        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(ddlInstaller.SelectedValue);

                        TextWriter txtWriter = new StringWriter() as TextWriter;
                        string mailto = stCont.ContEmail;
                        string subject = "Installation Cancel Updates from Arise SOLAR";
                        string Address = stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + " - " + stPro.InstallPostCode;
                        string MobileNo = "";
                        string EmailAddress = "";

                        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(stPro.CustomerID);
                        if (dtCont.Rows.Count > 0)
                        {
                            MobileNo = dtCont.Rows[0]["ContMobile"].ToString();
                            EmailAddress = dtCont.Rows[0]["ContEmail"].ToString();
                        }
                        else
                        {
                            MobileNo = "-";
                            EmailAddress = "-";
                        }
                        Server.Execute("~/mailtemplate/instcancel.aspx?InstallerName=" + stPro.InstallerName + "&InstallBookingDate=" + stPro.InstallBookingDate + "&ProjectNumber=" + stPro.ProjectNumber + "&Customer=" + stPro.Customer + "&Address=" + Address + "&MobileNo=" + MobileNo + "&EmailAddress=" + EmailAddress + "&SystemDetails=" + stPro.SystemDetails + "&InstallationNote=" + stPro.InstallerNotes, txtWriter);
                        try
                        {
                            Utilities.SendMail(from, mailto, subject, txtWriter.ToString(), "cc:warehouse");
                        }
                        catch
                        {
                        }
                    }
                }
                /* ============================================================ */

                /* =========================== Project Move Mail =========================== */

                if (ddlMoveProject.SelectedValue != string.Empty)
                {
                    SttblProjects stPM = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

                    TextWriter txtWriter = new StringWriter() as TextWriter;
                    string mailto = "";
                    string subject = "Project Move Updates from Arise SOLAR";

                    string Project = stPro.ProjectNumber + " : " + stPro.Project;
                    string ProjectMove = stPM.ProjectNumber + " : " + stPM.Project;

                    Server.Execute("~/mailtemplate/warehouse.aspx?Project=" + Project + "&ProjectMove=" + ProjectMove, txtWriter);
                    try
                    {
                        Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                    }
                    catch
                    {
                    }
                }
                /* ========================================================================= */

                if (chkElecDistOK.Checked == true && txtElecDistApproved.Text != "")
                {
                    if ((rblAM1.Checked != false || rblAM2.Checked != false || rblPM1.Checked != false || rblPM2.Checked != false) && txtInstallDays.Text != "")
                    {
                        if (UpdatedBy == null)
                        {
                            UpdatedBy = "";
                        }
                        sucPreInst = ClsProjectSale.tblProjects_UpdatePreInst(ProjectID, InstallBookingDate, InstallDays, ElecDistOK, InstallerNotes, ElecDistApproved, InstallAddress, InstallCity, InstallState, InstallPostCode, Installer, Designer, Electrician, "", "False", StockAllocationStore, InstallAM1, InstallAM2, InstallPM1, InstallPM2, UpdatedBy, IsFormBay);

                        bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value, project);
                        //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);

                        bool succ_street1 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);
                        //bool succ_street1 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);

                        string formbayinstalldate = "";
                        if (InstallBookingDate != string.Empty)
                        {
                            formbayinstalldate = InstallBookingDate;
                        }
                        else //if null than get next sunday date
                        {
                            if (chkElecDistOK.Checked == true)
                            {
                                DateTime InstallDate = DateTime.Now;
                                DayOfWeek dayOfWeek = DayOfWeek.Sunday;
                                int start = (int)InstallDate.DayOfWeek;
                                int target = (int)dayOfWeek;
                                if (target <= start)
                                    target += 7;
                                formbayinstalldate = (InstallDate.AddDays(target - start)).ToString();
                            }
                        }
                        ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, formbayinstalldate);
                    }
                    else
                    {
                        DivInstalldetails.Visible = true;
                    }
                }
                else
                {
                    sucPreInst = ClsProjectSale.tblProjects_UpdatePreInst(ProjectID, InstallBookingDate, InstallDays, ElecDistOK, InstallerNotes, ElecDistApproved, InstallAddress, InstallCity, InstallState, InstallPostCode, Installer, Designer, Electrician, "", "False", StockAllocationStore, InstallAM1, InstallAM2, InstallPM1, InstallPM2, UpdatedBy, IsFormBay);

                    //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
                    bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value, project);

                    //bool succ_street2 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);
                    bool succ_street2 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);

                    string formbayinstalldate = "";
                    if (InstallBookingDate != string.Empty)
                    {
                        formbayinstalldate = InstallBookingDate;
                    }
                    else //if null than get next sunday date
                    {
                        if (chkElecDistOK.Checked == true)
                        {
                            DateTime InstallDate = DateTime.Now;
                            DayOfWeek dayOfWeek = DayOfWeek.Sunday;
                            int start = (int)InstallDate.DayOfWeek;
                            int target = (int)dayOfWeek;
                            if (target <= start)
                                target += 7;
                            formbayinstalldate = (InstallDate.AddDays(target - start)).ToString();
                        }
                    }
                    ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, formbayinstalldate);
                }
                //Project Status cant change if Project in "Mtn-New Inquiry"
                SttblProjects stmntc = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                if (stmntc.ProjectStatusID != "15")
                {
                    /* -------------------- Job Booked -------------------- */
                    if (txtInstallBookingDate.Text.Trim() != string.Empty)
                    {
                        if (stPro.ProjectStatusID == "3")
                        {
                            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "11");
                            if (dtStatus.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, UpdatedBy, stPro.NumberPanels);
                            }
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(stPro.ProjectStatusID) > 11 || stPro.ProjectStatusID == "10" || stPro.ProjectStatusID == "5")
                        {
                            RequiredFieldValidatorIBDate.Visible = true;
                        }
                        else
                        {
                            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                            if (dtStatus.Rows.Count > 0)
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, stPro.NumberPanels);
                            }
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    /* ---------------------------------------------------- */
                }
                /* =========================== Mail =========================== */
                if (ddlSendMail.SelectedValue == "1")  // Customer Mail
                {
                    if (txtInstallBookingDate.Text.Trim() != string.Empty && ddlInstaller.SelectedValue != string.Empty)
                    {
                        string CustomerID = stPro.CustomerID;
                        string Customer = stPro.Customer;

                        if (CustomerID != string.Empty)
                        {
                            SttblContacts stContC = ClstblContacts.tblContacts_SelectByContactID(Installer);
                            string InstallerName = stContC.ContFirst + ' ' + stContC.ContLast;

                            TextWriter txtWriter = new StringWriter() as TextWriter;
                            string mailtoC = stContC.ContEmail;
                            string subjectC = "Installation Updates from Arise SOLAR";

                            Server.Execute("~/mailtemplate/custbookmail.aspx?Customer=" + Customer + "&InstallBookingDate=" + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(InstallBookingDate)), txtWriter);
                            try
                            {
                                Utilities.SendMail(from, mailtoC, subjectC, txtWriter.ToString());
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                if (ddlSendMail.SelectedValue == "2") // Installer Mail
                {
                    if (txtInstallBookingDate.Text.Trim() != string.Empty && ddlInstaller.SelectedValue != string.Empty)
                    {
                        string CustomerID = stPro.CustomerID;
                        string Customer = stPro.Customer;

                        if (Installer != string.Empty)
                        {
                            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
                            SttblContacts stContI = ClstblContacts.tblContacts_SelectByContactID(Installer);

                            TextWriter txtWriterI = new StringWriter() as TextWriter;
                            string mailtoI = stContI.ContEmail;
                            string subjectI = "Installation Updates from Arise SOLAR";

                            if (rblAM1.Checked == true)
                            {
                                InstallAM1 = ", AM1";
                            }
                            if (rblPM1.Checked == true)
                            {
                                InstallPM1 = ", PM1";
                            }
                            if (rblAM2.Checked == true)
                            {
                                InstallAM2 = ", AM2";
                            }
                            if (rblPM2.Checked == true)
                            {
                                InstallPM2 = ", PM2";
                            }
                            InstallDays = ", Install Days: " + txtInstallDays.Text;

                            string InstallationDate = InstallBookingDate + InstallAM1 + InstallPM1 + InstallAM2 + InstallPM2 + InstallDays;
                            string Address = stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + " - " + stPro.InstallPostCode;

                            string MobileNo = "";
                            string EmailAddress = "";

                            DataTable dtCont = ClstblContacts.tblContacts_SelectTop(CustomerID);
                            if (dtCont.Rows.Count > 0)
                            {
                                MobileNo = dtCont.Rows[0]["ContMobile"].ToString();
                                EmailAddress = dtCont.Rows[0]["ContEmail"].ToString();
                            }
                            else
                            {
                                MobileNo = "-";
                                EmailAddress = "-";
                            }

                            Server.Execute("~/mailtemplate/instbookmail.aspx?InstallerName=" + stPro.InstallerName + "&InstallationDate=" + InstallationDate + "&ProjectNumber=" + stPro.ProjectNumber + "&Customer=" + Customer + "&Address=" + Address + "&MobileNo=" + MobileNo + "&EmailAddress=" + EmailAddress + "&SystemDetails=" + stPro.SystemDetails + "&InstallationNote=" + stPro.InstallerNotes, txtWriterI);
                            try
                            {
                                Utilities.SendMail(from, mailtoI, subjectI, txtWriterI.ToString());
                            }
                            catch
                            {
                            }
                        }
                    }
                }

                //if (stPro.ProjectTypeID == "8")
                //{
                //    if (ddlInstaller.SelectedValue != string.Empty)
                //    {
                //        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "16");//Technician Assign
                //    }
                //    if (txtInstallBookingDate.Text != string.Empty)
                //    {
                //        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "17");//Schedule for MTCE
                //    }
                //}

                /* ============================================================ */
                //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);


                //string formbayinstalldate = "";
                //if (InstallBookingDate != string.Empty)
                //{
                //    formbayinstalldate = InstallBookingDate;
                //}
                //else //if null than get next sunday date
                //{
                //    DateTime InstallDate = Convert.ToDateTime(InstallBookingDate);
                //    DayOfWeek dayOfWeek = DayOfWeek.Sunday;
                //    int start = (int)InstallDate.DayOfWeek;
                //    int target = (int)dayOfWeek;
                //    if (target <= start)
                //        target += 7;
                //    formbayinstalldate = (InstallDate.AddDays(target - start)).ToString();
                //}
                //ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, formbayinstalldate);

                BindProjects();
                if (sucPreInst)
                {


                    if (chkquickform.Checked == true)
                    {
                        bool check = ClstblProjects.tblProjects_update_IsQuickForm("True", Request.QueryString["proid"]);

                    }
                    else
                    {
                        bool check = ClstblProjects.tblProjects_update_IsQuickForm("False", Request.QueryString["proid"]);
                        bool ISExport = ClstblProjects.tbl_projects_update_IsExport_zeroByProjectID(Request.QueryString["proid"]);
                    }
                    SetAdd1();
                    //PanSuccess.Visible = true;
                    DivInstalldetails.Visible = false;
                    try
                    {

                    }
                    catch { }
                    AjaxControlToolkit.ModalPopupExtender m = this.Parent.FindControl("ModalPopupExtender1") as AjaxControlToolkit.ModalPopupExtender;
                    // AjaxControlToolkit hdnclosepopup = this.Page.Parent.FindControl("hdnclosepopup") as AjaxControlToolkit;
                    HiddenField hdnclosepopup = this.Parent.FindControl("hdnclosepopup") as HiddenField;

                    if (hdnclosepopup != null)
                    {
                        hdnclosepopup.Value = "2";
                        m.Hide();
                        //GridView GridView1 = (GridView)this.Parent.FindControl("GridView1");
                        //GridView1.DataBind();
                        //Page.ClientScript.(this.GetType(), "close", "<script language=javascript>window.opener.location.reload(true);self.close();</script>");
                    }
                }
                else
                {
                    PanSuccess.Visible = false;
                    DivInstalldetails.Visible = true;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
                {
                    BindProjectPreInst(Request.QueryString["proid"]);
                    panelPickListBtn.Enabled = true;
                }
                else
                {
                    BindProjectPreInst(ProjectID);
                }

                BindProjectPreInst(ProjectID);
            }
        }
    }

    private void BindGrid(int v)
    {
        //throw new NotImplementedException();
    }

    protected void btnCustAddress_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            //string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
            txtInstallAddress.Text = st.StreetAddress;
            txtformbayUnitNo.Text = st.unit_number;
            ddlformbayunittype.SelectedValue = st.unit_type;
            txtformbayStreetNo.Text = st.street_number;
            txtformbaystreetname.Text = st.street_name;
            ddlformbaystreettype.SelectedValue = st.street_type;
            txtInstallCity.Text = st.StreetCity;
            txtInstallState.Text = st.StreetState;
            txtInstallPostCode.Text = st.StreetPostCode;
            txtInstallAddressline.Text = st.street_address;
        }
    }
    protected void ddlInstaller_SelectedIndexChanged(object sender, EventArgs e)
    {
        //UserControlButtonClicked(sender);
        BindScript();
        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        PanAddUpdate.Visible = true;
        if (ddlInstaller.SelectedValue != string.Empty)
        {
            // Response.Write(ddlInstaller.SelectedValue);
            DataTable dtDes = ClstblContacts.tblContacts_SelectDesignerByContactID(ddlInstaller.SelectedValue);
            if (dtDes.Rows.Count > 0)
            {
                ddlDesigner.SelectedValue = ddlInstaller.SelectedValue;
            }
            else
            {
                ddlDesigner.SelectedValue = "";
            }

            DataTable dtEle = ClstblContacts.tblContacts_SelectElectricianByContactID(ddlInstaller.SelectedValue);
            if (dtEle.Rows.Count > 0)
            {
                ddlElectrician.SelectedValue = ddlInstaller.SelectedValue;
            }
            else
            {
                ddlElectrician.SelectedValue = "";
            }
        }
        else
        {
            ddlDesigner.SelectedValue = "";
            ddlElectrician.SelectedValue = "";
        }
    }

    protected void btnPickList_Click(object sender, EventArgs e)
    {

        //try
        //{
        string ProjectID = "";
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        //int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID(ProjectID);
        int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
        if (PickListExist == 0)
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtreason.Text, txtnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
            if (success > 0)
            {
                //Telerik_reports.generate_PickList(ProjectID);
                picklistdiv.Visible = false;
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            hdnPickListLogID.Value = string.Empty;
            txtreason.Text = string.Empty;
            txtnote.Text = string.Empty;
            txtreason.Enabled = true;
            txtnote.Enabled = true;
            picklistdiv.Visible = true;
            btnPickList.Attributes.Add("disabled", "disabled");
            btnDownload.Style.Add("display", "none");
            btnSubmit.Attributes.Remove("disabled");
        }

        //}
        //catch
        //{
        //}

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //try
        //{
        string ProjectID = "";
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        if (!string.IsNullOrEmpty(txtnote.Text) && !string.IsNullOrEmpty(txtreason.Text))
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtreason.Text, txtnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
            //            
            if (success > 0)
            {
                hdnPickListLogID.Value = success.ToString();
                //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
                DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");
                if (dtPickList.Rows.Count > 0)
                {
                    divPickList.Visible = true;
                    rptPickList.DataSource = dtPickList;
                    rptPickList.DataBind();
                }
                else
                {
                    divPickList.Visible = false;
                }
                //ClientScriptManager cs = Page.ClientScript;
                //cs.RegisterStartupScript(this.GetType(), "modalstuff", "$('#<%=btnHidden.ClientID %>').click();", true);
                // Telerik_reports.generate_PickList(ProjectID);
                txtreason.Enabled = false;
                txtnote.Enabled = false;
                btnSubmit.Attributes.Add("disabled", "disabled");
                // btnSubmit.Enabled = false;
                btnDownload.Style.Add("display", "");
                SetAdd1();
            }
            else
            {
                txtreason.Enabled = true;
                txtnote.Enabled = true;
                btnSubmit.Enabled = true;
                btnDownload.Style.Add("display", "none");
                SetError1();
            }
        }

        //}
        //catch
        //{
        //}
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        try
        {
            Telerik_reports.generate_PickList(ProjectID);
        }
        catch
        {
            if (!string.IsNullOrEmpty(hdnPickListLogID.Value))
            {
                SetError1();
                picklistdiv.Visible = false;
                btnPickList.Attributes.Remove("disabled");
                bool suc = ClstblProjects.tbl_PickListLog_DeleteByID(hdnPickListLogID.Value);

                //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
                DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");
                if (dtPickList.Rows.Count > 0)
                {
                    divPickList.Visible = true;
                    rptPickList.DataSource = dtPickList;
                    rptPickList.DataBind();
                }
                else
                {
                    divPickList.Visible = false;
                }

            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        picklistdiv.Visible = false;
        btnPickList.Attributes.Remove("disabled");

        if (!string.IsNullOrEmpty(hdnPickListLogID.Value))
        {
            bool suc = ClstblProjects.tbl_PickListLog_DeleteByID(hdnPickListLogID.Value);

            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }
            else
            {
                ProjectID = HiddenField2.Value;
            }

            //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
            DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");

            if (dtPickList.Rows.Count > 0)
            {
                divPickList.Visible = true;
                rptPickList.DataSource = dtPickList;
                rptPickList.DataBind();
            }
            else
            {
                divPickList.Visible = false;
            }

        }
    }

    public void HTMLExportToPDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    //pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdfheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    string HeaderHtml = txtWriter.ToString();
        //    string FooterHtml = txtWriter2.ToString();

        //    client.setHeaderHtml(HeaderHtml);
        //    client.setFooterHtml(FooterHtml);
        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }
    protected void btnRemoveInst_Click(object sender, EventArgs e)
    {
        //ModalPopupStockUpdate.Show();
        ddlstockstatus.SelectedValue = "";
        txtDate.Text = string.Empty;
        txtNotes.Text = string.Empty;
        txtProjNo.Text = string.Empty;
        //txtProjNo.Visible = false;
        modalstockupdate.Show();
    }
    protected void txtInstallBookingDate_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        if (txtInstallBookingDate.Text.Trim() != string.Empty)
        {
            if (txtInstallDays.Text.Trim() != string.Empty)
            {
                string date1 = txtInstallBookingDate.Text.Trim();
                string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

                ListItem item2 = new ListItem();
                item2.Text = "Installer";
                item2.Value = "";
                ddlInstaller.Items.Clear();
                ddlInstaller.Items.Add(item2);

                ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
                ddlInstaller.DataValueField = "ContactID";
                ddlInstaller.DataTextField = "Contact";
                ddlInstaller.DataMember = "Contact";
                ddlInstaller.DataBind();
            }
        }
    }
    protected void txtInstallDays_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        if (txtInstallDays.Text != string.Empty)
        {
            if (txtInstallBookingDate.Text != string.Empty)
            {
                string date1 = txtInstallBookingDate.Text.Trim();
                string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

                ListItem item2 = new ListItem();
                item2.Text = "Installer";
                item2.Value = "";
                ddlInstaller.Items.Clear();
                ddlInstaller.Items.Add(item2);

                ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
                ddlInstaller.DataValueField = "ContactID";
                ddlInstaller.DataTextField = "Contact";
                ddlInstaller.DataMember = "Contact";
                ddlInstaller.DataBind();
            }
        }
    }
    protected void txtInstallCity_TextChanged(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        PanAddUpdate.Visible = true;
        string[] cityarr = txtInstallCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            txtInstallCity.Text = cityarr[0].Trim();
            txtInstallState.Text = cityarr[1].Trim();
            txtInstallPostCode.Text = cityarr[2].Trim();
        }
        else
        {
            txtInstallState.Text = string.Empty;
            txtInstallPostCode.Text = string.Empty;
        }
    }
    protected void btnMove_Click(object sender, EventArgs e)
    {
        chkElecDistOK.Checked = false;
        divElecDistApproved.Visible = false;
        txtElecDistApproved.Text = string.Empty;
        txtInstallBookingDate.Text = string.Empty;
        rblAM1.Checked = false;
        rblAM2.Checked = false;
        rblPM1.Checked = false;
        rblPM2.Checked = false;
        txtInstallDays.Text = "1";
        txtInstallerNotes.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        ddlDesigner.SelectedValue = "";
        ddlElectrician.SelectedValue = "";
    }


    async Task RunAsync()
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
            SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
            SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
            string ProjectTypeName = "New";
            if (stProject.ProjectTypeID == "3")
            {
                ProjectTypeName = "Adding";
            }
            if (stProject.ProjectTypeID == "7")
            {
                ProjectTypeName = "Replacing";
            }
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
            SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
            SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
            DataTable dt = ClstblCustomers.tbl_formbayunittypeID_SelectByUnittype(stcustomer.unit_type);
            DataTable dt2 = ClstblCustomers.tbl_formbayStreettypeID_SelectByStreetCode(stcustomer.street_type);
            string installbooking = stProject.InstallBookingDate;
            Decimal stcrate = 0;
            if (!string.IsNullOrEmpty(installbooking))
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt3 = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                stcrate = Convert.ToDecimal(dt3.Rows[0]["STCRate"]);
            }
            string Unitid = "";
            String StreetId = "";
            if (dt.Rows.Count > 0)
            {
                Unitid = dt.Rows[0]["Id"].ToString();
            }
            if (dt2.Rows.Count > 0)
            {
                StreetId = dt2.Rows[0]["GreenBoatStreetType"].ToString();
            }

            string SystemMountingType = "Ground mounted or free standing";
            string typeofconnection = "Connected to an electricity grid with battery storage";
            if (stProject.InstallBase == "1")
            {
                SystemMountingType = "Building or structure";
            }
            if (Convert.ToBoolean(stProject.GridConnected))
            {
                typeofconnection = "Connected to an electricity grid without battery storage";
            }
            try
            {
                JObject oJsonObject = new JObject();
                oJsonObject.Add("Username", "arisesolar");
                oJsonObject.Add("Password", "arisesolar1");
                var oTaskPostAsync = await client.PostAsync("https://api.greenbot.com.au/api/Account/Login", new StringContent(oJsonObject.ToString(), Encoding.UTF8, "application/json"));
                bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
                if (IsSuccess)
                {
                    var tokenJson = await oTaskPostAsync.Content.ReadAsStringAsync();
                    RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(tokenJson);
                    string AccessToken = RootObject.TokenData.access_token;

                    litmessage.Text = AccessToken.ToString();
                    client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                    var panellist = new List<Panel>();
                    panellist.Add(new Panel { Brand = stProject.PanelBrand, Model = stProject.PanelModel, NoOfPanel = stProject.NumberPanels });
                    var inverterlist = new List<Inverter>();
                    inverterlist.Add(new Inverter { Brand = stProject.InverterBrand, Model = stProject.InverterModel, Series = stProject.InverterSeries });
                    //var testlist = new List<JobStcDetailsPanelBrand>();
                    //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                    //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                    try
                    {
                        var content = new CreateJob
                        {

                            VendorJobId = Convert.ToInt32(stProject.ProjectNumber),
                            BasicDetails = new BasicDetails
                            {
                                JobType = 1,
                                RefNumber = stProject.ProjectNumber,
                                JobStage = 1,
                                Title = stcustomer.Customer,
                                StrInstallationDate = Convert.ToDateTime(stProject.InstallBookingDate),
                                Priority = 1,
                                Description = stProject.ProjectNotes

                            },
                            JobOwnerDetails = new JobOwnerDetails
                            {

                                OwnerType = "Individual",
                                //CompanyName = "Customer Name",
                                FirstName = stCustContact.ContFirst,
                                LastName = stCustContact.ContLast,
                                Email = stCustContact.ContEmail,
                                Phone = stCustContact.ContMobile,
                                Mobile = stCustContact.ContMobile,
                                AddressID = 1,
                                UnitTypeID = Unitid,
                                UnitNumber = stcustomer.unit_number,
                                StreetName = stcustomer.street_name,
                                StreetNumber = stcustomer.street_number,
                                StreetTypeId = StreetId,
                                Town = stcustomer.StreetCity,
                                State = stcustomer.StreetState,
                                PostCode = stcustomer.StreetPostCode,
                                IsPostalAddress = false
                            },
                            JobInstallationDetails = new JobInstallationDetails
                            {
                                AddressId = 1,
                                UnitTypeID = Unitid,
                                UnitNumber = stcustomer.unit_number,
                                StreetName = stProject.street_name,
                                StreetNumber = stProject.street_number,
                                StreetTypeId = StreetId,
                                Town = stProject.InstallCity,
                                State = stProject.InstallState,
                                PostCode = stProject.InstallPostCode,
                                //isPostalAddress = false,
                                DistributorID = Convert.ToInt32(stdist.GreenBoatDistributor),
                                ElectricityProviderID = Convert.ToInt32(stelect.ElectricityProviderId),
                                NMI = stProject.NMINumber,
                                MeterNumber = stProject.MeterNumber1,
                                PhaseProperty = stProject.MeterPhase,
                                PropertyType = stcustomer.ResCom == "1" ? "Residential" : "Commercial",
                                SingleMultipleStory = sthouse.HouseType,
                                InstallingNewPanel = ProjectTypeName,
                                //NoOfPanels = Convert.ToInt32(stProject.NumberPanels),
                                ExistingSystem = false
                            },
                            JobSystemDetails = new JobSystemDetails
                            {
                                SystemSize = Convert.ToDecimal(stProject.SystemCapKW),
                                //InstallationType = "Other"
                                //SystemModel = "TestModel",
                                //NoOfPanel = 5
                            },
                            JobStcDetails = new JobStcDetails
                            {
                                TypeOfConnection = typeofconnection,
                                SystemMountingType = SystemMountingType,
                                // DeemingPeriod = "13",
                                DeemingPeriod = stcrate.ToString(),
                                MultipleSguAddress = "No"
                            },
                            Panel = panellist.ToArray(),
                            Inverter = inverterlist.ToArray(),
                            InstallerView = new InstallerView
                            {
                                CECAccreditationNumber = stinst.Accreditation,
                                SeDesignRoleId = 1,
                                //Email = "richa@meghtechnologies.com",
                                FirstName = stinst.ContFirst,
                                LastName = stinst.ContLast,
                                Phone = stinst.ContMobile,
                                Mobile = stinst.ContMobile,
                                //ElectricalContractorsLicenseNumber = "9876543214",
                                AddressID = 1,
                                //UnitTypeID = 1,
                                //UnitNumber = "1",
                                StreetName = stcustomerInst.street_name,
                                StreetNumber = stcustomerInst.street_number,
                                StreetTypeId = 1,
                                Town = stcustomerInst.StreetCity,
                                State = stcustomerInst.StreetState,
                                PostCode = stcustomerInst.StreetPostCode,
                                IsPostalAddress = true,
                            }

                        };
                        var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                        // lbltesting.Text = temp.ToString();
                        var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/CreateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                        bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                        var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                        RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);
                        //Response.Write(JsonString + "   " + IsSuccessJob.ToString() + " Message: " + RootObjectJob.Message + " statusCode: " + RootObjectJob.StatusCode + "Status:" + RootObjectJob.Status);
                        //Response.Write(Convert.ToDateTime(stProject.InstallBookingDate));
                        //Response.End();

                        bool GreenBotUpdate = ClstblProjects.tblProjects_update_IsGreenBot("true", ProjectID);

                        litmessage.Text = RootObjectJob.Message;
                        PanError.Visible = true;
                    }
                    catch (Exception e)
                    {
                        litmessage.Text = e.Message;
                        PanError.Visible = true;
                        //Console.WriteLine(e.Message);
                    }
                }


            }
            catch (Exception e)
            {
                litmessage.Text = e.Message;
                //Console.WriteLine(e.Message);
            }
        }
    }
    public void FormbayRunAsync()
    {
        // lbltesting.Text = "hii2";
        try
        {
            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }
            else
            {
                ProjectID = HiddenField2.Value;
            }
            if (ProjectID != string.Empty)
            {
                // lbltesting.Text = "hii3";
                SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                // SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
                // SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
                SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
                string ProjectTypeName = "1";
                if (stProject.ProjectTypeID == "2")
                {
                    ProjectTypeName = "1";
                }
                else if (stProject.ProjectTypeID == "3")
                {
                    ProjectTypeName = "2";
                }
                else if (stProject.ProjectTypeID == "7")
                {
                    ProjectTypeName = "3";
                }

                SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
                //SttblContacts stelect = ClstblContacts.tblContacts_SelectByContactID(stProject.Electrician);
                //SttblContacts stdesig = ClstblContacts.tblContacts_SelectByContactID(stProject.Designer);
                SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
                SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
                SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
                Sttbl_formbaystreettype stStreeType = ClstblStreetType.tbl_formbaystreettype_SelectByStreetCode(stProject.street_type);
                string installbooking = stProject.InstallBookingDate;
                Decimal stcrate = 0;
                if (!string.IsNullOrEmpty(installbooking))
                {
                    string date = Convert.ToDateTime(installbooking).ToShortDateString();
                    DateTime dt1 = Convert.ToDateTime(date);
                    String year = dt1.Year.ToString();
                    DataTable dt3 = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                    stcrate = Convert.ToDecimal(dt3.Rows[0]["STCRate"]);
                }
                string SystemMountingType = "Ground mounted or free standing";
                string typeofconnection = "Connected to an electricity grid with battery storage";
                if (stProject.InstallBase == "1")
                {
                    SystemMountingType = "Building or structure";
                }
                if (Convert.ToBoolean(stProject.GridConnected))
                {
                    typeofconnection = "Connected to an electricity grid without battery storage";
                }
                try
                {
                    //  lbltesting.Text = "hii4";
                    Dictionary<string, string> oJsonObject = new Dictionary<string, string>();
                    /// JObject oJsonObject = new JObject();
                    oJsonObject.Add("Username", "arisesolar");
                    oJsonObject.Add("Password", "Arise@123");
                    oJsonObject.Add("redirect_uri", "http://api.quickforms.com.au/");
                    oJsonObject.Add("client_id", "5D95C4B5-EE01-4D87-B1BC-B4C159CBEFDF");
                    // oJsonObject.Add("scope", "JobData");
                    oJsonObject.Add("scope", "CreateJob");
                    oJsonObject.Add("response_type", "code");

                    HttpClient client = new HttpClient();

                    var content1 = new FormUrlEncodedContent(oJsonObject);
                    var result = client.PostAsync("http://api.quickforms.com.au/auth/logon", content1).Result;
                    var tokenJson = result.Content.ReadAsStringAsync();
                    bool IsSuccess = result.IsSuccessStatusCode;
                    if (IsSuccess)
                    {
                        TokenObject RootObject = JsonConvert.DeserializeObject<TokenObject>(tokenJson.Result);
                        string AccessToken = RootObject.access_token;
                        string CustomerID = RootObject.CustomerUserID;
                        //lbltesting.Text = AccessToken;
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                        var panellist = new List<Panel>();
                        panellist.Add(new Panel { Brand = stProject.PanelBrand, Model = stProject.PanelModel, NoOfPanel = stProject.NumberPanels, STC = stProject.SystemCapKW, size = stProject.PanelOutput });
                        var inverterlist = new List<Inverter>();
                        inverterlist.Add(new Inverter { Brand = stProject.InverterBrand, Model = stProject.InverterModel, Series = stProject.InverterSeries, size = stProject.InverterOutput, noofinverter = Convert.ToInt32(stProject.inverterqty) });
                        var content = new CreateJob
                        {
                            Arisesolarid = Convert.ToInt32(ProjectID),
                            VendorJobId = Convert.ToInt32(stProject.ProjectNumber),
                            BasicDetails = new BasicDetails
                            {
                                JobType = Convert.ToInt32(stcustomer.ResCom),
                                RefNumber = stProject.ProjectNumber,
                                //JobStage = Convert.ToInt32(stcustomer.ResCom),
                                JobStage = Convert.ToInt32(ProjectTypeName),
                                Jobstatus = 1,
                                Createddate = DateTime.Now,
                                StrInstallationDate = Convert.ToDateTime(stProject.InstallBookingDate),
                                Description = stProject.ProjectNotes,
                            },
                            JobOwnerDetails = new JobOwnerDetails
                            {
                                //  OwnerType = "Individual",
                                CompanyName = stCustContact.ContFirst + " " + stCustContact.ContLast,
                                FirstName = stCustContact.ContFirst,
                                LastName = stCustContact.ContLast,
                                Phone = stCustContact.ContMobile,
                                Email = stCustContact.ContEmail,
                                Mobile = stCustContact.ContMobile,
                                // AddressID = 1,
                                UnitTypeID = stProject.unit_type,
                                UnitNumber = stProject.unit_number,
                                StreetName = stProject.street_name,
                                StreetNumber = stProject.street_number,
                                StreetTypeId = stStreeType.StreetCode,
                                StreetAddress = stProject.street_address,
                                Town = stcustomer.StreetCity,
                                State = stcustomer.StreetState,
                                PostCode = stcustomer.StreetPostCode,
                                IsPostalAddress = true,
                            },
                            JobInstallationDetails = new JobInstallationDetails
                            {
                                // AddressId = 1,
                                Inst_UnitNo = stProject.unit_number,
                                Inst_UnitType = stProject.unit_type,
                                Inst_StreetName = stProject.street_name,
                                Inst_StreetNo = stProject.street_number,
                                Inst_StreetType = stStreeType.StreetCode,
                                Inst_StreetAddress = stProject.street_address,
                                Inst_Suburb = stcustomer.StreetCity,
                                Inst_PostCode = stcustomer.StreetPostCode,
                                Inst_State = stcustomer.StreetState,

                                //isPostalAddress = false,
                                //  DistributorID = Convert.ToInt32(stdist.GreenBoatDistributor),
                                // ElectricityProviderID = Convert.ToInt32(stelect.ElectricityProviderId),
                                NMI = stProject.NMINumber,
                                // MeterNumber = stProject.MeterNumber1,
                                //  PhaseProperty = stProject.MeterPhase,
                                //PropertyType = stcustomer.ResCom == "1" ? "Residential" : "Commercial",
                                SingleMultipleStory = sthouse.HouseType,
                                Rooftype = stProject.RoofType,
                                RegPlanNo = stProject.RegPlanNo,
                                LotNo = stProject.LotNumber,

                                // InstallingNewPanel = ProjectTypeName,
                                //NoOfPanels = Convert.ToInt32(stProject.NumberPanels),
                                //  ExistingSystem = false 
                            },
                            JobSystemDetails = new JobSystemDetails
                            {
                                Systemtype = 1,
                                MountingType = 1,
                                ConnectionType = 1,
                                SystemSize = Convert.ToDecimal(stProject.SystemCapKW),
                                //InstallationType = "Other"
                                //SystemModel = "TestModel",
                                //NoOfPanel = 5
                            },
                            JobStcDetails = new JobStcDetails
                            {
                                TypeOfConnection = typeofconnection,
                                SystemMountingType = SystemMountingType,
                                //DeemingPeriod = "13",
                                DeemingPeriod = stcrate.ToString(),
                                MultipleSguAddress = "No"
                            },
                            Panel = panellist.ToArray(),
                            Inverter = inverterlist.ToArray(),
                            InstallerView = new InstallerView
                            {
                                Installerid = stinst.formbayid,
                                FirstName = stinst.ContFirst,
                                LastName = stinst.ContLast,
                                FullName = stProject.InstallerName,
                                //Customerid = 2,
                                Customerid = Convert.ToInt32(CustomerID),
                                CECAccreditationNumber = stinst.Accreditation,
                                SeDesignRoleId = 1,
                                IsCompleteUnit = stProject.ProjectTypeID == "3" ? "No" : "Yes",
                                IsMoreSGU = stProject.ProjectTypeID == "3" ? "No" : "Yes",
                            },
                            ElectricianView = new ElectricianView
                            {
                                FullName = stProject.ElectricianName
                            },
                            DesignerView = new DesignerView
                            {
                                FullName = stProject.DesignerName
                            },
                            quickformGuid = stProject.quickformGuid
                        };

                        var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                        //lbltesting.Text = temp;
                        if (string.IsNullOrEmpty(stProject.quickformGuid))
                        {
                            using (var client1 = new HttpClient())
                            {
                                client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/Createjob/jobData");

                                //HTTP POST
                                var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                                postTask.Wait();

                                var resultdata = postTask.Result;
                                // lbltesting.Text = resultdata.ToString();
                                if (resultdata.IsSuccessStatusCode)
                                {
                                    var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                                    RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                                    //   litmessage.Text = jsondata.ToString();
                                    PanError.Visible = true;
                                    litmessage.Text = JObresult.Message;
                                    if (!string.IsNullOrEmpty(JObresult.guidvalue))
                                    {
                                        bool succ_apicall = ClstblProjects.tblproject_update_quickformFlag(ProjectID, JObresult.guidvalue);
                                        bool check = ClstblProjects.tblProjects_update_IsQuickForm("True", Request.QueryString["proid"]);
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                        //   lbltesting.Text = JObresult.Message;
                                    }
                                    else
                                    {
                                        //   lbltesting.Text = JObresult.Message.ToString();
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (var client1 = new HttpClient())
                            {
                                client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/Updatejob/jobData");

                                //HTTP POST
                                var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                                postTask.Wait();

                                var resultdata = postTask.Result;
                                // lbltesting.Text = resultdata.ToString();
                                if (resultdata.IsSuccessStatusCode)
                                {
                                    var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                                    RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                                    //   litmessage.Text = jsondata.ToString();
                                    PanError.Visible = true;
                                    litmessage.Text = JObresult.Message;
                                    if (!string.IsNullOrEmpty(JObresult.code))
                                    {
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                    }
                                }
                            }
                        }
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    lbltesting.Text = e.Message;
                    //Console.WriteLine(e.Message);
                }
            }

        }
        catch (Exception e)
        {
            lbltesting.Text = e.Message;
            //Console.WriteLine(e.Message);
        }

    }
    protected async void GreenBotSave(object sender, EventArgs e)
    {
        await RunAsync();
    }
    protected void btnformbay1_Click(object sender, EventArgs e)
    {
        // FormbayRunAsync();

    }
    protected void btnquickfrom_Click(object sender, EventArgs e)
    {
        //lbltesting.Text = "dfdhfdhd";
        FormbayRunAsync();
    }

    protected void btnquickfromUpdate_Click(object sender, EventArgs e)
    {
        FormbayRunAsync();
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            string quickformguid = stProject.quickformGuid;
        }
    }

    protected void btnUpdateAddress_Click(object sender, EventArgs e)
    {

        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            //string ProjectID = Request.QueryString["proid"];
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProject.CustomerID);
            SttblEmployees stEmpID = ClstblEmployees.tblEmployees_SelectByEmployeeID(stContact.EmployeeID);

            //if (stProject.IsFormBay == "2")
            {
                ModalPopupExtender1.Show();
                div_popup.Visible = true;

                ddlUnitType.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
                ddlUnitType.DataMember = "UnitType";
                ddlUnitType.DataTextField = "UnitType";
                ddlUnitType.DataValueField = "UnitType";
                ddlUnitType.DataBind();

                ddlstreetType.DataSource = ClstblStreetType.tblStreetType_SelectAsc();
                ddlstreetType.DataMember = "StreetTypeName";
                ddlstreetType.DataTextField = "StreetTypeName";
                ddlstreetType.DataValueField = "StreetTypeCode";
                ddlstreetType.DataBind();
                ddlstreetType.SelectedValue = stProject.street_type.ToUpper().ToString();

                txtUpdateFirstName.Text = stContact.ContFirst;
                txtUpdateLastName.Text = stContact.ContLast;
                txtContMobile.Text = stContact.ContMobile;
                txtCustPhone.Text = stCust.CustPhone;
                txtContEmail.Text = stContact.ContEmail;
                ddlUnitType.SelectedValue = stProject.unit_type.ToString();
                txtUnitNo.Text = stProject.unit_number;
                txtstreetno.Text = stProject.street_number;
                txtStreetName.Text = stProject.street_name;



                txtCity.Text = stProject.InstallCity;
                txtState.Text = stProject.InstallState;
                txtpostcode.Text = stProject.InstallPostCode;


            }
        }
    }
    protected void btnUpdateAddressData_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            //string ProjectID = Request.QueryString["proid"];
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProject.CustomerID);
            SttblEmployees stEmpID = ClstblEmployees.tblEmployees_SelectByEmployeeID(stContact.EmployeeID);
            string formbayUnitNo = txtformbayUnitNo.Text;
            string formbayunittype = ddlformbayunittype.SelectedValue;
            string formbayStreetNo = txtformbayStreetNo.Text;
            string formbaystreetname = txtformbaystreetname.Text;
            string formbaystreettype = ddlformbaystreettype.SelectedValue;

            txtInstallAddress.Text = formbayUnitNo + " " + formbayunittype + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
            string InstallAddress = txtInstallAddress.Text;

            string InstallCity = txtInstallCity.Text;
            string InstallState = txtInstallState.Text;
            string InstallPostCode = txtInstallPostCode.Text;
            string project = InstallCity + "-" + InstallAddress;
            //update data
            ClstblCustomers.tblCustomers_InsertContactsDetails(stProject.CustomerID, txtUpdateFirstName.Text, txtUpdateLastName.Text, txtContMobile.Text, txtContEmail.Text);

            //string suffix="";
            //if(stCust.street_suffix !=string.Empty)
            //{
            //    suffix=stCust.street_suffix;
            //}
            ClstblCustomers.tblCustomer_Update_Address(stProject.CustomerID, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, "");

            bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(stProject.CustomerID, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, "");
            //bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), txtpostaladdressline.Text);
            //bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtStreetAddressLine.Text);chkformbayid.Checked.ToString());
            ClstblCustomers.tblCustomers_UpdateAddressDetails(stProject.CustomerID, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text, txtCity.Text, txtState.Text, txtpostcode.Text, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text, txtCity.Text, txtState.Text, txtpostcode.Text, txtCustPhone.Text);

            bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, "", project);
            bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text);
        }
        BindProjects();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            BindProjectPreInst(Request.QueryString["proid"]);
        }
        else
        {
            BindProjectPreInst(HiddenField2.Value);
        }
        //BindProjectPreInst();
    }
    public void HTMLExportToPDF1(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    //pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    //client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/stcheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    string HeaderHtml = txtWriter.ToString();
        //    string FooterHtml = txtWriter2.ToString();

        //    //client.setHeaderHtml(HeaderHtml);
        //    //client.setFooterHtml(FooterHtml);

        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }



    //protected void btnCreateSTCForm_Click(object sender, ImageClickEventArgs e)
    protected void btnCreateSTCForm_Click(object sender, EventArgs e)
    {
        //string ProjectID = Request.QueryString["proid"];
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        try
        {
            if (st.ProjectTypeID == "3")
            {
                Telerik_reports.generate_stcug(ProjectID);
            }
            else
            {
                Telerik_reports.generate_STCform(ProjectID);

            }


        }
        catch { }

    }

    public void GetPreInstClick()
    {
        BindScript();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            HiddenField2.Value = Request.QueryString["proid"];
            BindProjectPreInst(Request.QueryString["proid"]);
        }
    }
    public void GetPreInstClickByProject(string proid)
    {
        //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
        //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());
        HiddenField2.Value = proid;
        BindProjectPreInst(proid);
    }
    public void BindScript()
    {
        // ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    //public void Closepopup()
    //{
    //    ScriptManager.RegisterStartupScript(btnAdd, this.GetType(), "MyAction", " closemodalpopup()", true);
    //}

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderAddress.Hide();
    }

    protected void ibtnCancel2_Click(object sender, EventArgs e)
    {
        //ModalPopupStockUpdate.Hide();
    }

    protected void rptaddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderAddress.Show();
        rptaddress.PageIndex = e.NewPageIndex;

        string formbayUnitNo = txtformbayUnitNo.Text;
        string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;

        txtInstallAddress.Text = formbayUnitNo + " " + formbayunittype + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
        string InstallAddress = txtInstallAddress.Text;

        string InstallCity = txtInstallCity.Text;
        string InstallState = txtInstallState.Text;
        string InstallPostCode = txtInstallPostCode.Text;

        string CustomerID = Request.QueryString["compid"];
        //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
        //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
        //rptaddress.DataSource = dt;
        // rptaddress.DataBind();
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }
    public void SetItemError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunItemError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void ddlstockstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlstockstatus.SelectedIndex == 3)
        {
            txtProjNo.Text = string.Empty;
            txtprono.Visible = true;
        }
        else
        {
            txtprono.Visible = false;
        }
        modalstockupdate.Show();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            string InstallBookingDate = txtInstallBookingDate.Text;
            string InstallerID = ddlInstaller.SelectedValue;
            string designerid = ddlDesigner.SelectedValue;
            string electricianid = ddlElectrician.SelectedValue;
            chkElecDistOK.Checked = false;
            divElecDistApproved.Visible = false;
            txtElecDistApproved.Text = string.Empty;
            txtInstallBookingDate.Text = string.Empty;
            rblAM1.Checked = false;
            rblAM2.Checked = false;
            rblPM1.Checked = false;
            rblPM2.Checked = false;
            txtInstallDays.Text = "1";
            //txtInstallerNotes.Text = string.Empty;
            ddlInstaller.SelectedValue = "";
            ddlDesigner.SelectedValue = "";
            ddlElectrician.SelectedValue = "";
            //===========================================================================================
            // string InstallBookingDate = txtInstallBookingDate.Text.Trim();
            // string InstallDays = txtInstallDays.Text;
            // string ElecDistOK = Convert.ToString(chkElecDistOK.Checked) = false.ToString();
            string InstallerNotes = txtInstallerNotes.Text;
            //string ElecDistApproved = txtElecDistApproved.Text.Trim();

            string formbayUnitNo = txtformbayUnitNo.Text;
            string formbayunittype = ddlformbayunittype.SelectedValue;
            string formbayStreetNo = txtformbayStreetNo.Text;
            string formbaystreetname = txtformbaystreetname.Text;
            string formbaystreettype = ddlformbaystreettype.SelectedValue;

            txtInstallAddress.Text = formbayUnitNo + " " + formbayunittype + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
            string InstallAddress = txtInstallAddress.Text;

            string InstallCity = txtInstallCity.Text;
            string InstallState = txtInstallState.Text;
            string InstallPostCode = txtInstallPostCode.Text;
            //  string Installer = ddlInstaller.SelectedValue;
            // string Designer = ddlDesigner.SelectedValue;
            // string Electrician = ddlElectrician.SelectedValue;
            string StockAllocationStore = ddlStockAllocationStore.SelectedValue;
            //string InstallAM1 = Convert.ToString(rblAM1.Checked);
            //string InstallAM2 = Convert.ToString(rblAM2.Checked);
            //string InstallPM1 = Convert.ToString(rblPM1.Checked);
            //string InstallPM2 = Convert.ToString(rblPM2.Checked);
            string IsFormBay = "";
            String UpdatedDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
            if (cbkIsFormBay.Checked == true)
            {
                IsFormBay = "1";
            }
            else
            {
                IsFormBay = "2";
            }
            //=============================================================================
            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }
            else
            {
                ProjectID = HiddenField2.Value;
            }
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string UpdatedBy = stEmp.EmployeeID;

            if (ProjectID != string.Empty)
            {
                //string ProjectID = Request.QueryString["proid"];
                ClstblProjects.tblProjects_Update_ProjectStatusID(ProjectID, "3");
                ClsProjectSale.tblProjects_UpdateIsFormBay(ProjectID, "2");
                ClstblProjects.tblProjects_UpdateInstallBookingDate(ProjectID, "");
                ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, "");
            }
            // string UpdatedBy = "";
            bool sucPreInst = false;
            sucPreInst = ClsProjectSale.tblProjects_UpdatePreInst(ProjectID, txtInstallBookingDate.Text, txtInstallDays.Text, chkElecDistOK.Checked.ToString(), InstallerNotes, txtElecDistApproved.Text, InstallAddress, InstallCity, InstallState, InstallPostCode, ddlInstaller.SelectedValue, ddlDesigner.SelectedValue, ddlElectrician.SelectedValue, "", "False", StockAllocationStore, Convert.ToString(rblAM1.Checked), Convert.ToString(rblAM2.Checked), Convert.ToString(rblPM1.Checked), Convert.ToString(rblPM2.Checked), UpdatedBy, IsFormBay);
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (string.IsNullOrEmpty(st.InstallBookingDate))
            {
                int suc1 = ClstblProjects.tblStockUpdate_Insert(ProjectID, st.ProjectNumber, ddlstockstatus.SelectedValue.ToString(), ddlstockstatus.SelectedItem.Text, txtDate.Text, txtNotes.Text, txtProjNo.Text, InstallBookingDate, InstallerID, designerid, electricianid, UpdatedDate, UpdatedBy);
                SetAdd1();
            }
            BindProjects();
        }
        catch (Exception ex)
        {
            SetError1();
        }
    }

    protected void rptattribute_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtQuantity");



        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hdnStockCategoryId");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdbQty = (HiddenField)e.Item.FindControl("hndQty");
        HiddenField hndqty1 = (HiddenField)e.Item.FindControl("hndqty1");
        TextBox txtwallateQty = (TextBox)e.Item.FindControl("txtwallateQty");
        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);

        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();

        ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;

        if (string.IsNullOrEmpty(hndqty1.Value))
        {
            txtOrderQuantity.Text = "0";
        }
        else
        {
            txtOrderQuantity.Text = hndqty1.Value;
        }




        if (e.Item.ItemIndex == 0)
        {
            /// btnaddnew.Attributes.Remove("disabled");
            //litremove.Attributes.Remove("disabled");
            litremove.Visible = false;
        }
        else
        {
            // chkdelete.Visible = true;
            litremove.Visible = true;
        }

        //Wallate Qty Installaer wise
        string Accreditation = "";
        string wallQty = "0";
        txtwallateQty.Text = "0";
        if (ddlinstall2.SelectedValue != string.Empty)
        {
            SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(ddlinstall2.SelectedValue);
            Accreditation = st.Accreditation;
            if (!string.IsNullOrEmpty(Accreditation))
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlinstall2.SelectedValue);
                if (ddlStockItem.SelectedValue != string.Empty)
                {
                    //string StockItemid = ddlStockCategoryID.SelectedValue.ToString();
                    //string Installer = ddlInstaller1.SelectedValue.ToString();
                    if (dtAcno.Rows.Count > 0)
                    {
                        if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                        {
                            string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                            DataTable dt = ClstblProjects.spexistsPicklIstdata(ddlStockItem.SelectedValue, Acno);

                            if (dt.Rows.Count > 0)
                            {
                                wallQty = dt.Rows[0]["Qty"].ToString();
                                txtwallateQty.Text = wallQty;
                            }
                        }
                    }
                }
                else
                {
                    // MsgError("Please select Item");
                }
            }
        }
        else
        {
            // MsgError("Please select Installer");
        }

    }

    public void BindAddStockModel()
    {
        ScriptManager.RegisterStartupScript(updatestockpanel, this.GetType(), "MyAction", "doMyAction();", true);
    }
    //protected void btnaddnew_Click(object sender, EventArgs e)
    //{
    //    //AddmoreAttribute();
    //    //ModalPopupExtender4.Show();
    //}
    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }
    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("Qty", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));
        rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
            //TextBox txtSysDet = (TextBox)item.FindControl("txtSysDetails");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

            HiddenField hdnlistitemId = (HiddenField)item.FindControl("hndPckListItemId");
            DataRow dr = rpttable.NewRow();
            dr["type"] = hdntype.Value;

            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["Qty"] = txtQuantity.Text;
            if (hdnlistitemId != null)
            {
                if (hdnlistitemId.Value != null && hdnlistitemId.Value != "")
                {
                    dr["PicklstItemId"] = hdnlistitemId.Value;
                }
            }

            rpttable.Rows.Add(dr);

        }
    }
    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("Qty", Type.GetType("System.String"));
            // rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));
            //rpttable.Columns.Add("SysDetails", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["Qty"] = "";
            //  dr["StockOrderItemID"] = "";
            dr["type"] = "";
            dr["PicklstItemId"] = "";
            // dr["SysDetails"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        ddlStockItem.Items.Clear();

        if (ddlStockCategoryID.SelectedValue != string.Empty)
        {
            hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue.ToString();
            // ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();

        }

        ModalPopupExtender4.Show();
    }
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }
    protected void litremove_Click(object sender, EventArgs e)
    {
        divin.Visible = false;
        divpnl.Visible = false;
        DivPanel1.Visible = false;
        DivInverter1.Visible = false;
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;


        hdntype.Value = "1";
        int y = 0;
        //int pnlerror = 0;
        //int inverror = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            HiddenField hndPickLIstItemId = (HiddenField)item1.FindControl("hndPickLIstItemId");
            // int MinStock = 0;
            //int LiveStock = 0;
            //DropDownList ddlStockCategoryID = (DropDownList)item1.FindControl("ddlStockCategoryID");
            //DropDownList ddlStockItem = (DropDownList)item1.FindControl("ddlStockItem");
            //int pnlttl = 0;
            //int invttl = 0;
            //DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty_Qty_Pickist(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue);
            //if (ddlStockCategoryID.SelectedValue == "1")
            //{
            //    if (lblPt.Text != null && lblPt.Text != "")
            //    {
            //        pnlttl = Convert.ToInt32(lblPt.Text);
            //    }
            //    if (pnlttl > 0)
            //    {
            //        if (dtLive.Rows[0]["StockQuantity"].ToString() != null && dtLive.Rows[0]["StockQuantity"].ToString() != "")
            //        {
            //            LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
            //        }
            //    }
            //    if ((pnlttl <= MinStock))
            //    {
            //        pnlerror++;
            //    }
            //}
            //else if (ddlStockCategoryID.SelectedValue == "2")
            //{
            //    if (lblIt.Text != null && lblPt.Text != "")
            //    {
            //        invttl = Convert.ToInt32(lblIt.Text);
            //    }
            //    if (invttl > 0)
            //    {
            //        if (dtLive.Rows[0]["StockQuantity"].ToString() != null && dtLive.Rows[0]["StockQuantity"].ToString() != "")
            //        {
            //            LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
            //        }
            //    }
            //    if ((invttl <= MinStock))
            //    {
            //        inverror++;
            //    }
            //}


            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }

        //if (pnlerror > 0)
        //{
        //    divpnl.Visible = true;
        //    DivPanel1.Visible = true;

        //}
        //else
        //{
        //    divpnl.Visible = false;
        //    DivPanel1.Visible = false;

        //}

        //if (inverror > 0)
        //{
        //    divin.Visible = true;

        //    DivInverter1.Visible = true;
        //}
        //else
        //{
        //    divin.Visible = false;

        //    DivInverter1.Visible = false;
        //}
        ModalPopupExtender4.Show();
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        ModalPopupExtender4.Show();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        divminStockModel4.Visible = false;
        DivPanel1.Visible = false;
        DivInverter1.Visible = false;
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Compid = Request.QueryString["compid"];

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        int FirstInv = 0;
        int Secondinv = 0;
        int thirdinv = 0;
        int TotalInv = 0;
        int pnlerror = 0;
        int inverror = 0;
        if (Roles.IsUserInRole("Adistrator"))
        {
            int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
            int success = 0;
            if (PickListExist == 0)
            {
                // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

                success = ClstblProjects.tblPickListLog_Insert(ProjectID, TextBox1.Text, TextBox2.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                if (success > 0)
                {
                    bool suc1 = ClstblProjects.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
                    ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                    //Update IsTopone by PickList Id
                    ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                    ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                    try
                    {
                        bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                        bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                    }
                    catch (Exception ex) { }
                    //Telerik_reports.generate_PickList(ProjectID);
                    // picklistdiv.Visible = false;

                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                }
                bool P = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                bool I = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), "0");

                foreach (RepeaterItem item in rptattribute.Items)
                {
                    HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                    if (hdntype.Value != "1")
                    {

                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);
                        if (ddlStockCategoryID.SelectedValue == "2")
                        {
                            if (st.inverterqty != null && st.inverterqty != "")
                            {
                                FirstInv = Convert.ToInt32(st.inverterqty);
                            }
                            if (st.inverterqty2 != null && st.inverterqty2 != "")
                            {
                                Secondinv = Convert.ToInt32(st.inverterqty2);
                            }
                            if (st.inverterqty3 != null && st.inverterqty3 != "")
                            {
                                thirdinv = Convert.ToInt32(st.inverterqty3);
                            }
                            TotalInv = FirstInv + Secondinv + thirdinv;
                            bool s2 = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                        }
                        else if (ddlStockCategoryID.SelectedValue == "1")
                        {
                            if (st.NumberPanels != null && st.NumberPanels != "")
                            {
                                bool s244 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                            }

                        }
                        //string stockitem = ddlStockItem.SelectedItem.Text;  

                    }
                }
                SetAdd1();
                ModalPopupExtender4.Hide();
                BindProjectPreInst(ProjectID);
            }
        }
        else
        {
            foreach (RepeaterItem item in rptattribute.Items)
            {

                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    //int MinStock = 0;

                    // int pnllivestock = 0;
                    int LiveStock = 0;
                    int pnlwholesale = 0;
                    int pnlapl = 0;
                    int pnlsmpl = 0;
                    int pnlminstock = 0;
                    int pnlavailqty = 0;

                    int InvLiveStock = 0;
                    int Invwholesale = 0;
                    int Invapl = 0;
                    int Invsmpl = 0;
                    int Invminstock = 0;
                    int Invavailqty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                    // DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty_Qty_Pickist(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue);
                    int pnlttl = 0;
                    int invttl = 0;

                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                        {
                            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                            {
                                DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                                if (dtpaneldata.Rows.Count > 0)
                                {
                                    if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                                    {
                                        LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                                    }
                                    if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                                    {
                                        pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                                    }
                                }
                                //ArisePanelPicklistCount
                                DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                                if (DtApl.Rows.Count > 0)
                                {
                                    if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //SolarminerPanelPicklistCount
                                DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                                if (DtSmpl.Rows.Count > 0)
                                {
                                    if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //PanelWholeSale
                                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                                if (dtorderQty.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                    {
                                        pnlwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                            pnlavailqty = LiveStock - pnlttl;
                            if ((pnlavailqty <= pnlminstock))
                            {
                                pnlerror++;
                            }
                        }
                        //if (lblPt.Text != null && lblPt.Text != "")
                        //{
                        //    pnlttl = Convert.ToInt32(lblPt.Text);
                        //}
                        //if (pnlttl > 0)
                        //{
                        //    if (dtLive.Rows[0]["StockQuantity"].ToString() != null && dtLive.Rows[0]["StockQuantity"].ToString() != "")
                        //    {
                        //        LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
                        //    }
                        //}
                        //if ((pnlttl <= MinStock))
                        //{
                        //    pnlerror++;
                        //}
                    }
                    else if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                        {
                            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                            {
                                DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                                if (DtInvData.Rows.Count > 0)
                                {
                                    if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                                    {
                                        InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                                    }
                                    if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                                    {
                                        Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                                    }
                                }
                                //AriseINVERTERPicklistCount
                                DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                                if (DtApl.Rows.Count > 0)
                                {
                                    if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //SolarminerINVERTERPicklistCount
                                DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                                if (DtSmpl.Rows.Count > 0)
                                {
                                    if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //InverterWholeSale
                                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                                if (dtorderQty.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                    {
                                        Invwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            invttl = Invapl + Invsmpl + Invwholesale;
                            Invavailqty = InvLiveStock - invttl;
                            if ((Invavailqty <= Invminstock))
                            {
                                inverror++;
                            }
                        }

                    }
                }
            }
            // int success = 0;
            if (pnlerror == 0 && inverror == 0)
            {
                int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");

                if (PickListExist == 0)
                {
                    // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

                    int success = ClstblProjects.tblPickListLog_Insert(ProjectID, TextBox1.Text, TextBox2.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                    bool suc1 = ClstblProjects.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
                    if (success > 0)
                    {
                        ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                        //Update IsTopone by PickList Id
                        ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                        //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                        ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                        try
                        {
                            bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                            bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                        }
                        catch (Exception ex) { }
                        //Telerik_reports.generate_PickList(ProjectID);
                        // picklistdiv.Visible = false;
                        bool P = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                        bool I = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                        foreach (RepeaterItem item in rptattribute.Items)
                        {
                            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                            if (hdntype.Value != "1")
                            {

                                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                                // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                                int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                                //string stockitem = ddlStockItem.SelectedItem.Text;                          
                                if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                                {
                                    if (ddlStockCategoryID.SelectedValue == "1")
                                    {
                                        if (st.NumberPanels != null && st.NumberPanels != "")
                                        {
                                            bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                        }
                                        else
                                        {
                                            bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                        }
                                    }
                                    if (ddlStockCategoryID.SelectedValue == "2")
                                    {
                                        if (st.inverterqty != null && st.inverterqty != "")
                                        {
                                            FirstInv = Convert.ToInt32(st.inverterqty);
                                        }
                                        if (st.inverterqty2 != null && st.inverterqty2 != "")
                                        {
                                            Secondinv = Convert.ToInt32(st.inverterqty2);
                                        }
                                        if (st.inverterqty3 != null && st.inverterqty3 != "")
                                        {
                                            thirdinv = Convert.ToInt32(st.inverterqty3);
                                        }
                                        TotalInv = FirstInv + Secondinv + thirdinv;
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                    }
                                }
                            }
                        }

                        //Save Data in QuickStock===
                        //==========================

                        ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        BindProjectPreInst(ProjectID);
                        SetAdd1();
                        //ModalPopupExtender4.Hide();
                    }
                }

            }
            if (pnlerror > 0)
            {
                DivPanel1.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Show();
            }
            if (inverror > 0)
            {
                DivInverter1.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Show();
            }
            //else
            //{
            //    ModalPopupExtender4.Hide();
            //}
        }
        //if (error == 0)
        //{
        //    int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
        //    int success = 0;
        //    if (PickListExist == 0)
        //    {
        //        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //        success = ClstblProjects.tblPickListLog_Insert(ProjectID, TextBox1.Text, TextBox2.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
        //        if (success > 0)
        //        {
        //            ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
        //            //Update IsTopone by PickList Id
        //            ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

        //            //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
        //            ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
        //            try
        //            {
        //                bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
        //                bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
        //            }
        //            catch (Exception ex) { }
        //            //Telerik_reports.generate_PickList(ProjectID);
        //            // picklistdiv.Visible = false;

        //            ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
        //        }

        //        foreach (RepeaterItem item in rptattribute.Items)
        //        {
        //            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        //            if (hdntype.Value != "1")
        //            {

        //                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        //                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        //                // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
        //                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
        //                int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

        //                //string stockitem = ddlStockItem.SelectedItem.Text;                          
        //            }


        //        }
        //        ModalPopupExtender4.Hide();
        //        SetAdd1();
        //        BindProjectPreInst(ProjectID);
        //    }

        //}
        //else
        //{
        //    ModalPopupExtender4.Show();
        //    divminStockModel4.Visible = true;
        //}
    }


    // BindAddStockModel();

    //}
    //protected void Button3_Click(object sender, EventArgs e)
    //{

    //}

    protected void Button4_Click(object sender, EventArgs e)
    {

    }

    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int index = GetControlIndex(((DropDownList)sender).ClientID);
        //RepeaterItem item = rptattribute.Items[index];
        //DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        //DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        //if (ddlStockCategoryID.SelectedValue != string.Empty)
        //{
        //    hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue.ToString();

        //}
    }

    protected void btnaddnew_Click1(object sender, EventArgs e)
    {
        string ProjectID = "";
        divin.Visible = false;
        divpnl.Visible = false;
        DivPanel1.Visible = false;
        DivInverter1.Visible = false;
        //bindrepeateragain();
        //BindAddedAttributeagain();
        //bindrepeater();
        //BindAddedAttribute();
        ProjectID = Request.QueryString["proid"];

        //DataTable dt = new DataTable();
        //dt = ClstblProjects.tblProjects_ItemDetailsWise(ProjectID);
        //DataTable dt1 = new DataTable();
        //DataTable dt2 = new DataTable();
        //dt1.Columns.AddRange(new DataColumn[4] { new DataColumn("StockItemID", typeof(int)),
        //                    new DataColumn("Qty",typeof(string)),
        //                     new DataColumn("StockCategoryID",typeof(int)),
        //                      new DataColumn("Type",typeof(int))});


        //if (!string.IsNullOrEmpty(dt.Rows[0]["PanelBrandID"].ToString()) || (dt.Rows[0]["PanelBrandID"].ToString()) != "0")
        //{
        //    if (Convert.ToInt32(dt.Rows[0]["PanelBrandID"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["PanelBrandID"]), dt.Rows[0]["Panelqty"], dt.Rows[0]["PanelCategoryID"], dt.Rows[0]["Type"]);
        //    }

        //}
        //if (!string.IsNullOrEmpty(dt.Rows[0]["InverterDetailsId"].ToString()))
        //{

        //    if (Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]), dt.Rows[0]["inverterqty"], dt.Rows[0]["InverterCategoryId"], dt.Rows[0]["Type"]);
        //    }

        //}
        //if (!string.IsNullOrEmpty(dt.Rows[0]["SecondInverterDetailsID"].ToString()))
        //{
        //    if (Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]), dt.Rows[0]["inverterqty2"], dt.Rows[0]["SecondInverterCategoryID"], dt.Rows[0]["Type"]);
        //    }
        //}

        //if (!string.IsNullOrEmpty(dt.Rows[0]["ThirdInverterDetailsID"].ToString()))
        //{
        //    if (Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]), dt.Rows[0]["inverterqty3"], dt.Rows[0]["ThirdInverterCategoryID"], dt.Rows[0]["Type"]);
        //    }

        //}

        //if (dt1.Rows.Count > 0)
        //{
        //    MaxAttribute = dt1.Rows.Count;
        //}
        //else
        //{
        //    MaxAttribute = 1;
        //}
        //// dr["Type"] = hdntype.Value;
        //rptattribute.DataSource = dt1;
        //rptattribute.DataBind();

        ////ModalPopupExtender4.Show();
        divMinStock.Visible = false;
        bindDropdownForinsertPopup();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;

            ddlInstaller.Visible = false;
            ddlDesigner.Visible = false;
            ddlElectrician.Visible = false;
            ddlStockAllocationStore.Visible = false;
            ddlformbayunittype.Visible = false;
            ddlformbaystreettype.Visible = false;
            lbldesign.Visible = false;
            lblInstaller.Visible = false;
            lblec.Visible = false;
            lblloc.Visible = false;

        }
        //int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID(ProjectID);
        int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
        if (PickListExist == 0)
        {
            bindrepeater();
            bindDropdownForEditPopup();
            BindAddedAttribute();
            DataTable dt = new DataTable();
            dt = ClstblProjects.tblProjects_ItemDetailsWise(ProjectID);
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            dt1.Columns.AddRange(new DataColumn[4] { new DataColumn("StockItemID", typeof(int)),
                            new DataColumn("Qty",typeof(string)),
                             new DataColumn("StockCategoryID",typeof(int)),
                              new DataColumn("Type",typeof(int))});


            if (!string.IsNullOrEmpty(dt.Rows[0]["PanelBrandID"].ToString()) || (dt.Rows[0]["PanelBrandID"].ToString()) != "0")
            {
                if (Convert.ToInt32(dt.Rows[0]["PanelBrandID"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["PanelBrandID"]), dt.Rows[0]["Panelqty"], dt.Rows[0]["PanelCategoryID"], dt.Rows[0]["Type"]);
                }

            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["InverterDetailsId"].ToString()))
            {

                if (Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]), dt.Rows[0]["inverterqty"], dt.Rows[0]["InverterCategoryId"], dt.Rows[0]["Type"]);
                }

            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["SecondInverterDetailsID"].ToString()))
            {
                if (Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]), dt.Rows[0]["inverterqty2"], dt.Rows[0]["SecondInverterCategoryID"], dt.Rows[0]["Type"]);
                }
            }

            if (!string.IsNullOrEmpty(dt.Rows[0]["ThirdInverterDetailsID"].ToString()))
            {
                if (Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]), dt.Rows[0]["inverterqty3"], dt.Rows[0]["ThirdInverterCategoryID"], dt.Rows[0]["Type"]);
                }

            }

            if (dt1.Rows.Count > 0)
            {
                MaxAttribute = dt1.Rows.Count;
            }
            else
            {
                MaxAttribute = 1;
            }

            // dr["Type"] = hdntype.Value;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            try
            {
                ddlinstall2.SelectedValue = st.Installer;
            }
            catch { }
            try
            {
                ddldesign2.SelectedValue = st.Designer;
            }
            catch { }
            try
            {
                ddlec2.SelectedValue = st.Electrician;
            }
            catch { }
            TextBox5.Text = txtInstallBookingDate.Text;
            rptattribute.DataSource = dt1;
            rptattribute.DataBind();
            btnaddagain.Visible = true;
            ModalPopupExtender4.Show();
        }
        else
        {
            ModalPopupExtender4.Hide();
            hdnPickListLogID.Value = string.Empty;
            txtpicklistreason.Text = string.Empty;
            txtpicklistnote.Text = string.Empty;
            txtpicklistreason.Enabled = true;
            txtpicklistnote.Enabled = true;
            //picklistdiv.Visible = true;
            btnaddagain.Visible = true;
            btnupdate.Visible = false;
            btnPickList.Attributes.Add("disabled", "disabled");
            btnDownload.Style.Add("display", "none");
            btnSubmit.Attributes.Remove("disabled");
            bindpicklistagain();

            //MaxAttribute = 1;
        }

    }

    public void bindpicklistagain()
    {
        bindrepeateragain();
        bindDropdownForEditPopup();
        BindAddedAttributeagain();
        String ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
            DataTable dt = new DataTable();
        dt = ClstblProjects.tblProjects_ItemDetailsWise(ProjectID);
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        dt1.Columns.AddRange(new DataColumn[4] { new DataColumn("StockItemID", typeof(int)),
                            new DataColumn("Qty",typeof(string)),
                             new DataColumn("StockCategoryID",typeof(int)),
                              new DataColumn("Type",typeof(int))});

        btnaddagain.Visible = true;

        if (dt1.Rows.Count > 0)
        {
            MaxAttribute = dt1.Rows.Count;
        }
        else
        {
            MaxAttribute = 1;
        }
        // dr["Type"] = hdntype.Value;
        //Repeater1.DataSource = dt1;
        //Repeater1.DataBind();
        TextBox6.Text = txtInstallBookingDate.Text;
        ModalPopupExtenderPickListAgain.Show();
    }
    protected void BindAddedAttributeagain()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("Qty", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));
        rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));

        foreach (RepeaterItem item in Repeater1.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
            //TextBox txtSysDet = (TextBox)item.FindControl("txtSysDetails");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            HiddenField hndPckListItemId = (HiddenField)item.FindControl("hndPckListItemId");
            DataRow dr = rpttable.NewRow();
            dr["type"] = hdntype.Value;

            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["Qty"] = txtQuantity.Text;
            dr["PicklstItemId"] = hndPckListItemId.Value;

            rpttable.Rows.Add(dr);

        }
    }
    protected void bindrepeateragain()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("Qty", Type.GetType("System.String"));
            // rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["Qty"] = "";
            //  dr["StockOrderItemID"] = "";
            dr["type"] = "";
            // dr["SysDetails"] = "";
            dr["PicklstItemId"] = "";
            rpttable.Rows.Add(dr);
        }
        Repeater1.DataSource = rpttable;
        Repeater1.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in Repeater1.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = Repeater1.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in Repeater1.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }
    protected void AddmoreAttributeagain()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttributeagain();
        bindrepeateragain();
    }

    protected void btnAddRowagain_Click(object sender, EventArgs e)
    {
        AddmoreAttributeagain();

        ModalPopupExtenderPickListAgain.Show();
    }

    protected void btnaddagain_Click(object sender, EventArgs e)
    {
        divMinStock.Visible = false;
        divin.Visible = false;
        divpnl.Visible = false;
        string ProjectID = "";
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Compid = Request.QueryString["compid"];
        int TotalInv = 0;
        int FirstInv = 0;
        int Secondinv = 0;
        int thirdinv = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        int pnlerror = 0;
        int inverror = 0;
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
        if (Roles.IsUserInRole("Administrator"))
        {
            int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtpicklistreason.Text, txtpicklistnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
            bool suc1 = ClstblProjects.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
            if (success > 0)
            {

                //Update IsTopone by Project Id
                ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");

                //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);

                bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlInstaller1.SelectedValue, ddlInstaller1.SelectedItem.Text, ddldesigner1.SelectedValue, ddlec.SelectedValue);
                //Update IsTopone by PickList Id
                ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");
                //Update Fileds in Picklist
                bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(success.ToString(), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);

                hdnPickListLogID.Value = success.ToString();
                //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
                DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");
                if (dtPickList.Rows.Count > 0)
                {
                    divPickList.Visible = true;
                    rptPickList.DataSource = dtPickList;
                    rptPickList.DataBind();

                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                    #region insert itemd
                    bool P = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                    bool I = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                    ClstblProjects.tbl_PicklistItemsDetail_Delete(success.ToString());

                    foreach (RepeaterItem item in Repeater1.Items)
                    {
                        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                        if (hdntype.Value != "1")
                        {

                            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                            // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                            int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);
                            if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                            {
                                if (ddlStockCategoryID.SelectedValue == "1")
                                {
                                    if (st.NumberPanels != null && st.NumberPanels != "")
                                    {
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                    }
                                    else
                                    {
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                    }
                                }
                                if (ddlStockCategoryID.SelectedValue == "2")
                                {
                                    if (st.inverterqty != null && st.inverterqty != "")
                                    {
                                        FirstInv = Convert.ToInt32(st.inverterqty);
                                    }
                                    if (st.inverterqty2 != null && st.inverterqty2 != "")
                                    {
                                        Secondinv = Convert.ToInt32(st.inverterqty2);
                                    }
                                    if (st.inverterqty3 != null && st.inverterqty3 != "")
                                    {
                                        thirdinv = Convert.ToInt32(st.inverterqty3);
                                    }
                                    TotalInv = FirstInv + Secondinv + thirdinv;
                                    bool s2 = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                }
                            }
                            //string stockitem = ddlStockItem.SelectedItem.Text;                          
                        }


                    }
                    #endregion
                    Response.Redirect("~/admin/adminfiles/company/company.aspx?m=propre&compid=" + Compid + "&proid=" + ProjectID);
                    SetAdd1();
                    ModalPopupExtenderPickListAgain.Hide();

                }

            }

        }
        else
        {

            foreach (RepeaterItem item in Repeater1.Items)
            {

                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    //int MinStock = 0;

                    // int pnllivestock = 0;
                    int LiveStock = 0;
                    int pnlwholesale = 0;
                    int pnlapl = 0;
                    int pnlsmpl = 0;
                    int pnlminstock = 0;
                    int pnlavailqty = 0;

                    int InvLiveStock = 0;
                    int Invwholesale = 0;
                    int Invapl = 0;
                    int Invsmpl = 0;
                    int Invminstock = 0;
                    int Invavailqty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                    // DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty_Qty_Pickist(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue);
                    int pnlttl = 0;
                    int invttl = 0;

                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                        {
                            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                            {
                                DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                                if (dtpaneldata.Rows.Count > 0)
                                {
                                    if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                                    {
                                        LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                                    }
                                    if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                                    {
                                        pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                                    }
                                }
                                //ArisePanelPicklistCount
                                DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                                if (DtApl.Rows.Count > 0)
                                {
                                    if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //SolarminerPanelPicklistCount
                                DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                                if (DtSmpl.Rows.Count > 0)
                                {
                                    if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //PanelWholeSale
                                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                                if (dtorderQty.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                    {
                                        pnlwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                            pnlavailqty = LiveStock - pnlttl;
                            if ((pnlavailqty <= pnlminstock))
                            {
                                pnlerror++;
                            }
                        }
                        //if (lblPt.Text != null && lblPt.Text != "")
                        //{
                        //    pnlttl = Convert.ToInt32(lblPt.Text);
                        //}
                        //if (pnlttl > 0)
                        //{
                        //    if (dtLive.Rows[0]["StockQuantity"].ToString() != null && dtLive.Rows[0]["StockQuantity"].ToString() != "")
                        //    {
                        //        LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
                        //    }
                        //}
                        //if ((pnlttl <= MinStock))
                        //{
                        //    pnlerror++;
                        //}
                    }
                    else if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                        {
                            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                            {
                                DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                                if (DtInvData.Rows.Count > 0)
                                {
                                    if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                                    {
                                        InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                                    }
                                    if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                                    {
                                        Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                                    }
                                }
                                //AriseINVERTERPicklistCount
                                DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                                if (DtApl.Rows.Count > 0)
                                {
                                    if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //SolarminerINVERTERPicklistCount
                                DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                                if (DtSmpl.Rows.Count > 0)
                                {
                                    if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //InverterWholeSale
                                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                                if (dtorderQty.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                    {
                                        Invwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            invttl = Invapl + Invsmpl + Invwholesale;
                            Invavailqty = InvLiveStock - invttl;
                            if ((Invavailqty <= Invminstock))
                            {
                                inverror++;
                            }
                        }

                    }
                }
            }
            // int success = 0;
            if (pnlerror == 0 && inverror == 0)
            {
                //int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");

                //if (PickListExist == 0)
                //{
                // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

                int success = ClstblProjects.tblPickListLog_Insert(ProjectID, TextBox1.Text, TextBox2.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                bool suc1 = ClstblProjects.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
                if (success > 0)
                {
                    ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                    //Update IsTopone by PickList Id
                    ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                    ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                    try
                    {
                        bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                        bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                    }
                    catch (Exception ex) { }
                    bool P = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                    bool I = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                    //Telerik_reports.generate_PickList(ProjectID);
                    // picklistdiv.Visible = false;
                    ClstblProjects.tbl_PicklistItemsDetail_Delete(success.ToString());
                    foreach (RepeaterItem item in Repeater1.Items)
                    {
                        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                        if (hdntype.Value != "1")
                        {

                            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                            // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                            int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                            //string stockitem = ddlStockItem.SelectedItem.Text;                       if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                            {
                                if (ddlStockCategoryID.SelectedValue == "1")
                                {
                                    if (st.NumberPanels != null && st.NumberPanels != "")
                                    {
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                    }
                                    else
                                    {
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                    }
                                }
                                if (ddlStockCategoryID.SelectedValue == "2")
                                {
                                    if (st.inverterqty != null && st.inverterqty != "")
                                    {
                                        FirstInv = Convert.ToInt32(st.inverterqty);
                                    }
                                    if (st.inverterqty2 != null && st.inverterqty2 != "")
                                    {
                                        Secondinv = Convert.ToInt32(st.inverterqty2);
                                    }
                                    if (st.inverterqty3 != null && st.inverterqty3 != "")
                                    {
                                        thirdinv = Convert.ToInt32(st.inverterqty3);
                                    }
                                    TotalInv = FirstInv + Secondinv + thirdinv;
                                    bool s2 = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                }
                            }
                        }

                    }
                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                    BindProjectPreInst(ProjectID);
                    SetAdd1();
                    ModalPopupExtenderPickListAgain.Hide();
                }
                //}

            }
            if (pnlerror > 0)
            {
                divpnl.Visible = true;
                ModalPopupExtenderPickListAgain.Show();
                ModalPopupExtender7.Show();
            }
            if (inverror > 0)
            {
                divin.Visible = true;
                ModalPopupExtenderPickListAgain.Show();
                ModalPopupExtender7.Show();
            }
        }

        //if(pnlerror > 0 )
        //{
        //    divpnl.Visible = true;
        //    ModalPopupExtenderPickListAgain.Show();
        //}
        //if (inverror > 0)
        //{
        //    divin.Visible = true;
        //    ModalPopupExtenderPickListAgain.Show();

        //}
        //if (error == 0)
        //{
        //    //if (!string.IsNullOrEmpty(txtpicklistnote.Text) && !string.IsNullOrEmpty(txtpicklistreason.Text))
        //    //{

        //    int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtpicklistreason.Text, txtpicklistnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
        //    //Update IsTopone by Project Id
        //    ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");

        //    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
        //    ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);

        //    bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlInstaller1.SelectedValue, ddlInstaller1.SelectedItem.Text, ddldesigner1.SelectedValue, ddlec.SelectedValue);
        //    //Update IsTopone by PickList Id
        //    ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");
        //    //Update Fileds in Picklist
        //    bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(success.ToString(), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);


        //    //            
        //    if (success > 0)
        //    {
        //        try
        //        {
        //           // Telerik_reports.generate_PickList(ProjectID);
        //        }
        //        catch
        //        {

        //        }
        //        hdnPickListLogID.Value = success.ToString();
        //        //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
        //        DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");
        //        if (dtPickList.Rows.Count > 0)
        //        {
        //            divPickList.Visible = true;
        //            rptPickList.DataSource = dtPickList;
        //            rptPickList.DataBind();

        //            ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
        //            #region insert item d

        //            ClstblProjects.tbl_PicklistItemsDetail_Delete(success.ToString());

        //            foreach (RepeaterItem item in Repeater1.Items)
        //            {
        //                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        //                if (hdntype.Value != "1")
        //                {

        //                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        //                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        //                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
        //                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
        //                    int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

        //                    //string stockitem = ddlStockItem.SelectedItem.Text;                          
        //                }


        //            }
        //            #endregion
        //        }
        //        else
        //        {
        //            divPickList.Visible = false;
        //        }
        //        txtpicklistreason.Enabled = false;
        //        txtpicklistnote.Enabled = false;
        //        btnaddagain.Enabled = true;
        //        // btnupdate.Visible = false;
        //        //btnaddagain.Attributes.Add("disabled", "disabled");
        //        Response.Redirect("~/admin/adminfiles/company/company.aspx?m=propre&compid=" + Compid + "&proid=" + ProjectID);
        //        SetAdd1();
        //    }
        //    else
        //    {

        //        txtpicklistreason.Enabled = true;
        //        txtpicklistnote.Enabled = true;
        //        btnaddagain.Enabled = true;
        //        // btnupdate.Visible = false;
        //        SetError1();
        //    }
        //    //}
        //}
        //else
        //{
        //    ModalPopupExtenderPickListAgain.Show();
        //    txtpicklistreason.Enabled = true;
        //    txtpicklistnote.Enabled = true;
        //    btnaddagain.Enabled = true;
        //    // btnupdate.Visible = false;
        //    divMinStock.Visible = true;
        //    //SetItemError();
        //}

    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtQuantity");



        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hdnStockCategoryId");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdbQty = (HiddenField)e.Item.FindControl("hdbQty");
        HiddenField hndqtynew = (HiddenField)e.Item.FindControl("hndqtynew");
        HiddenField hndPckListItemId = (HiddenField)e.Item.FindControl("hndPckListItemId");
        TextBox txtwallateQty = (TextBox)item.FindControl("txtwallateQty");

        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();

        ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;

        if (ddlStockItem.SelectedValue == "")
        {
            txtOrderQuantity.Text = "0";
        }
        else
        {
            txtOrderQuantity.Text = hndqtynew.Value;
        }

        if (e.Item.ItemIndex == 0)
        {

            /// btnaddnew.Attributes.Remove("disabled");
            //litremove.Attributes.Remove("disabled");
            litremove.Visible = false;

        }
        else
        {

            // chkdelete.Visible = true;
            litremove.Visible = true;
        }

        if (!string.IsNullOrEmpty(hdnPickId.Value))
        {
            if (ddlStockItem.SelectedValue != "")
            {
                int scanqty = 0;
                DataTable dt = ClstblMaintainHistory.Select_tblMaintainHistory_CountItemWise(hdnPickId.Value, ddlStockItem.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        scanqty = Convert.ToInt32(dt.Rows[0]["ScanQty"].ToString());
                    }
                    catch { }
                    if (txtOrderQuantity.Text != "0")
                    {
                        int Qty = Convert.ToInt32(txtOrderQuantity.Text);
                        if (scanqty == Qty)
                        {
                            ddlStockCategoryID.Enabled = false;
                            ddlStockItem.Enabled = false;
                            txtOrderQuantity.Enabled = false;
                            litremove.Enabled = false;
                        }
                    }

                }
            }
        }

        //Wallate Qty Installaer wise
        string Accreditation = "";
        string wallQty = "0";
        txtwallateQty.Text = "0";

        if (ddlInstaller1.SelectedValue != string.Empty)
        {
            SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(ddlInstaller1.SelectedValue);
            Accreditation = st.Accreditation;
            if (!string.IsNullOrEmpty(Accreditation))
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlInstaller1.SelectedValue);
                if (ddlStockItem.SelectedValue != string.Empty)
                {
                    //string StockItemid = ddlStockCategoryID.SelectedValue.ToString();
                    //string Installer = ddlInstaller1.SelectedValue.ToString();
                    if (dtAcno.Rows.Count > 0)
                    {
                        if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                        {
                            string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                            DataTable dt = ClstblProjects.spexistsPicklIstdata(ddlStockItem.SelectedValue, Acno);

                            if (dt.Rows.Count > 0)
                            {
                                wallQty = dt.Rows[0]["Qty"].ToString();
                                txtwallateQty.Text = wallQty;
                            }
                        }
                    }
                }
                else
                {
                    // MsgError("Please select Item");
                }
            }
        }
        else
        {
            // MsgError("Please select Installer");
        }
    }

    protected void ddlStockCategoryID_SelectedIndexChanged1(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = Repeater1.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        ddlStockItem.Items.Clear();

        if (ddlStockCategoryID.SelectedValue != string.Empty)
        {
            hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue.ToString();
            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();

        }

        ModalPopupExtenderPickListAgain.Show();
    }
    protected void litremove_Click1(object sender, EventArgs e)
    {
        //int index = GetControlIndex(((Button)sender).ClientID);

        //RepeaterItem item = rptattribute.Items[index];

        //HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        //PanAddUpdate.Visible = true;


        //hdntype.Value = "1";
        //int y = 0;
        //foreach (RepeaterItem item1 in rptattribute.Items)
        //{
        //    HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
        //    HiddenField hndPickLIstItemId = (HiddenField)item1.FindControl("hndPickLIstItemId");
        //    Button lbremove1 = (Button)item1.FindControl("litremove");
        //    if (hdntype1.Value == "1")
        //    {
        //        item1.Visible = false;
        //        y++;
        //    }
        //    int count = rptattribute.Items.Count - y;
        //    lbremove1.Visible = true;
        //    if (count < 2)
        //    {
        //        foreach (RepeaterItem item2 in rptattribute.Items)
        //        {
        //            Button lbremove = (Button)item2.FindControl("litremove");
        //            lbremove.Visible = false;
        //        }
        //    }
        //}
        //ModalPopupExtender4.Show();
        #region old
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = Repeater1.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;
        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in Repeater1.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            HiddenField HiddenField3 = (HiddenField)item1.FindControl("StockItemID");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }

            int count = Repeater1.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in Repeater1.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
            if (item.ItemIndex == item1.ItemIndex)
            {
                HiddenField HiddenField4 = (HiddenField)item1.FindControl("hndPckListItemId");
                string id = HiddenField4.Value;
                if (id != null & id != "")
                {
                    bool s1 = ClstblProjects.tbl_PicklistItemsDetail_remove(id);
                }


            }
        }
        DataTable dtpicklistitem = ClstblProjects.tbl_PicklistItemDetail_SelectbyPickId(hdnPickId.Value);
        if (dtpicklistitem != null && dtpicklistitem.Rows.Count > 0)
        {
            Repeater1.DataSource = dtpicklistitem;
            Repeater1.DataBind();
        }
        ModalPopupExtenderPickListAgain.Show();
        #endregion old
    }

    protected void btnQuckFormnew_Click(object sender, EventArgs e)
    {
        SendToBridgeSelect();
    }

    private void SendToBridgeSelect()
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            // SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
            // SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
            SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
            SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
            //SttblContacts stelect = ClstblContacts.tblContacts_SelectByContactID(stProject.Electrician);
            //SttblContacts stdesig = ClstblContacts.tblContacts_SelectByContactID(stProject.Designer);
            SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
            SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
            Sttbl_formbaystreettype stStreeType = ClstblStreetType.tbl_formbaystreettype_SelectByStreetCode(stProject.street_type);
            SttblStockItems stitems = ClstblStockItems.tblStockItems_SelectByStockItemID(stProject.PanelBrandID);
            string InverterDetailsID = "0";
            if (!string.IsNullOrEmpty(stProject.InverterDetailsID))
            {
                InverterDetailsID = stProject.InverterDetailsID;
            }
            SttblStockItems Inverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
            string SecondInverterDetailsID = "0";
            if (!string.IsNullOrEmpty(stProject.SecondInverterDetailsID))
            {
                SecondInverterDetailsID = stProject.SecondInverterDetailsID;
            }
            SttblStockItems secondInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
            string ThirdInverterDetailsID = "0";
            if (!string.IsNullOrEmpty(stProject.ThirdInverterDetailsID))
            {
                ThirdInverterDetailsID = stProject.ThirdInverterDetailsID;
            }
            SttblStockItems thirdInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ThirdInverterDetailsID);
            string panel = GetPanels(stProject, stitems);
            //string inverter = GetInverters(stProject, Inverter, secondInverter, thirdInverter);
            string inverter = GetInvertersnew(stProject, Inverter, secondInverter, thirdInverter);
            string iiavalue = "";
            if (stProject.ProjectTypeID == "1")
            {
                iiavalue = "No";
            }
            else
            {
                iiavalue = "No";
            }
            string ngcsvalue = "";
            if (stProject.GridConnected == "1")
            {
                ngcsvalue = "Yes";
            }
            else
            {
                ngcsvalue = "No";
            }
            string pobxnvalue = "";
            if (stProject.unit_type == "8")
            {
                pobxnvalue = stProject.unit_number;
            }
            string smtvalue = "";
            if (stProject.InstallBase == "1")
            {
                smtvalue = "Building or structure";
            }
            if (stProject.InstallBase == "2")
            {
                smtvalue = "Ground mounted or free standing";
            }
            string srtvalue = "";
            if (stProject.HouseTypeID == "1")
            {
                srtvalue = "Single";
            }
            else
            {
                srtvalue = "Multi";
            }
            string ampmvalue = "";
            if (stProject.InstallAM1 == "True")
            {
                ampmvalue = "AM";
            }
            else if (stProject.InstallAM2 == "False")
            {
                ampmvalue = "PM";
            }
            string InstallDate = "";
            if (stProject.InstallBookingDate != null && stProject.InstallBookingDate != "")
            {
                InstallDate = Convert.ToDateTime(stProject.InstallBookingDate).ToShortDateString();
            }

            var obj = new ClsBridgeSelect
            {
                crmid = stProject.ProjectNumber,
                crms = "",
                installer = stinst.Accreditation,
                mfs = panel,
                ifs = inverter,
                fn = stCustContact.ContFirst,
                ln = stCustContact.ContLast,
                ifn = stCustContact.ContFirst,
                iln = stCustContact.ContLast,
                m = stCustContact.ContMobile,
                im = stCustContact.ContMobile,
                ipa = stcustomer.StreetAddress,
                istp = stcustomer.brgStreetName,
                ot = "Individual",
                anzss = "Yes",
                at = "Physical",
                ac = stProject.InstallerNotes,
                bb = "",
                bm = "",
                cd = "",
                cecss = "Yes",
                cgst = "No",//0 or 1
                ctieg = "Connected to an electricity grid without battery storage",
                e = stCustContact.ContEmail,
                esd = "Yes",
                fp = stProject.RECRebate,
                iia = iiavalue,
                ibpoa = "No",
                icfc = "No",
                id = InstallDate,
                //id = InstallDate.ToString("dd-mm-yyyy"),//NEW
                ie = stCustContact.ContEmail,
                imto = iiavalue,
                ist = stProject.InstallState,
                ipc = stProject.InstallPostCode,
                iph = stcustomer.CustPhone,
                ipra = stProject.street_name,
                isb = stProject.InstallCity,
                istn = stcustomer.street_number,
                lat = "",
                lng = "",
                mea = "Yes",
                ngcs = ngcsvalue,
                nmi = stProject.NMINumber,
                noep = "",
                norp = "",
                p = stcustomer.CustPhone,
                pa = stcustomer.street_number + " " + stcustomer.street_name,
                pc = stcustomer.StreetPostCode,
                pobxn = pobxnvalue,
                potyp = "",
                pra = stcustomer.street_name,
                pt = stcustomer.SolarType,
                rn = "Arise Solar Pty Ltd",
                sb = stProject.InstallCity,
                sia = "Yes",
                siad = "Yes",
                smt = smtvalue,
                srt = srtvalue,
                st = stcustomer.StreetState,
                stcdp = "Eleven years",
                stn = stcustomer.street_number,
                stp = stcustomer.street_type,
                tos = "S.G.U. - Solar (Deemed)",
                tp = stProject.NumberPanels,
                sw = "",
                sz = stProject.SystemCapKW,
                wd = "",
                sl = 0,
                ampm = ampmvalue,
                jt = "1",
                mtrn = stProject.MeterNumber1,
                dstbn = stProject.ElecDistributor,
                ismd = "1",
                trn = "QUICKFORM TRADES PTY.LTD."
                // stcdp= "Eleven years"
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "";
            string URL = "";


            //URL key 6d44470c6f7a1923d979f9d9209e298d626efccb0c35aea760fa93cd5a299b8cret
            //SALT is 1717ef54f2070a4c9ba451b9a4d15ba0f2851c2f7edbcf31f86c3b1454b8b2c5

            //salt = "1717ef54f2070a4c9ba451b9a4d15ba0f2851c2f7edbcf31f86c3b1454b8b2c5"; // salt need to be added
            //URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/6d44470c6f7a1923d979f9d9209e298d626efccb0c35aea760fa93cd5a299b8cret/job/create-or-edit"; // url need to be added
            salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            URL = "https://us-central1-bs-897a3.cloudfunctions.net/handleJob?other=7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = DATA.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(DATA);
            }

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();

                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + response + "')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
            }
        }
    }

    private string GetPanels(SttblProjects stPro, SttblStockItems stp)
    {
        string fpanels = "";
        List<string> modelfields = new List<string>();
        modelfields.Add("n");
        modelfields.Add("np");
        modelfields.Add("rs");
        modelfields.Add("w");
        dynamic mexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelfields)
        {
            if (field == "n")
            {
                ((IDictionary<String, Object>)mexo).Add(field, stp.StockModel);
            }
            if (field == "np")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(stPro.NumberPanels) ? 0 : Convert.ToInt32(stPro.NumberPanels));
            }
            if (field == "rs")
            {
                ((IDictionary<String, Object>)mexo).Add(field, stp.RespSup);
            }
            if (field == "w")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(stp.StockSize) ? 0 : Convert.ToInt32(stp.StockSize));
            }
        }
        List<string> modelField = new List<string>();
        // my 'columns'
        //modelField.Add(stp.StockModel);
        string Model = stp.StockModel;
        string Model1 = Regex.Replace(Model, "[^a-zA-Z0-9-]", "_");
        modelField.Add(Model1);
        dynamic mmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelField)
        {
            ((IDictionary<String, Object>)mmexo).Add(field, mexo);
        }

        List<string> pnlmodelField = new List<string>();
        // my 'columns'
        pnlmodelField.Add("ms");
        pnlmodelField.Add("n");
        dynamic pnlmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in pnlmodelField)
        {
            if (field == "ms")
            {
                ((IDictionary<String, Object>)pnlmexo).Add(field, mmexo);
            }
            if (field == "n")
            {
                ((IDictionary<String, Object>)pnlmexo).Add(field, stp.StockManufacturer);
            }
        }

        List<string> fields = new List<string>();
        // my 'columns'

        fields.Add(Regex.Replace(stp.StockManufacturer, "[^a-zA-Z0-9-]", "_")); //var nstr = str.replace(/[^A-Z0-9^-]+/ig, "_"); return nstr;//
                                                                                //fields.Add(Regex.Replace(stp.StockManufacturer, @"\s^[a-zA-Z0-9_]*$", "_"));

        dynamic exo = new System.Dynamic.ExpandoObject();
        foreach (string field in fields)
        {
            ((IDictionary<String, Object>)exo).Add(field, pnlmexo);
        }
        fpanels = Newtonsoft.Json.JsonConvert.SerializeObject(exo);
        return fpanels;
    }
    private string GetInvertersnew(SttblProjects stPro, SttblStockItems sti, SttblStockItems sti2, SttblStockItems sti3)
    {
        string fInverters = "";
        List<string> modelfields = new List<string>();
        modelfields.Add("n");
        modelfields.Add("np");
        modelfields.Add("sr");
        modelfields.Add("w");
        dynamic mexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelfields)
        {
            int InverterQty1 = 0;
            int InverterQty2 = 0;
            int InverterQty3 = 0;
            if (stPro.inverterqty != null && stPro.inverterqty != "")
            {
                InverterQty1 = Convert.ToInt32(stPro.inverterqty);
            }
            if (stPro.inverterqty2 != null && stPro.inverterqty2 != "")
            {
                InverterQty2 = Convert.ToInt32(stPro.inverterqty2);
            }
            if (stPro.inverterqty3 != null && stPro.inverterqty3 != "")
            {
                InverterQty3 = Convert.ToInt32(stPro.inverterqty3);
            }
            int Qty = InverterQty1 + InverterQty2 + InverterQty3;
            string qty1 = Convert.ToString(Qty);
            if (field == "n")
            {
                ((IDictionary<String, Object>)mexo).Add(field, sti.StockModel);
            }
            if (field == "np")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(qty1) ? 0 : Convert.ToInt32(qty1));
            }
            if (field == "sr")
            {
                ((IDictionary<String, Object>)mexo).Add(field, sti.StockSeries);
            }
            if (field == "w")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(sti.StockSize) ? 0 : Convert.ToInt32(sti.StockSize));
            }
        }
        List<string> modelField = new List<string>();
        // my 'columns'
        string Model = sti.StockModel;
        string Model1 = Regex.Replace(Model, "[^a-zA-Z0-9-]", "_");
        modelField.Add(Model1);
        dynamic mmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelField)
        {
            ((IDictionary<String, Object>)mmexo).Add(field, mexo);
        }

        List<string> invfields = new List<string>();
        // my 'columns'
        invfields.Add("ms");
        invfields.Add("n");
        dynamic pnlmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in invfields)
        {
            if (field == "ms")
            {

                ((IDictionary<String, Object>)pnlmexo).Add(field, mmexo);
            }
            if (field == "n")
            {
                ((IDictionary<String, Object>)pnlmexo).Add(field, sti.StockManufacturer);
            }
        }

        List<string> fields = new List<string>();
        // my 'columns'

        fields.Add(Regex.Replace(sti.StockManufacturer, "[^a-zA-Z0-9-]", "_")); //var nstr = str.replace(/[^A-Z0-9^-]+/ig, "_"); return nstr;//
                                                                                //fields.Add(Regex.Replace(stp.StockManufacturer, @"\s^[a-zA-Z0-9_]*$", "_"));

        dynamic exo = new System.Dynamic.ExpandoObject();
        foreach (string field in fields)
        {
            ((IDictionary<String, Object>)exo).Add(field, pnlmexo);
        }
        fInverters = Newtonsoft.Json.JsonConvert.SerializeObject(exo);
        return fInverters;
    }
    //private string GetPanels(SttblProjects stPro, SttblStockItems stp)
    //{
    //    string fpanels = "";
    //    List<string> modelfields = new List<string>();
    //    modelfields.Add("n");
    //    modelfields.Add("ms");
    //    modelfields.Add("rs");
    //    modelfields.Add("w");
    //    modelfields.Add("np");
    //    dynamic mexo = new System.Dynamic.ExpandoObject();
    //    foreach (string field in modelfields)
    //    {
    //        if (field == "n")
    //        {
    //            ((IDictionary<String, Object>)mexo).Add(field, stp.StockManufacturer);
    //        }
    //        if (field == "ms")
    //        {
    //            ((IDictionary<String, Object>)mexo).Add(field, stp.StockModel);
    //        }
    //        if (field == "np")
    //        {
    //            ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(stPro.NumberPanels) ? 0 : Convert.ToInt32(stPro.NumberPanels));
    //        }
    //        if (field == "rs")
    //        {
    //            ((IDictionary<String, Object>)mexo).Add(field, stp.RespSup);
    //        }
    //        if (field == "w")
    //        {
    //            ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(stp.StockSize) ? 0 : Convert.ToInt32(stp.StockSize));
    //        }
    //    }
    //    List<string> modelField = new List<string>();
    //    // my 'columns'
    //    modelField.Add(stp.StockModel);
    //    dynamic mmexo = new System.Dynamic.ExpandoObject();
    //    foreach (string field in modelField)
    //    {
    //        ((IDictionary<String, Object>)mmexo).Add(field, mexo);
    //    }

    //    List<string> pnlmodelField = new List<string>();
    //    // my 'columns'
    //    pnlmodelField.Add("ms");
    //    pnlmodelField.Add("n");
    //    dynamic pnlmexo = new System.Dynamic.ExpandoObject();
    //    foreach (string field in pnlmodelField)
    //    {
    //        if (field == "ms")
    //        {
    //            ((IDictionary<String, Object>)pnlmexo).Add(field, mmexo);
    //        }
    //        if (field == "n")
    //        {
    //            ((IDictionary<String, Object>)pnlmexo).Add(field, stp.StockManufacturer);
    //        }
    //    }

    //    List<string> fields = new List<string>();
    //    // my 'columns'

    //    fields.Add(Regex.Replace(stp.StockManufacturer, "[^a-zA-Z0-9-]", "_")); //var nstr = str.replace(/[^A-Z0-9^-]+/ig, "_"); return nstr;//
    //                                                                            //fields.Add(Regex.Replace(stp.StockManufacturer, @"\s^[a-zA-Z0-9_]*$", "_"));

    //    dynamic exo = new System.Dynamic.ExpandoObject();
    //    foreach (string field in fields)
    //    {
    //        ((IDictionary<String, Object>)exo).Add(field, mexo);
    //    }
    //    fpanels = Newtonsoft.Json.JsonConvert.SerializeObject(exo);
    //    return fpanels;

    //}

    private string GetInverters(SttblProjects stPro, SttblStockItems sti, SttblStockItems sti2, SttblStockItems sti3)
    {

        string finverters = "";
        List<string> invfields = new List<string>();
        invfields.Add("ms");
        invfields.Add("n");
        invfields.Add("w");
        invfields.Add("sr");
        invfields.Add("np");
        dynamic invexo = new System.Dynamic.ExpandoObject();
        foreach (string field in invfields)
        {
            if (field == "np")
            {
                ((IDictionary<String, Object>)invexo).Add(field, "1");
            }
            if (field == "ms")
            {
                ((IDictionary<String, Object>)invexo).Add(field, sti.StockManufacturer);
            }
            if (field == "n")
            {
                ((IDictionary<String, Object>)invexo).Add(field, sti.StockModel);
            }
            if (field == "w")
            {
                ((IDictionary<String, Object>)invexo).Add(field, sti.StockSize);
            }
            if (field == "sr")
            {
                ((IDictionary<String, Object>)invexo).Add(field, sti.StockSeries);
            }
        }
        List<string> manumodelField = new List<string>();
        // my 'columns'
        manumodelField.Add(Regex.Replace(sti.StockManufacturer, "[^a-zA-Z0-9-]", "_"));
        dynamic mnmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in manumodelField)
        {
            ((IDictionary<String, Object>)mnmexo).Add(field, invexo);
        }
        finverters = Newtonsoft.Json.JsonConvert.SerializeObject(mnmexo);

        return finverters;
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    protected void btnUpdatepicklist_Click(object sender, EventArgs e)
    {
        //string ProjectID = Request.QueryString["proid"];
        //DataTable dtpicklist = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
        //if(dtpicklist.Rows.Count>0)
        //{
        //    string PickID = dtpicklist.Rows[0]["PickId"].ToString();


        //}



    }

    protected void rptPickList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        LinkButton updatepicklist = (LinkButton)item.FindControl("btnUpdatepicklist");
        LinkButton btnwallet = (LinkButton)item.FindControl("btnwallet");
        LinkButton btnpicklistDelete = (LinkButton)item.FindControl("btnpicklistDelete");
        HiddenField picklistid = (HiddenField)item.FindControl("hndpicklistid");
        Label lblstytemdetail = (Label)item.FindControl("lblstytemdetail");
        HiddenField hndEmployeeID = (HiddenField)item.FindControl("hdnEmpId");
        Label lblEmpName = (Label)item.FindControl("lblEmpName");
        DataTable dtpicklistitem = ClstblProjects.tbl_PicklistItemDetail_SelectbyPickId(picklistid.Value);
        DataTable dtpicklist = ClstblProjects.tbl_PicklistItemDetail_SelectByPId(picklistid.Value);
        if (hndEmployeeID.Value != null && hndEmployeeID.Value != "")
        {
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
            lblEmpName.Text = stEmp.EmpNicName;
        }
        if (dtpicklistitem.Rows.Count > 0)
        {
            updatepicklist.Visible = true;
            btnpicklistDelete.Visible = false;
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Installation Manager"))
            {
                if (Roles.IsUserInRole("Administrator"))
                {
                    btnpicklistDelete.Visible = true;
                }
                if (!string.IsNullOrEmpty(dtpicklistitem.Rows[0]["DeductOn"].ToString()) && !string.IsNullOrEmpty(dtpicklistitem.Rows[0]["IsPartialComplete"].ToString()) && dtpicklistitem.Rows[0]["IsPartialComplete"].ToString() != "")
                {
                    if (dtpicklistitem.Rows[0]["IsPartialComplete"].ToString() == "False")
                    {
                        btnwallet.Visible = true;
                        updatepicklist.Visible = false;
                        btnpicklistDelete.Visible = false;
                    }
                    else
                    {
                        btnwallet.Visible = false;
                        updatepicklist.Visible = true;
                    }
                }
                else
                {
                    btnwallet.Visible = false;
                    updatepicklist.Visible = true;
                }
            }
        }
        else
        {
            updatepicklist.Visible = false;
            btnwallet.Visible = false;
        }
        if (dtpicklist.Rows.Count > 0)
        {
            string cc = "";
            if (!string.IsNullOrEmpty(dtpicklist.Rows[0]["Systemdetail"].ToString()))
            {
                cc = dtpicklist.Rows[0]["Systemdetail"].ToString();
                lblstytemdetail.Text = cc.Remove(cc.Length - 5, 5);
            }

        }
        //btnpicklistDelete.Visible = false;
        //if (Roles.IsUserInRole("Administrator"))
        //{
        //    btnpicklistDelete.Visible = true;
        //}
        //if(updatepicklist.CommandName == "Edit")
        //{
        //    string PickID = updatepicklist.CommandArgument; 
        //    DataTable dtpicklistitem = ClstblProjects.tbl_PicklistItemDetail_SelectbyPickId(PickID);
        //rptattribute.DataSource = dtpicklistitem;

        //rptattribute.DataBind();
        //ModalPopupExtender4.Show();

    }
    public void bindDropdownForEditPopup()
    {

        if (txtInstallBookingDate.Text.Trim() != string.Empty)
        {
            string date1 = txtInstallBookingDate.Text.Trim();
            string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller1.Items.Clear();
            ddlInstaller1.Items.Add(item2);
            //   Response.Write(date1+"="+ date2);
            ddlInstaller1.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
            ddlInstaller1.DataValueField = "ContactID";
            ddlInstaller1.DataTextField = "Contact";
            ddlInstaller1.DataMember = "Contact";
            ddlInstaller1.DataBind();
        }
        else
        {
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller1.Items.Clear();
            ddlInstaller1.Items.Add(item2);
            ddlInstaller1.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller1.DataValueField = "ContactID";
            ddlInstaller1.DataTextField = "Contact";
            ddlInstaller1.DataMember = "Contact";
            ddlInstaller1.DataBind();
        }
        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlec.Items.Clear();
        ddlec.Items.Add(item3);

        ddlec.DataSource = ClstblContacts.tblContacts_SelectElectrician();
        ddlec.DataValueField = "ContactID";
        ddlec.DataTextField = "Contact";
        ddlec.DataMember = "Contact";
        ddlec.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddldesigner1.Items.Clear();
        ddldesigner1.Items.Add(item4);

        ddldesigner1.DataSource = ClstblContacts.tblContacts_SelectDesigner();
        ddldesigner1.DataValueField = "ContactID";
        ddldesigner1.DataTextField = "Contact";
        ddldesigner1.DataMember = "Contact";
        ddldesigner1.DataBind();
    }

    public void bindDropdownForinsertPopup()
    {

        if (txtInstallBookingDate.Text.Trim() != string.Empty)
        {
            string date1 = txtInstallBookingDate.Text.Trim();
            string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlinstall2.Items.Clear();
            ddlinstall2.Items.Add(item2);
            //   Response.Write(date1+"="+ date2);
            ddlinstall2.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
            ddlinstall2.DataValueField = "ContactID";
            ddlinstall2.DataTextField = "Contact";
            ddlinstall2.DataMember = "Contact";
            ddlinstall2.DataBind();
        }
        else
        {
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlinstall2.Items.Clear();
            ddlinstall2.Items.Add(item2);
            ddlinstall2.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlinstall2.DataValueField = "ContactID";
            ddlinstall2.DataTextField = "Contact";
            ddlinstall2.DataMember = "Contact";
            ddlinstall2.DataBind();
        }
        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlec2.Items.Clear();
        ddlec2.Items.Add(item3);

        ddlec2.DataSource = ClstblContacts.tblContacts_SelectElectrician();
        ddlec2.DataValueField = "ContactID";
        ddlec2.DataTextField = "Contact";
        ddlec2.DataMember = "Contact";
        ddlec2.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddldesign2.Items.Clear();
        ddldesign2.Items.Add(item4);

        ddldesign2.DataSource = ClstblContacts.tblContacts_SelectDesigner();
        ddldesign2.DataValueField = "ContactID";
        ddldesign2.DataTextField = "Contact";
        ddldesign2.DataMember = "Contact";
        ddldesign2.DataBind();
    }
    protected void rptPickList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        bindrepeateragain();
        BindAddedAttributeagain();
        RepeaterItem item = e.Item;
        LinkButton updatepicklist = (LinkButton)item.FindControl("btnUpdatepicklist");
        int succe = 0;

        //DropDownList  ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        //DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        //TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

        if (e.CommandName == "Edit")
        {
            divMinStock.Visible = false;
            TextBox6.Text = txtInstallBookingDate.Text;
            string PickID = e.CommandArgument.ToString();
            string[] ids = PickID.Split(',');
            DataTable dtpicklistitem = ClstblProjects.tbl_PicklistItemDetail_SelectbyPickId(ids[0].ToString());
            hdnPickId.Value = ids[0].ToString();
            hndAccreditation.Value = ids[1].ToString();
            DataTable dtpickcount = ClstblProjects.Select_CountforPickList(ids[0].ToString());
            bindDropdownForEditPopup();
            if (dtpicklistitem.Rows.Count > 0)
            {
                ddlInstaller1.SelectedValue = dtpicklistitem.Rows[0]["InstallerId"].ToString();
                ddlInstaller1.SelectedItem.Text = dtpicklistitem.Rows[0]["InstallerName"].ToString();
                ddlec.SelectedValue = dtpicklistitem.Rows[0]["ElectricianID"].ToString();
                ddldesigner1.SelectedValue = dtpicklistitem.Rows[0]["DesignerID"].ToString();
                try
                {
                    Repeater1.DataSource = dtpicklistitem;
                    Repeater1.DataBind();
                }
                catch
                { }
                txtpicklistreason.Text = dtpicklistitem.Rows[0]["Reasons"].ToString();
                txtpicklistnote.Text = dtpicklistitem.Rows[0]["Note"].ToString();
            }
            else
            {
                Repeater1.DataSource = null;
                Repeater1.DataBind();

                txtpicklistreason.Text = string.Empty;
                txtpicklistnote.Text = string.Empty;
                bindrepeateragain();
                BindAddedAttributeagain();
            }

            ModalPopupExtenderPickListAgain.Show();
            txtpicklistreason.Enabled = true;
            txtpicklistnote.Enabled = true;
            btnaddagain.Visible = false;
            btnupdate.Visible = true;
            int Qty = 0;
            int scanqty = Convert.ToInt32(dtpickcount.Rows[0]["Column1"]);
            for (int i = 0; i < dtpicklistitem.Rows.Count; i++)
            {
                Qty += Convert.ToInt32(dtpicklistitem.Rows[i]["Qty"]);
                //if (dtpicklistitem.Rows[i]["Qty"].ToString() == dtpickcount.Rows[0]["Column1"].ToString())
                //{
                //    succe++;

                //}
            }
            btnaddagain.Visible = false;
            //if (!(Roles.IsUserInRole("Administrator")))
            //{
            //    //if (succe > 0)
            //    //{
            //    //    btnupdate.Visible = false;

            //    //}
            //    //else
            //    //{
            //    //    btnupdate.Visible = true;
            //    //}
            //    //if (Qty != 0)
            //    //{
            //    //    if (Qty == scanqty)
            //    //    {
            //    //        btnupdate.Visible = false;
            //    //    }
            //    //    else
            //    //    {
            //    //        btnupdate.Visible = true;
            //    //    }
            //    //}
            //    //else
            //    //{
            //    //    btnupdate.Visible = true;
            //    //}

            //}
            //else
            //{
            //    btnupdate.Visible = true;
            //}


        }
        if (e.CommandName == "wallet")
        {
            ModalPopupExtenderwallet.Show();
            string PickID = e.CommandArgument.ToString();
            string[] ids = PickID.Split(',');
            //  HiddenField instId = (HiddenField)item.FindControl("hndInstaller");
            DataTable dtpicklistitem = ClstblProjects.tbl_PicklistItemDetail_SelectbyPickId(ids[0].ToString());
            hdnPickId.Value = PickID;
            DataTable dtpickcount = ClstblProjects.Select_CountforPickList(ids[0].ToString());
            //string Installer = instId.Value;
            hndInstallerId.Value = ids[1].ToString();
            //InstallerId.Value = Installer;
            if (dtpicklistitem.Rows.Count > 0)
            {
                repwallet.DataSource = dtpicklistitem;
                repwallet.DataBind();
                txtpicklistreason.Text = dtpicklistitem.Rows[0]["Reasons"].ToString();
                txtpicklistnote.Text = dtpicklistitem.Rows[0]["Note"].ToString();
            }
            //dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(Installer);
            TextBox3.Enabled = false;
            TextBox4.Enabled = false;
            Btnaddmore.Visible = false;
        }
        if (e.CommandName == "delete")
        {
            string PickID = e.CommandArgument.ToString();
            hndpicklistid.Value = PickID;
            ModalPopupExtenderDelete.Show();
        }
        if (e.CommandName == "notes")
        {
            string PickID = e.CommandArgument.ToString();
            hndpicklistid.Value = PickID;
            DataTable dt = ClstblProjects.tbl_PicklistNotes_Select(PickID);
            div5.Visible = false;
            if (dt.Rows.Count > 0)
            {
                div5.Visible = true;
                rptPicklistNotes.DataSource = dt;
                rptPicklistNotes.DataBind();
            }
            if (dt.Rows.Count > 3)
            {
                div5.Attributes.Add("style", "overflow-y: scroll; height: 200px !important;");
            }
            else
            {
                div5.Attributes.Add("style", "");
            }
            ModalPopupExtenderNotes.Show();
        }
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        divin.Visible = false;
        divpnl.Visible = false;
        string PickId = hdnPickId.Value;
        string proid = Request.QueryString["proid"];
        string Compid = Request.QueryString["compid"];

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        string nm = "";
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        int pnlerror = 0;
        int inverror = 0;
        //int error = 0;
        ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
        foreach (RepeaterItem item in Repeater1.Items)
        {

            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

            if (hdntype.Value != "1")
            {
                //int MinStock = 0;

                // int pnllivestock = 0;
                int LiveStock = 0;
                int pnlwholesale = 0;
                int pnlapl = 0;
                int pnlsmpl = 0;
                int pnlminstock = 0;
                int pnlavailqty = 0;

                int InvLiveStock = 0;
                int Invwholesale = 0;
                int Invapl = 0;
                int Invsmpl = 0;
                int Invminstock = 0;
                int Invavailqty = 0;
                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                // DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty_Qty_Pickist(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue);
                int pnlttl = 0;
                int invttl = 0;

                if (ddlStockCategoryID.SelectedValue == "1")
                {
                    if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                    {
                        if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                        {
                            DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (dtpaneldata.Rows.Count > 0)
                            {
                                if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                                {
                                    pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }
                            //ArisePanelPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerPanelPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //PanelWholeSale
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    pnlwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                        }
                        pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                        pnlavailqty = LiveStock - pnlttl;
                        if ((pnlavailqty <= pnlminstock))
                        {
                            pnlerror++;
                        }
                    }
                    //if (lblPt.Text != null && lblPt.Text != "")
                    //{
                    //    pnlttl = Convert.ToInt32(lblPt.Text);
                    //}
                    //if (pnlttl > 0)
                    //{
                    //    if (dtLive.Rows[0]["StockQuantity"].ToString() != null && dtLive.Rows[0]["StockQuantity"].ToString() != "")
                    //    {
                    //        LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
                    //    }
                    //}
                    //if ((pnlttl <= MinStock))
                    //{
                    //    pnlerror++;
                    //}
                }
                else if (ddlStockCategoryID.SelectedValue == "2")
                {
                    if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                    {
                        if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                        {
                            DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (DtInvData.Rows.Count > 0)
                            {
                                if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                                {
                                    Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }
                            //AriseINVERTERPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerINVERTERPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //InverterWholeSale
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    Invwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                        }
                        invttl = Invapl + Invsmpl + Invwholesale;
                        Invavailqty = InvLiveStock - pnlttl;
                        if ((Invavailqty <= Invminstock))
                        {
                            inverror++;
                        }
                    }

                }
            }
        }
        if (pnlerror > 0)
        {
            divpnl.Visible = true;
            ModalPopupExtenderPickListAgain.Show();
            ModalPopupExtender8.Show();
        }
        if (inverror > 0)
        {
            divin.Visible = true;
            ModalPopupExtenderPickListAgain.Show();
            ModalPopupExtender8.Show();
        }
        //foreach (RepeaterItem item in Repeater1.Items)
        //{
        //    HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        //    if (hdntype.Value != "1")
        //    {
        //        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        //        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        //        int MinStock = 0;
        //        int LiveStock = 0;
        //        //DataTable dtmin = ClstblProjects.sp_GetMinstock(ddlStockItem.SelectedValue);
        //        DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedItem.ToString());
        //        if (dtLive.Rows.Count > 0)
        //        {
        //            MinStock = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
        //        }
        //        if (dtLive.Rows.Count > 0)
        //        {
        //            LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
        //        }
        //        if ((LiveStock <= MinStock))
        //        {
        //            error++;
        //        }
        //    }


        //}

        if (inverror == 0 && pnlerror == 0)
        {
            ClstblProjects.tbl_PicklistItemsDetail_Delete(PickId);
            foreach (RepeaterItem item in Repeater1.Items)
            {
                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(PickId, ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);
                bool succ = ClstblProjects.tbl_PickListLog_Update_ReasonandNotes(PickId, txtpicklistreason.Text, txtpicklistnote.Text);
                ClstblProjects.tbl_PickListLog_Update_InstallBookedDate(PickId, TextBox6.Text);
                bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(PickId, ddlInstaller1.SelectedValue, ddlInstaller1.SelectedItem.Text, ddldesigner1.SelectedValue, ddlec.SelectedValue);

            }
            bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(PickId, ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
            //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(proid);
            DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(proid, "1");
            if (dtPickList.Rows.Count > 0)
            {
                divPickList.Visible = true;
                rptPickList.DataSource = dtPickList;
                rptPickList.DataBind();
            }
            SetAdd1();
            btnupdate.Visible = false;
            Response.Redirect("~/admin/adminfiles/company/company.aspx?m=propre&compid=" + Compid + "&proid=" + proid);
        }
        //else
        //{
        //    ModalPopupExtenderPickListAgain.Show();
        //    divMinStock.Visible = true;
        //    //SetItemError();
        //}


    }
    protected void btnDownloadPickList_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }


        Telerik_reports.generate_PickList(ProjectID);
        try
        {
        }
        catch
        {
        }

    }

    protected void btnwallet_Click(object sender, EventArgs e)
    {

    }

    protected void repwallet_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtQuantity");

        ddlStockCategoryID.Enabled = false;
        ddlStockItem.Enabled = false;
        txtOrderQuantity.Enabled = false;
        //litremove.Visible = false;


        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hdnStockCategoryId");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdbQty = (HiddenField)e.Item.FindControl("hdbQty");
        HiddenField hndqtynew = (HiddenField)e.Item.FindControl("hndqtynew");

        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();

        ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;

        if (ddlStockItem.SelectedValue == "")
        {
            txtOrderQuantity.Text = "0";
        }
        else
        {
            txtOrderQuantity.Text = hndqtynew.Value;
        }




        //if (e.Item.ItemIndex == 0)
        //{

        //    /// btnaddnew.Attributes.Remove("disabled");
        //    //litremove.Attributes.Remove("disabled");
        //    litremove.Visible = false;

        //}
        //else
        //{

        //    // chkdelete.Visible = true;
        //    litremove.Visible = true;


        //}
    }

    protected void btnwallet_Click1(object sender, EventArgs e)
    {

    }

    protected void Btnaddmore_Click(object sender, EventArgs e)
    {

    }
    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }
    protected void btnInserwallet_Click(object sender, EventArgs e)
    {
        try
        {
            string StkId = "";
            //HiddenField hndInstaller = new HiddenField();

            string Instl = InstallerId.Value;

            string PickId = hdnPickId.Value;
            string[] IDs = PickId.Split(',');
            DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(IDs[1].ToString());
            foreach (RepeaterItem i in repwallet.Items)
            {
                HiddenField stockItemID = (HiddenField)i.FindControl("hdnStockItem");
                TextBox txtwalletqty = (TextBox)i.FindControl("txtwalletqty");//diffQty-InstallQty=walletqty
                TextBox txtdiffqty = (TextBox)i.FindControl("txtdiffqty");//InstallQty
                TextBox txtQty = (TextBox)i.FindControl("txtQuantity");//Saved Qty
                TextBox txtinstallQty = (TextBox)i.FindControl("txtinstallQty");//Enter Qty    

                string WallQty = Convert.ToString(txtwalletqty.Text);
                int walletQty = 0;
                int diffqty = 0;
                int InstallQty = 0;
                if (WallQty != null && WallQty != "")
                {
                    walletQty = Convert.ToInt32(WallQty);
                }
                //string difQty = Convert.ToString(txtdiffqty.Text);
                //if (difQty != null && difQty != "")
                //{
                //    diffqty = Convert.ToInt32(difQty);
                //}
                string qty = Convert.ToString(txtQty.Text);
                int Qty = 0;
                if (qty != null && qty != "")
                {
                    Qty = Convert.ToInt32(qty);
                }
                if (!string.IsNullOrEmpty(txtinstallQty.Text))
                {
                    InstallQty = Convert.ToInt32(txtinstallQty.Text);
                }
                if (stockItemID.Value != null)
                {
                    if (txtinstallQty.Text != "0" && !string.IsNullOrEmpty(txtinstallQty.Text))
                    {
                        StkId = stockItemID.Value;
                        //if (dtAcno != null && dtAcno.Rows.Count > 0)
                        //{
                        if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                        {
                            string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                            DataTable dt = ClstblProjects.spexistsPicklIstdata(StkId, Acno);

                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Rows[0]["Qty"].ToString() != null && dt.Rows[0].ToString() != "")
                                {
                                    string OQty = dt.Rows[0]["Qty"].ToString();
                                    int newQty = 0;
                                    if (OQty != null && OQty != null)
                                    {
                                        int Oldqty = Convert.ToInt32(OQty);
                                        newQty = walletQty + Oldqty;
                                        //if (Qty > walletQty)
                                        //{
                                        //    MsgError("Enter Less Quantity Then Qty:=" + Qty);
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        bool s = ClstblProjects.Updatetbl_tbl_Wallet_ItemDetail(StkId, Convert.ToString(newQty), Acno);
                                        // }
                                    }
                                }
                                else
                                {
                                    bool s = ClstblProjects.Updatetbl_tbl_Wallet_ItemDetail(StkId, WallQty, Acno);
                                }
                            }
                            else
                            {
                                int yes = ClstblProjects.Inserttbl_tbl_Wallet_ItemDetail(StkId, Acno, WallQty);


                            }
                            if (txtwalletqty.Text != null && txtwalletqty.Text != "")
                            {
                                int s1 = ClstblProjects.Inserttbl_Walletdata_Picklistwise(StkId, Acno, WallQty, IDs[0].ToString());
                                //bool s2 = ClstblProjects.Update_tbl_PicklistItemDetail_Qty(StkId, Convert.ToString(txtinstallQty.Text), IDs[0].ToString());
                                string proid = Request.QueryString["proid"];
                                bool updateprojectqty = ClstblProjects.tbl_projects_updateqty(StkId, proid, Convert.ToString(txtinstallQty.Text));

                                //STC Calculatation
                                string txtZoneRt = "";
                                string txtPanelBrand = "";
                                string txtPanelModel = "";
                                string txtWatts = "";
                                string txtSystemCapacity = "";
                                string txtSTCMult = "";
                                string txtSTCValue = "";
                                string txtSTCNo = "";
                                string txtRebate = "";
                                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(stockItemID.Value);
                                if (st.StockCategoryID == "1")
                                {
                                    SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(proid);

                                    DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st2.InstallPostCode);
                                    if (dtZoneCode.Rows.Count > 0)
                                    {
                                        string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                                        DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                        txtZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();
                                    }
                                    if (stockItemID.Value != string.Empty)
                                    {
                                        string ProjectID = Request.QueryString["proid"];
                                        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                                        //SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(stockItemID.Value);
                                        txtPanelBrand = st.StockManufacturer;
                                        txtPanelModel = st.StockModel;
                                        txtWatts = st.StockSize;

                                        if (!string.IsNullOrEmpty(st.StockSize) && !string.IsNullOrEmpty(txtinstallQty.Text))
                                        {
                                            SystemCapacity = (Convert.ToDecimal(st.StockSize) * Convert.ToDecimal(txtinstallQty.Text)) / 1000;
                                            txtSystemCapacity = Convert.ToString(SystemCapacity);
                                        }

                                        txtSTCMult = "1";
                                        DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                                        if (dtSTCValue.Rows.Count > 0)
                                        {
                                            try
                                            {
                                                txtSTCValue = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                                            }
                                            catch { }
                                        }
                                        capacity = (Convert.ToDecimal(txtinstallQty.Text) * Convert.ToDecimal(txtWatts)) / 1000;
                                        string installbooking = stPro.InstallBookingDate;
                                        if (installbooking == null || installbooking == "")
                                        {
                                            DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                                            String year = currentdate.Year.ToString();
                                            DataTable dtyear = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                                            Decimal stcrate = Convert.ToDecimal(dtyear.Rows[0]["STCRate"]);
                                            stcno = ((capacity) * Convert.ToDecimal(txtZoneRt) * stcrate);

                                            //-------------------------------------------------------------------
                                            //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                                            //---------------------------------------------------------------------
                                        }
                                        else
                                        {
                                            string date = Convert.ToDateTime(installbooking).ToShortDateString();
                                            DateTime dt1 = Convert.ToDateTime(date);
                                            String year = dt1.Year.ToString();
                                            DataTable dtyear = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                                            Decimal stcrate = Convert.ToDecimal(dtyear.Rows[0]["STCRate"]);
                                            stcno = ((capacity) * Convert.ToDecimal(txtZoneRt) * stcrate);

                                        }
                                        int myInt = (int)Math.Floor(stcno);
                                        txtSTCNo = Convert.ToString(myInt);
                                        rebate = myInt * Convert.ToDecimal(txtSTCValue);
                                        txtRebate = Convert.ToString(rebate);

                                        bool succ = ClstblProjects.tblProjects_STCValues(proid, txtRebate, txtZoneRt, txtSystemCapacity, txtSTCNo, stockItemID.Value);

                                    }
                                    else
                                    {
                                        txtPanelBrand = string.Empty;
                                        txtPanelModel = string.Empty;
                                        txtWatts = string.Empty;
                                    }
                                }
                            }

                        }
                        // }
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void txtwalletqty_TextChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem i in repwallet.Items)
        {
            HiddenField stockItemID = (HiddenField)i.FindControl("hdnStockItem");
            TextBox txtwalletqty = (TextBox)i.FindControl("txtwalletqty");//diffQty-InstallQty=walletqty
            TextBox txtdiffqty = (TextBox)i.FindControl("txtdiffqty");//InstallQty 
            TextBox txtQty = (TextBox)i.FindControl("txtQuantity");//  Actual Qty
            TextBox txtinstallQty = (TextBox)i.FindControl("txtinstallQty");//Enter Qty
            string WallQty = Convert.ToString(txtwalletqty.Text);

            int walletQty = 0;
            int diffqty = 0;


            if (WallQty != null && WallQty != "")
            {
                walletQty = Convert.ToInt32(WallQty);
            }
            string difQty = Convert.ToString(txtdiffqty.Text);
            if (difQty != null && difQty != "")
            {
                diffqty = Convert.ToInt32(difQty);
            }
            //int remQty = Qty-walletQty;
            // txtQty.Text = Convert.ToString(remQty);
            string qty = Convert.ToString(txtQty.Text);
            int Qty = 0;
            if (qty != null && qty != "")
            {
                Qty = Convert.ToInt32(qty);
            }
            if (Qty < walletQty)
            {
                MsgError("Enter Less Quantity Then Qty:=" + Qty);
                //return;
            }

        }
        ModalPopupExtenderwallet.Show();
    }

    protected void ddlInstaller1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlInstaller.SelectedValue != string.Empty)
        {
            // Response.Write(ddlInstaller.SelectedValue);
            BindScript();
            DataTable dtDes = ClstblContacts.tblContacts_SelectDesignerByContactID(ddlInstaller1.SelectedValue);
            if (dtDes.Rows.Count > 0)
            {
                ddldesigner1.SelectedValue = ddlInstaller1.SelectedValue;
            }
            else
            {
                ddldesigner1.SelectedValue = "";
            }

            DataTable dtEle = ClstblContacts.tblContacts_SelectElectricianByContactID(ddlInstaller1.SelectedValue);
            if (dtEle.Rows.Count > 0)
            {
                ddlec.SelectedValue = ddlInstaller1.SelectedValue;
            }
            else
            {
                ddlec.SelectedValue = "";
            }
        }
        else
        {
            ddldesigner1.SelectedValue = "";
            ddlec.SelectedValue = "";
        }
        ModalPopupExtenderPickListAgain.Show();
    }


    protected void ddlinstall2_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlinstall2.SelectedValue != string.Empty)
        {
            // Response.Write(ddlInstaller.SelectedValue);
            BindScript();
            DataTable dtDes = ClstblContacts.tblContacts_SelectDesignerByContactID(ddlinstall2.SelectedValue);
            if (dtDes.Rows.Count > 0)
            {
                ddldesign2.SelectedValue = ddlinstall2.SelectedValue;
            }
            else
            {
                ddldesigner1.SelectedValue = "";
            }

            DataTable dtEle = ClstblContacts.tblContacts_SelectElectricianByContactID(ddlinstall2.SelectedValue);
            if (dtEle.Rows.Count > 0)
            {
                ddlec2.SelectedValue = ddlinstall2.SelectedValue;
            }
            else
            {
                ddlec2.SelectedValue = "";
            }
        }
        else
        {
            ddldesign2.SelectedValue = "";
            ddlec2.SelectedValue = "";
        }
        ModalPopupExtender4.Show();
    }

    protected void txtinstallQty_TextChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem i in repwallet.Items)
        {
            HiddenField stockItemID = (HiddenField)i.FindControl("hdnStockItem");
            TextBox txtdiffqty = (TextBox)i.FindControl("txtdiffqty");//Diff
            TextBox txtQty = (TextBox)i.FindControl("txtQuantity");//           
            TextBox txtinstallQty = (TextBox)i.FindControl("txtinstallQty");
            TextBox txtwalletqty = (TextBox)i.FindControl("txtwalletqty");

            int walletQty = 0;
            int diffqty = 0;
            int installQty = 0;
            int Qty = 0;

            if (!string.IsNullOrEmpty(txtinstallQty.Text))
            {
                installQty = Convert.ToInt32(txtinstallQty.Text);
                diffqty = Convert.ToInt32(txtdiffqty.Text);
                walletQty = diffqty - installQty;
                txtwalletqty.Text = Convert.ToString(walletQty);
                txtQty.Text = txtinstallQty.Text;
            }

        }
        ModalPopupExtenderwallet.Show();
    }

    protected void ddlStockItem_SelectedIndexChanged1(object sender, EventArgs e)
    {
        ////bindrepeateragain();
        ////BindAddedAttributeagain();
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = Repeater1.Items[index];
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtwallateQty = (TextBox)item.FindControl("txtwallateQty");

        string wallQty = "0";
        txtwallateQty.Text = "0";
        if (ddlInstaller1.SelectedValue != string.Empty)
        {
            if (ddlStockItem.SelectedValue != string.Empty)
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlInstaller1.SelectedValue);
                string StockItemid = ddlStockItem.SelectedValue.ToString();
                if (dtAcno.Rows.Count > 0)
                {
                    if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                    {
                        string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                        DataTable dt = ClstblProjects.spexistsPicklIstdata(StockItemid, Acno);

                        if (dt.Rows.Count > 0)
                        {
                            wallQty = dt.Rows[0]["Qty"].ToString();
                            txtwallateQty.Text = wallQty;
                        }
                    }
                }
            }
            else
            {
                MsgError("Please select Item");
            }
        }
        else
        {
            MsgError("Please select Installer");
        }

        ModalPopupExtenderPickListAgain.Show();
    }

    protected void ddlStockItem_SelectedIndexChanged2(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtwallateQty = (TextBox)item.FindControl("txtwallateQty");

        string wallQty = "0";
        txtwallateQty.Text = "0";
        if (ddlinstall2.SelectedValue != string.Empty)
        {
            if (ddlStockItem.SelectedValue != string.Empty)
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlinstall2.SelectedValue);
                string StockItemid = ddlStockItem.SelectedValue.ToString();
                if (dtAcno.Rows.Count > 0)
                {
                    if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                    {
                        string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                        DataTable dt = ClstblProjects.spexistsPicklIstdata(StockItemid, Acno);

                        if (dt.Rows.Count > 0)
                        {
                            wallQty = dt.Rows[0]["Qty"].ToString();
                            txtwallateQty.Text = wallQty;
                        }
                    }
                }

            }
            else
            {
                MsgError("Please select Item");
            }
        }
        else
        {
            MsgError("Please select Installer");
        }

        ModalPopupExtender4.Show();
    }


    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string proid = Request.QueryString["proid"];
        string Compid = Request.QueryString["compid"];
        if (!string.IsNullOrEmpty(hndpicklistid.Value))
        {
            ClstblProjects.tbl_PickListLog_DeleteByID(hndpicklistid.Value);
            ClstblProjects.tbl_PicklistItemsDetail_Delete(hndpicklistid.Value);
            SetAdd1();
        }
        else
        {
            SetError1();
        }
        Response.Redirect("~/admin/adminfiles/company/company.aspx?m=propre&compid=" + Compid + "&proid=" + proid);
        BindProjectPreInst(proid);
    }
    public void MsgSuccess(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyGreenfun('{0}');", msg), true);
    }

    protected void btnnote_Click(object sender, EventArgs e)
    {
        string picklistid = hndpicklistid.Value;
        string ProjectID = Request.QueryString["proid"];
        string note = txtpicknote.Text;
        ClstblProjects.tbl_PicklistNotes_Insert(picklistid, ProjectID, note);
        DataTable dt = ClstblProjects.tbl_PicklistNotes_Select(picklistid);
        div5.Visible = false;
        if (dt.Rows.Count > 0)
        {
            div5.Visible = true;
            rptPicklistNotes.DataSource = dt;
            rptPicklistNotes.DataBind();
        }
        ModalPopupExtenderNotes.Show();
    }

    protected void rptPicklistNotes_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string picklistid = hndpicklistid.Value;
        if (e.CommandName == "delete")
        {
            string id = e.CommandArgument.ToString();
            ClstblProjects.tbl_PicklistNotes_delete(id);
            DataTable dt = ClstblProjects.tbl_PicklistNotes_Select(picklistid);
            div5.Visible = false;
            if (dt.Rows.Count > 0)
            {
                div5.Visible = true;
                rptPicklistNotes.DataSource = dt;
                rptPicklistNotes.DataBind();
            }
            ModalPopupExtenderNotes.Show();
        }
    }

    protected void lblPO_Click(object sender, EventArgs e)
    {
        Label10.Text = "Stock Order Detail";
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        //ModalPopupExtenderOrder.Show();
        string nm = "";
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        if (!string.IsNullOrEmpty(st.PanelBrandID) && !string.IsNullOrEmpty(nm))
        {
            DataTable dtorderQty = ClstblProjects.tblStockOrderItems_SelectByStockItemIDandCompanyLocationID(st.PanelBrandID, nm);
            if (dtorderQty.Rows.Count > 0)
            {
                ModalPopupExtender3.Show();
                Repeater3.DataSource = dtorderQty;
                Repeater3.DataBind();
                if (dtorderQty.Rows.Count > 5)
                {
                    divOrStockderDetail.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
                }
                else
                {
                    divOrStockderDetail.Attributes.Add("style", "");
                }
            }
            else
            {
                SetNoRecords();
            }
        }
    }

    protected void lblIo_Click(object sender, EventArgs e)
    {
        lblname.Text = "Stock Order Detail";
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        string nm = "";
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        string itemid = "";
        if (!string.IsNullOrEmpty(st.InverterDetailsID))
        {
            itemid = st.InverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID))
        {
            itemid = st.InverterDetailsID + "," + st.SecondInverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(st.ThirdInverterDetailsID))
        {
            itemid = st.InverterDetailsID + "," + st.SecondInverterDetailsID + "," + st.ThirdInverterDetailsID;
        }
        if (!string.IsNullOrEmpty(itemid))
        {
            string Query = "select *,(select ExpectedDelivery from tblStockOrders where StockOrderID=tblStockOrderItems.StockOrderID ) as ExpDelivery from" +
                " tblStockOrderItems where StockLocation " +
                "in(select state  From tblcompanylocations where CompanyLocation ='" + nm + "') and tblStockOrderItems.StockItemID in (" + itemid + ") and " +
" StockOrderID in (Select StockOrderID from tblStockOrders where Delivered = 0) ";
            //           string Query = "select * from tblStockOrderItems where StockLocation in(select state from tblCompanyLocations " +
            //            "where CompanyLocationID =" + st.StockAllocationStore + ")  and tblStockOrderItems.StockItemID in (" + itemid + ") and " +
            //" StockOrderID in (Select StockOrderID from tblStockOrders where Delivered = 0),(select ExpectedDelivery from tblStockOrders where StockOrderID=tblStockOrderItems.StockOrderID ) as ExpDelivery  ";
            //DataTable dtorderQty = ClstblProjects.tblStockOrderItems_SelectByStockItemIDandCompanyLocationID(st.PanelBrandID, st.StockAllocationStore);
            DataTable dtorderQty = ClstblCustomers.query_execute(Query);
            if (dtorderQty.Rows.Count > 0)
            {
                ModalPopupExtender3.Show();
                Repeater3.DataSource = dtorderQty;
                Repeater3.DataBind();
                if (dtorderQty.Rows.Count > 5)
                {
                    divOrStockderDetail.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
                }
                else
                {
                    divOrStockderDetail.Attributes.Add("style", "");
                }
            }
            else
            {
                SetNoRecords();
            }
        }
    }

    protected void lblPW_Click(object sender, EventArgs e)
    {
        lblname.Text = "Wholesale Order Detail";
        divOrderDetail.Visible = true;
        ModalPopupExtenderOrder.Show();
        string ProjectID = Request.QueryString["proid"];

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string nm = "";
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        DataTable dt = ClstblProjects.tbl_WholesaleOrderItems_SelectByStockItemIDandCompanyLocationID(st.PanelBrandID, nm);
        if (dt != null && dt.Rows.Count > 0)
        {
            rptOrderDetil.Visible = true;
            rptOrderDetil.DataSource = dt;
            rptOrderDetil.DataBind();
            if (dt.Rows.Count > 5)
            {
                div8.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                div8.Attributes.Add("style", "");
            }
        }
        else
        {
            SetNoRecords();
            rptOrderDetil.Visible = false;
        }
        ModalPopupExtenderOrder.Show();
    }

    protected void lblIW_Click(object sender, EventArgs e)
    {
        lblname.Text = "Wholesale Order Detail";
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        string nm = "";
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        string itemid = "";
        if (!string.IsNullOrEmpty(st.InverterDetailsID))
        {
            itemid = st.InverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID))
        {
            itemid = st.InverterDetailsID + "," + st.SecondInverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(st.ThirdInverterDetailsID))
        {
            itemid = st.InverterDetailsID + "," + st.SecondInverterDetailsID + "," + st.ThirdInverterDetailsID;
        }
        if (!string.IsNullOrEmpty(itemid))
        {
            // string Query = "select WholesaleOrderID as StockOrderID,WholesaleOrderItem as StockOrderItem,OrderQuantity from tbl_WholesaleOrderItems" +
            //" where WholesaleLocation in(select state from tblCompanyLocations where CompanyLocationID =" + st.StockAllocationStore + ") and " +
            //"tbl_WholesaleOrderItems.StockItemID in (" + itemid + ") and  WholesaleOrderID in " +
            //"(Select WholesaleOrderID from tbl_WholesaleOrders where Delivered = 0) ";
            string Query = "select tbl_WholesaleOrderItems.OrderQuantity as OrderQuantity, " +
                "tbl_WholesaleOrderItems.WholesaleOrderID  as StockOrderID, WholesaleOrderItem as StockOrderItem,OrderQuantity,concat(tbl_WholesaleOrders.InvoiceNo,'/',tbl_WholesaleOrderItems.WholesaleOrderID) as InvoiceNo," +
                "tbl_WholesaleOrders.ExpectedDelivery as ExpDelivery,(select customer from tblcustomers where custtypeid=6 and CustomerId=tbl_WholesaleOrders.CustomerId) as WhCust  from tbl_WholesaleOrderItems join tbl_WholesaleOrders  on tbl_WholesaleOrderItems.WholesaleOrderID = tbl_WholesaleOrders.WholesaleOrderID" +
                " where WholesaleLocation in(select state from tblCompanyLocations where CompanyLocation = '" + nm + "') and tbl_WholesaleOrderItems.StockItemID in(" + itemid + ") and  tbl_WholesaleOrders.StockDeductDate is null and tbl_WholesaleOrders.JobStatusid in(1,4) and tbl_WholesaleOrders.InvoiceTypeId=2  order by   DateOrdered desc";
            //DataTable dtorderQty = ClstblProjects.tblStockOrderItems_SelectByStockItemIDandCompanyLocationID(st.PanelBrandID, st.StockAllocationStore);
            DataTable dtorderQty = ClstblCustomers.query_execute(Query);
            if (dtorderQty.Rows.Count > 0)
            {
                ModalPopupExtenderOrder.Show();
                rptOrderDetil.Visible = true;
                rptOrderDetil.DataSource = dtorderQty;
                rptOrderDetil.DataBind();
                if (dtorderQty.Rows.Count > 5)
                {
                    div8.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
                }
                else
                {
                    div8.Attributes.Add("style", "");
                }
            }
            else
            {
                SetNoRecords();
            }
        }
    }

    protected void txtInstallBookingDate_TextChanged1(object sender, EventArgs e)
    {
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (st.InstallEndDate != null && st.InstallEndDate != "")
        {
            //DateTime InstallDate = Convert.ToDateTime(st.InstallBookingDate);
            DateTime EndDate = Convert.ToDateTime(st.InstallEndDate);
            //DateTime TodayDate = Convert.ToDateTime(txtInstallBookingDate.Text);
            //string TodayDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();
            string InstallDate = Convert.ToDateTime(txtInstallBookingDate.Text).ToShortDateString();
            //string EndDate = Convert.ToDateTime(txtInstallBookingDate.Text).AddDays(28).ToShortDateString();
            //string TodayDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();
            if (Convert.ToDateTime(InstallDate) <= Convert.ToDateTime(EndDate))
            {
                btnPickList.Visible = true;
                btnaddnew.Visible = true;
            }
            else
            {
                btnPickList.Visible = false;
                btnaddnew.Visible = false;
                divinstalldate.Visible = true;
            }
        }
    }

    protected void lblApl_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        if (st.PanelBrandID != null && st.PanelBrandID != "")
        {
            DataTable dt = ClstblProjects.tbl_PickListLog_SelectBy_CompnayID("1", st.PanelBrandID, ddlStockAllocationStore.SelectedValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                Repeater2.DataSource = dt;
                Repeater2.DataBind();
                if (dt.Rows.Count > 5)
                {
                    div7.Attributes.Add("style", "overflow-x: scroll; height: 400px !important; max-width:90%; width:100%; margin-left:auto!important; margin-right:auto!important;");
                }
                else
                {
                    div7.Attributes.Add("style", "");
                }
            }
            else
            {
                SetNoRecords();
            }
        }
        else
        {
            SetNoRecords();
        }

    }

    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        if (e.CommandName == "delete")
        {
            string PickID = e.CommandArgument.ToString();
            hndpicklistid.Value = PickID;
            ModalPopupExtenderDelete.Show();
        }

    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        HiddenField hdnCompanyid = (HiddenField)item.FindControl("hdnCompanyid");
        LinkButton updatepicklist = (LinkButton)item.FindControl("btnUpdatepicklist");
        LinkButton btnwallet = (LinkButton)item.FindControl("btnwallet");
        LinkButton btnpicklistDelete = (LinkButton)item.FindControl("btnpicklistDelete");
        HiddenField picklistid = (HiddenField)item.FindControl("hndpicklistid");
        Label lblstytemdetail = (Label)item.FindControl("lblstytemdetail");
        HiddenField hndEmployeeID = (HiddenField)item.FindControl("hdnEmpId");
        Label lblEmpName = (Label)item.FindControl("lblEmpName");
        DataTable dtpicklistitem = ClstblProjects.tbl_PicklistItemDetail_SelectbyPickId(picklistid.Value);
        //DataTable dtpicklist = ClstblProjects.tbl_PicklistItemDetail_SelectByPId(picklistid.Value);
        if (hdnCompanyid.Value == "1")
        {
            if (hndEmployeeID.Value != null && hndEmployeeID.Value != "")
            {
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
                lblEmpName.Text = stEmp.EmpNicName;
            }
        }
        else if (hdnCompanyid.Value == "2")
        {
            if (hndEmployeeID.Value != null && hndEmployeeID.Value != "")
            {
                SttblEmployees stEmp = ClstblEmployees.Get_SolarMiner_tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
                lblEmpName.Text = stEmp.EmpNicName;
            }
        }
        if (dtpicklistitem.Rows.Count > 0)
        {
            // updatepicklist.Visible = true;
            btnpicklistDelete.Visible = false;
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager"))
            {
                btnpicklistDelete.Visible = true;
            }
        }
        else
        {
            btnpicklistDelete.Visible = false;

        }
        //if (dtpicklist.Rows.Count > 0)
        //{
        //    string cc = "";
        //    if (!string.IsNullOrEmpty(dtpicklist.Rows[0]["Systemdetail"].ToString()))
        //    {
        //        cc = dtpicklist.Rows[0]["Systemdetail"].ToString();
        //        lblstytemdetail.Text = cc.Remove(cc.Length - 5, 5);
        //    }

        //}
    }

    protected void lblSMPL_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        if (st.PanelBrandID != null && st.PanelBrandID != "")
        {
            //SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.PanelBrandID);
            DataTable dt = ClstblProjects.tbl_PickListLog_SelectBy_CompnayID("2", st.PanelBrandID, ddlStockAllocationStore.SelectedValue);
            // DataTable dt = ClstblProjects.tbl_PickListLog_SelectBy_CompnayID("2", st1.FixStockItemID);
            if (dt != null && dt.Rows.Count > 0)
            {
                Repeater2.DataSource = dt;
                Repeater2.DataBind();
                if (dt.Rows.Count > 5)
                {
                    Data.Attributes.Add("style", "overflow-x: scroll; height: 400px !important; max-width:100%; width:100%; margin-left:auto!important; margin-right:auto!important;");
                }
                else
                {
                    div7.Attributes.Add("style", "");
                }
            }
            else
            {
                ModalPopupExtender2.Hide();
                SetNoRecords();
            }
        }
        else
        {
            SetNoRecords();
        }

    }

    protected void lblAriseInv_Click(object sender, EventArgs e)
    {
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        string itemid = "";
        if (!string.IsNullOrEmpty(st.InverterDetailsID))
        {
            itemid = st.InverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID))
        {
            itemid = st.InverterDetailsID + "," + st.SecondInverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(st.ThirdInverterDetailsID))
        {
            itemid = st.InverterDetailsID + "," + st.SecondInverterDetailsID + "," + st.ThirdInverterDetailsID;
        }
        if (!string.IsNullOrEmpty(itemid))
        {
            string Qr = "select * From tbl_PickListLog  join  tbl_PicklistItemDetail on tbl_picklistlog.id = tbl_PicklistItemDetail.PickId " +
                "where Companyid =1  and tbl_PicklistItemDetail.StockitemId in (" + itemid + ") and Picklistlocation='" + ddlStockAllocationStore.SelectedItem.Text + "' and tbl_picklistlog.DeductOn is null order By tbl_picklistlog.InstallBookedDate Desc";
            DataTable dt = ClstblCustomers.query_execute(Qr);

            if (dt != null && dt.Rows.Count > 0)
            {
                ModalPopupExtender2.Show();
                Repeater2.DataSource = dt;
                Repeater2.DataBind();
                if (dt.Rows.Count > 5)
                {
                    Data.Attributes.Add("style", "overflow-x: scroll; height: 400px !important; max-width:100%; width:100%; margin-left:auto!important; margin-right:auto!important;");
                }
                else
                {
                    Data.Attributes.Add("style", "");
                }
            }
            else
            {
                ModalPopupExtender2.Hide();
                SetNoRecords();
            }
        }
        else
        {
            SetNoRecords();
        }
    }

    protected void lblSmInv_Click(object sender, EventArgs e)
    {
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        string itemid = "0";
        string itemid1 = "0";
        string itemid2 = "0";
        string items = "";
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && st.InverterDetailsID != "0")
        {
            //SttblStockItems sttblStockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.InverterDetailsID);
            //if (!string.IsNullOrEmpty(sttblStockItems.FixStockItemID))
            //{
            //    itemid = sttblStockItems.FixStockItemID;
            //}st.InverterDetailsID
            itemid = st.InverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID) && st.SecondInverterDetailsID != "0")
        {
            //SttblStockItems sttblStockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.SecondInverterDetailsID);
            //if (!string.IsNullOrEmpty(sttblStockItems.FixStockItemID))
            //{
            //    //itemid = sttblStockItems.FixStockItemID;
            //    itemid1 = sttblStockItems.FixStockItemID;
            //}
            itemid = st.SecondInverterDetailsID;
        }
        if (!string.IsNullOrEmpty(st.InverterDetailsID) && !string.IsNullOrEmpty(st.SecondInverterDetailsID) && !string.IsNullOrEmpty(st.ThirdInverterDetailsID))
        {
            //SttblStockItems sttblStockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.ThirdInverterDetailsID);
            //itemid2 = sttblStockItems.FixStockItemID;
            //itemid = itemid + "," + itemid + "," + sttblStockItems.FixStockItemID;
        }
        items = st.InverterDetailsID + "," + st.SecondInverterDetailsID + "," + st.ThirdInverterDetailsID;
        items = items.TrimEnd(',');
        //string itemid = "";

        if (!string.IsNullOrEmpty(items))
        {
            string Qr = "select * From tbl_PickListLog  join  tbl_PicklistItemDetail on tbl_picklistlog.id = tbl_PicklistItemDetail.PickId " +
                "where Companyid =2  and tbl_PicklistItemDetail.StockitemId in (" + items + ") and Picklistlocation='" + ddlStockAllocationStore.SelectedItem.Text + "' and tbl_picklistlog.DeductOn is null order By id Desc";
            DataTable dt = ClstblCustomers.query_execute(Qr);

            if (dt != null && dt.Rows.Count > 0)
            {
                ModalPopupExtender2.Show();
                Repeater2.DataSource = dt;
                Repeater2.DataBind();
                if (dt.Rows.Count > 5)
                {
                    Data.Attributes.Add("style", "overflow-x: scroll; height: 400px !important; max-width:100%; width:100%; margin-left:auto!important; margin-right:auto!important;");
                }
                else
                {
                    div7.Attributes.Add("style", "");
                }
            }
            else
            {
                ModalPopupExtender2.Hide();
                SetNoRecords();
            }
        }
        else
        {
            SetNoRecords();
        }
    }

    protected void lnkDraft_Click(object sender, EventArgs e)
    {

        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Compid = Request.QueryString["compid"];
        string ProjectID = "";

        int success = 0;
        int FirstInv = 0;
        int Secondinv = 0;
        int thirdinv = 0;
        int TotalInv = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
        if (PickListExist == 0)
        {
            // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            int pnl = 0;
            int inv = 0;
            int LiveStock = 0;
            int pnlwholesale = 0;
            int pnlapl = 0;
            int pnlsmpl = 0;
            int pnlminstock = 0;
            int pnlavailqty = 0;

            int InvLiveStock = 0;
            int Invwholesale = 0;
            int Invapl = 0;
            int Invsmpl = 0;
            int Invminstock = 0;
            int Invavailqty = 0;

            int pnlttl = 0;
            int invttl = 0;

            int pnlerror = 0;
            int inverror = 0;

            foreach (RepeaterItem item in rptattribute.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    int pnlTotalOrderQtyETA = 0;
                    int pnlDraftPckQty = 0;
                    int pnlRemQty = 0;
                    int pnlOrderQty = 0;


                    int invTotalOrderQtyETA = 0;
                    int invDraftPckQty = 0;
                    int invRemQty = 0;
                    int invOrderQty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");

                    //
                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        
                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            #region ETA
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    if (dtorderQty.Rows[0]["OrderQuantity"].ToString() != null && dtorderQty.Rows[0]["OrderQuantity"].ToString() != "")
                                    {
                                        pnlTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());

                                    }
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (dtDraftQty.Rows[0]["orderQty"].ToString() != null && dtDraftQty.Rows[0]["orderQty"].ToString() != "")
                                {
                                    pnlDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                pnlOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }

                            pnlRemQty = pnlTotalOrderQtyETA - pnlDraftPckQty;
                            if (pnlOrderQty > pnlRemQty)
                            {
                                pnl++;
                            }
                            #endregion ETA
                            #region TotalCount
                            DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (dtpaneldata.Rows.Count > 0)
                            {
                                if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                                {
                                    pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }

                            //ArisePanelPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerPanelPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //PanelWholeSale
                            DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    pnlwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                            pnlavailqty = LiveStock - pnlttl;
                            if ((pnlavailqty <= pnlminstock))
                            {
                                pnlerror++;
                            }

                            #endregion
                        }
                    }
                    if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            #region ETA
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    invTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtDraftQty.Rows[0]["orderQty"].ToString()))
                                {
                                    invDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                invOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (invOrderQty > invRemQty)
                            {
                                inv++;
                            }
                            #endregion ETA
                            #region Total
                            DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (DtInvData.Rows.Count > 0)
                            {
                                if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                                {
                                    Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }
                            //AriseINVERTERPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerINVERTERPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //InverterWholeSale
                            DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    Invwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            invttl = Invapl + Invsmpl + Invwholesale;
                            Invavailqty = InvLiveStock - invttl;
                            if ((Invavailqty <= Invminstock))
                            {
                                inverror++;
                            }

                            #endregion Total
                        }
                    }

                }
            }

            if ((pnl == 0 || pnlerror==0) && (inv == 0 || inverror==0))
            {
                    success = ClstblProjects.tblPickListLog_Insert(ProjectID, TextBox1.Text, TextBox2.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                    bool suc1 = ClstblProjects.tbl_picklistlog_updatePicklistType(success.ToString(), "1");//1)Draft (2) PickLIst
                    if (success > 0)
                    {
                        ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                        //Update IsTopone by PickList Id
                        ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                        //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                        ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                        try
                        {
                            bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                            bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                        }
                        catch (Exception ex) { }
                        //Telerik_reports.generate_PickList(ProjectID);
                        // picklistdiv.Visible = false;
                        bool P = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                        bool I = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                        foreach (RepeaterItem item in rptattribute.Items)
                        {
                            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                            if (hdntype.Value != "1")
                            {

                                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                                // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                                int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                                //string stockitem = ddlStockItem.SelectedItem.Text;                          
                                if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                                {
                                    if (ddlStockCategoryID.SelectedValue == "1")
                                    {
                                        if (st.NumberPanels != null && st.NumberPanels != "")
                                        {
                                            bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                        }
                                        else
                                        {
                                            bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                        }
                                    }
                                    if (ddlStockCategoryID.SelectedValue == "2")
                                    {
                                        if (st.inverterqty != null && st.inverterqty != "")
                                        {
                                            FirstInv = Convert.ToInt32(st.inverterqty);
                                        }
                                        if (st.inverterqty2 != null && st.inverterqty2 != "")
                                        {
                                            Secondinv = Convert.ToInt32(st.inverterqty2);
                                        }
                                        if (st.inverterqty3 != null && st.inverterqty3 != "")
                                        {
                                            thirdinv = Convert.ToInt32(st.inverterqty3);
                                        }
                                        TotalInv = FirstInv + Secondinv + thirdinv;
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                    }
                                }
                            }
                        }
                        ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        BindProjectPreInst(ProjectID);
                        SetAdd1();
                    ModalPopupExtender6.Hide();
                    ModalPopupExtender4.Hide();
                }
                //}

            }
            if (pnl > 0)
            {
                DivPanel1.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Hide();
            }
            if (inv > 0)
            {
                DivInverter1.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Hide();
            }

        }
    }
    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Compid = Request.QueryString["compid"];
        string ProjectID = "";

        int success = 0;
        int FirstInv = 0;
        int Secondinv = 0;
        int thirdinv = 0;
        int TotalInv = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
        if (PickListExist == 0)
        {
            // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            int pnl = 0;
            int inv = 0;

            foreach (RepeaterItem item in Repeater1.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    int pnlTotalOrderQtyETA = 0;
                    int pnlDraftPckQty = 0;
                    int pnlRemQty = 0;
                    int pnlOrderQty = 0;


                    int invTotalOrderQtyETA = 0;
                    int invDraftPckQty = 0;
                    int invRemQty = 0;
                    int invOrderQty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");

                    //
                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    if (dtorderQty.Rows[0]["OrderQuantity"].ToString() != null && dtorderQty.Rows[0]["OrderQuantity"].ToString() != "")
                                    {
                                        pnlTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());

                                    }
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (dtDraftQty.Rows[0]["orderQty"].ToString() != null && dtDraftQty.Rows[0]["orderQty"].ToString() != "")
                                {
                                    pnlDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                pnlOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }
                            pnlRemQty = pnlTotalOrderQtyETA - pnlDraftPckQty;
                            if (pnlOrderQty > pnlRemQty)
                            {
                                pnl++;
                            }
                        }
                    }
                    if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    invTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtDraftQty.Rows[0]["orderQty"].ToString()))
                                {
                                    invDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                invOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (invOrderQty > invRemQty)
                            {
                                inv++;
                            }
                        }
                    }

                }
            }

            if (pnl == 0 && inv == 0)
            {
                success = ClstblProjects.tblPickListLog_Insert(ProjectID, TextBox1.Text, TextBox2.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                bool suc1 = ClstblProjects.tbl_picklistlog_updatePicklistType(success.ToString(), "1");//1)Draft (2) PickLIst
                if (success > 0)
                {
                    ClstblProjects.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                    //Update IsTopone by PickList Id
                    ClstblProjects.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                    ClstblProjects.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                    try
                    {
                        bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                        bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                    }
                    catch (Exception ex) { }
                    //Telerik_reports.generate_PickList(ProjectID);
                    // picklistdiv.Visible = false;
                    bool P = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                    bool I = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                    foreach (RepeaterItem item in Repeater1.Items)
                    {
                        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                        if (hdntype.Value != "1")
                        {

                            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                            // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                            int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                            //string stockitem = ddlStockItem.SelectedItem.Text;                          
                            if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                            {
                                if (ddlStockCategoryID.SelectedValue == "1")
                                {
                                    if (st.NumberPanels != null && st.NumberPanels != "")
                                    {
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                    }
                                    else
                                    {
                                        bool s2 = ClstblProjects.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                    }
                                }
                                if (ddlStockCategoryID.SelectedValue == "2")
                                {
                                    if (st.inverterqty != null && st.inverterqty != "")
                                    {
                                        FirstInv = Convert.ToInt32(st.inverterqty);
                                    }
                                    if (st.inverterqty2 != null && st.inverterqty2 != "")
                                    {
                                        Secondinv = Convert.ToInt32(st.inverterqty2);
                                    }
                                    if (st.inverterqty3 != null && st.inverterqty3 != "")
                                    {
                                        thirdinv = Convert.ToInt32(st.inverterqty3);
                                    }
                                    TotalInv = FirstInv + Secondinv + thirdinv;
                                    bool s2 = ClstblProjects.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                }
                            }
                        }
                    }
                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                    BindProjectPreInst(ProjectID);
                    SetAdd1();
                    ModalPopupExtender7.Hide();
                    //ModalPopupExtender4.Hide();
                }
                //}

            }
            if (pnl > 0)
            {
                DivPanel1.Visible = true;
                ModalPopupExtenderPickListAgain.Show();
                ModalPopupExtender7.Hide();
            }
            if (inv > 0)
            {
                DivInverter1.Visible = true;
                ModalPopupExtenderPickListAgain.Show();
                ModalPopupExtender7.Hide();
            }

        }
    }

    protected void LnkUpdate_Click(object sender, EventArgs e)
    {

        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Compid = Request.QueryString["compid"];
        string ProjectID = "";
        string PickId = hdnPickId.Value;
        int success = 0;
        int FirstInv = 0;
        int Secondinv = 0;
        int thirdinv = 0;
        int TotalInv = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        ////int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
        ////if (PickListExist == 0)
        ////{
            // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            int pnl = 0;
            int inv = 0;
        int LiveStock = 0;
        int pnlwholesale = 0;
        int pnlapl = 0;
        int pnlsmpl = 0;
        int pnlminstock = 0;
        int pnlavailqty = 0;

        int InvLiveStock = 0;
        int Invwholesale = 0;
        int Invapl = 0;
        int Invsmpl = 0;
        int Invminstock = 0;
        int Invavailqty = 0;

        int pnlttl = 0;
        int invttl = 0;

        int pnlerror = 0;
        int inverror = 0;

        foreach (RepeaterItem item in Repeater1.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    int pnlTotalOrderQtyETA = 0;
                    int pnlDraftPckQty = 0;
                    int pnlRemQty = 0;
                    int pnlOrderQty = 0;


                    int invTotalOrderQtyETA = 0;
                    int invDraftPckQty = 0;
                    int invRemQty = 0;
                    int invOrderQty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");

                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                    #region ETA
                    DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                        {
                            if (dtorderQty.Rows[0]["OrderQuantity"].ToString() != null && dtorderQty.Rows[0]["OrderQuantity"].ToString() != "")
                            {
                                pnlTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());

                            }
                        }
                    }
                    DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                    if (dtDraftQty.Rows.Count > 0)
                    {
                        if (dtDraftQty.Rows[0]["orderQty"].ToString() != null && dtDraftQty.Rows[0]["orderQty"].ToString() != "")
                        {
                            pnlDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                        }
                    }
                    if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                    {
                        pnlOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                    }

                    pnlRemQty = pnlTotalOrderQtyETA - pnlDraftPckQty;
                    if (pnlOrderQty > pnlRemQty)
                    {
                        pnl++;
                    }
                    #endregion ETA
                    #region TotalCount
                    DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                    if (dtpaneldata.Rows.Count > 0)
                    {
                        if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                        {
                            LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                        }
                        if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                        {
                            pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                        }
                    }

                    //ArisePanelPicklistCount
                    DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                    if (DtApl.Rows.Count > 0)
                    {
                        if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                        {
                            pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                    //SolarminerPanelPicklistCount
                    DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                    if (DtSmpl.Rows.Count > 0)
                    {
                        if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                        {
                            pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                            //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                        }
                    }
                    //PanelWholeSale
                    DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                    if (dtorderQty.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                        {
                            pnlwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                        }
                    }
                    pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                    pnlavailqty = LiveStock - pnlttl;
                    if ((pnlavailqty <= pnlminstock))
                    {
                        pnlerror++;
                    }

                    #endregion
                }
                if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                        #region ETA
                        DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                        if (dtorderQty.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                            {
                                invTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                            }
                        }
                        DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                        if (dtDraftQty.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(dtDraftQty.Rows[0]["orderQty"].ToString()))
                            {
                                invDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                            }
                        }
                        invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                        if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                        {
                            invOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                        }
                        invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                        if (invOrderQty > invRemQty)
                        {
                            inv++;
                        }
                        #endregion ETA
                        #region Total
                        DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                        if (DtInvData.Rows.Count > 0)
                        {
                            if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                            {
                                InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                            }
                            if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                            {
                                Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                            }
                        }
                        //AriseINVERTERPicklistCount
                        DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                        if (DtApl.Rows.Count > 0)
                        {
                            if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                            {
                                Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                            }
                        }
                        //SolarminerINVERTERPicklistCount
                        DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                        if (DtSmpl.Rows.Count > 0)
                        {
                            if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                            {
                                Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                            }
                        }
                        //InverterWholeSale
                        DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                        if (dtorderQty.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                            {
                                Invwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                            }
                        }
                        invttl = Invapl + Invsmpl + Invwholesale;
                        Invavailqty = InvLiveStock - invttl;
                        if ((Invavailqty <= Invminstock))
                        {
                            inverror++;
                        }

                        #endregion Total
                            }
                }

                }
            }

            if((pnl == 0 || pnlerror == 0) && (inv == 0 || inverror == 0))
            {
                ClstblProjects.tbl_PicklistItemsDetail_Delete(PickId);
                foreach (RepeaterItem item in Repeater1.Items)
                {
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                    int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(PickId, ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);
                    bool succ = ClstblProjects.tbl_PickListLog_Update_ReasonandNotes(PickId, txtpicklistreason.Text, txtpicklistnote.Text);
                    ClstblProjects.tbl_PickListLog_Update_InstallBookedDate(PickId, TextBox6.Text);
                    bool s1 = ClstblProjects.tbl_PickListLog_updateInstallerAndEcDetails(PickId, ddlInstaller1.SelectedValue, ddlInstaller1.SelectedItem.Text, ddldesigner1.SelectedValue, ddlec.SelectedValue);

                }
                bool loc = ClstblProjects.tbl_PickListLog_Update_locationId_CompanyWise(PickId, ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
            bool suc1 = ClstblProjects.tbl_picklistlog_updatePicklistType(PickId, "1");//1)Draft (2) PickLIst
                                                                                                   //DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(proid);
            DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");
                if (dtPickList.Rows.Count > 0)
                {
                    divPickList.Visible = true;
                    rptPickList.DataSource = dtPickList;
                    rptPickList.DataBind();
                }
                SetAdd1();
                btnupdate.Visible = false;
                Response.Redirect("~/admin/adminfiles/company/company.aspx?m=propre&compid=" + Compid + "&proid=" + ProjectID);

            }
            if (pnl > 0)
            {
                divpnl.Visible = true;
                ModalPopupExtenderPickListAgain.Show();
                ModalPopupExtender8.Hide();
            }
            if (inv > 0)
            {
                DivInverter1.Visible = true;
                ModalPopupExtenderPickListAgain.Show();
                ModalPopupExtender8.Hide();
            }

       // }

    }
}