<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectdetail.ascx.cs"
    Inherits="includes_controls_projectdetail" %>


<input type="hidden" id="chkaddvalpro" value="0" />
<style>
    .ui-autocomplete-loading {
        background: white url("../../../images/indicator.gif") right center no-repeat;
    }
</style>
<style type="text/css">
    .selected_row {
        background-color: #A1DCF2!important;
    }
</style>
<%--<script src="../../assets/js/jquery.min.js"></script>--%>
<script type="text/javascript">
            //Function to disable validator
            function disableValidator() {
                 var valName6 = document.getElementById("<%=RequiredFieldValidator4.ClientID%>");
               // var e = document.getElementById(ddlProjectTypeID);
                var e= document.getElementById('<%= ddlProjectTypeID.ClientID%>');
                var selval = e.options[e.selectedIndex].value;
                if (selval == "5" || selval == "3") {
                    ValidatorEnable(valName6, true);
                }
                else {
                    ValidatorEnable(valName6, false
                    );
                }
            }
        </script>
<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        //HighlightControlToValidate();
        <%--    $('#<%=btnUpdateDetail.ClientID %>').click(function () {
            formValidate();
        });
        $('#<%=ibtnUpdateDetail.ClientID %>').click(function () {
            formValidate();
        });--%>


    
        $('#<%=btnUpdateDetail.ClientID %>').click(function (e) {
            formValidate();
          
        });



        $('#<%=ibtnUpdateDetail.ClientID %>').click(function (e) {
            formValidate();
           
        });

    });

    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(this).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(this).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
<script type="text/javascript">

    function Check(txtProjectNotes, maxLength) {

        if (txtProjectNotes.value.length > maxLength) {

            txtProjectNotes.value = txtProjectNotes.value.substr(0, maxLength);
        }
    }

</script>

<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>
         
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                        <div class="alert alert-danger" id="PanCompanyError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Please First Update Information in Company.</strong>
                        </div>
                    </div>


                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-info" id="lblerror" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this address aleardy Exist</strong>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="paddall">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Project Type<span class="symbol required"></span>
                                                            </label>
                                                        </span>
                                                        <div class="drpValidate">
                                                            <asp:DropDownList ID="ddlProjectTypeID" runat="server"  Width="200px"  aria-controls="DataTables_Table_0" class="myval"
                                                                AppendDataBoundItems="true" onchange="disableValidator()">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                                ControlToValidate="ddlProjectTypeID" Display="Dynamic" ValidationGroup="projectdetail"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="divOnHold" runat="server" visible="false">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                OnHold Reason<span class="symbol required"></span>
                                                            </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlProjectOnHoldID" runat="server" Width="200px" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value=""></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                                                ValidationGroup="detail1" ControlToValidate="ddlProjectOnHoldID" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="divCancelReason" runat="server" visible="false">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Cancel Reason<span class="symbol required"></span>
                                                            </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlProjectCancelID" runat="server" Width="200px" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value=""></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required."
                                                                ValidationGroup="detail1" ControlToValidate="ddlProjectCancelID" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="divSalesRep" runat="server">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Sales Rep<span class="symbol required"></span>
                                                            </label>
                                                        </span>                                                     
                                                        <div class="drpValidate">
                                                            <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                Width="200px" AppendDataBoundItems="true" Enabled="false">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                                ValidationGroup="projectdetail" ControlToValidate="ddlSalesRep" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </div>                                                        
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Contact<span class="symbol required"></span>
                                                            </label>
                                                        </span>
                                                        <div class="drpValidate">
                                                            <asp:DropDownList ID="ddlContact" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                Width="200px" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                                ValidationGroup="projectdetail" ControlToValidate="ddlContact" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Old Project No.
                                                            </label>
                                                        </span><span class="withpersonal">
                                                            <asp:TextBox ID="txtOldProjectNumber" runat="server" MaxLength="200" class="form-control" ></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Manual Quote No.
                                                            </label>
                                                        </span><span class="withpersonal">
                                                            <asp:TextBox ID="txtManualQuoteNumber" runat="server" MaxLength="200" class="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" style="display:none" >
                                                        <div class="form-group col-md-6" id="divFollowupDate" runat="server" visible="false" style="display:none">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Follow Up Date
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtFollowupDate" runat="server" Width="120px" CssClass="form-control"
                                                                    Enabled="false"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6" id="divNextFollowUpDate" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Next Follow Up
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtNextFollowUpDate" runat="server" Width="120px" CssClass="form-control"
                                                                    Enabled="false"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-6">
                                                            
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                House statys<span class="symbol required"></span>
                                                            </label>
                                                        </span>
                                                        <div class="drpValidate">
                                                            <asp:DropDownList ID="ddlHousestatys" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                Width="200px" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>                                                            
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <span class="name disblock">
                                                            <label class="control-label">
                                                                Completion Date<span class="symbol required"></span>
                                                            </label>
                                                        </span>
                                                                    <div class="input-group date datetimepicker1 ">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtStartDate" runat="server" class="form-control" Width="125px">
                                                                        </asp:TextBox>
                                                                        <%--<cc1:MaskedEditExtender ID="mskeditCheckIn" runat="server" Mask="99/99/9999" MessageValidatorTip="true"
                                                                        OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="date"
                                                                        TargetControlID="txtStartDate" CultureName="en-GB">
                                                                    </cc1:MaskedEditExtender>--%>
                                                                    </div>
                                                            </div>
                                                    </div>
                                                    <%--<div class="form-group">
                                                        
                                                                </div>--%>

                                                    <div class="form-group">
                                                       <%-- <span class="name disblock">
                                                            <label class="control-label">
                                                                SM Ready<span class="symbol required"></span>
                                                            </label>
                                                        </span>--%>
                                                                     <div class=" checkbox-info checkbox">
                                                                            <span class="fistname">
                                                                                <label for="<%=chkclickSMready.ClientID %>">
                                                                                    <asp:CheckBox ID="chkclickSMready" runat="server" />   
                                                                                    <span class="text">SM Ready  &nbsp;</span>                                                                                
                                                                                </label>                                                                           
                                                                        </div>
                                                                </div>

                                                    <div class="form-group row" style="display:none" >
                                                        <div  class="form-group col-md-6" id="divinstaller" runat="server" visible="false" style="padding-top:12px">
                                                        <asp:Button ID="btnInstDetail" runat="server" Text="Installation Detail" OnClick="btnInstDetail_Click" CssClass="btninstallation" />
                                                        </div>
                                                        <div class="form-group col-md-6 " runat="server" id="divclickcustomer" visible="false">
                                                         <div class=" checkbox-info checkbox">
                                                                            <span class="fistname">
                                                                                <label for="<%=chkclickcustomer.ClientID %>">
                                                                                    <asp:CheckBox ID="chkclickcustomer" runat="server" />
                                                                                    <span class="text">Click Customer  &nbsp;</span>
                                                                                </label>
                                                                            </span><asp:Button class="btn btn-primary savewhiteicon" ID="btnSave" runat="server" OnClick="btnSave_Click"  Text="Save" />
                                                                        </div>
                                                       <%-- <label>Click Customer</label>
                                                        <asp:CheckBox ID="chkclickcustomer" runat="server" />
                                                        <label for="<%=chkclickcustomer.ClientID %>">
                                                            <span></span>
                                                        </label>--%>
                                                        
                                                    </div>
                                                        
                                                    </div>


                                                    
                                                     
                                                    
                                                    <%--<div class="form-group checkareanew" runat="server" id="divcancelproject" visible="false">
                                                        <label>Project Cancel</label>
                                                        <asp:CheckBox ID="chkprojectcancel" runat="server" />
                                                        <label for="<%=chkprojectcancel.ClientID %>">
                                                            <span></span>
                                                        </label>
                                                        <div class="clear"></div>
                                                    </div>--%>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Project Opened<span class="symbol required"></span>
                                                            </label>
                                                        </span><span>

                                                            <asp:TextBox ID="txtProjectOpened" runat="server" Style="float: left;" Width="120px" class="form-control" ReadOnly="true"></asp:TextBox>
                                                            <%--<asp:ImageButton ID="Image15" Enabled="false" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                    <cc1:CalendarExtender ID="CalendarExtender15" runat="server" PopupButtonID="Image15"
                                                        TargetControlID="txtProjectOpened" Format="dd/MM/yyyy">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtProjectOpened"
                                                        ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>--%>

                                                        </span>


                                                        <div class="clear">
                                                        </div>
                                                    </div>




                                                    <div class="form-group">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Install Site
                                                            </label>
                                                        </span><span class="fullspan">
                                                            <div id="Div1" class="marginbtm10  col-md-12" visible="false" runat="server">
                                                                <asp:HiddenField ID="hndaddress" runat="server" />
                                                                <asp:TextBox ID="txtInstallAddressline" runat="server" MaxLength="200" class="form-control"></asp:TextBox>
                                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                                ControlToValidate="txtInstallAddressline" Display="Dynamic" ValidationGroup="detail1"></asp:RequiredFieldValidator>--%>
                                                                <%-- <asp:CustomValidator ID="Customdetail" runat="server"
                                                                    ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="projectdetail" Enabled="false"
                                                                    ClientValidationFunction="ChkFundetail"></asp:CustomValidator>--%>
                                                            </div>
                                                            <div class="marginbtm10">

                                                                <asp:Panel runat="server" ID="PanInstallAddress">
                                                                    <asp:TextBox ID="txtInstallAddress" runat="server" MaxLength="200" Enabled="false" class="form-control"></asp:TextBox>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required."
                                                                    ControlToValidate="txtInstallAddress" Display="Dynamic" ValidationGroup="detail1"></asp:RequiredFieldValidator>--%>
                                                                </asp:Panel>

                                                            </div>
                                                            <asp:HiddenField ID="hndstreetno" runat="server" />
                                                            <asp:HiddenField ID="hndstreetname" runat="server" />
                                                            <asp:HiddenField ID="hndstreettype" runat="server" />
                                                            <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                            <asp:HiddenField ID="hndunittype" runat="server" />
                                                            <asp:HiddenField ID="hndunitno" runat="server" />
                                                            <div id="validaddressid" style="display: none">
                                                                <i class='fa fa-check'></i>
                                                                <%--        <img src="../../../images/check.png" alt="check">--%>
                                                                Address is valid.
                                                            </div>
                                                            <div id="invalidaddressid" style="display: none">
                                                                <i class='fa fa-close'></i>
                                                                <%--   <img src="../../../images/x.png" alt="cross">--%>
                                                                Address is invalid.
                                                            </div>
                                                            <div class="onelindiv marginbtm10 row">
                                                                <span class="col-sm-6">
                                                                    <asp:TextBox ID="txtformbayUnitNo" runat="server" placeholder="Unit No" CssClass="form-control" OnTextChanged="txtformbayUnitNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required."
                                                                        ControlToValidate="txtformbayUnitNo" Display="Dynamic" ValidationGroup="detail1"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <span class="col-sm-6">
                                                                    <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="true" OnSelectedIndexChanged="ddlformbayunittype_SelectedIndexChanged">
                                                                        <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                                        ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="detail1"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <div class="onelindiv marginbtm10 row">
                                                                <span class="col-md-3 col-sm-3">
                                                                    <asp:TextBox ID="txtformbayStreetNo" runat="server" placeholder="Street No" CssClass="form-control" OnTextChanged="txtformbayStreetNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage=""
                                                                        ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="detail1"></asp:RequiredFieldValidator>
                                                                </span>
                                                                <span class="col-md-4 col-sm-4 padleftzero">
                                                                    <asp:TextBox ID="txtformbaystreetname" runat="server" placeholder="Street Name" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                                        ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="projectdetail"></asp:RequiredFieldValidator>
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                        ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="projectdetail"
                                                                        ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                                    <div id="Divvalidstreetname" style="display: none">
                                                                        <i class='fa fa-check'></i>
                                                                        <%--       <img src="../../../images/check.png" alt="check">--%>
                                                                        Address is valid.
                                                                    </div>
                                                                    <div id="DivInvalidstreetname" style="display: none">
                                                                        <i class='fa fa-close'></i>
                                                                        <%--    <img src="../../../images/x.png" alt="cross">--%>
                                                                        Address is invalid.
                                                                    </div>
                                                                </span>
                                                                <span class="col-md-5 col-sm-5 padleftzero">
                                                                    <div class="drpValidate">
                                                                        <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                        </asp:DropDownList>

                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage=""
                                                                            ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="projectdetail"></asp:RequiredFieldValidator>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="marginbtm10 row">
                                                        <span class="col-sm-6 col-md-6">

                                                            <asp:TextBox ID="txtInstallCity" runat="server" placeholder="Street Name" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtInstallCity_TextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                                                ControlToValidate="txtInstallCity" Display="Dynamic" ValidationGroup="projectdetail"></asp:RequiredFieldValidator>
                                                            <cc1:AutoCompleteExtender CompletionListCssClass="autocompletedrop" ID="autocomplete_detail" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtInstallCity" ServicePath="~/Search.asmx"
                                                                ServiceMethod="GetCitiesList" EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />


                                                        </span>
                                                        <span class="col-sm-3 col-md-3 padleftzero">
                                                            <asp:TextBox ID="txtInstallState" runat="server" MaxLength="200" class="form-control"></asp:TextBox></span>
                                                        <span class="col-sm-3 col-md-3 padleftzero">
                                                            <asp:TextBox ID="txtInstallPostCode" runat="server" MaxLength="200" class="form-control"></asp:TextBox></span>

                                                    </div>

                                                    <div class="clear">
                                                    </div>
                                                    <asp:ImageButton ID="btnCustAddress" Style="margin-top: 5px;" runat="server" OnClick="btnCustAddress_Click"
                                                        ImageUrl="~/images/user_cust_address_btn1.png" CausesValidation="false" />
                                                    </span>
                                                        <div class="clear">
                                                        </div>
                                                </div>

                                                <asp:Panel runat="server">
                                                <div class="form-group"  style="margin-top: 46px;">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Old System Details
                                                        </label>
                                                        <br />
                                                    </span><span class="with44">
                                                        <asp:TextBox ID="txtoldsystemdetails" CssClass="form-control" runat="server" Width="100%"
                                                            Height="118px" TextMode="MultiLine" ></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                                ValidationGroup="projectdetail" ControlToValidate="txtoldsystemdetails" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                    </asp:Panel>
                                              <%--  <div class="form-group" id="divFollowUpNote" runat="server" visible="false" style="margin-top: 46px;">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Follow Up
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtFollowUpNote" runat="server" CssClass="form-control textareabox" Height="110px"
                                                            TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>--%>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Project Notes
                                                        </label>
                                                        <br />
                                                    </span><span>
                                                        <asp:TextBox ID="txtProjectNotes" CssClass="form-control" OnTextChanged="txtProjectNotes_TextChanged" MaxLength="250" onkeyup="charcountupdate(this.value)" AutoPostBack="true" runat="server" Width="100%" Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Installer Notes
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtInstallerNotes" runat="server" Width="100%" Height="118px"
                                                            TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                        <%--<asp:ImageButton ID="btnViewNotes" runat="server" CausesValidation="false"
                                                                OnClick="btnViewNotes_Click" ImageUrl="~/images/new/view_notes1.png" />--%>

                                                        <asp:Button class="savewhiteicon btnviewnotes savewhiteicon martop5"
                                                            ID="btnViewNotes" runat="server" OnClick="btnViewNotes_Click"
                                                            Text="View Notes" CausesValidation="true" />

                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Notes for Installation Department
                                                        </label>
                                                        <br />
                                                    </span><span class="with44">
                                                        <asp:TextBox ID="txtMeterInstallerNotes" CssClass="form-control" Enabled="false" runat="server" Width="100%"
                                                            Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 textcenterbutton center-text">
                                <div style="text-align: center;">
                                    <asp:Button class="btn btn-primary savewhiteicon savewhiteicon btnsaveicon" ID="btnUpdateDetail" runat="server" OnClick="btnUpdateDetail_Click"
                                        Text="Save" CausesValidation="true" ValidationGroup="projectdetail" />
                                </div>
                            </div>
                            <br />
                        </div>
                    </asp:Panel>


                </div>
            </div>

        </section>

   
<!------------------------------------ View Installer Notes ------------------------------------>
<cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
    CancelControlID="ibtnCancelNotes" DropShadow="false" PopupControlID="divAddNotes" TargetControlID="btnNULLNotes">
</cc1:ModalPopupExtender>
<div id="divAddNotes" runat="server" style="display: none" class="modal_popup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                 <div style="float: right">
                <button id="ibtnCancelNotes" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                     </div>
                <h3 class="modal-title" id="myModalLabel">
                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                    Installer Notes</h3>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <div id="divInstallgrid" runat="server">
                        <div class="table-responsive tableblack tableminpadd finalgrid">
                            <asp:GridView ID="GridView1" DataKeyNames="id"
                                runat="server" PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft">
                                        <ItemTemplate>
                                            <%#Eval("Notes")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                        <ItemTemplate>
                                            <%#Eval("Employee")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="130px" HeaderStyle-CssClass="brdrgrayright">
                                        <ItemTemplate>
                                            <%#Eval("Date","{0:dd MMM yyyy}")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULLNotes" Style="display: none;" runat="server" />
<!---------------------------------------------------------------------------------------------->

<!----------------------------------- Update Installer Detail ---------------------------------->
<cc1:ModalPopupExtender ID="ModalPopupExtenderInst" runat="server" BackgroundCssClass="modalbackground"
    CancelControlID="ibtnCancelInstDetail" DropShadow="false" PopupControlID="divInstDetail"
    TargetControlID="btnNULLInstDetail">
</cc1:ModalPopupExtender>


<div id="divInstDetail" runat="server" class="modal_popup" align="center" style="width: auto;">

    <div class="modal-dialog " style="width: 400px;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                 <div style="float: right">
                <asp:LinkButton ID="ibtnCancelInstDetail" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                    Close
                </asp:LinkButton>
                     </div>
                <h3 class="modal-title" id="H2">Update Installation Detail</h3>
                <%--  <h4 class="modal-title" id="H1">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                      </h4>--%>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <div style="width: 215px; margin: 0px auto; border: 0px solid #ccc;">

                        <div class="form-group checkareanew">

                            <div class="name left-text form-group">
                                <span></span>
                                <div class=" checkbox-info checkbox">
                                    <span class="fistname">
                                        <label for="<%=chkElecDistOK.ClientID %>" class="control-label">
                                            <asp:CheckBox ID="chkElecDistOK" runat="server" />
                                            <span class="text">Elec Dist OK  &nbsp;</span>
                                        </label>
                                    </span>
                                </div>
                                <div class="clear">
                                </div>
                            </div>

                            <div class="form-group" id="divElecDistApproved" runat="server">
                                <span class="name">
                                    <asp:Label ID="Label23" runat="server" class="control-label">
                                            Elec Dist Approved</asp:Label></span>
                                <div class="input-group date datetimepicker1">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <asp:TextBox ID="txtElecDistApproved" runat="server" class="form-control" Width="175px">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEDA" runat="server" ErrorMessage=""
                                        Visible="false" ValidationGroup="instdetail" ControlToValidate="txtElecDistApproved"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </div>


                            <div class="form-group">
                                <span class="name left-text">
                                    <label class="control-label">
                                        Installer
                                    </label>
                                </span>
                                <div style="text-align: left;">
                                    <div style="width: 210px;">
                                        <div class="drpValidate">
                                            <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" Width="200px" CssClass="myval">
                                            </asp:DropDownList>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" ValidationGroup="instdetail"
                                                ControlToValidate="ddlInstaller" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <br />
                                <div class="form-group center-text">
                                    <span class="name"></span><span class="dateimg">
                                        <asp:Button ID="ibtnUpdateDetail" runat="server" Text="Update" OnClick="ibtnUpdateDetail_Click"
                                            CssClass="btn btn-primary btnsaveicon" ValidationGroup="instdetail" />
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<asp:Button ID="btnNULLInstDetail" Style="display: none;" runat="server" />
<!---------------------------------------------------------------------------------------------->


<asp:Button ID="Button2" Style="display: none;" runat="server" />
<cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
    DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
    OkControlID="btnOKAddress" TargetControlID="Button2">
</cc1:ModalPopupExtender>
<div id="divAddressCheck" runat="server" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <div style="float: right">
                <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close</button>
                     </div>
                <h4 class="modal-title" id="H4">Duplicate Address</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                        <tbody>
                            <tr align="center">
                                <td>
                                    <h4 class="noline" style="color: black"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                        This looks like a Duplicate Entry.</b></h4>
                                </td>
                            </tr>
                            <tr align="center">
                                <td>
                                    <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="tablescrolldiv">
                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                            <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server"
                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Customers">
                                        <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadeddetail);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress

            //if (args.get_error() != undefined) {
            //    args.set_errorhandled(true);
            //}

        }
        function pageLoadeddetail() {


            //gridviewScroll();
            $('#<%=btnUpdateDetail.ClientID %>').click(function () {
                formValidate();
               
            });
            $('#<%=ibtnUpdateDetail.ClientID %>').click(function () {
                formValidate();               
            });
            ////alert($(".search-select").attr("class"));
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            //$("[data-toggle=tooltip]").tooltip();
            //$("searchbar").attr("imgbtn");
            //gridviewScroll();
            //=============Street 
        }
        function address() {

            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

          $.ajax({
                type: "POST",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                        $("#chkaddvalpro").val("1");
                    }
                    else {
                        $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                        $("#chkaddvalpro").val("0");
                    }
                }
            });
        }
        //=================Street



        function validatestreetAddress(source, args) {
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
                $.ajax({
                    type: "POST",
                    //action:"continue.aspx",
                    url: "company.aspx/Getstreetname",
                    data: "{'streetname':'" + streetname + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == true) {
                            //document.getElementById("Divvalidstreetname").style.display = "block";
                            //document.getElementById("DivInvalidstreetname").style.display = "none";
                            $("#chkaddvalpro").val("1");
                        }
                        else {
                            //document.getElementById("Divvalidstreetname").style.display = "none";
                            //document.getElementById("DivInvalidstreetname").style.display = "block";
                            $("#chkaddvalpro").val("0");
                        }
                    }
                });
                var mydataval = $("#chkaddvalpro").val();
                if (mydataval == "1") {
                   // args.IsValid = true;
                }
                else {
                    //args.IsValid = false;
                }
            }
    </script>
    </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpdateDetail" />
    </Triggers>
</asp:UpdatePanel>






