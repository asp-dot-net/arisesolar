﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

public partial class includes_controls_projectdetail : System.Web.UI.UserControl
{
    protected static string Siteurl;
    protected static string address;
    protected void Page_Load(object sender, EventArgs e)
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;

        if (!IsPostBack)
        {

            StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st1.siteurl;
            string ProjectID = Request.QueryString["proid"];
            ////
            ////Response.End();
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                // Response.Write(ProjectID);
                SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                if ((Roles.IsUserInRole("Sales Manager")))
                {
                    if (st2.ProjectStatusID == "3")
                    {
                        btnUpdateDetail.Visible = false;
                        btnSave.Visible = false;
                    }
                }
                if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Administrator"))
                {
                    divclickcustomer.Visible = true;
                }

                //if (Roles.IsUserInRole("SalesRep"))
                //{
                //    if (st2.DepositReceived != string.Empty)
                //    {
                //        btnUpdateDetail.Visible = false;
                //    }
                //}

                //    if (st2.ProjectTypeID == "8")
                //    {
                //        ddlProjectTypeID_SelectedIndexChanged(sender, e);
                //    }

                ModalPopupExtenderInst.Hide();
            }
        }
    }

    public void BindProjectDetail()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                string companyid = Request.QueryString["compid"];
                SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(companyid);
                string formbayadd = st.isformbayadd;

                txtInstallState.Attributes.Add("readonly", "readonly");
                txtInstallPostCode.Attributes.Add("readonly", "readonly");

                if (st.isformbayadd != string.Empty)
                {
                    if (Convert.ToBoolean(st.isformbayadd) == true)
                    {
                        txtInstallAddressline.Enabled = true;
                        //Customdetail.Enabled = true;
                        //rfv_detail.Enabled = true;
                        //txtInstallAddress.ReadOnly = true;
                        //txtInstallAddress.Enabled = false;
                        PanInstallAddress.Enabled = false;
                        autocomplete_detail.Enabled = true;
                        //txtInstallCity.Attributes.Add("readonly", "readonly");
                        //txtInstallCity.ReadOnly = true;
                    }
                    else if (Convert.ToBoolean(st.isformbayadd) == false)
                    {
                        txtInstallAddressline.ReadOnly = true;
                        //Customdetail.Enabled = false;
                        // rfv_detail.Enabled = false;
                        txtInstallAddress.Enabled = true;
                        autocomplete_detail.Enabled = true;
                        txtInstallCity.Enabled = true;
                    }
                }
            }
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (Roles.IsUserInRole("Administrator"))
            {
                ddlSalesRep.Enabled = true;
                PanAddUpdate.Enabled = true;
                divinstaller.Visible = true;
                //txtProjectOpened.Enabled = true;
                //Image15.Enabled = true;
            }

            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                PanAddUpdate.Enabled = true;
                divinstaller.Visible = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                ddlSalesRep.Enabled = true;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st2.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }
            if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Administrator"))
            {
                txtMeterInstallerNotes.Enabled = true;
            }
            else
            {
                txtMeterInstallerNotes.Enabled = false;

            }
            chkclickcustomer.Checked = Convert.ToBoolean(st2.IsClickCustomer);

            if (Roles.IsUserInRole("Installation Manager"))
            {
                divinstaller.Visible = true;
                ddlSalesRep.CssClass = "myval";
                //CssClass is used because if we enable the dropdown false,then the class file is not applied.So,it is added from the backcode.
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st2.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }

            BindDropDown();

            if (Roles.IsUserInRole("SalesRep") && (st2.ProjectStatusID == "3" || st2.ProjectStatusID == "5"))
            {
                PanAddUpdate.Enabled = false;
            }
            try
            {
                ddlSalesRep.SelectedValue = st2.SalesRep;
            }
            catch { }
            try
            {
                ddlProjectTypeID.SelectedValue = st2.ProjectTypeID;
                // if project type is Maintainanace

                // HideAndShowPanel(st2.ProjectTypeID);


            }
            catch { }

            try
            {
                ddlContact.SelectedValue = st2.ContactID;
            }
            catch { }

            try
            {
                txtProjectOpened.Text = Convert.ToDateTime(st2.ProjectOpened).ToShortDateString();
            }
            catch { }

            try
            {
                txtOldProjectNumber.Text = st2.OldProjectNumber;
            }
            catch { }
            try
            {
                txtManualQuoteNumber.Text = st2.ManualQuoteNumber;
            }
            catch { }
            try
            {
                txtInstallAddressline.Text = st2.InstallAddress + ", " + st2.InstallCity + " " + st2.InstallState + " " + st2.InstallPostCode;
            }
            catch { }

            try
            {
                txtformbayUnitNo.Text = st2.unit_number;
            }
            catch { }
            try
            {
                //Response.Write(st2.unit_type);
                //Response.End();
                ddlformbayunittype.SelectedValue = st2.unit_type;
            }
            catch { }
            try
            {
                txtformbayStreetNo.Text = st2.street_number;
            }
            catch { }
            try
            {
                txtformbaystreetname.Text = st2.street_name;
            }
            catch { }
            try
            {
                ddlformbaystreettype.SelectedValue = st2.street_type;
            }
            catch { }

            try
            {
                txtInstallCity.Text = st2.InstallCity;
            }
            catch { }
            try
            {
                txtInstallState.Text = st2.InstallState;
            }
            catch { }
            try
            {
                txtInstallPostCode.Text = st2.InstallPostCode;
            }
            catch { }
            try
            {
                //Response.Write(txtformbayUnitNo.Text +"=="+ ddlformbayunittype.SelectedValue);
                //Response.End();
                txtInstallAddress.Text = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue; //st2.InstallAddress;
            }
            catch { }
            try
            {
                txtProjectNotes.Text = st2.ProjectNotes;
            }
            catch { }
            try
            {
                ddlInstaller.SelectedValue = st2.Installer;
            }
            catch { }
            try
            {
                txtElecDistApproved.Text = Convert.ToDateTime(st2.ElecDistApproved).ToShortDateString();
            }
            catch { }
            try
            {
                ddlHousestatys.SelectedValue = st2.HouseStayID;
            }
            catch { }
            try
            {
                txtStartDate.Text = st2.HouseStayDate;
            }
            catch { }

            //try
            //{
            //    chkprojectcancel.Checked = Convert.ToBoolean(st2.projectcancel);
            //}
            //catch { }
            if (st2.IsSMReady == "True")
            {
                chkclickSMready.Checked = true;
            }
            DataTable dtFollwUp = ClstblCustInfo.tblCustInfo_SelectTop1ByCustID(st2.CustomerID);
            if (dtFollwUp.Rows.Count > 0)
            {
                divFollowupDate.Visible = true;
                divNextFollowUpDate.Visible = true;
                //divFollowUpNote.Visible = true;

                try
                {
                    txtFollowupDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["FollowupDate"].ToString()));
                }
                catch { }
                try
                {
                    txtNextFollowUpDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["NextFollowupDate"].ToString()));
                }
                catch { }
                // txtFollowUpNote.Text = dtFollwUp.Rows[0]["Description"].ToString();
            }
            else
            {
                divFollowupDate.Visible = false;
                divNextFollowUpDate.Visible = false;
                // divFollowUpNote.Visible = false;
            }

            try
            {
                txtInstallerNotes.Text = st2.InstallerNotes;
            }
            catch { }
            try
            {
                txtMeterInstallerNotes.Text = st2.MeterInstallerNotes;
            }
            catch { }
            try
            {
                txtoldsystemdetails.Text = st2.OldSystemDetails;
            }
            catch { }

            DataTable dtInstNotes = ClstblInstallerNotes.tblInstallerNotes_Select(Request.QueryString["proid"]);
            if (dtInstNotes.Rows.Count > 0)
            {
                btnViewNotes.Visible = true;
            }
            else
            {
                btnViewNotes.Visible = false;
            }
            //if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
            //{
            //    divcancelproject.Visible = true;
            //}
            //else
            //{
            //    divcancelproject.Visible = false;
            //}

            //if (ddlProjectTypeID.SelectedItem.Text == "Replacing" || ddlProjectTypeID.SelectedItem.Text == "Solar UG")
            //{
            //    oldsystemdetaildiv.Visible = true;
            //}
            //else
            //{
            //    oldsystemdetaildiv.Visible = false;
            //}
        }
    }


    public void BindDropDown()
    {
        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlProjectTypeID.Items.Clear();
        ddlProjectTypeID.Items.Add(item2);

        ddlProjectTypeID.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectTypeID.DataValueField = "ProjectTypeID";
        ddlProjectTypeID.DataTextField = "ProjectType";
        ddlProjectTypeID.DataMember = "ProjectType";
        ddlProjectTypeID.DataBind();

        ListItem item3 = new ListItem();
        item3.Text = "Installer";
        item3.Value = "";
        ddlInstaller.Items.Clear();
        ddlInstaller.Items.Add(item3);

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataBind();

        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();
        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();

        ddlHousestatys.DataSource = ClstblProjects.tbl_HouseStatys_Select();
        ddlHousestatys.DataValueField = "Id";
        ddlHousestatys.DataTextField = "Name";
        ddlHousestatys.DataMember = "Name";
        ddlHousestatys.DataBind();

        //ListItem item3 = new ListItem();
        //item3.Text = "Select";
        //item3.Value = "";
        //ddlProjectStatusID.Items.Clear();
        //ddlProjectStatusID.Items.Add(item3);

        //ddlProjectStatusID.DataSource = ClstblProjectStatus.tblProjectStatus_Select();
        //ddlProjectStatusID.DataValueField = "ProjectStatusID";
        //ddlProjectStatusID.DataTextField = "ProjectStatus";
        //ddlProjectStatusID.DataMember = "ProjectStatus";
        //ddlProjectStatusID.DataBind();

        //ListItem item4 = new ListItem();
        //item4.Text = "Select";
        //item4.Value = "";
        //ddlprojectmgr.Items.Clear();
        //ddlprojectmgr.Items.Add(item4);

        //ddlprojectmgr.DataSource = ClstblEmployees.tblEmployees_Select();
        //ddlprojectmgr.DataValueField = "EmployeeID";
        //ddlprojectmgr.DataTextField = "fullname";
        //ddlprojectmgr.DataMember = "fullname";
        //ddlprojectmgr.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlSalesRep.Items.Clear();
        ddlSalesRep.Items.Add(item5);

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
        }

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            if (SalesTeam != string.Empty)
            {
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        //ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectByUserASC(userid);
        ddlSalesRep.DataValueField = "EmployeeID";
        ddlSalesRep.DataTextField = "fullname";
        ddlSalesRep.DataMember = "fullname";
        ddlSalesRep.DataBind();

        //ListItem item6 = new ListItem();
        //item6.Text = "Select";
        //item6.Value = "";
        //ddlFind.Items.Clear();
        //ddlFind.Items.Add(item6);

        //ddlFind.DataSource = ClstblProjects.tblProjects_Select();
        //ddlFind.DataValueField = "ProjectID";
        //ddlFind.DataTextField = "ProjectFine";
        //ddlFind.DataMember = "ProjectFine";
        //ddlFind.DataBind();

        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        //string com_id = "";
        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        //{
        //    com_id = st2.CustomerID;
        //}

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);

        ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(st2.CustomerID);
        ddlContact.DataValueField = "ContactID";
        ddlContact.DataTextField = "Contact";
        ddlContact.DataMember = "Contact";
        ddlContact.DataBind();

        ListItem item7 = new ListItem();
        item7.Text = "Select";
        item7.Value = "";
        ddlProjectOnHoldID.Items.Clear();
        ddlProjectOnHoldID.Items.Add(item7);

        ddlProjectOnHoldID.DataSource = ClstblProjectOnHold.tblProjectOnHold_SelectASC();
        ddlProjectOnHoldID.DataValueField = "ProjectOnHoldID";
        ddlProjectOnHoldID.DataTextField = "ProjectOnHold";
        ddlProjectOnHoldID.DataMember = "ProjectOnHold";
        ddlProjectOnHoldID.DataBind();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlProjectCancelID.Items.Clear();
        ddlProjectCancelID.Items.Add(item8);

        ddlProjectCancelID.DataSource = ClstblProjectCancel.tblProjectCancel_SelectASC();
        ddlProjectCancelID.DataValueField = "ProjectCancelID";
        ddlProjectCancelID.DataTextField = "ProjectCancel";
        ddlProjectCancelID.DataMember = "ProjectCancel";
        ddlProjectCancelID.DataBind();
    }

    protected void btnUpdateDetail_Click(object sender, EventArgs e)
    {
        BindScript();
        //Response.Write("heloo");
        //Response.End();
        string ProjectID = Request.QueryString["proid"];
        string CustomerID = Request.QueryString["compid"];
        string ContactID = ddlContact.SelectedValue;
        string EmployeeID = ddlSalesRep.SelectedValue;
        //if (ddlProjectTypeID.SelectedItem.Text == "Replacing" || ddlProjectTypeID.SelectedItem.Text == "Solar UG")
        //{
        //   // ValidationGroup = "projectdetail" ControlToValidate = "txtoldsystemdetails"
        //    RequiredFieldValidator4.Enabled = false;
        //    RequiredFieldValidator4.ValidationGroup = "";
        //    RequiredFieldValidator4.ControlToValidate = null;
        //}
        //else
        //{
            
        //    RequiredFieldValidator4.Enabled = true;
        //    RequiredFieldValidator4.ValidationGroup = "projectdetail";
        //    RequiredFieldValidator4.ControlToValidate=txtoldsystemdetails.Text;

        //}

        string SalesRep = ddlSalesRep.SelectedValue;
        string ProjectTypeID = ddlProjectTypeID.SelectedValue;
        //string ProjectStatusID = ddlProjectStatusID.SelectedValue;
        string ProjectOpened = txtProjectOpened.Text;
        string OldProjectNumber = txtOldProjectNumber.Text;
        string ManualQuoteNumber = txtManualQuoteNumber.Text;
        string Project = txtInstallCity.Text + "-" + txtInstallAddress.Text;

        string formbayUnitNo = txtformbayUnitNo.Text;
        string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;

        string InstallCity = txtInstallCity.Text;
        string InstallState = txtInstallState.Text;
        string InstallPostCode = txtInstallPostCode.Text;

        string ProjectNotes = txtProjectNotes.Text;
        string FollowUp = txtFollowupDate.Text;
        string FollowUpNote = "";
        string InstallerNotes = txtInstallerNotes.Text;
        string MeterInstallerNotes = txtMeterInstallerNotes.Text;
        string oldsystemdetail = txtoldsystemdetails.Text;
        //string projectcancel = chkprojectcancel.Checked.ToString();

        txtInstallAddress.Text = formbayunittype + " " + formbayUnitNo + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
        string InstallAddress = txtInstallAddress.Text;
        string project = InstallCity + '-' + InstallAddress;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        SttblProjects stproject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //Response.Write("'" + CustomerID + "','" + formbayunittype + "','" + formbayUnitNo + "','" + formbayStreetNo + "','" + formbaystreetname + "','" + formbaystreettype + "','" + InstallCity + "','" + InstallState + "','" + InstallPostCode + "'");
        //int existaddress = ClstblCustomers.tblCustomers_ExitsByCustID_Address(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
        //if (Roles.IsUserInRole("PreInstaller"))
        //{
        //    existaddress = 1;
        //}
        //if (existaddress > 0)
        //{
        int existcustomer = ClstblCustomers.tblCustomers_ExitsByID_Address(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
        DataTable dtaddress = ClstblCustomers.tblCustomers_tblProjects_Exits_Address_new(CustomerID, InstallAddress, InstallCity, InstallState, InstallPostCode);
        if (dtaddress.Rows.Count > 0)
        {
            PanAddUpdate.Visible = true;
            //lblerror.Visible = true;
            ModalPopupExtenderAddress.Show();
            // DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
            //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
            rptaddress.DataSource = dtaddress;
            rptaddress.DataBind();
        }
        else
        {
            //    lblerror.Visible = false;
            if (ddlSalesRep.SelectedValue.ToString() != stproject.EmployeeID)
            {
                ClstblCustomers.tblCustomers_Update_Assignemployee(CustomerID, ddlSalesRep.SelectedValue.ToString(), stEmp.EmployeeID, DateTime.Now.AddHours(14).ToString(), Convert.ToString(true));
                ClstblProjects.tblProjects_UpdateEmployee(CustomerID, ddlSalesRep.SelectedValue.ToString());
                ClstblContacts.tblContacts_UpdateEmployee(CustomerID, ddlSalesRep.SelectedValue.ToString());
            }
            //Response.Write(InstallCity);
            //Response.End();
            bool sucDetail = ClsProjectSale.tblProjects_UpdateDetails(ProjectID, ContactID, EmployeeID, SalesRep, ProjectTypeID, ProjectOpened, OldProjectNumber, ManualQuoteNumber, ProjectNotes, FollowUp, FollowUpNote, InstallerNotes, MeterInstallerNotes, InstallAddress, InstallCity, InstallState, InstallPostCode, UpdatedBy, Project);
            bool sucDetail2 = ClsProjectSale.OldSystemDetails(ProjectID, oldsystemdetail);
            //Response.Write(sucDetail);
            //Response.End();

            //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
            bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value, project);

            //bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);
            bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);

            bool success_House = ClstblProjects.tblProjects_UpdateHouseStays(ProjectID, ddlHousestatys.SelectedValue, txtStartDate.Text.Trim());

            bool succ = ClstblCustomers.tblCustomer_Update_Address(CustomerID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);

            //bool succ_updateprojectcancel = ClstblProjects.tblProjects_projectcancel_update(ProjectID, projectcancel);

            //if (txtFollowUpNote.Text != string.Empty && txtFollowupDate.Text != string.Empty)
            //{

            //    int exist = ClsProjectSale.tblCustInfo_Exists(CustomerID, ContactID, FollowUp);
            //    if (exist == 0)
            //    {
            //        int successInfo = ClstblCustInfo.tblCustInfo_Insert(CustomerID, txtFollowUpNote.Text, txtFollowupDate.Text, ContactID, UpdatedBy, "1");
            //    }
            //}
            if (txtInstallerNotes.Text != string.Empty)
            {
                int exist2 = ClsProjectSale.tblInstallerNotes_Exists(ProjectID, EmployeeID, txtInstallerNotes.Text);
                if (exist2 == 0)
                {
                    int success3 = ClstblInstallerNotes.tblInstallerNotes_Insert(ProjectID, EmployeeID, txtInstallerNotes.Text);
                    BindProjectDetail();
                }
            }
            if (ddlProjectTypeID.SelectedValue == "2")
            {
                bool suc = ClsProjectSale.tblProjects_UpdateFormsSolar(ProjectID);
            }
            if (ddlProjectTypeID.SelectedValue == "3")
            {
                bool suc = ClsProjectSale.tblProjects_UpdateFormsSolarUG(ProjectID);
            }
            if (ddlInstaller.SelectedValue != string.Empty)
            {
                string installer = ddlInstaller.SelectedValue;
                string Designer = installer;
                string Electrician = installer;
                bool suc = ClsProjectSale.tblProjects_UpdatePreInstaller(ProjectID, installer, Designer, Electrician);

                int suc1 = ClsProjectSale.tblInstallBookings_InsertprojectDetail(stproject.ProjectNumber, ProjectID);
            }

            /* --------------------- SM Ready. --------------------- */
            string SMready = "0";
            if (string.IsNullOrEmpty(stproject.IsSMReady) || stproject.IsSMReady == "False")
            {
                if (chkclickSMready.Checked == true)
                {
                    SMready = "1";
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "24");
                    bool suc1 = ClsProjectSale.tblProjects_UpdateIsSMReady(ProjectID, SMready);
                }
            }

            /* --------------------- Update STC No. --------------------- */

            decimal stcno;
            //if (stproject.InstallPostCode != string.Empty)
            //{
            //    DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stproject.InstallPostCode);
            //    if (dtZoneCode.Rows.Count > 0)
            //    {
            //        string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            //        DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            //        if (dtZoneRt.Rows.Count > 0)
            //        {
            //            string ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();

            //            stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * 15);
            //            int myInt = (int)Math.Round(stcno);
            //            ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));
            //        }
            //    }
            //}

            if (stproject.InstallPostCode != string.Empty)
            {
                DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stproject.InstallPostCode);
                if (dtZoneCode.Rows.Count > 0)
                {
                    string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                    if (dtZoneRt.Rows.Count > 0)
                    {
                        string ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();

                        string installbooking = stproject.InstallBookingDate;
                        if (installbooking == null || installbooking == "")
                        {
                            DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                            String year = currentdate.Year.ToString();
                            DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                            Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                            stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * stcrate);
                            int myInt = (int)Math.Round(stcno);
                            ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));

                        }
                        else
                        {
                            string date = Convert.ToDateTime(installbooking).ToShortDateString();
                            DateTime dt1 = Convert.ToDateTime(date);
                            String year = dt1.Year.ToString();
                            DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                            Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                            stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * stcrate);
                            int myInt = (int)Math.Round(stcno);
                            ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));
                        }

                    }
                }
            }

            /* ---------------------------------------------------------- */


            //--- do not chage this code Start------
            if (sucDetail)
            {
                BindProjects();
                SetAdd1();
                //PanSuccess.Visible = true;
            }
            else
            {
                SetError1();
                //PanError.Visible = true;
            }

        }
        //else
        //{
        //    PanCompanyError.Visible = true;
        //}

    }
    protected void btnCustAddress_Click(object sender, EventArgs e)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        txtInstallAddress.Text = st.StreetAddress;
        txtformbayUnitNo.Text = st.unit_number;
        ddlformbayunittype.SelectedValue = st.unit_type;
        txtformbayStreetNo.Text = st.street_number;
        txtformbaystreetname.Text = st.street_name;
        ddlformbaystreettype.SelectedValue = st.street_type;
        txtInstallCity.Text = st.StreetCity;
        txtInstallState.Text = st.StreetState;
        txtInstallPostCode.Text = st.StreetPostCode;
    }
    protected void btnViewNotes_Click(object sender, EventArgs e)
    {
        DataTable dt = ClstblInstallerNotes.tblInstallerNotes_Select(Request.QueryString["proid"]);
        if (dt.Rows.Count > 0)
        {
            ModalPopupExtender2.Show();
            divInstallgrid.Visible = true;
            GridView1.DataSource = ClstblInstallerNotes.tblInstallerNotes_Select(Request.QueryString["proid"]);
            GridView1.DataBind();
        }
        else
        {
            divInstallgrid.Visible = false;
        }
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        Literal ltProject = (Literal)this.Parent.Parent.FindControl("ltproject");
        string Project = txtInstallCity.Text + "-" + txtInstallAddress.Text;
        ltProject.Text = Project;
        string formbayUnitNo = txtformbayUnitNo.Text;
        string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;

        string InstallCity = txtInstallCity.Text;
        string InstallState = txtInstallState.Text;
        string InstallPostCode = txtInstallPostCode.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();

    }
    protected void ddlFind_SelectedIndexChanged(object sender, EventArgs e)
    {
        //PanAddUpdate.Visible = true;

        //if (ddlFind.SelectedValue != string.Empty)
        //{
        //    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ddlFind.SelectedValue);
        //    SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
        //    lblcompanyname.Text = stCust.Customer;

        //    lblcity.Text = st.InstallCity;
        //    lblstreet.Text = st.InstallAddress;
        //    lbladdress.Text = st.InstallAddress + "," + st.InstallCity + "," + st.InstallState;

        //    txtInstallAddress.Text = st.InstallAddress;
        //    txtInstallCity.Text = st.InstallCity;
        //    txtInstallState.Text = st.InstallState;
        //    txtInstallPostCode.Text = st.InstallPostCode;
        //}
    }
    protected void txtInstallCity_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        string[] cityarr = txtInstallCity.Text.Split('|');

        if (cityarr.Length > 1)
        {
            txtInstallCity.Text = cityarr[0].Trim();
            txtInstallState.Text = cityarr[1].Trim();
            txtInstallPostCode.Text = cityarr[2].Trim();

        }
        else
        {
            txtInstallState.Text = string.Empty;
            txtInstallPostCode.Text = string.Empty;
        }
    }

    public void BindScript()
    {

        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);

    }


    protected void btnInstDetail_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        chkElecDistOK.Checked = Convert.ToBoolean(st2.ElecDistOK);
        txtElecDistApproved.Text = st2.ElecDistApproved;
        ddlInstaller.SelectedValue = st2.Installer;
        ModalPopupExtenderInst.Show();

        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void ibtnUpdateDetail_Click(object sender, EventArgs e)
    {

        if (ddlInstaller.SelectedValue != string.Empty && txtElecDistApproved.Text.Trim() != string.Empty)
        {

            bool succ = ClsProjectSale.tblProjects_UpdateInstDetail(Convert.ToString(Request.QueryString["proid"]), Convert.ToString(chkElecDistOK.Checked), txtElecDistApproved.Text.Trim(), ddlInstaller.SelectedValue);

            //Response.Write(Convert.ToString(succ) + "==" + projectcancel);
            //Response.End();


            if (succ)
            {
                ModalPopupExtenderInst.Hide();
            }
        }
    }

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderAddress.Hide();
    }
    //protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    //{
    //    ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "address();", true);
    //    PanAddUpdate.Visible = true;
    //    streetaddress();
    //}
    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtInstallAddress.Text = address;
        BindScript();
    }
    protected void rptaddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderAddress.Show();
        rptaddress.PageIndex = e.NewPageIndex;

        string CustomerID = Request.QueryString["compid"];
        string formbayUnitNo = txtformbayUnitNo.Text;
        string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;

        txtInstallAddress.Text = formbayunittype + " " + formbayUnitNo + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
        string InstallAddress = txtInstallAddress.Text;

        string InstallCity = txtInstallCity.Text;
        string InstallState = txtInstallState.Text;
        string InstallPostCode = txtInstallPostCode.Text;

        DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
        //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
        rptaddress.DataSource = dt;
        rptaddress.DataBind();
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {

        PanAddUpdate.Visible = true;
        streetaddress();
        BindScript();
        ddlformbayunittype.Focus();

    }
    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        BindScript();
        txtformbayStreetNo.Focus();

    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        BindScript();
        txtformbaystreetname.Focus();


    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);

        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();

        BindScript();

        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);
        ddlformbaystreettype.Focus();
        // Response.End();

    }
    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        txtInstallCity.Focus();
    }
    protected void ddlProjectTypeID_SelectedIndexChanged(object sender, EventArgs e)
    {

        // HideAndShowPanel(ddlProjectTypeID.SelectedValue);
        //if(ddlProjectTypeID.SelectedItem.Text=="Replacing" || ddlProjectTypeID.SelectedItem.Text == "Solar UG")
        //{
        //    oldsystemdetaildiv.Visible = true;            
        //}
        //else
        //{
        //    oldsystemdetaildiv.Visible = false;
        //}

    }
    public void HideAndShowPanel(string flag)
    {
        //7 : Maintainance
        Panel myPanel = (Panel)ddlProjectTypeID.NamingContainer.NamingContainer.FindControl("Panel2");

        AjaxControlToolkit.TabContainer TabContainer1 = myPanel.FindControl("TabContainer1") as AjaxControlToolkit.TabContainer;
        //  AjaxControlToolkit.TabPanel TabSale = TabContainer1.FindControl("TabSale") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabPrice = TabContainer1.FindControl("TabPrice") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabQuote = TabContainer1.FindControl("TabQuote") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabFinance = TabContainer1.FindControl("TabFinance") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabForms = TabContainer1.FindControl("TabForms") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabSTC = TabContainer1.FindControl("TabSTC") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabPreInst = TabContainer1.FindControl("TabPreInst") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabPostInst = TabContainer1.FindControl("TabPostInst") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabElecInv = TabContainer1.FindControl("TabElecInv") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabMtce = TabContainer1.FindControl("TabMtce") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabDocs = TabContainer1.FindControl("TabDocs") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabRefund = TabContainer1.FindControl("TabRefund") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabMaintenance = TabContainer1.FindControl("TabMaintenance") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabPanel1 = TabContainer1.FindControl("TabPanel1") as AjaxControlToolkit.TabPanel;
        AjaxControlToolkit.TabPanel TabVicDoc = TabContainer1.FindControl("TabVicDoc") as AjaxControlToolkit.TabPanel;
        string openMaintainace;

        if (flag == "7")
        {
            openMaintainace = "open";

            //   TabSale.Visible = false;
            TabPrice.Visible = true;
            TabQuote.Visible = true;
            TabFinance.Visible = true;
            TabForms.Visible = true;
            TabSTC.Visible = true;
            TabPreInst.Visible = true;
            TabPostInst.Visible = true;
            TabElecInv.Visible = true;
            TabMtce.Visible = true;
            TabDocs.Visible = true;
            TabRefund.Visible = true;
            TabMaintenance.Visible = true;
            TabPanel1.Visible = false;
            TabVicDoc.Visible = false;
            //TabMaintenance.HeaderText = "Maintenance";

        }

        else
        {
            openMaintainace = "close";

            //   TabSale.Visible = true;
            TabPrice.Visible = true;
            TabQuote.Visible = true;
            TabFinance.Visible = true;
            TabForms.Visible = true;
            TabSTC.Visible = true;
            TabPreInst.Visible = true;
            TabPostInst.Visible = true;
            TabElecInv.Visible = true;
            TabMtce.Visible = true;
            TabDocs.Visible = true;
            TabRefund.Visible = true;
            TabMaintenance.Visible = true;
            TabPanel1.Visible = false;
            TabVicDoc.Visible = false;
            //TabMaintenance.HeaderText = "Sale";
        }
        Session["openMaintainace"] = openMaintainace;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool success = ClsProjectSale.tblProjects_IsClickCustomer(Request.QueryString["proid"], chkclickcustomer.Checked.ToString());
        if (success)
        {
            SetAdd1();
        }
        else
        {
            SetError1();
        }

    }
    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }
    protected void txtProjectNotes_TextChanged(object sender, EventArgs e)
    {
        if (txtProjectNotes.Text != "")
        {
            string original = Convert.ToString(txtProjectNotes.Text);
            int Count = Convert.ToInt32(txtProjectNotes.Text.Length);
            //var valid = new char[] {
            //'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
            //'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
            //'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
            //'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8',
            //'9', '0', '.', '_','$','+','[',']','\n'};
            //        var result = string.Join("",
            //            (from x in original.ToCharArray()
            //             where valid.Contains(x)
            //             select x.ToString())
            //                .ToArray());

            //txtProjectNotes.Text = result. Replace("\\r\\n", "<br/>");
            // }
            //txtProjectNotes.Text = result.ToString();
            if (original.Contains(',') || original.Contains('"') || original.Contains("''") || original.Contains('*'))
            {
                string Data = original;

                Data = original.Replace(',', ' ').Trim();
                Data = Data.Replace('"', ' ').Trim();
                Data = Data.Replace(',', ' ').Trim();
                Data = Data.Replace('*', ' ').Trim();
                Data = Data.Replace("'", " ").Trim();
                txtProjectNotes.Text = Data;
            }
            
            if (Count > 300)
            {

                MsgError("Do not Enter MoreThen 300 Chars..");
                string st = original.Substring(0, 300);
                txtProjectNotes.Text = st;
            }

        }

    }
}