﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

public partial class includes_controls_info : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();

        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
    }
    
    public void BindInfo()
    {
      //  cmpNextDate.ValueToCompare = DateTime.Now.ToShortDateString();
        //if (Convert.ToDateTime(st.date).ToShortDateString() == DateTime.Now.ToShortDateString() && st.logflag == "False")
        //{
        //    if (st.userid == userid_login)
        //    {
        //        route.Visible = true;
        //    }

        //}
        HidePanels();
        //divrightbtn.Visible = true;
        BindGrid(0);
        ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        ddlSelectRecords.DataBind();

        BindDropDown();
    }

    public void HideInfoAction()
    {
        if (Roles.IsUserInRole("Administrator"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Accountant"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Sales Manager"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("STC"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            string CSalesTeamID = stEmpC.SalesTeamID;

            if (Request.QueryString["compid"] != string.Empty)
            {
                SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stCust.EmployeeID);
                string EmpType = stEmp.EmpType;
                string SalesTeamID = stEmp.SalesTeamID;

                if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
                {
                    divrightbtn.Visible = true;
                }
                else
                {
                    divrightbtn.Visible = false;
                }
            }
        }
    }
    public void BindDropDown()
    {
        ddlContact.Items.Clear();
        ListItem item = new ListItem();
        item.Value = "";
        item.Text = "Select";
        ddlContact.Items.Add(item);

        ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(Request.QueryString["compid"]);
        ddlContact.DataMember = "Contact";
        ddlContact.DataTextField = "Contact";
        ddlContact.DataValueField = "ContactID";
        ddlContact.DataBind();

        DataTable dt = ClstblContacts.tblContacts_SelectByCustId(Request.QueryString["compid"]);
        if (dt.Rows.Count > 0)
        {
            ddlContact.SelectedValue = dt.Rows[0]["ContactID"].ToString();
        }
    }

    protected DataTable GetGridData()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(Request.QueryString["compid"]);
        //DataTable dt = ClstblCustInfo.tblCustInfo_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
           // GridView GridView = (GridView)this.Parent.FindControl("GridView1");
            //for (int i = 0; i > dt.Rows.Count;i++ )
            //{
            //    if (Convert.ToDateTime(dt.Rows[0]["date"].ToString()).ToShortDateString() == Convert.ToDateTime(dt.Rows[i]["NextFollowupDate"].ToString()).ToShortDateString())
            //    {

            //        gvbtnUpdate.Visible = true;
            //    }
            //}
           
            PanNoRecord.Visible = false;
        }
    }

    protected void btnAddFollowUp_Click(object sender, EventArgs e)
    {
        string CustomerID = Request.QueryString["compid"];
        string Description = txtDescription.Text;
        string FollowupDate = txtFollowupDate.Text;
        string NextFollowupDate = txtNextFollowupDate.Text;
        string ContactID = ddlContact.SelectedValue.ToString();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;

        //Response.Write(CustomerID + "<br />" + Description + "<br />" + FollowupDate + "<br />" + NextFollowupDate + "<br />" + ContactID + "<br />" + CustInfoEnteredBy);
        //Response.End();
        int success = ClstblCustInfo.tblCustInfo_Insert(CustomerID, Description, NextFollowupDate, ContactID, CustInfoEnteredBy, "1");
        //--- do not chage this code start------
        if (Convert.ToString(success) != "")
        {
            Reset();
            SetUpdate();
        }
        else
        {
            SetError();
        }
        BindGrid(0);

        //--- do not chage this code end------
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string CustomerID = Request.QueryString["compid"];
        string Description = txtDescription.Text;
        string FollowupDate = txtFollowupDate.Text;
        string NextFollowupDate = txtNextFollowupDate.Text;
        string ContactID = ddlContact.SelectedValue;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;
        //Response.Write(Convert.ToDateTime(txtFollowupDate.Text) + "<br>" + Convert.ToDateTime(NextFollowupDate));
        //Response.End();
        bool success = ClstblCustInfo.tblCustInfo_Update(id1, Request.QueryString["compid"], txtDescription.Text, txtFollowupDate.Text, txtNextFollowupDate.Text, ddlContact.SelectedValue, CustInfoEnteredBy);
        //--- do not chage this code Start------
      
        if (success)
        {
            SetUpdate();
        }
        else
        {
            SetError();
        }
        BindGrid(0);

        //--- do not chage this code end------
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
      
        //--- do not chage this code end------
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblCustInfo st = ClstblCustInfo.tblCustInfo_SelectByCustInfoID(id);
        txtDescription.Text = st.Description;

        txtFollowupDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(st.FollowupDate));
        txtNextFollowupDate.Text =string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(st.NextFollowupDate));
        ddlContact.SelectedValue = st.ContactID;

        //--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------

        //BindDropDown();
    }

    protected void ddlcategorysearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        //BindDropDown();
        InitAdd();
        divrightbtn.Visible = false;
        PanSearch.Visible = false;
        PanGrid.Visible = false;

        txtFollowupDate.Text = Convert.ToString(DateTime.Now.AddHours(14).ToShortDateString());
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>doMyAction();</script>");
        //Response.Write(DateTime.Now.ToShortDateString());
        //Response.Write("<br/>"+DateTime.Now);
        //Response.End();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        lnkAdd.Visible = false;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
        PanAddUpdate.Visible = false;
        lnkAdd.Visible = true;
        divrightbtn.Visible = true;
        PanSearch.Visible = true;
        PanGrid.Visible = true;
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        btnAddFollowUp.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAddFollowUp.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        // btnCancel.Visible = true;
        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAddFollowUp.Visible = false;
        btnUpdate.Visible = true;
        //btnCancel.Visible = true;
        btnReset.Visible = false;
        lblAddUpdate.Text = "Update ";

        divrightbtn.Visible = false;
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
        PanAddUpdate.Visible = false;

        PanSearch.Visible = true;
        divrightbtn.Visible = true;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtDescription.Text = string.Empty;
        txtFollowupDate.Text = string.Empty;
        txtNextFollowupDate.Text = string.Empty;
        ddlContact.SelectedValue = "";
        PanSearch.Visible = false;
        //divrightbtn.Visible = true;
        txtFollowupDate.Text = DateTime.Now.ToShortDateString();
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
       
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");
            LinkButton gvbtnUpdate = (LinkButton)e.Row.FindControl("gvbtnUpdate");

            HiddenField FirstRecord = (HiddenField)e.Row.FindControl("FirstRecord");
            HiddenField LastRecord = (HiddenField)e.Row.FindControl("LastRecord");
           
            HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;
            //DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(Request.QueryString["compid"]);
           
            //for (int i = 0; i < dt.Rows.Count;i++ )
            //{
             
              
            //    if (Convert.ToDateTime(dt.Rows[i]["date"].ToString()).ToShortDateString() == Convert.ToDateTime(dt.Rows[i]["NextFollowupDate"].ToString()).ToShortDateString())
            //    {
                   
            //        gvbtnUpdate.Visible = true;
            //    }
            //    else
            //    {
                  
            //        gvbtnUpdate.Visible = false;
            //    }
            //}
          //  Response.End();

            if (hndEmployeeID.Value == EmployeeID)
            {
                //  gvbtnDelete.Visible = true;
                // gvbtnUpdate.Visible = true;
                HideShowButton(gvbtnDelete, gvbtnUpdate, FirstRecord.Value, LastRecord.Value);
            }
            else
            {
                gvbtnDelete.Visible = false;
               // gvbtnUpdate.Visible = false;
                if ((Roles.IsUserInRole("Administrator")))
                {
                    gvbtnDelete.Visible = true;
                    //  gvbtnUpdate.Visible = true;

                    HideShowButton(gvbtnDelete, gvbtnUpdate, FirstRecord.Value, LastRecord.Value);

                }

                if ((Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")))
                {
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }
                    string EmpType = stEmp.EmpType;

                    string CSalesTeamID = "";
                    SttblEmployees stSalesRep = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
                    DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(hndEmployeeID.Value);
                    if (dt_empsale.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale.Rows)
                        {
                            CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                        }
                        CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                    }


                    if (stSalesRep.EmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        gvbtnDelete.Visible = true;
                        // gvbtnUpdate.Visible = true;
                        HideShowButton(gvbtnDelete, gvbtnUpdate, FirstRecord.Value, LastRecord.Value);
                    }
                    else
                    {
                        gvbtnDelete.Visible = false;
                      //  gvbtnUpdate.Visible = false;
                    }
                }
            }

            if ((Roles.IsUserInRole("Administrator")))
            {
                gvbtnDelete.Visible = true;
            }
            else
            {
                gvbtnDelete.Visible = false;
            }
        }
    }

    public void HideShowButton(LinkButton delete, LinkButton Edit, string FirstRecord, string LastRecord)
    {
        if(FirstRecord == "1" && LastRecord == "1")
        {
            delete.Visible = false;
            Edit.Visible = false;
        }
        else if (FirstRecord == "1")
        {
            delete.Visible = true;
            Edit.Visible = true;
        }
        else
        {
            delete.Visible = false;
            Edit.Visible = false;
        }
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblCustInfo.tblCustInfo_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetAdd1();
            //PanSuccess.Visible = true;

        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }

        Reset();
        PanAddUpdate.Visible = false;
        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        PanSearch.Visible = true;
    }
}