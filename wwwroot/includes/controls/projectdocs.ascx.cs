using System;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class includes_controls_projectdocs : System.Web.UI.UserControl
{
    public includes_controls_projectdocs()
        : base()
    {
    }

    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {

                if (st2.ProjectStatusID == "3")
                {
                    btnSave.Visible = false;
                }
            }
            //if (Roles.IsUserInRole("SalesRep"))
            //{
            //    if (st2.DepositReceived != string.Empty)
            //    {
            //        btnSave.Visible = false;
            //    }
            //}

        }
    }

    public void BindDocs()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (st.InvoiceNumber == string.Empty && st.InvoiceDoc == string.Empty)
            {
                divInvoice.Visible = false;
            }
            else
            {
                divInvoice.Visible = true;
            }

            if (st.SQ != string.Empty)
            {
                //HypSQ.Text = st.SQ;
                //HypSQ.NavigateUrl = pdfURL + "SQ/" + st.SQ;
                HypSQ.NavigateUrl = SiteConfiguration.GetDocumnetPath("SQ", st.SQ);
                //HypSQ.NavigateUrl = "~/userfiles/SQ/" + st.SQ;
            }
            if (st.PD != string.Empty)
            {
                //hypProp.Text = st.PD;
                //hypProp.NavigateUrl = pdfURL + "PD/" + st.PD;
                hypProp.NavigateUrl = SiteConfiguration.GetDocumnetPath("PD", st.PD);
            }
            if (st.MP != string.Empty)
            {
                //hypMP.Text = st.MP;
                //hypMP.NavigateUrl = pdfURL + "MP/" + st.MP;
                hypMP.NavigateUrl = SiteConfiguration.GetDocumnetPath("MP", st.MP);
            }
            if (st.EB != string.Empty)
            {
                //hypEB.NavigateUrl = pdfURL + "EB/" + st.EB;
                hypEB.NavigateUrl = SiteConfiguration.GetDocumnetPath("EB", st.EB);
            }
            if (st.PR != string.Empty)
            {
                //hypPR.NavigateUrl = pdfURL + "PR/" + st.PR;
                hypPR.NavigateUrl = SiteConfiguration.GetDocumnetPath("PR", st.PR);
            }
            if (st.beatquotedoc != string.Empty)
            {
                //hypbeatQuote.NavigateUrl = pdfURL + "BeatQuote/" + st.beatquotedoc;
                hypbeatQuote.NavigateUrl = SiteConfiguration.GetDocumnetPath("BeatQuote", st.beatquotedoc);
            }
            if (st.nearmapdoc != string.Empty)
            {
                //hypNearmap.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc;
                hypNearmap.NavigateUrl = SiteConfiguration.GetDocumnetPath("NearMap", st.nearmapdoc);
            }
            if (st.InstallerNotes != string.Empty)
            {
                lblInstallerNotes.Text = st.InstallerNotes;
            }

            DataTable dt = ClstblProjects.tblProjectQuotes_SelectTop1(ProjectID);
            if (dt.Rows.Count > 0)
            {
                divQuote.Visible = true;
                //hypDoc.NavigateUrl = pdfURL + "quotedoc/" + dt.Rows[0]["QuoteDoc"].ToString();
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("quotedoc", dt.Rows[0]["QuoteDoc"].ToString());
                //                hypDoc.NavigateUrl = "~/userfiles/quotedoc/" + dt.Rows[0]["QuoteDoc"].ToString();
            }
            else
            {
                divQuote.Visible = false;
            }
            if (st.ST != string.Empty)
            {
                lblST.Visible = true;
                lblST.Text = st.ST;
                //lblST.NavigateUrl = pdfURL + "ST/" + st.ST;
                lblST.NavigateUrl = SiteConfiguration.GetDocumnetPath("ST", st.ST);
                //lblST.NavigateUrl = "~/userfiles/ST/" + st.ST;
            }
            else
            {
                lblST.Visible = false;
            }
            if (st.CE != string.Empty)
            {
                lblCE.Visible = true;
                lblCE.Text = st.CE;
                //lblCE.NavigateUrl = pdfURL + "CE/" + st.CE;
                lblCE.NavigateUrl = SiteConfiguration.GetDocumnetPath("CE", st.CE);
                //lblCE.NavigateUrl = "~/userfiles/CE/" + st.CE;
            }
            else
            {
                lblCE.Visible = false;
            }
            if (st.InstInvDoc != string.Empty)
            {
                lblInstInvDoc.Visible = true;
                lblInstInvDoc.Text = st.InstInvDoc;
                //lblInstInvDoc.NavigateUrl = pdfURL + "InstInvDoc/" + st.InstInvDoc;
                lblInstInvDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("InstInvDoc", st.InstInvDoc);
                //lblInstInvDoc.NavigateUrl = "~/userfiles/InstInvDoc/" + st.InstInvDoc;
            }
            else
            {
                lblInstInvDoc.Visible = false;
            }

            DataTable dtDocs = ClsProjectSale.tblInstallerExtraDocs_Select(ProjectID);
            if (dt.Rows.Count > 0)
            {
                divOtherDocs.Visible = true;
                rptInstDocs.DataSource = dtDocs;
                rptInstDocs.DataBind();
            }
            else
            {
                divOtherDocs.Visible = false;
            }           
            if (st.InstallState == "")
            {
                divInstallerDocs.Visible = true;
                HyperLink1.Visible = true;
                HyperLink2.Visible = true;
                HyperLink3.Visible = true;
                HyperLink4.Visible = true;
                HyperLink5.Visible = true;

                HyperLink1.NavigateUrl = "~/userfiles/InstallerDocs/AusNet IES Form.pdf";
                HyperLink2.NavigateUrl = "~/userfiles/InstallerDocs/PV Form.pdf";
                HyperLink3.NavigateUrl = "~/userfiles/InstallerDocs/AusNet Alteration or Upgrade of  Inverter Energy System Form.pdf";
                HyperLink4.NavigateUrl = "~/userfiles/InstallerDocs/UE - Upgrade ExistingSolarPV_SCF UE 06012012.pdf";
                HyperLink5.NavigateUrl = "~/userfiles/InstallerDocs/EWR LATEST.pdf";
            }
            else
            {
                divInstallerDocs.Visible = false;
            }
        }
    }

    protected void btnOpenInvoice_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];


        try
        {
            Telerik_reports.generate_Taxinvoice(ProjectID, "Download");
            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/invoiceold.aspx?id=" + ProjectID, txtWriter);
            //String htmlText = txtWriter.ToString();
            //HTMLExportToPDF(htmlText, ProjectID + "Invoice.pdf");
        }
        catch (Exception ex) { }
        //try
        //{
        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/invoiceold.aspx?id=" + ProjectID, txtWriter);
        //    String htmlText = txtWriter.ToString();
        //    HTMLExportToPDF(htmlText, ProjectID + "Invoice.pdf");
        //}
        //catch { }
    }

    public void HTMLExportToPDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    //pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdfheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    string HeaderHtml = txtWriter.ToString();
        //    string FooterHtml = txtWriter2.ToString();

        //    client.setHeaderHtml(HeaderHtml);
        //    client.setFooterHtml(FooterHtml);
        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }
    protected void btnCreateSTCForm_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //try
        //{
        TextWriter txtWriter = new StringWriter() as TextWriter;
        //if (st.ProjectTypeID == "3")
        //{
        //    Telerik_reports.generate_stcug(ProjectID);
        //}
        //else
        //{
        //Telerik_reports.generate_Testing(ProjectID);
        Telerik_reports.generate_STCform(ProjectID);
        //}
        //}
        //catch(Exception ex) { }
        //try
        //{

        //}
        //catch (Exception ex) { }
    }
    public void HTMLExportToPDFSTC(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    //client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/stcheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }
    protected void btnPickList_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        try
        {
            Telerik_reports.generate_PickList(ProjectID);
            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/picklist.aspx?id=" + ProjectID, txtWriter);
            //String htmlText = txtWriter.ToString();
            //HTMLExportToPDF(htmlText, ProjectID + "PickList.pdf");
        }
        catch { }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string ST = "";
        string CE = "";
        string InstInvDoc = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        if (fuST.HasFile)
        {
            SiteConfiguration.DeletePDFFile("ST", st.ST);
            ST = ProjectID + fuST.FileName;
            fuST.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\ST\\") + ST);
            bool sucST = ClsProjectSale.tblProjects_UpdateST(ProjectID, ST);
            SiteConfiguration.UploadPDFFile("ST", ST);
            SiteConfiguration.deleteimage(ST, "ST");
        }
        if (fuCE.HasFile)
        {
            SiteConfiguration.DeletePDFFile("CE", st.CE);
            CE = ProjectID + fuCE.FileName;
            fuCE.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\CE\\") + CE);
            bool sucCE = ClsProjectSale.tblProjects_UpdateCE(ProjectID, CE);
            SiteConfiguration.UploadPDFFile("CE", CE);
            SiteConfiguration.deleteimage(CE, "CE");
        }
        if (fuInstInvDoc.HasFile)
        {
            SiteConfiguration.DeletePDFFile("InstInvDoc", st.InstInvDoc);
            InstInvDoc = ProjectID + fuInstInvDoc.FileName;
            fuInstInvDoc.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\InstInvDoc\\") + InstInvDoc);
            bool sucIID = ClsProjectSale.tblProjects_UpdateInstInvDoc(ProjectID, InstInvDoc);
            SiteConfiguration.UploadPDFFile("InstInvDoc", InstInvDoc);
            SiteConfiguration.deleteimage(InstInvDoc, "InstInvDoc");
        }
        string InstallerID = "";
        if (Roles.IsUserInRole("Installer"))
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(userid);
            InstallerID = stCont.ContactID;
        }
        else
        {
            InstallerID = st.Installer;
        }
        if (FileUpload1.HasFile)
        {

            int successupload = ClsProjectSale.tblInstallerExtraDocs_Insert(ProjectID, InstallerID, "");
            string fileName = Convert.ToString(successupload) + FileUpload1.FileName;
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\instotherdocs\\") + fileName);
            bool successuploaddoc = ClsProjectSale.tblInstallerExtraDocs_UpdateDoc(successupload.ToString(), fileName);

            SiteConfiguration.UploadPDFFile("instotherdocs", fileName);
            SiteConfiguration.deleteimage(fileName, "instotherdocs");
        }

        /* -------------------- Email -------------------- */

        string from = "admin@eurosolar.com.au";
        string mailto = "payments@eurosolar.com.au";
        string body = "";
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);

        if (fuST.HasFile)
        {
            body += "STC Form for Project Number " + st.ProjectNumber + " on " + DateTime.Now.ToShortDateString();
        }
        if (fuCE.HasFile)
        {
            body += "Compliance Certificate for Project Number " + st.ProjectNumber + " on " + DateTime.Now.ToShortDateString();
        }

        if (FileUpload1.HasFile)
        {
            body += "Other Document for Project Number " + st.ProjectNumber + " on " + DateTime.Now.ToShortDateString();
        }
        //stEmp.EmpFirst + " " + stEmp.EmpLast + " has uploaded 

        string installer = st.Installer;
        if (st.Installer != string.Empty)
        {
            SttblContacts stcon = ClstblContacts.tblContacts_SelectByContactID(installer);
            string name = stcon.ContFirst + "  " + stcon.ContLast;

            TextWriter txtWriter = new StringWriter() as TextWriter;
            string subjectC = name + " has uplaoded documents for" + st.ProjectNumber;

            // "Nilesh Dhimar has uplaoded documents for project# "+st.ProjectNumber 
            Server.Execute("~/mailtemplate/installerdocs.aspx?body=" + body, txtWriter);
            try
            {
                Utilities.SendMail(from, mailto, subjectC, txtWriter.ToString(), "nishet@eurosolar.com.au");
            }
            catch { }

            string body1 = "";
            TextWriter txtWriter1 = new StringWriter() as TextWriter;
            string subjectC1 = name + " has uplaoded invoice doc for" + st.ProjectNumber;

            if (fuInstInvDoc.HasFile)
            {
                body1 = "Invoice Document for Project Number " + st.ProjectNumber + " on " + DateTime.Now.ToShortDateString() + "<br />";
            }
            Server.Execute("~/mailtemplate/installerdocs.aspx?body=" + body1, txtWriter1);
            //l1.Text = txtWriter1.ToString();
            try
            {
                Utilities.SendMail(from, mailto, subjectC1, txtWriter1.ToString());
            }
            catch { }
        }
        /* ----------------------------------------------- */
        BindDocs();
    }
    protected void rptInstDocs_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
    {
        string ID = e.CommandArgument.ToString();
        if (e.CommandName == "Delete")
        {
            hdndelete.Value = ID;
            ModalPopupExtenderDelete.Show();

        }
        BindDocs();
    }
    protected void rptInstDocs_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndID = (HiddenField)e.Item.FindControl("hndID");
            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc");

            SttblInstallerExtraDocs st = ClsProjectSale.tblInstallerExtraDocs_SelectByID(hndID.Value);
            // hypDoc.NavigateUrl = "~/userfiles/instotherdocs/" + st.DocName;
            hypDoc.Text = st.DocName;
            //hypDoc.NavigateUrl = pdfURL + "instotherdocs/" + st.DocName;
            hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("instotherdocs", st.DocName);
        }
    }
    protected void imgCustAck_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];

        try
        {
            Telerik_reports.generate_customeracknowledgement(ProjectID);

            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/customeracknowledgement.aspx?id=" + ProjectID, txtWriter);
            //String htmlText = txtWriter.ToString();
            //HTMLExportToPDF(htmlText, ProjectID + "CustomerAcknowledgement.pdf");
        }
        catch { }
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string ID = hdndelete.Value.ToString();
        SttblInstallerExtraDocs stdoc = ClsProjectSale.tblInstallerExtraDocs_SelectByID(ID);
        bool success = SiteConfiguration.DeletePDFFile("instotherdocs", stdoc.DocName);
        if (success)
        {
            bool sucess1 = ClsProjectSale.tblInstallerExtraDocs_Delete(ID);
            if (sucess1)
            {
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }
        BindDocs();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

}