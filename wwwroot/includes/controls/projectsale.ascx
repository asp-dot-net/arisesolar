<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectsale.ascx.cs" Inherits="includes_controls_projectsale" %>


<script>


    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedsale);
    function pageLoadedsale() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
        $('#<%=btnUpdateSale.ClientID %>').click(function () {
            formValidate();
        });
    }
</script>
<script type="text/javascript">
    //Function to disable validator
    function ShowDataNew() {

         var e = document.getElementById('<%= ddlRoofType.ClientID%>');
        var selval = e.options[e.selectedIndex].value;
       

        // var e = document.getElementById(ddlProjectTypeID);
        // alert(selval);
        //var flatpanels = document.getElementById('divFlatPanels');  
        //var PitchedPanels = document.getElementById('divPitchedPanels');  


        // var selval = e.options[e.selectedIndex].value;
        if (selval == "7") {
            //flatpanels.style.display = "block";
            //PitchedPanels.style.display = "block";
            var label = document.getElementById("<%= lblFlatPanels.ClientID %>");
                    label.innerText = "Panels on Tin";
                    var label34 = document.getElementById("<%= lblPitchedPanels.ClientID %>").innerText = "Panels on Tile";
                  //  ValidatorEnable(valName6, true);
                    //ValidatorEnable(valName7, true);
                }
                else if (selval == "9") {
                    //flatpanels.style.display = "block";
                    //divPitchedPanels.style.display = "block";

                    var label = document.getElementById("<%= lblFlatPanels.ClientID %>");
                    label.innerText = "Panels on Flat";
                    var label34 = document.getElementById("<%= lblPitchedPanels.ClientID %>");
            label34.innerText = "Panels on Pitched";
            //ValidatorEnable(valName6, true);
          //  ValidatorEnable(valName7, true);
            // document.getElementById('lblFlatPanels').innerHTML = 'Panels on Pitched';
        }
        else {
             var label = document.getElementById("<%= lblFlatPanels.ClientID %>");
                    label.innerText = "Panels on Tin";
                    var label34 = document.getElementById("<%= lblPitchedPanels.ClientID %>").innerText = "Panels on Tile";
            // flatpanels.style.display = "none";
            //divPitchedPanels.display = "none";
            //ValidatorEnable(valName6, false);
            //ValidatorEnable(valName7, false);
        }
    }
</script>
<script type="text/javascript">
    //Function to disable validator
    function disableValidatorNEW() {
               <%-- var valName6 = document.getElementById("<%=RequiredFieldValidator4.ClientID%>");--%>
        // var e = document.getElementById(ddlProjectTypeID);
        //var flatpanels = document.getElementById('divFlatPanels');  
        //var PitchedPanels = document.getElementById('divPitchedPanels');  

        var e = document.getElementById('<%= ddlRoofType.ClientID%>');
        var selval = e.options[e.selectedIndex].value;
        if (selval == "7") {
            //flatpanels.style.display = "block";
            //PitchedPanels.style.display = "block";
            var label = document.getElementById("<%= lblFlatPanels.ClientID %>");
                    label.innerText = "Panels on Tin";
                    var label34 = document.getElementById("<%= lblPitchedPanels.ClientID %>").innerText = "Panels on Tile";
                   // label34.innerText = "Panels on Tile";
                    //document.getElementById('lblFlatPanels').innerHTML = 'Panels on Tin.'
                  <%--  document.getElementById('<%=lblFlatPanels.ClientID %>').value = 'Panels on Tin'--%>;
                    //document.getElementById('lblFlatPanels').innerHTML = '';
                    //document.getElementById('lblPitchedPanels').innerHTML = 'Panels on Tile';
                }
                else if (selval == "9") {
                    // flatpanels.style.display = "block";
                    //divPitchedPanels.style.display = "block";

                    var label = document.getElementById("<%= lblFlatPanels.ClientID %>");
                    label.innerText = "Panels on Flat";
                    var label34 = document.getElementById("<%= lblPitchedPanels.ClientID %>");
            label34.innerText = "Panels on Pitched";
            // document.getElementById('lblFlatPanels').innerHTML = 'Panels on Pitched';
        }
        else {
            //flatpanels.style.display = "none";
            //divPitchedPanels.display = "none";

        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //HighlightControlToValidate();
        $('#<%=btnUpdateSale.ClientID %>').click(function () {
            formValidate();
        });
    });
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#b94a48");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
<asp:UpdatePanel ID="UpdatePanel" runat="server">
    <ContentTemplate>

        <section class="row m-b-md" id="panprojectsale" runat="server">
            <div class="col-sm-12 minhegihtarea">
                <div class="contactsarea">

                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate projectsale">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="div1" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Select Type</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Panel1">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span>
                                                                <div class="radio radio-info radio-inline">
                                                                    <label for="<%=rblonlypanel.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblonlypanel" GroupName="qq" Checked="" />
                                                                        <span class="text">Only Panel&nbsp; </span>
                                                                    </label>
                                                                </div>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span>
                                                                <div class="radio radio-info radio-inline">
                                                                    <label for="<%=rblonlyinverter.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblonlyinverter" GroupName="qq" Checked="" />
                                                                        <span class="text">Only Inverter&nbsp; </span>
                                                                    </label>
                                                                </div>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span>
                                                                <div class="radio radio-info radio-inline">

                                                                    <label for="<%=rblboth.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblboth" GroupName="qq" Checked="" />
                                                                        <span class="text">Both&nbsp; </span>
                                                                    </label>
                                                                </div>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>

                                <div id="divPanelDetail" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Select Panels</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan1">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Select Panel
                                                                </label>
                                                            </span><span>
                                                                <asp:Label ID="labl" runat="server"></asp:Label>
                                                                <asp:DropDownList ID="ddlPanel" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPanel_SelectedIndexChanged1" AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    No. of Panels
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtNoOfPanels" runat="server" MaxLength="10" CssClass="form-control"
                                                                    AutoPostBack="true" OnTextChanged="txtNoOfPanels_TextChanged"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" Style="color: red;"
                                                                    ControlToValidate="txtNoOfPanels" Display="Dynamic" ErrorMessage="Number Only"
                                                                    ValidationGroup="sale" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    System Capacity
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSystemCapacity" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Brand 
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtPanelBrand" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Watts/Panel 
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtWatts" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Model
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtPanelModel" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Rebate
                                                                </label>
                                                                <br />
                                                            </span><span class="pricespan">
                                                                <asp:TextBox ID="txtRebate" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRebate"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                    ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    STC Mult
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSTCMult" runat="server" Text="1" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Zone Rt
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtZoneRt" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>


                                </div>

                                <div id="divInverterDetail" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Select Inverter</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan2">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen row">
                                                            <div class="col-md-9">
                                                                <span class="name disblock ">
                                                                    <label class="control-label">
                                                                        Select Inverter
                                                                    </label>
                                                                </span><span>
                                                                    <asp:DropDownList ID="ddlInverter" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                        OnSelectedIndexChanged="ddlInverter_SelectedIndexChanged" AutoPostBack="true"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                        Qty1
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtqty" runat="server" Text="0" MaxLength="50" CssClass="form-control alignright" Width="50"></asp:TextBox>
                                                                    <%--<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtqty"
                                                                        ValidationGroup="sale" MinimumValue="1" Type="Integer" Text="Enter Qty greater than 0"
                                                                        Display="Dynamic" />--%>
                                                                    <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" Style="color: red;"
                                                                        ControlToValidate="txtqty" ErrorMessage="Enter Qty &gt; than 0"
                                                                        Operator="LessThanEqual" Type="Integer" ValidationGroup="sale" Display="Dynamic"
                                                                        ValueToCompare="0" />--%>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Brand
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtInverterBrand" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Series
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSeries" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen row">
                                                            <div class="col-md-9">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                        2nd Inverter
                                                                    </label>
                                                                </span><span>
                                                                    <asp:DropDownList ID="ddlInverter2" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                        OnSelectedIndexChanged="ddlInverter2_SelectedIndexChanged" AutoPostBack="true"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </span>

                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                        Qty2
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtqty2" Text="0" runat="server" MaxLength="50" CssClass="form-control alignright" Width="50"></asp:TextBox>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group selpen">
                                                            <div class="row">
                                                                <div class="col-md-3 ">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            STC
                                                                        </label>
                                                                    </span>
                                                                    <span class="spicalspanwith">
                                                                        <div class="first1">
                                                                            <asp:TextBox ID="txtSTCNo" runat="server" Enabled="false" MaxLength="50" CssClass="form-control"
                                                                                Width="50"></asp:TextBox>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="second1">
                                                                        <label class="control-label">
                                                                            Value
                                                                        </label>
                                                                    </div>
                                                                    <div class="thrid1">
                                                                        <asp:TextBox ID="txtSTCValue" runat="server" Enabled="false" MaxLength="50" CssClass="form-control alignright floatleft dolarsingn" Width="80"></asp:TextBox>

                                                                    </div>
                                                                    <div class="four1">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Inverter Output
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtInverterOutput" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                </div>

                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Model
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtInverterModel" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group selpen row">
                                                            <div class="col-md-9">
                                                                <span class="name disblock ">
                                                                    <label class="control-label">
                                                                        Export Control Device
                                                                    </label>
                                                                </span><span>
                                                                    <asp:DropDownList ID="ddlInverter3" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlInverter3_SelectedIndexChanged" AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                        Qty3
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtqty3" runat="server" MaxLength="50" CssClass="form-control alignright" Width="50"></asp:TextBox>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="displaynone768">
                                                            <div class="form-group selpen">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                    </label>
                                                                </span><span></span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <div class="form-group selpen">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                    </label>
                                                                </span><span></span>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="form-group selpen">
                                                            <div style="height: 50px;"></div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>



                                </div>

                                <div id="divSitedetails" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Site Details</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan3">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    House Type
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlHouseType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Roof Type
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlRoofType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control" onchange="ShowDataNew()">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen" id="divFlatPanels">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <asp:Label ID="lblFlatPanels" Text="Panel On Flat" runat="server"></asp:Label>:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtFlatPanels" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtFlatPanels"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                               <%-- <asp:RequiredFieldValidator ID="Req89" runat="server" ControlToValidate="txtFlatPanels"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage=""></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen" id="divPitchedPanels">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <asp:Label ID="lblPitchedPanels" Text="Panel on Pitched" runat="server"></asp:Label>:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtPitchedPanels" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtPitchedPanels"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                              <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator90" runat="server" ControlToValidate="txtPitchedPanels"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage=""></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Roof Angle
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlRoofAngle" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Elec Dist
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecDist" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen" id="divSurveyCerti" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Survey Certificate
                                                                </label>
                                                            </span><span class="radiogruopmain">
                                                                <asp:RadioButtonList ID="rblSurveyCerti" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen" id="divCertiApprove" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Certificate Approve
                                                                </label>
                                                            </span><span class="radiogruopmain">
                                                                <asp:RadioButtonList ID="rblCertiApprove" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Elec Retailer
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecRetailer" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Meter Upgrade
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlmeterupgrade" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control" Width="200">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    NMI Number
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtNMINumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Reg Plan No
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtRegPlanNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Lot Num
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtLotNum" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtLotNum"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." Style="color: red;"
                                                                    ValidationGroup="sale" ControlToValidate="txtLotNum" Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                </div>

                                <div id="divDistAppDetail" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Distributor Application Detail</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan4">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="Label23" runat="server" class="control-label">
                                                Dist Applied</asp:Label>
                                                            </span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtElecDistApplied" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <%--<div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Dist Applied:
                                                            </label>
                                                        </span><span>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtElecDistApplied" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="padding-left: 7px;">
                                                                        <asp:ImageButton ID="Image15" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender15" runat="server" PopupButtonID="Image15"
                                                                            TargetControlID="txtElecDistApplied" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtElecDistApplied"
                                                                            ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>--%>
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Employee:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlEmployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    From Email/Fax:
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtElecDistApplySentFrom" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Approval Ref:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtApprovalRef" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Applied By:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecDistApplyMethod" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Dist Approved By:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecDistAppBy" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="Label2" runat="server" class="  control-label">
                                               Dist Approved Date</asp:Label>
                                                            </span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtElecDistAppDate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <%--       <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" class="col-sm-2  control-label">
                                                <strong></strong></asp:Label>
                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="" runat="server" class="form-control" placeholder="">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>--%>
                                                        <%--<div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Dist Approved Date:
                                                            </label>
                                                        </span><span>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtElecDistAppDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="padding-left: 7px;">
                                                                        <asp:ImageButton ID="ImageButton1" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="ImageButton1"
                                                                            TargetControlID="txtElecDistAppDate" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtElecDistAppDate"
                                                                            ID="RegularExpressionValidator7" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>--%>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>


                                </div>

                                <div class="" id="divSPA" runat="server" visible="false">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">SP Ausnet Payment</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan5">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    SPA Inv No
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSPAInvoiceNumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group  dateimgarea selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Date Paid
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtSPAPaid" runat="server" Width="85" CssClass="form-control"></asp:TextBox>
                                                                <asp:ImageButton ID="Image16" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                <cc1:CalendarExtender ID="CalendarExtender16" runat="server" PopupButtonID="Image16"
                                                                    TargetControlID="txtSPAPaid" Format="dd/MM/yyyy">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtSPAPaid" Style="color: red;"
                                                                    ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter valid date"
                                                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen ">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Paid By
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlSPAPaidBy" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>

                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Amount Paid
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSPAPaidAmount" runat="server" MaxLength="20" CssClass="form-control"
                                                                    Width="200px"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtSPAPaidAmount" Style="color: red;"
                                                                    Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen ">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Pay Method
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlSPAPaidMethod" runat="server" AppendDataBoundItems="true"
                                                                    Width="200px" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>



                                </div>
                                <div id="divMeterDetails" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Meter Details</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan6">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Peak Meter No:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtPeakMeterNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <%--<span class="name paddtop2px floatleft">
                                                            <label class="control-label">
                                                                
                                                            </label>
                                                            </span>--%><span class="checkbox-info checkbox">



                                                                <label for="<%=chkEnoughMeterSpace.ClientID %>">
                                                                    <asp:CheckBox ID="chkEnoughMeterSpace" runat="server" />
                                                                    <span class="text">&nbsp;Enough Meter Space:</span>
                                                                </label>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Off-Peak Meters:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtOffPeakMeters" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen">
                                                            <%--<span class="name paddtop2px floatleft">
                                                            <label class="control-label">
                                                                
                                                            </label>
                                                        </span>--%><span class="checkbox-info checkbox">

                                                            <label for="<%=chkIsSystemOffPeak.ClientID %>">
                                                                <asp:CheckBox ID="chkIsSystemOffPeak" runat="server" />
                                                                <span class="text">&nbsp;Is System Off-Peak:</span>
                                                            </label>
                                                        </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Meter Phase 1-2-3:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtMeterPhase" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="txtMeterPhase" Style="color: red;"
                                                                    ValidationGroup="sale" MinimumValue="1" MaximumValue="3" Type="Integer" Text="Enter 1, 2 or 3."
                                                                    Display="Dynamic" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                </div>

                                <div id="divVicRebate" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Vic Govt Rebate Detail</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Panel2">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Rebate App Ref:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtVicAppRefNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="lblVicdate" runat="server" class="control-label">
                                                Date</asp:Label>
                                                            </span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtVicdate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Vic Rebate:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlvicrebate" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                    <asp:ListItem Value="1">With Rebate Install</asp:ListItem>
                                                                    <asp:ListItem Value="0">Without Rebate Install</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Note:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtVicNote" CssClass="form-control" runat="server" Width="100%" Height="78px" TextMode="MultiLine"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                </div>

                                <div id="divMtce" runat="server" style="display: none">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Maintenance Details</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan7">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group dateimgarea">
                                                            <span class="name">
                                                                <label class="control-label">Call Date </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtOpenDate" runat="server" CssClass="form-control" Width="100px" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="Image3" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image3"
                                                                                TargetControlID="txtOpenDate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtOpenDate" ID="RegularExpressionValidator8" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Call Reason1: </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceReasonID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Call Reason2: </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceReasonSubID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group dateimgarea checkboxmain3">
                                                            <%--<span class="name paddtop9none">
                                                            <label class="control-label"></label>
                                                        </span>--%><span>
                                                            <asp:CheckBox ID="chkWarranty" runat="server" />
                                                            <label for="<%=chkWarranty.ClientID %>">
                                                                <span></span>
                                                            </label>
                                                            &nbsp;Warranty
                                                        </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Call Method </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceCallID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value=""> Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sales Rep</label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    Width="200px" AppendDataBoundItems="true" Enabled="false">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Call Status</label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceStatusID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value=""> Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Work Done</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtWorkDone" runat="server" Width="300px" Height="108px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Customer's Description of Work to be Done</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtCustomerInput" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Description of Fault Identified</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtFaultIdentified" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Description of ActionsThis value is required. to Fix</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtActionRequired" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>


                                </div>

                                <div id="divsupplier" runat="server" style="display: none">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Inverter Replacement Details</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan8">
                                                <div class="col-md-4">
                                                    <div id="divthrough" class="form-group" runat="server">
                                                        <span class="name">
                                                            <label class="control-label">Through Type </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlthroughtype" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlthroughtype_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">Select </asp:ListItem>
                                                                <asp:ListItem Value="1">Supplier Through </asp:ListItem>
                                                                <asp:ListItem Value="2">In House Through </asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                                <div id="divsupplierthrough" runat="server" visible="false">
                                                    <div class="col-md-4" id="divinvioceno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice No. </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtinvoiceno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4" id="oldserialno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Old Serial No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtoldserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divlogdement" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtlogdement" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4" id="divlogdementdate" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement Date </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtlogdementdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="img2" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="img2"
                                                                                TargetControlID="txtlogdementdate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtlogdementdate" ID="RegularExpressionValidator10" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divtrackingno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Tracking no </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txttrackingno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="col-md-4" id="Divnewserialno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">New Serail No  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtnewserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="diverrormessage" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Error Message </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txterrormessage" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divdaulty" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Faulty UNit Pickup Date& Time </label>
                                                            </span><span>

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtfaultydate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imgfaulty" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender4" runat="server" PopupButtonID="imgfaulty"
                                                                                TargetControlID="txtfaultydate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtfaultydate" ID="RegularExpressionValidator11" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divpickthrough" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Pickup Through </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlpickthrough" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="divdeliveryrec" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Delievery Received  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtdeliveryreceived" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="divsentsupplier" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent to Suppllier Date&time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtsentsupplier" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imgsent" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" PopupButtonID="imgsent"
                                                                                TargetControlID="txtsentsupplier" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtsentsupplier" ID="RegularExpressionValidator12" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divsentthrough" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent Through </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlsentthrough" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="divinviocedate" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice Sent Date&Time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtinvoicesent" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imginvoice" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender6" runat="server" PopupButtonID="imginvoice"
                                                                                TargetControlID="txtinvoicesent" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtinvoicesent" ID="RegularExpressionValidator13" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divinhousethrough" runat="server" visible="false">

                                                    <div class="col-md-4" id="holdserialno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Old Serial No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="htxtoldserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="hlogdementno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txthlogdementno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4" id="divhlogdementdate" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement Date </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txthlogdementdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="Imghlogdate" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender7" runat="server" PopupButtonID="Imghlogdate"
                                                                                TargetControlID="txthlogdementdate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txthlogdementdate" ID="RegularExpressionValidator14" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div6" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Installer Pickup </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlinstallerpickup" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="col-md-4" id="Divhnewserailno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">New Serail No  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txthnewserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divherrormesage" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Error Message </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtherrormesage" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div9" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Faulty UNit Pickup Date& Time </label>
                                                            </span><span>

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txth_faultyunit" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="Imgfaultyunit" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender8" runat="server" PopupButtonID="Imgfaultyunit"
                                                                                TargetControlID="txth_faultyunit" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txth_faultyunit" ID="RegularExpressionValidator16" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="div11" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Delievery Received  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtdeliveryrec" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div12" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent to Suppllier Date&time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txt_hsentsupplierdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imgsentsupplier" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender9" runat="server" PopupButtonID="imgsentsupplier"
                                                                                TargetControlID="txt_hsentsupplierdate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txt_hsentsupplierdate" ID="RegularExpressionValidator17" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div13" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent Through </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlhsentthrough" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="col-md-4" id="div14" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice Sent Date&Time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txt_incoicesent_date" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="img_invoice" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender10" runat="server" PopupButtonID="img_invoice"
                                                                                TargetControlID="txt_incoicesent_date" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txt_incoicesent_date" ID="RegularExpressionValidator18" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div2" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice No. </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txt_invoiceno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 textcenterbutton center-text">
                                <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateSale" runat="server" OnClick="btnUpdateSale_Click"
                                    Text="Save" CausesValidation="true" ValidationGroup="sale" />
                            </div>
                            <br />
                        </div>
                    </asp:Panel>



                </div>
            </div>
        </section>

    </ContentTemplate>
    <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnUpdateSale" />--%>
        <%--<asp:PostBackTrigger ControlID="ddlpanel" />--%>
    </Triggers>
</asp:UpdatePanel>





