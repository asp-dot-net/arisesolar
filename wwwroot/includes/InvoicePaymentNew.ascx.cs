﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_InvoicePaymentNew : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
       
        GridView GridView1 = (GridView)this.Parent.FindControl("GridView1");
        if (GridView1 != null)
        {
            btnInvPay.Visible = false;
        }
    }

    public void GetInvPayClickByProject(string proid)
    {
        hndProjectId.Value = proid;
        BindProjectInvoice(proid);
        ModalPopupExtenderInvPay.Show();
    }

    public void BindProjectInvoice(string proid)
    {
        if (proid != string.Empty)
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(proid);
            
            DataTable dtPaymentDetails = ClstblInvoicePayments.tblInvoicePayments_tbl_bank_SelectByProjectID(proid);
            rptPaymentDetails.DataSource = dtPaymentDetails;
            rptPaymentDetails.DataBind();

            if (stPro.InvoiceNumber != string.Empty && stPro.InvoiceDoc != string.Empty)
            {
                btnInvPay.Enabled = true;
            }
            else
            {
                btnInvPay.Enabled = false;
            }
            
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
            ltcustdetail.Text = stCust.Customer + " " + stPro.InstallAddress;// +"<br/>" + stPro.InstallAddress;
            if (stPro.SystemDetails != string.Empty)
            {
                divsysdetail.Visible = true;
                ltsysdetail.Text = stPro.SystemDetails;
            }
            else
            {
                divsysdetail.Visible = false;
            }

            lblProjectNumber.Text = stPro.ProjectNumber;
            txtInvoiceNotes.Text = stPro.InvoiceNotes;
            lblStatus.Text = stPro.InvoiceStatus;

            DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(proid);
            try
            {
                decimal totalcostnew = Convert.ToDecimal(stPro.TotalQuotePrice) + Convert.ToDecimal(stPro.RECRebate);
                txtTotalCostNew.Text = totalcostnew.ToString("F");
            }
            catch { }
            try
            {
                txtLessRebate.Text = Convert.ToDecimal(stPro.RECRebate).ToString("F");
            }
            catch { }
            try
            {
                txtTotalCost.Text = Convert.ToDecimal(stPro.TotalQuotePrice).ToString("F");
            }
            catch { }

            decimal paiddate = 0;
            decimal balown = 0;
            if (dtCount.Rows[0]["BankInvoicePay"].ToString() != "")
            {
                paiddate = Convert.ToDecimal(dtCount.Rows[0]["BankInvoicePay"].ToString());
                
                balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
                try
                {
                    txtPaidDate.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
                }
                catch { }
                try
                {
                    txtBalOwing.Text = Convert.ToDecimal(balown).ToString("F");
                }
                catch { }
            }
            else
            {
                txtPaidDate.Text = "0";
                try
                {
                    txtBalOwing.Text = Convert.ToDecimal(stPro.TotalQuotePrice).ToString("F");
                }
                catch { }
            }
        }
    }
    
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            string ProjectID = Request.QueryString["id"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            Response.Redirect("~/admin/adminfiles/company/projects.aspx?id=" + stPro.CustomerID);
        }
    }

    protected void btnPrintReceipt_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderInvPay.Show();
        string ProjectID = hndProjectId.Value;

        if (ProjectID != string.Empty)
        {
            Telerik_reports.generate_receipt(ProjectID);
        }
    }

    //protected void btnClose_Click(object sender, EventArgs e)
    //{
    //    ModalPopupExtenderInvPay.Hide();
    //}

    //public int GetControlIndex(String controlID)
    //{
    //    Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
    //    Match match = regex.Match(controlID);
    //    return Convert.ToInt32(match.Value);
    //}

    protected void btnInvPay_Click1(object sender, EventArgs e)
    {
        BindProjectInvoice(Request.QueryString["proid"]);

        ModalPopupExtenderInvPay.Show();
    }

    protected void rptPaymentDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName.ToLower() == "delete")
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            hndInvoiceID.Value = arg[0];
            hndInvoiceDeleteType.Value = arg[1];
            ModalPopupExtenderInvPay.Show();
            ModalPopupExtenderDelete.Show();
        }
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hndInvoiceID.Value;
        string deleteFlag = hndInvoiceDeleteType.Value;
        
        bool suc = ClstblInvoicePayments.tblInvoicePayments_tbl_BankInvoicePayments_Delete(id, deleteFlag);
        if(suc)
        {
            BindProjectInvoice(hndProjectId.Value);
        }
        ModalPopupExtenderInvPay.Show();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectId.Value;
        
        if (ProjectID != string.Empty)
        {
            bool suc = ClstblInvoicePayments.tblProjects_UpdateInvoiceNotes(ProjectID, txtInvoiceNotes.Text);
        }

        ModalPopupExtenderInvPay.Hide();
    }
}

