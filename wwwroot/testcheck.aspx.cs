using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _testcheck : System.Web.UI.Page
{
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
        //  int suc = clstblcustomers.tbl_custimage_insert("2", "test");
        //bool suc2 = clstblcustomers.tbl_custimage_updateimage(suc.tostring(), "nirali");

        if (!IsPostBack)
        {

            //Membership.CreateUser("admin", "Camlin357", "admin@urosolar.com.au");
            // Roles.AddUserToRole("Divya", "Sales Manager");
            //Roles.CreateRole("Sales Manager");
            //Roles.CreateRole("SalesRep");
            //Roles.CreateRole("STC");
            //Roles.CreateRole("SubAdministrator");
            //Roles.CreateRole("Verification");
            //Roles.CreateRole("WarehouseManager");
            //Roles.CreateRole("Administrator");
            //Roles.CreateRole("Administrator");
            //Roles.CreateRole("Administrator");
            //Roles.CreateRole("Warehouse");
            //Roles.CreateRole("Stock Verification");
            Roles.CreateRole("Quick Stock");


            //StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            //SiteName = st.sitename;

            //Siteurl = SiteConfiguration.GetURL();
            //SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
            //if (Request.Cookies["myCookie"] != null)
            //{
            //    HttpCookie cookie = Request.Cookies.Get("myCookie");

            //    string username = cookie.Values["Uname"].ToString();
            //    string password = cookie.Values["Pass"].ToString();

            //    TextBox uname = (TextBox)login1.FindControl("UserName");
            //    uname.Attributes.Add("value", username);

            //    TextBox pass = (TextBox)login1.FindControl("Password");
            //    pass.TextMode = TextBoxMode.Password;
            //    pass.Attributes.Add("value", password);
            //    login1.RememberMeSet = true;
            //}
            DonwloadExcel();
        }
        //TextBox txt = (TextBox)login1.FindControl("UserName");
        //txt.Focus();
       
    }
    public void Login1_LoggedIn(object sender, EventArgs e)
    {
        CheckBox rm = (CheckBox)login1.FindControl("RememberMe");
        TextBox UserName = (TextBox)login1.FindControl("UserName");
        if (rm.Checked)
        {
            HttpCookie myCookie = new HttpCookie("myCookie");
            Response.Cookies.Remove("myCookie");
            Response.Cookies.Add(myCookie);

            myCookie.Values.Add("Uname", this.login1.UserName.ToString());
            myCookie.Values.Add("Pass", this.login1.Password.ToString());
            DateTime dtExpiry = DateTime.Now.AddDays(100);

            Response.Cookies["myCookie"].Expires = dtExpiry;
        }
        else
        {
            HttpCookie myCookie = new HttpCookie("myCookie");

            Response.Cookies.Remove("myCookie");
            DateTime dtExpiry = DateTime.Now.AddDays(-10);
            Response.Cookies["myCookie"].Expires = dtExpiry;
        }

        //DataTable dt = ClsAdminUtilities.Aspnet_Users_GetRole(UserName.Text);
        //if (dt.Rows.Count > 0)
        //{
        //    if (dt.Rows[0]["rolename"].ToString() == "Administrator")
        //    {
        //        Response.Redirect("~/admin/adminfiles/dashboard.aspx");
        //    }

        //}

    }

    public void DonwloadExcel()
    {
        string Count = "";
        Count = "select * from tbl_SignatureLog order by id desc";
         DataTable dt = ClstblCustomers.query_execute(Count);
        try
        {
            Export oExport = new Export();
            string FileName = "Sing Excel" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 1,2,3,4,
                                5,6,7,8,
                                9,10
                };
            string[] arrHeader = { "Customer_Name", "Signed_Date", "Latitude_Location", "Longitude_Location",
                                         "IP_Address", "Customer_Email", "Token", "ProjectID",
                                         "Image_Address", "QDocNo"
                };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
}
