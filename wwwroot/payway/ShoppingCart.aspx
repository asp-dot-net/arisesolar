<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShoppingCart.aspx.cs" Inherits="payway_ShoppingCart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Shopping Cart</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <p>Your shopping cart contents:</p>
    <form action="ShoppingCart.aspx" method="POST">
      <table border="0" cellpadding="3" cellspacing="0" width="350">
        <tr align="center">
          <td class="listHeading"><b>Product</b></td>
          <td class="listHeading"><b>Quantity</b></td> 
          <td class="listHeading"><b>Price (AUD$)</b></td> 
        </tr> 
      
        <tr class="row1"> 
          <td>Product 1</td> 
          <td>
            <input type="text" name="Product 1" size="4" value="<%=HttpUtility.HtmlEncode( Convert.ToString( quantities["Product 1"] ) )%>">
          </td> 
          <td align="right"><%=HttpUtility.HtmlEncode( String.Format("{0:c}", prices["Product 1"] ) )%></td> 
        </tr> 
        <tr class="row2"> 
          <td>Product 2</td> 
          <td>
            <input type="text" name="Product 2" size="4" value="<%=HttpUtility.HtmlEncode( Convert.ToString( quantities["Product 2"] ) )%>">
          </td>
          <td align="right"><%=HttpUtility.HtmlEncode( String.Format("{0:c}", prices["Product 2"] ) )%></td>
        </tr> 
        <tr class="row1"> 
          <td>Product 3</td> 
          <td>
            <input type="text" name="Product 3" size="4" value="<%=HttpUtility.HtmlEncode( Convert.ToString( quantities["Product 3"] ) )%>">
          </td> 
          <td align="right"><%=HttpUtility.HtmlEncode( String.Format("{0:c}", prices["Product 3"] ) )%></td> 
        </tr> 
        <tr class="row2"> 
          <td>Total</td> 
          <td>
          </td> 
          <td align="right"><b><%=HttpUtility.HtmlEncode( String.Format("{0:c}", totalAmount ) )%></b></td> 
        </tr> 
        <tr>
          <td>&nbsp;</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><input type="submit" name="Submit" value="Update"></td>
          <td></td>
          <td align="right"><input type="submit" name="Submit" value="Check Out"></td>
        </tr>
      </table>
    </form>
</body>
</html>
