using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Qvalent.PayWay;
using System.Web.Security;

namespace PayWayNet
{
    /// <summary>
    /// Summary description for processCard.
    /// </summary>
    public partial class processCard : System.Web.UI.Page
    {
        protected string cardNumber;
        protected string cardVerificationNumber;
        protected string cardExpiryYear;
        protected string cardExpiryMonth;
        protected string orderAmountCents;


        protected string projectnum;
        protected string summaryCode;
        protected string responseCode;
        protected string description;
        protected string receiptNo;
        protected string settlementDate;
        protected string creditGroup;
        protected string previousTxn;
        protected string cardSchemeName;
        protected string projectstatus;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            PayWayAPI payWayAPI = (PayWayAPI)Application[ "PayWayAPI" ];

            //----------------------------------------------------------------------------
            // SET CONNECTION DEFAULTS
            //----------------------------------------------------------------------------
            string orderECI               = "SSL";
            string orderType              = "capture";

            cardNumber = Request.Form["no_credit_card"];
            cardVerificationNumber = Request.Form["no_cvn"];
            cardExpiryYear = Request.Form["dt_expiry_year"];
            cardExpiryMonth = Request.Form["dt_expiry_month"];

            //Response.Write(cardNumber + "==" + cardVerificationNumber + "==" + cardExpiryYear + "==" + cardExpiryMonth);
            //Response.End();
            string card_currency          = "AUD";
            orderAmountCents =
                Convert.ToString(Convert.ToUInt64(Math.Round(
                    Convert.ToDouble(Request.Form["orderAmount"]) * 100)));

            string customerUsername = "Q24694";
            string customerPassword = "Axarpv5fm";
            string customerMerchant       = "TEST";

            // Note: you must supply a unique order number for each transaction request.
            // We recommend that you store each transaction request in your database and
            // that the order number is the primary key for the transaction record in that
            // database.
            int no=ClstblInvoicePayments.tbl_payway_orderno_Genrate();
            string orderNumber = no.ToString();
            projectnum = Request.Form["prono"];
            
            //----------------------------------------------------------------------------
            //INITIALISE CONNECTION VARIABLES
            //----------------------------------------------------------------------------
            Hashtable requestParameters = new Hashtable();
           
            requestParameters.Add( "customer.username", customerUsername );
            requestParameters.Add( "customer.password", customerPassword );
            requestParameters.Add( "customer.merchant", customerMerchant );
            requestParameters.Add( "order.type", orderType );
            requestParameters.Add( "card.PAN", cardNumber );
            requestParameters.Add( "card.CVN", cardVerificationNumber );
            requestParameters.Add( "card.expiryYear", cardExpiryYear );
            requestParameters.Add( "card.expiryMonth", cardExpiryMonth );
            requestParameters.Add( "order.amount", orderAmountCents );
            requestParameters.Add( "customer.orderNumber", orderNumber );
            requestParameters.Add( "card.currency", card_currency );
            requestParameters.Add( "order.ECI", orderECI );

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            //Response.Write(customerUsername +"=="+ customerPassword);
            //Response.End();
            string requestText = payWayAPI.FormatRequestParameters( requestParameters );
           
            string responseText = payWayAPI.ProcessCreditCard( requestText );
         
            // Break the response string into its component parameters
            IDictionary responseParameters = payWayAPI.ParseResponseParameters( responseText );
            Response.Write(responseText +"</br>");
            Response.End();
            // Get the required parameters from the response
            summaryCode = (string)responseParameters[ "response.summaryCode" ];
            responseCode = (string)responseParameters[ "response.responseCode" ];
            description = (string)responseParameters[ "response.text" ];
            receiptNo = (string)responseParameters[ "response.receiptNo" ];
            settlementDate = (string)responseParameters[ "response.settlementDate" ];
            creditGroup = (string)responseParameters[ "response.creditGroup" ];
            previousTxn = (string)responseParameters[ "response.previousTxn" ];
            cardSchemeName = (string)responseParameters[ "response.cardSchemeName" ];
            if (summaryCode == "1")
            {
                projectstatus = "Declined";
                tranfail.Visible = false;
            }
            if (summaryCode == "2")
            {
                projectstatus = "Erred";
                tranfail.Visible = false;
            }
          if (summaryCode == "3")
          {
              projectstatus = "Rejected";
              transsucc.Visible = true;
           }
           //  if (string.IsNullOrEmpty(settlementDate))
           //  {
           //settlementDate="";

           //  }
           //  if (string.IsNullOrEmpty(previousTxn))
           //      {
           //   previousTxn="";

           //  }
             string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
          
            int succc = ClstblInvoicePayments.tbl_payway_Insert(orderNumber, projectnum, stEmp.EmployeeID, summaryCode, responseCode, description, receiptNo, "", creditGroup, "", cardSchemeName, projectstatus, Request.Form["orderAmount"]);
//            1 � Declined
//2 � Erred
//3 � Rejected
        }

    }
}
