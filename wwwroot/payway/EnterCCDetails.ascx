﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EnterCCDetails.ascx.cs" Inherits="payway_EnterCCDetails" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Insert title here</title>
    <style>
        .cardbox{border: 1px solid #ccc; width: 310px; padding:0px 0px 20px; font-family: arial; border-radius:10px; overflow: hidden;}
        .paytitle{background: #eef2f4; font-size: 20px; padding: 15px 15px; text-align: center; margin-bottom: 15px;}
        .form-group{margin-bottom: 18px;}
        label{display: block; margin-bottom: 5px; font-size: 14px;}
        .form-control{border: 1px solid #ccc; border-radius:5px; padding:8px 1%; width: 98%;}
        .formbox{padding:10px 20px 20px;} 
        .formbox{padding:10px 20px 20px;} 
        .btndefaultbox{background: #54c7c3!important; color: #fff; border:0px; padding: 12px 0px; width: 100%; font-size:14px;}
        .cardbox img{display: block; margin:10px auto;}
        .paymentimg{margin-bottom: 20px!important;}
    
    </style>
</head>
<body>
    <%--<form method="post" action="<% Response.Write(ConfigurationSettings.AppSettings["payWayBaseUrl"] + "MakePayment"); %>">--%>
   <%-- <form method="post" runat="server">
        <input type="hidden" name="biller_code" value="<% Response.Write(ConfigurationSettings.AppSettings["billerCode"]); %>" />
        <input type="hidden" name="token" value="<% Response.Write(Session["token"]); %>" />
        <input type="hidden" name="action" value="MakePayment" />--%>
    <div class="cardbox"> 
       <!-- <img src="../admin/images/img_payway.jpg"/> -->
        
        <div class="paytitle"> Pay Invoice</div>
    <form id="creditCardForm" name="creditCardForm" method="POST" runat="server" action="processCard.aspx">
  <input type="hidden" name="biller_code" value="<% Response.Write(ConfigurationSettings.AppSettings["billerCode"]); %>" />
  <input type="hidden" name="token"  value="<% Response.Write(Session["token"]); %>" />
  <input type="hidden" name="action" value="MakePayment" />
        <div class="formbox">
<div class="form-group">
    <img src="../admin/images/img_payment.jpg" class="paymentimg"/>
        <label>Cardholder Name:</label>
    <input name="nm_card_holder" type="text" class="form-control" />
        </div>
            <div class="form-group">
        <label> Card Number:</label>
    <input name="no_credit_card" class="form-control" type="text"/>
        </div>
     <div class="form-group">
        <label>Expiry Date:</label>
   <input name="dt_expiry_month" class="form-control" type="text" style="width:44%;"/> / 
    <input name="dt_expiry_year" class="form-control"  style="width:44%;"/>
        </div>
             <div class="form-group">
        <label>Card Verification Number:</label>
   <input name="no_cvn" class="form-control"/>  
        </div>
            <div class="form-group">
        <label>OrderAmoun:</label>
  <input name="orderAmount" value="<%=cardamount%>" class="form-control"/>
        </div>
             <div class="form-group">      
             <input name="prono" type="hidden"  value="<%=projectno%>" class="btndefaultbox" />
            </div>
        <%--<input type="submit" value="Make Payment">--%>
        <asp:Button runat="server" ID="btnMakePayment" Text ="Make Payment" class="btndefaultbox" OnClick="btnMakePayment_Click" />
    </form>
        </div>
        </div>
</body>
</html>
