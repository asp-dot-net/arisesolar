﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExportControlSign.aspx.cs" Inherits="ExportControlSign" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title runat="server" id="pagetitle"></title>

    <style>
        .lds-dual-ring {
            background-color: transparent;
            display: inline-block;
            width: 64px;
            height: 64px;
        }

            .lds-dual-ring:after {
                content: " ";
                display: block;
                width: 46px;
                height: 46px;
                margin: 1px;
                border-radius: 50%;
                border: 5px solid black;
                border-color: black transparent black transparent;
                animation: lds-dual-ring 1.2s linear infinite;
            }

        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .overlay {
            background-color: #EFEFEF;
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 1000;
            top: 0px;
            left: 0px;
            opacity: .5; /* in FireFox */
            filter: alpha(opacity=50); /* in IE */
        }
    </style>

</head>
<body>
    <div style="position: fixed; left: 45%;">
        <div id="div6" style="text-align: center; margin-top: 200px; z-index: 1000; display: none;">
            <div class="lds-dual-ring"></div>
        </div>
    </div>

     <form id="form1" runat="server" style="z-index: -1;">

         <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <%--    <script src="<%=Siteurl %>admin/js/jquery-3.2.1.js"></script>--%>
        <%--  <script type="text/javascript" src="<%=Siteurl %>admin/js/jquery-1.8.2.min.js"></script>--%>
        <%--    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>
        <link href="https://portal.arisesolar.com.au/admin/css/Signature.css" rel="stylesheet" />
        <link href="https://portal.arisesolar.com.au/admin/css/jquery.signaturepad.css" rel="stylesheet" />
        <script src="https://portal.arisesolar.com.au/admin/js/jquery.signaturepad.min.js"></script>
        <script src="https://portal.arisesolar.com.au/admin/js/signature_pad.js"></script>
        <script src="https://portal.arisesolar.com.au/admin/js/signature_pad.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script type="text/javascript"> 

            $(document).ready(function () {

                var latitude, longitude;
                $(".overlay").hide();//to hide grey overlay
          
            <%--$.getJSON("http://ip-api.com/json", function (data) {
                //alert(data.query);
                //var IP = data.query;
                //$("#lblIPAdd").html(data.query)
                $("#<%=lblIPAdd.ClientID %>").html(data.query)
            })--%>

                $.get("https://ipinfo.io", function (response) {

                    var location = response.loc;
                    var index = location.indexOf(",");

                    latitude = location.substring(0, index); //lbllatitude
                    longitude = location.substring(index + 1, index + 8);// lbllongitude                
                    $("#<%=lbllatitude.ClientID %>").html(latitude)
                        $("#<%=lbllongitude.ClientID %>").html(longitude)

                        $("#<%=lblIPAdd.ClientID %>").html(response.ip)

                        $("#<%=lblCity.ClientID %>").html(response.city)
                        $("#<%=lblRegion.ClientID %>").html(response.region)
                        $("#<%=lblCountry.ClientID %>").html(response.country)
                        $("#<%=lblPostal.ClientID %>").html(response.postal)

                    }, "jsonp");

                var wrapper = document.getElementById("signature-pad"),
                    clearButton = wrapper.querySelector("[data-action=clear]"),
                    saveButton = wrapper.querySelector("[data-action=save]"),
                    canvas = wrapper.querySelector("canvas"),
                    signaturePad;

                function resizeCanvas() {

                    if (signaturePad.isEmpty()) {//it is used because when pad is empty and rotated it takes empty image
                        var ratio = window.devicePixelRatio || 1;
                        canvas.width = canvas.offsetWidth * ratio;
                        canvas.height = canvas.offsetHeight * ratio;
                        canvas.getContext("2d").scale(ratio, ratio);
                    }
                    else {//it is used because when pad has value and rotated then it must retain value of old orientation(Horizontal/Vertical)
                        var data2 = signaturePad.toDataURL();
                        var ratio = Math.max(window.devicePixelRatio || 1, 1);
                        canvas.width = canvas.offsetWidth * ratio;
                        canvas.height = canvas.offsetHeight * ratio;
                        canvas.getContext("2d").scale(ratio, ratio);
                        signaturePad.fromDataURL(data2);
                    }
                }


                window.onresize = resizeCanvas;
                // resizeCanvas();

                signaturePad = new SignaturePad(canvas);

                clearButton.addEventListener("click", function (event) {
                    signaturePad.clear();
                });

                saveButton.addEventListener("click", function (event) {

                    if ($("#chkYes").is(":not(:checked)") && $("#chkNo").is(":not(:checked)")) {
                        alert("Please select yes or no.");
                    }
                    else if (signaturePad.isEmpty()) {
                        alert("Please provide signature first.");
                    }
                    else {
                        if (signaturePad.isEmpty()) {
                            alert("Please provide signature first.");
                        }
                        //alert(signaturePad.toDataURL());
                        FadeBackground();
                        $("#div6").show();
                        SaveImage(signaturePad.toDataURL());
                    }
                });

            });

            //////////////////////////////////do not delete////////////////////////////////////////////////////
            //it is need because once we click try again button or save button due to update panel page though may not
            // appear refreshing but it gets refreshed without pageload due to which document.ready function won't be
            //called again .Therefore ,this code is written which does the same work upon document.ready function when
            //page is refreshed.

            //--------5star=Using jQuery $(document).ready with ASP.Net AJAX UpdatePanel---------------
            //code reference:https://www.aspsnippets.com/Articles/Using-jQuery-documentready-with-ASPNet-AJAX-UpdatePanel.aspx

            //Another Posibility could be UseSubmitBehavior="false" for asp:Button but we are using simple Button


            //var prm = Sys.WebForms.PageRequestManager.getInstance();
            //if (prm != null) {
            //    prm.add_endRequest(function (sender, e) {
            //        if (sender._postBackSettings.panelsToUpdate != null) {


            //        }
            //    });
            //}
            ////////////////////////////////////////////////////////////////////////////////////////////////


            function SaveImage(dataURL) {
                //alert("rrrrrrrrrrrrrrrrrrrrr");

                var projectid = <%=Request.QueryString["proid"]%>;
                var token = "<%=Request.QueryString["rt"]%>";

                var PhysicalRootPath = ('<%=HttpUtility.JavaScriptStringEncode(Server.MapPath(Request.Url.LocalPath))%>');
                var index = PhysicalRootPath.indexOf('ExportControlSign');
                PhysicalRootPath = PhysicalRootPath.substring(0, index);

                var latitude = $("#<%=lbllatitude.ClientID %>").text();
                var longitude = $("#<%=lbllongitude.ClientID %>").text();

                if (latitude == "" && longitude == "") {
                    latitude = "";
                    longitude = "";
                }
                //alert(latitude);
                var IP = $("#<%=lblIPAdd.ClientID %>").text();

                var Country = $("#<%=lblCountry.ClientID %>").text();
                var City = $("#<%=lblCity.ClientID %>").text();
                var Postal = $("#<%=lblPostal.ClientID %>").text();
                var Region = $("#<%=lblRegion.ClientID %>").text();
                var Yes = $("#<%=chkYes.ClientID %>").is(':checked');
                var No = $("#<%=chkNo.ClientID %>").is(':checked');

                var data = JSON.stringify(
                    {
                        value: dataURL,
                        ProjectID: projectid,
                        Token: token,
                        PhysicalRootLocation: PhysicalRootPath,
                        Latitude: latitude,
                        Longitude: longitude,
                        IPAddress: IP,
                        Country: Country,
                        Region: Region,
                        City: City,
                        Postal: Postal,
                        Yes: Yes,
                        No: No
                    });
                //alert("wwrwrf");
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ExportControlSign.aspx/InsertData",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "success") {
                            $("#maindiv").hide();
                            $(".overlay").hide();
                            $("#div6").hide();
                            $("#div1").show();
                        }
                        else {
                            $("#errormsg").html(data.d)
                            $("#maindiv").hide();
                            $("#div3").show();
                        }
                    },
                    error: function (result) {
                        //alert("Error");
                        $("#errormsg").html(result.d)
                        $("#maindiv").hide();
                        $("#div3").show();
                    }
                });
            }

            function FadeBackground() {
                var div = document.createElement("div");
                div.className += "overlay";
                document.body.appendChild(div);
            }
        </script>
    <div id="maindiv">
        <asp:Panel runat="server" ID="panelNotComplete">
            <table class="Signaturehide" align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border: #ddd solid 1px; font-size: 14px; font-family: arial; color: #494949;" max-width="100%">
                <!-- 1 Column Text + Button : BEGIN -->
                <tbody>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="padding: 0px 30px; max-width: 730px">
                                <tbody>
                                    <tr>
                                        <td colspan="2" style="border-bottom: 1px solid #ccc; font-size: 25px; padding-bottom: 10px; padding-top: 10px; text-align: center">
                                            <b>Export Control Acknowledgement</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <p style="line-height: 24px; font-size: 14px; font-family: arial; font-weight: 400; margin: 0px; padding: 20px 0px 0px 0px;">
                                                Dear
                                                <asp:Label runat="server" ID="lblCustName"></asp:Label>,
                                            </p>
                                            <p style="line-height: 24px; font-size: 14px; font-family: arial; font-weight: 400; margin: 0px; padding: 10px 0px 0px 0px;">
                                                Your Solar Advisor
                                                <asp:Label runat="server" ID="lblInstallerName"></asp:Label>
                                                from
                                                    <asp:Label runat="server" ID="lblRetailer"></asp:Label>
                                                is requesting a signature on Export Control Acknowledgement Form
                                            </p>

                                            <p style="line-height: 24px; font-size: 14px; font-family: arial; font-weight: 400; margin: 8px 0px;">
                                                <b>Customer’s Details</b>
                                            </p>

                                            <p style="line-height: 24px; font-size: 14px; font-family: arial; font-weight: 400; margin: 8px 0px;">
                                                Name :
                                                    <asp:Label runat="server" ID="lblName"></asp:Label>
                                            </p>
                                            <p style="line-height: 24px; font-size: 14px; font-family: arial; font-weight: 400; margin: 8px 0px;">
                                                Address :
                                                    <asp:Label runat="server" ID="lblCustAdd"></asp:Label>
                                            </p>
                                            <p style="line-height: 24px; font-size: 14px; font-family: arial; font-weight: 400; margin: 8px 0px;">
                                                Mobile Number :
                                                    <asp:Label runat="server" ID="lblMobileNo"></asp:Label>
                                            </p>
                                            <p style="line-height: 24px; font-size: 14px; font-family: arial; font-weight: 400; margin: 8px 0px;">
                                                Email ID :
                                                    <asp:Label runat="server" ID="lblEmail"></asp:Label>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="color: #333; line-height: 24px; font-size: 18px; font-family: arial; margin: 0px; padding: 10px 0px 10px 0px;"><b>Mandatory Declaration: </b></td>
                                    </tr>
                                    <%--<tr>
                                            <td colspan="2" style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">* Provided layout of the solar panels for your property is just a reference only. Final Decision will be made on the day of installation by CEC Certified Installer.</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">* Free panels will be subject to available roof space and will be taken back by the installer if not installed. The price will remain unchanged. For any panel installed less than that, the price will be reduced by $75 per panel.</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">* Upgrade of Meter Box (if needed) will be negotiated and charged by our Electrician directly. The cost of the replacement of your current electric meter has to be borne by you, which can be legal charges imposed by the Electricity Distributor. Customer agrees to pay the balance amount of the system plus any additional costs towards the system installation. Arise Solar must have the final payment on the day of the installation by EFT,Credit Card or Cheque. </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">* In case of Certegy EziPay or any other finance option offered by Arise Solar, cooling off period of 10 days will be applicable and the installation date will be scheduled accordingly. </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">* It is mandatory to inform Arise Solar in case you change your electricity retailer anywhere between the date of initial deposit and the installation. Failure to inform us may result in delay in grid connection and Arise Solar will not be held liable for any such consequences. </td>
                                        </tr>--%>
                                    <tr>
                                        <td align="left" style="padding-bottom: 12px;">
                                            <%--<input type="checkbox" id="chkYes" />--%>
                                            <asp:CheckBox ID="chkYes" runat="server" />
                                            Yes, I accept the connection approval with imposed export control limit of <asp:Label runat="server" ID="lblExportControlLimitKw"></asp:Label>kw. I understand that this may impact ROI/Expected Payback period.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="padding-bottom: 12px;">
                                            <%--<input type="checkbox" id="chkNo" />--%>
                                            <asp:CheckBox ID="chkNo" runat="server" />
                                            No, I don’t agree with imposed export control limit. I wish to cancel the project and get refund of my deposit (if paid). Please contact your sales rep for refund process.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #ccc; margin: 0 20px 0 30px; padding-bottom: 20px; max-wdith: 100%">
                                <tbody>
                                    <tr align="left">
                                        <td style="background: #ccc; padding: 10px 5px; font-size: 14px;">SIGN HERE: </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; padding: 0px 10px;" colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <div id="myDIV">
                                                <div class="panel panel-default">
                                                    <div class="panel-body" id="signature-pad">
                                                        <div style="text-align: center">
                                                            <canvas id="canvas1" style="width: 95%; border: 1px solid black;"></canvas>
                                                        </div>
                                                        <div style="text-align: right; padding-right: 10px;">
                                                            <br />
                                                            <%--<asp:HiddenField ID="SignatureImageData" runat="server" />--%>


                                                            <asp:Label ID="lblToken" runat="server" Visible="false" />
                                                            <%--<label id="SignatureImageData" >ttttttt</label>--%>
                                                            <button id="btnTryAgain" type="button" data-action="clear" class="" style="padding: 5px; font-size: 20px; border-radius: 10px; border-color: #86A9AB; background-color: #86A9AB; margin-right: 10px;">Try Again</button>
                                                            <%--<asp:Button runat="server" id="btnTryAgain" style="padding: 5px; font-size: 20px; border-radius: 10px; border-color: #86A9AB; background-color: #86A9AB; margin-right: 10px;" Text="Try Again"  />--%>
                                                            <button data-action="save" type="button" id="button" class="" style="padding: 5px; font-size: 20px; border-radius: 10px; border-color: #86A9AB; background-color: #86A9AB; margin-top: 10px; margin-bottom: 10px;">Done</button>
                                                            <%--<asp:Button data-action="save" runat="server" ID="btnSave" Style="padding: 5px; font-size: 20px; border-radius: 10px; border-color: #86A9AB; background-color: #86A9AB; margin-right: 10px;" Text="Done" />--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="padding: 10px 20px; max-width: 730px;">
                                    <tbody>
                                        <tr>
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Customer name:
                                                <asp:Label runat="server" ID="lblCustName2"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Date:
                                                <asp:Label runat="server" ID="lblTodayDate"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Email:
                                                <asp:Label runat="server" ID="lblCustEmailID"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Captured IP Address:
                                                <asp:Label runat="server" ID="lblIPAdd"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Location:
                                                <asp:Label runat="server" ID="lblDeviceLocation"></asp:Label>
                                                Latitude:<asp:Label runat="server" ID="lbllatitude"></asp:Label><br />
                                                <div style="padding-left: 60px;">Longitide:<asp:Label runat="server" ID="lbllongitude" /></label></div>
                                            </td>
                                        </tr>
										<tr style="display:none">
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">City:
                                                <asp:Label runat="server" ID="lblCity"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Region:
                                                <asp:Label runat="server" ID="lblRegion"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Country:
                                                <asp:Label runat="server" ID="lblCountry"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Postal:
                                                <asp:Label runat="server" ID="lblPostal"></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </div>

    <div id="div1" style="display: none; margin-top: 200px;">
        <div style="font-size: 20px; text-align: center">
            Hello
            <asp:Label runat="server" ID="lblCustName3"></asp:Label>,
        </div>
        <div style="font-size: 20px; text-align: center">Signature saved successfully.</div>
    </div>

    <asp:Panel runat="server" ID="panelExist" Visible="false" Style="margin-top: 200px;">
        <div runat="server" id="div2">
            <div style="font-size: 20px; text-align: center">
                Hello
                <asp:Label runat="server" ID="lblCustName4"></asp:Label>,
            </div>
            <div style="font-size: 20px; text-align: center">
                You have already signed the Quotation .<br />
                Please Ignore this email/SMS
            </div>
        </div>
    </asp:Panel>

    <div id="div3" style="display: none; margin-top: 200px;">
        <div style="font-size: 20px; text-align: center">
            <label id="errormsg" />
        </div>
    </div>

    <asp:Panel runat="server" ID="PanelPageNotExist" Visible="false" Style="margin-top: 200px;">
        <div runat="server" id="div4">
            <div style="font-size: 20px; text-align: center">Page does not exist.</div>
        </div>
    </asp:Panel>

    </form>
</body>
</html>
