using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblElecDistributor
	{
		public string ElecDistributor;
		public string Active;
		public string Seq;
		public string NSW;
		public string SA;
		public string QLD;
		public string VIC;
		public string WA;
		public string ACT;
		public string TAS;
		public string NT;
		public string ElecDistABB;
		public string ElecDistAppReq;
    public string GreenBoatDistributor;

	}


public class ClstblElecDistributor
{
	public static SttblElecDistributor tblElecDistributor_SelectByElecDistributorID (String ElecDistributorID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblElecDistributor_SelectByElecDistributorID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ElecDistributorID";
		if (ElecDistributorID != string.Empty)
			param.Value = ElecDistributorID;
		else
			param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblElecDistributor details = new SttblElecDistributor();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.ElecDistributor = dr["ElecDistributor"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();
			details.NSW = dr["NSW"].ToString();
			details.SA = dr["SA"].ToString();
			details.QLD = dr["QLD"].ToString();
			details.VIC = dr["VIC"].ToString();
			details.WA = dr["WA"].ToString();
			details.ACT = dr["ACT"].ToString();
			details.TAS = dr["TAS"].ToString();
			details.NT = dr["NT"].ToString();
			details.ElecDistABB = dr["ElecDistABB"].ToString();
			details.ElecDistAppReq = dr["ElecDistAppReq"].ToString();
            details.GreenBoatDistributor = dr["GreenBoatDistributor"].ToString();
		}
		// return structure details
		return details;
	}
	public static DataTable tblElecDistributor_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblElecDistributor_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblElecDistributor_SelectQLD()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistributor_SelectQLD";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblElecDistributor_SelectEnergex()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistributor_SelectEnergex";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblElecDistributor_SelectErgon()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistributor_SelectErgon";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

	public static int tblElecDistributor_Insert ( String ElecDistributor, String Active, String Seq, String NSW, String SA, String QLD, String VIC, String WA, String ACT, String TAS, String NT, String ElecDistABB, String ElecDistAppReq,string GreenBoatDistributor)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblElecDistributor_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ElecDistributor";
		param.Value = ElecDistributor;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@NSW";
		param.Value = NSW;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SA";
		param.Value = SA;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@QLD";
		param.Value = QLD;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@VIC";
		param.Value = VIC;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@WA";
		param.Value = WA;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ACT";
		param.Value = ACT;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@TAS";
		param.Value = TAS;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@NT";
		param.Value = NT;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ElecDistABB";
		param.Value = ElecDistABB;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ElecDistAppReq";
		param.Value = ElecDistAppReq;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GreenBoatDistributor";
        param.Value = GreenBoatDistributor;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblElecDistributor_Update (string ElecDistributorID, String ElecDistributor, String Active, String Seq, String NSW, String SA, String QLD, String VIC, String WA, String ACT, String TAS, String NT, String ElecDistABB, String ElecDistAppReq,string GreenBoatDistributor)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblElecDistributor_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ElecDistributorID";
		param.Value = ElecDistributorID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ElecDistributor";
		param.Value = ElecDistributor;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@NSW";
		param.Value = NSW;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SA";
		param.Value = SA;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@QLD";
		param.Value = QLD;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@VIC";
		param.Value = VIC;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@WA";
		param.Value = WA;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ACT";
		param.Value = ACT;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@TAS";
		param.Value = TAS;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@NT";
		param.Value = NT;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ElecDistABB";
		param.Value = ElecDistABB;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ElecDistAppReq";
		param.Value = ElecDistAppReq;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GreenBoatDistributor";
        param.Value = GreenBoatDistributor;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblElecDistributor_Delete (string ElecDistributorID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblElecDistributor_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ElecDistributorID";
		param.Value = ElecDistributorID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}

    public static int tblElecDistributor_Exists(string ElecDistributor)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistributor_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributor";
        param.Value = ElecDistributor;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblElecDistributor_ExistsById(string ElecDistributor, string ElecDistributorID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistributor_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributor";
        param.Value = ElecDistributor;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributorID";
        param.Value = ElecDistributorID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblElecDistributor_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistributor_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblElecDistributor_GetDataBySearch(string alpha,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistributor_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
}