using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblProjectCancel
	{
		public string ProjectCancel;
		public string Active;
		public string Seq;

	}


public class ClstblProjectCancel
{
	public static SttblProjectCancel tblProjectCancel_SelectByProjectCancelID (String ProjectCancelID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectCancel_SelectByProjectCancelID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectCancelID";
		param.Value = ProjectCancelID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblProjectCancel details = new SttblProjectCancel();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.ProjectCancel = dr["ProjectCancel"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblProjectCancel_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectCancel_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblProjectCancel_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectCancel_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
	public static int tblProjectCancel_Insert ( String ProjectCancel, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectCancel_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectCancel";
		param.Value = ProjectCancel;
		param.DbType = DbType.String;
		param.Size = 30;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblProjectCancel_Update (string ProjectCancelID, String ProjectCancel, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectCancel_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectCancelID";
		param.Value = ProjectCancelID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectCancel";
		param.Value = ProjectCancel;
		param.DbType = DbType.String;
		param.Size = 30;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblProjectCancel_Delete (string ProjectCancelID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectCancel_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectCancelID";
		param.Value = ProjectCancelID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}

    public static int tblProjectCancel_Exists(string ProjectCancel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectCancel_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancel";
        param.Value = ProjectCancel;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblProjectCancel_ExistsById(string ProjectCancel, string ProjectCancelID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectCancel_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancel";
        param.Value = ProjectCancel;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        param.Value = ProjectCancelID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblProjectCancel_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectCancel_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblProjectCancel_GetDataBySearch(string alpha ,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectCancel_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
}