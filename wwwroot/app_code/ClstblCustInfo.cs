using System;
using System.Data;
using System.Data.Common;

public struct SttblCustInfo
{
    public string CustomerID;
    public string Description;
    public string FollowupDate;
    public string NextFollowupDate;
    public string ContactID;
    public string CustInfoEnteredBy;
}

public class ClstblCustInfo
{
    public static SttblCustInfo tblCustInfo_SelectByCustInfoID(String CustInfoID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_SelectByCustInfoID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoID";
        param.Value = CustInfoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustInfo details = new SttblCustInfo();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.Description = dr["Description"].ToString();
            details.FollowupDate = dr["FollowupDate"].ToString();
            details.NextFollowupDate = dr["NextFollowupDate"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.NextFollowupDate = dr["NextFollowupDate"].ToString();
            details.CustInfoEnteredBy = dr["CustInfoEnteredBy"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblCustInfo_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustInfo_SelectByDate(string startdate, string enddate, string CustInfoEnteredBy, string CustSourceID, string CustTypeID, string EmployeeID, string SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_SelectByDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustInfoEnteredBy";
        if (CustInfoEnteredBy != string.Empty)
            param.Value = CustInfoEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustInfo_SelectNext10(string CustInfoEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_SelectNext10";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoEnteredBy";
        if (CustInfoEnteredBy != string.Empty)
            param.Value = CustInfoEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustInfo_SelectLast10(string CustInfoEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_SelectLast10";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoEnteredBy";
        if (CustInfoEnteredBy != string.Empty)
            param.Value = CustInfoEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustInfo_SelectByUserIdCust(String userid, String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_SelectByUserIdCust";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustInfo_SelectByCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_SelectByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblCustInfo_Insert(String CustomerID, String Description, String NextFollowupDate, String ContactID, String CustInfoEnteredBy, String FollowupType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        param.Value = Description;
        param.DbType = DbType.String;
        param.Size = 2147483647;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NextFollowupDate";
        if (NextFollowupDate != string.Empty)
            param.Value = NextFollowupDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustInfoEnteredBy";
        if (CustInfoEnteredBy != string.Empty)
            param.Value = CustInfoEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowupType";
        if (FollowupType != string.Empty)
            param.Value = FollowupType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblCustInfo_Update(string CustInfoID, String CustomerID, String Description, String FollowupDate, String NextFollowupDate, String ContactID, String CustInfoEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoID";
        param.Value = CustInfoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        param.Value = Description;
        param.DbType = DbType.String;
        param.Size = 2147483647;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowupDate";
        if (FollowupDate != string.Empty)
            param.Value = FollowupDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NextFollowupDate";
        if (NextFollowupDate != string.Empty)
            param.Value = NextFollowupDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustInfoEnteredBy";
        param.Value = CustInfoEnteredBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
      
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblCustInfo_InsertUpdate(Int32 CustInfoID, String CustomerID, String Description, String FollowupDate, String NextFollowupDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoID";
        param.Value = CustInfoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        param.Value = Description;
        param.DbType = DbType.String;
        param.Size = 2147483647;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowupDate";
        param.Value = FollowupDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NextFollowupDate";
        param.Value = NextFollowupDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustInfo_Delete(string CustInfoID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoID";
        param.Value = CustInfoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblCustInfo_SelectTop1ByCustID(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_SelectTop1ByCustID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != null && CustomerID!="")
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustInfo_Close(string CustInfoID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_Close";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoID";
        param.Value = CustInfoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblCustInfo_TodayFollowup(string CustInfoEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_TodayFollowup";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoEnteredBy";
        if (CustInfoEnteredBy != string.Empty)
            param.Value = CustInfoEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
   
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustInfo_UpdateReject(string CustInfoID,  String Reject)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_UpdateReject";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoID";
        param.Value = CustInfoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reject";
        if (Reject != string.Empty)
            param.Value = Reject;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
}