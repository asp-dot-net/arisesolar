﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

/// <summary>
/// Summary description for Job_Response
/// </summary>
public class Job_Response
{
    public Job_Response()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class RootObject
    {
        public List<lstJobData> lstJobData { get; set; }

        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class lstJobData
    {
        public BasicDetails BasicDetails { get; set; }
        public JobSystemDetails JobSystemDetails { get; set; }
        public JobSTCDetails JobSTCDetails { get; set; }
        public JobSTCStatusData JobSTCStatusData { get; set; }
    }

    public class BasicDetails
    {
        public string VendorJobId { get; set; }
        public string JobID { get; set; }
        public string Title { get; set; }
        public string RefNumber { get; set; }
        public string Description { get; set; }
        public string JobType { get; set; }
        public string JobStage { get; set; }
        public string Priority { get; set; }
        public string InstallationDate { get; set; }
        public string CreatedDate { get; set; }
    }

    public class JobSystemDetails
    {
        public string SystemSize { get; set; }
        public string SerialNumbers { get; set; }
        public string CalculatedSTC { get; set; }
        public string NoOfPanel { get; set; }
    }

    public class JobSTCDetails
    {
        public string FailedAccreditationCode { get; set; }
    }

    public class JobSTCStatusData
    {
        public string STCStatus { get; set; }
        public string CalculatedSTC { get; set; }
        public string STCSubmissionDate { get; set; }
    }


    #region
    public partial class RootObjectBS
    {
        [JsonProperty("Success")]
        public Success Success { get; set; }
    }

    public partial class Success
    {
        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Details")]
        public Details Details { get; set; }
    }

    public partial class Details
    {
        [JsonProperty("panels")]
        public List<string> Panels { get; set; }

        [JsonProperty("inverters")]
        public List<string> Inverters { get; set; }
    }

    public partial class NoOfPanelInverter
    {
        public string NoOfPanel;
        public string NoOfInverter;
    }

    public partial class RootObjectBSStatus
    {
        [JsonProperty("Success")]
        public SuccessStatus Success { get; set; }
    }

    public partial class SuccessStatus
    {
        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Details")]
        public DetailsStatus Details { get; set; }
    }

    public partial class DetailsStatus
    {
        [JsonProperty("stc")]
        public long Stc { get; set; }

        [JsonProperty("pvd")]
        public string Pvd { get; set; }

        [JsonProperty("pvdStatus")]
        public string pvdStatus { get; set; }

        [JsonProperty("processedDate")]
        public string processedDate { get; set; }
    }

    #endregion
}