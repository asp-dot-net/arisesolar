﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClstblSalesinfo
/// </summary>
/// 

public class ClstblSalesinfo
{
    public struct StStockItemData
    {
        public string StockManufacturer;
        public string StockModel;
        public string StockSeries;
        public string StockSize;
    }

    public ClstblSalesinfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable tbl_Variation_GetData(string var)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Variation_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@var";
        if (var != string.Empty)
            param.Value = var;
        else
            param.Value = "";
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_GetCommonDataByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetCommonDataByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdatePriceVeriation(string ProjectID, string FinanceWithID, string PaymentTypeID, string TotalQuotePrice, string DepositRequired, string ServiceValue, string VarHouseType, string VarRoofType, string VarRoofAngle, string VarMeterInstallation, string VarMeterUG, string VarAsbestos, string VarSplitSystem, string VarCherryPicker, string VarTravelTime, string VarHotWaterMeter, string VarSmartMeter, string VarOther, string VarSpecialDiscount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePriceVeriation";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeID";
        if (PaymentTypeID != string.Empty)
            param.Value = PaymentTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TotalQuotePrice";
        if (TotalQuotePrice != string.Empty)
            param.Value = TotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositRequired";
        if (DepositRequired != string.Empty)
            param.Value = DepositRequired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceValue";
        if (ServiceValue != string.Empty)
            param.Value = ServiceValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarHouseType";
        if (VarHouseType != string.Empty)
            param.Value = VarHouseType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofType";
        if (VarRoofType != string.Empty)
            param.Value = VarRoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofAngle";
        if (VarRoofAngle != string.Empty)
            param.Value = VarRoofAngle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterInstallation";
        if (VarMeterInstallation != string.Empty)
            param.Value = VarMeterInstallation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterUG";
        if (VarMeterUG != string.Empty)
            param.Value = VarMeterUG;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarAsbestos";
        if (VarAsbestos != string.Empty)
            param.Value = VarAsbestos;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarSplitSystem";
        if (VarSplitSystem != string.Empty)
            param.Value = VarSplitSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarCherryPicker";
        if (VarCherryPicker != string.Empty)
            param.Value = VarCherryPicker;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarTravelTime";
        if (VarTravelTime != string.Empty)
            param.Value = VarTravelTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarHotWaterMeter";
        if (VarHotWaterMeter != string.Empty)
            param.Value = VarHotWaterMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarSmartMeter";
        if (VarSmartMeter != string.Empty)
            param.Value = VarSmartMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarOther";
        if (VarOther != string.Empty)
            param.Value = VarOther;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarSpecialDiscount";
        if (VarSpecialDiscount != string.Empty)
            param.Value = VarSpecialDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_UploadDocumentList_GetActiveDocument()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_UploadDocumentList_GetActiveDocument";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_GetDocDetailsbyProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetDocDetailsbyProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblInvoicePayments_SalesInfo_SelectByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_SalesInfo_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblInvoicePayments_DeleteByInvoicePaymentID(string InvoicePaymentID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_DeleteByInvoicePaymentID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        if (InvoicePaymentID != string.Empty)
            param.Value = InvoicePaymentID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblCustomer_GetData(string CustTypeID, string ResCom, string Area, string Customer, string StreetAddress, string StreetCity, string StreetState, string StreetPostCode, string StartDate, string EndDate, string Roles, string UserID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        if (Area != string.Empty)
            param.Value = Area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Roles";
        if (Roles != string.Empty)
            param.Value = Roles;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        if (UserID != string.Empty)
            param.Value = UserID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateData(string ProjectID, string QuoteAccepted, string DepositReceived, string UpdatedBy, string ActiveDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteAccepted";
        if (QuoteAccepted != string.Empty)
            param.Value = QuoteAccepted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositReceived";
        if (DepositReceived != string.Empty)
            param.Value = DepositReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveDate";
        if (ActiveDate != string.Empty)
            param.Value = ActiveDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static SttblCustomers tblCustomers_getAdress_SelectByCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustomers details = new SttblCustomers();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details

            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.StreetCity = dr["StreetCity"].ToString();
            details.StreetState = dr["StreetState"].ToString();
            details.StreetPostCode = dr["StreetPostCode"].ToString();
        }

        return details;
    }

    public static DataTable tblProjects_NewProjectTabSelectByUCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_NewProjectTabSelectByUCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomer_GetData_LeadTracker(string CustType, string ContLeadStatusID, string CustSourceID, string Customer, string MobileNo, string ProjectNumber, string Email, string Roles, string UserID, string StreetAddress, string StreetCity, string StreetState, string StreetPostCode, string DateType, string StartDate, string EndDate, string SalesTeamID, string CustSubSourceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        //comm.CommandText = "tblCustomer_GetData_LeadTracker";
        comm.CommandText = "tblCustomer_GetData_LeadTrackerNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Type";
        if (CustType != string.Empty)
            param.Value = CustType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        if (ContLeadStatusID != string.Empty)
            param.Value = ContLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Mobile";
        if (MobileNo != string.Empty)
            param.Value = MobileNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Email";
        if (Email != string.Empty)
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Roles";
        if (Roles != string.Empty)
            param.Value = Roles;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        if (UserID != string.Empty)
            param.Value = UserID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSubSourceID";
        if (CustSubSourceID != string.Empty)
            param.Value = CustSubSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static StStockItemData SalesInfo_tblStockItems_SelectByStockItemID(String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SalesInfo_tblStockItems_SelectByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        StStockItemData details = new StStockItemData();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockManufacturer = dr["StockManufacturer"].ToString();
            details.StockModel = dr["StockModel"].ToString();
            details.StockSeries = dr["StockSeries"].ToString();
            details.StockSize = dr["StockSize"].ToString();
        }

        // return structure details
        return details;
    }

    public static bool tblProjects_UpdateFinanceNew(string ProjectID, string ApplicationDate, string PurchaseNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateFinanceNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApplicationDate";
        if (ApplicationDate != string.Empty)
            param.Value = ApplicationDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseNo";
        if (PurchaseNo != string.Empty)
            param.Value = PurchaseNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable SP_LeadTracker_tblCustomer_GetCustomerByCustomerID(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SP_LeadTracker_tblCustomer_GetCustomerByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblCustInfo_UpdateFollowUp(string ContactID, string Description, string NextFollowupDate,  string FollowupType, string CustInfoID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_UpdateFollowUp";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustInfoID";
        param.Value = CustInfoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        if (Description != string.Empty)
            param.Value = Description;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 2147483647;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NextFollowupDate";
        if (NextFollowupDate != string.Empty)
            param.Value = NextFollowupDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowupType";
        if (FollowupType != string.Empty)
            param.Value = FollowupType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_MeterBoxPhotosSaved(string ProjectID, string MeterBoxPhotosSaved)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_MeterBoxPhotosSaved";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterBoxPhotosSaved";
        if (MeterBoxPhotosSaved != string.Empty)
            param.Value = MeterBoxPhotosSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_ElecBillSaved(string ProjectID, string ElecBillSaved)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_ElecBillSaved";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecBillSaved";
        if (ElecBillSaved != string.Empty)
            param.Value = ElecBillSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_ProposedDesignSaved(string ProjectID, string ProposedDesignSaved)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_ProposedDesignSaved";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProposedDesignSaved";
        if (ProposedDesignSaved != string.Empty)
            param.Value = ProposedDesignSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_PaymentReceipt(string ProjectID, string PaymentReceipt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_PaymentReceipt";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentReceipt";
        if (PaymentReceipt != string.Empty)
            param.Value = PaymentReceipt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblContacts_GetData(string SerachMode, string EmployeeID, string Company, string ContFirst, string ContLast, string Mobile, string ProjectNo, string Phone, string City, string Street, string PostCode, string Email)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerachMode";
        if (SerachMode != string.Empty)
            param.Value = SerachMode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Company";
        if (Company != string.Empty)
            param.Value = Company;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Mobile";
        if (Mobile != string.Empty)
            param.Value = Mobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Phone";
        if (Phone != string.Empty)
            param.Value = Phone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@City";
        if (City != string.Empty)
            param.Value = City;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Street";
        if (Street != string.Empty)
            param.Value = Street;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostCode";
        if (PostCode != string.Empty)
            param.Value = PostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Email";
        if (Email != string.Empty)
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProject_GetData_Project()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProject_GetData_Project";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@CustomerID";
        //param.Value = CustomerID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProject_GetData_Project_New(string Roles, string UserID, string SalesTeamID, string ProjectNo, string Customer, string ManualNo, string ProjectStatus, string ProjectType, string FinanceOption, string ReadyToActive, string Source, string SubSource, string State, string DateType, string StartDate, string EndDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProject_GetData_Project_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Roles";
        if (Roles != string.Empty)
            param.Value = Roles;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        if (UserID != string.Empty)
            param.Value = UserID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualNo";
        if (ManualNo != string.Empty)
            param.Value = ManualNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectType";
        if (ProjectType != string.Empty)
            param.Value = ProjectType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceOption";
        if (FinanceOption != string.Empty)
            param.Value = FinanceOption;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";
        if (ReadyToActive != string.Empty)
            param.Value = ReadyToActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_GetDataByProjectIDforNewProject(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetDataByProjectIDforNewProject";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateData_SQ(string ProjectID, string SignedQuote, string UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateData_SQ";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignedQuote";
        if (SignedQuote != string.Empty)
            param.Value = SignedQuote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_PriceCat(string ProjectID, string PriceCat)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_PriceCat";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PriceCat";
        if (PriceCat != string.Empty)
            param.Value = PriceCat;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {

        }

        return (result != -1);
    }

    public static bool tblProjects_UpdateBookingData(string ProjectID, string ElecDistOK, string Installer, string Designer, string Electrician, string StockAllocationStore, string InstallerNotes, string ProjectNotes, string MeterInstallerNotes, string ElecDistApproved, string InstallBookingDate, string UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateBookingData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistOK";
        if (ElecDistOK != string.Empty)
            param.Value = ElecDistOK;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockAllocationStore";
        if (StockAllocationStore != string.Empty)
            param.Value = StockAllocationStore;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotes";
        if (InstallerNotes != string.Empty)
            param.Value = InstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNotes";
        if (ProjectNotes != string.Empty)
            param.Value = ProjectNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstallerNotes";
        if (MeterInstallerNotes != string.Empty)
            param.Value = MeterInstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApproved";
        if (ElecDistApproved != string.Empty)
            param.Value = ElecDistApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {

        }

        return (result != -1);
    }

    public static bool tblProjects_Update_RemoveIntallation(string ProjectID, string InstallBookingDate, string Installer, string Designer, string Electrician, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_RemoveIntallation";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblCustomer_tblContact_Update_EmployeeIDByID(string ID, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_tblContact_Update_EmployeeIDByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_EmployeeIDByProjectNumber(string ProjectNo, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_EmployeeIDByProjectNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblProject_GetData_Project_New1(string Roles, string UserID, string SalesTeamID, string ProjectNo, string Customer, string ManualNo, string ProjectStatus, string ProjectType, string FinanceOption, string ReadyToActive, string Source, string SubSource, string State, string DateType, string StartDate, string EndDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProject_GetData_Project_New1";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Roles";
        if (Roles != string.Empty)
            param.Value = Roles;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        if (UserID != string.Empty)
            param.Value = UserID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualNo";
        if (ManualNo != string.Empty)
            param.Value = ManualNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectType";
        if (ProjectType != string.Empty)
            param.Value = ProjectType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceOption";
        if (FinanceOption != string.Empty)
            param.Value = FinanceOption;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";
        if (ReadyToActive != string.Empty)
            param.Value = ReadyToActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProject_GetData_Project_New_lts(string Roles, string UserID, string SalesTeamID, string ProjectNo, string Customer, string ManualNo, string ProjectStatus, string ProjectType, string FinanceOption, string ReadyToActive, string Source, string SubSource, string State, string DateType, string StartDate, string EndDate, string PanelModel, string InverterModel, string From, string To, string NMINo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProject_GetData_Project_New_lts";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Roles";
        if (Roles != string.Empty)
            param.Value = Roles;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        if (UserID != string.Empty)
            param.Value = UserID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualNo";
        if (ManualNo != string.Empty)
            param.Value = ManualNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectType";
        if (ProjectType != string.Empty)
            param.Value = ProjectType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceOption";
        if (FinanceOption != string.Empty)
            param.Value = FinanceOption;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";
        if (ReadyToActive != string.Empty)
            param.Value = ReadyToActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@From";
        if (From != string.Empty)
            param.Value = From;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@To";
        if (To != string.Empty)
            param.Value = To;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@NMINumber";
        if (NMINo != string.Empty)
            param.Value = NMINo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProject_GetData_Project_New_lts_Excel(string Roles, string UserID, string SalesTeamID, string ProjectNo, string Customer, string ManualNo, string ProjectStatus, string ProjectType, string FinanceOption, string ReadyToActive, string Source, string SubSource, string State, string DateType, string StartDate, string EndDate, string PanelModel, string InverterModel, string From, string To, string NMINo)
    {
        string Constr = SiteConfiguration.DbConnectionString;
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Constr);
        conn.Open();
        System.Data.SqlClient.SqlCommand comm = new System.Data.SqlClient.SqlCommand();
        comm.Connection = conn;
        comm.CommandText = "tblProject_GetData_Project_New_lts_Excel";
        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandTimeout = 500000;

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Roles";
        if (Roles != string.Empty)
            param.Value = Roles;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        if (UserID != string.Empty)
            param.Value = UserID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNo";
        if (ProjectNo != string.Empty)
            param.Value = ProjectNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualNo";
        if (ManualNo != string.Empty)
            param.Value = ManualNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectType";
        if (ProjectType != string.Empty)
            param.Value = ProjectType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceOption";
        if (FinanceOption != string.Empty)
            param.Value = FinanceOption;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";
        if (ReadyToActive != string.Empty)
            param.Value = ReadyToActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@From";
        if (From != string.Empty)
            param.Value = From;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@To";
        if (To != string.Empty)
            param.Value = To;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NMINumber";
        if (NMINo != string.Empty)
            param.Value = NMINo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            DbDataReader reader = comm.ExecuteReader();

            result.Load(reader);
            reader.Close();
            conn.Close();
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;

    }

    

    public static DataTable tbl_SignatureLog_GetSignedDateByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SignatureLog_GetSignedDateByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomer_GetDataByIDforAssignLead(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_GetDataByIDforAssignLead";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_leadData_Track_UpdateExtra(string ID, string Source, string SubSource, string CustType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_leadData_Track_UpdateExtra";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustType";
        if (CustType != string.Empty)
            param.Value = CustType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateDOBbyProjectID(string ProjectID, string DoB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateDOBbyProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DOB";
        if (DoB != string.Empty)
            param.Value = DoB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_InstallCompleted(string ProjectID, string InstallCompleted)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_InstallCompleted";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCompleted";
        if (InstallCompleted != string.Empty)
            param.Value = InstallCompleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);

        try
        {
        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }

    public static bool tblProjects_Update_MeterDetail(string ProjectID, string MeterAppliedRef, string ComplainceCertificate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_MeterDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedRef";
        if (MeterAppliedRef != string.Empty)
            param.Value = MeterAppliedRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ComplainceCertificate";
        if (ComplainceCertificate != string.Empty)
            param.Value = ComplainceCertificate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_leadData_Track_Update_CustEmpIDByID(string ID, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_leadData_Track_Update_CustEmpIDByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_DepositReceivedNew(string ProjectID, string DepositReceivedNew)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_DepositReceivedNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositReceivedNew";
        if (DepositReceivedNew != string.Empty)
            param.Value = DepositReceivedNew;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_varOtherBasicCost(string ProjectID, string varOtherBasicCost)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_varOtherBasicCost";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@varOtherBasicCost";
        if (varOtherBasicCost != string.Empty)
            param.Value = varOtherBasicCost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {

        }

        return (result != -1);
    }

    public static int tbl_VariationLog_Insert(string projectId, string variationId, string oldValue, string newValue, string createdOn, string createdBy, string isDeleted)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_VariationLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectId";
        if (projectId != string.Empty)
            param.Value = projectId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@variationId";
        if (variationId != string.Empty)
            param.Value = variationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@oldValue";
        if (oldValue != string.Empty)
            param.Value = oldValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@newValue";
        if (newValue != string.Empty)
            param.Value = newValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@createdOn";
        if (createdOn != string.Empty)
            param.Value = createdOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@createdBy";
        if (createdBy != string.Empty)
            param.Value = createdBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@isDeleted";
        if (isDeleted != string.Empty)
            param.Value = isDeleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch (Exception ex)
        {

        }
        return result;
    }

    public static bool tblProjects_Update_DiscountNotes(string ProjectID, string varSpecialDiscountNotes, string varManagerDiscountNotes, string varOtherBasicCostNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_DiscountNotes";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@varSpecialDiscountNotes";
        if (varSpecialDiscountNotes != string.Empty)
            param.Value = varSpecialDiscountNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@varManagerDiscountNotes";
        if (varManagerDiscountNotes != string.Empty)
            param.Value = varManagerDiscountNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@varOtherBasicCostNotes";
        if (varOtherBasicCostNotes != string.Empty)
            param.Value = varOtherBasicCostNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {

        }

        return (result != -1);
    }

    public static bool Update_tblProjects_OldPanelDetails(string projectId, string oldPanelBrandId, string oldPanelBrand, string oldPanelModel, string oldPanelOutput, string oldNoOfPanels, string oldSystemCapKW)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblProjects_OldPanelDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectId";
        if (projectId != string.Empty)
            param.Value = projectId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldPanelBrandId";
        if (oldPanelBrandId != string.Empty)
            param.Value = oldPanelBrandId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldPanelBrand";
        if (oldPanelBrand != string.Empty)
            param.Value = oldPanelBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldPanelModel";
        if (oldPanelModel != string.Empty)
            param.Value = oldPanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@oldPanelOutput";
        if (oldPanelOutput != string.Empty)
            param.Value = oldPanelOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@oldNoOfPanels";
        if (oldNoOfPanels != string.Empty)
            param.Value = oldNoOfPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@oldSystemCapKW";
        if (oldSystemCapKW != string.Empty)
            param.Value = oldSystemCapKW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {

        }

        return (result != -1);
    }

    public static bool Update_tblProjects_OldPanelDetailsManual(string projectId, string oldPanelBrandManual, string oldPanelModelManual, string oldPanelOutputManual, string oldNoOfPanelsManual, string oldSystemCapKWManual)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblProjects_OldPanelDetailsManual";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectId";
        if (projectId != string.Empty)
            param.Value = projectId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldPanelBrandManual";
        if (oldPanelBrandManual != string.Empty)
            param.Value = oldPanelBrandManual;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldPanelModelManual";
        if (oldPanelModelManual != string.Empty)
            param.Value = oldPanelModelManual;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldPanelOutputManual";
        if (oldPanelOutputManual != string.Empty)
            param.Value = oldPanelOutputManual;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldNoOfPanelsManual";
        if (oldNoOfPanelsManual != string.Empty)
            param.Value = oldNoOfPanelsManual;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldSystemCapKWManual";
        if (oldSystemCapKWManual != string.Empty)
            param.Value = oldSystemCapKWManual;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {

        }

        return (result != -1);
    }

    public static bool Update_tblProjects_OldRemoveOldSystem(string projectId, string OldRemoveOldSystem)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tblProjects_OldRemoveOldSystem";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectId";
        if (projectId != string.Empty)
            param.Value = projectId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldRemoveOldSystem";
        if (OldRemoveOldSystem != string.Empty)
            param.Value = OldRemoveOldSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {

        }
        return (result != -1);
    }
}

