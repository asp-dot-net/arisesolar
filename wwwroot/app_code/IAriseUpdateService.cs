﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAriseUpdateService" in both code and config file together.
[ServiceContract]
public interface IAriseUpdateService
{
    [OperationContract]
    void DoWork();
    [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "tblProjects_UpdateDeductNote_FromService")]
    AriseUpdateService.UpdateData tblProjects_UpdateDeductNote_FromService(string projectId, string projectnumber, string note,
         string pickupdate, string installerid, string pickupnote, string updatedby,
         string updateddate, string picklistid);

    [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "tblprojectsUpdateStatus")]
    AriseUpdateService.UpdateData tblprojectsUpdateStatus(string ProjectId);

    [WebInvokeAttribute(Method = "GET",
         RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "/GetProjectsRelatedDetails/{ProjectId}")]
    AriseUpdateService.ProjectDetails GetProjectsRelatedDetails(string ProjectId);
}
