using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblHouseType
{
    public string HouseType;
    public string Active;
    public string Seq;
    public string Variation;
    public string HouseTypeABB;
}


public class ClstblHouseType
{
    public static SttblHouseType tblHouseType_SelectByHouseTypeID(String HouseTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_SelectByHouseTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        if (HouseTypeID != string.Empty)
            param.Value = HouseTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblHouseType details = new SttblHouseType();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.HouseType = dr["HouseType"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();
            details.Variation = dr["Variation"].ToString();
            details.HouseTypeABB = dr["HouseTypeABB"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tblHouseType_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblHouseType_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblHouseType_SelectByHouseType(string HouseType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_SelectByHouseType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HouseType";
        param.Value = HouseType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblHouseType_Insert(String HouseType, String Active, String Seq, String Variation, String HouseTypeABB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HouseType";
        param.Value = HouseType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Variation";
        if (Variation != string.Empty)
            param.Value = Variation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeABB";
        if (HouseTypeABB != string.Empty)
            param.Value = HouseTypeABB;
        else
            param.Value = DBNull.Value; 
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblHouseType_Update(string HouseTypeID, String HouseType, String Active, String Seq, String Variation, String HouseTypeABB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        param.Value = HouseTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseType";
        param.Value = HouseType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Variation";
        if (Variation != string.Empty)
            param.Value = Variation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeABB";
        if (HouseTypeABB != string.Empty)
            param.Value = HouseTypeABB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);



        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblHouseType_InsertUpdate(Int32 HouseTypeID, String HouseType, String Active, String Seq, String Variation, String HouseTypeABB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        param.Value = HouseTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseType";
        param.Value = HouseType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Variation";
        param.Value = Variation;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeABB";
        param.Value = HouseTypeABB;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblHouseType_Delete(string HouseTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseType_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        param.Value = HouseTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static int tblHouseTypeNameExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseTypeNameExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblHouseTypeExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseTypeExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }


    public static DataTable tblHouseTypeGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblHouseTypeGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblHouseTypeGetDataBySearch(string alpha,string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblHouseTypeGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}
