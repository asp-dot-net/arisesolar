using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Globalization;
using net.openstack.Providers.Rackspace;
using net.openstack.Core.Domain;
using net.openstack.Core.Providers;

/// <summary>
/// Summary description for configuration
/// </summary>
public static class SiteConfiguration
{
    private readonly static string dbConnectionString;
    private readonly static string dbProviderName;
    private readonly static string requireLogin;
    private readonly static string payPalAPIAccountName;
    private readonly static string payPalAPICertificationPath;
    private readonly static string payPalAPICertificationPassword;
    private readonly static string payPalAPIAccountPassword;
    private readonly static string solarminerConnectionString;
    private readonly static string solarminerdbProviderName;
    private readonly static string quickstockConnectionString;
    private readonly static string quickstockdbProviderName;


    //-------------------Change Here For Currency Start-------------------


    //-------------------Change Here For Currency End-------------------
    public static string GetURL()
    {
        return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/";
    }
    static SiteConfiguration()
    {
        dbConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        dbProviderName = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ProviderName;

        solarminerConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringSolarMiner"].ConnectionString;
        solarminerdbProviderName = ConfigurationManager.ConnectionStrings["SolarMinerLocalSqlServer"].ProviderName;

        quickstockConnectionString = ConfigurationManager.ConnectionStrings["QuickStockConnectionString"].ConnectionString;
        quickstockdbProviderName = ConfigurationManager.ConnectionStrings["QuickStockLocalSqlServer"].ProviderName;
    }
    //--------------dropdownlist bind-----------------
    public static string SolarminerConnectionString
    {
        get
        {
            return solarminerConnectionString;
        }
    }
    public static string QuickStockConnectionString
    {
        get
        {
            return quickstockConnectionString;
        }
    }
    public static string GetArray()
    {
        return "50,100,All";
    }
    public static string GetPageSize()
    {
        return "25";
    }
    //---------------Product Size-----------------------
    //public static int ProductSize()
    //{
    //    StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
    //    return Convert.ToInt32(st.ProductSize);
    //}
    public static string PayPalAPIAccountPassword
    {
        get
        {
            return payPalAPIAccountPassword;
        }
    }
    public static string PayPalAPICertificationPassword
    {
        get
        {
            return payPalAPICertificationPassword;
        }
    }
    public static string PayPalAPICertificationPath
    {
        get
        {
            return payPalAPICertificationPath;
        }
    }
    public static string PayPalAPIAccountName
    {
        get
        {
            return payPalAPIAccountName;
        }
    }
    public static string RequireLogin
    {
        get
        {
            return requireLogin;
        }
    }
    // Returns the address of the mail server
    public static string MailServer
    {
        get
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            return st.MailServer;
            //   return ConfigurationManager.AppSettings["MailServer"];
        }
    }
    // Returns the connection string for the BalloonShop database
    public static int MailServerPort
    {
        get
        {
            //return ConfigurationManager.AppSettings["MailServer"];
            return Convert.ToInt32(ClsAdminUtilities.StUtilitiesGetDataStructById("1").port.ToString());
        }
    }

    public static string DbConnectionString
    {
        get
        {
            return dbConnectionString;
        }
    }
    // Send error log emails?
    public static bool EnableErrorLogEmail
    {
        get
        {
            return bool.Parse(ConfigurationManager.AppSettings["EnableErrorLogEmail"]);
        }
    }
    // Returns the email address where to send error reports
    public static string ErrorLogEmail
    {
        get
        {
            return ConfigurationManager.AppSettings["ErrorLogEmail"];
        }
    }
    public static string WebAddress
    {
        get
        {
            return ConfigurationManager.AppSettings["WebAddress"];
        }
    }
    //----------------------Image Resize code Start----------------------------
    public static bool ThumbnailCallBack()
    {
        return false;
    }

    public static bool getimage(string pimage, string foldername, int large, int medium, int small)
    {
        int imgwidth = 0;
        int imgheight = 0;
        int imgthumbwidth = 0;
        int imgthumbheight = 0;
        int imgiconwidth = 0;
        int imgiconheight = 0;

        //Panel1.Visible = true;
        System.Drawing.Image.GetThumbnailImageAbort callback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallBack);
        System.Drawing.Image mainImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\") + pimage);

        //--resize main image - if image's height or width > 250 px

        if (mainImage.Width >= large || mainImage.Height >= large)
        {
            if (mainImage.Width > mainImage.Height)
            {
                imgwidth = large;
                imgheight = (mainImage.Height * large) / mainImage.Width;

                imgthumbwidth = medium;
                imgthumbheight = (mainImage.Height * medium) / mainImage.Width;

                imgiconwidth = small;
                imgiconheight = (mainImage.Height * small) / mainImage.Width;

            }
            else if (mainImage.Width < mainImage.Height)
            {
                imgheight = large;
                imgwidth = (mainImage.Width * large) / mainImage.Height;

                imgthumbheight = medium;
                imgthumbwidth = (mainImage.Width * medium) / mainImage.Height;

                imgiconheight = small;
                imgiconwidth = (mainImage.Width * small) / mainImage.Height;
            }
            else if (mainImage.Width == mainImage.Height)
            {
                imgheight = large;
                imgwidth = large;
                imgthumbwidth = medium;
                imgthumbheight = medium;
                imgiconheight = small;
                imgiconwidth = small;
            }

            string ProductImageMain = pimage;
            Bitmap thumbnail1 = new Bitmap(imgwidth, imgheight, PixelFormat.Format24bppRgb);
            thumbnail1.SetResolution(mainImage.HorizontalResolution, mainImage.VerticalResolution);
            Graphics grPhoto = Graphics.FromImage(thumbnail1);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
            grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
            grPhoto.CompositingQuality = CompositingQuality.HighQuality;

            grPhoto.DrawImage(mainImage,
                new Rectangle(-1, -1, imgwidth + 2, imgheight + 2),
                new Rectangle(-1, -1, mainImage.Width + 2, mainImage.Height + 2),
                GraphicsUnit.Pixel);
            grPhoto.Dispose();
            thumbnail1.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\large\\") + ProductImageMain);
            thumbnail1.Dispose();


            string ProudctImageThumb = pimage;
            Bitmap thumbnail2 = new Bitmap(imgthumbwidth, imgthumbheight, PixelFormat.Format24bppRgb);
            thumbnail2.SetResolution(mainImage.HorizontalResolution, mainImage.VerticalResolution);
            Graphics grPhoto2 = Graphics.FromImage(thumbnail2);
            grPhoto2.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto2.SmoothingMode = SmoothingMode.AntiAlias;
            grPhoto2.PixelOffsetMode = PixelOffsetMode.HighQuality;
            grPhoto2.CompositingQuality = CompositingQuality.HighQuality;

            grPhoto2.DrawImage(mainImage,
                new Rectangle(-1, -1, imgthumbwidth + 2, imgthumbheight + 2),
                new Rectangle(-1, -1, mainImage.Width + 2, mainImage.Height + 2),
                GraphicsUnit.Pixel);
            grPhoto2.Dispose();
            thumbnail2.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + ProudctImageThumb);
            thumbnail2.Dispose();


            //--create icon of the image
            string ProductImageIcon = pimage;
            Bitmap thumbnail3 = new Bitmap(imgiconwidth, imgiconheight, PixelFormat.Format24bppRgb);
            thumbnail3.SetResolution(mainImage.HorizontalResolution, mainImage.VerticalResolution);
            Graphics grPhoto3 = Graphics.FromImage(thumbnail3);
            grPhoto3.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto3.SmoothingMode = SmoothingMode.AntiAlias;
            grPhoto3.PixelOffsetMode = PixelOffsetMode.HighQuality;
            grPhoto3.CompositingQuality = CompositingQuality.HighQuality;

            grPhoto3.DrawImage(mainImage,
                new Rectangle(-1, -1, imgiconwidth + 2, imgiconheight + 2),
                new Rectangle(-1, -1, mainImage.Width + 2, mainImage.Height + 2),
                GraphicsUnit.Pixel);
            grPhoto3.Dispose();
            thumbnail3.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\small\\") + ProductImageIcon);
            thumbnail3.Dispose();

        }
        else
        {
            mainImage.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\large\\") + pimage);


            if ((mainImage.Width >= medium & mainImage.Width < large) | (mainImage.Height >= medium & mainImage.Width < large))
            {
                if (mainImage.Width > mainImage.Height)
                {

                    imgthumbwidth = medium;
                    imgthumbheight = (mainImage.Height * medium) / mainImage.Width;

                    imgiconwidth = small;
                    imgiconheight = (mainImage.Height * small) / mainImage.Width;

                }
                else if (mainImage.Width < mainImage.Height)
                {

                    imgthumbheight = medium;
                    imgthumbwidth = (mainImage.Width * medium) / mainImage.Height;

                    imgiconheight = small;
                    imgiconwidth = (mainImage.Width * small) / mainImage.Height;
                }
                else if (mainImage.Width == mainImage.Height)
                {
                    imgthumbwidth = medium;
                    imgthumbheight = medium;
                    imgiconheight = small;
                    imgiconwidth = small;
                }
                //--create thumbnail of the image
                string ProudctImageThumb = pimage;
                Bitmap thumbnail2 = new Bitmap(imgthumbwidth, imgthumbheight, PixelFormat.Format24bppRgb);
                thumbnail2.SetResolution(mainImage.HorizontalResolution, mainImage.VerticalResolution);
                Graphics grPhoto2 = Graphics.FromImage(thumbnail2);
                grPhoto2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto2.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto2.PixelOffsetMode = PixelOffsetMode.HighQuality;
                grPhoto2.CompositingQuality = CompositingQuality.HighQuality;

                grPhoto2.DrawImage(mainImage,
                    new Rectangle(-1, -1, imgthumbwidth + 2, imgthumbheight + 2),
                    new Rectangle(-1, -1, mainImage.Width + 2, mainImage.Height + 2),
                    GraphicsUnit.Pixel);
                grPhoto2.Dispose();
                thumbnail2.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + ProudctImageThumb);
                thumbnail2.Dispose();

                //--create icon of the image
                string ProductImageIcon = pimage;
                Bitmap thumbnail3 = new Bitmap(imgiconwidth, imgiconheight, PixelFormat.Format24bppRgb);
                thumbnail3.SetResolution(mainImage.HorizontalResolution, mainImage.VerticalResolution);
                Graphics grPhoto3 = Graphics.FromImage(thumbnail3);
                grPhoto3.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto3.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto3.PixelOffsetMode = PixelOffsetMode.HighQuality;
                grPhoto3.CompositingQuality = CompositingQuality.HighQuality;

                grPhoto3.DrawImage(mainImage,
                    new Rectangle(-1, -1, imgiconwidth + 2, imgiconheight + 2),
                    new Rectangle(-1, -1, mainImage.Width + 2, mainImage.Height + 2),
                    GraphicsUnit.Pixel);
                grPhoto3.Dispose();
                thumbnail3.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\small\\") + ProductImageIcon);
                thumbnail3.Dispose();
            }
            else
            {
                //  mainImage.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + pimage);

                if (mainImage.Width < medium | mainImage.Height < medium)
                {
                    if (mainImage.Width > mainImage.Height)
                    {


                        imgiconwidth = small;
                        imgiconheight = (mainImage.Height * small) / mainImage.Width;

                    }
                    else if (mainImage.Width < mainImage.Height)
                    {

                        imgiconheight = small;
                        imgiconwidth = (mainImage.Width * small) / mainImage.Height;
                    }
                    else if (mainImage.Width == mainImage.Height)
                    {
                        imgiconheight = small;
                        imgiconwidth = small;
                    }
                    mainImage.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\large\\") + pimage);
                    mainImage.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + pimage);

                    //--create icon of the image
                    string ProductImageIcon = pimage;
                    Bitmap thumbnail3 = new Bitmap(imgiconwidth, imgiconheight, PixelFormat.Format24bppRgb);
                    thumbnail3.SetResolution(mainImage.HorizontalResolution, mainImage.VerticalResolution);
                    Graphics grPhoto3 = Graphics.FromImage(thumbnail3);
                    grPhoto3.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    grPhoto3.SmoothingMode = SmoothingMode.AntiAlias;
                    grPhoto3.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    grPhoto3.CompositingQuality = CompositingQuality.HighQuality;

                    grPhoto3.DrawImage(mainImage,
                        new Rectangle(-1, -1, imgiconwidth + 2, imgiconheight + 2),
                        new Rectangle(-1, -1, mainImage.Width + 2, mainImage.Height + 2),
                        GraphicsUnit.Pixel);
                    grPhoto3.Dispose();
                    thumbnail3.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\small\\") + ProductImageIcon);
                    thumbnail3.Dispose();
                }
            }
        }
        mainImage.Dispose();
        return true;
    }
    //----------------------Image Resize code End-------------------------------
    //----------------------Image Delete Code Start-----------------------------
   
    //----------------------Image Delete Code End-----------------------------
    //----------------------Add To Cart Start----------------------------------
    //----------------------Add To Cart Start----------------------------------

    //----------------------ChangeCurrency Start-----------------------------
    public static string ChangeCurrencyUser(string Price)
    {
        StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
        int Currencydigit = Convert.ToInt32(st.Currencydigit);
        string Currency = st.Symbol;
        // HttpContext.Current.Response.Write(Price);
        //HttpContext.Current.Response.End();
        Price = Convert.ToString(Math.Round(Convert.ToDecimal(Price), Currencydigit));
        return Price;
    }
    public static string ChangeCurrency(string Price)
    {
        StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
        int Currencydigit = Convert.ToInt32(st.Currencydigit);
        string Currency = st.Symbol;

        Price = Convert.ToString(Math.Round(Convert.ToDecimal(Price), Currencydigit)) + " " + Currency;
        return Price;
    }
    public static string ChangeCurrencyVal(string Price)
    {
        StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
        int Currencydigit = Convert.ToInt32(st.Currencydigit);
        string Currency = st.Symbol;
        //Price = string.Format("{0:0.00 Rs}", Convert.ToDecimal(Price));
        //  Price = Convert.ToString(Math.Round(Convert.ToDecimal(Price), Currencydigit));
        return Price;
    }

    public static string ChangeCurrency_Val(string Price)
    {
        StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
        //int Price1 = Convert.ToInt32(st.Currencydigit);
        // string Currency = st.Symbol;
        //Price = string.Format("{0:0.000 Rs}", Convert.ToDecimal(Price));
        Price = string.Format("{0:0.00}", Convert.ToDecimal(Price));
        return Price.ToString();
    }

    public static string ChangeCurrency_Val1(string Price)
    {
        StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
        //int Price1 = Convert.ToInt32(st.Currencydigit);
        // string Currency = st.Symbol;
        //Price = string.Format("{0:0.000 Rs}", Convert.ToDecimal(Price));
        Price = string.Format("{0:0.0}", Convert.ToDecimal(Price));
        return Price.ToString();
    }
    //----------------------ChangeCurrency End-----------------------------

    public static bool getimageGallery(string pimage, string foldername, int large, string imagepath)
    {
        int imgwidth = 0;
        int imgheight = 0;
        int imgthumbwidth = 0;
        int imgthumbheight = 0;
        int imgiconwidth = 0;
        int imgiconheight = 0;

        //Panel1.Visible = true;
        System.Drawing.Image.GetThumbnailImageAbort callback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallBack);
        System.Drawing.Image mainImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(imagepath));
        //--resize main image - if image's height or width > 250 px

        if (mainImage.Width >= large || mainImage.Height >= large)
        {
            if (mainImage.Width > mainImage.Height)
            {
                imgwidth = large;
                imgheight = (mainImage.Height * large) / mainImage.Width;


            }
            else if (mainImage.Width < mainImage.Height)
            {
                imgheight = large;
                imgwidth = (mainImage.Width * large) / mainImage.Height;

            }
            else if (mainImage.Width == mainImage.Height)
            {
                imgheight = large;
                imgwidth = large;

            }
            System.Drawing.Image thumbnail1 = mainImage.GetThumbnailImage(imgwidth, imgheight, callback, IntPtr.Zero);
            string ProductImageMain = pimage;
            thumbnail1.Save(HttpContext.Current.Server.MapPath("\\galleryimages\\" + foldername + "\\large\\") + ProductImageMain);
            thumbnail1.Dispose();


        }
        else
        {
            mainImage.Save(HttpContext.Current.Server.MapPath("\\galleryimages\\" + foldername + "\\large\\") + pimage);
        }
        mainImage.Dispose();
        return true;
    }
    //----------------------Image Resize code End-------------------------------
    public static bool getlogo(string pimage, string foldername, int medium, int small)
    {

        int imgthumbwidth = 0;
        int imgthumbheight = 0;
        int imgiconwidth = 0;
        int imgiconheight = 0;

        //Panel1.Visible = true;
        System.Drawing.Image.GetThumbnailImageAbort callback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallBack);
        System.Drawing.Image mainImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\") + pimage);
        //--resize main image - if image's height or width > 250 px

        if ((mainImage.Width >= medium) | (mainImage.Height >= medium))
        {
            if (mainImage.Width > mainImage.Height)
            {

                imgthumbwidth = medium;
                imgthumbheight = (mainImage.Height * medium) / mainImage.Width;

                imgiconwidth = small;
                imgiconheight = (mainImage.Height * small) / mainImage.Width;

            }
            else if (mainImage.Width < mainImage.Height)
            {

                imgthumbheight = medium;
                imgthumbwidth = (mainImage.Width * medium) / mainImage.Height;

                imgiconheight = small;
                imgiconwidth = (mainImage.Width * small) / mainImage.Height;
            }
            else if (mainImage.Width == mainImage.Height)
            {
                imgthumbwidth = medium;
                imgthumbheight = medium;
                imgiconheight = small;
                imgiconwidth = small;
            }

            //--create thumbnail of the image
            System.Drawing.Image thumbnail2 = mainImage.GetThumbnailImage(imgthumbwidth, imgthumbheight, callback, IntPtr.Zero);
            string ProudctImageThumb = pimage;
            thumbnail2.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + ProudctImageThumb);
            thumbnail2.Dispose();

            //--create icon of the image
            System.Drawing.Image thumbnail3 = mainImage.GetThumbnailImage(imgiconwidth, imgiconheight, callback, IntPtr.Zero);
            string ProductImageIcon = pimage;
            thumbnail3.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\small\\") + ProductImageIcon);
            thumbnail3.Dispose();
        }
        else
        {

            mainImage.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + pimage);

            if (mainImage.Width < medium | mainImage.Height < medium)
            {
                if (mainImage.Width > mainImage.Height)
                {


                    imgiconwidth = small;
                    imgiconheight = (mainImage.Height * small) / mainImage.Width;

                }
                else if (mainImage.Width < mainImage.Height)
                {

                    imgiconheight = small;
                    imgiconwidth = (mainImage.Width * small) / mainImage.Height;
                }
                else if (mainImage.Width == mainImage.Height)
                {
                    imgiconheight = small;
                    imgiconwidth = small;
                }
                mainImage.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + pimage);

                //--create icon of the image
                System.Drawing.Image thumbnail3 = mainImage.GetThumbnailImage(imgiconwidth, imgiconheight, callback, IntPtr.Zero);
                string ProductImageIcon = pimage;
                thumbnail3.Save(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\small\\") + ProductImageIcon);
                thumbnail3.Dispose();
            }
        }

        mainImage.Dispose();
        return true;
    }

    public static string ConvertToDateView(string date)
    {
        string dateval = "";
        try
        {
            dateval = string.Format("{0:dd MMM yyyy }", Convert.ToDateTime(date));
        }
        catch { }
        return dateval;
    }

    public static string ConvertToSystemDateDefault(string date)
    {
        string dateval = "";
        try
        {
            dateval = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(date));
        }
        catch
        {

            dateval = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(Getdate_current()));
        }
        return dateval;
    }

    public static string Getdate_current()
    {
        string dateval = "";
        try
        {
            dateval = DateTime.Now.AddHours(14).ToShortDateString();
        }
        catch { }
        return dateval;
    }
    //==============First Letter Capital
    public static string FirstLetterToUpper(string str)
    {
        str = str.Trim();
        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
    }
    public static string LetterTOCode(string str)
    {
        str = str.Trim();
        str = str.Replace(" ", "");
        return CultureInfo.CurrentCulture.TextInfo.ToUpper(str.ToUpper());
    }
    public static string GetNameImage(String name)
    {
        name = name.Trim();
        return name.Replace(" ", "").Replace("`", "").Replace("~", "").Replace("!", "").Replace("@", "").Replace("#", "").Replace("$", "").Replace("%", "").Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace("_", "").Replace("=", "").Replace("+", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("|", "").Replace("\\", "").Replace(";", "").Replace(":", "").Replace("\"", "").Replace("'", "").Replace("<", "").Replace(">", "").Replace("?", "").Replace(",", "").Replace("/", "");
    }
    public static bool DeletePDFFile(string foldername, string filename)
    {
        try
        {
            GC.Collect();
            string username = ConfigurationManager.AppSettings["ClaudUsername"];
            string api_key = ConfigurationManager.AppSettings["ClaudKey"];
            string chosenContainer = ConfigurationManager.AppSettings["ClaudContainer"];
            string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
            var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
            var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
            if (!string.IsNullOrEmpty(filename))
            {
                if (foldername != "")
                {
                    chosenContainer = Path.Combine(chosenContainer, foldername);
                }
                //HttpContext.Current.Response.Write(filename);
                //HttpContext.Current.Response.End();
                // cloudFilesProvider.DeleteObject(chosenContainer, filename);
                cloudFilesProvider.DeleteObject(chosenContainer, filename);
            }
        }
        catch
        {
        }
        return true;
    }
    public static bool UploadPDFFile(string foldername, string filename)
    {

        try
        {
            
	    GC.Collect();
            string username = ConfigurationManager.AppSettings["ClaudUsername"];
            string api_key = ConfigurationManager.AppSettings["ClaudKey"];
            string chosenContainer = ConfigurationManager.AppSettings["ClaudContainer"];
            string filePath = HttpContext.Current.Request.PhysicalApplicationPath + "/" + foldername;
            var cloudIdentity = new CloudIdentity() { APIKey = api_key, Username = username };
            var cloudFilesProvider = new CloudFilesProvider(cloudIdentity);

            /*using (FileStream stream = System.IO.File.OpenRead(HttpContext.Current.Request.PhysicalApplicationPath + "/userfiles/" + foldername + "/" + filename))
            {
                //HttpContext.Current.Response.Write(stream);

                
                cloudFilesProvider.CreateObject(chosenContainer, stream, filename);

                stream.Flush();
                stream.Close();
            }*/
		if (foldername != "")
                {
                    chosenContainer = Path.Combine(chosenContainer, foldername);
                }
		cloudFilesProvider.CreateObjectFromFile(chosenContainer, HttpContext.Current.Request.PhysicalApplicationPath + "/userfiles/" + foldername + "/"+filename, filename);
        }
        catch (Exception ex)
        {

        }
        //HttpContext.Current.Response.End();
        return true;
    }
    public static bool deleteimage(string cimage, string foldername)
    {

        System.IO.FileInfo image = new System.IO.FileInfo(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\") + cimage);
        bool isfileUploaded = image.Exists;
        if (isfileUploaded)
        {
            GC.Collect();
            try
            {
                image.Delete();
            }
            catch (Exception ex)
            {

            }
        }
        System.IO.FileInfo image1 = new System.IO.FileInfo(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\large\\") + cimage);
        bool isfileUploaded1 = image1.Exists;
        if (isfileUploaded1)
        {
            GC.Collect();
            try
            {
                image1.Delete();
            }
            catch (Exception ex)
            {

            }
        }
        System.IO.FileInfo image2 = new System.IO.FileInfo(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\medium\\") + cimage);
        bool isfileUploaded2 = image2.Exists;
        if (isfileUploaded2)
        {
            GC.Collect();
            try
            {
                image2.Delete();
            }
            catch (Exception ex)
            {

            }
        }
        System.IO.FileInfo image3 = new System.IO.FileInfo(HttpContext.Current.Server.MapPath("\\userfiles\\" + foldername + "\\small\\") + cimage);
        bool isfileUploaded3 = image3.Exists;
        if (isfileUploaded3)
        {
            GC.Collect();
            try
            {
                image3.Delete();
            }
            catch (Exception ex)
            {

            }
        }

        return true;
    }
    public static string FromSqlDate(DateTime date)
    {
        return date.ToString("dd/MM/yyyy");
    }
    public static string ConveqrtToSqlDate_View(string date)
    {
        string dateval = "";
        try
        {
            dateval = string.Format("{0:MM-dd-yyyy }", Convert.ToDateTime(date));
           // dateval = string.Format("{0:yyyy-MM-dd }", Convert.ToDateTime(date));
        }
        catch
        { }
        return dateval;
    }
    public static string ConveqrtToSqlDate_View1(string date)
    {
        string dateval = "";
        try
        {
            dateval = string.Format("{0:dd/MM/yyyy }", Convert.ToDateTime(date));
        }
        catch { }
        return dateval;
    }

     public static string GetDocumnetPath(string FolderName, string FileName)
    {
        string FilePath = "";

        System.IO.FileInfo image = new System.IO.FileInfo(HttpContext.Current.Request.PhysicalApplicationPath + "/userfiles/" + FolderName + "/" + FileName);

        bool isfileUploaded = image.Exists;
        if (isfileUploaded)
        {
            FilePath = ConfigurationManager.AppSettings["SiteURL"] + "/userfiles/" + FolderName + "/" + FileName;
        }
        else
        {
            FilePath = ConfigurationManager.AppSettings["cdnURL"] + "/" + FolderName + "/" + FileName;
        }
        //FilePath = ConfigurationManager.AppSettings["cdnURL"] + "/" + FolderName + "/" + FileName;

        return FilePath;
    }
}
