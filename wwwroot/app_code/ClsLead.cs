﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsLead
/// </summary>
public class ClsLead
{
    public ClsLead()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable tblWebDownload_GetLeadData(string StartDate, string EndDate, string Source, string SubSource, string Duplicate, string IsDelete, string State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_GetLeadData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Duplicate";
        if (Duplicate != string.Empty)
            param.Value = Duplicate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDelete";
        if (IsDelete != string.Empty)
            param.Value = IsDelete;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;

    }

    public static DataTable tblWebDownload_GetLeadDataFoeExcel(string StartDate, string EndDate, string Source, string SubSource, string Duplicate, string IsDelete, string State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_GetLeadDataFoeExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Duplicate";
        if (Duplicate != string.Empty)
            param.Value = Duplicate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDelete";
        if (IsDelete != string.Empty)
            param.Value = IsDelete;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;

    }

    public static DataTable SP_GetLead_ByEmployee(string StartDate, string EndDate, string SalesTeamID, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SP_GetLead_ByEmployee";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblWebDownload_GetDuplicateByEmailMobile(string Mabile, string Email)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_GetDuplicateByEmailMobile";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Mabile";
        if (Mabile != string.Empty)
            param.Value = Mabile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Email";
        if (Email != string.Empty)
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_GetDuplicateByEmailMobile(string Mabile, string Email)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_GetDuplicateByEmailMobile";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Mabile";
        if (Mabile != string.Empty)
            param.Value = Mabile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Email";
        if (Email != string.Empty)
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int tblLeadHistory_Insert(string CustomerID, string LeadName, string Source, string SubSource, string Message, string EmployeeID, string CreatedDate, string CreatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLeadHistory_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadName";
        if (LeadName != string.Empty)
            param.Value = LeadName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Message";
        if (Message != string.Empty)
            param.Value = Message;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedDate";
        if (CreatedDate != string.Empty)
            param.Value = CreatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblLeadHistory_ExistsByCustomerID(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLeadHistory_ExistsByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerId";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataSet SP_GetLead_ByEmployeeV2(string DateType, string StartDate, string EndDate, string SalesTeamID, string EmployeeID)
    {
        string connectionString = SiteConfiguration.DbConnectionString;
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);

        var comm = conn.CreateCommand();
        comm.CommandText = "SP_GetLead_ByEmployeeV2";
        comm.CommandType = CommandType.StoredProcedure;

        System.Data.SqlClient.SqlParameter param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartDate";
        if (StartDate != string.Empty)
            param.Value = StartDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndDate";
        if (EndDate != string.Empty)
            param.Value = EndDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataSet result = new DataSet();
        try
        {
            System.Data.SqlClient.SqlDataAdapter dsa = new System.Data.SqlClient.SqlDataAdapter(comm);
            dsa.Fill(result);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
}