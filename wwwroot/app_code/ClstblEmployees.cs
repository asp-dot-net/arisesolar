using System;
using System.Data;
using System.Data.Common;

public struct SttblEmployees
{
    public string EmployeeID;
    public string ContactID;
    public string EmployeeStatusID;
    public string SalesRep;
    public string SalesTeamID;
    public string SalesQuotaPeriodID;
    public string InitialSalesQuota;
    public string AdminStaff;
    public string HireDate;
    public string Location;
    public string EmpMr;
    public string EmpFirst;
    public string EmpLast;
    public string EmpAddress;
    public string EmpCity;
    public string EmpState;
    public string EmpPostCode;
    public string EmpTitle;
    public string EmpMobile;
    public string EmpPhone;
    public string EmpExtNo;
    public string EmpEmail;
    public string EmpPersEmail;
    public string EmpInitials;
    public string EmpInfo;
    public string UserPassword;
    public string TaxFileNumber;
    public string SuperFund;
    public string SuperFundAccount;
    public string PaysOwnSuper;
    public string EmpBSB;
    public string EmpBankAcct;
    public string Include;
    public string ActiveEmp;
    public string EmpNicName;
    public string DateOfBirth;
    public string ExecAccess;
    public string AdminAccess;
    public string DeleteAccess;
    public string ProjDispAccess;
    public string ManagerAccess;
    public string AdminPL;
    public string BillsOwingPL;
    public string BillsPaidPL;
    public string BookingsPL;
    public string CompaniesPL;
    public string ContactsPL;
    public string FollowUpPL;
    public string InvIssuedPL;
    public string InvPaidPL;
    public string MeetingsPL;
    public string ProjectsPL;
    public string StatisticsPL;
    public string StockItemsPL;
    public string StockOrdersPL;
    public string SuperTaxPL;
    public string WagesPL;
    public string WholesalePL;
    public string OnRoster;
    public string StartTime;
    public string EndTime;
    public string BreakTime;
    public string upsize_ts;
    public string userid;
    public string username;
    public string RoleName;
    public string EmpType;
    public string EmpABN;
    public string EmpAccountName;
    public string GSTPayment;
    public string LTeamOutDoor;
    public string LTeamCloser;
    public string EmpImage;
    public string EmpBio;
    public string LocationName;
    public string TeamName;
    public string FormbayInstallerId;
    public string GroupId;
    public string showexcel;
    public string EmpCatID;
}


public class ClstblEmployees
{
    public static SttblEmployees tblEmployees_SelectByEmployeeID(String EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectByEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblEmployees details = new SttblEmployees();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeStatusID = dr["EmployeeStatusID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.SalesTeamID = dr["SalesTeamID"].ToString();
            details.SalesQuotaPeriodID = dr["SalesQuotaPeriodID"].ToString();
            details.InitialSalesQuota = dr["InitialSalesQuota"].ToString();
            details.AdminStaff = dr["AdminStaff"].ToString();
            details.HireDate = dr["HireDate"].ToString();
            details.Location = dr["Location"].ToString();
            details.EmpMr = dr["EmpMr"].ToString();
            details.EmpFirst = dr["EmpFirst"].ToString();
            details.EmpLast = dr["EmpLast"].ToString();
            details.EmpAddress = dr["EmpAddress"].ToString();
            details.EmpCity = dr["EmpCity"].ToString();
            details.EmpState = dr["EmpState"].ToString();
            details.EmpPostCode = dr["EmpPostCode"].ToString();
            details.EmpTitle = dr["EmpTitle"].ToString();
            details.EmpMobile = dr["EmpMobile"].ToString();
            details.EmpEmail = dr["EmpEmail"].ToString();
            details.EmpPersEmail = dr["EmpPersEmail"].ToString();
            details.EmpInitials = dr["EmpInitials"].ToString();
            details.EmpInfo = dr["EmpInfo"].ToString();
            details.UserPassword = dr["UserPassword"].ToString();
            details.TaxFileNumber = dr["TaxFileNumber"].ToString();
            details.SuperFund = dr["SuperFund"].ToString();
            details.SuperFundAccount = dr["SuperFundAccount"].ToString();
            details.PaysOwnSuper = dr["PaysOwnSuper"].ToString();
            details.EmpBSB = dr["EmpBSB"].ToString();
            details.EmpBankAcct = dr["EmpBankAcct"].ToString();
            details.Include = dr["Include"].ToString();
            details.ActiveEmp = dr["ActiveEmp"].ToString();
            details.EmpNicName = dr["EmpNicName"].ToString();
            details.DateOfBirth = dr["DateOfBirth"].ToString();
            details.ExecAccess = dr["ExecAccess"].ToString();
            details.AdminAccess = dr["AdminAccess"].ToString();
            details.DeleteAccess = dr["DeleteAccess"].ToString();
            details.ProjDispAccess = dr["ProjDispAccess"].ToString();
            details.ManagerAccess = dr["ManagerAccess"].ToString();
            details.AdminPL = dr["AdminPL"].ToString();
            details.BillsOwingPL = dr["BillsOwingPL"].ToString();
            details.BillsPaidPL = dr["BillsPaidPL"].ToString();
            details.BookingsPL = dr["BookingsPL"].ToString();
            details.CompaniesPL = dr["CompaniesPL"].ToString();
            details.ContactsPL = dr["ContactsPL"].ToString();
            details.FollowUpPL = dr["FollowUpPL"].ToString();
            details.InvIssuedPL = dr["InvIssuedPL"].ToString();
            details.InvPaidPL = dr["InvPaidPL"].ToString();
            details.MeetingsPL = dr["MeetingsPL"].ToString();
            details.ProjectsPL = dr["ProjectsPL"].ToString();
            details.StatisticsPL = dr["StatisticsPL"].ToString();
            details.StockItemsPL = dr["StockItemsPL"].ToString();
            details.StockOrdersPL = dr["StockOrdersPL"].ToString();
            details.SuperTaxPL = dr["SuperTaxPL"].ToString();
            details.WagesPL = dr["WagesPL"].ToString();
            details.WholesalePL = dr["WholesalePL"].ToString();
            details.OnRoster = dr["OnRoster"].ToString();
            details.StartTime = dr["StartTime"].ToString();
            details.EndTime = dr["EndTime"].ToString();
            details.BreakTime = dr["BreakTime"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.userid = dr["userid"].ToString();
            details.EmpType = dr["EmpType"].ToString();
            details.EmpABN = dr["EmpABN"].ToString();
            details.EmpAccountName = dr["EmpAccountName"].ToString();
            details.GSTPayment = dr["GSTPayment"].ToString();
            details.LTeamOutDoor = dr["LTeamOutDoor"].ToString();
            details.LTeamCloser = dr["LTeamCloser"].ToString();

            details.username = dr["username"].ToString();
            details.RoleName = dr["RoleName"].ToString();

            details.EmpImage = dr["EmpImage"].ToString();
            details.EmpBio = dr["EmpBio"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.EmpPostCode = dr["EmpPostCode"].ToString();
            details.EmpState = dr["EmpState"].ToString();
            details.EmpCity = dr["EmpCity"].ToString();
            details.EmpAddress = dr["EmpAddress"].ToString();
            details.HireDate = dr["HireDate"].ToString();
            details.LocationName = dr["LocationName"].ToString();
            details.EmpType = dr["EmpType"].ToString();
            details.TeamName = dr["TeamName"].ToString();
            details.FormbayInstallerId = dr["FormbayInstallerId"].ToString();
            details.GroupId = dr["GroupId"].ToString();
            details.showexcel = dr["showexcel"].ToString();
            details.EmpPhone = dr["EmpPhoneNo"].ToString();
            details.EmpExtNo = dr["EmpPhoneExtNo"].ToString();
            details.EmpCatID = dr["Emp_CategoryID"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblEmployees tblEmployees_SelectByUserId(String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectByUserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        SttblEmployees details = new SttblEmployees();

        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeStatusID = dr["EmployeeStatusID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.SalesTeamID = dr["SalesTeamID"].ToString();
            details.SalesQuotaPeriodID = dr["SalesQuotaPeriodID"].ToString();
            details.InitialSalesQuota = dr["InitialSalesQuota"].ToString();
            details.AdminStaff = dr["AdminStaff"].ToString();
            details.HireDate = dr["HireDate"].ToString();
            details.Location = dr["Location"].ToString();
            details.EmpMr = dr["EmpMr"].ToString();
            details.EmpFirst = dr["EmpFirst"].ToString();
            details.EmpLast = dr["EmpLast"].ToString();
            details.EmpAddress = dr["EmpAddress"].ToString();
            details.EmpCity = dr["EmpCity"].ToString();
            details.EmpState = dr["EmpState"].ToString();
            details.EmpPostCode = dr["EmpPostCode"].ToString();
            details.EmpTitle = dr["EmpTitle"].ToString();
            details.EmpMobile = dr["EmpMobile"].ToString();
            details.EmpEmail = dr["EmpEmail"].ToString();
            details.EmpPersEmail = dr["EmpPersEmail"].ToString();
            details.EmpInitials = dr["EmpInitials"].ToString();
            details.EmpInfo = dr["EmpInfo"].ToString();
            details.UserPassword = dr["UserPassword"].ToString();
            details.TaxFileNumber = dr["TaxFileNumber"].ToString();
            details.SuperFund = dr["SuperFund"].ToString();
            details.SuperFundAccount = dr["SuperFundAccount"].ToString();
            details.PaysOwnSuper = dr["PaysOwnSuper"].ToString();
            details.EmpBSB = dr["EmpBSB"].ToString();
            details.EmpBankAcct = dr["EmpBankAcct"].ToString();
            details.Include = dr["Include"].ToString();
            details.ActiveEmp = dr["ActiveEmp"].ToString();
            details.EmpNicName = dr["EmpNicName"].ToString();
            details.DateOfBirth = dr["DateOfBirth"].ToString();
            details.ExecAccess = dr["ExecAccess"].ToString();
            details.AdminAccess = dr["AdminAccess"].ToString();
            details.DeleteAccess = dr["DeleteAccess"].ToString();
            details.ProjDispAccess = dr["ProjDispAccess"].ToString();
            details.ManagerAccess = dr["ManagerAccess"].ToString();
            details.AdminPL = dr["AdminPL"].ToString();
            details.BillsOwingPL = dr["BillsOwingPL"].ToString();
            details.BillsPaidPL = dr["BillsPaidPL"].ToString();
            details.BookingsPL = dr["BookingsPL"].ToString();
            details.CompaniesPL = dr["CompaniesPL"].ToString();
            details.ContactsPL = dr["ContactsPL"].ToString();
            details.FollowUpPL = dr["FollowUpPL"].ToString();
            details.InvIssuedPL = dr["InvIssuedPL"].ToString();
            details.InvPaidPL = dr["InvPaidPL"].ToString();
            details.MeetingsPL = dr["MeetingsPL"].ToString();
            details.ProjectsPL = dr["ProjectsPL"].ToString();
            details.StatisticsPL = dr["StatisticsPL"].ToString();
            details.StockItemsPL = dr["StockItemsPL"].ToString();
            details.StockOrdersPL = dr["StockOrdersPL"].ToString();
            details.SuperTaxPL = dr["SuperTaxPL"].ToString();
            details.WagesPL = dr["WagesPL"].ToString();
            details.WholesalePL = dr["WholesalePL"].ToString();
            details.OnRoster = dr["OnRoster"].ToString();
            details.StartTime = dr["StartTime"].ToString();
            details.EndTime = dr["EndTime"].ToString();
            details.BreakTime = dr["BreakTime"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.userid = dr["userid"].ToString();
            details.EmpType = dr["EmpType"].ToString();
            details.EmpABN = dr["EmpABN"].ToString();
            details.EmpAccountName = dr["EmpAccountName"].ToString();

            details.GSTPayment = dr["GSTPayment"].ToString();
            details.LTeamOutDoor = dr["LTeamOutDoor"].ToString();
            details.LTeamCloser = dr["LTeamCloser"].ToString();

            details.username = dr["username"].ToString();
            details.RoleName = dr["RoleName"].ToString();
            details.EmpImage = dr["EmpImage"].ToString();
            details.EmpPhone = dr["EmpPhoneNo"].ToString();
            details.EmpExtNo = dr["EmpPhoneExtNo"].ToString();
        }
        return details;
    }
    public static DataTable tblEmployees_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployees_SelectByUserASC(string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectByUserASC";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable SpMigrateUsers(string username)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SpMigrateUsers";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@username";
        param.Value = username;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployees_ManagerSelect(string SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_ManagerSelect";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else 
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_Select_EmpNameAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Select_EmpNameAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployees_SelectAll()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectAll";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_SelectAll_include()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectAll_include";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_Select_ByRoleName_Employee()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Select_ByRoleName_Employee";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployeeStatus_select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeStatus_select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable SpRolesGetDataByAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "SpRolesGetDataByAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int Spaspnet_UsersAddDataExist(String uname)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Spaspnet_UsersAddDataExist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@uname";
        param.Value = uname;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblEmployees_Insert(String ContactID, String EmployeeStatusID, String SalesRep, String SalesTeamID, String SalesQuotaPeriodID, String InitialSalesQuota, String AdminStaff, String HireDate, String Location, String EmpMr, String EmpFirst, String EmpLast, String EmpAddress, String EmpCity, String EmpState, String EmpPostCode,  String EmpMobile, String EmpEmail, String EmpPersEmail, String EmpInitials, String EmpInfo, String UserPassword, String TaxFileNumber, String SuperFund, String SuperFundAccount, String PaysOwnSuper, String EmpBSB, String EmpBankAcct, String Include, String ActiveEmp, String EmpNicName, String DateOfBirth, String ExecAccess, String AdminAccess, String DeleteAccess, String ProjDispAccess, String ManagerAccess, String AdminPL, String BillsOwingPL, String BillsPaidPL, String BookingsPL, String CompaniesPL, String ContactsPL, String FollowUpPL, String InvIssuedPL, String InvPaidPL, String MeetingsPL, String ProjectsPL, String StatisticsPL, String StockItemsPL, String StockOrdersPL, String SuperTaxPL, String WagesPL, String WholesalePL, String OnRoster, String StartTime, String EndTime, String BreakTime, String upsize_ts, string EmpABN, string EmpAccountName, string GSTPayment)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeStatusID";
        if (EmployeeStatusID != string.Empty)
            param.Value = EmployeeStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesQuotaPeriodID";
        if (SalesQuotaPeriodID != string.Empty)
            param.Value = SalesQuotaPeriodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InitialSalesQuota";
        param.Value = InitialSalesQuota;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminStaff";
        if (AdminStaff != string.Empty)
            param.Value = AdminStaff;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HireDate";
        if (HireDate != string.Empty)
            param.Value = HireDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMr";
        param.Value = EmpMr;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpFirst";
        param.Value = EmpFirst;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpLast";
        param.Value = EmpLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpAddress";
        param.Value = EmpAddress;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpCity";
        param.Value = EmpCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpState";
        param.Value = EmpState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPostCode";
        param.Value = EmpPostCode;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMobile";
        param.Value = EmpMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpEmail";
        param.Value = EmpEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPersEmail";
        param.Value = EmpPersEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInitials";
        param.Value = EmpInitials;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInfo";
        param.Value = EmpInfo;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserPassword";
        param.Value = UserPassword;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TaxFileNumber";
        param.Value = TaxFileNumber;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFund";
        param.Value = SuperFund;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFundAccount";
        param.Value = SuperFundAccount;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaysOwnSuper";
        if (PaysOwnSuper != string.Empty)
            param.Value = PaysOwnSuper;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBSB";
        param.Value = EmpBSB;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBankAcct";
        param.Value = EmpBankAcct;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Include";
        if (Include != string.Empty)
            param.Value = Include;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveEmp";
        if (ActiveEmp != string.Empty)
            param.Value = ActiveEmp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpNicName";
        param.Value = EmpNicName;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateOfBirth";
        if (DateOfBirth != string.Empty)
            param.Value = DateOfBirth;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExecAccess";
        if (ExecAccess != string.Empty)
            param.Value = ExecAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminAccess";
        if (AdminAccess != string.Empty)
            param.Value = AdminAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleteAccess";
        if (DeleteAccess != string.Empty)
            param.Value = DeleteAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjDispAccess";
        if (ProjDispAccess != string.Empty)
            param.Value = ProjDispAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManagerAccess";
        if (ManagerAccess != string.Empty)
            param.Value = ManagerAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminPL";
        if (AdminPL != string.Empty)
            param.Value = AdminPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsOwingPL";
        if (BillsOwingPL != string.Empty)
            param.Value = BillsOwingPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsPaidPL";
        if (BillsPaidPL != string.Empty)
            param.Value = BillsPaidPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BookingsPL";
        if (BookingsPL != string.Empty)
            param.Value = BookingsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompaniesPL";
        if (CompaniesPL != string.Empty)
            param.Value = CompaniesPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactsPL";
        if (ContactsPL != string.Empty)
            param.Value = ContactsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUpPL";
        if (FollowUpPL != string.Empty)
            param.Value = FollowUpPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvIssuedPL";
        if (InvIssuedPL != string.Empty)
            param.Value = InvIssuedPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvPaidPL";
        if (InvPaidPL != string.Empty)
            param.Value = InvPaidPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeetingsPL";
        if (MeetingsPL != string.Empty)
            param.Value = MeetingsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectsPL";
        param.Value = ProjectsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatisticsPL";
        if (StatisticsPL != string.Empty)
            param.Value = StatisticsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemsPL";
        if (StockItemsPL != string.Empty)
            param.Value = StockItemsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrdersPL";
        if (StockOrdersPL != string.Empty)
            param.Value = StockOrdersPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperTaxPL";
        if (SuperTaxPL != string.Empty)
            param.Value = SuperTaxPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WagesPL";
        if (WagesPL != string.Empty)
            param.Value = WagesPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesalePL";
        if (WholesalePL != string.Empty)
            param.Value = WholesalePL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OnRoster";
        if (OnRoster != string.Empty)
            param.Value = OnRoster;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartTime";
        if (StartTime != string.Empty)
            param.Value = StartTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndTime";
        if (EndTime != string.Empty)
            param.Value = EndTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BreakTime";
        if (BreakTime != string.Empty)
            param.Value = BreakTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpABN";
        if (EmpABN != string.Empty)
            param.Value = EmpABN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpAccountName";
        if (EmpAccountName != string.Empty)
            param.Value = EmpAccountName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GSTPayment";
        param.Value = GSTPayment;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblEmployees_Update(string EmployeeID, String ContactID, String EmployeeStatusID, String SalesRep, String SalesTeamID, String SalesQuotaPeriodID, String InitialSalesQuota, String AdminStaff, String HireDate, String Location, String EmpMr, String EmpFirst, String EmpLast, String EmpAddress, String EmpCity, String EmpState, String EmpPostCode,  String EmpMobile, String EmpEmail, String EmpPersEmail, String EmpInitials, String EmpInfo, String UserPassword, String TaxFileNumber, String SuperFund, String SuperFundAccount, String PaysOwnSuper, String EmpBSB, String EmpBankAcct, String Include, String ActiveEmp, String EmpNicName, String DateOfBirth, String ExecAccess, String AdminAccess, String DeleteAccess, String ProjDispAccess, String ManagerAccess, String AdminPL, String BillsOwingPL, String BillsPaidPL, String BookingsPL, String CompaniesPL, String ContactsPL, String FollowUpPL, String InvIssuedPL, String InvPaidPL, String MeetingsPL, String ProjectsPL, String StatisticsPL, String StockItemsPL, String StockOrdersPL, String SuperTaxPL, String WagesPL, String WholesalePL, String OnRoster, String StartTime, String EndTime, String BreakTime, String upsize_ts, string EmpABN, string EmpAccountName, string GSTPayment)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeStatusID";
        if (EmployeeStatusID != string.Empty)
            param.Value = EmployeeStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesQuotaPeriodID";
        if (SalesQuotaPeriodID != string.Empty)
            param.Value = SalesQuotaPeriodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InitialSalesQuota";
        param.Value = InitialSalesQuota;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminStaff";
        if (AdminStaff != string.Empty)
            param.Value = AdminStaff;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HireDate";
        if (HireDate != string.Empty)
            param.Value = HireDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMr";
        param.Value = EmpMr;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpFirst";
        param.Value = EmpFirst;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpLast";
        param.Value = EmpLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpAddress";
        param.Value = EmpAddress;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpCity";
        param.Value = EmpCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpState";
        param.Value = EmpState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPostCode";
        param.Value = EmpPostCode;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

       

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMobile";
        param.Value = EmpMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpEmail";
        param.Value = EmpEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPersEmail";
        param.Value = EmpPersEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInitials";
        param.Value = EmpInitials;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInfo";
        param.Value = EmpInfo;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserPassword";
        param.Value = UserPassword;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TaxFileNumber";
        param.Value = TaxFileNumber;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFund";
        param.Value = SuperFund;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFundAccount";
        param.Value = SuperFundAccount;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaysOwnSuper";
        if (PaysOwnSuper != string.Empty)
            param.Value = PaysOwnSuper;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBSB";
        param.Value = EmpBSB;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBankAcct";
        param.Value = EmpBankAcct;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Include";
        if (Include != string.Empty)
            param.Value = Include;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveEmp";
        if (ActiveEmp != string.Empty)
            param.Value = ActiveEmp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpNicName";
        param.Value = EmpNicName;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateOfBirth";
        if (DateOfBirth != string.Empty)
            param.Value = DateOfBirth;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExecAccess";
        if (ExecAccess != string.Empty)
            param.Value = ExecAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminAccess";
        if (AdminAccess != string.Empty)
            param.Value = AdminAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleteAccess";
        if (DeleteAccess != string.Empty)
            param.Value = DeleteAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjDispAccess";
        if (ProjDispAccess != string.Empty)
            param.Value = ProjDispAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManagerAccess";
        if (ManagerAccess != string.Empty)
            param.Value = ManagerAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminPL";
        if (AdminPL != string.Empty)
            param.Value = AdminPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsOwingPL";
        if (BillsOwingPL != string.Empty)
            param.Value = BillsOwingPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsPaidPL";
        if (BillsPaidPL != string.Empty)
            param.Value = BillsPaidPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BookingsPL";
        if (BookingsPL != string.Empty)
            param.Value = BookingsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompaniesPL";
        if (CompaniesPL != string.Empty)
            param.Value = CompaniesPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactsPL";
        if (ContactsPL != string.Empty)
            param.Value = ContactsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUpPL";
        if (FollowUpPL != string.Empty)
            param.Value = FollowUpPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvIssuedPL";
        if (InvIssuedPL != string.Empty)
            param.Value = InvIssuedPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvPaidPL";
        if (InvPaidPL != string.Empty)
            param.Value = InvPaidPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeetingsPL";
        if (MeetingsPL != string.Empty)
            param.Value = MeetingsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectsPL";
        param.Value = ProjectsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatisticsPL";
        if (StatisticsPL != string.Empty)
            param.Value = StatisticsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemsPL";
        if (StockItemsPL != string.Empty)
            param.Value = StockItemsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrdersPL";
        if (StockOrdersPL != string.Empty)
            param.Value = StockOrdersPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperTaxPL";
        if (SuperTaxPL != string.Empty)
            param.Value = SuperTaxPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WagesPL";
        if (WagesPL != string.Empty)
            param.Value = WagesPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesalePL";
        if (WholesalePL != string.Empty)
            param.Value = WholesalePL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OnRoster";
        if (OnRoster != string.Empty)
            param.Value = OnRoster;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartTime";
        if (StartTime != string.Empty)
            param.Value = StartTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndTime";
        if (EndTime != string.Empty)
            param.Value = EndTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BreakTime";
        if (BreakTime != string.Empty)
            param.Value = BreakTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpABN";
        if (EmpABN != string.Empty)
            param.Value = EmpABN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpAccountName";
        if (EmpAccountName != string.Empty)
            param.Value = EmpAccountName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GSTPayment";
        param.Value = GSTPayment;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblEmployees_UpdateDetails(string EmployeeID, String EmployeeStatusID, String HireDate, String Location, String EmpFirst, String EmpLast, String EmpTitle, String EmpMobile, String EmpEmail, String EmpInitials, String EmpInfo, String PaysOwnSuper, String Include, String ActiveEmp, String EmpNicName, String OnRoster, String StartTime, String EndTime, String BreakTime)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeStatusID";
        if (EmployeeStatusID != string.Empty)
            param.Value = EmployeeStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HireDate";
        if (HireDate != string.Empty)
            param.Value = HireDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpFirst";
        param.Value = EmpFirst;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpLast";
        param.Value = EmpLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpTitle";
        param.Value = EmpTitle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMobile";
        param.Value = EmpMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpEmail";
        param.Value = EmpEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInitials";
        param.Value = EmpInitials;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInfo";
        param.Value = EmpInfo;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaysOwnSuper";
        if (PaysOwnSuper != string.Empty)
            param.Value = PaysOwnSuper;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Include";
        if (Include != string.Empty)
            param.Value = Include;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveEmp";
        if (ActiveEmp != string.Empty)
            param.Value = ActiveEmp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpNicName";
        param.Value = EmpNicName;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OnRoster";
        if (OnRoster != string.Empty)
            param.Value = OnRoster;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartTime";
        if (StartTime != string.Empty)
            param.Value = StartTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndTime";
        if (EndTime != string.Empty)
            param.Value = EndTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BreakTime";
        if (BreakTime != string.Empty)
            param.Value = BreakTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblEmployees_UpdatePermissions(string EmployeeID, String AdminPL, String BookingsPL, String CompaniesPL, String ContactsPL, String FollowUpPL, String InvIssuedPL, String InvPaidPL, String ProjectsPL, String StockItemsPL, String StockOrdersPL, String SuperTaxPL, String WagesPL, String WholesalePL, String BillsOwingPL, String BillsPaidPL, String ExecAccess, String DeleteAccess, String AdminAccess, String ProjDispAccess, String ManagerAccess)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdatePermissions";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExecAccess";
        if (ExecAccess != string.Empty)
            param.Value = ExecAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminAccess";
        if (AdminAccess != string.Empty)
            param.Value = AdminAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleteAccess";
        if (DeleteAccess != string.Empty)
            param.Value = DeleteAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjDispAccess";
        if (ProjDispAccess != string.Empty)
            param.Value = ProjDispAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManagerAccess";
        if (ManagerAccess != string.Empty)
            param.Value = ManagerAccess;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminPL";
        if (AdminPL != string.Empty)
            param.Value = AdminPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsOwingPL";
        if (BillsOwingPL != string.Empty)
            param.Value = BillsOwingPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsPaidPL";
        if (BillsPaidPL != string.Empty)
            param.Value = BillsPaidPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BookingsPL";
        if (BookingsPL != string.Empty)
            param.Value = BookingsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompaniesPL";
        if (CompaniesPL != string.Empty)
            param.Value = CompaniesPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactsPL";
        if (ContactsPL != string.Empty)
            param.Value = ContactsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUpPL";
        if (FollowUpPL != string.Empty)
            param.Value = FollowUpPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvIssuedPL";
        if (InvIssuedPL != string.Empty)
            param.Value = InvIssuedPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvPaidPL";
        if (InvPaidPL != string.Empty)
            param.Value = InvPaidPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectsPL";
        param.Value = ProjectsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemsPL";
        if (StockItemsPL != string.Empty)
            param.Value = StockItemsPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrdersPL";
        if (StockOrdersPL != string.Empty)
            param.Value = StockOrdersPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperTaxPL";
        if (SuperTaxPL != string.Empty)
            param.Value = SuperTaxPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WagesPL";
        if (WagesPL != string.Empty)
            param.Value = WagesPL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesalePL";
        if (WholesalePL != string.Empty)
            param.Value = WholesalePL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblEmployees_UpdateReferences(string EmployeeID, String SalesRep, String InitialSalesQuota, String AdminStaff, String TaxFileNumber, String SuperFund, String SuperFundAccount, String EmpBSB, String EmpBankAcct, String EmpAddress, String EmpCity, String EmpState, String EmpPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateReferences";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InitialSalesQuota";
        param.Value = InitialSalesQuota;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminStaff";
        if (AdminStaff != string.Empty)
            param.Value = AdminStaff;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpAddress";
        param.Value = EmpAddress;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpCity";
        param.Value = EmpCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpState";
        param.Value = EmpState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPostCode";
        param.Value = EmpPostCode;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TaxFileNumber";
        param.Value = TaxFileNumber;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFund";
        param.Value = SuperFund;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFundAccount";
        param.Value = SuperFundAccount;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBSB";
        param.Value = EmpBSB;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBankAcct";
        param.Value = EmpBankAcct;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static string tblEmployees_InsertUpdate(Int32 EmployeeID, String ContactID, String EmployeeStatusID, String SalesRep, String SalesTeamID, String SalesQuotaPeriodID, String InitialSalesQuota, String AdminStaff, String HireDate, String Location, String EmpMr, String EmpFirst, String EmpLast, String EmpAddress, String EmpCity, String EmpState, String EmpPostCode, String EmpTitle, String EmpMobile, String EmpEmail, String EmpPersEmail, String EmpInitials, String EmpInfo, String UserPassword, String TaxFileNumber, String SuperFund, String SuperFundAccount, String PaysOwnSuper, String EmpBSB, String EmpBankAcct, String Include, String ActiveEmp, String EmpNicName, String DateOfBirth, String ExecAccess, String AdminAccess, String DeleteAccess, String ProjDispAccess, String ManagerAccess, String AdminPL, String BillsOwingPL, String BillsPaidPL, String BookingsPL, String CompaniesPL, String ContactsPL, String FollowUpPL, String InvIssuedPL, String InvPaidPL, String MeetingsPL, String ProjectsPL, String StatisticsPL, String StockItemsPL, String StockOrdersPL, String SuperTaxPL, String WagesPL, String WholesalePL, String OnRoster, String StartTime, String EndTime, String BreakTime, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeStatusID";
        param.Value = EmployeeStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        param.Value = SalesRep;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesQuotaPeriodID";
        param.Value = SalesQuotaPeriodID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InitialSalesQuota";
        param.Value = InitialSalesQuota;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminStaff";
        param.Value = AdminStaff;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HireDate";
        param.Value = HireDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        param.Value = Location;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMr";
        param.Value = EmpMr;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpFirst";
        param.Value = EmpFirst;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpLast";
        param.Value = EmpLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpAddress";
        param.Value = EmpAddress;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpCity";
        param.Value = EmpCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpState";
        param.Value = EmpState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPostCode";
        param.Value = EmpPostCode;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpTitle";
        param.Value = EmpTitle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMobile";
        param.Value = EmpMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpEmail";
        param.Value = EmpEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPersEmail";
        param.Value = EmpPersEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInitials";
        param.Value = EmpInitials;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInfo";
        param.Value = EmpInfo;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserPassword";
        param.Value = UserPassword;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TaxFileNumber";
        param.Value = TaxFileNumber;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFund";
        param.Value = SuperFund;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperFundAccount";
        param.Value = SuperFundAccount;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaysOwnSuper";
        param.Value = PaysOwnSuper;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBSB";
        param.Value = EmpBSB;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBankAcct";
        param.Value = EmpBankAcct;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Include";
        param.Value = Include;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveEmp";
        param.Value = ActiveEmp;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpNicName";
        param.Value = EmpNicName;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateOfBirth";
        param.Value = DateOfBirth;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExecAccess";
        param.Value = ExecAccess;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminAccess";
        param.Value = AdminAccess;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleteAccess";
        param.Value = DeleteAccess;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjDispAccess";
        param.Value = ProjDispAccess;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManagerAccess";
        param.Value = ManagerAccess;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdminPL";
        param.Value = AdminPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsOwingPL";
        param.Value = BillsOwingPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BillsPaidPL";
        param.Value = BillsPaidPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BookingsPL";
        param.Value = BookingsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompaniesPL";
        param.Value = CompaniesPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactsPL";
        param.Value = ContactsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUpPL";
        param.Value = FollowUpPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvIssuedPL";
        param.Value = InvIssuedPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvPaidPL";
        param.Value = InvPaidPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeetingsPL";
        param.Value = MeetingsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectsPL";
        param.Value = ProjectsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatisticsPL";
        param.Value = StatisticsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemsPL";
        param.Value = StockItemsPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrdersPL";
        param.Value = StockOrdersPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SuperTaxPL";
        param.Value = SuperTaxPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WagesPL";
        param.Value = WagesPL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesalePL";
        param.Value = WholesalePL;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OnRoster";
        param.Value = OnRoster;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StartTime";
        param.Value = StartTime;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EndTime";
        param.Value = EndTime;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BreakTime";
        param.Value = BreakTime;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        param.Value = upsize_ts;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblEmployees_Delete(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblEmployees_Update_Userid(string EmployeeID, string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Update_Userid";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblEmployees_Update_Team(string EmployeeID, string SalesTeamID, string EmpType, string LTeamOutDoor, string LTeamCloser)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Update_Team";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpType";
        if (EmpType != string.Empty)
            param.Value = EmpType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LTeamOutDoor";
        param.Value = LTeamOutDoor;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LTeamCloser";
        param.Value = LTeamCloser;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }



    public static bool tblEmployees_Update_Phone(string EmployeeID,string EmpPhoneNo, string EmpPhoneExtNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Update_Phone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPhoneNo";
        if (EmpPhoneNo != string.Empty)
            param.Value = EmpPhoneNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPhoneExtNo";
        if (EmpPhoneExtNo != string.Empty)
            param.Value = EmpPhoneExtNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

       

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_employee_emptitle(string EmployeeID, string EmpTitle)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_employee_emptitle";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpTitle";
        param.Value = EmpTitle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
      


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblEmployeesGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeesGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblEmployeesGetDataBySearch(string alpha, string rolename, string SalesTeamID, String username)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeesGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@rolename";
        if (rolename != string.Empty)
            param.Value = rolename;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@username";
        if (username != string.Empty)
            param.Value = username;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@startindex";
        //if (startindex != string.Empty)
        //    param.Value = startindex;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@pageindex";
        //if (pageindex != string.Empty)
        //    param.Value = pageindex;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable aspnet_Users_SelectByUserId(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnet_Users_SelectByUserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCompanyLocations_SelectAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCompanyLocations_SelectAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblEmpTitle_InsertRole(string EmployeeID, string RoleName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_InsertRole";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoleName";
        param.Value = RoleName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;

    }

    public static bool tblEmpTitle_DeleteRole(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_DeleteRole";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;	// result will represent the number of changed rows
        result = DataAccess.ExecuteNonQuery(comm);	// execute the stored procedure
        return (result != -1);	// result will be 1 in case of success
    }

    public static DataTable tblEmpTitle_Select(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);

    }
    public static DataTable get_EmpCategory( )
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "get_EmpCategory";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@EmployeeID";
        //param.Value = EmployeeID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable AdminUserRoleMasterGetData(string userid)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "AdminUserRoleMasterGetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static bool tblEmployees_UpdatePassword(string UserId, string Password)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdatePassword";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Password";
        param.Value = Password;
        param.DbType = DbType.String;
        param.Size = 128;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblEmployees_TeamExist(string SalesTeamID, String EmpType, String RoleName)
    {
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "tblEmployees_TeamExist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpType";
        param.Value = EmpType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoleName";
        param.Value = RoleName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;

    }

    public static int tblEmployees_TeamExistByID(string SalesTeamID, String EmpType, String RoleName, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "tblEmployees_TeamExistByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpType";
        param.Value = EmpType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoleName";
        param.Value = RoleName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool aspnetUsers_Update_LastActivityDate(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnetUsers_Update_LastActivityDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool aspnetUsers_Lock(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnetUsers_Lock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool aspnetUsers_UnLock(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnetUsers_UnLock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblEmployees_SelectLeft()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectLeft";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_SelectOutDoor(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectOutDoor";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_SelectCloser(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectCloser";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblEmployees_UpdateProfileDetails(string userid, String EmpFirst, String EmpLast, String EmpTitle, String EmpInitials, String EmpMobile, String UserPassword, String EmpNicName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateProfileDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpFirst";
        param.Value = EmpFirst;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpLast";
        param.Value = EmpLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpTitle";
        param.Value = EmpTitle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpInitials";
        param.Value = EmpInitials;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpMobile";
        param.Value = EmpMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserPassword";
        param.Value = UserPassword;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpNicName";
        param.Value = EmpNicName;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool aspnetmembership_Update_Password(string userid, string UserPassword)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnetmembership_Update_Password";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserPassword";
        param.Value = UserPassword;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static SttblEmployees tblEmployees_SelectByUserID(String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectByUserID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblEmployees details = new SttblEmployees();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.userid = dr["userid"].ToString();

            details.EmpFirst = dr["EmpFirst"].ToString();
            details.EmpLast = dr["EmpLast"].ToString();

            details.EmpTitle = dr["EmpTitle"].ToString();
            details.EmpMobile = dr["EmpMobile"].ToString();
            details.EmpEmail = dr["EmpEmail"].ToString();

            details.EmpInitials = dr["EmpInitials"].ToString();

            details.UserPassword = dr["UserPassword"].ToString();
            details.EmpNicName = dr["EmpNicName"].ToString();
            details.username = dr["username"].ToString();
            details.RoleName = dr["RoleName"].ToString();
            details.EmpImage = dr["EmpImage"].ToString();
            details.EmpBio = dr["EmpBio"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.EmpPostCode = dr["EmpPostCode"].ToString();
            details.EmpState = dr["EmpState"].ToString();
            details.EmpCity = dr["EmpCity"].ToString();
            details.EmpAddress = dr["EmpAddress"].ToString();
            details.HireDate = dr["HireDate"].ToString();
            details.LocationName = dr["LocationName"].ToString();
            details.EmpType = dr["EmpType"].ToString();
            details.TeamName = dr["TeamName"].ToString();
            details.showexcel = dr["showexcel"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblEmployees_SelectbySalesTeamID(string SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectbySalesTeamID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_SearchContacts(string SalesTeamID, string name)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SearchContacts";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@name";
        if (name != string.Empty)
            param.Value = name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_SearchContactsbyTeam(string SalesTeamID, string name, string type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SearchContactsbyTeam";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@name";
        if (name != string.Empty)
            param.Value = name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblEmployees_UpdateImage(string EmployeeID, string EmpImage)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateImage";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpImage";
        param.Value = EmpImage;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblEmployees_UpdateEmpBio(string EmployeeID, string EmpBio)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateEmpBio";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpBio";
        param.Value = EmpBio;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblEmployees_UpdateEmpAddressDetails(string EmployeeID, string EmpAddress, string EmpCity, string EmpState, string EmpPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateEmpAddressDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpAddress";
        param.Value = EmpAddress;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpCity";
        param.Value = EmpCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpState";
        param.Value = EmpState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpPostCode";
        param.Value = EmpPostCode;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblEmployeesGetDataByInstallerRole()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeesGetDataByInstallerRole";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblEmployees_UpdateFormbayId(string EmployeeID, string FormbayInstallerId, string GroupId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateFormbayId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FormbayInstallerId";
        param.Value = FormbayInstallerId;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GroupId";
        param.Value = GroupId;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblEmployees_Update_ShowExcel(string EmployeeID, string showexcel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_Update_ShowExcel";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@showexcel";
        param.Value = showexcel;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }


    public static DataTable tblEmployees_GetByEmployeeID(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_GetByEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_GetSalesManager(string SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_GetSalesManager";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_employeee_data()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_employeee_data";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_emp_roles(string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_emp_roles";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tbl_employee_insertuserid(string EmployeeID, String UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_employee_insertuserid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@empid";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

      

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblEmployees_SelectAll_userselect(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectAll_userselect";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_SelectASC_Include()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectASC_Include";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblEmployees_UpdateProfile(string Profile, string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateProfile";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Profile";
        param.Value = Profile;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static SttblEmployees Get_SolarMiner_tblEmployees_SelectByEmployeeID(String EmployeeID)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblEmployees_SelectByEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblEmployees details = new SttblEmployees();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeStatusID = dr["EmployeeStatusID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.SalesTeamID = dr["SalesTeamID"].ToString();
            details.SalesQuotaPeriodID = dr["SalesQuotaPeriodID"].ToString();
            details.InitialSalesQuota = dr["InitialSalesQuota"].ToString();
            details.AdminStaff = dr["AdminStaff"].ToString();
            details.HireDate = dr["HireDate"].ToString();
            details.Location = dr["Location"].ToString();
            details.EmpMr = dr["EmpMr"].ToString();
            details.EmpFirst = dr["EmpFirst"].ToString();
            details.EmpLast = dr["EmpLast"].ToString();
            details.EmpAddress = dr["EmpAddress"].ToString();
            details.EmpCity = dr["EmpCity"].ToString();
            details.EmpState = dr["EmpState"].ToString();
            details.EmpPostCode = dr["EmpPostCode"].ToString();
            details.EmpTitle = dr["EmpTitle"].ToString();
            details.EmpMobile = dr["EmpMobile"].ToString();
            details.EmpEmail = dr["EmpEmail"].ToString();
            details.EmpPersEmail = dr["EmpPersEmail"].ToString();
            details.EmpInitials = dr["EmpInitials"].ToString();
            details.EmpInfo = dr["EmpInfo"].ToString();
            details.UserPassword = dr["UserPassword"].ToString();
            details.TaxFileNumber = dr["TaxFileNumber"].ToString();
            details.SuperFund = dr["SuperFund"].ToString();
            details.SuperFundAccount = dr["SuperFundAccount"].ToString();
            details.PaysOwnSuper = dr["PaysOwnSuper"].ToString();
            details.EmpBSB = dr["EmpBSB"].ToString();
            details.EmpBankAcct = dr["EmpBankAcct"].ToString();
            details.Include = dr["Include"].ToString();
            details.ActiveEmp = dr["ActiveEmp"].ToString();
            details.EmpNicName = dr["EmpNicName"].ToString();
            details.DateOfBirth = dr["DateOfBirth"].ToString();
            details.ExecAccess = dr["ExecAccess"].ToString();
            details.AdminAccess = dr["AdminAccess"].ToString();
            details.DeleteAccess = dr["DeleteAccess"].ToString();
            details.ProjDispAccess = dr["ProjDispAccess"].ToString();
            details.ManagerAccess = dr["ManagerAccess"].ToString();
            details.AdminPL = dr["AdminPL"].ToString();
            details.BillsOwingPL = dr["BillsOwingPL"].ToString();
            details.BillsPaidPL = dr["BillsPaidPL"].ToString();
            details.BookingsPL = dr["BookingsPL"].ToString();
            details.CompaniesPL = dr["CompaniesPL"].ToString();
            details.ContactsPL = dr["ContactsPL"].ToString();
            details.FollowUpPL = dr["FollowUpPL"].ToString();
            details.InvIssuedPL = dr["InvIssuedPL"].ToString();
            details.InvPaidPL = dr["InvPaidPL"].ToString();
            details.MeetingsPL = dr["MeetingsPL"].ToString();
            details.ProjectsPL = dr["ProjectsPL"].ToString();
            details.StatisticsPL = dr["StatisticsPL"].ToString();
            details.StockItemsPL = dr["StockItemsPL"].ToString();
            details.StockOrdersPL = dr["StockOrdersPL"].ToString();
            details.SuperTaxPL = dr["SuperTaxPL"].ToString();
            details.WagesPL = dr["WagesPL"].ToString();
            details.WholesalePL = dr["WholesalePL"].ToString();
            details.OnRoster = dr["OnRoster"].ToString();
            details.StartTime = dr["StartTime"].ToString();
            details.EndTime = dr["EndTime"].ToString();
            details.BreakTime = dr["BreakTime"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.userid = dr["userid"].ToString();
            details.EmpType = dr["EmpType"].ToString();
            details.EmpABN = dr["EmpABN"].ToString();
            details.EmpAccountName = dr["EmpAccountName"].ToString();
            details.GSTPayment = dr["GSTPayment"].ToString();
            details.LTeamOutDoor = dr["LTeamOutDoor"].ToString();
            details.LTeamCloser = dr["LTeamCloser"].ToString();

            details.username = dr["username"].ToString();
            details.RoleName = dr["RoleName"].ToString();

            details.EmpImage = dr["EmpImage"].ToString();
            details.EmpBio = dr["EmpBio"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.EmpPostCode = dr["EmpPostCode"].ToString();
            details.EmpState = dr["EmpState"].ToString();
            details.EmpCity = dr["EmpCity"].ToString();
            details.EmpAddress = dr["EmpAddress"].ToString();
            details.HireDate = dr["HireDate"].ToString();
            details.LocationName = dr["LocationName"].ToString();
            details.EmpType = dr["EmpType"].ToString();
            details.TeamName = dr["TeamName"].ToString();
            details.FormbayInstallerId = dr["FormbayInstallerId"].ToString();
            details.GroupId = dr["GroupId"].ToString();
            details.showexcel = dr["showexcel"].ToString();
            details.EmpPhone = dr["EmpPhoneNo"].ToString();
            details.EmpExtNo = dr["EmpPhoneExtNo"].ToString();
        }
        // return structure details
        return details;
    }
    public static bool tblEmployees_UpdateEmpCatId(string EmployeeID, string CatId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_UpdateEmpCatId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@empcatID";
        if (CatId != string.Empty)
            param.Value = CatId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@empId";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}
