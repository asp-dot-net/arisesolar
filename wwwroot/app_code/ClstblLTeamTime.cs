using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblLTeamTime
{
    public string Time;
    public string Active;

}


public class ClstblLTeamTime
{
    public static SttblLTeamTime tblLTeamTime_SelectByID(String ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblLTeamTime details = new SttblLTeamTime();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Time = dr["Time"].ToString();
            details.Active = dr["Active"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblLTeamTime_Select(string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblLTeamTime_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblLTeamTime_SelectByEmp(string D2DEmployee)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_SelectByEmp";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@D2DEmployee";
        if (D2DEmployee != string.Empty)
            param.Value = D2DEmployee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblLTeamTime_SelectByEmp_Edit(string D2DEmployee, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_SelectByEmp_Edit";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@D2DEmployee";
        param.Value = D2DEmployee;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblLTeamTime_Insert(String Time, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Time";
        param.Value = Time;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblLTeamTime_Update(string ID, String Time, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Time";
        param.Value = Time;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblLTeamTime_InsertUpdate(Int32 ID, String Time, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Time";
        param.Value = Time;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblLTeamTime_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLTeamTime_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}