using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct Sttbl_SignatureLog
{
    public string ID;
    public string Customer_Name;
    public string Signed_Date;
    public string Latitude_Location;
    public string Longitude_Location;
    public string IP_Address;
    public string Customer_Email;
    public string Token;
    public string ProjectID;
    public string Image_Address;
    public string QDocNo;
}


public class Clstbl_SignatureLog
{

    public static Sttbl_SignatureLog tbl_SignatureLog_SelectByID(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SignatureLog_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_SignatureLog details = new Sttbl_SignatureLog();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Customer_Name = dr["Customer_Name"].ToString();
            details.Signed_Date = dr["Signed_Date"].ToString();
            details.Latitude_Location = dr["Latitude_Location"].ToString();
            details.Longitude_Location = dr["Longitude_Location"].ToString();
            details.IP_Address = dr["IP_Address"].ToString();
            details.Customer_Email = dr["Customer_Email"].ToString();
            details.Token = dr["Token"].ToString();
            details.ID = dr["ID"].ToString();
            details.Image_Address = dr["Image_Address"].ToString();
            details.QDocNo = dr["QDocNo"].ToString();
        }
        // return structure details
        return details;
    }

    public static Sttbl_SignatureLog tbl_SignatureLog_SelectByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SignatureLog_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_SignatureLog details = new Sttbl_SignatureLog();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Customer_Name = dr["Customer_Name"].ToString();
            details.Signed_Date = dr["Signed_Date"].ToString();
            details.Latitude_Location = dr["Latitude_Location"].ToString();
            details.Longitude_Location = dr["Longitude_Location"].ToString();
            details.IP_Address = dr["IP_Address"].ToString();
            details.Customer_Email = dr["Customer_Email"].ToString();
            details.Token = dr["Token"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.Image_Address = dr["Image_Address"].ToString();
            details.QDocNo = dr["QDocNo"].ToString();
        }
        // return structure details
        return details;
    }

    public static Sttbl_SignatureLog tbl_SignatureLog_SelectByProjectID_QDocNo(string ProjectID,String QDocNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SignatureLog_SelectByProjectID_QDocNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QDocNo";
        param.Value = QDocNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_SignatureLog details = new Sttbl_SignatureLog();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Customer_Name = dr["Customer_Name"].ToString();
            details.Signed_Date = dr["Signed_Date"].ToString();
            details.Latitude_Location = dr["Latitude_Location"].ToString();
            details.Longitude_Location = dr["Longitude_Location"].ToString();
            details.IP_Address = dr["IP_Address"].ToString();
            details.Customer_Email = dr["Customer_Email"].ToString();
            details.Token = dr["Token"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.Image_Address = dr["Image_Address"].ToString();
            details.QDocNo = dr["QDocNo"].ToString();
        }
        // return structure details
        return details;
    }

    //public static Sttbl_SignatureLog tbl_TokenData_SelectByToken_ProjectID(string Token,String ProjectID)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tbl_TokenData_SelectByToken_ProjectID";

    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@Token";
    //    param.Value = Token;
    //    param.DbType = DbType.String;
    //    param.Size = 50;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@ProjectID";
    //    param.Value = ProjectID;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);
    //    // execute the stored procedure
    //    DataTable table = DataAccess.ExecuteSelectCommand(comm);
    //    // wrap retrieved data into a ProductDetails object
    //    Sttbl_TokenData details = new Sttbl_TokenData();
    //    if (table.Rows.Count > 0)
    //    {
    //        // get the first table row
    //        DataRow dr = table.Rows[0];
    //        // get product details
    //        details.Id = dr["Id"].ToString();
    //        details.TokenDate = dr["TokenDate"].ToString();
    //        details.ProjectID = dr["ProjectID"].ToString();
    //        details.ProjectNumber = dr["ProjectNumber"].ToString();
    //        details.IsExpired = dr["IsExpired"].ToString();
    //        details.IsOwner = dr["IsOwner"].ToString();


    //    }
    //    // return structure details
    //    return details;
    //}

    public static DataTable tblProjects_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_tbl_SignatureLog_Insert(String Customer_Name, String Signed_Date, String Latitude_Location, String Longitude_Location, String IP_Address, String Customer_Email, String Token, String ProjectID, String Image_Address,string QDocNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_tbl_SignatureLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Customer_Name";
        if (Customer_Name != string.Empty)
            param.Value = Customer_Name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Signed_Date";
        if (Signed_Date != string.Empty)
            param.Value = Signed_Date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Latitude_Location";
        if (Latitude_Location != string.Empty)
            param.Value = Latitude_Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Longitude_Location";
        if (Longitude_Location != string.Empty)
            param.Value = Longitude_Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IP_Address";
        if (IP_Address != string.Empty)
            param.Value = IP_Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer_Email";
        if (Customer_Email != string.Empty)
            param.Value = Customer_Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Token";
        if (Token != string.Empty)
            param.Value = Token;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Image_Address";
        if (Image_Address != string.Empty)
            param.Value = Image_Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QDocNo";
        if (QDocNo != string.Empty)
            param.Value = QDocNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblProjects_Update_STC(string ProjectNumber, string PVDNumber, string PVDStatusID, string ManualQuoteNumber, string STCApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_STC";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Delete(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tbl_SignatureLogExistByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "tbl_SignatureLogExistByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;

    }
    public static int tbl_SignatureLogExistByProjectID_QDocNo(string ProjectID,string QDocNo)
    {
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "tbl_SignatureLogExistByProjectID_QDocNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@QDocNo";
        param.Value = QDocNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tbl_tbl_SignatureLog_Update(string ID, string Country, string Region, string City, string Postal)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_tbl_SignatureLog_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Country";
        if (Country != string.Empty)
            param.Value = Country;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Region";
        if (Region != string.Empty)
            param.Value = Region;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@City";
        if (City != string.Empty)
            param.Value = City;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Postal";
        if (Postal != string.Empty)
            param.Value = Postal;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tbl_SignatureLogAcno_Insert(String Customer_Name, String Signed_Date, String Latitude_Location, String Longitude_Location, String IP_Address, String Customer_Email, String Token, String ProjectID, String Image_Address, string DocNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SignatureLogAcno_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Customer_Name";
        if (Customer_Name != string.Empty)
            param.Value = Customer_Name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Signed_Date";
        if (Signed_Date != string.Empty)
            param.Value = Signed_Date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Latitude_Location";
        if (Latitude_Location != string.Empty)
            param.Value = Latitude_Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Longitude_Location";
        if (Longitude_Location != string.Empty)
            param.Value = Longitude_Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IP_Address";
        if (IP_Address != string.Empty)
            param.Value = IP_Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer_Email";
        if (Customer_Email != string.Empty)
            param.Value = Customer_Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Token";
        if (Token != string.Empty)
            param.Value = Token;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Image_Address";
        if (Image_Address != string.Empty)
            param.Value = Image_Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocNo";
        if (DocNo != string.Empty)
            param.Value = DocNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_SignatureLogAcno_Update(string ID, string Country, string Region, string City, string Postal)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SignatureLogAcno_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Country";
        if (Country != string.Empty)
            param.Value = Country;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Region";
        if (Region != string.Empty)
            param.Value = Region;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@City";
        if (City != string.Empty)
            param.Value = City;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Postal";
        if (Postal != string.Empty)
            param.Value = Postal;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tbl_SignatureLogAcnoExistByProjectID_DocNo(string ProjectID, string DocNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        // set the stored procedure name
        comm.CommandText = "tbl_SignatureLogAcnoExistByProjectID_DocNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@DocNo";
        param.Value = DocNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static Sttbl_SignatureLog tbl_SignatureLogAcno_SelectByProjectID_DocNo(string ProjectID, String QDocNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SignatureLogAcno_SelectByProjectID_DocNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocNo";
        param.Value = QDocNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_SignatureLog details = new Sttbl_SignatureLog();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Customer_Name = dr["Customer_Name"].ToString();
            details.Signed_Date = dr["Signed_Date"].ToString();
            details.Latitude_Location = dr["Latitude_Location"].ToString();
            details.Longitude_Location = dr["Longitude_Location"].ToString();
            details.IP_Address = dr["IP_Address"].ToString();
            details.Customer_Email = dr["Customer_Email"].ToString();
            details.Token = dr["Token"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.Image_Address = dr["Image_Address"].ToString();
            details.QDocNo = dr["DocNo"].ToString();
        }
        // return structure details
        return details;
    }
}