using System;
using System.Data;
using System.Data.Common;


public struct SttblProjectRefund
{
    public string RefundID;
    public string ProjectID;
    public string OptionID;
    public string Amount;
    public string UserID;
    public string CreateDate;
    public string PaymentMode;
    public string PaidDate;
    public string AccActionDate;
    public string AccUserID;
    public string Remarks;
    public string Status;
    public string Notes;
    public string AccName;
    public string BSBNo;
    public string AccNo;
    public string ProjectStatusID;
    public string ProjectNumber;
    public string SalesRep;
    public string Customer;
    public string ProjectStatus;
    public string NumberPanels;
    public string BankName;
}


public class ClstblProjectRefund
{
    public static SttblProjectRefund tblProjectRefund_SelectByRefundID(String RefundID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_SelectByRefundID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjectRefund details = new SttblProjectRefund();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProjectID = dr["ProjectID"].ToString();
            details.OptionID = dr["OptionID"].ToString();
            details.Amount = dr["Amount"].ToString();
            details.UserID = dr["UserID"].ToString();
            details.CreateDate = dr["CreateDate"].ToString();
            details.PaymentMode = dr["PaymentMode"].ToString();
            details.PaidDate = dr["PaidDate"].ToString();
            details.AccActionDate = dr["AccActionDate"].ToString();
            details.AccUserID = dr["AccUserID"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.Status = dr["Status"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.AccName = dr["AccName"].ToString();
            details.BSBNo = dr["BSBNo"].ToString();
            details.AccNo = dr["AccNo"].ToString();
            details.ProjectStatusID = dr["ProjectStatusID"].ToString();
            details.BankName = dr["BankName"].ToString();
        }
        // return structure details
        return details;
    }
    public static SttblProjectRefund tblProjectRefund_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure  
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjectRefund details = new SttblProjectRefund();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.RefundID = dr["RefundID"].ToString();
            details.OptionID = dr["OptionID"].ToString();
            details.Amount = dr["Amount"].ToString();
            details.UserID = dr["UserID"].ToString();
            details.CreateDate = dr["CreateDate"].ToString();
            details.PaymentMode = dr["PaymentMode"].ToString();
            details.PaidDate = dr["PaidDate"].ToString();
            details.AccActionDate = dr["AccActionDate"].ToString();
            details.AccUserID = dr["AccUserID"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.Status = dr["Status"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.AccName = dr["AccName"].ToString();
            details.BSBNo = dr["BSBNo"].ToString();
            details.AccNo = dr["AccNo"].ToString();
           
            //details.SalesRep = dr["SalesRep"].ToString();
            //details.Customer = dr["Customer"].ToString();
            //details.ProjectStatus = dr["ProjectStatus"].ToString();
            //details.NumberPanels = dr["NumberPanels"].ToString();
            
        }
        // return structure details
        return details;
    }
    public static DataTable tblProjectRefund_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectRefund_SelectByPID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_SelectByPID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
            
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByProjectID_picklist2(string ProjectID,string RefundID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID_picklist2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int testtable_insert(String name)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "testtable_insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@name";
        param.Value = name;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblProjectRefund_Insert(String ProjectID, String OptionID, String Amount, String UserID, String Notes, String AccName, String BSBNo, String AccNo,String BankName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        if (OptionID != string.Empty)
            param.Value = OptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        if (Amount != string.Empty)
            param.Value = Amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        param.Value = UserID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AccName";
        param.Value = AccName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BSBNo";
        param.Value = BSBNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AccNo";
        param.Value = AccNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BankName";
        param.Value = BankName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblProjectRefund_Update(string RefundID, String PaymentMode, String PaidDate, String AccUserID, String Remarks)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentMode";
        if (PaymentMode != string.Empty)
            param.Value = PaymentMode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaidDate";
        if (PaidDate != string.Empty)
            param.Value = PaidDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AccUserID";
        param.Value = AccUserID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Remarks";
        param.Value = Remarks;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblProjectRefund_InsertUpdate(Int32 RefundID, String ProjectID, String OptionID, String Amount, String UserID, String CreateDate, String PaymentMode, String PaidDate, String AccActionDate, String AccUserID, String Remarks, String Status)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        param.Value = OptionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        param.Value = Amount;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        param.Value = UserID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreateDate";
        param.Value = CreateDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentMode";
        param.Value = PaymentMode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaidDate";
        param.Value = PaidDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AccActionDate";
        param.Value = AccActionDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AccUserID";
        param.Value = AccUserID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Remarks";
        param.Value = Remarks;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Status";
        param.Value = Status;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjectRefund_Delete(string RefundID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjectRefund_Search(string Status, string Project, string OptionID, string startdate, string enddate, string SalesTeamID, String EmployeeID, string userid, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_Search";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Status";
        if (Status != string.Empty)
           param.Value = Status;
        else
           param.Value = DBNull.Value;        
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Project != string.Empty)
            param.Value = Project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        if (OptionID != string.Empty)
            param.Value = OptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@ProjectStatusID";
        //if (ProjectStatusID != string.Empty)
        //    param.Value = ProjectStatusID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectRefund_Search1(string Status, string Project, string OptionID, string startdate, string enddate, string SalesTeamID, String EmployeeID, string userid, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_Search";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Status";
     //   if (Status != string.Empty)
            param.Value = Status;
     //   else
     //       param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Project != string.Empty)
            param.Value = Project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        if (OptionID != string.Empty)
            param.Value = OptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@ProjectStatusID";
        //if (ProjectStatusID != string.Empty)
        //    param.Value = ProjectStatusID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjectRefund_UpdateProjectStatusID(string RefundID, String ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_UpdateProjectStatusID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjectRefund_UpdateStatus(string RefundID, String Status)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_UpdateStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Status";
        if (Status != string.Empty)
            param.Value = Status;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);
         
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjectRefund_UpdateBy_RefundId(string RefundID, String OptionID, String AccName, String Amount, String BSBNo, String AccNo, String Notes,String BankName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectRefund_UpdateBy_RefundId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        if (OptionID != string.Empty)
            param.Value = OptionID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AccName";
        if (AccName != string.Empty)
            param.Value = AccName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Amount";
        param.Value = Amount;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BSBNo";
        param.Value = BSBNo;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AccNo";
        param.Value = AccNo;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BankName";
        param.Value = BankName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblProjectgridconnection_Search(string MeterAppliedRef, string Projectno, string startdate, string enddate, String EmployeeID, string InstallState, string ProjectStatusID,string ComplainceCertificate,string EmailFlag,string datatype,string Installer)
    {
    //    System.Web.HttpContext.Current.Response.Write(ComplainceCertificate);
    //    System.Web.HttpContext.Current.Response.End();
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectgridconnection_Search";
        
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedRef";
        if (MeterAppliedRef != string.Empty)
            param.Value = MeterAppliedRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

         param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datatype != string.Empty)
            param.Value = datatype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        
        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Projectno != string.Empty)
            param.Value = Projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ComplainceCertificate";
        if (ComplainceCertificate != string.Empty)
            param.Value = ComplainceCertificate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmailFlag";
        if (EmailFlag != string.Empty)
            param.Value = EmailFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    
    public static DataTable tblEmployees_sales_manager(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_sales_manager";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

}