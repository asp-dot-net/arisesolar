using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblFinanceWith
	{
		public string FinanceWith;
		public string Active;
		public string Seq;

	}


public class ClstblFinanceWith
{
	public static SttblFinanceWith tblFinanceWith_SelectByFinanceWithID (String FinanceWithID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblFinanceWith_SelectByFinanceWithID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@FinanceWithID";
		param.Value = FinanceWithID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblFinanceWith details = new SttblFinanceWith();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.FinanceWith = dr["FinanceWith"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblFinanceWith_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblFinanceWith_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}



    public static DataTable tblFinanceWith_SelectByIsActive()
	{
		DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFinanceWith_SelectByIsActive";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}

    public static DataTable tblFinanceWithDeposit_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFinanceWithDeposit_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

	public static int tblFinanceWith_Insert ( String FinanceWith, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblFinanceWith_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@FinanceWith";
		param.Value = FinanceWith;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblFinanceWith_Update (string FinanceWithID, String FinanceWith, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblFinanceWith_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@FinanceWithID";
		param.Value = FinanceWithID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@FinanceWith";
		param.Value = FinanceWith;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static string tblFinanceWith_InsertUpdate (Int32 FinanceWithID, String FinanceWith, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblFinanceWith_InsertUpdate";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@FinanceWithID";
		param.Value = FinanceWithID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@FinanceWith";
		param.Value = FinanceWith;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		string result = "";
		try
		{
			result = DataAccess.ExecuteScalar(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static bool tblFinanceWith_Delete (string FinanceWithID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblFinanceWith_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@FinanceWithID";
		param.Value = FinanceWithID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
    public static int FinanceWithNameExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "FinanceWithNameExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int FinanceWithNameExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "FinanceWithNameExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static DataTable tblFinanceWithGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFinanceWithGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblFinanceWithGetDataBySearch(string alpha ,string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblFinanceWithGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblFinanceWith_SelectByFinanceWithID_New(String FinanceWithID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFinanceWith_SelectByFinanceWithID_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        param.Value = FinanceWithID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblFinanceWithDeposit_SelectByFinanceWithID(String FinanceWithDepositID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFinanceWithDeposit_SelectByFinanceWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithDepositID";
        param.Value = FinanceWithDepositID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}