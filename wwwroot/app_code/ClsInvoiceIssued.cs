﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsInvoiceIssued
/// </summary>
public class ClsInvoiceIssued
{
    public ClsInvoiceIssued()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable Sp_InvoiceIssuedNew(string invoiceNo, string contact, string suburb, string state, string paidStatus, string projectStatus, string financeWith, string payBy, string area, string dateType, string startDate, string endDate, string postCodeFrom, string postCodeTo, string installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_InvoiceIssuedNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@contact";
        if (contact != string.Empty)
            param.Value = contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@suburb";
        if (suburb != string.Empty)
            param.Value = suburb;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@state";
        if (state != string.Empty)
            param.Value = state;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@paidStatus";
        if (paidStatus != string.Empty)
            param.Value = paidStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@projectStatus";
        if (projectStatus != string.Empty)
            param.Value = projectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@financeWith";
        if (financeWith != string.Empty)
            param.Value = financeWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@payBy";
        if (payBy != string.Empty)
            param.Value = payBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@area";
        if (area != string.Empty)
            param.Value = area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@postCodeFrom";
        if (postCodeFrom != string.Empty)
            param.Value = postCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@postCodeTo";
        if (postCodeTo != string.Empty)
            param.Value = postCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installer";
        if (installer != string.Empty)
            param.Value = installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Sp_InvoiceIssuedNew_DepositReceived(string invoiceNo, string contact, string suburb, string state, string paidStatus, string projectStatus, string financeWith, string payBy, string area, string dateType, string startDate, string endDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_InvoiceIssuedNew_DepositReceived";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contact";
        if (contact != string.Empty)
            param.Value = contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@suburb";
        if (suburb != string.Empty)
            param.Value = suburb;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@state";
        if (state != string.Empty)
            param.Value = state;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@paidStatus";
        if (paidStatus != string.Empty)
            param.Value = paidStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectStatus";
        if (projectStatus != string.Empty)
            param.Value = projectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@financeWith";
        if (financeWith != string.Empty)
            param.Value = financeWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@payBy";
        if (payBy != string.Empty)
            param.Value = payBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@area";
        if (area != string.Empty)
            param.Value = area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Sp_InvoiceIssuedNew_InoviceIssued(string invoiceNo, string contact, string suburb, string state, string paidStatus, string projectStatus, string financeWith, string payBy, string area, string dateType, string startDate, string endDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_InvoiceIssuedNew_InoviceIssued";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contact";
        if (contact != string.Empty)
            param.Value = contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@suburb";
        if (suburb != string.Empty)
            param.Value = suburb;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@state";
        if (state != string.Empty)
            param.Value = state;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@paidStatus";
        if (paidStatus != string.Empty)
            param.Value = paidStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectStatus";
        if (projectStatus != string.Empty)
            param.Value = projectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@financeWith";
        if (financeWith != string.Empty)
            param.Value = financeWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@payBy";
        if (payBy != string.Empty)
            param.Value = payBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@area";
        if (area != string.Empty)
            param.Value = area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
}