﻿using System;
using System.Web;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Web.Security;
//using iTextSharp.text;
//using iTextSharp.text.pdf;


/// <summary>
/// Summary description for Telerik_reports
/// </summary>
public class Telerik_reports
{

    public Telerik_reports()
    {
        //
        // TODO: Add constructor logic here
        //

    }

    public static void generate_quatation(string ProjectID, string QuoteID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        //string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Nquotation();

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table_Contact = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table2 = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt1;
        table_Contact.DataSource = dt1;
        // table2.DataSource = dt1;
        table3.DataSource = dt1;
        table4.DataSource = dt1;
        table5.DataSource = dt1;
        table6.DataSource = dt1;
        table7.DataSource = dt1;
        table8.DataSource = dt1;

        //Telerik.Reporting.TextBox txtInverterdetail = rpt.Items.Find("txtInverterdetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInverterdetail1 = rpt.Items.Find("txtInverterdetail1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeter = rpt.Items.Find("txtmeter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtswitch = rpt.Items.Find("txtswitch", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox195 = rpt.Items.Find("textBox195", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpaymentplan = rpt.Items.Find("txtpaymentplan", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox214 = rpt.Items.Find("textBox214", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtABN = rpt.Items.Find("textBox215", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox179 = rpt.Items.Find("textBox179", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox180 = rpt.Items.Find("textBox180", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox191 = rpt.Items.Find("textBox191", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.PictureBox pictureBox2 = rpt.Items.Find("pictureBox2", true)[0] as Telerik.Reporting.PictureBox;

        txtProjectNumber.Value = stPro.ProjectNumber;

        string inverterdetail = string.Empty;
        string lblbasiccost = string.Empty;
        string lblreq = "+ N/A";
        string lblcompl = "+ N/A";
        string lbltax = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbldeposit = string.Empty;
        string lbllessdeposit = string.Empty;
        string otherdetail = string.Empty;
        string paymentplan = string.Empty;

        //==================================================
        try
        {
            if (dt1.Rows[0]["FinanceWith"].ToString() != string.Empty)
            {
                paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString();//
                //if (st2.FinanceWithDeposit != string.Empty)
                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                {
                    paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%";
                    if (dt1.Rows[0]["PaymentType"].ToString() != string.Empty)
                    {
                        paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%" + " for " + dt1.Rows[0]["PaymentType"].ToString();
                    }
                }
            }

            txtpaymentplan.Value = paymentplan;

            if (st2.PromoText != string.Empty)
            {
                textBox214.Visible = true;
                otherdetail = st2.PromoText;
            }
            else
            {
                textBox214.Visible = false;
            }
            txtotherdetail.Value = otherdetail;
        }
        catch
        {

        }
        if (stPro.InverterDetailsID != "")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
        }
        if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. ";
        }
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }
        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        //lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldeposit = "0";
        }
        try
        {
            lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        }
        catch { }
        try
        {
            textBox179.Value = dt1.Rows[0]["ProjectNumber"].ToString();
        }
        catch { }
        try
        {
            textBox180.Value = dt1.Rows[0]["SalesRepName"].ToString();
        }
        catch { }
        try
        {
            textBox191.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
        }
        catch { }
        txtmeterphase.Value = stPro.MeterPhase;

        txtmeter.Value = "Meter Phase : " + stPro.MeterPhase;
        txtswitch.Value = "Switchboard Upgrades : " + stPro.meterupgrade;

        //txtInverterdetail.Value = inverterdetail.ToString();
        txtInverterdetail1.Value = inverterdetail.ToString();
        txtbasiccost.Value = lblbasiccost.ToString();
        txtextra.Value = "+ " + lbltax.ToString();
        txtreq.Value = lblreq.ToString();
        txtcompl.Value = lblcompl.ToString();
        txtfullretail.Value = lblfullretail.ToString();
        txtdiscount2.Value = "- " + lbldiscount2.ToString();
        txtstc.Value = "- " + lblstc.ToString();
        txtgst.Value = lbltotalcost.ToString();
        txtdeposit.Value = lbldeposit.ToString();
        txtlessdeposit.Value = lbllessdeposit.ToString();
        //txtcustname.Value = stPro.Customer;
        textBox195.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));

        // int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID(ProjectID);
        int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, QuoteID);

        if (existinSignLog == 1)
        {
            // Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID(ProjectID);
            Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID_QDocNo(ProjectID, QuoteID);
            String ImageAdd = stsign.Image_Address;
            String Token = stsign.Token;
            //string Token = System.Web.HttpContext.Current.Request.QueryString["rt"];
            //if(string.IsNullOrEmpty(Token))
            //{
            //   Token = stsign.Token;
            //}
            Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

            Page p = new Page();
            String Virtualpath = stsign.Image_Address;
            int index = Virtualpath.IndexOf("userfiles");
            Virtualpath = Virtualpath.Substring(index);
            string img1 = p.Server.MapPath("~/" + Virtualpath);
            pictureBox2.Value = img1;

            try
            {
                textBox195.Value = (Convert.ToDateTime(stsign.Signed_Date)).ToString(("dd-MMM-yyyy"));
            }
            catch { }
            Clstbl_TokenData.tbl_TokenData_Update_SignFlag(Token, ProjectID, "True");

            /////////////////////////////////////////////////////////////
            //string fileName = stToken.QDocNo + "Quotation.pdf";
            //// ExportPdf(rpt);
            //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
            //SiteConfiguration.deleteimage(fileName, "quotedoc");
            /////////////////////////////////////////////////////////////////////
            // SavePDF(rpt, QuoteID);
            //SiteConfiguration.DeletePDFFile("quotedoc", fileName);

            // string nQoute = "s" + stToken.QDocNo;
            string nQoute = stToken.QDocNo;
            SavePDF(rpt, nQoute);

            DataTable dt2 = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteDoc(stToken.QDocNo);
            string ProjectQuoteDoc = dt2.Rows[0]["ProjectQuoteDoc"].ToString();
            string QuoteDoc = nQoute + "Quotation.pdf";
            ClstblProjects.tblProjectQuotes_UpdateQuoteDoc(ProjectQuoteDoc, QuoteDoc);

            string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "Quotation.pdf";
            SavePDFForSQ(rpt, SQFileNmae);

            //Server.Execute("~/app_code/);
            //HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);
        }
        else
        {
            pictureBox2.Value = null;
            SavePDF(rpt, QuoteID);
        }




        //try
        //{
        //    string ABN = dt.Rows[0]["CustWebSiteLink"].ToString();
        //    if (ABN != "" && ABN != "0" && ABN != null)
        //    {
        //        txtABN.Value = ABN;
        //    }
        //    //else
        //    //{
        //    //    txtABN.Value = "";
        //    //}
        //}
        //catch { }
        //SavePDF(rpt, QuoteID);
        // ExportPdf(rpt);

    }
    public static void generate_Quote(string ProjectID, string QuoteID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        //string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        Telerik.Reporting.Report rpt = new EurosolarReporting.Quote_new();
        // rpt.DocumentName = "Qoute";

        Telerik.Reporting.Table tblcustomer = rpt.Items.Find("table25", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblsales = rpt.Items.Find("table26", true)[0] as Telerik.Reporting.Table;
        // Telerik.Reporting.Table tblsystemdetail = rpt.Items.Find("table27", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table28 = rpt.Items.Find("table28", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table29 = rpt.Items.Find("table29", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table30 = rpt.Items.Find("table30", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table31 = rpt.Items.Find("table31", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table32 = rpt.Items.Find("table32", true)[0] as Telerik.Reporting.Table;


        tblcustomer.DataSource = dt1;
        tblsales.DataSource = dt1;
        // tblsystemdetail.DataSource = dt1;
        table28.DataSource = dt1;
        table29.DataSource = dt1;
        table30.DataSource = dt1;
        table31.DataSource = dt1;
        //table32.DataSource = dt1;

        Telerik.Reporting.TextBox textBox928 = rpt.Items.Find("textBox928", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox931 = rpt.Items.Find("textBox931", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.PictureBox pictureBox14 = rpt.Items.Find("pictureBox14", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.TextBox txtmeter = rpt.Items.Find("txtmeter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtswitch = rpt.Items.Find("txtswitch", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInverterdetail1 = rpt.Items.Find("txtInverterdetail1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsystemcap = rpt.Items.Find("txtsystemcap", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtpaymentplan = rpt.Items.Find("txtpaymentplan", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox949 = rpt.Items.Find("textBox949", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;21-02-20
        //Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtspdescount = rpt.Items.Find("txtspdescount", true)[0] as Telerik.Reporting.TextBox;        
        //Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox836 = rpt.Items.Find("textBox836", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox74 = rpt.Items.Find("textBox74", true)[0] as Telerik.Reporting.TextBox;
       Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInstallAddress = rpt.Items.Find("txtInstallAddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsidedetail = rpt.Items.Find("txtsidedetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttotal = rpt.Items.Find("txttotal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstreetadd = rpt.Items.Find("textBox838", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstreetadd1 = rpt.Items.Find("textBox927", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtPoption = rpt.Items.Find("txtPoption", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.PictureBox pictureBox13 = rpt.Items.Find("pictureBox13", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.TextBox textBox730 = rpt.Items.Find("textBox730", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttoalcost = rpt.Items.Find("txttoalcost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttoalcostbal = rpt.Items.Find("txttoalcostbal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox946 = rpt.Items.Find("textBox946", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmulti = rpt.Items.Find("txtmulti", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpnumber = rpt.Items.Find("txtpnumber", true)[0] as Telerik.Reporting.TextBox;
        
        Telerik.Reporting.PictureBox picbxnearby1 = rpt.Items.Find("picbocnearmap1", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.PictureBox pic2 = rpt.Items.Find("pictureBox16", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.PictureBox pic3 = rpt.Items.Find("pictureBox17", true)[0] as Telerik.Reporting.PictureBox;

        txtsystemcap.Value = stPro.SystemCapKW + "KW";
        textBox836.Value = stPro.SystemCapKW + "KW";
        txtInstallAddress.Value = (dt1.Rows[0]["InstallationFullAddress"].ToString()).TrimStart();
        txtsidedetail.Value = (dt1.Rows[0]["InstallationFullAddress"].ToString()).TrimStart();
        txtpnumber.Value = stPro.ProjectNumber;
        
        string inverterdetail = string.Empty;
        string otherdetail = string.Empty;
        string paymentplan = string.Empty;
        string lblbasiccost = string.Empty;
        string lblreq = "+ N/A";
        string lblcompl = "+ N/A";
        string lbltax = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbldeposit = string.Empty;
        string lbllessdeposit = string.Empty;
        string lblother = string.Empty;
        
        try
        {
            txtstreetadd.Value = dt1.Rows[0]["InstallAddress"].ToString().TrimStart() + ", ";
            txtstreetadd1.Value = dt1.Rows[0]["InstallCity"].ToString() + ", " + dt1.Rows[0]["InstallState"].ToString() + ", " + dt1.Rows[0]["InstallPostCode"].ToString();

            textBox928.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));            
            textBox931.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
        }
        catch
        { }
        try
        {
            //textBox74.Value = "ARISE SOLAR PTY LTD and ABN " + dt.Rows[0]["CustWebSiteLink"].ToString() + ", referred to as “we” or “us”; and";     
        }
        catch
        {
        }
        try
        {
            if (dt1.Rows[0]["FinanceWith"].ToString() != string.Empty)
            {
                paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString();//
                //if (st2.FinanceWithDeposit != string.Empty)
              
                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                {
                    paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%";
                    if (dt1.Rows[0]["PaymentType"].ToString() != string.Empty)
                    {
                        paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%" + " for " + dt1.Rows[0]["PaymentType"].ToString();
                    }
                    
                }

                txtPoption.Value = "-";
                textBox946.Value = "-";
                txttoalcostbal.Value = "-";
                txttoalcost.Value = "-";
                txtmulti.Value = "-";
                textBox730.Value = "-";
                //if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() == "Humm")
                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")              
                {
                    txtPoption.Value = dt1.Rows[0]["FinanceWith"].ToString();
                    txttoalcost.Value = Convert.ToDecimal(stPro.TotalQuotePrice).ToString("0.00");
                    txttoalcost.Style.Font.Bold=true;
                    decimal DepositRequired = 0;
                    if (!string.IsNullOrEmpty(stPro.DepositRequired))
                    {
                        DepositRequired = Convert.ToDecimal(stPro.DepositRequired);
                    }
                    decimal TotalQuotePrice = 0;
                    if (!string.IsNullOrEmpty(stPro.TotalQuotePrice))
                    {
                        TotalQuotePrice = Convert.ToDecimal(stPro.TotalQuotePrice);
                    }
                    decimal balance = TotalQuotePrice - DepositRequired;
                    //txttoalcostbal.Value = balance.ToString("0.00") + "(Included Monthly account keeping fees for " + dt1.Rows[0]["PaymentType"].ToString().Trim() + ")";
                    txttoalcostbal.Value = Convert.ToDecimal(stPro.TotalQuotePrice).ToString("0.00") + "(excluded Monthly account keeping fees for " + dt1.Rows[0]["PaymentType"].ToString().Trim() + ")";
                    txttoalcostbal.Style.Font.Bold = true; 
                    textBox946.Value = dt1.Rows[0]["PaymentType"].ToString();
                    textBox946.Style.Font.Bold = true;
                    string value = Regex.Replace(textBox946.Value, "[A-Za-z ]", "");
                    txtmulti.Value = Convert.ToString(8 * Convert.ToDecimal(value));
                    txtmulti.Style.Font.Bold = true;
                    textBox730.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));
                }
            }

            //txtpaymentplan.Value = paymentplan;

            if (st2.PromoText != string.Empty)
            {
                //  textBox949.Visible = true;
                otherdetail = st2.PromoText;
            }
            else
            {
                //  textBox949.Visible = false;
            }
            // txtotherdetail.Value = otherdetail;
        }
        catch
        {

        }

        //Invert Detail bind
        if (stPro.InverterDetailsID != "")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
        }
        if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. ";
        }


        decimal varOtherBasicCost = stPro.varOtherBasicCost != "" ? Convert.ToDecimal(stPro.varOtherBasicCost) : 0;
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate) + varOtherBasicCost).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }

        //lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        // lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString() );
        //lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.HotWaterMeter) + Convert.ToDecimal(stPro.SmartMeter) + Convert.ToDecimal(stPro.OldSystemRemovalCost) + Convert.ToDecimal(stPro.TiltkitBrackets) + Convert.ToDecimal(stPro.VarOther)).ToString());
        //lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.HotWaterMeter) + Convert.ToDecimal(stPro.SmartMeter) + Convert.ToDecimal(stPro.VarOther)).ToString() + Convert.ToDecimal(stPro.collectioncharge)).ToString() + Convert.ToDecimal(stPro.OldSystemRemovalCost) + Convert.ToDecimal(stPro.TiltkitBrackets) + Convert.ToDecimal(stPro.varOtherBasicCost);

        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation)
            + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime)
            + Convert.ToDecimal(stPro.HotWaterMeter) + Convert.ToDecimal(stPro.SmartMeter) + Convert.ToDecimal(stPro.VarOther) + Convert.ToDecimal(stPro.collectioncharge)
            + Convert.ToDecimal(stPro.OldSystemRemovalCost) + Convert.ToDecimal(stPro.TiltkitBrackets) + Convert.ToDecimal(stPro.varOtherBasicCost)).ToString());

        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        lblother = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lblother == string.Empty)
        {
            lblother = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
           // lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost)).ToString());
        }
        catch { }

        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }

        if (stPro.mgrrdisc != string.Empty || stPro.mgrrdisc != "")
        {
            lbldiscount2 = (Convert.ToDecimal(lbldiscount2) + Convert.ToDecimal(stPro.mgrrdisc)).ToString("F");
        }
       

        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldeposit = "0";
        }
        try
        {
            lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        }
        catch { }

        //try
        //{
        //    textBox179.Value = dt1.Rows[0]["ProjectNumber"].ToString();
        //}
        //catch { }
        //try
        //{
        //    textBox180.Value = dt1.Rows[0]["SalesRepName"].ToString();
        //}
        //catch { }
        //try
        //{
        //    textBox191.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
        //}
        //catch { }
        txtmeterphase.Value = stPro.MeterPhase;


        txtInverterdetail1.Value = inverterdetail.ToString();
        txtbasiccost.Value = "$" + lblbasiccost.ToString();
        txtextra.Value = "$ " + lbltax.ToString();
        //txtreq.Value = lblreq.ToString();
        //txtcompl.Value = lblcompl.ToString();
        //txtfullretail.Value = lblfullretail.ToString();
        //txtdiscount2.Value = "- " + lbldiscount2.ToString();
        txtstc.Value = "$" + lblstc.ToString();
        txtspdescount.Value= "$" + lbldiscount2.ToString();
        //txtgst.Value = lbltotalcost.ToString();
        txtdeposit.Value = "$" + lbldeposit.ToString();
        txtlessdeposit.Value = "$" + lbllessdeposit.ToString();
        //txtother.Value = "$" + lblother.ToString();
       
        decimal tot = 0;
        //tot = (Convert.ToDecimal(lbltax) + Convert.ToDecimal(lblbasiccost)) - Convert.ToDecimal(lblstc)- Convert.ToDecimal(lbldiscount2);
        //tot = (Convert.ToDecimal(lblother) + Convert.ToDecimal(lbltax) + Convert.ToDecimal(lblbasiccost)) - Convert.ToDecimal(lblstc) - Convert.ToDecimal(lbldiscount2);
        tot = (Convert.ToDecimal(lbltax) + Convert.ToDecimal(lblbasiccost)) - Convert.ToDecimal(lblstc) - Convert.ToDecimal(lbldiscount2);
        txttotal.Value = "$" + tot.ToString();

        txtmeter.Value = "Meter Phase : " + stPro.MeterPhase;
        txtswitch.Value = "Switchboard Upgrades : " + stPro.meterupgrade;
        if(ProjectID!=null && ProjectID!="")
        {
            DataTable dtMap = ClsProjectSale.tblNearMapImages_GetData(ProjectID);
            if (dtMap.Rows.Count > 0)
            {
                if (dtMap.Rows[0]["Image1"].ToString() != null && dtMap.Rows[0]["Image1"].ToString() != "")
                {
                    Page p = new Page();
                    //string Url = pdfURL + "NearMap/" + dtMap.Rows[0]["Image1"].ToString();
                    string Url = SiteConfiguration.GetDocumnetPath("NearMap", dtMap.Rows[0]["Image1"].ToString());
                    picbxnearby1.Value = Url;
                }
                else
                {
                    picbxnearby1.Value = null;

                }
                if (dtMap.Rows[0]["image2"].ToString() != null && dtMap.Rows[0]["image2"].ToString() != "")
                {
                    Page p = new Page();
                    //string Url = pdfURL + "NearMap/" + dtMap.Rows[0]["image2"].ToString();
                    string Url = SiteConfiguration.GetDocumnetPath("NearMap", dtMap.Rows[0]["image2"].ToString());
                    pic2.Value = Url;
                }
                else
                {
                    pic2.Value = null;

                }
                if (dtMap.Rows[0]["image3"].ToString() != null && dtMap.Rows[0]["image3"].ToString() != "")
                {
                    Page p = new Page();
                    //string Url = pdfURL + "NearMap/" + dtMap.Rows[0]["image3"].ToString();
                    string Url = SiteConfiguration.GetDocumnetPath("NearMap", dtMap.Rows[0]["image3"].ToString());
                    pic3.Value = Url;
                }
                else
                {
                    pic3.Value = null;

                }
                

            }
        }

        //if (stPro.nearmapdoc != null && stPro.nearmapdoc != "")
        //{
        //    Page p = new Page();
        //    string Url = pdfURL + "NearMap/" + stPro.nearmapdoc;
        //    picbxnearby1.Value = Url;
        //}
        //else
        //{
        //    picbxnearby1.Value = null;
        //}
        //if (stPro.nearmapdoc1 != null && stPro.nearmapdoc1 != "")
        //{
        //    Page p = new Page();
        //    string Url = pdfURL + "NearMap/" + stPro.nearmapdoc1;
        //    pic2.Value = Url;
        //}
        //else
        //{
        //    pic2.Value = null;
        //}
        //if (stPro.nearmapdoc2 != null && stPro.nearmapdoc2 != "")
        //{
        //    Page p = new Page();
        //    string Url = pdfURL + "NearMap/" + stPro.nearmapdoc2;
        //    pic3.Value = Url;
        //}
        //else
        //{
        //    pic3.Value = null;
        //}
        int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, QuoteID);
        if (existinSignLog == 1)
        {
            // Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID(ProjectID);
            Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID_QDocNo(ProjectID, QuoteID);
            String ImageAdd = stsign.Image_Address;
            String Token = stsign.Token;
            //string Token = System.Web.HttpContext.Current.Request.QueryString["rt"];
            //if(string.IsNullOrEmpty(Token))
            //{
            //   Token = stsign.Token;
            //}
            Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

            Page p = new Page();
            String Virtualpath = stsign.Image_Address;
            int index = Virtualpath.IndexOf("userfiles");
            Virtualpath = Virtualpath.Substring(index);
            string img1 = p.Server.MapPath("~/" + Virtualpath);
            pictureBox14.Value = img1;

            if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() == "Humm")
            {
                pictureBox13.Value = img1;
            }
            else
            {
                pictureBox13.Value = null;
            }
            try
            {
                textBox928.Value = (Convert.ToDateTime(stsign.Signed_Date)).ToString(("dd-MMM-yyyy"));
            }
            catch { }
            Clstbl_TokenData.tbl_TokenData_Update_SignFlag(Token, ProjectID, "True");

            /////////////////////////////////////////////////////////////
            //string fileName = stToken.QDocNo + "Quotation.pdf";
            //// ExportPdf(rpt);
            //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
            //SiteConfiguration.deleteimage(fileName, "quotedoc");
            /////////////////////////////////////////////////////////////////////
            // SavePDF(rpt, QuoteID);
            //SiteConfiguration.DeletePDFFile("quotedoc", fileName);

            // string nQoute = "s" + stToken.QDocNo;
            string nQoute = stToken.QDocNo;
            SavePDF(rpt, nQoute);

            DataTable dt2 = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteDoc(stToken.QDocNo);
            string ProjectQuoteDoc = dt2.Rows[0]["ProjectQuoteDoc"].ToString();
            string QuoteDoc = nQoute + "Quotation.pdf";
            ClstblProjects.tblProjectQuotes_UpdateQuoteDoc(ProjectQuoteDoc, QuoteDoc);

            string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "Quotation.pdf";
            SavePDFForSQ(rpt, SQFileNmae);

            //Server.Execute("~/app_code/);
            //HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);
        }
        else
        {
            pictureBox14.Value = null;
            pictureBox13.Value = null;
            SavePDF(rpt, QuoteID);
        }
        //ExportPdf(rpt);      

    }
    public static void generate_QuoteCash(string ProjectID, string QuoteID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        //string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        Telerik.Reporting.Report rpt = new EurosolarReporting.Quote();
        // rpt.DocumentName = "Qoute";

        Telerik.Reporting.Table tblcustomer = rpt.Items.Find("table25", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblsales = rpt.Items.Find("table26", true)[0] as Telerik.Reporting.Table;
        // Telerik.Reporting.Table tblsystemdetail = rpt.Items.Find("table27", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table28 = rpt.Items.Find("table28", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table29 = rpt.Items.Find("table29", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table30 = rpt.Items.Find("table30", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table31 = rpt.Items.Find("table31", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table32 = rpt.Items.Find("table32", true)[0] as Telerik.Reporting.Table;


        tblcustomer.DataSource = dt1;
        tblsales.DataSource = dt1;
        // tblsystemdetail.DataSource = dt1;
        table28.DataSource = dt1;
        table29.DataSource = dt1;
        table30.DataSource = dt1;
        table31.DataSource = dt1;
        //table32.DataSource = dt1;

        Telerik.Reporting.TextBox textBox928 = rpt.Items.Find("textBox928", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox931 = rpt.Items.Find("textBox931", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.PictureBox pictureBox14 = rpt.Items.Find("pictureBox14", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.TextBox txtmeter = rpt.Items.Find("txtmeter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtswitch = rpt.Items.Find("txtswitch", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInverterdetail1 = rpt.Items.Find("txtInverterdetail1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsystemcap = rpt.Items.Find("txtsystemcap", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtpaymentplan = rpt.Items.Find("txtpaymentplan", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox949 = rpt.Items.Find("textBox949", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtspdescount = rpt.Items.Find("txtspdescount", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox836 = rpt.Items.Find("textBox836", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox74 = rpt.Items.Find("textBox74", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtother = rpt.Items.Find("txtother", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInstallAddress = rpt.Items.Find("txtInstallAddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsidedetail = rpt.Items.Find("txtsidedetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttotal = rpt.Items.Find("txttotal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstreetadd = rpt.Items.Find("textBox838", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstreetadd1 = rpt.Items.Find("textBox927", true)[0] as Telerik.Reporting.TextBox;       
        Telerik.Reporting.TextBox txtpnumber = rpt.Items.Find("txtpnumber", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.PictureBox picbxnearby1 = rpt.Items.Find("picbocnearmap1", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.PictureBox pic2 = rpt.Items.Find("pictureBox16", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.PictureBox pic3 = rpt.Items.Find("pictureBox17", true)[0] as Telerik.Reporting.PictureBox;

        txtsystemcap.Value = stPro.SystemCapKW + "KW";
        textBox836.Value = stPro.SystemCapKW + "KW";
        txtInstallAddress.Value = (dt1.Rows[0]["InstallationFullAddress"].ToString()).TrimStart();
        txtsidedetail.Value = (dt1.Rows[0]["InstallationFullAddress"].ToString()).TrimStart();
        txtpnumber.Value = stPro.ProjectNumber;

        string inverterdetail = string.Empty;
        string otherdetail = string.Empty;
        string paymentplan = string.Empty;
        string lblbasiccost = string.Empty;
        string lblreq = "+ N/A";
        string lblcompl = "+ N/A";
        string lbltax = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbldeposit = string.Empty;
        string lbllessdeposit = string.Empty;
        string lblother = string.Empty;

        try
        {
            txtstreetadd.Value = dt1.Rows[0]["InstallAddress"].ToString().TrimStart() + ", ";
            txtstreetadd1.Value = dt1.Rows[0]["InstallCity"].ToString() + ", " + dt1.Rows[0]["InstallState"].ToString() + ", " + dt1.Rows[0]["InstallPostCode"].ToString();

            textBox928.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));
            textBox931.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
        }
        catch
        { }
        try
        {
            //textBox74.Value = "ARISE SOLAR PTY LTD and ABN " + dt.Rows[0]["CustWebSiteLink"].ToString() + ", referred to as “we” or “us”; and";     
        }
        catch
        {
        }
        try
        {
            if (dt1.Rows[0]["FinanceWith"].ToString() != string.Empty)
            {
                paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString();//
                                                                                                              //if (st2.FinanceWithDeposit != string.Empty)

                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                {
                    paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%";
                    if (dt1.Rows[0]["PaymentType"].ToString() != string.Empty)
                    {
                        paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%" + " for " + dt1.Rows[0]["PaymentType"].ToString();
                    }

                }                
            }

            //txtpaymentplan.Value = paymentplan;

            if (st2.PromoText != string.Empty)
            {
                //  textBox949.Visible = true;
                otherdetail = st2.PromoText;
            }
            else
            {
                //  textBox949.Visible = false;
            }
            // txtotherdetail.Value = otherdetail;
        }
        catch
        {

        }

        //Invert Detail bind
        if (stPro.InverterDetailsID != "")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
        }
        if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. ";
        }

        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }
        //lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.HotWaterMeter) + Convert.ToDecimal(stPro.SmartMeter) + Convert.ToDecimal(stPro.VarOther)).ToString() + Convert.ToDecimal(stPro.collectioncharge)).ToString() + Convert.ToDecimal(stPro.OldSystemRemovalCost) + Convert.ToDecimal(stPro.TiltkitBrackets) + Convert.ToDecimal(stPro.varOtherBasicCost));
        
        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) 
            + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) 
            + Convert.ToDecimal(stPro.HotWaterMeter) + Convert.ToDecimal(stPro.SmartMeter) + Convert.ToDecimal(stPro.VarOther) + Convert.ToDecimal(stPro.collectioncharge)
            + Convert.ToDecimal(stPro.OldSystemRemovalCost) + Convert.ToDecimal(stPro.TiltkitBrackets) + Convert.ToDecimal(stPro.varOtherBasicCost)).ToString());
        
        
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        lblother = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lblother == string.Empty)
        {
            lblother = "0";
        }
        try
        {
           // lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost)).ToString());
             lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }

        if (stPro.mgrrdisc != string.Empty || stPro.mgrrdisc != "")
        {
            lbldiscount2 = (Convert.ToDecimal(lbldiscount2) + Convert.ToDecimal(stPro.mgrrdisc)).ToString("F");
        }

        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldeposit = "0";
        }
        try
        {
            lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        }
        catch { }

        //try
        //{
        //    textBox179.Value = dt1.Rows[0]["ProjectNumber"].ToString();
        //}
        //catch { }
        //try
        //{
        //    textBox180.Value = dt1.Rows[0]["SalesRepName"].ToString();
        //}
        //catch { }
        //try
        //{
        //    textBox191.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
        //}
        //catch { }
        txtmeterphase.Value = stPro.MeterPhase;


        txtInverterdetail1.Value = inverterdetail.ToString();
        txtbasiccost.Value = "$" + lblbasiccost.ToString();
        txtextra.Value = "$" + lbltax.ToString();
        //txtreq.Value = lblreq.ToString();
        //txtcompl.Value = lblcompl.ToString();
        //txtfullretail.Value = lblfullretail.ToString();
        //txtdiscount2.Value = "- " + lbldiscount2.ToString();
        txtstc.Value = "$" + lblstc.ToString();
        txtspdescount.Value = "$" + lbldiscount2.ToString();
        //txtgst.Value = lbltotalcost.ToString();
        txtdeposit.Value = "$" + lbldeposit.ToString();
        txtlessdeposit.Value = "$" + lbllessdeposit.ToString();
        //txtother.Value = "$ " + lblother.ToString();
        decimal tot = 0;
        //tot = (Convert.ToDecimal(lbltax) + Convert.ToDecimal(lblbasiccost)) - Convert.ToDecimal(lblstc) - Convert.ToDecimal(lbldiscount2);
        //tot = (Convert.ToDecimal(lblother) + Convert.ToDecimal(lbltax) + Convert.ToDecimal(lblbasiccost)) - Convert.ToDecimal(lblstc) - Convert.ToDecimal(lbldiscount2);
        tot = (Convert.ToDecimal(lbltax) + Convert.ToDecimal(lblbasiccost)) - Convert.ToDecimal(lblstc) - Convert.ToDecimal(lbldiscount2);
        txttotal.Value = "$" + tot.ToString();

        txtmeter.Value = "Meter Phase : " + stPro.MeterPhase;
        txtswitch.Value = "Switchboard Upgrades : " + stPro.meterupgrade;


        //if (stPro.nearmapdoc != null && stPro.nearmapdoc != "")
        //{
        //    Page p = new Page();
        //    string Url = pdfURL + "NearMap/" + stPro.nearmapdoc;
        //    picbxnearby1.Value = Url;
        //}
        //else
        //{
        //    picbxnearby1.Value = null;
        //}
        //if (stPro.nearmapdoc1 != null && stPro.nearmapdoc1 != "")
        //{
        //    Page p = new Page();
        //    string Url = pdfURL + "NearMap/" + stPro.nearmapdoc1;
        //    pic2.Value = Url;
        //}
        //else
        //{
        //    pic2.Value = null;
        //}
        //if (stPro.nearmapdoc2 != null && stPro.nearmapdoc2 != "")
        //{
        //    Page p = new Page();
        //    string Url = pdfURL + "NearMap/" + stPro.nearmapdoc2;
        //    pic3.Value = Url;
        //}
        //else
        //{
        //    pic3.Value = null;
        //}
        if (ProjectID != null && ProjectID != "")
        {
            DataTable dtMap = ClsProjectSale.tblNearMapImages_GetData(ProjectID);
            if (dtMap.Rows.Count > 0)
            {
                if (dtMap.Rows[0]["Image1"].ToString() != null && dtMap.Rows[0]["Image1"].ToString() != "")
                {
                    Page p = new Page();
                    //string Url = pdfURL + "NearMap/" + dtMap.Rows[0]["Image1"].ToString();
                    string Url = SiteConfiguration.GetDocumnetPath("NearMap", dtMap.Rows[0]["Image1"].ToString());
                    picbxnearby1.Value = Url;
                }
                else
                {
                    picbxnearby1.Value = null;

                }
                if (dtMap.Rows[0]["image2"].ToString() != null && dtMap.Rows[0]["image2"].ToString() != "")
                {
                    Page p = new Page();
                    //string Url = pdfURL + "NearMap/" + dtMap.Rows[0]["image2"].ToString();
                    string Url = SiteConfiguration.GetDocumnetPath("NearMap", dtMap.Rows[0]["image2"].ToString());
                    pic2.Value = Url;
                }
                else
                {
                    pic2.Value = null;

                }
                if (dtMap.Rows[0]["image3"].ToString() != null && dtMap.Rows[0]["image3"].ToString() != "")
                {
                    Page p = new Page();
                    //string Url = pdfURL + "NearMap/" + dtMap.Rows[0]["image3"].ToString();
                    string Url = SiteConfiguration.GetDocumnetPath("NearMap", dtMap.Rows[0]["image3"].ToString());
                    pic3.Value = Url;
                }
                else
                {
                    pic3.Value = null;

                }


            }
        }
        int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, QuoteID);
        if (existinSignLog == 1)
        {
            // Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID(ProjectID);
            Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID_QDocNo(ProjectID, QuoteID);
            String ImageAdd = stsign.Image_Address;
            String Token = stsign.Token;
            //string Token = System.Web.HttpContext.Current.Request.QueryString["rt"];
            //if(string.IsNullOrEmpty(Token))
            //{
            //   Token = stsign.Token;
            //}
            Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

            Page p = new Page();
            String Virtualpath = stsign.Image_Address;
            int index = Virtualpath.IndexOf("userfiles");
            Virtualpath = Virtualpath.Substring(index);
            string img1 = p.Server.MapPath("~/" + Virtualpath);
            pictureBox14.Value = img1;

          
            try
            {
                textBox928.Value = (Convert.ToDateTime(stsign.Signed_Date)).ToString(("dd-MMM-yyyy"));
            }
            catch { }
            Clstbl_TokenData.tbl_TokenData_Update_SignFlag(Token, ProjectID, "True");

            /////////////////////////////////////////////////////////////
            //string fileName = stToken.QDocNo + "Quotation.pdf";
            //// ExportPdf(rpt);
            //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
            //SiteConfiguration.deleteimage(fileName, "quotedoc");
            /////////////////////////////////////////////////////////////////////
            // SavePDF(rpt, QuoteID);
            //SiteConfiguration.DeletePDFFile("quotedoc", fileName);

            // string nQoute = "s" + stToken.QDocNo;
            string nQoute = stToken.QDocNo;
            SavePDF(rpt, nQoute);

            DataTable dt2 = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteDoc(stToken.QDocNo);
            string ProjectQuoteDoc = dt2.Rows[0]["ProjectQuoteDoc"].ToString();
            string QuoteDoc = nQoute + "Quotation.pdf";
            ClstblProjects.tblProjectQuotes_UpdateQuoteDoc(ProjectQuoteDoc, QuoteDoc);

            string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "Quotation.pdf";
            SavePDFForSQ(rpt, SQFileNmae);

            //Server.Execute("~/app_code/);
            //HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);
        }
        else
        {
            pictureBox14.Value = null;         
            SavePDF(rpt, QuoteID);
        }
        //ExportPdf(rpt);      

    }
    public static void generate_receipt(string ProjectID)
    {
        DataTable dt = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.new_paymentreceipt();

        //Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table11 = rpt.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;

        //table1.DataSource = dt;
        table4.DataSource = dt;
        table3.DataSource = dt;
        table8.DataSource = dt;
        //table11.DataSource = dt;
        //table7.DataSource = dt;

        Telerik.Reporting.TextBox lblInvoiceNumber = rpt.Items.Find("lblInvoiceNumber", true)[0] as Telerik.Reporting.TextBox;
        lblInvoiceNumber.Value = stPro.InvoiceNumber;
        Telerik.Reporting.TextBox lblInvoiceSent = rpt.Items.Find("lblInvoiceSent", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            lblInvoiceSent.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch { }

        //DataTable dt1 = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
        DataTable dt1 = ClstblInvoicePayments.tblInvoicePayments_tbl_bank_SelectByProjectID(ProjectID);

        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox10 = rpt.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox20 = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        table1.DataSource = dt1;

        Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;

        string lbltax = string.Empty;
        string lblbasiccost = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbllessdeposit = string.Empty;

        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        try
        {
            if (!string.IsNullOrEmpty(stPro.SpecialDiscount) || stPro.SpecialDiscount != "")
            {
                lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
            }
            else
            {
                lbldiscount2 = "0";
            }
        }
        catch
        {

        }
        
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }

        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }


        txtfullretail.Value = "$ " + lblfullretail.ToString();
        txtdiscount2.Value = "- $ " + lbldiscount2.ToString();
        txtstc.Value = "- $ " + lblstc.ToString();
        txtgst.Value = "$ " + lbltotalcost.ToString();

        //textBox3.Value = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(dt1.Rows[0]["InvoicePayTotal"]));
        //if (!string.IsNullOrEmpty(Convert.ToString(dt1.Rows[0]["CCSurcharge"])))
        //{
        //    textBox10.Value = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(dt1.Rows[0]["CCSurcharge"]));
        //}
        //else
        //{
        //    textBox10.Value = "";
        //}
        try
        {
            textBox20.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(dt1.Rows[0]["TotalQuotePrice"]));
            //textBox1.Value = SiteConfiguration.ConvertToSystemDateDefault(Convert.ToString(dt1.Rows[0]["InvoicePayDate"]));
        }
        catch { }
        decimal paiddate = 0;
        decimal balown = 0;
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        if (dtCount.Rows[0]["BankInvoicePay"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["BankInvoicePay"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            //var balminus = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"]);
            //decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice); /*- balminus;*/
            //try
            //{
            Telerik.Reporting.TextBox lblBalanceOwing = rpt.Items.Find("lblBalanceOwing", true)[0] as Telerik.Reporting.TextBox;
            //Telerik.Reporting.TextBox lblBalanceOwing1 = rpt.Items.Find("lblBalanceOwing1", true)[0] as Telerik.Reporting.TextBox;
            lblBalanceOwing.Value = "Balance Due: $ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown)) + "        [Including – GST]";
            //  lblBalanceOwing1.Value = "$ "+SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
            //}
            //catch { }
        }
        //Telerik.Reporting.TextBox lblReceiptDate = rpt.Items.Find("lblReceiptDate", true)[0] as Telerik.Reporting.TextBox;
        //lblReceiptDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToString()));
        ExportPdf(rpt);
    }
    public static void generate_performainvoice(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.NInvoice();
        rpt.DocumentName = "PERFORMA INV";
        Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtheader", true)[0] as Telerik.Reporting.TextBox;
        txtheader.Value = "PERFORMA INV - " + stPro.InvoiceNumber;
        txtheader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(25D);


        try
        {
            Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
            lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;

        //Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;

        //table8.DataSource = dt;

        Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;

        ////==================================================
        if (stPro.InverterDetailsID != "")
        {

            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }


        ////********************  amount ****************
        //Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblrebate = rpt.Items.Find("lblrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;

        //Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(newtotalcost));
            }
            catch { }
        }
        catch { }

        string lblstc = string.Empty;
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }

        lblrebate.Value = "- $ " + lblstc.ToString();

        try
        {
            lblNetCost.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
        }
        catch { }
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(paiddate.ToString());
            }
            catch { }
            try
            {
                lblBalanceDue.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Value = "0";
            try
            {
                lblBalanceDue.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
            }
            catch { }
        }

        decimal invamnt = 0;

        try
        {

            invamnt = (Convert.ToDecimal(stPro.TotalQuotePrice) * Convert.ToDecimal(0.9091));

        }
        catch { }
        //if (stEmp.GSTPayment == "True")
        {
            decimal GST = 0;
            try
            {

                GST = ((invamnt) * 10) / 100;
            }
            catch { }


            lblCommissionGST.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(GST));
        }


        ExportPdf(rpt);
    }
    public static void generate_Taxinvoice(string ProjectID, String SaveOrDownload)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.NInvoice();

        rpt.DocumentName = "TAX INVOICE";

        Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtheader", true)[0] as Telerik.Reporting.TextBox;

        if(Roles.IsUserInRole("SalesRep"))
        {
            txtheader.Value = "PERFORMA INVOICE - " + stPro.InvoiceNumber;
            txtheader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
        }
        else
        {
            txtheader.Value = "TAX INVOICE - " + stPro.InvoiceNumber;
        }
        
        
        try
        {
            Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
            textBox1.Value = "INVOICE DATE : " + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch { }
        try
        {
            Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
            lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;

        //Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;

        //table8.DataSource = dt;

        Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;

        ////==================================================
        if (stPro.InverterDetailsID != "")
        {

            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }


        ////********************  amount ****************
        //Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblrebate = rpt.Items.Find("lblrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;

        //Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Value = "$ " + newtotalcost.ToString("F");
            }
            catch { }
        }
        catch { }

        string lblstc = string.Empty;
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = stPro.RECRebate == "" ? "" : Convert.ToDecimal(stPro.RECRebate).ToString("F");
        }
        else
        {
            lblstc = "0";
        }
        lblrebate.Value = "- $ " + lblstc.ToString();

        try
        {
            lblNetCost.Value = "$ " + stPro.TotalQuotePrice == "" ? "" : Convert.ToDecimal(stPro.TotalQuotePrice).ToString("F");
        }
        catch { }

        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["BankInvoicePay"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["BankInvoicePay"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Value = "$ " + paiddate.ToString("F");
            }
            catch { }
            try
            {
                lblBalanceDue.Value = "$ " + balown.ToString("F");
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Value = "0";
            try
            {
                lblBalanceDue.Value = "$ " + stPro.TotalQuotePrice == "" ? "" : Convert.ToDecimal(stPro.TotalQuotePrice).ToString("F");
            }
            catch { }
        }

        decimal invamnt = 0;
        try
        {
            invamnt = (Convert.ToDecimal(stPro.TotalQuotePrice) * Convert.ToDecimal(0.9091));

        }
        catch { }
        //if (stEmp.GSTPayment == "True")
        {
            decimal GST = 0;
            try
            {

                GST = ((invamnt) * 10) / 100;
            }
            catch { }

            lblCommissionGST.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(GST));
        }

        if (SaveOrDownload == "Save")
        {
            SavePDF2(rpt, stPro.ProjectNumber + "_TaxInvoice.pdf", "SavePDF2");
        }
        else if (SaveOrDownload == "Download")
        {
            ExportPdf(rpt);
        }

    }
    public static void generate_refundform(string ProjectID, string RefundID)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string inverterdetail = string.Empty;

        DataTable dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);
        // dt1 = Clscustomerlead.tblProjects_SelectByProjectID_demotelerik(ProjectID);
        DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist2(ProjectID, RefundID);
        SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByProjectID(ProjectID);
        SttblProjectRefund stRefund = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
        DataTable stsales = ClstblProjectRefund.tblEmployees_sales_manager(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.NRefundForm();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        // Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        // Telerik.Reporting.Table table5 = rpt.Items.Find("tabl5", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table tbl_sitedetail = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;

        //table1.DataSource = dt1;
        // table4.DataSource = dt;
        //table1.DataSource = dt1;
        //tbl_sitedetail.DataSource = dt1;

        table1.DataSource = dt1;
        table4.DataSource = dt1;
        Telerik.Reporting.TextBox txtSTATUS = rpt.Items.Find("txtSTATUS", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtNOOFPANELS = rpt.Items.Find("txtNOOFPANELS", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtAPPLICATION = rpt.Items.Find("txtAPPLICATION", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDEPOSITDATE = rpt.Items.Find("txtDEPOSITDATE", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtENTRYDATE = rpt.Items.Find("txtENTRYDATE", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtPROMOTION = rpt.Items.Find("txtPROMOTION", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtBANKNAME = rpt.Items.Find("txtBANKNAME", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtBSBNO = rpt.Items.Find("txtBSBNO", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtACCNO = rpt.Items.Find("txtACCNO", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAMOUNT = rpt.Items.Find("txtAMOUNT", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtACCOUNTNAME = rpt.Items.Find("txtACCOUNTNAME", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtREFUNDDATE = rpt.Items.Find("txtREFUNDDATE", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtSIGNATURE = rpt.Items.Find("txtSIGNATURE", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber1 = rpt.Items.Find("txtProjectNumber1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;


        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNumber.Value = stPro.ProjectNumber;
        txtProjectNumber1.Value = stPro.ProjectNumber;
        txtBSBNO.Value = stRefund.BSBNo;
        txtACCNO.Value = stRefund.AccNo;
        txtAMOUNT.Value = "$ " + Convert.ToDecimal(stRefund.Amount).ToString("#,##0.00");
        txtACCOUNTNAME.Value = stRefund.AccName;
        txtBANKNAME.Value = stRefund.BankName;

        if (stRefund.AccActionDate == "" || stRefund.AccActionDate == null)
        {
            txtREFUNDDATE.Value = "";
        }
        else
        {
            try
            {
                txtREFUNDDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.AccActionDate));
            }
            catch { }
        }

        textBox25.Value = stRefund.Notes;

        if (stRefund.Status == "1")
        {
            txtSTATUS.Value = "Successfull";
        }
        else
        {
            txtSTATUS.Value = "Failed";
        }
        txtNOOFPANELS.Value = stPro.NumberPanels;
        if (stRefund.CreateDate == "" || stRefund.CreateDate == null)
        {
            txtENTRYDATE.Value = "";
        }
        else
        {
            try
            {
                txtENTRYDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.CreateDate));
            }
            catch { }
        }
        if (stPro.DepositReceived == "" || stPro.DepositReceived == null)
        {
            txtDEPOSITDATE.Value = "";
        }
        else
        {
            try
            {
                txtDEPOSITDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.DepositReceived));
            }
            catch { }
        }
        try
        {
            textBox21.Value = stsales.Rows[0]["sales_manager"].ToString();
        }
        catch { }

        ExportPdf(rpt);

    }
    public static void generate_welcomeletter(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.new_welcomletter1();


        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table_Contact = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table2 = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt1;
        table_Contact.DataSource = dt1;
        // table2.DataSource = dt1;
        table3.DataSource = dt1;
        table4.DataSource = dt1;
        table5.DataSource = dt1;
        table6.DataSource = dt1;
        table7.DataSource = dt1;
        table8.DataSource = dt1;

        //Telerik.Reporting.TextBox txtInverterdetail = rpt.Items.Find("txtInverterdetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInverterdetail1 = rpt.Items.Find("txtInverterdetail1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeter = rpt.Items.Find("txtmeter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtswitch = rpt.Items.Find("txtswitch", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txtgetdate = rpt.Items.Find("txtgetdate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtpaymentplan = rpt.Items.Find("txtpaymentplan", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox textBox214 = rpt.Items.Find("textBox214", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txtABN = rpt.Items.Find("textBox215", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNumber.Value = stPro.ProjectNumber;

        string inverterdetail = string.Empty;
        string lblbasiccost = string.Empty;
        string lblreq = "+ N/A";
        string lblcompl = "+ N/A";
        string lbltax = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbldeposit = string.Empty;
        string lbllessdeposit = string.Empty;
        string otherdetail = string.Empty;
        string paymentplan = string.Empty;



        //==================================================
        try
        {
            if (dt1.Rows[0]["FinanceWith"].ToString() != string.Empty)
            {
                paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString();//
                if (st2.FinanceWithDeposit != string.Empty)
                {
                    paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%";
                    if (dt1.Rows[0]["PaymentType"].ToString() != string.Empty)
                    {
                        paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%" + " for " + dt1.Rows[0]["PaymentType"].ToString();

                    }
                }

            }

            //txtpaymentplan.Value = paymentplan;

            //if (st2.PromoText != string.Empty)
            //{
            //    textBox214.Visible = true;
            //    otherdetail = st2.PromoText;
            //}
            //else
            //{
            //    textBox214.Visible = false;
            //}

            txtotherdetail.Value = otherdetail;
        }
        catch
        {

        }
        if (stPro.InverterDetailsID != "")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }
        if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. ";
        }
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }

        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldeposit = "0";
        }
        try
        {
            lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        }
        catch { }

        txtmeterphase.Value = stPro.NMINumber;

        txtmeter.Value = "Meter Phase : " + stPro.MeterPhase;
        txtswitch.Value = "Switchboard Upgrades : " + stPro.meterupgrade;

        //txtInverterdetail.Value = inverterdetail.ToString();
        txtInverterdetail1.Value = inverterdetail.ToString();
        txtbasiccost.Value = lblbasiccost.ToString();
        txtextra.Value = "+ " + lbltax.ToString();
        txtreq.Value = lblreq.ToString();
        txtcompl.Value = lblcompl.ToString();
        txtfullretail.Value = lblfullretail.ToString();
        txtdiscount2.Value = "- " + lbldiscount2.ToString();
        txtstc.Value = "- " + lblstc.ToString();
        txtgst.Value = lbltotalcost.ToString();
        txtdeposit.Value = lbldeposit.ToString();
        txtlessdeposit.Value = lbllessdeposit.ToString();
        // txtcustname.Value = stPro.Customer;
        // txtgetdate.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));


        try
        {



            Telerik.Reporting.PictureBox picbxnearby = rpt.Items.Find("picbxnearby", true)[0] as Telerik.Reporting.PictureBox;

            if (!string.IsNullOrEmpty(stPro.nearmapdoc))
            {
                //string pdfURL = ConfigurationManager.AppSettings["cdnURL"];

                //string img1 = pdfURL + "NearMap/" + stPro.nearmapdoc;
                string img1 = SiteConfiguration.GetDocumnetPath("NearMap", stPro.nearmapdoc);
                //Image myImg1 = Image.FromFile(img1);
                //picbxnearby.Value = pdfURL + "NearMap/" + stPro.nearmapdoc;
                picbxnearby.Value = SiteConfiguration.GetDocumnetPath("NearMap", stPro.nearmapdoc);
            }
            else
            {
                picbxnearby.Visible = false;
                picbxnearby.Value = null;
            }
        }
        catch { }




        //try
        //{
        //    string ABN = dt.Rows[0]["CustWebSiteLink"].ToString();


        //    if (ABN != "" && ABN != "0" && ABN != null)
        //    {
        //        txtABN.Value = ABN;
        //    }
        //    //else
        //    //{
        //    //    txtABN.Value = "";
        //    //}
        //}
        //catch { }
        ExportPdf(rpt);
    }
    public static void generate_PickList(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dtitemlist = new DataTable();
        DataTable dtlist = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        dtitemlist = ClsQuickStock.tbl_PicklistItemDetail_LastPickListWise(ProjectID);
        dtlist = ClsQuickStock.tbl_Picklistlog_LastPickListWise(ProjectID);


        //Telerik.Reporting.Report rpt = new EurosolarReporting.Npicklist_new();
        Telerik.Reporting.Report rpt = new EurosolarReporting.Npicklist_Pickwise();


        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        // Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        
        table1.DataSource = dt;
        //table2.DataSource = dt;
        table5.DataSource = dtitemlist;

        table3.DataSource = dt;
        //table8.DataSource = dt;
        table2.DataSource = dtitemlist;

        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox113 = rpt.Items.Find("textBox113", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox txrintadd1 = rpt.Items.Find("txrintadd1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox119 = rpt.Items.Find("textBox119", true)[0] as Telerik.Reporting.TextBox;
        //  Telerik.Reporting.TextBox txrintadd2 = rpt.Items.Find("txrintadd2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txrintadd1_1 = rpt.Items.Find("txrintadd1_1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox63 = rpt.Items.Find("textBox63", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox96 = rpt.Items.Find("textBox96", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox29 = rpt.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox31 = rpt.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txrintadd2_2 = rpt.Items.Find("txrintadd2_2", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblProject = rpt.Items.Find("lblProject", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblProject1 = rpt.Items.Find("lblProject1", true)[0] as Telerik.Reporting.TextBox;
        //lblProject.Value = stPro.Project;
        //lblProject1.Value = stPro.Project;

        //Telerik.Reporting.TextBox lblPrinted = rpt.Items.Find("lblPrinted", true)[0] as Telerik.Reporting.TextBox;
        //lblPrinted.Value = "Printed:  " + string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14)));
        //Telerik.Reporting.TextBox lblPrinted1 = rpt.Items.Find("lblPrinted1", true)[0] as Telerik.Reporting.TextBox;
        //lblPrinted1.Value = "Printed:  " + string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14)));


        //textBox96.Value = dt.Rows[0]["ProjectNumber"].ToString() + "/" + dtlist.Rows[0]["Pickid"].ToString();
        //textBox29.Value = dt.Rows[0]["ProjectNumber"].ToString() + "/" + dtlist.Rows[0]["Pickid"].ToString();

        textBox96.Value = dtlist.Rows[0]["Projectnumber"].ToString() + "/" + dtlist.Rows[0]["ID"].ToString();
        textBox29.Value = dtlist.Rows[0]["Projectnumber"].ToString() + "/" + dtlist.Rows[0]["ID"].ToString();
        textBox31.Value = dtlist.Rows[0]["InstallerName"].ToString();


        //if (stPro.PanelBrandID != string.Empty)
        //{
        //    SttblStockItems stPan = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.PanelBrandID);
        //    Telerik.Reporting.TextBox lblPanDesc = rpt.Items.Find("lblPanDesc", true)[0] as Telerik.Reporting.TextBox;
        //    lblPanDesc.Value = stPan.StockDescription;
        //    Telerik.Reporting.TextBox lblPanDesc1 = rpt.Items.Find("lblPanDesc1", true)[0] as Telerik.Reporting.TextBox;
        //    lblPanDesc1.Value = stPan.StockDescription;
        //}

        //Telerik.Reporting.TextBox lblqty1 = rpt.Items.Find("lblqty1", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty2 = rpt.Items.Find("lblqty2", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty11 = rpt.Items.Find("lblqty11", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty21 = rpt.Items.Find("lblqty21", true)[0] as Telerik.Reporting.TextBox;

        //if (stPro.inverterqty != string.Empty)
        //{

        //    lblqty1.Value = stPro.inverterqty;
        //    lblqty11.Value = stPro.inverterqty;
        //}
        //else
        //{

        //    lblqty1.Value = "0";
        //    lblqty11.Value = "0";
        //}
        //if (stPro.inverterqty2 != string.Empty)
        //{
        //    lblqty2.Value = stPro.inverterqty2;
        //    lblqty21.Value = stPro.inverterqty2;
        //}
        //else
        //{
        //    lblqty2.Value = "0";
        //    lblqty21.Value = "0";
        //}


        //if (stPro.InverterDetailsID != string.Empty)
        //{
        //    table4.DataSource = dt;
        //    table7.DataSource = dt;
        //}
        //else
        //{
        //    table4.Visible = false;
        //    table7.Visible = false;
        //}
        //if (stPro.SecondInverterDetailsID != string.Empty)
        //{
        //    table5.DataSource = dt;
        //    table6.DataSource = dt;
        //}
        //else
        //{
        //    table5.Visible = false;
        //    table6.DataSource = dt;
        //}

        //try
        //{
        //    textBox3.Value = "Date : " + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime("InstallBookingDate".ToString()));
        //}
        //catch { }
        if (stPro.InstallBookingDate == "" || stPro.InstallBookingDate == null)
        {
            textBox3.Value = "";
            textBox113.Value = "";
        }
        else
        {
            try
            {
                textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
                textBox113.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }
        }
        try
        {
            //txrintadd1.Value = dt.Rows[0]["PostalAddress"].ToString();
            //txrintadd1.Value = dt.Rows[0]["InstallAddress"].ToString();
            //textBox119.Value = dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();

            txrintadd1.Value = dt.Rows[0]["InstallAddress"].ToString();
            textBox119.Value = dt.Rows[0]["InstallCity"].ToString() + ", " + dt.Rows[0]["InstallState"].ToString() + " - " + dt.Rows[0]["InstallPostCode"].ToString();

            // txrintadd2.Value = dt.Rows[0]["Installeraddress2"].ToString();
            // txrintadd1_1.Value = dt.Rows[0]["PostalAddress"].ToString();
            //txrintadd1_1.Value= dt.Rows[0]["InstallCity"].ToString() + ", " + dt.Rows[0]["InstallState"].ToString() + " - " + dt.Rows[0]["InstallPostCode"].ToString();
            //textBox63.Value = dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();

            txrintadd1_1.Value = dt.Rows[0]["InstallAddress"].ToString();
            textBox63.Value = dt.Rows[0]["InstallCity"].ToString() + ", " + dt.Rows[0]["InstallState"].ToString() + " - " + dt.Rows[0]["InstallPostCode"].ToString();
            // txrintadd2_2.Value = dt.Rows[0]["Installeraddress2"].ToString();
        }
        catch { }

        Telerik.Reporting.TextBox textBox61 = rpt.Items.Find("textBox61", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox122 = rpt.Items.Find("textBox122", true)[0] as Telerik.Reporting.TextBox;

        

        textBox61.Value = stPro.InstallerNotes; /*stPro.notes;*/
        textBox122.Value = stPro.InstallerNotes; /*stPro.notes;*/ /*stPro.InstallerNotes;*/
        ExportPdf(rpt);


    }

    public static void generate_solar_system(string ProjectID)
    {
        //System.Web.HttpContext.Current.Response.Write(ProjectID);
        //System.Web.HttpContext.Current.Response.End();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string inverterdetail = string.Empty;
        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.solar_system();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox2 = rpt.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox4 = rpt.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox5 = rpt.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox6 = rpt.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            textBox1.Value = dt1.Rows[0]["ContFirst"].ToString();
            textBox2.Value = dt1.Rows[0]["ContLast"].ToString();
            textBox3.Value = dt1.Rows[0]["StreetAddress"].ToString();
            // textBox4.Value = dt1.Rows[0]["StreetAddress"].ToString();
            textBox4.Value = dt1.Rows[0]["InstallAddress"].ToString();
            textBox5.Value = dt1.Rows[0]["PostalCity"].ToString();
            textBox6.Value = dt1.Rows[0]["PostalState"].ToString();
            textBox7.Value = dt1.Rows[0]["PostalPostCode"].ToString();
            //System.Web.HttpContext.Current.Response.Write(dt1.Rows[0]["ContFirst"].ToString()+"--"+ dt1.Rows[0]["ContLast"].ToString());
            //System.Web.HttpContext.Current.Response.End();
        }
        catch { }

        ExportPdf(rpt);
    }

    public static void generate_WholesalePicklist(string WholesaleOrderID)
    {

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(WholesaleOrderID);


        Telerik.Reporting.Report rpt = new EurosolarReporting.WholesalePicklist();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            try
            {
                textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.DateOrdered));
            }
            catch { }
            textBox7.Value = st.InvoiceNo;
            textBox11.Value = st.ReferenceNo;

            table3.DataSource = dt;
        }
        catch { }

        ExportPdf(rpt);
    }

    public static void generate_EWR(string ProjectID)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        SttblContacts stcont = ClstblContacts.tblContacts_SelectByCustomerID(stPro.CustomerID);
        DataTable dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        //To get installer eleclicense
        string InstallerContactId = stPro.Installer;
        DataTable dt2;
        if (string.IsNullOrEmpty(InstallerContactId))
        {
            InstallerContactId = "";
            dt2 = ClstblContacts.tblContacts_SelectElectricianByContactID(InstallerContactId);
        }
        else
        {
            dt2 = ClstblContacts.tblContacts_SelectElectricianByContactID(InstallerContactId);
        }



        Telerik.Reporting.Report rpt = new EurosolarReporting.EWR();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmobile = rpt.Items.Find("txtmobile", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstreetno = rpt.Items.Find("txtstreetno", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress = rpt.Items.Find("txtaddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsuburb = rpt.Items.Find("txtsuburb", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsolarpanel = rpt.Items.Find("txtsolarpanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinverter = rpt.Items.Find("txtinverter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinstaller = rpt.Items.Find("txtinstaller", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress2 = rpt.Items.Find("txtaddress2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcust2 = rpt.Items.Find("txtcust2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txt_elec_licence = rpt.Items.Find("txt_elec_licence", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmobile2 = rpt.Items.Find("txtmobile2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtday = rpt.Items.Find("txtday", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmonth = rpt.Items.Find("txtmonth", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtyear = rpt.Items.Find("txtyear", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtday2 = rpt.Items.Find("txtday2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmonth2 = rpt.Items.Find("txtmonth2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtyear2 = rpt.Items.Find("txtyear2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtprojectno = rpt.Items.Find("txtprojectno", true)[0] as Telerik.Reporting.TextBox;


        try
        {

            //System.Web.HttpContext.Current.Response.Write(string.Format("{0:yyyy}", Convert.ToDateTime(stPro.InstallBookingDate)));
            //System.Web.HttpContext.Current.Response.End();
            string day = "", month = "", year = "";
            if (!string.IsNullOrEmpty(stPro.InstallBookingDate))
            {
                try
                {
                    day = string.Format("{0:dd}", Convert.ToDateTime(stPro.InstallBookingDate));
                    month = string.Format("{0:MM}", Convert.ToDateTime(stPro.InstallBookingDate));
                    year = string.Format("{0:yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
                }
                catch { }
            }
            txtcustname.Value = st.Customer;
            txtmobile.Value = stcont.ContMobile;
            txtstreetno.Value = st.street_number;
            txtaddress.Value = st.street_name + " " + st.street_type + " " + st.StreetCity;
            txtsuburb.Value = st.StreetState + " " + st.StreetPostCode;
            txtsolarpanel.Value = stPro.SystemCapKW;
            txtinverter.Value = stPro.InverterOutput;
            txtinstaller.Value = dt.Rows[0]["InstallerName"].ToString();
            txtaddress2.Value = st.StreetAddress + " " + st.StreetState + " " + st.StreetPostCode;
            txtcust2.Value = dt.Rows[0]["InstallerName"].ToString();
            txtmobile2.Value = stcont.ContMobile;
            txtday.Value = day;
            txtday2.Value = day;
            txtmonth.Value = month;
            txtmonth2.Value = month;
            txtyear.Value = year;
            txtyear2.Value = year;
            txtprojectno.Value = stPro.ProjectNumber;
            if (dt2.Rows.Count > 0)
            {
                txt_elec_licence.Value = dt2.Rows[0]["ElecLicence"].ToString();
            }
            else
            {
                txt_elec_licence.Value = "";
            }

        }
        catch { }


        ExportPdf(rpt);
    }

    public static void generate_customeracknowledgement(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Ncustomeracknowledgement();
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox44 = rpt.Items.Find("textBox44", true)[0] as Telerik.Reporting.TextBox;

        table1.DataSource = dt;
        try
        {
            textBox44.Value = dt.Rows[0]["Customer"].ToString();
        }
        catch { }

        Telerik.Reporting.TextBox textBox45 = rpt.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            textBox45.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        }
        catch { }

        ExportPdf(rpt);
    }
    public static void generate_maintenance(string ProjectID, string ProjectMaintenanceID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notesMaintenance(ProjectID, ProjectMaintenanceID);
        SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintainanceByProjectID(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Nmaintainance();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        //Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblmaintainaceno", true)[0] as Telerik.Reporting.TextBox;
        //lblinvoiceno.Value = "Maintenance Contract - " + stPro.InvoiceNumber;

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;


        Telerik.Reporting.TextBox txtInstallBookingDate = rpt.Items.Find("txtInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceCost = rpt.Items.Find("txtMtceCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceDiscount = rpt.Items.Find("txtMtceDiscount", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceBalance = rpt.Items.Find("txtMtceBalance", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox24 = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox30 = rpt.Items.Find("textBox30", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox16 = rpt.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox29 = rpt.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox18 = rpt.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox34 = rpt.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox36 = rpt.Items.Find("textBox36", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox46 = rpt.Items.Find("textBox46", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;

        table3.DataSource = dt;
        //table4.DataSource = dt;
        table8.DataSource = dt;
        //  table6.DataSource = dt;
        //table1.DataSource = dt;
        // txtInstallBookingDate.Value = Convert.ToDateTime(stPro.InstallBookingDate).ToString("dd MMM yyyy");
        //txtInstallBookingDate.Value = stPro.InstallBookingDate;
        if (stPro.InstallBookingDate == "" || stPro.InstallBookingDate == null)
        {
            txtInstallBookingDate.Value = "";
        }
        else
        {
            try
            {
                txtInstallBookingDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }
        }

        try
        {
            txtMtceCost.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceCost);
        }
        catch { }

        try
        {
            txtMtceDiscount.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceDiscount);
        }
        catch { }
        try
        {
            txtMtceBalance.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceBalance);
        }
        catch { }
        try
        {
            textBox24.Value = stPro.ProjectNumber;
        }
        catch { }
        try
        {
            textBox25.Value = stPro.ManualQuoteNumber;
        }
        catch { }
        try
        {
            textBox30.Value = stPro.NextMaintenanceCall;
        }
        catch { }
        try
        {
            textBox16.Value = dt.Rows[0]["CustomerInput"].ToString();
        }
        catch { }
        try
        {
            textBox18.Value = dt.Rows[0]["ActionRequired"].ToString();
        }
        catch { }
        try
        {
            textBox29.Value = dt.Rows[0]["FaultIdentified"].ToString();
        }
        catch { }
        try
        {
            textBox34.Value = dt.Rows[0]["WorkDone"].ToString();
        }
        catch { }
        try
        {
            textBox36.Value = dt.Rows[0]["Contact"].ToString();
        }
        catch { }
        try
        {
            string panelbrand = dt.Rows[0]["PanelBrandName"].ToString();
            string panelqty = dt.Rows[0]["NumberPanels"].ToString();
            textBox46.Value = panelqty + "X" + panelbrand;
        }
        catch { }
        try
        {
            string inv1brand = dt.Rows[0]["InverterDetailsName"].ToString();
            string inv1qty = dt.Rows[0]["inverterqty"].ToString();
            string inv2brand = dt.Rows[0]["SecondInverterDetails"].ToString();
            string inv2qty = dt.Rows[0]["inverterqty"].ToString();
            textBox21.Value = inv1qty + "X" + inv1brand;
            if (!string.IsNullOrEmpty(inv2qty))
            {
                textBox21.Value = inv1qty + "X" + inv1brand + "+" + inv2qty + "X" + inv2brand;
            }
        }
        catch { }
        ExportPdf(rpt);

    }



    public static void generate_stcug(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);



        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.STCUGForm();

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table10 = rpt.Items.Find("table10", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table11 = rpt.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table12 = rpt.Items.Find("table12", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table13 = rpt.Items.Find("table13", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table15 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        table1.DataSource = dt;
        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        table8.DataSource = dt;
        table10.DataSource = dt;
        table11.DataSource = dt;
        table12.DataSource = dt;
        table13.DataSource = dt;
        table15.DataSource = dt;



        Telerik.Reporting.TextBox ProjectNumber3 = rpt.Items.Find("ProjectNumber3", true)[0] as Telerik.Reporting.TextBox;
        ProjectNumber3.Value = stPro.ProjectNumber;
        Telerik.Reporting.TextBox litPanelDetails = rpt.Items.Find("litPanelDetails", true)[0] as Telerik.Reporting.TextBox;
        litPanelDetails.Value = stPro.PanelDetails;

        Telerik.Reporting.TextBox lblInverterBrand2 = rpt.Items.Find("lblInverterBrand2", true)[0] as Telerik.Reporting.TextBox;
        lblInverterBrand2.Value = stPro.InverterBrand;

        Telerik.Reporting.TextBox lblInverterModel2 = rpt.Items.Find("lblInverterModel2", true)[0] as Telerik.Reporting.TextBox;
        lblInverterModel2.Value = stPro.InverterModel;

        Telerik.Reporting.TextBox lblInverterSeries2 = rpt.Items.Find("lblInverterSeries2", true)[0] as Telerik.Reporting.TextBox;
        lblInverterSeries2.Value = stPro.InverterSeries;



        Telerik.Reporting.TextBox lblProjectNumber5 = rpt.Items.Find("lblProjectNumber5", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblProjectNumber4 = rpt.Items.Find("lblProjectNumber4", true)[0] as Telerik.Reporting.TextBox;
        lblProjectNumber5.Value = lblProjectNumber5.Value = stPro.ProjectNumber;


        Telerik.Reporting.TextBox lblInstallerName7 = rpt.Items.Find("lblInstallerName7", true)[0] as Telerik.Reporting.TextBox;
        lblInstallerName7.Value = stPro.InstallerName;
        Telerik.Reporting.TextBox lblFinanceOption = rpt.Items.Find("lblFinanceOption", true)[0] as Telerik.Reporting.TextBox;
        lblFinanceOption.Value = stPro.FinanceWith;


        Telerik.Reporting.TextBox lblMeterPhase = rpt.Items.Find("lblMeterPhase", true)[0] as Telerik.Reporting.TextBox;
        if (stPro.MeterPhase == "1")
        {
            lblMeterPhase.Value = "Single";
        }
        if (stPro.MeterPhase == "2")
        {
            lblMeterPhase.Value = "Double";
        }
        if (stPro.MeterPhase == "3")
        {
            lblMeterPhase.Value = "Three";
        }

        Telerik.Reporting.TextBox lblOffPeak = rpt.Items.Find("lblOffPeak", true)[0] as Telerik.Reporting.TextBox;
        if (stPro.OffPeak == "True")
        {
            lblOffPeak.Value = "Yes";
        }
        else
        {
            lblOffPeak.Value = "No";
        }
        Telerik.Reporting.TextBox lblRetailer = rpt.Items.Find("lblRetailer", true)[0] as Telerik.Reporting.TextBox;
        lblRetailer.Value = stPro.ElecRetailer;

        Telerik.Reporting.TextBox lblPeakMeterNo = rpt.Items.Find("lblPeakMeterNo", true)[0] as Telerik.Reporting.TextBox;
        lblPeakMeterNo.Value = stPro.MeterNumber1;


        Telerik.Reporting.HtmlTextBox txtpaymnetfrm = rpt.Items.Find("txtpaymnetfrm", true)[0] as Telerik.Reporting.HtmlTextBox;
        txtpaymnetfrm.Value = "<b> Paymnet Form </b><br/>Project: " + stPro.ProjectNumber + "<br/>Customer: " + stCont.ContFirst + " " + stCont.ContLast + "<br/> Installation at: " + stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;






        Telerik.Reporting.TextBox lblInstallBookingDate = rpt.Items.Find("lblInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            lblInstallBookingDate.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        }
        catch { }
        ExportPdf(rpt);

    }
    public static void generate_STCform(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);



        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.STCFormNewImage();


        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table10 = rpt.Items.Find("table10", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table11 = rpt.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table12 = rpt.Items.Find("table12", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table13 = rpt.Items.Find("table13", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table14 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table15 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        //Change for New
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table9 = rpt.Items.Find("table9", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.TextBox textBox56 = rpt.Items.Find("textBox56", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox128 = rpt.Items.Find("textBox128", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox140 = rpt.Items.Find("textBox140", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox146 = rpt.Items.Find("textBox146", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox153 = rpt.Items.Find("textBox153", true)[0] as Telerik.Reporting.TextBox;


        //Testing
        Telerik.Reporting.TextBox textBox102 = rpt.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox99 = rpt.Items.Find("textBox99", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox97 = rpt.Items.Find("textBox97", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox94 = rpt.Items.Find("textBox94", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox92 = rpt.Items.Find("textBox92", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox90 = rpt.Items.Find("textBox90", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox91 = rpt.Items.Find("textBox91", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox87 = rpt.Items.Find("textBox87", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox84 = rpt.Items.Find("textBox84", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox78 = rpt.Items.Find("textBox78", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox82 = rpt.Items.Find("textBox82", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox81 = rpt.Items.Find("textBox81", true)[0] as Telerik.Reporting.TextBox;
        //

        Telerik.Reporting.Table table19 = rpt.Items.Find("table19", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox52 = rpt.Items.Find("textBox52", true)[0] as Telerik.Reporting.TextBox;


        table19.DataSource = dt;
        try
        {
            textBox52.Value = dt.Rows[0]["Customer"].ToString();
        }
        catch { }

        Telerik.Reporting.TextBox textBox45 = rpt.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            textBox25.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        }
        catch { }



        if (st.ReceivedCredits == "True")
        {
            textBox128.Value = "YES";
        }
        else
        {
            textBox128.Value = "NO";
        }
        if (st.RebateApproved == "True")
        {
            textBox140.Value = "YES";
        }
        else
        {
            textBox140.Value = "NO";
        }
        if (st.CreditEligible == "True")
        {
            textBox146.Value = "YES";
        }
        else
        {
            textBox146.Value = "NO";
        }
        if (st.AdditionalSystem == "True")
        {
            textBox153.Value = "YES";
        }
        else
        {
            textBox153.Value = "NO";
        }

        //Change for New End
        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        table8.DataSource = dt;
        table10.DataSource = dt;
        table11.DataSource = dt;
        table12.DataSource = dt;
        table13.DataSource = dt;
        //table14.DataSource = dt;
        //table15.DataSource = dt;
        //Change for New
        table4.DataSource = dt;
        table5.DataSource = dt;
        table9.DataSource = dt;

        //testing
        textBox102.Value = dt.Rows[0]["ContFirst"].ToString();
        textBox99.Value = dt.Rows[0]["ContLast"].ToString();
        textBox97.Value = dt.Rows[0]["StreetAddress"].ToString();
        textBox94.Value = dt.Rows[0]["StreetCity"].ToString();
        textBox92.Value = dt.Rows[0]["StreetPostCode"].ToString();
        textBox91.Value = dt.Rows[0]["InstallerName"].ToString();
        textBox90.Value = dt.Rows[0]["InstallerName"].ToString();
        textBox87.Value = dt.Rows[0]["Electricianmolbile"].ToString();
        textBox84.Value = dt.Rows[0]["ElecLicence"].ToString();
        try
        {
            textBox82.Value = string.Format("{0:dd}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            textBox81.Value = string.Format("{0:MMM}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            textBox78.Value = string.Format("{0:yyyy}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            //

            textBox56.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        }
        catch { }




        //Telerik.Reporting.TextBox ProjectNumber3 = rpt.Items.Find("ProjectNumber3", true)[0] as Telerik.Reporting.TextBox;
        //ProjectNumber3.Value = stPro.ProjectNumber;
        //Telerik.Reporting.TextBox litPanelDetails = rpt.Items.Find("litPanelDetails", true)[0] as Telerik.Reporting.TextBox;
        //litPanelDetails.Value = stPro.PanelDetails;

        //Telerik.Reporting.TextBox lblInverterBrand2 = rpt.Items.Find("lblInverterBrand2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterBrand2.Value = stPro.InverterBrand;

        //Telerik.Reporting.TextBox lblInverterModel2 = rpt.Items.Find("lblInverterModel2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterModel2.Value = stPro.InverterModel;

        //Telerik.Reporting.TextBox lblInverterSeries2 = rpt.Items.Find("lblInverterSeries2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterSeries2.Value = stPro.InverterSeries;



        //Telerik.Reporting.TextBox lblProjectNumber5 = rpt.Items.Find("lblProjectNumber5", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblProjectNumber4 = rpt.Items.Find("lblProjectNumber4", true)[0] as Telerik.Reporting.TextBox;
        //lblProjectNumber5.Value = lblProjectNumber5.Value = stPro.ProjectNumber;


        //Telerik.Reporting.TextBox lblInstallerName7 = rpt.Items.Find("lblInstallerName7", true)[0] as Telerik.Reporting.TextBox;
        //lblInstallerName7.Value = stPro.InstallerName;
        //Telerik.Reporting.TextBox lblFinanceOption = rpt.Items.Find("lblFinanceOption", true)[0] as Telerik.Reporting.TextBox;
        //lblFinanceOption.Value = stPro.FinanceWith;


        //Telerik.Reporting.TextBox lblMeterPhase = rpt.Items.Find("lblMeterPhase", true)[0] as Telerik.Reporting.TextBox;
        //if (stPro.MeterPhase == "1")
        //{
        //    lblMeterPhase.Value = "Single";
        //}
        //if (stPro.MeterPhase == "2")
        //{
        //    lblMeterPhase.Value = "Double";
        //}
        //if (stPro.MeterPhase == "3")
        //{
        //    lblMeterPhase.Value = "Three";
        //}

        //Telerik.Reporting.TextBox lblOffPeak = rpt.Items.Find("lblOffPeak", true)[0] as Telerik.Reporting.TextBox;
        //if (stPro.OffPeak == "True")
        //{
        //    lblOffPeak.Value = "Yes";
        //}
        //else
        //{
        //    lblOffPeak.Value = "No";
        //}
        //Telerik.Reporting.TextBox lblRetailer = rpt.Items.Find("lblRetailer", true)[0] as Telerik.Reporting.TextBox;
        //lblRetailer.Value = stPro.ElecRetailer;

        //Telerik.Reporting.TextBox lblPeakMeterNo = rpt.Items.Find("lblPeakMeterNo", true)[0] as Telerik.Reporting.TextBox;
        //lblPeakMeterNo.Value = stPro.MeterNumber1;


        //Telerik.Reporting.HtmlTextBox txtpaymnetfrm = rpt.Items.Find("txtpaymnetfrm", true)[0] as Telerik.Reporting.HtmlTextBox;
        //txtpaymnetfrm.Value = "<b> Paymnet Form </b><br/>Project: " + stPro.ProjectNumber + "<br/>Customer: " + stCont.ContFirst + " " + stCont.ContLast + "<br/> Installation at: " + stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;


        //Telerik.Reporting.TextBox lblInstallBookingDate = rpt.Items.Find("lblInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        //try
        //{
        //    lblInstallBookingDate.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        //}
        //catch { }


        ExportPdf(rpt);

    }
    public static void generate_Testing(string ProjectID)
    {
        DataTable dt = new DataTable();

        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        Telerik.Reporting.Report rpt = new EurosolarReporting.Testing_new();

        //Telerik.Reporting.Table table15 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        //table15.DataSource = dt;
        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox2 = rpt.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox4 = rpt.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox5 = rpt.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox6 = rpt.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox8 = rpt.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox9 = rpt.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox10 = rpt.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox12 = rpt.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;

        textBox1.Value = dt.Rows[0]["ContFirst"].ToString();
        textBox2.Value = dt.Rows[0]["ContLast"].ToString();
        textBox3.Value = dt.Rows[0]["StreetAddress"].ToString();
        textBox4.Value = dt.Rows[0]["StreetCity"].ToString();
        textBox5.Value = dt.Rows[0]["StreetPostCode"].ToString();
        textBox6.Value = dt.Rows[0]["InstallerName"].ToString();
        textBox7.Value = dt.Rows[0]["InstallerName"].ToString();
        textBox8.Value = dt.Rows[0]["Electricianmolbile"].ToString();
        textBox9.Value = dt.Rows[0]["ElecLicence"].ToString();
        try
        {
            textBox10.Value = string.Format("{0:dd}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            textBox11.Value = string.Format("{0:MMM}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            textBox12.Value = string.Format("{0:yyyy}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
        }
        catch { }


        ExportPdf(rpt);
    }



    public static void generate_mtceinvoice(string ProjectID, string SaveOrDownload)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.performamtceinvoice();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblinvoiceno", true)[0] as Telerik.Reporting.TextBox;
        lblinvoiceno.Value = "Tax Invoice No: " + stPro.InvoiceNumber;
        try
        {
            Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
            lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table3.DataSource = dt;
        table8.DataSource = dt;

        Telerik.Reporting.TextBox lblInverterDetails = rpt.Items.Find("lblInverterDetails", true)[0] as Telerik.Reporting.TextBox;
        lblInverterDetails.Value = stPro.InverterDetails;

        //********************  amount ****************

        Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(newtotalcost));
            }
            catch { }
        }
        catch { }

        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
            }
            catch { }
            try
            {
                lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Value = "0";
            try
            {
                lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            }
            catch { }
        }
        try
        {
            lblCommissionGST.Value = "Note: The Net Cost above includes GST of: $" + SiteConfiguration.ChangeCurrencyVal(stPro.InvoiceGST);
        }
        catch { }

        if (SaveOrDownload == "Download")
        {
            ExportPdf(rpt);
        }
        else if (SaveOrDownload == "Save")
        {
            SavePDF2(rpt, stPro.ProjectNumber + "_TaxInvoice.pdf", "SavePDF2");
        }
    }
    public static void generate_performamtceinvoice(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.performamtceinvoice();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblinvoiceno", true)[0] as Telerik.Reporting.TextBox;
        lblinvoiceno.Value = "Tax Invoice No: " + stPro.InvoiceNumber;
        try
        {
            Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
            lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table3.DataSource = dt;
        table8.DataSource = dt;

        Telerik.Reporting.TextBox lblInverterDetails = rpt.Items.Find("lblInverterDetails", true)[0] as Telerik.Reporting.TextBox;
        lblInverterDetails.Value = stPro.InverterDetails;



        //********************  amount ****************

        Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;





        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(newtotalcost));
            }
            catch { }
        }
        catch { }

        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
            }
            catch { }
            try
            {
                lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Value = "0";
            try
            {
                lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            }
            catch { }
        }
        try
        {
            lblCommissionGST.Value = "Note: The Net Cost above includes GST of: $" + SiteConfiguration.ChangeCurrencyVal(stPro.InvoiceGST);
        }
        catch { }

        ExportPdf(rpt);






    }

    public static void generate_welcomelettervic(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.welcomelettervic();

        Telerik.Reporting.TextBox lblContact = rpt.Items.Find("lblContact", true)[0] as Telerik.Reporting.TextBox;
        lblContact.Value = stCont.ContFirst + " " + stCont.ContLast;
        ExportPdf(rpt);
    }

    //public static void generate_performainvoice(string ProjectID)
    //{
    //    DataTable dt = new DataTable();
    //    DataTable dt1 = new DataTable();


    //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    dt = ClstblProjects.tblProjects_SelectByProjectID_picklist(ProjectID);

    //    Telerik.Reporting.Report rpt = new EurosolarReporting.perfome_invoice_new();

    //    Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblinvoiceno", true)[0] as Telerik.Reporting.TextBox;
    //    lblinvoiceno.Value = "Performa Invoice - " + stPro.InvoiceNumber;
    //    try
    //    {
    //        Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
    //        lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
    //    }
    //    catch { }

    //    Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;

    //    Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

    //    table3.DataSource = dt;
    //    table4.DataSource = dt;
    //    //table5.DataSource = dt;
    //    table8.DataSource = dt;

    //    Telerik.Reporting.TextBox textBox20 = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;

    //    ////==================================================
    //    if (stPro.InverterDetailsID != "")
    //    {

    //        textBox20.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

    //    }
    //    if (stPro.SecondInverterDetailsID != "0")
    //    {
    //        textBox20.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

    //    }
    //    if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
    //    {
    //        textBox20.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

    //    }


    //    ////********************  amount ****************
    //    Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;

    //    Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;

    //    //Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
    //    try
    //    {
    //        decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
    //        try
    //        {
    //            lblTotalQuotePrice.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(newtotalcost));
    //        }
    //        catch { }
    //    }
    //    catch { }

    //    try
    //    {
    //        lblNetCost.Value = "$" + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
    //    }
    //    catch { }
    //    DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
    //    decimal paiddate = 0;
    //    decimal balown = 0;
    //    if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
    //    {
    //        paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
    //        balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
    //        try
    //        {
    //            lblDepositPaid.Value = "$" + SiteConfiguration.ChangeCurrency_Val(paiddate.ToString());
    //        }
    //        catch { }
    //        try
    //        {
    //            lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
    //        }
    //        catch { }
    //    }
    //    else
    //    {
    //        lblDepositPaid.Value = "0";
    //        try
    //        {
    //            lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
    //        }
    //        catch { }
    //    }

    //    decimal invamnt = 0;

    //    try
    //    {

    //        invamnt = (Convert.ToDecimal(stPro.TotalQuotePrice) * Convert.ToDecimal(0.9091));

    //    }
    //    catch { }
    //    //if (stEmp.GSTPayment == "True")
    //    {
    //        decimal GST = 0;
    //        try
    //        {

    //            GST = ((invamnt) * 10) / 100;
    //        }
    //        catch { }


    //        lblCommissionGST.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(GST));
    //    }


    //    ExportPdf(rpt);
    //}





    //public static void generate_refundform(string ProjectID, string RefundID)
    //{
    //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    string inverterdetail = string.Empty;

    //    DataTable dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);
    //    // dt1 = Clscustomerlead.tblProjects_SelectByProjectID_demotelerik(ProjectID);
    //    DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist2(ProjectID, RefundID);
    //    SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByProjectID(ProjectID);
    //    SttblProjectRefund stRefund = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);

    //    Telerik.Reporting.Report rpt = new EurosolarReporting.RefundForm();
    //    Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();
    //    Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
    //    // Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
    //    // Telerik.Reporting.Table table5 = rpt.Items.Find("tabl5", true)[0] as Telerik.Reporting.Table;
    //    //Telerik.Reporting.Table tbl_sitedetail = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;

    //    //table1.DataSource = dt1;
    //   // table4.DataSource = dt;
    //    //table1.DataSource = dt1;
    //    //tbl_sitedetail.DataSource = dt1;

    //    table2.DataSource = dt1;
    //    table6.DataSource = dt1;
    //    Telerik.Reporting.TextBox txtSTATUS = rpt.Items.Find("txtSTATUS", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtNOOFPANELS = rpt.Items.Find("txtNOOFPANELS", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtAPPLICATION = rpt.Items.Find("txtAPPLICATION", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtDEPOSITDATE = rpt.Items.Find("txtDEPOSITDATE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtENTRYDATE = rpt.Items.Find("txtENTRYDATE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtPROMOTION = rpt.Items.Find("txtPROMOTION", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtBANKNAME = rpt.Items.Find("txtBANKNAME", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtBSBNO = rpt.Items.Find("txtBSBNO", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtACCNO = rpt.Items.Find("txtACCNO", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtAMOUNT = rpt.Items.Find("txtAMOUNT", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtACCOUNTNAME = rpt.Items.Find("txtACCOUNTNAME", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtREFUNDDATE = rpt.Items.Find("txtREFUNDDATE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtSIGNATURE = rpt.Items.Find("txtSIGNATURE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;



    //    Telerik.Reporting.TextBox txtCancleReason = rpt.Items.Find("txtCancleReason", true)[0] as Telerik.Reporting.TextBox;
    //    txtProjectNumber.Value = stPro.ProjectNumber; 
    //    txtBSBNO.Value = stRefund.BSBNo;
    //    txtACCNO.Value = stRefund.AccNo;
    //    txtAMOUNT.Value = stRefund.Amount;
    //    txtACCOUNTNAME.Value = stRefund.AccName;
    //    txtBANKNAME.Value = stRefund.BankName;

    //    if (stRefund.AccActionDate == "" || stRefund.AccActionDate == null)
    //    {
    //        txtREFUNDDATE.Value = "";
    //    }
    //    else
    //    {

    //        txtREFUNDDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.AccActionDate));
    //    }
    //    txtCancleReason.Value = stRefund.Notes;


    //    if (stRefund.Status == "1")
    //    {
    //        txtSTATUS.Value = "Successfull";
    //    }
    //    else
    //    {
    //        txtSTATUS.Value = "Failed";
    //    }
    //    txtNOOFPANELS.Value = stPro.NumberPanels;
    //    if (stRefund.CreateDate == "" || stRefund.CreateDate == null)
    //    {
    //        txtENTRYDATE.Value = "";
    //    }
    //    else
    //    {

    //        txtENTRYDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.CreateDate));
    //    }
    //    if (stPro.DepositReceived == "" || stPro.DepositReceived == null)
    //    {
    //        txtDEPOSITDATE.Value = "";
    //    }
    //    else
    //    {

    //        txtDEPOSITDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.DepositReceived));
    //    }


    //    ExportPdf(rpt);

    //}


    public static void ExportPdf(Telerik.Reporting.Report rpt)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = result.DocumentName + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);
    }

    public static void SavePDF(Telerik.Reporting.Report rpt, string QuoteID)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = QuoteID + result.DocumentName + "." + result.Extension;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\quotedoc\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();
        //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
        //SiteConfiguration.deleteimage(fileName, "quotedoc");

        //var convertApi = new ConvertApi("<YOUR SECRET HERE>");
        //convertApi.ConvertAsync("pdf", "compress",
        //    new ConvertApiFileParam(@"C:\path\to\my_file.pdf")
        //).Result.SaveFiles(@"C:\converted-files\");

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        //    if (page != null)
        //    {
        //        if (page.ImagesInfo != null)
        //        {
        //            foreach (PdfImageInfo info in page.ImagesInfo)
        //            {
        //                page.TryCompressImage(info.Index);
        //            }
        //        }
        //    }
        //}
        //doc.SaveToFile(fullPath);
        //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
        //SiteConfiguration.deleteimage(fileName, "quotedoc");

    }

    public static void SavePDFForSQ(Telerik.Reporting.Report rpt, string FileName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        //string fileName = QuoteID + result.DocumentName + "." + result.Extension;
        string fileName = FileName;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SQ\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();
        SiteConfiguration.UploadPDFFile("SQ", fileName);
        SiteConfiguration.deleteimage(fileName, "SQ");

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        //    if (page != null)
        //    {
        //        if (page.ImagesInfo != null)
        //        {
        //            foreach (PdfImageInfo info in page.ImagesInfo)
        //            {
        //                page.TryCompressImage(info.Index);
        //            }
        //        }
        //    }
        //}
        //doc.SaveToFile(fullPath);
        //SiteConfiguration.UploadPDFFile("SQ", fileName);
        //SiteConfiguration.deleteimage(fileName, "SQ");

    }

    public static void SavePDF2(Telerik.Reporting.Report rpt, string FileName, string FolderName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = FileName;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\" + FolderName + "\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        //    if (page != null)
        //    {
        //        if (page.ImagesInfo != null)
        //        {
        //            foreach (PdfImageInfo info in page.ImagesInfo)
        //            {
        //                page.TryCompressImage(info.Index);
        //            }
        //        }
        //    }
        //}
        //doc.SaveToFile(fullPath);
    }

    public static void Generate_NewPickList(string ProjectID)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        DataTable dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        DataTable dtitemlist = ClsQuickStock.tbl_PicklistItemDetail_LastPickListWise(ProjectID);
        DataTable dtlist = ClsQuickStock.tbl_Picklistlog_LastPickListWise(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.New_Picklist();

        Telerik.Reporting.TextBox textBox39 = rpt.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        textBox39.Value = string.Format("{0:dd/MM/yyyy}", System.DateTime.Now.AddHours(14));
        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox13 = rpt.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox5 = rpt.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox6 = rpt.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox20 = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox44 = rpt.Items.Find("textBox44", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox textBox2 = rpt.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox4 = rpt.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox8 = rpt.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox9 = rpt.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox10 = rpt.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox12 = rpt.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox18 = rpt.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox19 = rpt.Items.Find("textBox19", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        table1.DataSource = dtitemlist;

        if (dtlist.Rows.Count > 0)
        {
            textBox1.Value = dtlist.Rows[0]["InstallerName"].ToString();
            textBox5.Value = dt.Rows[0]["InstallAddress"].ToString();
            textBox6.Value = dt.Rows[0]["InstallCity"].ToString() + ", " + dt.Rows[0]["InstallState"].ToString() + " - " + dt.Rows[0]["InstallPostCode"].ToString();
            try
            {
                textBox13.Value = string.Format("{0:MMM dd, yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
                textBox21.Value = ": " + string.Format("{0:MMM dd, yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            textBox20.Value = ": " + dtlist.Rows[0]["InstallerName"].ToString();
            textBox44.Value = stPro.InstallerNotes;

            textBox2.Value = dt.Rows[0]["Customer"].ToString();
            textBox3.Value = dt.Rows[0]["ContMobile"].ToString();
            textBox4.Value = dt.Rows[0]["ContEmail"].ToString();

            textBox18.Value = ": " + stPro.ProjectNumber + "/" + dtlist.Rows[0]["ID"].ToString();
        }

        textBox7.Value = ": " + stPro.HouseType;
        textBox8.Value = ": " + stPro.RoofType;
        textBox9.Value = ": " + stPro.RoofAngle;
        textBox10.Value = ": " + stPro.ManualQuoteNumber;
        textBox11.Value = ": " + stPro.ProjectNumber;
        textBox12.Value = ": " + stPro.StoreName;

        

        SttblContacts sttblContacts = ClstblContacts.tblContacts_SelectByContactID(stPro.Installer);
        textBox19.Value = sttblContacts.ContMobile;

        ExportPdf(rpt);
    }

    //public static void generate_RetailerStatement(string ProjectID)
    //{
    //    DataTable dt = new DataTable();

    //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    //dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

    //    Telerik.Reporting.Report rpt = new EurosolarReporting.RetailerStatement();
    //    //table32.DataSource = dt1;

    //    Telerik.Reporting.TextBox txtRetailerABN = rpt.Items.Find("txtRetailerABN", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtSalesRepName = rpt.Items.Find("txtSalesRepName", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtSalesRepName2 = rpt.Items.Find("txtSalesRepName2", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtSalesRapSignDate = rpt.Items.Find("txtSalesRapSignDate", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtWitnessSignDate = rpt.Items.Find("txtWitnessSignDate", true)[0] as Telerik.Reporting.TextBox;

    //    txtRetailerABN.Value = stPro.OwnerABN;
    //    txtRetailerABN.Value = stPro.SalesRepName;
    //    txtSalesRepName2.Value = stPro.SalesRepName;
    //    txtSalesRapSignDate.Value = DateTime.Now.AddHours(14).ToString("dd MMM yyyy");
    //    txtWitnessSignDate.Value = DateTime.Now.AddHours(14).ToString("dd MMM yyyy");

       
    //    ExportPdf(rpt);      

    //}

    public static void generate_ExportControl(string DocId, string ProjectID)
    {
        DataTable dt = new DataTable();
        dt = ClstblProjects.tblProjectAcknowledgement_DocID(DocId);
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.ExportControl();
        //table32.DataSource = dt1;

        Telerik.Reporting.TextBox txtName = rpt.Items.Find("txtName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAddress = rpt.Items.Find("txtAddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMobile = rpt.Items.Find("txtMobile", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtEmail = rpt.Items.Find("txtEmail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDocNo = rpt.Items.Find("txtDocNo", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtSignDate = rpt.Items.Find("txtSignDate", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.CheckBox chkYes = rpt.Items.Find("chkYes", true)[0] as Telerik.Reporting.CheckBox;
        Telerik.Reporting.CheckBox chkNo = rpt.Items.Find("chkNo", true)[0] as Telerik.Reporting.CheckBox;
        Telerik.Reporting.PictureBox picSign = rpt.Items.Find("picSign", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.TextBox txtYesNotes = rpt.Items.Find("txtYesNotes", true)[0] as Telerik.Reporting.TextBox;

        if (dt.Rows.Count > 0)
        {
            txtName.Value = dt.Rows[0]["Name"].ToString();
            txtAddress.Value = dt.Rows[0]["FullAddress"].ToString();
            txtMobile.Value = dt.Rows[0]["Mobile"].ToString();
            txtEmail.Value = dt.Rows[0]["Email"].ToString();
            txtDocNo.Value = DocId;
            chkYes.Value = !string.IsNullOrEmpty(dt.Rows[0]["Yes"].ToString()) ? dt.Rows[0]["Yes"].ToString() : "False";
            chkNo.Value = !string.IsNullOrEmpty(dt.Rows[0]["No"].ToString()) ? dt.Rows[0]["No"].ToString() : "False";

            txtYesNotes.Value = "Yes, I accept the connection approval with imposed export control limit of " + stPro.KwExport + "kw. I understand that this may impact ROI/Expected Payback period.";

            int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogAcnoExistByProjectID_DocNo(ProjectID, DocId);
            if (existinSignLog == 1)
            {
                Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLogAcno_SelectByProjectID_DocNo(ProjectID, DocId);
                String ImageAdd = stsign.Image_Address;
                String Token = stsign.Token;
                
                Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenDataAcno_SelectByToken_ProjectID(Token, ProjectID);

                Page p = new Page();
                String Virtualpath = stsign.Image_Address;
                int index = Virtualpath.IndexOf("userfiles");
                Virtualpath = Virtualpath.Substring(index);
                string img1 = p.Server.MapPath("~/" + Virtualpath);
                picSign.Value = img1;

                txtSignDate.Value = !string.IsNullOrEmpty(stsign.Signed_Date) ? Convert.ToDateTime(stsign.Signed_Date).ToString("dd MMM yyyy @ hh:mm") : "";
            }
            else
            {
                picSign.Value = null;
            }

            //ExportPdf(rpt);
            ExportPdf2(rpt, DocId + "ExportControl");
        }
    }

    public static void generate_FeedInTariff(string DocId, string ProjectID)
    {
        DataTable dt = new DataTable();
        dt = ClstblProjects.tblProjectAcknowledgement_DocID(DocId);
        //SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.FeedInTariff();

        Telerik.Reporting.TextBox txtName = rpt.Items.Find("txtName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAddress = rpt.Items.Find("txtAddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMobile = rpt.Items.Find("txtMobile", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtEmail = rpt.Items.Find("txtEmail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDocNo = rpt.Items.Find("txtDocNo", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtSignDate = rpt.Items.Find("txtSignDate", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txtYesNotes = rpt.Items.Find("txtYesNotes", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.CheckBox chkYes = rpt.Items.Find("chkYes", true)[0] as Telerik.Reporting.CheckBox;
        Telerik.Reporting.CheckBox chkNo = rpt.Items.Find("chkNo", true)[0] as Telerik.Reporting.CheckBox;
        Telerik.Reporting.PictureBox picSign1 = rpt.Items.Find("picSign1", true)[0] as Telerik.Reporting.PictureBox;

        if (dt.Rows.Count > 0)
        {
            txtName.Value = dt.Rows[0]["Name"].ToString();
            txtAddress.Value = dt.Rows[0]["FullAddress"].ToString();
            txtMobile.Value = dt.Rows[0]["Mobile"].ToString();
            txtEmail.Value = dt.Rows[0]["Email"].ToString();
            txtDocNo.Value = DocId;
            //txtSignDate.Value = dt.Rows[0]["Name"].ToString();

            //chkYes.Value = dt.Rows[0]["Yes"].ToString();
            //chkNo.Value = dt.Rows[0]["No"].ToString();
            chkYes.Value = !string.IsNullOrEmpty(dt.Rows[0]["Yes"].ToString()) ? dt.Rows[0]["Yes"].ToString() : "False";
            chkNo.Value = !string.IsNullOrEmpty(dt.Rows[0]["No"].ToString()) ? dt.Rows[0]["No"].ToString() : "False";

            //txtYesNotes.Value = "Yes, I accept the connection approval with imposed export control limit of "+ stPro.KwExport +"kw. I understand that this may impact ROI/Expected Payback period.";

            int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogAcnoExistByProjectID_DocNo(ProjectID, DocId);
            if (existinSignLog == 1)
            {
                Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLogAcno_SelectByProjectID_DocNo(ProjectID, DocId);
                String ImageAdd = stsign.Image_Address;
                String Token = stsign.Token;

                Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenDataAcno_SelectByToken_ProjectID(Token, ProjectID);

                Page p = new Page();
                String Virtualpath = stsign.Image_Address;
                int index = Virtualpath.IndexOf("userfiles");
                Virtualpath = Virtualpath.Substring(index);
                string img1 = p.Server.MapPath("~/" + Virtualpath);
                picSign1.Value = img1;

                txtSignDate.Value = !string.IsNullOrEmpty(stsign.Signed_Date) ? Convert.ToDateTime(stsign.Signed_Date).ToString("dd MMM yyyy @ hh:mm") : "";
            }
            else
            {
                picSign1.Value = null;
            }

            ExportPdf2(rpt, DocId + "FeedInTariff");
        }
    }

    public static void ExportPdf2(Telerik.Reporting.Report rpt, string FileName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = FileName + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);
    }
}