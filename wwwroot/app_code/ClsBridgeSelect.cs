﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsBridgeSelect
/// </summary>
public class ClsBridgeSelect
{
    public string sz;
    public string crms;
    public string crmid;/////Y
    public string fn;   /////Y Cust First Name
    public string ln;   /////Y Cust Last Name
    public string ifn;  /////Y Cust installation fn
    public string iln;  /////Y Cust installation ln
    public string m;    /////Y cust mob
    public string im;   /////Y inst mob
    public string ipa;  /////Y (if inst add) cust StreetAdd
    public string istp; /////Y (if inst add) cust street type
    public string ot;   /////Y (if add)  (Individual by default)
    /////
    public string ac;   /////
    //public string acf;  /////
    public string anzss;///// (Yes by default)
    public string at;   ///// (Physical by default)
    public string bb;   ///// battery manufacturer
    public string bm;   ///// battery model
    //public string cbs;  /////
    public string cd;   ///// Completion Date
    public string cecss;///// (Yes by default)
    public string cgst; ///// (No by default)
    //public string co;   ///// 
    public string ctieg;///// (Connected to an electricity grid without battery storage/)
    //public string cxpv; /////
    public string e;    /////Y Cust Email
    //public string enff; /////
    public string esd;  ///// (Yes by default)
    public string fp;   ///// need to discuss with JP
    public string iia;  ///// (no by default)
    public string ibpoa;///// (no by default)
    public string icfc; ///// (no by default) 
    public string id;   ///// inst booking date
    public string ie;   ///// Cust email
    public string imto; ///// if existing sys then y else n
    //public string ilat; /////Y (if inst add) need to discuss
    public string ist;  /////Y (if inst add) Cust state
    //public string ilng; /////Y (if inst add)
    public string ipc;  /////Y (if inst add) Cust PCode
    public string iph;  ///// Cust Phone
    //public string ipnn; /////Y need to discuss
    public string ipra; /////Y (if inst add) Cust street
    public string isb;  /////Y (if inst add) Cust suburb
    public string istn; /////Y (if inst add) Cust streetnum
    public string lat;  /////Y (if inst add)
    public string lng;  /////Y (if inst add)
    //public string loos; /////
    public string mea;  /////Y (if add) manually added add (Yes)
    public string ngcs; ///// (no by default)
    public string nmi;  ///// nmi num
    public string noep; ///// existing num panels
    public string norp; ///// (no by default)
    //public string orn;  /////
    public string p;    ///// cust phone
    public string pa;   /////Y (if add) cust postal add
    //public string pabn; /////
    public string pc;   /////Y (if add) postal pcode
    public string pobxn;/////Y (if add) postal po box
    public string potyp;/////Y (if add) postal del type
    public string pra;  /////Y (if add) postal street
    public string pt;   ///// (Res by default)
    public string rn;   /////Y (Bell Solar Pty Ltd T/A Sunboost)
    public string sb;   /////Y (if add) cust suburb
    public string sia;  ///// (Yes by default)
    public string siad; /////Y (if add) Yes if customer address is same as installation address and No if the 2 addresses are different
    public string smt;  ///// (Bulding or Structure by default)
    public string srt;  ///// (single or multi (if double then multi))
    public string st;   /////Y (if add) cust state 
    public string stcdp;/////Y (if inst data) stc deeming period
    public string stn;  /////Y (if add) cust street num
    public string stp;  /////Y (if add) cust street type
    public string sw;   ///// need to discuss with BK
    public string tos;  ///// S.G.U. - Solar(Deemed) by default
    public string tp;   ///// num panels
    public string wd;   ///// (Send inv model num)
    public string mfs;
    public string inverters;
    public string installer;
    public Int32 sl;
    public string ampm;
    public string jt;
    public string mtrn;
    public string dstbn;
    public string ifs;
    public string ismd;
    public string trn;
    //public string stcdp;
    public string peivf;
    public string erpf;
    public string eicrf;
}