using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct SttblContacts
{
    public string CustomerID;
    public string EmployeeID;
    public string CustStatusID;
    public string ContLeadStatusID;
    public string ContLeadCancelReasonID;
    public string ContTag;
    public string ContTag1;
    public string ContTag2;
    public string ContTag3;
    public string PromoTag;
    public string LeadPromo;
    public string Newsletter;
    public string UploadTag;
    public string ActiveTag;
    public string ReferrerTag;
    public string ContactEntered;
    public string ContactEnteredBy;
    public string ContTitle;
    public string ContMr;
    public string ContFirst;
    public string ContLast;
    public string ContMobile;
    public string ContPhone;
    public string ContHomePhone;
    public string ContFax;
    public string ContEmail;
    public string ContEmailLink;
    public string SkypeID;
    public string ContGone;
    public string ContGoneDate;
    public string SendEmail;
    public string SendMail;
    public string SendSMS;
    public string ContNotes;
    public string LeadFollowUp;
    public string OldCustomerID;
    public string Accreditation;
    public string ElecLicence;
    public string ElecLicenceExpires;
    public string Installer;
    public string Designer;
    public string Electrician;
    public string MeterElectrician;
    public string LinkID;
    public string InstallerActive;
    public string InstallerAvailability;
    public string DocStore;
    public string DocStoreDone;
    public string WelcomeSMSDone;
    public string RefBSB;
    public string RefAccount;
    public string upsize_ts;
    public string userid;
    public string ContactID;
    public string AL;
    public string ContMr2;
    public string ContFirst2;
    public string ContLast2;

    public string referralProgram;

    public string UserName;
    public string RoleName;
    public string formbayid;
    public string groupid;
}
public struct SttblLeaddata
{
    public string CustomerID;
    public string ContactID;
    public string TransferEmpId;

}

public class ClstblContacts
{
    public static SttblLeaddata tbl_leadData_Track_GetdData(string CustomerId,string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_leadData_Track_GetdData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerId";
        if (CustomerId != string.Empty)
            param.Value = CustomerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblLeaddata details = new SttblLeaddata();
        if(table.Rows.Count > 0)
        {

        }
        return details;
    }

    public static DataTable tblContacts_UpdateEmployee_getsalearap(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateEmployee_getsalearap";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != String.Empty)
        {
            param.Value = CustomerID;
        }
        else
        {
            param.Value = DBNull.Value;
        }
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static SttblContacts tblContacts_SelectByContactID(String ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectByContactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != String.Empty)
        {
            param.Value = ContactID;
        }
        else
        {
            param.Value = DBNull.Value;
        }
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblContacts details = new SttblContacts();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.CustStatusID = dr["CustStatusID"].ToString();
            details.ContLeadStatusID = dr["ContLeadStatusID"].ToString();
            details.ContLeadCancelReasonID = dr["ContLeadCancelReasonID"].ToString();
            details.ContTag = dr["ContTag"].ToString();
            details.ContTag1 = dr["ContTag1"].ToString();
            details.ContTag2 = dr["ContTag2"].ToString();
            details.ContTag3 = dr["ContTag3"].ToString();
            details.PromoTag = dr["PromoTag"].ToString();
            details.LeadPromo = dr["LeadPromo"].ToString();
            details.Newsletter = dr["Newsletter"].ToString();
            details.UploadTag = dr["UploadTag"].ToString();
            details.ActiveTag = dr["ActiveTag"].ToString();
            details.ReferrerTag = dr["ReferrerTag"].ToString();
            details.ContactEntered = dr["ContactEntered"].ToString();
            details.ContactEnteredBy = dr["ContactEnteredBy"].ToString();
            details.ContTitle = dr["ContTitle"].ToString();
            details.ContMr = dr["ContMr"].ToString();
            details.ContFirst = dr["ContFirst"].ToString();
            details.ContLast = dr["ContLast"].ToString();
            details.ContMobile = dr["ContMobile"].ToString();
            details.ContPhone = dr["ContPhone"].ToString();
            details.ContHomePhone = dr["ContHomePhone"].ToString();
            details.ContFax = dr["ContFax"].ToString();
            details.ContEmail = dr["ContEmail"].ToString();
            details.ContEmailLink = dr["ContEmailLink"].ToString();
            details.SkypeID = dr["SkypeID"].ToString();
            details.ContGone = dr["ContGone"].ToString();
            details.ContGoneDate = dr["ContGoneDate"].ToString();
            details.SendEmail = dr["SendEmail"].ToString();
            details.SendMail = dr["SendMail"].ToString();
            details.SendSMS = dr["SendSMS"].ToString();
            details.ContNotes = dr["ContNotes"].ToString();
            details.LeadFollowUp = dr["LeadFollowUp"].ToString();
            details.OldCustomerID = dr["OldCustomerID"].ToString();
            details.Accreditation = dr["Accreditation"].ToString();
            details.ElecLicence = dr["ElecLicence"].ToString();
            details.ElecLicenceExpires = dr["ElecLicenceExpires"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.LinkID = dr["LinkID"].ToString();
            details.InstallerActive = dr["InstallerActive"].ToString();
            details.InstallerAvailability = dr["InstallerAvailability"].ToString();
            details.DocStore = dr["DocStore"].ToString();
            details.DocStoreDone = dr["DocStoreDone"].ToString();
            details.WelcomeSMSDone = dr["WelcomeSMSDone"].ToString();
            details.RefBSB = dr["RefBSB"].ToString();
            details.RefAccount = dr["RefAccount"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.userid = dr["userid"].ToString();
            details.AL = dr["AL"].ToString();
            details.ContMr2 = dr["ContMr2"].ToString();
            details.ContFirst2 = dr["ContFirst2"].ToString();
            details.ContLast2 = dr["ContLast2"].ToString();

            details.referralProgram = dr["referralProgram"].ToString();

            details.UserName = dr["UserName"].ToString();
            details.RoleName = dr["RoleName"].ToString();
            details.formbayid = dr["formbayid"].ToString();
            details.groupid = dr["groupid"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblContacts tblContacts_StructByUserId(String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_StructByUserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);

        SttblContacts details = new SttblContacts();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ContactID = dr["ContactID"].ToString();
            details.CustomerID = dr["CustomerID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.CustStatusID = dr["CustStatusID"].ToString();
            details.ContLeadStatusID = dr["ContLeadStatusID"].ToString();
            details.ContLeadCancelReasonID = dr["ContLeadCancelReasonID"].ToString();
            details.ContTag = dr["ContTag"].ToString();
            details.ContTag1 = dr["ContTag1"].ToString();
            details.ContTag2 = dr["ContTag2"].ToString();
            details.ContTag3 = dr["ContTag3"].ToString();
            details.PromoTag = dr["PromoTag"].ToString();
            details.LeadPromo = dr["LeadPromo"].ToString();
            details.Newsletter = dr["Newsletter"].ToString();
            details.UploadTag = dr["UploadTag"].ToString();
            details.ActiveTag = dr["ActiveTag"].ToString();
            details.ReferrerTag = dr["ReferrerTag"].ToString();
            details.ContactEntered = dr["ContactEntered"].ToString();
            details.ContactEnteredBy = dr["ContactEnteredBy"].ToString();
            details.ContTitle = dr["ContTitle"].ToString();
            details.ContMr = dr["ContMr"].ToString();
            details.ContFirst = dr["ContFirst"].ToString();
            details.ContLast = dr["ContLast"].ToString();
            details.ContMobile = dr["ContMobile"].ToString();
            details.ContPhone = dr["ContPhone"].ToString();
            details.ContHomePhone = dr["ContHomePhone"].ToString();
            details.ContFax = dr["ContFax"].ToString();
            details.ContEmail = dr["ContEmail"].ToString();
            details.ContEmailLink = dr["ContEmailLink"].ToString();
            details.SkypeID = dr["SkypeID"].ToString();
            details.ContGone = dr["ContGone"].ToString();
            details.ContGoneDate = dr["ContGoneDate"].ToString();
            details.SendEmail = dr["SendEmail"].ToString();
            details.SendMail = dr["SendMail"].ToString();
            details.SendSMS = dr["SendSMS"].ToString();
            details.ContNotes = dr["ContNotes"].ToString();
            details.LeadFollowUp = dr["LeadFollowUp"].ToString();
            details.OldCustomerID = dr["OldCustomerID"].ToString();
            details.Accreditation = dr["Accreditation"].ToString();
            details.ElecLicence = dr["ElecLicence"].ToString();
            details.ElecLicenceExpires = dr["ElecLicenceExpires"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.LinkID = dr["LinkID"].ToString();
            details.InstallerActive = dr["InstallerActive"].ToString();
            details.InstallerAvailability = dr["InstallerAvailability"].ToString();
            details.DocStore = dr["DocStore"].ToString();
            details.DocStoreDone = dr["DocStoreDone"].ToString();
            details.WelcomeSMSDone = dr["WelcomeSMSDone"].ToString();
            details.RefBSB = dr["RefBSB"].ToString();
            details.RefAccount = dr["RefAccount"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.userid = dr["userid"].ToString();
            details.AL = dr["AL"].ToString();

            details.UserName = dr["UserName"].ToString();
            details.RoleName = dr["RoleName"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblContacts tblContacts_SelectByCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblContacts details = new SttblContacts();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.CustStatusID = dr["CustStatusID"].ToString();
            details.ContLeadStatusID = dr["ContLeadStatusID"].ToString();
            details.ContLeadCancelReasonID = dr["ContLeadCancelReasonID"].ToString();
            details.ContTag = dr["ContTag"].ToString();
            details.ContTag1 = dr["ContTag1"].ToString();
            details.ContTag2 = dr["ContTag2"].ToString();
            details.ContTag3 = dr["ContTag3"].ToString();
            details.PromoTag = dr["PromoTag"].ToString();
            details.LeadPromo = dr["LeadPromo"].ToString();
            details.Newsletter = dr["Newsletter"].ToString();
            details.UploadTag = dr["UploadTag"].ToString();
            details.ActiveTag = dr["ActiveTag"].ToString();
            details.ReferrerTag = dr["ReferrerTag"].ToString();
            details.ContactEntered = dr["ContactEntered"].ToString();
            details.ContactEnteredBy = dr["ContactEnteredBy"].ToString();
            details.ContTitle = dr["ContTitle"].ToString();
            details.ContMr = dr["ContMr"].ToString();
            details.ContFirst = dr["ContFirst"].ToString();
            details.ContLast = dr["ContLast"].ToString();
            details.ContMobile = dr["ContMobile"].ToString();
            details.ContPhone = dr["ContPhone"].ToString();
            details.ContHomePhone = dr["ContHomePhone"].ToString();
            details.ContFax = dr["ContFax"].ToString();
            details.ContEmail = dr["ContEmail"].ToString();
            details.ContEmailLink = dr["ContEmailLink"].ToString();
            details.SkypeID = dr["SkypeID"].ToString();
            details.ContGone = dr["ContGone"].ToString();
            details.ContGoneDate = dr["ContGoneDate"].ToString();
            details.SendEmail = dr["SendEmail"].ToString();
            details.SendMail = dr["SendMail"].ToString();
            details.SendSMS = dr["SendSMS"].ToString();
            details.ContNotes = dr["ContNotes"].ToString();
            details.LeadFollowUp = dr["LeadFollowUp"].ToString();
            details.OldCustomerID = dr["OldCustomerID"].ToString();
            details.Accreditation = dr["Accreditation"].ToString();
            details.ElecLicence = dr["ElecLicence"].ToString();
            details.ElecLicenceExpires = dr["ElecLicenceExpires"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.LinkID = dr["LinkID"].ToString();
            details.InstallerActive = dr["InstallerActive"].ToString();
            details.InstallerAvailability = dr["InstallerAvailability"].ToString();
            details.DocStore = dr["DocStore"].ToString();
            details.DocStoreDone = dr["DocStoreDone"].ToString();
            details.WelcomeSMSDone = dr["WelcomeSMSDone"].ToString();
            details.RefBSB = dr["RefBSB"].ToString();
            details.RefAccount = dr["RefAccount"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tblContacts_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_Select_all()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Select_all";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContLeadStatus_SelectByAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContLeadStatus_SelectByAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_Select_ByUserID(string userid, string userid1)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Select_ByUserID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        param.Value = userid1;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblcontactsGetDataBySearch(string CustTypeID, string ContMobile, string ContPhone, string ResCom, string CustSourceID, string Client, string CompanyNumber, string ContFirst, string ContLast, string StreetPostCode, string StreetState, string StreetCity, String ContEmail, String EmployeeID, string SalesTeamID, string userid, string userid1, string startdate, string enddate, string ProjectStatusID, string StreetAddress, string CustSourceSubID, string projectnum)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblcontactsGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContPhone";
        if (ContPhone != string.Empty)
            param.Value = ContPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (projectnum != string.Empty)
            param.Value = projectnum;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
        //return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustType_SelectVender()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_SelectVender";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustType_SelectWholesaleVendor()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_SelectWholesaleVendor";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectTop(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectTop";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectLast(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectLast";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectByUserId(String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectByUserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_SelectByCustId(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectByCustId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectTop1ByCustId(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectTop1ByCustId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByContactID(String ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByContactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable AdmintblContactsGetCount(string userid, string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "AdmintblContactsGetCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_SelectByUserIdCust(String userid, String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectByUserIdCust";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContLeadStatus_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContLeadStatus_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContLeadStatus_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContLeadStatus_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblContacts_Insert(String CustomerID, String EmployeeID, String CustStatusID, String ContLeadCancelReasonID, String ContTag, String ContTag1, String ContTag2, String ContTag3, String PromoTag, String LeadPromo, String Newsletter, String UploadTag, String ReferrerTag, String ContactEntered, String ContactEnteredBy, String ContTitle, String ContMr, String ContFirst, String ContLast, String ContMobile, String ContPhone, String ContHomePhone, String ContFax, String ContEmail, String ContEmailLink, String SkypeID, String ContGone, String ContGoneDate, String SendEmail, String SendMail, String SendSMS, String ContNotes, String LeadFollowUp, String OldCustomerID, String Accreditation, String ElecLicence, String ElecLicenceExpires, String Installer, String Designer, String Electrician, String MeterElectrician, String LinkID, String InstallerActive, String InstallerAvailability, String DocStore, String DocStoreDone, String WelcomeSMSDone, String RefBSB, String RefAccount, String upsize_ts, String ContMr2, String ContFirst2, String ContLast2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustStatusID";
        if (CustStatusID != string.Empty)
            param.Value = CustStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadCancelReasonID";
        if (ContLeadCancelReasonID != string.Empty)
            param.Value = ContLeadCancelReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag";
        if (ContTag != string.Empty)
            param.Value = ContTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag1";
        if (ContTag1 != string.Empty)
            param.Value = ContTag1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag2";
        if (ContTag2 != string.Empty)
            param.Value = ContTag2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag3";
        if (ContTag3 != string.Empty)
            param.Value = ContTag3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoTag";
        if (PromoTag != string.Empty)
            param.Value = PromoTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadPromo";
        if (LeadPromo != string.Empty)
            param.Value = LeadPromo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Newsletter";
        if (Newsletter != string.Empty)
            param.Value = Newsletter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UploadTag";
        if (UploadTag != string.Empty)
            param.Value = UploadTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferrerTag";
        if (ReferrerTag != string.Empty)
            param.Value = ReferrerTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEntered";
        if (ContactEntered != string.Empty)
            param.Value = ContactEntered;
        else
            param.Value = DBNull.Value;
        param.Value = ContactEntered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEnteredBy";
        if (ContactEnteredBy != string.Empty)
            param.Value = ContactEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTitle";
        if (ContTitle != string.Empty)
            param.Value = ContTitle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr";
        if (ContMr != string.Empty)
            param.Value = ContMr;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContPhone";
        if (ContPhone != string.Empty)
            param.Value = ContPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContHomePhone";
        if (ContHomePhone != string.Empty)
            param.Value = ContHomePhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFax";
        if (ContFax != string.Empty)
            param.Value = ContFax;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmailLink";
        if (ContEmailLink != string.Empty)
            param.Value = ContEmailLink;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SkypeID";
        if (SkypeID != string.Empty)
            param.Value = SkypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContGone";
        if (ContGone != string.Empty)
            param.Value = ContGone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContGoneDate";
        if (ContGoneDate != string.Empty)
            param.Value = ContGoneDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendEmail";
        param.Value = SendEmail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendMail";
        param.Value = SendMail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendSMS";
        param.Value = SendSMS;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNotes";
        if (ContNotes != string.Empty)
            param.Value = ContNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadFollowUp";
        if (LeadFollowUp != string.Empty)
            param.Value = LeadFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldCustomerID";
        if (OldCustomerID != string.Empty)
            param.Value = OldCustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Accreditation";
        if (Accreditation != string.Empty)
            param.Value = Accreditation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicence";
        if (ElecLicence != string.Empty)
            param.Value = ElecLicence;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicenceExpires";
        if (ElecLicenceExpires != string.Empty)
            param.Value = ElecLicenceExpires;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElectrician";
        if (MeterElectrician != string.Empty)
            param.Value = MeterElectrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LinkID";
        if (LinkID != string.Empty)
            param.Value = LinkID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerActive";
        if (InstallerActive != string.Empty)
            param.Value = InstallerActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerAvailability";
        if (InstallerAvailability != string.Empty)
            param.Value = InstallerAvailability;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocStore";
        if (DocStore != string.Empty)
            param.Value = DocStore;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocStoreDone";
        if (DocStoreDone != string.Empty)
            param.Value = DocStoreDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WelcomeSMSDone";
        if (WelcomeSMSDone != string.Empty)
            param.Value = WelcomeSMSDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefBSB";
        if (RefBSB != string.Empty)
            param.Value = RefBSB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 12;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefAccount";
        if (RefAccount != string.Empty)
            param.Value = RefAccount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr2";
        if (ContMr2 != string.Empty)
            param.Value = ContMr2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst2";
        if (ContFirst2 != string.Empty)
            param.Value = ContFirst2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast2";
        if (ContLast2 != string.Empty)
            param.Value = ContLast2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblContacts_Update(string ContactID, String CustomerID, String EmployeeID, String CustStatusID, String ContLeadStatusID, String ContLeadCancelReasonID, String ContTag, String ContTag1, String ContTag2, String ContTag3, String PromoTag, String LeadPromo, String Newsletter, String UploadTag, String ActiveTag, String ReferrerTag, String ContactEntered, String ContactEnteredBy, String ContTitle, String ContMr, String ContFirst, String ContLast, String ContMobile, String ContPhone, String ContHomePhone, String ContFax, String ContEmail, String ContEmailLink, String SkypeID, String ContGone, String ContGoneDate, String SendEmail, String SendMail, String SendSMS, String ContNotes, String LeadFollowUp, String OldCustomerID, String Accreditation, String ElecLicence, String ElecLicenceExpires, String Installer, String Designer, String Electrician, String MeterElectrician, String LinkID, String InstallerActive, String InstallerAvailability, String DocStore, String DocStoreDone, String WelcomeSMSDone, String RefBSB, String RefAccount, String upsize_ts, String ContMr2, String ContFirst2, String ContLast2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustStatusID";
        if (CustStatusID != string.Empty)
            param.Value = CustStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        if (ContLeadStatusID != string.Empty)
            param.Value = ContLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadCancelReasonID";
        if (ContLeadCancelReasonID != string.Empty)
            param.Value = ContLeadCancelReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag";
        if (ContTag != string.Empty)
            param.Value = ContTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag1";
        if (ContTag1 != string.Empty)
            param.Value = ContTag1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag2";
        if (ContTag2 != string.Empty)
            param.Value = ContTag2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag3";
        if (ContTag3 != string.Empty)
            param.Value = ContTag3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoTag";
        if (PromoTag != string.Empty)
            param.Value = PromoTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadPromo";
        if (LeadPromo != string.Empty)
            param.Value = LeadPromo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Newsletter";
        if (Newsletter != string.Empty)
            param.Value = Newsletter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UploadTag";
        if (UploadTag != string.Empty)
            param.Value = UploadTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveTag";
        if (ActiveTag != string.Empty)
            param.Value = ActiveTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferrerTag";
        if (ReferrerTag != string.Empty)
            param.Value = ReferrerTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEntered";
        if (ContactEntered != string.Empty)
            param.Value = ContactEntered;
        else
            param.Value = DBNull.Value;
        param.Value = ContactEntered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEnteredBy";
        if (ContactEnteredBy != string.Empty)
            param.Value = ContactEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTitle";
        if (ContTitle != string.Empty)
            param.Value = ContTitle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr";
        if (ContMr != string.Empty)
            param.Value = ContMr;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContPhone";
        if (ContPhone != string.Empty)
            param.Value = ContPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContHomePhone";
        if (ContHomePhone != string.Empty)
            param.Value = ContHomePhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFax";
        if (ContFax != string.Empty)
            param.Value = ContFax;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmailLink";
        if (ContEmailLink != string.Empty)
            param.Value = ContEmailLink;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SkypeID";
        if (SkypeID != string.Empty)
            param.Value = SkypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContGone";
        if (ContGone != string.Empty)
            param.Value = ContGone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContGoneDate";
        if (ContGoneDate != string.Empty)
            param.Value = ContGoneDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendEmail";
        param.Value = SendEmail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendMail";
        param.Value = SendMail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendSMS";
        param.Value = SendSMS;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNotes";
        if (ContNotes != string.Empty)
            param.Value = ContNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadFollowUp";
        if (LeadFollowUp != string.Empty)
            param.Value = LeadFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldCustomerID";
        if (OldCustomerID != string.Empty)
            param.Value = OldCustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Accreditation";
        if (Accreditation != string.Empty)
            param.Value = Accreditation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicence";
        if (ElecLicence != string.Empty)
            param.Value = ElecLicence;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicenceExpires";
        if (ElecLicenceExpires != string.Empty)
            param.Value = ElecLicenceExpires;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElectrician";
        if (MeterElectrician != string.Empty)
            param.Value = MeterElectrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LinkID";
        if (LinkID != string.Empty)
            param.Value = LinkID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerActive";
        if (InstallerActive != string.Empty)
            param.Value = InstallerActive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerAvailability";
        if (InstallerAvailability != string.Empty)
            param.Value = InstallerAvailability;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocStore";
        if (DocStore != string.Empty)
            param.Value = DocStore;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocStoreDone";
        if (DocStoreDone != string.Empty)
            param.Value = DocStoreDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WelcomeSMSDone";
        if (WelcomeSMSDone != string.Empty)
            param.Value = WelcomeSMSDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefBSB";
        if (RefBSB != string.Empty)
            param.Value = RefBSB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 12;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefAccount";
        if (RefAccount != string.Empty)
            param.Value = RefAccount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr2";
        if (ContMr2 != string.Empty)
            param.Value = ContMr2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst2";
        if (ContFirst2 != string.Empty)
            param.Value = ContFirst2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast2";
        if (ContLast2 != string.Empty)
            param.Value = ContLast2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateDetail(string ContactID, String EmployeeID, String ContLeadStatusID, String ContLeadCancelReasonID, String ActiveTag, String ContTitle, String ContMr, String ContFirst, String ContLast, String ContMobile, String ContPhone, String ContHomePhone, String ContFax, String SendEmail, String SendMail, String SendSMS, string ContEmail, String ContMr2, String ContFirst2, String ContLast2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        if (ContLeadStatusID != string.Empty)
            param.Value = ContLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadCancelReasonID";
        if (ContLeadCancelReasonID != string.Empty)
            param.Value = ContLeadCancelReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveTag";
        if (ActiveTag != string.Empty)
            param.Value = ActiveTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTitle";
        if (ContTitle != string.Empty)
            param.Value = ContTitle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr";
        if (ContMr != string.Empty)
            param.Value = ContMr;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContPhone";
        if (ContPhone != string.Empty)
            param.Value = ContPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContHomePhone";
        if (ContHomePhone != string.Empty)
            param.Value = ContHomePhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFax";
        if (ContFax != string.Empty)
            param.Value = ContFax;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendEmail";
        param.Value = SendEmail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendMail";
        param.Value = SendMail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendSMS";
        param.Value = SendSMS;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr2";
        if (ContMr2 != string.Empty)
            param.Value = ContMr2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst2";
        if (ContFirst2 != string.Empty)
            param.Value = ContFirst2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast2";
        if (ContLast2 != string.Empty)
            param.Value = ContLast2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblContacts_UpdateInfo(string ContactID, String ContNotes, String ElecLicence, String Accreditation, String DocStoreDone, String ElecLicenceExpires, String RefBSB, String RefAccount, String Installer, String Designer, String Electrician, String MeterElectrician)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateInfo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNotes";
        if (ContNotes != string.Empty)
            param.Value = ContNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Accreditation";
        if (Accreditation != string.Empty)
            param.Value = Accreditation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicence";
        if (ElecLicence != string.Empty)
            param.Value = ElecLicence;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicenceExpires";
        if (ElecLicenceExpires != string.Empty)
            param.Value = ElecLicenceExpires;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElectrician";
        if (MeterElectrician != string.Empty)
            param.Value = MeterElectrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocStoreDone";
        if (DocStoreDone != string.Empty)
            param.Value = DocStoreDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefBSB";
        if (RefBSB != string.Empty)
            param.Value = RefBSB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 12;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefAccount";
        if (RefAccount != string.Empty)
            param.Value = RefAccount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblContacts_InsertUpdate(Int32 ContactID, String CustomerID, String EmployeeID, String CustStatusID, String ContLeadStatusID, String ContLeadCancelReasonID, String ContTag, String ContTag1, String ContTag2, String ContTag3, String PromoTag, String LeadPromo, String Newsletter, String UploadTag, String ActiveTag, String ReferrerTag, String ContactEntered, String ContactEnteredBy, String ContTitle, String ContMr, String ContFirst, String ContLast, String ContMobile, String ContPhone, String ContHomePhone, String ContFax, String ContEmail, String ContEmailLink, String SkypeID, String ContGone, String ContGoneDate, String SendEmail, String SendMail, String SendSMS, String ContNotes, String LeadFollowUp, String OldCustomerID, String Accreditation, String ElecLicence, String ElecLicenceExpires, String Installer, String Designer, String Electrician, String MeterElectrician, String LinkID, String InstallerActive, String InstallerAvailability, String DocStore, String DocStoreDone, String WelcomeSMSDone, String RefBSB, String RefAccount, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustStatusID";
        param.Value = CustStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        param.Value = ContLeadStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadCancelReasonID";
        param.Value = ContLeadCancelReasonID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag";
        param.Value = ContTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag1";
        param.Value = ContTag1;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag2";
        param.Value = ContTag2;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTag3";
        param.Value = ContTag3;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoTag";
        param.Value = PromoTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadPromo";
        param.Value = LeadPromo;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Newsletter";
        param.Value = Newsletter;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UploadTag";
        param.Value = UploadTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveTag";
        param.Value = ActiveTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferrerTag";
        param.Value = ReferrerTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEntered";
        param.Value = ContactEntered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEnteredBy";
        param.Value = ContactEnteredBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContTitle";
        param.Value = ContTitle;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr";
        param.Value = ContMr;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        param.Value = ContFirst;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        param.Value = ContLast;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContPhone";
        param.Value = ContPhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContHomePhone";
        param.Value = ContHomePhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFax";
        param.Value = ContFax;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmailLink";
        param.Value = ContEmailLink;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SkypeID";
        param.Value = SkypeID;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContGone";
        param.Value = ContGone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContGoneDate";
        param.Value = ContGoneDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendEmail";
        param.Value = SendEmail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendMail";
        param.Value = SendMail;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendSMS";
        param.Value = SendSMS;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNotes";
        param.Value = ContNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadFollowUp";
        param.Value = LeadFollowUp;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldCustomerID";
        param.Value = OldCustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Accreditation";
        param.Value = Accreditation;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicence";
        param.Value = ElecLicence;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecLicenceExpires";
        param.Value = ElecLicenceExpires;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        param.Value = Installer;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        param.Value = Designer;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        param.Value = Electrician;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElectrician";
        param.Value = MeterElectrician;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LinkID";
        param.Value = LinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerActive";
        param.Value = InstallerActive;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerAvailability";
        param.Value = InstallerAvailability;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocStore";
        param.Value = DocStore;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocStoreDone";
        param.Value = DocStoreDone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WelcomeSMSDone";
        param.Value = WelcomeSMSDone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefBSB";
        param.Value = RefBSB;
        param.DbType = DbType.String;
        param.Size = 12;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefAccount";
        param.Value = RefAccount;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        param.Value = upsize_ts;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblContacts_Delete(string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_Update_UserId(string ContactID, string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Update_UserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblContacts_SelectInverter()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectInverter";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblemployee_Mctefilter()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblemployee_Mctefilter";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_SelectAvailInstaller_new()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectAvailInstaller_new";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_SelectAvailInstaller(string date1, string date2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectAvailInstaller";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@date1";
        if (date1 != string.Empty)
            param.Value = date1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@date2";
        if (date2 != string.Empty)
            param.Value = date2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectElectrician()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectElectrician";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectElectricianByContactID(string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectElectricianByContactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectDesigner()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectDesigner";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectDesignerByContactID(string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectDesignerByContactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContacts_SelectMeterElec()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectMeterElec";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblContacts_UpdateAL(string ContactID, String AL)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateAL";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AL";
        if (AL != string.Empty)
            param.Value = AL;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateEmployee(string CustomerID, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateEmployee";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateContLeadStatus(string ContLeadStatusID, string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateContLeadStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        param.Value = ContLeadStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateContLeadStatusByCustID(string ContLeadStatusID, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateContLeadStatusByCustID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        param.Value = ContLeadStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateCustPhone(string CustPhone, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateCustPhone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        param.Value = CustPhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblContacts_ExistsByCustomerID(string CustomerID, string ContName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistsByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContName";
        param.Value = ContName;
        param.DbType = DbType.String;
        param.Size = 250;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblContacts_ExistsByContactID(string CustomerID, string ContName, string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistsByContactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContName";
        param.Value = ContName;
        param.DbType = DbType.String;
        param.Size = 250;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblContacts_SelectByLeadStatus(String CustomerID, string ContLeadStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectByLeadStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        param.Value = ContLeadStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblContacts_UpdateContLeadCancelReason(string ContLeadCancelReasonID, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateContLeadCancelReason";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContLeadCancelReasonID";
        param.Value = ContLeadCancelReasonID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateContLeadCancelReason_desc(string ContLeadCancelReasondesc, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateContLeadCancelReason_desc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContLeadCancelReasondesc";
        param.Value = ContLeadCancelReasondesc;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblcontacts_UpdatereasonByCustomerID(string reason, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblcontacts_UpdatereasonByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@reason";
        param.Value = reason;
        param.DbType = DbType.String;
        param.Size = 2000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateCustDetail(string ContactID, String ContMr, string ContFirst, string ContLast, string ContMobile, string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateCustDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr";
        if (ContMr != string.Empty)
            param.Value = ContMr;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblContacts_GetInstallers()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_GetInstallers";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblContactGetDataByInstaller()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContactGetDataByInstaller";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblContact_UpdateFormbayId(string ContactID, string formbayid, string groupid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContact_UpdateFormbayId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbayid";
        param.Value = formbayid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@groupid";
        param.Value = groupid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateSendSMS(string ContactID, String SendSMS)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateSendSMS";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SendSMS";
        param.Value = SendSMS;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblContacts_Update_ContPhone(string CustomerID, string ContPhone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Update_ContPhone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != String.Empty)
        {
            param.Value = CustomerID;
        }
        else
        {
            param.Value = DBNull.Value;
        }
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contphone";
        param.Value = ContPhone;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateContMobile(string CustomerID, string ContMobile1)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateContMobile";

        //HttpContext.Current.Response.Write(CustomerID + "ContMobile" + ContMobile);
        //HttpContext.Current.Response.End();

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile1;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdatereferralProgram(string referralProgram, string contactid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdatereferralProgram";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@referralProgram";
        param.Value = referralProgram;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = contactid;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblContacts_SelectbyInstaller(string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectbyInstaller";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_leadData_Track_Insert(string CustmerId,string ContactId,string LeadStatus,string Team,string CustUpdatedBy,string TransferEmpId,string CustEntered,string CustUpdate,string EmpID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_leadData_Track_Insert";

       DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerId";
        if (CustmerId != string.Empty)
            param.Value = CustmerId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactId != string.Empty)
            param.Value = ContactId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadStatus";
        if (LeadStatus != string.Empty)
            param.Value = LeadStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Team";
        if (Team != string.Empty)
            param.Value = Team;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustUpdatedBy";
        if (CustUpdatedBy != string.Empty)
            param.Value = CustUpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransferEmpId";
        if (TransferEmpId != null && TransferEmpId !="")
            param.Value = TransferEmpId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEntered";
        if (CustEntered != string.Empty)
            param.Value = CustEntered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustUpdate";
        if (CustUpdate != string.Empty)
            param.Value = CustUpdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Emp_ID";
        if (EmpID != null && EmpID != "")
            param.Value = EmpID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_leadData_Track_ExistsByContactID_CustomerID(string CustomerID, string ContactId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_leadData_Track_ExistsByContactID_CustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerId";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactId != string.Empty)
            param.Value = ContactId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tblCustomers_Update_CustEmailMobile(string CustomerID, string CustEmail, string CustMobile)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Update_CustEmailMobile";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEmail";
        if (CustEmail != string.Empty)
            param.Value = CustEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustMobile";
        if (CustMobile != string.Empty)
            param.Value = CustMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}
