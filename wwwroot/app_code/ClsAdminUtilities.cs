using System;
using System.Data;
using System.Configuration;
using System.Data.Common;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ClsAdminUtilities
/// </summary>
/// 

public struct StUtilities
{
    public int id;
    public string sitename;
    public string sitelogo;
    public string siteurl;
    public string MailServer;
    public string from;
    public string cc;
    public string username;
    public string Password;
    public string OrderThanksMessage;
    public string ContactUsThanksmessage;
    public string OrderSubject;
    public string Contactussubject;
    public string Regards_Name;
    public string emailbodybordercolor;
    public string adminfooter;
    public string PaypalAccountName;
    public string PaypalMode;
    public string PaypalCurrencySymbol;
    public string emailtop;
    public string emailbottom;
    public string sitelogoupload;
    public string Authenticate;
    public string SSLPortno;
    public string SSLValue;
    public string pagination;
    public string port;
    public string Hours;
    public string siteByUrl;
    public string sitebyname;
}


public class ClsAdminUtilities
{
    public ClsAdminUtilities()
    {

    }
    public static StUtilities StUtilitiesGetDataStructById(string id)
    {

        DbCommand comm = DataAccess.CreateCommand();

        comm.CommandText = "SpUtilitiesGetDataStructById";

        DbParameter param = comm.CreateParameter();

        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        StUtilities details = new StUtilities();

        if (table.Rows.Count > 0)
        {

            DataRow dr = table.Rows[0];

            details.id = Convert.ToInt32(dr["id"]);
            details.sitename = dr["sitename"].ToString();
            details.sitelogo = dr["sitelogo"].ToString();
            //details.siteurl =  dr["siteurl"].ToString();
            details.siteurl = "http://localhost:57341/";
            details.MailServer = dr["MailServer"].ToString();
            details.from = dr["from"].ToString();
            details.cc = dr["cc"].ToString();
            details.username = dr["username"].ToString();
            details.Password = dr["Password"].ToString();
            details.OrderThanksMessage = dr["OrderThanksMessage"].ToString();
            details.ContactUsThanksmessage = dr["ContactUsThanksmessage"].ToString();
            details.OrderSubject = dr["OrderSubject"].ToString();
            details.Contactussubject = dr["Contactussubject"].ToString();
            details.Regards_Name = dr["Regards_Name"].ToString();
            details.emailbodybordercolor = dr["emailbodybordercolor"].ToString();
            details.adminfooter = dr["adminfooter"].ToString();
            details.PaypalAccountName = dr["PaypalAccountName"].ToString();
            details.PaypalMode = dr["PaypalMode"].ToString();
            details.PaypalCurrencySymbol = dr["PaypalCurrencySymbol"].ToString();
            details.emailtop = dr["emailtop"].ToString();
            details.emailbottom = dr["emailbottom"].ToString();
            details.sitelogoupload = dr["sitelogoupload"].ToString();
            details.Authenticate = dr["Authenticate"].ToString();
            details.SSLPortno = dr["SSLPortno"].ToString();
            details.SSLValue = dr["SSLValue"].ToString();
            //details.pagination = dr["pagination"].ToString();
            details.Hours = dr["Hours"].ToString();
            //details.port = dr["port"].ToString();


            //details.sitebyname = dr["sitebyname"].ToString();
            //details.siteByUrl = dr["sitebyurl"].ToString();
        }
        // return product details
        return details;
    }



    public static bool SpUtilitiesUpdateData(string id, string sitename, string sitelogo, string siteurl, string MailServer, string from, string cc, string username, string Password, string OrderThanksMessage, string ContactUsThanksmessage, string OrderSubject, string Contactussubject, string Regards_Name, string emailbodybordercolor, string adminfooter, string PaypalAccountName, string PaypalMode, string PaypalCurrencySymbol, string emailtop, string emailbottom, string sitelogoupload, string Authenticate, String SSLValue, String SSLPortno)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "SpUtilitiesUpdateData";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // create a new parameter
        param = comm.CreateParameter();
        param.ParameterName = "@sitename";
        param.Value = sitename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@sitelogo";
        param.Value = sitelogo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@siteurl";
        param.Value = siteurl;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MailServer";
        param.Value = MailServer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@from";
        param.Value = from;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cc";
        param.Value = cc;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@username";
        param.Value = username;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Password";
        param.Value = Password;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@OrderThanksMessage";
        param.Value = OrderThanksMessage;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactUsThanksmessage";
        param.Value = ContactUsThanksmessage;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderSubject";
        param.Value = OrderSubject;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Contactussubject";
        param.Value = Contactussubject;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Regards_Name";
        param.Value = Regards_Name;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@emailbodybordercolor";
        param.Value = emailbodybordercolor;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@adminfooter";
        param.Value = adminfooter;
        param.DbType = DbType.String;
        param.Size = 80000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaypalAccountName";
        param.Value = PaypalAccountName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaypalMode";
        param.Value = PaypalMode;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaypalCurrencySymbol";
        param.Value = PaypalCurrencySymbol;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@emailtop";
        param.Value = emailtop;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@emailbottom";
        param.Value = emailbottom;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@sitelogoupload";
        param.Value = sitelogoupload;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Authenticate";
        if (Authenticate != string.Empty)
            param.Value = Authenticate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SSLPortno";
        if (SSLPortno != string.Empty)
            param.Value = SSLPortno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SSLValue";
        param.Value = SSLValue;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
        // result will represent the number of changed rows
        int result = -1;

       
        try
        {
            // execute the stored procedure
            result = DataAccess.ExecuteNonQuery(comm);
           // HttpContext.Current.Response.Write(result);
        }
        catch (Exception ex)
        {
        //    HttpContext.Current.Response.Write(ex);
            // log errors if any
        }
        // result will be 1 in case of success 
        return (result != -1);
    }
    public static bool SpUtilitiesUpdateData_port_pagination(string id, string port, String pagination)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "SpUtilitiesUpdateData_port_pagination";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@port";
        if (port != string.Empty)
            param.Value = port;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pagination";
        if (pagination != string.Empty)
            param.Value = pagination;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        // result will represent the number of changed rows
        int result = -1;


        try
        {
            // execute the stored procedure
            result = DataAccess.ExecuteNonQuery(comm);
            // HttpContext.Current.Response.Write(result);
        }
        catch (Exception ex)
        {
            //    HttpContext.Current.Response.Write(ex);
            // log errors if any
        }
        // result will be 1 in case of success 
        return (result != -1);
    }
    public static DataTable Aspnet_Users_GetRole(string UserId)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "Aspnet_Users_GetRole";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static bool SpUtilitiesUpdateData_SiteByNameUrl(string id, string sitebyname, String sitebyurl)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "SpUtilitiesUpdateData_SiteByNameUrl";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@sitebyname";
        param.Value = sitebyname;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@sitebyurl";
        param.Value = sitebyurl;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
           
            result = DataAccess.ExecuteNonQuery(comm);
           
        }
        catch (Exception ex)
        {
           
        }
        // result will be 1 in case of success 
        return (result != -1);
    }

}
