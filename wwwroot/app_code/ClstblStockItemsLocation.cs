using System;
using System.Data;
using System.Data.Common;


public struct SttblStockItemsLocation
{
    public string id;
    public string StockItemID;
    public string CompanyLocationID;
    public string StockQuantity;

    public string CompanyLocation;
    public string StockItem;
    public string StockManufacturer;
    public string StockModel;
    public string StockSize;
    public string StockSeries;
    public string Active;
    public string SalesTag;
    public string StockDescription;
    public string MinLevel;
    public string StockCategoryID;
    public string IsDashboard;
}


public class ClstblStockItemsLocation
{
    public static SttblStockItemsLocation tblStockItemsLocation_SelectByid(String id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_SelectByid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockItemsLocation details = new SttblStockItemsLocation();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockItemID = dr["StockItemID"].ToString();
            details.CompanyLocationID = dr["CompanyLocationID"].ToString();
            details.StockQuantity = dr["StockQuantity"].ToString();
            details.CompanyLocation = dr["CompanyLocation"].ToString();
            details.StockItem = dr["StockItem"].ToString();
            details.StockManufacturer = dr["StockManufacturer"].ToString();
            details.StockModel = dr["StockModel"].ToString();
            details.StockSize = dr["StockSize"].ToString();
            details.Active = dr["Active"].ToString();
            details.SalesTag = dr["SalesTag"].ToString();
            details.StockDescription = dr["StockDescription"].ToString();
            details.StockSeries = dr["StockSeries"].ToString();
            details.MinLevel = dr["MinLevel"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.IsDashboard = dr["IsDashboard"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblStockItemsLocation tblStockItemsLocation_SelectByStockItemID(string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_SelectByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockItemsLocation details = new SttblStockItemsLocation();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details

            details.id = dr["id"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.CompanyLocationID = dr["CompanyLocationID"].ToString();
            details.StockQuantity = dr["StockQuantity"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tblStockItemsLocation_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItemsLocation_ByStockItemID(String StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_ByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockItemsLocation_Insert(string StockItemID, string CompanyLocationID, string StockQuantity)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        if (StockQuantity != string.Empty)
            param.Value = StockQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int Insert_tblStockItemsLocationby_LocationId(string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Insert_tblStockItemsLocationby_LocationId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@CompanyLocationID";
        //if (CompanyLocationID != string.Empty)
        //    param.Value = CompanyLocationID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StockQuantity";
        //if (StockQuantity != string.Empty)
        //    param.Value = StockQuantity;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockItemsLocation_Update(string id, String StockQuantity)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        param.Value = StockQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItemsLocation_UpdateByStockItemID(string StockItemID, String StockQuantity)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_UpdateByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        param.Value = StockQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(string StockItemID, string CompanyLocationID, string StockQuantity)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        param.Value = StockQuantity;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItemsLocation_Update_StockItemID_CompanyLocationID(string StockItemID, string CompanyLocationID, String StockQuantity)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Update_StockItemID_CompanyLocationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        param.Value = StockQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItemsLocation_Update_Revert(string StockItemID, string CompanyLocationID, String StockQuantity)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Update_Revert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        param.Value = StockQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItemsLocation_Delete(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockItemsLocation_Delete_StockItemID(string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_Delete_StockItemID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int Insert_tblStockItemsLocation_Tabel(string StockItemID, string StockQuantity,string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Insert_tblStockItemsLocation_Tabel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        if (StockQuantity != string.Empty)
            param.Value = StockQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockItemsLocation_UpdateSalesTag(string id, String SalesTag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_UpdateSalesTag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        if (SalesTag != string.Empty)
            param.Value = SalesTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblStockItemsLocation_StockItemID(String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_StockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblStockItemsLocation_UpdateminQty(string id, string MinQty)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_UpdateminQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@MinQty";
        if (MinQty != string.Empty)
            param.Value = MinQty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
}