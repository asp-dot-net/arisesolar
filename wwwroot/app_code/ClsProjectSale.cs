﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Common;

public struct SttblInstallerExtraDocs
{
    public string ID;
    public string ProjectID;
    public string InstallerID;
    public string DocName;
    public string CreateDate;
}

public class ClsProjectSale
{
    public ClsProjectSale()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool tblProjects_Updatecollectioncharge(string ProjectID, String collectioncharge)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatecollectioncharge";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@collectioncharge";
        if (collectioncharge != string.Empty)
            param.Value = collectioncharge;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);



        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatePriceNew(string ProjectID, String collectioncharge,String Tiltkitbrackets)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePriceNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@oldsystemremovalcost";
        if (collectioncharge != string.Empty)
            param.Value = collectioncharge;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        //param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@tilikitbrackets";
        if (Tiltkitbrackets != string.Empty)
            param.Value = Tiltkitbrackets;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        //param.Size = 255;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblProjects_UpdateHotSmartMeter(string ProjectID, string HotWaterMeter, string SmartMeter)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateHotSmartMeter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@HotWaterMeter";
        if (HotWaterMeter != string.Empty)
            param.Value = HotWaterMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SmartMeter";
        if (SmartMeter != string.Empty)
            param.Value = SmartMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);

        return (result != -1);
    }

    public static bool tblProjects_UpdateQuote_SSActiveDate(string ProjectID, String SSActiveDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateQuote_SSActiveDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SSActiveDate";
        if (SSActiveDate != string.Empty)
            param.Value = SSActiveDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateQuotenearmapcheck(string ProjectID, string nearmapcheck)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateQuotenearmapcheck";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@nearmapcheck";
        if (nearmapcheck != string.Empty)
            param.Value = nearmapcheck;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);



        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateQuote_SSCompleteDate(string ProjectID, String SSCompleteDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateQuote_SSCompleteDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SSCompleteDate";
        if (SSCompleteDate != string.Empty)
            param.Value = SSCompleteDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockItems_SelectInverter_SalesRep(String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectInverter_SalesRep";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Location";
        param.Value = Location;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItems_SelectPanel(String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectPanel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Location";
        param.Value = Location;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectInverter(String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectInverter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Location";
        param.Value = Location;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItems_SelectPanel_SalesRep(String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectPanel_SalesRep";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Location";
        param.Value = Location;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_CheckProjectActiveBefore(String ProjectId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_CheckProjectActiveBefore";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateDetails(string ProjectID, String ContactID, String EmployeeID, String SalesRep, String ProjectTypeID, String ProjectOpened, String OldProjectNumber, String ManualQuoteNumber, String ProjectNotes, String FollowUp, String FollowUpNote, String InstallerNotes, String MeterInstallerNotes, String InstallAddress, String InstallCity, String InstallState, String InstallPostCode, String UpdatedBy, string Project)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectOpened";
        if (ProjectOpened != string.Empty)
            param.Value = ProjectOpened;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldProjectNumber";
        if (OldProjectNumber != string.Empty)
            param.Value = OldProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@Project";
        //if (Project != string.Empty)
        //    param.Value = Project;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 100;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNotes";
        if (ProjectNotes != string.Empty)
            param.Value = ProjectNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotes";
        if (InstallerNotes != string.Empty)
            param.Value = InstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstallerNotes";
        if (MeterInstallerNotes != string.Empty)
            param.Value = MeterInstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Project != string.Empty)
            param.Value = Project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUp";
        if (FollowUp != string.Empty)
            param.Value = FollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUpNote";
        if (FollowUpNote != string.Empty)
            param.Value = FollowUpNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool OldSystemDetails(string ProjectID, String OldSystemDetails)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateOldSystemDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldSystemDetails";
        if (OldSystemDetails != string.Empty)
            param.Value = OldSystemDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSale(string ProjectID, String PanelBrandID, String PanelBrand, String PanelOutput, String RECRebate, String STCMultiplier, String STCZoneRating, String PanelModel, String NumberPanels, String SystemCapKW, String PanelDetails, String InverterDetailsID, String InverterBrand, String InverterSeries, String SecondInverterDetailsID, String STCNumber, String STCValue, String InverterModel, String InverterOutput, String InverterDetails, String SystemDetails, String ElecDistributorID, String ElecDistApprovelRef, String ElecRetailerID, String RegPlanNo, String HouseTypeID, String RoofTypeID, String RoofAngleID, String LotNumber, String NMINumber, String MeterNumber1, String MeterNumber2, string MeterPhase, String MeterEnoughSpace, string OffPeak, string UpdatedBy, String ElecDistApplied, String ElecDistApplyMethod, String ElecDistApplySentFrom, string FlatPanels, string PitchedPanels, string SurveyCerti, string ElecDistAppDate, string ElecDistAppBy, string CertiApprove, string ThirdInverterDetailsID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSale";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        if (HouseTypeID != string.Empty)
            param.Value = HouseTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        if (RoofTypeID != string.Empty)
            param.Value = RoofTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofAngleID";
        if (RoofAngleID != string.Empty)
            param.Value = RoofAngleID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrandID";
        if (PanelBrandID != string.Empty)
            param.Value = PanelBrandID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrand";
        if (PanelBrand != string.Empty)
            param.Value = PanelBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelOutput";
        if (PanelOutput != string.Empty)
            param.Value = PanelOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelDetails";
        if (PanelDetails != string.Empty)
            param.Value = PanelDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetailsID";
        if (InverterDetailsID != string.Empty)
            param.Value = InverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterDetailsID";
        if (SecondInverterDetailsID != string.Empty)
            param.Value = SecondInverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ThirdInverterDetailsID";
        if (ThirdInverterDetailsID != string.Empty)
            param.Value =ThirdInverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterBrand";
        if (InverterBrand != string.Empty)
            param.Value = InverterBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSeries";
        if (InverterSeries != string.Empty)
            param.Value = InverterSeries;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterOutput";
        if (InverterOutput != string.Empty)
            param.Value = InverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetails";
        if (InverterDetails != string.Empty)
            param.Value = InverterDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemDetails";
        if (SystemDetails != string.Empty)
            param.Value = SystemDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NumberPanels";
        if (NumberPanels != string.Empty)
            param.Value = NumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKW";
        if (SystemCapKW != string.Empty)
            param.Value = SystemCapKW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCMultiplier";
        if (STCMultiplier != string.Empty)
            param.Value = STCMultiplier;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCZoneRating";
        if (STCZoneRating != string.Empty)
            param.Value = STCZoneRating;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumber";
        if (STCNumber != string.Empty)
            param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCValue";
        if (STCValue != string.Empty)
            param.Value = STCValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        if (ElecRetailerID != string.Empty)
            param.Value = ElecRetailerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributorID";
        if (ElecDistributorID != string.Empty)
            param.Value = ElecDistributorID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RegPlanNo";
        if (RegPlanNo != string.Empty)
            param.Value = RegPlanNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LotNumber";
        if (LotNumber != string.Empty)
            param.Value = LotNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NMINumber";
        if (NMINumber != string.Empty)
            param.Value = NMINumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber1";
        if (MeterNumber1 != string.Empty)
            param.Value = MeterNumber1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber2";
        if (MeterNumber2 != string.Empty)
            param.Value = MeterNumber2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RECRebate";
        if (RECRebate != string.Empty)
            param.Value = RECRebate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterPhase";
        if (MeterPhase != string.Empty)
            param.Value = MeterPhase;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterEnoughSpace";
        if (MeterEnoughSpace != string.Empty)
            param.Value = MeterEnoughSpace;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OffPeak";
        if (OffPeak != string.Empty)
            param.Value = OffPeak;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplied";
        if (ElecDistApplied != string.Empty)
            param.Value = ElecDistApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplyMethod";
        if (ElecDistApplyMethod != string.Empty)
            param.Value = ElecDistApplyMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplySentFrom";
        if (ElecDistApplySentFrom != string.Empty)
            param.Value = ElecDistApplySentFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FlatPanels";
        if (FlatPanels != string.Empty)
            param.Value = FlatPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PitchedPanels";
        if (PitchedPanels != string.Empty)
            param.Value = PitchedPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SurveyCerti";
        if (SurveyCerti != string.Empty)
            param.Value = SurveyCerti;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistAppDate";
        if (ElecDistAppDate != string.Empty)
            param.Value = ElecDistAppDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistAppBy";
        if (ElecDistAppBy != string.Empty)
            param.Value = ElecDistAppBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CertiApprove";
        if (CertiApprove != string.Empty)
            param.Value = CertiApprove;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //HttpContext.Current.Response.Write(SecondInverterDetailsID);
        //HttpContext.Current.Response.End();

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
           
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSaleForAppDetail(string ProjectID, String ElecDistributorID, String ElecDistApprovelRef, String ElecRetailerID, String RegPlanNo, String HouseTypeID, String RoofTypeID, String RoofAngleID, String LotNumber, String NMINumber, String ElecDistApplied, String ElecDistApplyMethod, string ElecDistAppDate, string ElecDistAppBy,string meterupgrade,string employee, string UpdatedBy )
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSaleForAppDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributorID";
        if (ElecDistributorID != string.Empty)
            param.Value = ElecDistributorID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        if (ElecRetailerID != string.Empty)
            param.Value = ElecRetailerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RegPlanNo";
        if (RegPlanNo != string.Empty)
            param.Value = RegPlanNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        if (HouseTypeID != string.Empty)
            param.Value = HouseTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        if (RoofTypeID != string.Empty)
            param.Value = RoofTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofAngleID";
        if (RoofAngleID != string.Empty)
            param.Value = RoofAngleID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LotNumber";
        if (LotNumber != string.Empty)
            param.Value = LotNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NMINumber";
        if (NMINumber != string.Empty)
            param.Value = NMINumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplied";
        if (ElecDistApplied != string.Empty)
            param.Value = ElecDistApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplyMethod";
        if (ElecDistApplyMethod != string.Empty)
            param.Value = ElecDistApplyMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistAppBy";
        if (ElecDistAppBy != string.Empty)
            param.Value = ElecDistAppBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistAppDate";
        if (ElecDistAppDate != string.Empty)
            param.Value = ElecDistAppDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@meterupgrade";
        if (meterupgrade != string.Empty)
            param.Value = meterupgrade;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@employee";
        if (employee != string.Empty)
            param.Value = employee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSale_inverterqty(string ProjectID, String inverterqty, String inverterqty2, string inverterqty3)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSale_inverterqty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@inverterqty";
        if (inverterqty != string.Empty)
            param.Value = inverterqty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@inverterqty2";
        if (inverterqty2 != string.Empty)
            param.Value = inverterqty2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@inverterqty3";
        if (inverterqty3 != string.Empty)
            param.Value = inverterqty3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);
        //HttpContext.Current.Response.Write(inverterqty2);
        //HttpContext.Current.Response.End();
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
           
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSale_VicRebate(string ProjectID, String VicAppReference, String VicDate, string VicRebateNote,string VicRebate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSale_VicRebate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VicAppReference";
        if (VicAppReference != string.Empty)
            param.Value = VicAppReference;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VicDate";
        if (VicDate != string.Empty)
            param.Value = VicDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VicRebateNote";
        if (VicRebateNote != string.Empty)
            param.Value = VicRebateNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VicRebate";
        if (VicRebate != string.Empty)
            param.Value = VicRebate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatePrice(string ProjectID, String ServiceValue, String VarHouseType, String VarRoofType, String VarRoofAngle, String VarMeterInstallation, String VarMeterUG, String VarAsbestos, String VarSplitSystem, String VarCherryPicker, String VarTravelTime, String VarOther, String SpecialDiscount, String TotalQuotePrice, String DepositRequired, String FinanceWithID, String MeterUG, String Asbestoss, String SplitSystem, String CherryPicker, String PanelConfigNW, String PanelConfigOTH, String TravelTime, String VariationOther, string UpdatedBy, string DepositAmount, string PaymentTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelConfigNW";
        if (PanelConfigNW != string.Empty)
            param.Value = PanelConfigNW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelConfigOTH";
        if (PanelConfigOTH != string.Empty)
            param.Value = PanelConfigOTH;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Asbestoss";
        if (Asbestoss != string.Empty)
            param.Value = Asbestoss;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterUG";
        if (MeterUG != string.Empty)
            param.Value = MeterUG;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SplitSystem";
        if (SplitSystem != string.Empty)
            param.Value = SplitSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CherryPicker";
        if (CherryPicker != string.Empty)
            param.Value = CherryPicker;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TravelTime";
        if (TravelTime != string.Empty)
            param.Value = TravelTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VariationOther";
        if (VariationOther != string.Empty)
            param.Value = VariationOther;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofType";
        if (VarRoofType != string.Empty)
            param.Value = VarRoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofAngle";
        if (VarRoofAngle != string.Empty)
            param.Value = VarRoofAngle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarHouseType";
        if (VarHouseType != string.Empty)
            param.Value = VarHouseType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarAsbestos";
        if (VarAsbestos != string.Empty)
            param.Value = VarAsbestos;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterInstallation";
        if (VarMeterInstallation != string.Empty)
            param.Value = VarMeterInstallation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterUG";
        if (VarMeterUG != string.Empty)
            param.Value = VarMeterUG;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarTravelTime";
        if (VarTravelTime != string.Empty)
            param.Value = VarTravelTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarSplitSystem";
        if (VarSplitSystem != string.Empty)
            param.Value = VarSplitSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarCherryPicker";
        if (VarCherryPicker != string.Empty)
            param.Value = VarCherryPicker;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarOther";
        if (VarOther != string.Empty)
            param.Value = VarOther;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SpecialDiscount";
        if (SpecialDiscount != string.Empty)
            param.Value = SpecialDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositRequired";
        if (DepositRequired != string.Empty)
            param.Value = DepositRequired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TotalQuotePrice";
        if (TotalQuotePrice != string.Empty)
            param.Value = TotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceValue";
        if (ServiceValue != string.Empty)
            param.Value = ServiceValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositAmount";
        if (DepositAmount != string.Empty)
            param.Value = DepositAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeID";
        if (PaymentTypeID != string.Empty)
            param.Value = PaymentTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateEx(string ProjectID, String InvoiceGST, String InvoiceExGST)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateEx";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceGST";
        if (InvoiceGST != string.Empty)
            param.Value = InvoiceGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceExGST";
        if (InvoiceExGST != string.Empty)
            param.Value = InvoiceExGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatePreviousTotalQuotePrice(string ProjectID, String PreviousTotalQuotePrice)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePreviousTotalQuotePrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreviousTotalQuotePrice";
        if (PreviousTotalQuotePrice != string.Empty)
            param.Value = PreviousTotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateTotalQuotePrice(string ProjectID, String TotalQuotePrice)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateTotalQuotePrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TotalQuotePrice";
        if (TotalQuotePrice != string.Empty)
            param.Value = TotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblPanelsUser_log_insert(String ProjectID, String No_Of_Panels, String Userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPanelsUser_log_insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@No_Of_Panels";
        param.Value = No_Of_Panels;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Userid";
        param.Value = Userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    
    public static bool tblProjects_UpdateSalePrice(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSalePrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateQuote(string ProjectID, String QuoteAccepted, String SignedQuote, String InvoiceNumber, String InvoiceSent, String DepositReceived, String InvoiceDoc, String MeterBoxPhotosSaved, String ElecBillSaved, String ProposedDesignSaved, string UpdatedBy, string PaymentReceipt, string ActiveDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateQuote";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteAccepted";
        if (QuoteAccepted != string.Empty)
            param.Value = QuoteAccepted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignedQuote";
        if (SignedQuote != string.Empty)
            param.Value = SignedQuote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterBoxPhotosSaved";
        if (MeterBoxPhotosSaved != string.Empty)
            param.Value = MeterBoxPhotosSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecBillSaved";
        if (ElecBillSaved != string.Empty)
            param.Value = ElecBillSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProposedDesignSaved";
        if (ProposedDesignSaved != string.Empty)
            param.Value = ProposedDesignSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceDoc";
        if (InvoiceDoc != string.Empty)
            param.Value = InvoiceDoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceSent";
        if (InvoiceSent != string.Empty)
            param.Value = InvoiceSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositReceived";
        if (DepositReceived != string.Empty)
            param.Value = DepositReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentReceipt";
        if (PaymentReceipt != string.Empty)
            param.Value = PaymentReceipt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActiveDate";
        if (ActiveDate != string.Empty)
            param.Value = ActiveDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateFinance(string ProjectID, String ApplicationDate, String AppliedBy, String PurchaseNo, String DocumentSentDate, String DocumentSentBy, String DocumentReceivedDate, String DocumentReceivedBy, String DocumentVerified, String SentBy, string SentDate, String PostalTrackingNumber, String Remarks, String ReceivedDate, String ReceivedBy, String PaymentVerified, string UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateFinance";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApplicationDate";
        if (ApplicationDate != string.Empty)
            param.Value = ApplicationDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AppliedBy";
        if (AppliedBy != string.Empty)
            param.Value = AppliedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseNo";
        if (PurchaseNo != string.Empty)
            param.Value = PurchaseNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocumentSentDate";
        if (DocumentSentDate != string.Empty)
            param.Value = DocumentSentDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocumentSentBy";
        if (DocumentSentBy != string.Empty)
            param.Value = DocumentSentBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocumentReceivedDate";
        if (DocumentReceivedDate != string.Empty)
            param.Value = DocumentReceivedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocumentReceivedBy";
        if (DocumentReceivedBy != string.Empty)
            param.Value = DocumentReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocumentVerified";
        param.Value = DocumentVerified;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SentBy";
        if (SentBy != string.Empty)
            param.Value = SentBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SentDate";
        if (SentDate != string.Empty)
            param.Value = SentDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalTrackingNumber";
        if (PostalTrackingNumber != string.Empty)
            param.Value = PostalTrackingNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Remarks";
        if (Remarks != string.Empty)
            param.Value = Remarks;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedDate";
        if (ReceivedDate != string.Empty)
            param.Value = ReceivedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        if (ReceivedBy != string.Empty)
            param.Value = ReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentVerified";
        param.Value = PaymentVerified;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateForms(string ProjectID, String AdditionalSystem, String GridConnected, String RebateApproved, String ReceivedCredits, String CreditEligible, String MoreThanOneInstall, String RequiredCompliancePaperwork, String OutOfPocketDocs, String OwnerGSTRegistered, String OwnerABN, String InverterCert, String InstallBase, string UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateForms";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdditionalSystem";
        if (AdditionalSystem != string.Empty)
            param.Value = AdditionalSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GridConnected";
        if (GridConnected != string.Empty)
            param.Value = GridConnected;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RebateApproved";
        if (RebateApproved != string.Empty)
            param.Value = RebateApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedCredits";
        if (ReceivedCredits != string.Empty)
            param.Value = ReceivedCredits;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreditEligible";
        if (CreditEligible != string.Empty)
            param.Value = CreditEligible;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MoreThanOneInstall";
        if (MoreThanOneInstall != string.Empty)
            param.Value = MoreThanOneInstall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RequiredCompliancePaperwork";
        if (RequiredCompliancePaperwork != string.Empty)
            param.Value = RequiredCompliancePaperwork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OutOfPocketDocs";
        if (OutOfPocketDocs != string.Empty)
            param.Value = OutOfPocketDocs;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OwnerGSTRegistered";
        if (OwnerGSTRegistered != string.Empty)
            param.Value = OwnerGSTRegistered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OwnerABN";
        if (OwnerABN != string.Empty)
            param.Value = OwnerABN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterCert";
        if (InverterCert != string.Empty)
            param.Value = InverterCert;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBase";
        if (InstallBase != string.Empty)
            param.Value = InstallBase;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSalesCommPaid(string ProjectID, string SalesCommPaid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSalesCommPaid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesCommPaid";
        if (SalesCommPaid != string.Empty)
            param.Value = SalesCommPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateFormsSolar(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateFormsSolar";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateFormsSolarUG(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateFormsSolarUG";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTC(string ProjectID, String STCUploaded, String STCApplied, String STCUploadNumber, String PVDNumber, String PVDStatusID, string UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTC";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploaded";
        if (STCUploaded != string.Empty)
            param.Value = STCUploaded;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploadNumber";
        if (STCUploadNumber != string.Empty)
            param.Value = STCUploadNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTCNotes(string ProjectID, string STCNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTCNotes";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNotes";
        param.Value = STCNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTCByProjNo(string ProjectNumber, String STCApplied, String PVDNumber, String PVDStatusID,string formbayid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTCByProjNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbayid";
        if (formbayid != string.Empty)
            param.Value = formbayid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTC_quickformid(string ProjectID, String formbayid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTC_quickformid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbayid";
        if (formbayid != string.Empty)
            param.Value = formbayid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTC_formbayid(string ProjectID, String formbayid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTC_formbayid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbayid";
        if (formbayid != string.Empty)
            param.Value = formbayid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatePreInst(string ProjectID, String InstallBookingDate, String InstallDays, String ElecDistOK, String InstallerNotes, String ElecDistApproved, String InstallAddress, String InstallCity, String InstallState, String InstallPostCode, String Installer, String Designer, String Electrician, String InstallerNotified, String CustNotifiedInstall, String StockAllocationStore, String InstallAM1, String InstallAM2, String InstallPM1, String InstallPM2, string UpdatedBy, string IsFormBay)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePreInst";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallDays";
        if (InstallDays != string.Empty)
            param.Value = InstallDays;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApproved";
        if (ElecDistApproved != string.Empty)
            param.Value = ElecDistApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistOK";
        if (ElecDistOK != string.Empty)
            param.Value = ElecDistOK;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotified";
        if (InstallerNotified != string.Empty)
            param.Value = InstallerNotified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotifiedInstall";
        param.Value = CustNotifiedInstall;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotes";
        if (InstallerNotes != string.Empty)
            param.Value = InstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockAllocationStore";
        if (StockAllocationStore != string.Empty)
            param.Value = StockAllocationStore;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM1";
        if (InstallAM1 != string.Empty)
            param.Value = InstallAM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM2";
        if (InstallAM2 != string.Empty)
            param.Value = InstallAM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM1";
        if (InstallPM1 != string.Empty)
            param.Value = InstallPM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM2";
        if (InstallPM2 != string.Empty)
            param.Value = InstallPM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsFormBay";
        if (IsFormBay != string.Empty)
            param.Value = IsFormBay;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblProjects_UpdatePreinstallation(string ProjectID, String InstallBookingDate, String InstallDays, String InstallAM1, String InstallAM2, String InstallPM1, String InstallPM2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePreinstallation";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallDays";
        if (InstallDays != string.Empty)
            param.Value = InstallDays;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM1";
        if (InstallAM1 != string.Empty)
            param.Value = InstallAM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM2";
        if (InstallAM2 != string.Empty)
            param.Value = InstallAM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM1";
        if (InstallPM1 != string.Empty)
            param.Value = InstallPM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM2";
        if (InstallPM2 != string.Empty)
            param.Value = InstallPM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@UpdatedBy";
        //if (UpdatedBy != string.Empty)
        //    param.Value = UpdatedBy;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatePreInstaller(string ProjectID, String Installer, String Designer, String Electrician)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePreInstaller";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatePostInst(string ProjectID, String InstallCompleted, String InstallDocsReceived, String InstallVerifiedBy, String STCFormsDone, String STCCheckedBy, String STCFormSaved, String CertificateIssued, String CertificateSaved, String InverterSerial, String SecondInverterSerial, String InvoiceSent, String ReceiptSent, String MeterAppliedDate, String MeterAppliedTime, String MeterAppliedMethod, String MeterAppliedRef, string UpdatedBy,int ComplianceCertificate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePostInst";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceSent";
        if (InvoiceSent != string.Empty)
            param.Value = InvoiceSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceiptSent";
        if (ReceiptSent != string.Empty)
            param.Value = ReceiptSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedRef";
        if (MeterAppliedRef != string.Empty)
            param.Value = MeterAppliedRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedDate";
        if (MeterAppliedDate != string.Empty)
            param.Value = MeterAppliedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedTime";
        if (MeterAppliedTime != string.Empty)
            param.Value = MeterAppliedTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedMethod";
        if (MeterAppliedMethod != string.Empty)
            param.Value = MeterAppliedMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFormsDone";
        if (STCFormsDone != string.Empty)
            param.Value = STCFormsDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallDocsReceived";
        if (InstallDocsReceived != string.Empty)
            param.Value = InstallDocsReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCompleted";
        if (InstallCompleted != string.Empty)
            param.Value = InstallCompleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallVerifiedBy";
        if (InstallVerifiedBy != string.Empty)
            param.Value = InstallVerifiedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSerial";
        if (InverterSerial != string.Empty)
            param.Value = InverterSerial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterSerial";
        if (SecondInverterSerial != string.Empty)
            param.Value = SecondInverterSerial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CertificateIssued";
        if (CertificateIssued != string.Empty)
            param.Value = CertificateIssued;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CertificateSaved";
        if (CertificateSaved != string.Empty)
            param.Value = CertificateSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCCheckedBy";
        if (STCCheckedBy != string.Empty)
            param.Value = STCCheckedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFormSaved";
        if (STCFormSaved != string.Empty)
            param.Value = STCFormSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //HttpContext.Current.Response.Write(ComplianceCertificate);
        //HttpContext.Current.Response.End();
        param = comm.CreateParameter();
        param.ParameterName = "@ComplianceCertificate";
        if (ComplianceCertificate != -1)
            param.Value = ComplianceCertificate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateElecInv(string ProjectID, String InstallerInvNo, String InstallerInvAmnt, String InstallerInvDate, String InstallerPayDate, String ElectricianInvoiceNotes, string UpdatedBy, string PreExtraWork, string PreAmount, string PreApprovedBy, string SalesComm)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateElecInv";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvNo";
        if (InstallerInvNo != string.Empty)
            param.Value = InstallerInvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvAmnt";
        if (InstallerInvAmnt != string.Empty)
            param.Value = InstallerInvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvDate";
        if (InstallerInvDate != string.Empty)
            param.Value = InstallerInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerPayDate";
        if (InstallerPayDate != string.Empty)
            param.Value = InstallerPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianInvoiceNotes";
        if (ElectricianInvoiceNotes != string.Empty)
            param.Value = ElectricianInvoiceNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreExtraWork";
        if (PreExtraWork != string.Empty)
            param.Value = PreExtraWork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreAmount";
        if (PreAmount != string.Empty)
            param.Value = PreAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreApprovedBy";
        if (PreApprovedBy != string.Empty)
            param.Value = PreApprovedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesComm";
        param.Value = SalesComm;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects2_UpdateElecInvTeamC(string ProjectID, String SalesLandingPrice, String SalesMarketPrice, String SalesInvAmnt, String SalesInvNo, String SalesInvDate, string SalesPayNotes, string SalesPayDate, string UserCommision, string EuroCommision)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdateElecInvTeamC";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesLandingPrice";
        if (SalesLandingPrice != string.Empty)
            param.Value = SalesLandingPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesMarketPrice";
        if (SalesMarketPrice != string.Empty)
            param.Value = SalesMarketPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesInvAmnt";
        if (SalesInvAmnt != string.Empty)
            param.Value = SalesInvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesInvNo";
        if (SalesInvNo != string.Empty)
            param.Value = SalesInvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesInvDate";
        if (SalesInvDate != string.Empty)
            param.Value = SalesInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesPayNotes";
        if (SalesPayNotes != string.Empty)
            param.Value = SalesPayNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesPayDate";
        if (SalesPayDate != string.Empty)
            param.Value = SalesPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserCommision";
        if (UserCommision != string.Empty)
            param.Value = UserCommision;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EuroCommision";
        if (EuroCommision != string.Empty)
            param.Value = EuroCommision;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects2_UpdateQuote(string ProjectLinkID, String SPAInvoiceNumber, String SPAPaid, String SPAPaidBy, String SPAPaidAmount, String SPAPaidMethod)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdateQuote";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SPAInvoiceNumber";
        if (SPAInvoiceNumber != string.Empty)
            param.Value = SPAInvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SPAPaid";
        if (SPAPaid != string.Empty)
            param.Value = SPAPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SPAPaidBy";
        if (SPAPaidBy != string.Empty)
            param.Value = SPAPaidBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SPAPaidAmount";
        if (SPAPaidAmount != string.Empty)
            param.Value = SPAPaidAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SPAPaidMethod";
        if (SPAPaidMethod != string.Empty)
            param.Value = SPAPaidMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects2_UpdatePrice(string ProjectLinkID, String PromoText, String Promo1ID, String Promo2ID, string Promo3)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdatePrice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoText";
        if (PromoText != string.Empty)
            param.Value = PromoText;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo1ID";
        if (Promo1ID != string.Empty)
            param.Value = Promo1ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo2ID";
        if (Promo2ID != string.Empty)
            param.Value = Promo2ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo3";
        if (Promo3 != string.Empty)
            param.Value = Promo3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects2_UpdatePostInstInspection(string ProjectLinkID, String InspectorName, string InspectionDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdatePostInstInspection";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InspectorName";
        param.Value = InspectorName;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InspectionDate";
        if (InspectionDate != string.Empty)
            param.Value = InspectionDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@NoDocs";
        //if (NoDocs != string.Empty)
        //    param.Value = NoDocs;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Boolean;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@DocsDone";
        //if (DocsDone != string.Empty)
        //    param.Value = DocsDone;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Boolean;
        //comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects2_UpdatePostInst(string ProjectLinkID, String ProjectIncomplete, string ProjectInCompReason)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdatePostInst";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectIncomplete";
        param.Value = ProjectIncomplete;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectInCompReason";
        if (ProjectInCompReason != string.Empty)
            param.Value = ProjectInCompReason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@NoDocs";
        //if (NoDocs != string.Empty)
        //    param.Value = NoDocs;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Boolean;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@DocsDone";
        //if (DocsDone != string.Empty)
        //    param.Value = DocsDone;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Boolean;
        //comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateProjectStatusID(string ProjectID, String ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateProjectStatusID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateProjectStatusIDByCustomerID(string CustomerID, String ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateProjectStatusIDByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_UpdateStock(string StockItemID, String StockSize, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_UpdateStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_RevertStock(string StockItemID, String StockSize, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_RevertStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_UpdateInvStock(string InverterDetailsID, String SecondInverterDetailsID, string StockLocation, string StockQuantity1, string StockQuantity2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_UpdateInvStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InverterDetailsID";
        param.Value = InverterDetailsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterDetailsID";
        if (SecondInverterDetailsID != string.Empty)
            param.Value = SecondInverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity1";
        if (StockQuantity1 != string.Empty)
            param.Value = StockQuantity1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity2";
        if (StockQuantity2 != string.Empty)
            param.Value = StockQuantity2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_RevertInvStock(string InverterDetailsID, String SecondInverterDetailsID, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_RevertInvStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InverterDetailsID";
        param.Value = InverterDetailsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterDetailsID";
        if (SecondInverterDetailsID != string.Empty)
            param.Value = SecondInverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblElecDistApplyMethod_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistApplyMethod_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblElecDistApplyMethod_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecDistApplyMethod_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblPVDStatus_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatus_SelectActive";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblDefaults_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDefaults_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateSQ(string ProjectID, String SQ)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSQ";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SQ";
        if (SQ != string.Empty)
            param.Value = SQ;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateMP(string ProjectID, String MP)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateMP";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MP";
        if (MP != string.Empty)
            param.Value = MP;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateEB(string ProjectID, String EB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateEB";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EB";
        if (EB != string.Empty)
            param.Value = EB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateST(string ProjectID, String ST)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateST";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ST";
        if (ST != string.Empty)
            param.Value = ST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateCE(string ProjectID, String CE)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateCE";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CE";
        if (CE != string.Empty)
            param.Value = CE;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateInstInvDoc(string ProjectID, String InstInvDoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInstInvDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstInvDoc";
        if (InstInvDoc != string.Empty)
            param.Value = InstInvDoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdatePD(string ProjectID, String PD)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePD";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PD";
        if (PD != string.Empty)
            param.Value = PD;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdatePR(string ProjectID, String PR)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePR";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PR";
        if (PR != string.Empty)
            param.Value = PR;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static int tblInstallBookings_Insert(String ProjectNumber, String BookingDate, String ProjectID, String BookingAmPm, String BookingBooked)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallBookings_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BookingDate";
        param.Value = BookingDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BookingAmPm";
        if (BookingAmPm != string.Empty)
            param.Value = BookingAmPm;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 3;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BookingBooked";
        if (BookingBooked != string.Empty)
            param.Value = BookingBooked;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }


    public static int tblInstallBookings_InsertprojectDetail(String ProjectNumber, String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallBookings_InsertprojectDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblInstallBookings_Delete(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallBookings_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblInvoicePayMethod_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayMethod_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblFPTransType_getInv()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_getInv";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjects_UpdateInvoicePayments(string ProjectID, String InvoiceNotes, String RECRebate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvoicePayments";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNotes";
        if (InvoiceNotes != string.Empty)
            param.Value = InvoiceNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RECRebate";
        if (RECRebate != string.Empty)
            param.Value = RECRebate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable ap_form_element_execute(string sqlstmt)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "ap_form_element_execute";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@sqlstmt";
        param.Value = sqlstmt;
        param.DbType = DbType.String;
        param.Size = 80000;
        comm.Parameters.Add(param);

        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblSTCPostCodes_SelectZoneByState(String PostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSTCPostCodes_SelectZoneByState";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PostCode";

        if (PostCode != string.Empty)
            param.Value = PostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblSTCZoneRatings_SelectBySTCZoneID(String STCZoneID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSTCZoneRatings_SelectBySTCZoneID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@STCZoneID";
        param.Value = STCZoneID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblCustInfo_Exists(String CustomerID, String ContactID, String FollowupDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustInfo_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowupDate";
        if (FollowupDate != string.Empty)
            param.Value = FollowupDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblInstallerNotes_Exists(String ProjectID, String EmployeeID, String Notes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallerNotes_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 2000;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblInvoiceStatus_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceStatus_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateInvDoc(string ProjectID, String InvDoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDoc";
        if (InvDoc != string.Empty)
            param.Value = InvDoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateInvDocDoor(string ProjectID, String InvDocDoor)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvDocDoor";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDocDoor";
        if (InvDocDoor != string.Empty)
            param.Value = InvDocDoor;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblProjects_SelectSalesReport(string ProjectStatusID, string SalesRep, string startdate, string enddate, string employeeid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectSalesReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectInstDocs(string Installer, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectInstDocs";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectProjectNumByInst(string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectProjectNumByInst";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectSalesReportCount(string ProjectStatusID, string SalesRep, string startdate, string enddate, string employeeid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectSalesReportCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectStatus(String ProjectID, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateStockAllocationStore(string ProjectID, String StockAllocationStore)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateStockAllocationStore";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockAllocationStore";
        if (StockAllocationStore != string.Empty)
            param.Value = StockAllocationStore;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateInvData(string ProjectNumber, string InstallerInvNo, string InstallerInvDate, string InstallerInvAmnt, string InstallerPayDate, string ElectricianInvoiceNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvNo";
        param.Value = InstallerInvNo;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvDate";
        if (InstallerInvDate != string.Empty)
            param.Value = InstallerInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvAmnt";
        param.Value = InstallerInvAmnt;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerPayDate";
        if (InstallerPayDate != string.Empty)
            param.Value = InstallerPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianInvoiceNotes";
        param.Value = ElectricianInvoiceNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateD2DData(string ProjectNumber, string SalesInvNo, string SalesMarketPrice, string SalesLandingPrice, string SalesInvAmnt, String SalesInvDate, string SalesPayNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateD2DData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesInvNo";
        param.Value = SalesInvNo;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesMarketPrice";
        param.Value = SalesMarketPrice;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesLandingPrice";
        param.Value = SalesLandingPrice;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesInvAmnt";
        param.Value = SalesInvAmnt;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesInvDate";
        if (SalesInvDate != string.Empty)
            param.Value = SalesInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesPayNotes";
        param.Value = SalesPayNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblInstallerExtraDocs_Insert(String ProjectID, String InstallerID, String DocName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallerExtraDocs_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocName";
        param.Value = DocName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblInstallerExtraDocs_Select(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallerExtraDocs_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblInstallerExtraDocs_UpdateDoc(string ID, String DocName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallerExtraDocs_UpdateDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocName";
        if (DocName != string.Empty)
            param.Value = DocName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblInstallerExtraDocs_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallerExtraDocs_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateInstDocs(string ProjectID, string STCFormSign, string SerialNumbers, string QuotationForm, string CustomerAck, String ComplianceCerti, string CustomerAccept, string EWRNumber, string PatmentMethod)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInstDocs";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFormSign";
        param.Value = STCFormSign;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNumbers";
        param.Value = SerialNumbers;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuotationForm";
        param.Value = QuotationForm;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerAck";
        param.Value = CustomerAck;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ComplianceCerti";
        param.Value = ComplianceCerti;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerAccept";
        param.Value = CustomerAccept;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EWRNumber";
        param.Value = EWRNumber;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PatmentMethod";
        if (PatmentMethod != string.Empty)
            param.Value = PatmentMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTCNo(string ProjectID, String STCNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTCNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumber";
        if (STCNumber != string.Empty)
            param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblProjects_StatusTrackReport(string ProjectStatus, string Status, string startdate, string enddate, string @datetype)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_StatusTrackReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        if (ProjectStatus != string.Empty)
            param.Value = ProjectStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Status";
        if (Status != string.Empty)
            param.Value = Status;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_EmployeeLeft(string startdate, string enddate, string EmployeeID, string type, string InstallState, string NextFollowupDate, string SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_EmployeeLeft";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@NextFollowupDate";
        if (NextFollowupDate != string.Empty)
            param.Value = NextFollowupDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_InstCompCount(String Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_InstCompCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        param.Value = Installer;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static SttblInstallerExtraDocs tblInstallerExtraDocs_SelectByID(String ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallerExtraDocs_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        SttblInstallerExtraDocs details = new SttblInstallerExtraDocs();

        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.ID = dr["ID"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.InstallerID = dr["InstallerID"].ToString();
            details.DocName = dr["DocName"].ToString();
            details.CreateDate = dr["CreateDate"].ToString();
        }
        // return structure details
        return details;
    }

    public static DataTable tblInstallerExtraDocs_SelectByInst(string InstallerID, String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInstallerExtraDocs_SelectByInst";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateNewDate(string CustomerID, String NewNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateNewDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewNotes";
        if (NewNotes != string.Empty)
            param.Value = NewNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblContacts_UpdateNewDate(string CustomerID, String NewNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateNewDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewNotes";
        if (NewNotes != string.Empty)
            param.Value = NewNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjectMaintenance_UpdateDetail(string ProjectID, string OpenDate, string ProjectMtceReasonID, string ProjectMtceReasonSubID, string ProjectMtceCallID, String ProjectMtceStatusID, String Warranty, string CustomerInput, string FaultIdentified, string ActionRequired, string WorkDone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_UpdateDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OpenDate";
        if (OpenDate != string.Empty)
            param.Value = OpenDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonID";
        if (ProjectMtceReasonID != string.Empty)
            param.Value = ProjectMtceReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonSubID";
        if (ProjectMtceReasonSubID != string.Empty)
            param.Value = ProjectMtceReasonSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceCallID";
        if (ProjectMtceCallID != string.Empty)
            param.Value = ProjectMtceCallID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceStatusID";
        if (ProjectMtceStatusID != string.Empty)
            param.Value = ProjectMtceStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Warranty";
        param.Value = Warranty;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerInput";
        if (CustomerInput != string.Empty)
            param.Value = CustomerInput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FaultIdentified";
        if (FaultIdentified != string.Empty)
            param.Value = FaultIdentified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActionRequired";
        if (ActionRequired != string.Empty)
            param.Value = ActionRequired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WorkDone";
        if (WorkDone != string.Empty)
            param.Value = WorkDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateMtce(string ProjectID, String PanelBrandID, String PanelBrand, String PanelOutput, String RECRebate, String STCMultiplier, String STCZoneRating, String PanelModel, String NumberPanels, String SystemCapKW, String InverterDetailsID, String InverterBrand, String InverterSeries, String SecondInverterDetailsID, String STCNumber, String STCValue, String InverterModel, String InverterOutput)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSale";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrandID";
        if (PanelBrandID != string.Empty)
            param.Value = PanelBrandID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrand";
        if (PanelBrand != string.Empty)
            param.Value = PanelBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelOutput";
        if (PanelOutput != string.Empty)
            param.Value = PanelOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetailsID";
        if (InverterDetailsID != string.Empty)
            param.Value = InverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterDetailsID";
        if (SecondInverterDetailsID != string.Empty)
            param.Value = SecondInverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterBrand";
        if (InverterBrand != string.Empty)
            param.Value = InverterBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSeries";
        if (InverterSeries != string.Empty)
            param.Value = InverterSeries;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterOutput";
        if (InverterOutput != string.Empty)
            param.Value = InverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NumberPanels";
        if (NumberPanels != string.Empty)
            param.Value = NumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKW";
        if (SystemCapKW != string.Empty)
            param.Value = SystemCapKW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCMultiplier";
        if (STCMultiplier != string.Empty)
            param.Value = STCMultiplier;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCZoneRating";
        if (STCZoneRating != string.Empty)
            param.Value = STCZoneRating;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumber";
        if (STCNumber != string.Empty)
            param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCValue";
        if (STCValue != string.Empty)
            param.Value = STCValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RECRebate";
        if (RECRebate != string.Empty)
            param.Value = RECRebate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);


       

        int result = -1;
      
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateInstDetail(string ProjectID, String ElecDistOK, string ElecDistApproved, string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInstDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistOK";
        if (ElecDistOK != string.Empty)
            param.Value = ElecDistOK;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApproved";
        if (ElecDistApproved != string.Empty)
            param.Value = ElecDistApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblProjects_FormBay(String IsFormBay)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_FormBay";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@IsFormBay";
        param.Value = IsFormBay;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateIsFormBay(string ProjectID, String IsFormBay)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateIsFormBay";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsFormBay";
        if (IsFormBay != string.Empty)
            param.Value = IsFormBay;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateDepositReceived(string ProjectID, String DepositReceived)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateDepositReceived";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositReceived";
        if (DepositReceived != string.Empty)
            param.Value = DepositReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdatePanelComm(string ProjectID, String PanelComm)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePanelComm";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelComm";
        if (PanelComm != string.Empty)
            param.Value = PanelComm;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_Update_Address(string ProjectID, string unit_type, string unit_number, string street_number, string street_name, string street_type, string street_suffix, string project)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_Address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_type";
        if (unit_type != string.Empty)
            param.Value = unit_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_number";
        if (unit_number != string.Empty)
            param.Value = unit_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_number";
        if (street_number != string.Empty)
            param.Value = street_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_name";
        if (street_name != string.Empty)
            param.Value = street_name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_type";
        if (street_type != string.Empty)
            param.Value = street_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_suffix";
        if (street_suffix != string.Empty)
            param.Value = street_suffix;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (street_type != string.Empty)
            param.Value = project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomer_Update_street_address(string ProjectID, String street_address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_Update_street_address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblProjects_Update_InverterReplacementDetail(string ProjectID, String ThroughType, String OldSerialNo, String LogdementNo, String LogdementDate, String TrackingNo, String NewSerailNo, string ErrorMessage, string FaultyUnitPickupDateTime, string PickupThrough, string DelieveryReceived, string SenttoSuppllierDatetime, string SentThrough, string InvoiceSentDateTime, string InvoiceNo, string InstallerPickup)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_InverterReplacementDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ThroughType";
        if (ThroughType != string.Empty)
            param.Value = ThroughType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldSerialNo";
        if (OldSerialNo != string.Empty)
            param.Value = OldSerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LogdementNo";
        if (LogdementNo != string.Empty)
            param.Value = LogdementNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LogdementDate";
        if (LogdementDate != string.Empty)
            param.Value = LogdementDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@TrackingNo";
        if (TrackingNo != string.Empty)
            param.Value = TrackingNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewSerailNo";
        if (NewSerailNo != string.Empty)
            param.Value = NewSerailNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ErrorMessage";
        if (ErrorMessage != string.Empty)
            param.Value = ErrorMessage;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FaultyUnitPickupDateTime";
        if (FaultyUnitPickupDateTime != string.Empty)
            param.Value = FaultyUnitPickupDateTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickupThrough";
        if (PickupThrough != string.Empty)
            param.Value = PickupThrough;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DelieveryReceived";
        if (DelieveryReceived != string.Empty)
            param.Value = DelieveryReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SenttoSuppllierDatetime";
        if (SenttoSuppllierDatetime != string.Empty)
            param.Value = SenttoSuppllierDatetime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SentThrough";
        if (SentThrough != string.Empty)
            param.Value = SentThrough;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceSentDateTime";
        if (InvoiceSentDateTime != string.Empty)
            param.Value = InvoiceSentDateTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
   
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size =50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerPickup";
        if (InstallerPickup != string.Empty)
            param.Value = InstallerPickup;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_Updatebeatquote(string ProjectID, String beatquote)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatebeatquote";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@beatquote";
        if (beatquote != string.Empty)
            param.Value = beatquote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateBeatQuoteDoc(string ProjectID, String beatquotedoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateBeatQuoteDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@beatquotedoc";
        if (beatquotedoc != string.Empty)
            param.Value = beatquotedoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSalesType(string ProjectID, String SalesType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSalesType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesType";
        if (SalesType != string.Empty)
            param.Value = SalesType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblFPTransType_getInvbyFPTransTypeID(String FPTransTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_getInvbyFPTransTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        param.Value = FPTransTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
            
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjects_UpdateSale_meterupgrade(string ProjectID, String meterupgrade)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSale_meterupgrade";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@meterupgrade";
        if (meterupgrade != string.Empty)
            param.Value = meterupgrade;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);



        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblProjects_Updatenearmapdoc(string ProjectID, String nearmapdoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatenearmapdoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@nearmapdoc";
        if (nearmapdoc != string.Empty)
            param.Value = nearmapdoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_Updatenearmapcheckbox(string ProjectID, String nearmapcheck)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatenearmapcheckbox";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@nearmapcheck";
        if (nearmapcheck != string.Empty)
            param.Value = nearmapcheck;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjects_IsClickCustomer(string ProjectID, String IsClickCustomer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_IsClickCustomer";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsClickCustomer";
        if (IsClickCustomer != string.Empty)
            param.Value = IsClickCustomer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateIsSMReady(string ProjectID, String IsSMReady)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateIsSMReady";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsSMReady";
        if (IsSMReady != string.Empty)
            param.Value = IsSMReady;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Updatenearmapdoc_New(string ProjectID, string nearmapdoc1)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatenearmapdoc_New";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@nearmapdoc1";
        param.Value = nearmapdoc1;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }

    public static bool tblProjects_Updatenearmapdoc_New1(string ProjectID, string nearmapdoc2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatenearmapdoc_New1";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@nearmapdoc2";
        param.Value = nearmapdoc2;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }


    public static bool tblprojects_UpdateInstallEndDate(string ProjectID, string EndDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojects_UpdateInstallEndDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectId";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallEndDate";
        param.Value = EndDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }

    public static bool tblProjects_UpdatenearmapPdf(string ProjectID, string nearmapdoc2)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatenearmapPdf";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@nearmapdoc";
        param.Value = nearmapdoc2;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }

    public static bool tblprojects_Update_mgrrdiscdatetime_Amt(string ProjectID, string MgrDiscount,string DateTime)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojects_Update_mgrrdiscdatetime_Amt";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@mgrrdisc";
        if (MgrDiscount != string.Empty)
            param.Value = MgrDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@mgrrdiscdatetime";
        if (DateTime != string.Empty)
            param.Value = DateTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }
    public static DataTable GetInstallerData(String Insytallerid, string date)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetInstallerData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@installerId";
        param.Value = Insytallerid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@installdate";
        param.Value = date;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    public static int tblNearMapImages_Insert(String ProjectID, String ProjectNumber, string filename,string Date )
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNearMapImages_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value =ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pdffilename";
        param.Value = filename;
        param.DbType = DbType.String;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@addeddate";
        param.Value = Date;
        param.DbType = DbType.DateTime;
        //param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblNearMapImages_UpdateImage3(string ID, string Image3)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNearMapImages_UpdateImage3";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@image1";
        param.Value = Image3;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }
    public static bool tblNearMapImages_UpdateImage2(string ID, string Image3)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNearMapImages_UpdateImage2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@image1";
        param.Value = Image3;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }
    public static bool tblNearMapImages_UpdateImage1(string ID, string Image3)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNearMapImages_UpdateImage1";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@image1";
        param.Value = Image3;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }
    public static DataTable tblNearMapImages_GetData(String ProjectId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNearMapImages_GetData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    public static int tbl_GridSentEmailLog_InsertData(String ProjectID, String ProjectNumber, string ContactID,string Email ,string Date,string Sentby)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_GridSentEmailLog_InsertData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEmail";
        param.Value = Email;
        param.DbType = DbType.String;
        //param.Size = 200;
        comm.Parameters.Add(param);
        

        param = comm.CreateParameter();
        param.ParameterName = "@sentdatetime";
        param.Value = Date;
        param.DbType = DbType.DateTime;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@sentby";
        param.Value = Sentby;
        param.DbType = DbType.Int32;
        //param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblprojects_updateemailflag(string ID, string EmailFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojects_updateemailflag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Emailflag";
        param.Value = EmailFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {

        }

        catch (Exception ex)
        {

        }

        result = DataAccess.ExecuteNonQuery(comm);
        return (result != -1);
    }
    public static bool tblProjects_UpdateSale_Appl(string ProjectID, String MeterNumber1, String MeterNumber2, string MeterPhase, String MeterEnoughSpace,string OffPeak)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSale_Appl";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber1";
        if (MeterNumber1 != string.Empty)
            param.Value = MeterNumber1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber2";
        if (MeterNumber2 != string.Empty)
            param.Value = MeterNumber2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        
        param = comm.CreateParameter();
        param.ParameterName = "@MeterPhase";
        if (MeterPhase != string.Empty)
            param.Value = MeterPhase;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterEnoughSpace";
        if (MeterEnoughSpace != string.Empty)
            param.Value = MeterEnoughSpace;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OffPeak";
        if (OffPeak != string.Empty)
            param.Value = OffPeak;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);
        //HttpContext.Current.Response.Write(SecondInverterDetailsID);
        //HttpContext.Current.Response.End();

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblPVDStatus_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatus_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}
