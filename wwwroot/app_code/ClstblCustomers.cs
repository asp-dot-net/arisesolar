using System;
using System.Web;
using System.Data;
using System.Data.Common;

public struct SttblCustomers
{
    public string CompanyNumber;
    public string EmployeeID;
    public string CustTag;
    public string CustTypeID;
    public string CustSourceID;
    public string CustSourceSubID;
    public string InvoicePayMethodID;
    public string SupplierPayByID;
    public string ResCom;
    public string CustEntered;
    public string CustEnteredBy;
    public string ReferredBy;
    public string RefName;
    public string RefBSB;
    public string RefAcct;
    public string RefPaid;
    public string Customer;
    public string BranchLocation;
    public string IndustryCat;
    public string StreetAddress;
    public string StreetCity;
    public string StreetState;
    public string StreetPostCode;
    public string PostalAddress;
    public string PostalCity;
    public string PostalState;
    public string PostalPostCode;
    public string Country;
    public string CustPhone;
    public string CustPhone2;
    public string CustPhone3;
    public string CustAltPhone;
    public string CustFax;
    public string CustNotes;
    public string CustFollowUp;
    public string CustWebSite;
    public string CustWebSiteLink;
    public string DiscountPlanID;
    public string RecurringBills;
    public string LinkID;
    public string OldQuoteNumber;
    public string ContFirstTM;
    public string ContLastTM;
    public string ContEmail;
    public string ContMobileTM;
    public string ContMobile2TM;
    public string upsize_ts;
    public string Area;
    public string AssignBy;
    public string AssignDate;
    public string AssignFlag;
    public string D2DEmployee;
    public string D2DEnteredBy;
    public string D2DEntered;
    public string D2DAppDate;
    public string D2DAppTime;
    public string D2DAppNote;
    public string appointment_status;
    public string customer_lead_status;
    public string D2DAppAttendedDate;
    public string AppFixDate;
    public string AppFixBy;
    public string RescheduleNote;
    public string RescheduleTime;
    public string RescheduleDate;
    public string CancelNotes;
    public string AttendFlag;
    public string AttendReason;
    public string NextFollowupDate;
    public string desp;

    public string CustType;
    public string CustSource;
    public string CustSourceSub;
    public string D2DEmpName;
    public string AppTime;
    public string street_address;
    public string Postal_address;
    public string isformbayadd;
    public string street_suffix;
    public string pstreet_suffix;
    public string street_number;
    public string street_type;

    public string pstreet_number;

    public string unit_type;
    public string unit_number;
    public string street_name;
    public string lead_status;
    public string CustImageID;
    public string Imagename;
    public string pstreet_name;
    public string SolarType;
    public string brgStreetName;
    public string gclid;
}

public class ClstblCustomers
{

    public static int tbl_contactdetaillog_InsertContacts(String contactid, String leadstatus, String salesrapid, String createuser)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_contactdetaillog_InsertContacts";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@contactid";
        if (contactid != string.Empty)
            param.Value = contactid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@leadstatusid";
        if (leadstatus != string.Empty)
            param.Value = leadstatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@salesemployeeid";
        if (salesrapid != string.Empty)
            param.Value = salesrapid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@empid";
        if (createuser != string.Empty)
            param.Value = createuser;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        //HttpContext.Current.Response.Write(returnid);
        //HttpContext.Current.Response.End();
        return returnid;
    }


    public static DataTable tbl_getUseridFromCustomerid(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_getUseridFromCustomerid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        return table;
    }

    public static DataTable getUserRoles(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "getUserRoles";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        return table;
    }



    public static SttblCustomers tblCustomers_SelectByCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustomers details = new SttblCustomers();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CompanyNumber = dr["CompanyNumber"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.CustTag = dr["CustTag"].ToString();
            details.CustTypeID = dr["CustTypeID"].ToString();
            details.CustSourceID = dr["CustSourceID"].ToString();
            details.CustSourceSubID = dr["CustSourceSubID"].ToString();
            details.InvoicePayMethodID = dr["InvoicePayMethodID"].ToString();
            details.SupplierPayByID = dr["SupplierPayByID"].ToString();
            details.ResCom = dr["ResCom"].ToString();
            details.CustEntered = dr["CustEntered"].ToString();
            details.CustEnteredBy = dr["CustEnteredBy"].ToString();
            details.ReferredBy = dr["ReferredBy"].ToString();
            details.RefName = dr["RefName"].ToString();
            details.RefBSB = dr["RefBSB"].ToString();
            details.RefAcct = dr["RefAcct"].ToString();
            details.RefPaid = dr["RefPaid"].ToString();
            details.Customer = dr["Customer"].ToString();
            details.BranchLocation = dr["BranchLocation"].ToString();
            details.IndustryCat = dr["IndustryCat"].ToString();
            details.StreetAddress = dr["StreetAddress"].ToString();
            details.StreetCity = dr["StreetCity"].ToString();
            details.StreetState = dr["StreetState"].ToString();
            details.StreetPostCode = dr["StreetPostCode"].ToString();
            details.PostalAddress = dr["PostalAddress"].ToString();
            details.PostalCity = dr["PostalCity"].ToString();
            details.PostalState = dr["PostalState"].ToString();
            details.PostalPostCode = dr["PostalPostCode"].ToString();

            details.Country = dr["Country"].ToString();
            details.CustPhone = dr["CustPhone"].ToString();
            details.CustPhone2 = dr["CustPhone2"].ToString();
            details.CustPhone3 = dr["CustPhone3"].ToString();
            details.CustAltPhone = dr["CustAltPhone"].ToString();
            details.CustFax = dr["CustFax"].ToString();
            details.CustNotes = dr["CustNotes"].ToString();
            details.CustFollowUp = dr["CustFollowUp"].ToString();
            details.CustWebSite = dr["CustWebSite"].ToString();
            details.CustWebSiteLink = dr["CustWebSiteLink"].ToString();
            details.DiscountPlanID = dr["DiscountPlanID"].ToString();
            details.RecurringBills = dr["RecurringBills"].ToString();
            details.LinkID = dr["LinkID"].ToString();
            details.OldQuoteNumber = dr["OldQuoteNumber"].ToString();
            details.ContFirstTM = dr["ContFirstTM"].ToString();
            details.ContLastTM = dr["ContLastTM"].ToString();
            details.ContMobileTM = dr["ContMobileTM"].ToString();
            details.ContMobile2TM = dr["ContMobile2TM"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.Area = dr["Area"].ToString();
            details.AssignBy = dr["AssignBy"].ToString();
            details.AssignDate = dr["AssignDate"].ToString();
            details.AssignFlag = dr["AssignFlag"].ToString();
            details.D2DEmployee = dr["D2DEmployee"].ToString();
            details.D2DEntered = dr["D2DEntered"].ToString();
            details.D2DEnteredBy = dr["D2DEnteredBy"].ToString();
            details.D2DAppDate = dr["D2DAppDate"].ToString();
            details.D2DAppTime = dr["D2DAppTime"].ToString();
            //details.ContEmail = dr["ContEmail"].ToString();
            details.D2DAppNote = dr["D2DAppNote"].ToString();
            details.appointment_status = dr["appointment_status"].ToString();
            details.customer_lead_status = dr["customer_lead_status"].ToString();
            details.D2DAppAttendedDate = dr["D2DAppAttendedDate"].ToString();
            details.AppFixDate = dr["AppFixDate"].ToString();
            details.AppFixBy = dr["AppFixBy"].ToString();
            details.RescheduleNote = dr["RescheduleNote"].ToString();
            details.RescheduleTime = dr["RescheduleTime"].ToString();
            details.RescheduleDate = dr["RescheduleDate"].ToString();
            details.CancelNotes = dr["CancelNotes"].ToString();
            details.AttendFlag = dr["AttendFlag"].ToString();
            details.AttendReason = dr["AttendReason"].ToString();
            details.NextFollowupDate = dr["NextFollowupDate"].ToString();
            details.desp = dr["desp"].ToString();

            details.CustType = dr["CustType"].ToString();
            details.CustSource = dr["CustSource"].ToString();
            details.CustSourceSub = dr["CustSourceSub"].ToString();
            details.D2DEmpName = dr["D2DEmpName"].ToString();
            details.AppTime = dr["AppTime"].ToString();
            details.street_address = dr["street_address"].ToString();
            details.Postal_address = dr["Postal_address"].ToString();
            details.isformbayadd = dr["isformbayadd"].ToString();


            details.street_number = dr["street_number"].ToString();
            details.street_type = dr["street_type"].ToString();

            details.pstreet_number = dr["pstreet_number"].ToString();

            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.pstreet_name = dr["pstreet_name"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_suffix = dr["street_suffix"].ToString();
            details.lead_status = dr["lead_status"].ToString();
            details.SolarType = dr["SolarType"].ToString();
            details.brgStreetName = dr["brgStreetName"].ToString();
            details.gclid = dr["gclid"].ToString();
        }

        return details;
    }

    public static DataTable tblCustomers_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_getcount(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_getcount";

        DataTable result = new DataTable();
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_SelectByAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_SelectByEmployeeIDIsNull()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByEmployeeIDIsNull";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable AdmintblCustomersGetCount(string userid, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "AdmintblCustomersGetCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomers_SelectByUserId(String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByUserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_SelectByEmpType_Team(String SalesTeamID, String EmpType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByEmpType_Team";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpType";
        if (EmpType != string.Empty)
            param.Value = EmpType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_Select_NewLead_ByEmployeeID(String EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Select_NewLead_ByEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_Select_NewLead_ByUserId(String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Select_NewLead_ByUserId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_SelectByCheckExists(String Customer, String CustPhone, String ContEmail, string ContMobile)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByCheckExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        if (CustPhone != string.Empty)
            param.Value = CustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblCustomers_Insert(String CompanyNumber, String EmployeeID, String CustTag, String CustTypeID, String CustSourceID, String CustSourceSubID, String InvoicePayMethodID, String SupplierPayByID, String ResCom, String CustEnteredBy, String ReferredBy, String RefName, String RefBSB, String RefAcct, String RefPaid, String Customer, String BranchLocation, String IndustryCat, String StreetAddress, String StreetCity, String StreetState, String StreetPostCode, String PostalAddress, String PostalCity, String PostalState, String PostalPostCode, String Country, String CustPhone, String CustPhone2, String CustPhone3, String CustAltPhone, String CustFax, String CustNotes, String CustFollowUp, String CustWebSite, String CustWebSiteLink, String DiscountPlanID, String RecurringBills, String LinkID, String OldQuoteNumber, String ContFirstTM, String ContLastTM, String ContMobileTM, String ContMobile2TM, String upsize_ts, String Area)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTag";
        if (CustTag != string.Empty)
            param.Value = CustTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        if (InvoicePayMethodID != string.Empty)
            param.Value = InvoicePayMethodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SupplierPayByID";
        if (SupplierPayByID != string.Empty)
            param.Value = SupplierPayByID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEnteredBy";
        if (CustEnteredBy != string.Empty)
            param.Value = CustEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferredBy";
        if (ReferredBy != string.Empty)
            param.Value = ReferredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefName";
        if (RefName != string.Empty)
            param.Value = RefName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefBSB";
        if (RefBSB != string.Empty)
            param.Value = RefBSB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 12;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefAcct";
        if (RefAcct != string.Empty)
            param.Value = RefAcct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefPaid";
        if (RefPaid != string.Empty)
            param.Value = RefPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BranchLocation";
        if (BranchLocation != string.Empty)
            param.Value = BranchLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IndustryCat";
        if (IndustryCat != string.Empty)
            param.Value = IndustryCat;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalAddress";
        if (PostalAddress != string.Empty)
            param.Value = PostalAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalCity";
        if (PostalCity != string.Empty)
            param.Value = PostalCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalState";
        if (PostalState != string.Empty)
            param.Value = PostalState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalPostCode";
        if (PostalPostCode != string.Empty)
            param.Value = PostalPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Country";
        if (Country != string.Empty)
            param.Value = Country;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        if (CustPhone != string.Empty)
            param.Value = CustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone2";
        if (CustPhone2 != string.Empty)
            param.Value = CustPhone2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone3";
        if (CustPhone3 != string.Empty)
            param.Value = CustPhone3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustAltPhone";
        if (CustAltPhone != string.Empty)
            param.Value = CustAltPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustFax";
        if (CustFax != string.Empty)
            param.Value = CustFax;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotes";
        if (CustNotes != string.Empty)
            param.Value = CustNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustFollowUp";
        if (CustFollowUp != string.Empty)
            param.Value = CustFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustWebSite";
        if (CustWebSite != string.Empty)
            param.Value = CustWebSite;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustWebSiteLink";
        if (CustWebSiteLink != string.Empty)
            param.Value = CustWebSiteLink;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DiscountPlanID";
        if (DiscountPlanID != string.Empty)
            param.Value = DiscountPlanID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RecurringBills";
        if (RecurringBills != string.Empty)
            param.Value = RecurringBills;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LinkID";
        if (LinkID != string.Empty)
            param.Value = LinkID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldQuoteNumber";
        if (OldQuoteNumber != string.Empty)
            param.Value = OldQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirstTM";
        if (ContFirstTM != string.Empty)
            param.Value = ContFirstTM;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLastTM";
        if (ContLastTM != string.Empty)
            param.Value = ContLastTM;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobileTM";
        if (ContMobileTM != string.Empty)
            param.Value = ContMobileTM;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile2TM";
        if (ContMobile2TM != string.Empty)
            param.Value = ContMobile2TM;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        if (Area != string.Empty)
            param.Value = Area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblCompanyNumber_Insert(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCompanyNumber_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static DataTable tblCompanyNumber_Select(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCompanyNumber_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustomers_Update(string CustomerID, String EmployeeID, String CustTag, String CustTypeID, String CustSourceID, String CustSourceSubID, String InvoicePayMethodID, String SupplierPayByID, String ResCom, String CustEnteredBy, String ReferredBy, String RefName, String RefBSB, String RefAcct, String RefPaid, String BranchLocation, String IndustryCat, String StreetAddress, String StreetCity, String StreetState, String StreetPostCode, String PostalAddress, String PostalCity, String PostalState, String PostalPostCode, String Country, String CustPhone, String CustPhone2, String CustPhone3, String CustAltPhone, String CustFax, String CustNotes, String CustFollowUp, String CustWebSite, String CustWebSiteLink, String DiscountPlanID, String RecurringBills, String LinkID, String OldQuoteNumber, String ContFirstTM, String ContLastTM, String ContMobileTM, String ContMobile2TM, String upsize_ts, String Area, String Customer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTag";
        if (CustTag != string.Empty)
            param.Value = CustTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        if (InvoicePayMethodID != string.Empty)
            param.Value = InvoicePayMethodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SupplierPayByID";
        if (SupplierPayByID != string.Empty)
            param.Value = SupplierPayByID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEnteredBy";
        if (CustEnteredBy != string.Empty)
            param.Value = CustEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferredBy";
        if (ReferredBy != string.Empty)
            param.Value = ReferredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefName";
        if (RefName != string.Empty)
            param.Value = RefName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefBSB";
        if (RefBSB != string.Empty)
            param.Value = RefBSB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 12;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefAcct";
        if (RefAcct != string.Empty)
            param.Value = RefAcct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefPaid";
        if (RefPaid != string.Empty)
            param.Value = RefPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@Customer";
        //if (Customer != string.Empty)
        //    param.Value = Customer;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 100;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BranchLocation";
        if (BranchLocation != string.Empty)
            param.Value = BranchLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IndustryCat";
        if (IndustryCat != string.Empty)
            param.Value = IndustryCat;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalAddress";
        if (PostalAddress != string.Empty)
            param.Value = PostalAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalCity";
        if (PostalCity != string.Empty)
            param.Value = PostalCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalState";
        if (PostalState != string.Empty)
            param.Value = PostalState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalPostCode";
        if (PostalPostCode != string.Empty)
            param.Value = PostalPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Country";
        if (Country != string.Empty)
            param.Value = Country;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        if (CustPhone != string.Empty)
            param.Value = CustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone2";
        if (CustPhone2 != string.Empty)
            param.Value = CustPhone2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone3";
        if (CustPhone3 != string.Empty)
            param.Value = CustPhone3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustAltPhone";
        if (CustAltPhone != string.Empty)
            param.Value = CustAltPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustFax";
        if (CustFax != string.Empty)
            param.Value = CustFax;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotes";
        if (CustNotes != string.Empty)
            param.Value = CustNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustFollowUp";
        if (CustFollowUp != string.Empty)
            param.Value = CustFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustWebSite";
        if (CustWebSite != string.Empty)
            param.Value = CustWebSite;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustWebSiteLink";
        if (CustWebSiteLink != string.Empty)
            param.Value = CustWebSiteLink;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DiscountPlanID";
        if (DiscountPlanID != string.Empty)
            param.Value = DiscountPlanID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RecurringBills";
        if (RecurringBills != string.Empty)
            param.Value = RecurringBills;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LinkID";
        if (LinkID != string.Empty)
            param.Value = LinkID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldQuoteNumber";
        if (OldQuoteNumber != string.Empty)
            param.Value = OldQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirstTM";
        if (ContFirstTM != string.Empty)
            param.Value = ContFirstTM;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLastTM";
        if (ContLastTM != string.Empty)
            param.Value = ContLastTM;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobileTM";
        param.Value = ContMobileTM;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile2TM";
        param.Value = ContMobile2TM;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        if (Area != string.Empty)
            param.Value = Area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblCustomers_Update_EmployeeID(string CustomerID, String EmployeeID, String CustEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Update_EmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEnteredBy";
        if (CustEnteredBy != string.Empty)
            param.Value = CustEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblCustomers_Update_Assignemployee(string CustomerID, String EmployeeID, String AssignBy, string AssignDate, string AssignFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Update_Assignemployee";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AssignBy";
        if (AssignBy != string.Empty)
            param.Value = AssignBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AssignDate";
        if (AssignDate != string.Empty)
            param.Value = AssignDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AssignFlag";
        if (AssignFlag != string.Empty)
            param.Value = AssignFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static string tblCustomers_InsertUpdate(Int32 CustomerID, String CompanyNumber, String EmployeeID, String CustTag, String CustTypeID, String CustSourceID, String CustSourceSubID, String InvoicePayMethodID, String SupplierPayByID, String ResCom, String CustEntered, String CustEnteredBy, String ReferredBy, String RefName, String RefBSB, String RefAcct, String RefPaid, String Customer, String BranchLocation, String IndustryCat, String StreetAddress, String StreetCity, String StreetState, String StreetPostCode, String PostalAddress, String PostalCity, String PostalState, String PostalPostCode, String Country, String CustPhone, String CustPhone2, String CustPhone3, String CustAltPhone, String CustFax, String CustNotes, String CustFollowUp, String CustWebSite, String CustWebSiteLink, String DiscountPlanID, String RecurringBills, String LinkID, String OldQuoteNumber, String ContFirstTM, String ContLastTM, String ContMobileTM, String ContMobile2TM, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        param.Value = CompanyNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTag";
        param.Value = CustTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        param.Value = CustTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        param.Value = InvoicePayMethodID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SupplierPayByID";
        param.Value = SupplierPayByID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        param.Value = ResCom;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEntered";
        param.Value = CustEntered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustEnteredBy";
        param.Value = CustEnteredBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferredBy";
        param.Value = ReferredBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefName";
        param.Value = RefName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefBSB";
        param.Value = RefBSB;
        param.DbType = DbType.String;
        param.Size = 12;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefAcct";
        param.Value = RefAcct;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefPaid";
        param.Value = RefPaid;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        param.Value = Customer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BranchLocation";
        param.Value = BranchLocation;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IndustryCat";
        param.Value = IndustryCat;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        param.Value = StreetAddress;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        param.Value = StreetCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        param.Value = StreetState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        param.Value = StreetPostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalAddress";
        param.Value = PostalAddress;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalCity";
        param.Value = PostalCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalState";
        param.Value = PostalState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalPostCode";
        param.Value = PostalPostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Country";
        param.Value = Country;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        param.Value = CustPhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone2";
        param.Value = CustPhone2;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone3";
        param.Value = CustPhone3;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustAltPhone";
        param.Value = CustAltPhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustFax";
        param.Value = CustFax;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotes";
        param.Value = CustNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustFollowUp";
        param.Value = CustFollowUp;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustWebSite";
        param.Value = CustWebSite;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustWebSiteLink";
        param.Value = CustWebSiteLink;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DiscountPlanID";
        param.Value = DiscountPlanID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RecurringBills";
        param.Value = RecurringBills;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LinkID";
        param.Value = LinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldQuoteNumber";
        param.Value = OldQuoteNumber;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirstTM";
        param.Value = ContFirstTM;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLastTM";
        param.Value = ContLastTM;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobileTM";
        param.Value = ContMobileTM;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile2TM";
        param.Value = ContMobile2TM;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        param.Value = upsize_ts;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustomers_Delete(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblCustomers_InsertContacts(String CustomerID, String ContMr, String ContFirst, String ContLast, String ContMobile, String ContEmail, String EmployeeID, String ContactEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_InsertContacts";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMr";
        param.Value = ContMr;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        param.Value = ContFirst;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        param.Value = ContLast;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactEnteredBy";
        if (ContactEnteredBy != string.Empty)
            param.Value = ContactEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblContacts_UpdateEmployeeID(string CustomerID, String EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_UpdateEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblCustomers_Updategclid(string CustomerID, string gclid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Updategclid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@gclid";
        if (gclid != string.Empty)
            param.Value = gclid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblCustomers_InsertContNotes(String ContactID, String ContNote)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_InsertContNotes";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNote";
        if (ContNote != string.Empty)
            param.Value = ContNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblCustomersAdminGetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomersAdminGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustomersAdminGetDataBySearch(string alpha, string CustTypeID, string CustSourceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomersAdminGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustomersGetDataByAlpha(string alpha, String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomersGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblCustomersGetDataBySearch(string alpha, string employeeid, string CustTypeID, string CustSourceID, string ResCom, string Area, string StreetState, string startdate, string enddate, string StreetPostCode, string curemployeeid, string CompanyNumber, string StreetCity, string StreetAddress, string CustSourceSubID) //, string startindex, string pageindex
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomersGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        if (curemployeeid != string.Empty)
            param.Value = curemployeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        if (Area != string.Empty)
            param.Value = Area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        param.Value = StreetState;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        param.Value = StreetPostCode;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        param.Value = StreetCity;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@startindex";
        //if (startindex != string.Empty)
        //    param.Value = startindex;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@pageindex";
        //if (pageindex != "All")
        //    param.Value = pageindex;
        //else
        //    param.Value = "0";
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustomersGetDataCount(string alpha, string employeeid, string CustTypeID, string CustSourceID, string ResCom, string Area, string StreetState, string startdate, string enddate, string StreetPostCode, string curemployeeid, string CompanyNumber, string StreetCity, string StreetAddress, string CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomersGetDataCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        if (curemployeeid != string.Empty)
            param.Value = curemployeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        if (Area != string.Empty)
            param.Value = Area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        param.Value = StreetState;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        param.Value = StreetPostCode;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        param.Value = StreetCity;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@startindex";
        //if (startindex != string.Empty)
        //    param.Value = startindex;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@pageindex";
        //if (pageindex != "All")
        //    param.Value = pageindex;
        //else
        //    param.Value = "0";
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);
        //HttpContext.Current.Response.Write(alpha + "','" + employeeid + "','" + CustTypeID + "','" + CustSourceID + "','" + ResCom + "','" + Area + "','" + StreetState + "','" + startdate + "','" + enddate + "','" + StreetPostCode + "','" + curemployeeid + "','" + CompanyNumber + "','" + StreetCity + "','" + StreetAddress + "','" + CustSourceSubID);
        //HttpContext.Current.Response.End();
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustomers_SelectRoleWise(string alpha, String userid, string CustTypeID, string CustSourceID, string ResCom, string Area, string StreetState, string startdate, string enddate, string StreetPostCode, string curuserid, string CompanyNumber, string StreetCity, string StreetAddress, string CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectRoleWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        param.Value = curuserid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        if (Area != string.Empty)
            param.Value = Area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        param.Value = StreetState;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        param.Value = StreetPostCode;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        param.Value = StreetCity;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustomersAdminGetDataBySearchBYEmployeeIDIsNull(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomersAdminGetDataBySearchBYEmployeeIDIsNull";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblPostCodes_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblPostCodes_SelectBy_PostCodeID(string Suburb)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_SelectBy_PostCodeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Suburb";
        param.Value = Suburb;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblContacts_ExistName(string Contact)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        param.Value = Contact;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblContacts_ExistMobile(string Contact)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistMobile";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        param.Value = Contact;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblCustomers_ExistPhone(string Contact)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExistPhone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        param.Value = Contact;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblContacts_ExistEmail(string Contact)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistEmail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        param.Value = Contact;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static DataTable tblContacts_ExistSelectEmail(string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistSelectEmail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_ExistSelectMobile(string ContMobile)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistSelectMobile";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomers_ExistSelectPhone(string CustPhone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExistSelectPhone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        param.Value = CustPhone;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_ExistSelect(string Contact)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_ExistSelect";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        param.Value = Contact;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCompanyLocations_SelectID(string State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCompanyLocations_SelectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@State";
        param.Value = State;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustomers_UpdateCustType(string CustTypeID, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateCustType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        param.Value = CustTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static int tblCustLogReport_Insert(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustLogReport_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblCustomers_UpdateCompanyNumber(string CustomerID, string CompanyNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateCompanyNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        param.Value = CompanyNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomers_UpdateD2DEmp(string CustomerID, string D2DEmployee, string D2DEnteredBy, string D2DAppDate, string D2DAppTime)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateD2DEmp";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DEmployee";
        if (D2DEmployee != string.Empty)
            param.Value = D2DEmployee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DEnteredBy";
        if (D2DEnteredBy != string.Empty)
            param.Value = D2DEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppDate";
        if (D2DAppDate != string.Empty)
            param.Value = D2DAppDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppTime";
        if (D2DAppTime != string.Empty)
            param.Value = D2DAppTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }


    public static bool tblCustomers_UpdateD2DEmpAppDate(string CustomerID, string D2DAppDate, string D2DAppTime, string D2DAppNote)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateD2DEmpAppDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppDate";
        if (D2DAppDate != string.Empty)
            param.Value = D2DAppDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppTime";
        if (D2DAppTime != string.Empty)
            param.Value = D2DAppTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppNote";
        if (D2DAppNote != string.Empty)
            param.Value = D2DAppNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblCustomers_UpdateReschedule(string CustomerID, string RescheduleDate, string RescheduleTime, string RescheduleNote)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateReschedule";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RescheduleDate";
        if (RescheduleDate != string.Empty)
            param.Value = RescheduleDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RescheduleTime";
        if (RescheduleTime != string.Empty)
            param.Value = RescheduleTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RescheduleNote";
        if (RescheduleNote != string.Empty)
            param.Value = RescheduleNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblCustomers_UpdateD2DEmpAppDateBYcustid(string CustomerID, string D2DAppDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateD2DEmpAppDateBYcustid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppDate";
        if (D2DAppDate != string.Empty)
            param.Value = D2DAppDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {

        }
        return (result != -1);
    }

    public static DataTable tblCustomers_CountLeadStatus(string CustEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_CountLeadStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustEnteredBy";
        if (CustEnteredBy != string.Empty)
            param.Value = CustEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustomers_UpdateEmployee(string CustomerID, String EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateEmployee";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblCustomers_SelectLTeam(String userid, string Customer, string StreetCity, string StreetPostCode, string D2DAppTime, string startdate, string enddate, string userid1, string DateType, string D2DEmployee, string EmployeeID, string CustLeadStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectLTeam";

        //HttpContext.Current.Response.Write(userid + "" + userid1);
        //HttpContext.Current.Response.End();

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppTime";
        if (D2DAppTime != string.Empty)
            param.Value = D2DAppTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DEmployee";
        if (D2DEmployee != string.Empty)
            param.Value = D2DEmployee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@CustLeadStatusID";
        if (CustLeadStatusID != string.Empty)
            param.Value = CustLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomers_GetLTeamCalendar(string date, String userid, string userid1)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_GetLTeamCalendar";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@date";
        if (date != string.Empty)
            param.Value = date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    //Appointment Status

    public static bool tblCustomers_Updateappointment_status(string appointment_status, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Updateappointment_status";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@appointment_status";
        param.Value = appointment_status;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblCustomers_UpdateAppFixBy(string CustomerID, string AppFixBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateAppFixBy";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AppFixBy";
        param.Value = AppFixBy;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblCustomers_Updatecustomer_lead_status(string customer_lead_status, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Updatecustomer_lead_status";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@customer_lead_status";
        param.Value = customer_lead_status;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomers_UpdateCancelNotes(string CustomerID, string CancelNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateCancelNotes";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CancelNotes";
        param.Value = CancelNotes;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblCustomers_UpdateD2DAppAttendedDate(string CustomerID, string D2DAppAttendedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateD2DAppAttendedDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppAttendedDate";
        param.Value = D2DAppAttendedDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblCustLeadStatus_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustLeadStatus_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustLeadStatus_SelectQuote()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustLeadStatus_SelectQuote";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustLeadStatus_ConfirmNo()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustLeadStatus_ConfirmNo";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustLeadStatus_SelectASc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustLeadStatus_SelectASc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployees_SelectTeamOutDoor(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_SelectTeamOutDoor";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblCustomers_UpdateAddress(string CustomerID, string StreetAddress, string StreetCity, string StreetState, string StreetPostCode, string Customer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateAddress";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomers_UpdateInContact(string CustomerID, string ResCom, string CustPhone, string Area)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateInContact";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        param.Value = CustPhone;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        if (Area != string.Empty)
            param.Value = Area;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblCustomers_UpdateEmployeeID(string CustomerID, String EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateEmployeeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblCustomers_UpdateAttend(string CustomerID, String AttendFlag, string AttendReason)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateAttend";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AttendFlag";
        param.Value = AttendFlag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AttendReason";
        if (AttendReason != string.Empty)
            param.Value = AttendReason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblCustomers_UpdateCustomer(string CustomerID, String Customer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateCustomer";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblCustomer_Update_Address(string CustomerID, string unit_type, string unit_number, string street_number, string street_name, string street_type, string street_suffix)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_Update_Address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_type";
        if (unit_type != string.Empty)
            param.Value = unit_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_number";
        if (unit_number != string.Empty)
            param.Value = unit_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_number";
        if (street_number != string.Empty)
            param.Value = street_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_name";
        if (street_name != string.Empty)
            param.Value = street_name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_type";
        if (street_type != string.Empty)
            param.Value = street_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_suffix";
        if (street_suffix != string.Empty)
            param.Value = street_suffix;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomer_Update_PostalAddress(string CustomerID, string punit_type, string punit_number, string pstreet_number, string pstreet_name, string pstreet_type, string pstreet_suffix)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_Update_PostalAddress";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@punit_type";
        if (punit_type != string.Empty)
            param.Value = punit_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@punit_number";
        if (punit_number != string.Empty)
            param.Value = punit_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pstreet_number";
        if (pstreet_number != string.Empty)
            param.Value = pstreet_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pstreet_name ";
        if (pstreet_name != string.Empty)
            param.Value = pstreet_name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pstreet_type";
        if (pstreet_type != string.Empty)
            param.Value = pstreet_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pstreet_suffix";
        if (pstreet_suffix != string.Empty)
            param.Value = pstreet_suffix;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomer_Update_Street_line(string CustomerID, string street_address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_Update_Street_line";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomer_Update_Postal_line(string CustomerID, string Postal_address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_Update_Postal_line";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Postal_address";
        if (Postal_address != string.Empty)
            param.Value = Postal_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblCustomer_Update_isformbayadd(string CustomerID, string isformbayadd)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomer_Update_isformbayadd";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@isformbayadd";
        param.Value = isformbayadd;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblCustomers_InsertContactsDetails(String CustomerID, String ContFirst, String ContLast, String ContMobile, String ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_InsertContactsDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        param.Value = ContFirst;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        param.Value = ContLast;
        param.DbType = DbType.String;
        param.Size = 40;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblCustomers_UpdateAddressDetails(string CustomerID, String StreetAddress, String StreetCity, String StreetState, String StreetPostCode, String PostalAddress, String PostalCity, String PostalState, String PostalPostCode, String CustPhone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_UpdateAddressDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalAddress";
        if (PostalAddress != string.Empty)
            param.Value = PostalAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalCity";
        if (PostalCity != string.Empty)
            param.Value = PostalCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalState";
        if (PostalState != string.Empty)
            param.Value = PostalState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostalPostCode";
        if (PostalPostCode != string.Empty)
            param.Value = PostalPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        if (CustPhone != string.Empty)
            param.Value = CustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);



        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static int tblCustomers_ExitsStreet_address(string street_address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExitsStreet_address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        param.Value = street_address;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblCustomers_ExitsByID_Street_address(string CustomerID, string street_address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExitsByID_Street_address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        param.Value = street_address;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblCustomers_ExitsByID_Streetaddress(string CustomerID, string street_address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExitsByID_Streetaddress";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        param.Value = street_address;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblCustomers_ExitsPostal_address(string Postal_address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExitsPostal_address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Postal_address";
        param.Value = Postal_address;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblCustomers_Exits_Address(string unit_type, string unit_number, string street_number, string street_name, string street_type, string StreetCity, string StreetState, string StreetPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_Address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@unit_type";
        if (unit_type != string.Empty)
            param.Value = unit_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_number";
        if (unit_number != string.Empty)
            param.Value = unit_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_number";
        if (street_number != string.Empty)
            param.Value = street_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_name";
        if (street_name != string.Empty)
            param.Value = street_name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_type";
        if (street_type != string.Empty)
            param.Value = street_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static DataTable tblCustomers_Exits_Select_Address(string street_address, string StreetCity, string StreetState, string StreetPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_Select_Address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomers_Exits_Select_StreetAddress(string street_address, string StreetCity, string StreetState, string StreetPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_Select_StreetAddress";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomers_Exits_Select_StreetAddressByID(string customerid, string street_address, string StreetCity, string StreetState, string StreetPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_Select_StreetAddressByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@customerid";
        if (customerid != string.Empty)
            param.Value = customerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_Exits_Select_MobileEmailByID(string customerid, string ContMobile, string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Exits_Select_MobileEmailByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@customerid";
        if (customerid != string.Empty)
            param.Value = customerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblContacts_Exits_Select_MobileEmailByID2(string customerid, string ContMobile, string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_Exits_Select_MobileEmailByID2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@customerid";
        if (customerid != string.Empty)
            param.Value = customerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        //DataTable result = new DataTable();
        //result = DataAccess.ExecuteSelectCommand(comm);

        try
        {
        }
        catch
        {
            // log errors if any
        }

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;

        //return result;
    }

    public static int tblCustomers_ExitsByID_Address(string CustomerID, string unit_type, string unit_number, string street_number, string street_name, string street_type, string StreetCity, string StreetState, string StreetPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExitsByID_Address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_type";
        if (unit_type != string.Empty)
            param.Value = unit_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_number";
        if (unit_number != string.Empty)
            param.Value = unit_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_number";
        if (street_number != string.Empty)
            param.Value = street_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_name";
        if (street_name != string.Empty)
            param.Value = street_name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_type";
        if (street_type != string.Empty)
            param.Value = street_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static int tblCustomers_ExitsByCustID_Address(string CustomerID, string unit_type, string unit_number, string street_number, string street_name, string street_type, string StreetCity, string StreetState, string StreetPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_ExitsByCustID_Address";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_type";
        if (unit_type != string.Empty)
            param.Value = unit_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_number";
        if (unit_number != string.Empty)
            param.Value = unit_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_number";
        if (street_number != string.Empty)
            param.Value = street_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_name";
        if (street_name != string.Empty)
            param.Value = street_name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_type";
        if (street_type != string.Empty)
            param.Value = street_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static DataTable tblCustomers_Exits_Select_StreetAddress_ByCustId(string CustomerID, string unit_type, string unit_number, string street_number, string street_name, string street_type, string StreetCity, string StreetState, string StreetPostCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_Select_StreetAddress_ByCustId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_type";
        if (unit_type != string.Empty)
            param.Value = unit_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@unit_number";
        if (unit_number != string.Empty)
            param.Value = unit_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_number";
        if (street_number != string.Empty)
            param.Value = street_number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_name";
        if (street_name != string.Empty)
            param.Value = street_name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@street_type";
        if (street_type != string.Empty)
            param.Value = street_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_Customers_searchdata(string data, string curuserid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Customers_searchdata";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@data";
        if (data != string.Empty)
            param.Value = data;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        if (curuserid != string.Empty)
            param.Value = curuserid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_Customers_searchdataAll(string data, string curuserid, string employeeid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Customers_searchdataAll";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@data";
        if (data != string.Empty)
            param.Value = data;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        if (curuserid != string.Empty)
            param.Value = curuserid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    //public static DataTable tblCustomersGetDataBySearch(string alpha, string employeeid, string CustTypeID, string CustSourceID, string ResCom, string Area, string StreetState, string startdate, string enddate, string StreetPostCode, string curemployeeid, string CompanyNumber, string StreetCity, string StreetAddress, string CustSourceSubID, string startindex, string pageindex)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tblCustomersGetDataBySearch";

    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@alpha";
    //    param.Value = alpha;
    //    param.DbType = DbType.String;
    //    param.Size = 100;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@userid";
    //    if (employeeid != string.Empty)
    //        param.Value = employeeid;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.String;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@curuserid";
    //    if (curemployeeid != string.Empty)
    //        param.Value = curemployeeid;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.String;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@CustTypeID";
    //    if (CustTypeID != string.Empty)
    //        param.Value = CustTypeID;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@CustSourceID";
    //    if (CustSourceID != string.Empty)
    //        param.Value = CustSourceID;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@ResCom";
    //    if (ResCom != string.Empty)
    //        param.Value = ResCom;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@Area";
    //    if (Area != string.Empty)
    //        param.Value = Area;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@StreetState";
    //    param.Value = StreetState;
    //    param.DbType = DbType.String;
    //    param.Size = 100;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@startdate";
    //    if (startdate != string.Empty)
    //        param.Value = startdate;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.DateTime;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@enddate";
    //    if (enddate != string.Empty)
    //        param.Value = enddate;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.DateTime;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@StreetPostCode";
    //    param.Value = StreetPostCode;
    //    param.DbType = DbType.String;
    //    param.Size = 100;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@StreetCity";
    //    param.Value = StreetCity;
    //    param.DbType = DbType.String;
    //    param.Size = 100;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@CompanyNumber";
    //    if (CompanyNumber != string.Empty)
    //        param.Value = CompanyNumber;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int64;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@StreetAddress";
    //    if (StreetAddress != string.Empty)
    //        param.Value = StreetAddress;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.String;
    //    param.Size = 100;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@CustSourceSubID";
    //    if (CustSourceSubID != string.Empty)
    //        param.Value = CustSourceSubID;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@startindex";
    //    if (startindex != string.Empty)
    //        param.Value = startindex;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@pageindex";
    //    if (pageindex != "All")
    //        param.Value = pageindex;
    //    else
    //        param.Value = "0";
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    return DataAccess.ExecuteSelectCommand(comm);
    //}

    public static DataTable query_execute(String sqlstmt)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "query_execute";

        //HttpContext.Current.Response.Write("sqlstmt" + sqlstmt);
        //HttpContext.Current.Response.End();

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@sqlstmt";
        param.Value = sqlstmt;
        param.DbType = DbType.String;
        param.Size = 2147483647;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int aspnet_Users_ExistsByRole(string Userid)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure CustType
        comm.CommandText = "aspnet_Users_ExistsByRole";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Userid";
        param.Value = Userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    //customer Image

    public static int tbl_CustImage_Insert(String CustomerID, String Imagename)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_CustImage_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Imagename";
        param.Value = Imagename;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tbl_CustImage_SelectByCustomerID(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_CustImage_SelectByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_CustImage_UpdateImage(string CustImageID, String Imagename)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_CustImage_UpdateImage";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustImageID";
        param.Value = CustImageID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Imagename";
        if (Imagename != string.Empty)
            param.Value = Imagename;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tbl_CustImage_DeleteByCustImageID(string CustImageID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_CustImage_DeleteByCustImageID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustImageID";
        param.Value = CustImageID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }


    public static DataTable tbl_CustImage_SelectByCustImageID(string CustImageID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_CustImage_SelectByCustImageID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustImageID";
        param.Value = CustImageID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblCustomers_Exits_Address_new(string streetaddress, string streetcity, string streetstate, string streetpostcode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_Address_new";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (streetaddress != string.Empty)
            param.Value = streetaddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (streetaddress != string.Empty)
            param.Value = streetcity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (streetaddress != string.Empty)
            param.Value = streetstate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (streetaddress != string.Empty)
            param.Value = streetpostcode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static DataTable tblCustomers_tblProjects_Exits_Address_new(string customerid, string streetaddress, string streetcity, string streetstate, string streetpostcode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_tblProjects_Exits_Address_new";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (streetaddress != string.Empty)
            param.Value = streetaddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@customerid";
        if (streetaddress != string.Empty)
            param.Value = customerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (streetcity != string.Empty)
            param.Value = streetcity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (streetstate != string.Empty)
            param.Value = streetstate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (streetpostcode != string.Empty)
            param.Value = streetpostcode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        DataTable dt = (DataAccess.ExecuteSelectCommand(comm));
        return dt;
    }

    public static DataTable tblCustomers_SelectLTeam_admin(String userid, string Customer, string StreetCity, string StreetPostCode, string D2DAppTime, string startdate, string enddate, string userid1, string DateType, string D2DEmployee, string EmployeeID, string CustLeadStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectLTeam_admin";

        //HttpContext.Current.Response.Write(userid + "" + userid1);
        //HttpContext.Current.Response.End();

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppTime";
        if (D2DAppTime != string.Empty)
            param.Value = D2DAppTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DEmployee";
        if (D2DEmployee != string.Empty)
            param.Value = D2DEmployee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@CustLeadStatusID";
        if (CustLeadStatusID != string.Empty)
            param.Value = CustLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblWebDownload_SelectByCheckExists(String Customer, String CustPhone, String ContEmail, string ContMobile, string Address)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_SelectByCheckExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        if (CustPhone != string.Empty)
            param.Value = CustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Address";
        if (Address != string.Empty)
            param.Value = Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomers_SelectLTeam_Eadmin(String userid, string Customer, string StreetCity, string StreetPostCode, string D2DAppTime, string startdate, string enddate, string userid1, string DateType, string D2DEmployee, string EmployeeID, string CustLeadStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectLTeam_Eadmin";

        //HttpContext.Current.Response.Write(userid + "" + userid1);
        //HttpContext.Current.Response.End();

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DAppTime";
        if (D2DAppTime != string.Empty)
            param.Value = D2DAppTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@D2DEmployee";
        if (D2DEmployee != string.Empty)
            param.Value = D2DEmployee;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@CustLeadStatusID";
        if (CustLeadStatusID != string.Empty)
            param.Value = CustLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable tblCustomers_Exits_SelectCustomer(string street_address, string StreetCity, string StreetState, string StreetPostCode, string ContMobile, string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_SelectCustomer";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_Exits_SelectCustomer2(string street_address, string StreetCity, string StreetState, string StreetPostCode, string ContMobile, string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_SelectCustomer2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_Exits_SelectCustomerbyID(string customerid, string street_address, string StreetCity, string StreetState, string StreetPostCode, string ContMobile, string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_SelectCustomerbyID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@customerid";
        if (street_address != string.Empty)
            param.Value = customerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_Exits_SelectCustomerbyID2(string customerid, string street_address, string StreetCity, string StreetState, string StreetPostCode, string ContMobile, string ContEmail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_Exits_SelectCustomerbyID2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_address";
        if (street_address != string.Empty)
            param.Value = street_address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@customerid";
        if (street_address != string.Empty)
            param.Value = customerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        param.Value = ContEmail;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_instinfosearch(String custid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_instinfosearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = custid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomers_SelectByUserIdSearchData_admin(String userid, string CustTypeID, string CustSourceID, string ContLeadStatusID, string CustSourceSubID, string StreetState, string SalesTeamID, string StreetPostCode, string StreetCity, string Contact, string startdate, string enddate, string EmployeeID, string userid1, string StreetAddress, string DateType, string ProjectStatusID, string Reject, string ProjectNumer, string Mobileno, string Email, string EmpCatID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByUserIdSearchData_admin";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        if (ContLeadStatusID != string.Empty)
            param.Value = ContLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StreetCity";
        //param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 50;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reject";
        if (Reject != string.Empty)
            param.Value = Reject;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumer != string.Empty)
            param.Value = ProjectNumer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@email";
        if (Email != string.Empty)
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@mobile";
        if (Mobileno != string.Empty)
            param.Value = Mobileno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@empcatid";
        if (EmpCatID != string.Empty)
            param.Value = EmpCatID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustomers_SelectByUserIdSearchData(String userid, string CustTypeID, string CustSourceID, string ContLeadStatusID, string CustSourceSubID, string StreetState, string SalesTeamID, string StreetPostCode, string StreetCity, string Contact, string startdate, string enddate, string EmployeeID, string userid1, string StreetAddress, string DateType, string ProjectStatusID, string Reject, string ProjectNumer, string Mobileno, string Email)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByUserIdSearchData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        if (ContLeadStatusID != string.Empty)
            param.Value = ContLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@StreetCity";
        //param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 50;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid1";
        if (userid1 != string.Empty)
            param.Value = userid1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reject";
        if (Reject != string.Empty)
            param.Value = Reject;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumer != string.Empty)
            param.Value = ProjectNumer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@email";
        if (Email != string.Empty)
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@mobile";
        if (Mobileno != string.Empty)
            param.Value = Mobileno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_formbayunittypeID_SelectByUnittype(String unit_type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbayunittypeID_SelectByUnittype";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@unit_type";
        param.Value = unit_type;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_formbayStreettypeID_SelectByStreetCode(String street_type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbayStreettypeID_SelectByStreetCode";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@street_type";
        param.Value = street_type;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable getLeadData(string ddlup, string webdup, string Customer, string source, string subsource, string state, string team, string startdate, string enddate, string delete, string upload, string datetype)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "getLeadDat";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ddlDup";
        if (ddlup != string.Empty)
            param.Value = ddlup;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@ddlWebDup";
        //if (webdup != string.Empty)
        //    param.Value = webdup;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cusutomer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@source";
        if (source != string.Empty)
            param.Value = source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@subsource";
        if (subsource != string.Empty)
            param.Value = subsource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@state";
        if (state != string.Empty)
            param.Value = state;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@team";
        if (team != string.Empty)
            param.Value = team;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@delete";
        if (delete != string.Empty)
            param.Value = delete;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upload";
        if (upload != string.Empty)
            param.Value = upload;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        try
        {
        }
        catch (Exception exs)
        {
            // log errors if any
        }
        return result;

    }
    public static DataTable getLeadDataCount(string ddlup, string webdup, string Customer, string source, string subsource, string state, string team, string startdate, string enddate, string delete, string upload, string datetype)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "getLeadDataCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ddlDup";
        if (ddlup != string.Empty)
            param.Value = ddlup;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ddlWebDup";
        if (webdup != string.Empty)
            param.Value = webdup;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cusutomer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@source";
        if (source != string.Empty)
            param.Value = source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@subsource";
        if (subsource != string.Empty)
            param.Value = subsource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@state";
        if (state != string.Empty)
            param.Value = state;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@team";
        if (team != string.Empty)
            param.Value = team;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@delete";
        if (delete != string.Empty)
            param.Value = delete;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upload";
        if (upload != string.Empty)
            param.Value = upload;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);

        try
        {
        }
        catch (Exception exs)
        {
            // log errors if any
        }
        return result;

    }
    public static DataTable GetDuplicateData(String ContEmail, string ContMobile)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetDuplicateData";

        DbParameter
        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contmobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static SttblCustomers tblCustomers_getAdress_SelectByCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_SelectByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustomers details = new SttblCustomers();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details

            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.StreetCity = dr["StreetCity"].ToString();
            details.StreetState = dr["StreetState"].ToString();
            details.StreetPostCode = dr["StreetPostCode"].ToString();
        }

        return details;
    }

    public static bool tblCustomers_userip(string CustomerID, string userip)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomers_userip";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userip";
        if (userip != string.Empty)
            param.Value = userip;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}