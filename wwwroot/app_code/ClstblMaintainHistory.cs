﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;

/// <summary>
/// Summary description for ClstblMaintainHistory
/// </summary>
public class ClstblMaintainHistory
{
    public static int tblMaintainHistory_InsertWithOrderID(String Userid, String SectionId, String SerialNo, String Section,string Message, DateTime CreatedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_InsertWithOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Userid";
        if (Userid != string.Empty)
            param.Value = Userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SectionId";
        if (SectionId != string.Empty)
            param.Value = SectionId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerailNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Section";
        if (Section != string.Empty)
            param.Value = Section;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Message";
        if (Message != string.Empty)
            param.Value = Message;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedDate";
        param.Value = CreatedDate;    
        param.DbType = DbType.DateTime;        
        comm.Parameters.Add(param);
      

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static DataTable tblMaintainHistory_GetData_BySerialNo(String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_GetData_BySerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblMaintainHistory_Delete_BySerailNo(string SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainHistory_Delete_BySerailNo";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerailNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);      
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable Select_tblMaintainHistory_CountItemWise(string PickId,string Itemid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Select_tblMaintainHistory_CountItemWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;        
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Itemid";
        param.Value = Itemid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}