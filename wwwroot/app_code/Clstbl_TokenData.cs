using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct Sttbl_TokenData
{
    public string Id;
    public string Token;
    public string TokenDate;
    public string ProjectID;
    public string ProjectNumber;
    public string IsExpired;
    public string IsOwner;
    public string EmpID;
    public string QDocNo;
    public string SignFlag;

}


public class Clstbl_TokenData
{

    public static Sttbl_TokenData tbl_TokenData_SelectById(string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenData_SelectById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_TokenData details = new Sttbl_TokenData();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Token = dr["Token"].ToString();
            details.TokenDate = dr["TokenDate"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.IsExpired = dr["IsExpired"].ToString();
            details.IsOwner = dr["IsOwner"].ToString();
            details.EmpID = dr["EmpID"].ToString();
            details.QDocNo = dr["QDocNo"].ToString();
            details.SignFlag = dr["SignFlag"].ToString();

        }
        // return structure details
        return details;
    }

    public static Sttbl_TokenData tbl_TokenData_SelectByToken_ProjectID(string Token,String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenData_SelectByToken_ProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_TokenData details = new Sttbl_TokenData();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Id = dr["Id"].ToString();
            details.TokenDate = dr["TokenDate"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.IsExpired = dr["IsExpired"].ToString();
            details.IsOwner = dr["IsOwner"].ToString();
            details.EmpID = dr["EmpID"].ToString();
            details.QDocNo = dr["QDocNo"].ToString();
            details.SignFlag = dr["SignFlag"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tblProjects_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_TokenData_Insert(String Token, String TokenDate, String ProjectID, String ProjectNumber, String IsExpired, String IsOwner,String EmpID,String QDocNo,String SignFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenData_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        if (Token != string.Empty)
            param.Value = Token;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TokenDate";
        if (TokenDate != string.Empty)
            param.Value = TokenDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsExpired";
        if (IsExpired != string.Empty)
            param.Value = IsExpired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsOwner";
        if (IsOwner != string.Empty)
            param.Value = IsOwner;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpID";
        if (EmpID != string.Empty)
            param.Value = EmpID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QDocNo";
        if (QDocNo != string.Empty)
            param.Value = QDocNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignFlag";
        if (SignFlag != string.Empty)
            param.Value = SignFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_TokenData_Update_IsExpired(string Token, String ProjectID,string IsExpired)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenData_Update_IsExpired";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsExpired";
        param.Value = IsExpired;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_TokenData_Update_SignFlag(string Token, String ProjectID, string SignFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenData_Update_SignFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignFlag";
        param.Value = SignFlag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Delete(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tbl_TokenDataExistByToken_ProjectID(string Token,string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "tbl_TokenDataExistByToken_ProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;

    }

    public static int tbl_TokenDataAcno_Insert(String Token, String TokenDate, String ProjectID, String ProjectNumber, String IsExpired, String IsOwner, String EmpID, String DocNo, String SignFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenDataAcno_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        if (Token != string.Empty)
            param.Value = Token;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TokenDate";
        if (TokenDate != string.Empty)
            param.Value = TokenDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsExpired";
        if (IsExpired != string.Empty)
            param.Value = IsExpired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsOwner";
        if (IsOwner != string.Empty)
            param.Value = IsOwner;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpID";
        if (EmpID != string.Empty)
            param.Value = EmpID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocNo";
        if (DocNo != string.Empty)
            param.Value = DocNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignFlag";
        if (SignFlag != string.Empty)
            param.Value = SignFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_TokenDataAcnoExistByToken_ProjectID(string Token, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenDataAcnoExistByToken_ProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;

    }

    public static Sttbl_TokenData tbl_TokenDataAcno_SelectByToken_ProjectID(string Token, String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenDataAcno_SelectByToken_ProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_TokenData details = new Sttbl_TokenData();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Id = dr["Id"].ToString();
            details.TokenDate = dr["TokenDate"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.IsExpired = dr["IsExpired"].ToString();
            details.IsOwner = dr["IsOwner"].ToString();
            details.EmpID = dr["EmpID"].ToString();
            details.QDocNo = dr["DocNo"].ToString();
            details.SignFlag = dr["SignFlag"].ToString();

        }
        // return structure details
        return details;
    }

    public static bool tbl_TokenDataAcno_Update_IsExpired(string Token, String ProjectID, string IsExpired)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenDataAcno_Update_IsExpired";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsExpired";
        param.Value = IsExpired;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_TokenDataAcno_Update_SignFlag(string Token, String ProjectID, string SignFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TokenDataAcno_Update_SignFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Token";
        param.Value = Token;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignFlag";
        param.Value = SignFlag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
}