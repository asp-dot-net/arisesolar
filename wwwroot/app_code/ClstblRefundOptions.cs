using System;
using System.Data;
using System.Data.Common;


public struct SttblRefundOptions
{
    public string OptionName;
    public string Active;
}


public class ClstblRefundOptions
{
    public static SttblRefundOptions tblRefundOptions_SelectByOptionID(String OptionID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_SelectByOptionID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        param.Value = OptionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblRefundOptions details = new SttblRefundOptions();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.OptionName = dr["OptionName"].ToString();
            details.Active = dr["Active"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblRefundOptions_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblRefundOptions_SelectActice()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_SelectActice";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblRefundOptions_Insert(String OptionName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OptionName";
        param.Value = OptionName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblRefundOptions_Update(string OptionID, String OptionName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        param.Value = OptionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionName";
        param.Value = OptionName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblRefundOptions_InsertUpdate(Int32 OptionID, String OptionName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        param.Value = OptionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionName";
        param.Value = OptionName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblRefundOptions_Delete(string OptionID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        param.Value = OptionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblRefundOptionsGetDataBySearch(string alpha,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptionsGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static int tblRefundOptions_Exists(string OptionName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OptionName";
        param.Value = OptionName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblRefundOptions_ExistsById(string OptionName, string OptionID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRefundOptions_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@OptionName";
        param.Value = OptionName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OptionID";
        param.Value = OptionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
}