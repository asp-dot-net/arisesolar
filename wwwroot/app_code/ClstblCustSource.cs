using System;
using System.Data;
using System.Data.Common;

public struct SttblCustSource
{
    public string CustSourceID;
    public string CustSource;
    public string Active;
    public string Seq;
    public string LeadSelect;
}

public class ClstblCustSource
{


    public static DataTable tbl_CustSourcelead_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_CustSourcelead_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static SttblCustSource tblCustSource_SelectByCustSourceID(String CustSourceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_SelectByCustSourceID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSource details = new SttblCustSource();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustSource = dr["CustSource"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();
            details.LeadSelect = dr["LeadSelect"].ToString();

        }
        // return structure details
        return details;
    }

    public static SttblCustSource tblCustSource_SelectByCustSourceIDCustSource(String CustSource)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_SelectByCustSourceIDCustSource";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSource";
        param.Value = CustSource;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSource details = new SttblCustSource();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustSourceID = dr["CustSourceID"].ToString();
            details.CustSource = dr["CustSource"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();
            details.LeadSelect = dr["LeadSelect"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblCustSource_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustSource_SelectByNewLead()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_SelectByNewLead";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustSource_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblCustSource_Insert(String CustSource, String Active, String Seq, String LeadSelect)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSource";
        param.Value = CustSource;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadSelect";
        if (LeadSelect != string.Empty)
            param.Value = LeadSelect;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblCustSource_Update(string CustSourceID, String CustSource, String Active, String Seq, String LeadSelect)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSource";
        param.Value = CustSource;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadSelect";
        param.Value = LeadSelect;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblCustSource_InsertUpdate(Int32 CustSourceID, String CustSource, String Active, String Seq, String LeadSelect)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSource";
        param.Value = CustSource;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LeadSelect";
        param.Value = LeadSelect;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustSource_Delete(string CustSourceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblCustSource_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblCustSource_GetDataBySearch(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static int tblCustSource_Exists(string CustSource)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSource";
        param.Value = CustSource;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblCustSource_ExistsById(string CustSource, string CustSourceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSource";
        param.Value = CustSource;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblCustSourceGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblCustSourceGetDataByAlpha";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblCustSourceGetDataBySearch(string alpha ,string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustSource_SelectbyNew(string source)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_SelectbyNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@source";
        if (source != string.Empty)
            param.Value = source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}