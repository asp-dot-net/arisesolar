using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct SttblInvoicePayments
{
    public string InvoicePaymentID;
    public string ProjectID;
    public string InvoicePayExGST;
    public string InvoicePayGST;
    public string InvoicePayTotal;
    public string InvoicePayDate;
    public string InvoicePayMethodID;
    public string CCSurcharge;
    public string EmployeeID;
    public string Verified;
    public string VerifiedBy;
    public string PaymentNote;
    public string Refund;
    public string RefundType;
    public string RefundDone;
    public string RefundDoneBY;
    public string PaymentNumber;
    public string IsVerified;
    public string UpdatedBy;
    public string ReceiptNumber;
    public string TransactionCode;
}


public class ClstblInvoicePayments
{
    public static SttblInvoicePayments tblInvoicePayments_SelectByInvoicePaymentID(String InvoicePaymentID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_SelectByInvoicePaymentID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblInvoicePayments details = new SttblInvoicePayments();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProjectID = dr["ProjectID"].ToString();
            details.InvoicePayExGST = dr["InvoicePayExGST"].ToString();
            details.InvoicePayGST = dr["InvoicePayGST"].ToString();
            details.InvoicePayTotal = dr["InvoicePayTotal"].ToString();
            details.InvoicePayDate = dr["InvoicePayDate"].ToString();
            details.InvoicePayMethodID = dr["InvoicePayMethodID"].ToString();
            details.CCSurcharge = dr["CCSurcharge"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.Verified = dr["Verified"].ToString();
            details.VerifiedBy = dr["VerifiedBy"].ToString();
            details.PaymentNote = dr["PaymentNote"].ToString();
            details.Refund = dr["Refund"].ToString();
            details.RefundType = dr["RefundType"].ToString();
            details.RefundDone = dr["RefundDone"].ToString();
            details.RefundDoneBY = dr["RefundDoneBY"].ToString();
            details.PaymentNumber = dr["PaymentNumber"].ToString();
            details.IsVerified = dr["IsVerified"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.ReceiptNumber = dr["ReceiptNumber"].ToString();
            details.TransactionCode = dr["TransactionCode"].ToString();

        }
        // return structure details
        return details;
    }
    public static SttblInvoicePayments tblInvoicePayments_StructSelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_StructSelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblInvoicePayments details = new SttblInvoicePayments();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.InvoicePaymentID = dr["InvoicePaymentID"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.InvoicePayExGST = dr["InvoicePayExGST"].ToString();
            details.InvoicePayGST = dr["InvoicePayGST"].ToString();
            details.InvoicePayTotal = dr["InvoicePayTotal"].ToString();
            details.InvoicePayDate = dr["InvoicePayDate"].ToString();
            details.InvoicePayMethodID = dr["InvoicePayMethodID"].ToString();
            details.CCSurcharge = dr["CCSurcharge"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.Verified = dr["Verified"].ToString();
            details.VerifiedBy = dr["VerifiedBy"].ToString();
            details.PaymentNote = dr["PaymentNote"].ToString();
            details.Refund = dr["Refund"].ToString();
            details.RefundType = dr["RefundType"].ToString();
            details.RefundDone = dr["RefundDone"].ToString();
            details.RefundDoneBY = dr["RefundDoneBY"].ToString();
            details.PaymentNumber = dr["PaymentNumber"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblInvoicePayments_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblInvoicePayments_SelectByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblInvoicePayments_SelectTop1ByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_SelectTop1ByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblInvoicePayments_GetDataBySearch(string InvoiceNumber, string Project, string InvoicePayMethodID, string InstallState, string startdate, string enddate, string VerifiedBy, string Customer, string IsVerified, string datetype, string FinanceWithID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Project != string.Empty)
            param.Value = Project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        if (InvoicePayMethodID != string.Empty)
            param.Value = InvoicePayMethodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifiedBy";
        if (VerifiedBy != string.Empty)
            param.Value = VerifiedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsVerified";
        if (IsVerified != string.Empty)
            param.Value = IsVerified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblInvoicePayments_GetDataBySearchCount(string InvoiceNumber, string Project, string InvoicePayMethodID, string InstallState, string startdate, string enddate, string VerifiedBy, string Customer, string IsVerified, string datetype,string FinanceWith)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_GetDataBySearchCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Project != string.Empty)
            param.Value = Project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        if (InvoicePayMethodID != string.Empty)
            param.Value = InvoicePayMethodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifiedBy";
        if (VerifiedBy != string.Empty)
            param.Value = VerifiedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsVerified";
        if (IsVerified != string.Empty)
            param.Value = IsVerified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWith";
        if (FinanceWith != string.Empty)
            param.Value = FinanceWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        try
        {
           result =  DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
      
    }

    public static DataTable tblInvoicePayMethod_GetdataByActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayMethod_GetdataByActive";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblInvoicePayments_Insert(String ProjectID, String InvoicePayExGST, String InvoicePayGST, String InvoicePayTotal, String InvoicePayDate, String InvoicePayMethodID, String CCSurcharge, String EmployeeID, String Verified, String VerifiedBy, String PaymentNote, String Refund, String RefundType, String RefundDone, String RefundDoneBY, String PaymentNumber, string ReceiptNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayExGST";
        if (InvoicePayExGST != string.Empty)
            param.Value = InvoicePayExGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayGST";
        if (InvoicePayGST != string.Empty)
            param.Value = InvoicePayGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayTotal";
        if (InvoicePayTotal != string.Empty)
            param.Value = InvoicePayTotal;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayDate";
        if (InvoicePayDate != string.Empty)
            param.Value = InvoicePayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        if (InvoicePayMethodID != string.Empty)
            param.Value = InvoicePayMethodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CCSurcharge";
        if (CCSurcharge != string.Empty)
            param.Value = CCSurcharge;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Verified";
        if (Verified != string.Empty)
            param.Value = Verified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifiedBy";
        if (VerifiedBy != string.Empty)
            param.Value = VerifiedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentNote";
        if (PaymentNote != string.Empty)
            param.Value = PaymentNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Refund";
        if (Refund != string.Empty)
            param.Value = Refund;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundType";
        if (RefundType != string.Empty)
            param.Value = RefundType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundDone";
        if (RefundDone != string.Empty)
            param.Value = RefundDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundDoneBY";
        if (RefundDoneBY != string.Empty)
            param.Value = RefundDoneBY;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentNumber";
        if (PaymentNumber != string.Empty)
            param.Value = PaymentNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceiptNumber";
        if (ReceiptNumber != string.Empty)
            param.Value = ReceiptNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblInvoicePayments_Update(string InvoicePaymentID, String InvoicePayExGST, String InvoicePayGST, String InvoicePayTotal, String InvoicePayDate, String InvoicePayMethodID, String CCSurcharge, String EmployeeID, string UpdatedBy, string ReceiptNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayExGST";
        if (InvoicePayExGST != string.Empty)
            param.Value = InvoicePayExGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayGST";
        if (InvoicePayGST != string.Empty)
            param.Value = InvoicePayGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayTotal";
        if (InvoicePayTotal != string.Empty)
            param.Value = InvoicePayTotal;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayDate";
        if (InvoicePayDate != string.Empty)
            param.Value = InvoicePayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        if (InvoicePayMethodID != string.Empty)
            param.Value = InvoicePayMethodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CCSurcharge";
        if (CCSurcharge != string.Empty)
            param.Value = CCSurcharge;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceiptNumber";
        if (ReceiptNumber != string.Empty)
            param.Value = ReceiptNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblInvoicePayments_InsertUpdate(Int32 InvoicePaymentID, String ProjectID, String InvoicePayExGST, String InvoicePayGST, String InvoicePayTotal, String InvoicePayDate, String InvoicePayMethodID, String CCSurcharge, String EmployeeID, String Verified, String VerifiedBy, String PaymentNote, String Refund, String RefundType, String RefundDone, String RefundDoneBY, String PaymentNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayExGST";
        param.Value = InvoicePayExGST;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayGST";
        param.Value = InvoicePayGST;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayTotal";
        param.Value = InvoicePayTotal;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayDate";
        param.Value = InvoicePayDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        param.Value = InvoicePayMethodID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CCSurcharge";
        param.Value = CCSurcharge;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Verified";
        param.Value = Verified;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifiedBy";
        param.Value = VerifiedBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentNote";
        param.Value = PaymentNote;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Refund";
        param.Value = Refund;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundType";
        param.Value = RefundType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundDone";
        param.Value = RefundDone;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundDoneBY";
        param.Value = RefundDoneBY;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentNumber";
        param.Value = PaymentNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        string result = "";
        result = DataAccess.ExecuteScalar(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblInvoicePayments_Delete(string InvoicePaymentID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;

        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblInvoicePayments_DeleteProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_DeleteProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateInvoice(string ProjectID, String BalanceGST, String InvoiceNotes, String InvoicePaid)
    {
        //HttpContext.Current.Response.Write(InvoiceNotes);
        //HttpContext.Current.Response.End();

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvoice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceGST";
        if (BalanceGST != string.Empty)
            param.Value = BalanceGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNotes";
        param.Value = InvoiceNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaid";
        if (InvoicePaid != string.Empty)
            param.Value = InvoicePaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblInvoicePayments_CountTotal(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_CountTotal";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblInvoicePayments_UpdateVerified(string InvoicePaymentID, String VerifiedBy, string PaymentNote, string IsVerified)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_UpdateVerified";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VerifiedBy";
        if (VerifiedBy != string.Empty)
            param.Value = VerifiedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentNote";
        param.Value = PaymentNote;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsVerified";
        param.Value = IsVerified;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateInvoiceStatusID(string ProjectID, string InvoiceStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvoiceStatusID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceStatusID";
        param.Value = InvoiceStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateInvoiceFU(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvoiceFU";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateRefund(string ProjectID, String ProjectStatusID, string ProjectCancelID, string InvRefund, string InvRefundBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateRefund";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefund";
        if (InvRefund != string.Empty)
            param.Value = InvRefund;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefundBy";
        if (InvRefundBy != string.Empty)
            param.Value = InvRefundBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblInvoicePayments_UpdateRefund(string InvoicePaymentID, String Refund, string RefundType, string RefundDone, string RefundDoneBY)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_UpdateRefund";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Refund";
        param.Value = Refund;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundType";
        if (RefundType != string.Empty)
            param.Value = RefundType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundDone";
        if (RefundDone != string.Empty)
            param.Value = RefundDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundDoneBY";
        if (RefundDoneBY != string.Empty)
            param.Value = RefundDoneBY;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblInvoicePayments_SelectTop1(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_SelectTop1";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblInvoicePayments_UpdateVerifiedDate(string InvoicePaymentID, String Verified)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_UpdateVerifiedDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Verified";
        if (Verified != string.Empty)
            param.Value = Verified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblInvoicePayments_UpdateVerifiednull(string InvoicePaymentID, String Verified)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_UpdateVerifiednull";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Verified";
        if (Verified != string.Empty)
            param.Value = Verified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblInvoicePayments_UpdatePaidDate(string InvoicePaymentID, string PaidDate, string comment)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_UpdatePaidDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaidDate";
        if (PaidDate != string.Empty)
            param.Value = PaidDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@comment";
        if (comment != string.Empty)
            param.Value = comment;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblInvoicePayments_UpdateActualPayDate(string InvoicePaymentID, string ActualPayDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_UpdateActualPayDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        param.Value = InvoicePaymentID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActualPayDate";
        if (ActualPayDate != string.Empty)
            param.Value = ActualPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static int tbl_payway_orderno_Genrate()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_payway_orderno_Genrate";



        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tbl_payway_Insert(String orderno, string projectno, String employeeid, string summaryCode, String responseCode, String description, String receiptNo, String settlementDate, string creditGroup, string previousTxn, string cardSchemeName, String projectstatus, string orderamount)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_payway_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@orderno";
        if (orderno != string.Empty)
            param.Value = orderno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectno";
        if (projectno != string.Empty)
            param.Value = projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@summaryCode";
        if (summaryCode != string.Empty)
            param.Value = summaryCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@responseCode";
        param.Value = responseCode;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@description";
        param.Value = description;
        param.DbType = DbType.String;
        param.Size = 100000000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@receiptNo";
        param.Value = receiptNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@settlementDate";
        if (settlementDate != string.Empty)
            param.Value = settlementDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@creditGroup";
        param.Value = creditGroup;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@previousTxn";
        if (previousTxn != string.Empty)
            param.Value = previousTxn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@cardSchemeName";
        param.Value = cardSchemeName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectstatus";
        param.Value = projectstatus;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@orderamount";
        if (orderamount != string.Empty)
            param.Value = orderamount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);



        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblInvoicePayments_Update_TransactionCode(string InvoicePaymentID, string TransactionCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_Update_TransactionCode";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        if (InvoicePaymentID != string.Empty)
            param.Value = InvoicePaymentID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TransactionCode";
        if (TransactionCode != string.Empty)
            param.Value = TransactionCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblInvoicePayments_Update_PaymentNote(string InvoicePaymentID, string paymentNote)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_Update_PaymentNote";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaymentID";
        if (InvoicePaymentID != string.Empty)
            param.Value = InvoicePaymentID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@paymentNote";
        if (paymentNote != string.Empty)
            param.Value = paymentNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblInvoicePayments_tbl_bank_SelectByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_tbl_bank_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblInvoicePayments_tbl_BankInvoicePayments_Delete(string id, string deleteFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_tbl_BankInvoicePayments_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@deleteFlag";
        param.Value = deleteFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateInvoiceNotes(string ProjectID, string InvoiceNotes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvoiceNotes";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNotes";
        if (InvoiceNotes != string.Empty)
            param.Value = InvoiceNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
}