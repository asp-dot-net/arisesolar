﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsSTCTracker
/// </summary>
public class ClsSTCTracker
{
    public ClsSTCTracker()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable tblProjects_GetProjectNumberByGBBSFlag(string GBBSFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetProjectNumberByGBBSFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@GBBSFlag";
        if (GBBSFlag != string.Empty)
            param.Value = GBBSFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_Update_STC_FromGBBS(string ProjectNumber, string STCNumberNew, string PVDNumber, string STCApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_STC_FromGBBS";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (!string.IsNullOrEmpty(ProjectNumber))
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumberNew";
        if (!string.IsNullOrEmpty(STCNumberNew))
            param.Value = STCNumberNew;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (!string.IsNullOrEmpty(PVDNumber))
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (!string.IsNullOrEmpty(STCApplied))
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_STC_PVDStatutsID(string ProjectNumber, string PVDStatutsID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_STC_PVDStatutsID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatutsID";
        if (!string.IsNullOrEmpty(PVDStatutsID))
            param.Value = PVDStatutsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool Update_STCDetails()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_STCDetails";

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
}