﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AriseUpdateService" in code, svc and config file together.
public class AriseUpdateService : IAriseUpdateService
{

    public void DoWork()
    {
    }
    public class ProjectDetails
    {
        public string Message;
        public string CustomerID;
        public string ContactID;
        public string EmployeeID;
        public string SalesRep;
        public string ProjectTypeID;
        public string ProjectStatusID;
        public string ProjectCancelID;
        public string ProjectOnHoldID;
        public string ProjectOpened;
        public string ProjectCancelled;
        public string ProjectNumber;
        public string OldProjectNumber;
        public string ManualQuoteNumber;
        public string Project;
        public string Projects;
        public string NextMaintenanceCall;
        public string InstallAddress;
        public string InstallCity;
        public string InstallState;
        public string InstallPostCode;
        public string ProjectNotes;
        public string AdditionalSystem;
        public string GridConnected;
        public string RebateApproved;
        public string ReceivedCredits;
        public string CreditEligible;
        public string MoreThanOneInstall;
        public string RequiredCompliancePaperwork;
        public string OutOfPocketDocs;
        public string OwnerGSTRegistered;
        public string OwnerABN;
        public string HouseTypeID;
        public string RoofTypeID;
        public string RoofAngleID;
        public string InstallBase;
        public string StockAllocationStore;
        public string StandardPack;
        public string PanelBrandID;
        public string PanelBrand;
        public string PanelModel;
        public string PanelOutput;
        public string Panelp1;
        public string Inverterp1ID;
        public string SecondInverterp1ID;
        public string ThirdInverterp1ID;
        public string InverterBrand;
        public string InverterModel;
        public string InverterSeries;
        public string InverterOutput;
        public string SecondInverterOutput;
        public string ThirdInverterOutput;
        public string TotalInverterOutput;
        public string InverterCert;
        public string Inverterp1;
        public string Systemp1;
        public string NumberPanels;
        public string PreviousNumberPanels;
        public string PanelConfigNW;
        public string PanelConfigOTH;
        public string SystemCapKW;
        public string STCMultiplier;
        public string STCZoneRating;
        public string STCNumber;
        public string STCValue;
        public string ElecRetailerID;
        public string ElecDistributorID;
        public string ElecDistApplied;
        public string ElecDistApplyMethod;
        public string ElecDistApplyBy;
        public string ElecDistApplySentFrom;
        public string ElecDistApproved;
        public string ElecDistApprovelRef;
        public string ElecDistOK;
        public string RegPlanNo;
        public string LotNumber;
        public string Asbestoss;
        public string MeterUG;
        public string MeterEnoughSpace;
        public string SplitSystem;
        public string CherryPicker;
        public string TravelTime;
        public string VariationOther;
        public string VarRoofType;
        public string VarRoofAngle;
        public string VarHouseType;
        public string VarAsbestos;
        public string VarMeterInstallation;
        public string VarMeterUG;
        public string VarTravelTime;
        public string HotWaterMeter;
        public string SmartMeter;
        public string VarSplitSystem;
        public string VarEnoughSpace;
        public string VarCherryPicker;
        public string VarOther;
        public string SpecialDiscount;
        public string collectioncharge;
        public string DepositRequired;
        public string TotalQuotePrice;
        public string PreviousTotalQuotePrice;
        public string InvoiceExGST;
        public string InvoiceGST;
        public string BalanceGST;
        public string FinanceWithID;
        public string ServiceValue;
        public string QuoteSent;
        public string QuoteSentNo;
        public string QuoteAccepted;
        public string SignedQuote;
        public string MeterBoxPhotosSaved;
        public string ElecBillSaved;
        public string ProposedDesignSaved;
        public string FollowUp;
        public string FollowUpNote;
        public string InvoiceNumber;
        public string InvoiceTag;
        public string InvoiceDoc;
        public string InvoiceDetail;
        public string InvoiceSent;
        public string InvoiceFU;
        public string InvoiceNotes;
        public string InvRefund;
        public string InvRefunded;
        public string InvRefundBy;
        public string DepositReceived;
        public string DepositAmount;
        public string ReceiptSent;
        public string SalesCommPaid;
        public string InstallBookingDate;
        public string MeterIncluded;
        public string MeterAppliedRef;
        public string MeterAppliedDate;
        public string MeterAppliedTime;
        public string MeterAppliedMethod;
        public string MeterApprovedDate;
        public string MeterApprovalNo;
        public string MeterPhase;
        public string OffPeak;
        public string NMINumber;
        public string MeterNumber1;
        public string meterupgrade;
        public string MeterNumber2;
        public string MeterNumber3;
        public string MeterNumber4;
        public string MeterFU;
        public string MeterNotes;
        public string OldSystemp1;
        public string REXAppliedRef;
        public string REXAppliedDate;
        public string REXStatusID;
        public string REXApprovalNotes;
        public string REXApprovalFU;
        public string STCPrice;
        public string RECRebate;
        public string BalanceRequested;
        public string Installer;
        public string Designer;
        public string Electrician;
        public string InstallAM1;
        public string InstallPM1;
        public string InstallAM2;
        public string InstallPM2;
        public string InstallDays;
        public string STCFormsDone;
        public string InstallDocsReceived;
        public string InstallerNotified;
        public string CustNotifiedInstall;
        public string InstallerFollowUp;
        public string InstallerNotes;
        public string InstallerDocsSent;
        public string InstallCompleted;
        public string InstallVerifiedBy;
        public string WelcomeLetterDone;
        public string InstallRequestSaved;
        public string PanelSerials;
        public string InverterSerial;
        public string SecondInverterSerial;
        public string CertificateIssued;
        public string CertificateSaved;
        public string STCReceivedBy;
        public string STCCheckedBy;
        public string STCFormSaved;
        public string STCUploaded;
        public string STCUploadNumber;
        public string STCFU;
        public string STCNotes;
        public string InstallationComment;
        public string PVDNumber;
        public string PVDStatusID;
        public string STCApplied;
        public string BalanceReceived;
        public string Witholding;
        public string BalanceVerified;
        public string InvoicePaid;
        public string InstallerNotifiedMeter;
        public string CustNotifiedMeter;
        public string MeterElectrician;
        public string MeterInstallerFU;
        public string MeterInstallerNotes;
        public string MeterJobBooked;
        public string MeterCompleted;
        public string MeterInvoice;
        public string MeterInvoiceNo;
        public string InstallerInvNo;
        public string InstallerInvAmnt;
        public string InstallerInvDate;
        public string InstallerPayDate;
        public string MeterElecInvNo;
        public string MeterElecInvAmnt;
        public string MeterElecInvDate;
        public string MeterElecPayDate;
        public string MeterElecFollowUp;
        public string ElectricianInvoiceNotes;
        public string upsize_ts;
        public string SQ;
        public string MP;
        public string EB;
        public string PD;
        public string InvoiceStatusID;
        public string ProjectEnteredBy;
        public string UpdatedBy;
        public string DepositeOption;
        public string ST;
        public string CE;
        public string StatusComment;
        public string IsDeduct;
        public string StockDeductBy;
        public string StockDeductDate;
        public string PaymentTypeID;
        public string ApplicationDate;
        public string AppliedBy;
        public string PurchaseNo;
        public string DocumentSentDate;
        public string DocumentSentBy;
        public string DocumentReceivedDate;
        public string DocumentReceivedBy;
        public string DocumentVerified;
        public string SentBy;
        public string SentDate;
        public string PostalTrackingNumber;
        public string Remarks;
        public string ReceivedDate;
        public string ReceivedBy;
        public string PaymentVerified;
        public string InvDoc;
        public string InvDocDoor;
        public string PreExtraWork;
        public string PreAmount;
        public string PreApprovedBy;
        public string PaymentReceipt;
        public string PR;
        public string ActiveDate;
        public string SalesComm;
        public string STCFormSign;
        public string SerialNumbers;
        public string QuotationForm;
        public string CustomerAck;
        public string ComplianceCerti;
        public string CustomerAccept;
        public string EWRNumber;
        public string PatmentMethod;
        public string FlatPanels;
        public string PitchedPanels;
        public string SurveyCerti;
        public string ElecDistAppDate;
        public string ElecDistAppBy;
        public string InstInvDoc;
        public string CertiApprove;
        public string NewDate;
        public string NewNotes;
        public string IsFormBay;
        public string financewith;
        public string inverterqty;
        public string inverterqty2;
        public string inverterqty3;
        public string StockCategoryID;
        public string StockItemID;
        public string ElecDistributor;
        public string ElecRetailer;
        public string HouseType;
        public string RoofType;
        public string RoofAngle;
        public string PanelBrandName;
        public string Inverterp1Name;
        public string SecondInverterp1;
        public string Contact;
        public string InvoiceStatus;
        public string SalesRepName;
        public string InstallerName;
        public string DesignerName;
        public string ElectricianName;
        public string FinanceWith;
        public string PaymentType;
        public string StoreName;
        public string Customer;
        public string FormbayId;
        public string unit_type;
        public string unit_number;
        public string street_number;
        public string street_address;
        public string street_name;
        public string street_type;
        public string street_suffix;
        public string mtcepaperwork;
        public string LinkProjectID;
        public string FormbayInstallBookingDate;
        public string beatquote;
        public string beatquotedoc;
        public string nearmap;
        public string nearmapdoc;
        public string SalesType;
        public string projecttype;
        public string projectcancel;
        public string PVDStatus;
        public string DocumentSentByName;
        public string DocumentReceivedByName;
        public string Empname1;
        public string FDA;
        public string readyactive;
        public string notes;
        public string paydate;
        public string SSCompleteDate;
        public string SSActiveDate;
        public string QuickForm;
        public string IsClickCustomer;
        public string HouseStayID;
        public string HouseStayDate;
        public string ComplainceCertificate;
        public string quickformGuid;
        public string GreenBotFlag;
        public string VicAppReference;
        public string VicDate;
        public string VicRebateNote;
        public string VicRebate;
        public string VicRebate1;
        public string ViCLoan;
        public string VicLoanDisc;
        public string RebateStatus;
        public string PrjStatus;
        public string STcYear;
        public string panelQty;
        public string InvertQty1;
        public string InvertQty2;
        public string InvertQty3;
    }

    public ProjectDetails GetProjectsRelatedDetails(string ProjectId)
    {
        //List<ProjeceDetails> p1 = new List<ProjeceDetails>();
        ProjectDetails p1 = new ProjectDetails();
        if (ProjectId != null && ProjectId != "")
        {
            try
            {
                DbCommand comm = DataAccess.CreateCommand();
                comm.CommandText = "tblProjects_SelectByProjectID";

                DbParameter param = comm.CreateParameter();
                param.ParameterName = "@ProjectID";
                param.Value = ProjectId;
                param.DbType = DbType.Int32;
                comm.Parameters.Add(param);
                // execute the stored procedure
                DataTable table = DataAccess.ExecuteSelectCommand(comm);
                if (table.Rows.Count > 0)
                {
                    DataRow dr = table.Rows[0];
                    // get product p1
                    p1.CustomerID = dr["CustomerID"].ToString();
                    p1.ContactID = dr["ContactID"].ToString();
                    p1.EmployeeID = dr["EmployeeID"].ToString();
                    p1.SalesRep = dr["SalesRep"].ToString();
                    p1.ProjectTypeID = dr["ProjectTypeID"].ToString();
                    p1.ProjectStatusID = dr["ProjectStatusID"].ToString();
                    p1.ProjectCancelID = dr["ProjectCancelID"].ToString();
                    p1.ProjectOnHoldID = dr["ProjectOnHoldID"].ToString();
                    p1.ProjectOpened = dr["ProjectOpened"].ToString();
                    p1.ProjectCancelled = dr["ProjectCancelled"].ToString();
                    p1.ProjectNumber = dr["ProjectNumber"].ToString();
                    p1.OldProjectNumber = dr["OldProjectNumber"].ToString();
                    p1.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
                    p1.Project = dr["Project"].ToString();
                    p1.readyactive = dr["readyactive"].ToString();
                    p1.NextMaintenanceCall = dr["NextMaintenanceCall"].ToString();
                    p1.InstallAddress = dr["InstallAddress"].ToString();
                    p1.InstallCity = dr["InstallCity"].ToString();
                    p1.InstallState = dr["InstallState"].ToString();
                    p1.InstallPostCode = dr["InstallPostCode"].ToString();
                    p1.ProjectNotes = dr["ProjectNotes"].ToString();
                    p1.AdditionalSystem = dr["AdditionalSystem"].ToString();
                    p1.GridConnected = dr["GridConnected"].ToString();
                    p1.RebateApproved = dr["RebateApproved"].ToString();
                    p1.ReceivedCredits = dr["ReceivedCredits"].ToString();
                    p1.CreditEligible = dr["CreditEligible"].ToString();
                    p1.MoreThanOneInstall = dr["MoreThanOneInstall"].ToString();
                    p1.RequiredCompliancePaperwork = dr["RequiredCompliancePaperwork"].ToString();
                    p1.OutOfPocketDocs = dr["OutOfPocketDocs"].ToString();
                    p1.OwnerGSTRegistered = dr["OwnerGSTRegistered"].ToString();
                    p1.OwnerABN = dr["OwnerABN"].ToString();
                    p1.HouseTypeID = dr["HouseTypeID"].ToString();
                    p1.RoofTypeID = dr["RoofTypeID"].ToString();
                    p1.RoofAngleID = dr["RoofAngleID"].ToString();
                    p1.InstallBase = dr["InstallBase"].ToString();
                    p1.StockAllocationStore = dr["StockAllocationStore"].ToString();
                    p1.StandardPack = dr["StandardPack"].ToString();
                    p1.PanelBrandID = dr["PanelBrandID"].ToString();
                    p1.PanelBrand = dr["PanelBrand"].ToString();
                    p1.PanelModel = dr["PanelModel"].ToString();
                    p1.PanelOutput = dr["PanelOutput"].ToString();
                    //p1.Panelp1 = dr["Panelp1"].ToString();
                    //p1.Inverterp1ID = dr["Inverterp1ID"].ToString();
                    //p1.SecondInverterp1ID = dr["SecondInverterp1ID"].ToString();
                    //p1.ThirdInverterp1ID = dr["ThirdInverterp1ID"].ToString();
                    p1.InverterBrand = dr["InverterBrand"].ToString();
                    p1.InverterModel = dr["InverterModel"].ToString();
                    p1.InverterSeries = dr["InverterSeries"].ToString();
                    p1.InverterOutput = dr["InverterOutput"].ToString();
                    p1.SecondInverterOutput = dr["SecondInverterOutput"].ToString();
                    p1.ThirdInverterOutput = dr["ThirdInverterOutput"].ToString();
                    p1.TotalInverterOutput = dr["TotalInverterOutput"].ToString();
                    p1.InverterCert = dr["InverterCert"].ToString();
                    //p1.Inverterp1 = dr["Inverterp1"].ToString();
                    //p1.Systemp1 = dr["Systemp1"].ToString();
                    p1.NumberPanels = dr["NumberPanels"].ToString();
                    p1.PreviousNumberPanels = dr["PreviousNumberPanels"].ToString();
                    p1.PanelConfigNW = dr["PanelConfigNW"].ToString();
                    p1.PanelConfigOTH = dr["PanelConfigOTH"].ToString();
                    p1.SystemCapKW = dr["SystemCapKW"].ToString();
                    p1.STCMultiplier = dr["STCMultiplier"].ToString();
                    p1.STCZoneRating = dr["STCZoneRating"].ToString();
                    p1.STCNumber = dr["STCNumber"].ToString();
                    p1.STCValue = dr["STCValue"].ToString();
                    p1.ElecRetailerID = dr["ElecRetailerID"].ToString();
                    p1.ElecDistributorID = dr["ElecDistributorID"].ToString();
                    p1.ElecDistApplied = dr["ElecDistApplied"].ToString();
                    p1.ElecDistApplyMethod = dr["ElecDistApplyMethod"].ToString();
                    p1.ElecDistApplyBy = dr["ElecDistApplyBy"].ToString();
                    p1.ElecDistApplySentFrom = dr["ElecDistApplySentFrom"].ToString();
                    p1.ElecDistApproved = dr["ElecDistApproved"].ToString();
                    p1.ElecDistApprovelRef = dr["ElecDistApprovelRef"].ToString();
                    p1.ElecDistOK = dr["ElecDistOK"].ToString();
                    p1.RegPlanNo = dr["RegPlanNo"].ToString();
                    p1.LotNumber = dr["LotNumber"].ToString();
                    p1.Asbestoss = dr["Asbestoss"].ToString();
                    p1.MeterUG = dr["MeterUG"].ToString();
                    p1.MeterEnoughSpace = dr["MeterEnoughSpace"].ToString();
                    p1.SplitSystem = dr["SplitSystem"].ToString();
                    p1.CherryPicker = dr["CherryPicker"].ToString();
                    p1.TravelTime = dr["TravelTime"].ToString();
                    p1.VariationOther = dr["VariationOther"].ToString();
                    p1.VarRoofType = dr["VarRoofType"].ToString();
                    p1.VarRoofAngle = dr["VarRoofAngle"].ToString();
                    p1.VarHouseType = dr["VarHouseType"].ToString();
                    p1.VarAsbestos = dr["VarAsbestos"].ToString();
                    p1.VarMeterInstallation = dr["VarMeterInstallation"].ToString();
                    p1.VarMeterUG = dr["VarMeterUG"].ToString();
                    p1.VarTravelTime = dr["VarTravelTime"].ToString();
                    p1.HotWaterMeter = dr["HotWaterMeter"].ToString();
                    p1.SmartMeter = dr["SmartMeter"].ToString();
                    p1.VarSplitSystem = dr["VarSplitSystem"].ToString();
                    p1.VarEnoughSpace = dr["VarEnoughSpace"].ToString();
                    p1.VarCherryPicker = dr["VarCherryPicker"].ToString();
                    p1.VarOther = dr["VarOther"].ToString();
                    p1.SpecialDiscount = dr["SpecialDiscount"].ToString();
                    p1.collectioncharge = dr["collectioncharge"].ToString();

                    p1.DepositRequired = dr["DepositRequired"].ToString();
                    p1.TotalQuotePrice = dr["TotalQuotePrice"].ToString();
                    p1.PreviousTotalQuotePrice = dr["PreviousTotalQuotePrice"].ToString();
                    p1.InvoiceExGST = dr["InvoiceExGST"].ToString();
                    p1.InvoiceGST = dr["InvoiceGST"].ToString();
                    p1.BalanceGST = dr["BalanceGST"].ToString();
                    p1.FinanceWithID = dr["FinanceWithID"].ToString();
                    p1.ServiceValue = dr["ServiceValue"].ToString();
                    p1.QuoteSent = dr["QuoteSent"].ToString();
                    p1.QuoteSentNo = dr["QuoteSentNo"].ToString();
                    p1.QuoteAccepted = dr["QuoteAccepted"].ToString();
                    p1.SignedQuote = dr["SignedQuote"].ToString();
                    p1.MeterBoxPhotosSaved = dr["MeterBoxPhotosSaved"].ToString();
                    p1.ElecBillSaved = dr["ElecBillSaved"].ToString();
                    p1.ProposedDesignSaved = dr["ProposedDesignSaved"].ToString();
                    p1.FollowUp = dr["FollowUp"].ToString();
                    p1.FollowUpNote = dr["FollowUpNote"].ToString();
                    p1.InvoiceNumber = dr["InvoiceNumber"].ToString();
                    p1.InvoiceTag = dr["InvoiceTag"].ToString();
                    p1.InvoiceDoc = dr["InvoiceDoc"].ToString();
                    p1.InvoiceDetail = dr["InvoiceDetail"].ToString();
                    p1.InvoiceSent = dr["InvoiceSent"].ToString();
                    p1.InvoiceFU = dr["InvoiceFU"].ToString();
                    p1.InvoiceNotes = dr["InvoiceNotes"].ToString();
                    p1.InvRefund = dr["InvRefund"].ToString();
                    p1.InvRefunded = dr["InvRefunded"].ToString();
                    p1.InvRefundBy = dr["InvRefundBy"].ToString();
                    p1.DepositReceived = dr["DepositReceived"].ToString();
                    p1.DepositAmount = dr["DepositAmount"].ToString();
                    p1.ReceiptSent = dr["ReceiptSent"].ToString();
                    p1.SalesCommPaid = dr["SalesCommPaid"].ToString();
                    p1.InstallBookingDate = dr["InstallBookingDate"].ToString();
                    p1.MeterIncluded = dr["MeterIncluded"].ToString();
                    p1.MeterAppliedRef = dr["MeterAppliedRef"].ToString();
                    p1.MeterAppliedDate = dr["MeterAppliedDate"].ToString();
                    p1.MeterAppliedTime = dr["MeterAppliedTime"].ToString();
                    p1.MeterAppliedMethod = dr["MeterAppliedMethod"].ToString();
                    p1.MeterApprovedDate = dr["MeterApprovedDate"].ToString();
                    p1.MeterApprovalNo = dr["MeterApprovalNo"].ToString();
                    p1.MeterPhase = dr["MeterPhase"].ToString();
                    p1.OffPeak = dr["OffPeak"].ToString();
                    p1.NMINumber = dr["NMINumber"].ToString();
                    p1.meterupgrade = dr["meterupgrade"].ToString();
                    p1.MeterNumber1 = dr["MeterNumber1"].ToString();
                    p1.MeterNumber2 = dr["MeterNumber2"].ToString();
                    p1.MeterNumber3 = dr["MeterNumber3"].ToString();
                    p1.MeterNumber4 = dr["MeterNumber4"].ToString();
                    p1.MeterFU = dr["MeterFU"].ToString();
                    p1.MeterNotes = dr["MeterNotes"].ToString();
                    //p1.OldSystemp1 = dr["OldSystemp1"].ToString();
                    p1.REXAppliedRef = dr["REXAppliedRef"].ToString();
                    p1.REXAppliedDate = dr["REXAppliedDate"].ToString();
                    p1.REXStatusID = dr["REXStatusID"].ToString();
                    p1.REXApprovalNotes = dr["REXApprovalNotes"].ToString();
                    p1.REXApprovalFU = dr["REXApprovalFU"].ToString();
                    p1.STCPrice = dr["STCPrice"].ToString();
                    p1.RECRebate = dr["RECRebate"].ToString();
                    p1.BalanceRequested = dr["BalanceRequested"].ToString();
                    p1.Installer = dr["Installer"].ToString();
                    p1.Designer = dr["Designer"].ToString();
                    p1.Electrician = dr["Electrician"].ToString();
                    p1.InstallAM1 = dr["InstallAM1"].ToString();
                    p1.InstallPM1 = dr["InstallPM1"].ToString();
                    p1.InstallAM2 = dr["InstallAM2"].ToString();
                    p1.InstallPM2 = dr["InstallPM2"].ToString();
                    p1.InstallDays = dr["InstallDays"].ToString();
                    p1.STCFormsDone = dr["STCFormsDone"].ToString();
                    p1.InstallDocsReceived = dr["InstallDocsReceived"].ToString();
                    p1.InstallerNotified = dr["InstallerNotified"].ToString();
                    p1.CustNotifiedInstall = dr["CustNotifiedInstall"].ToString();
                    p1.InstallerFollowUp = dr["InstallerFollowUp"].ToString();
                    p1.InstallerNotes = dr["InstallerNotes"].ToString();
                    p1.InstallerDocsSent = dr["InstallerDocsSent"].ToString();
                    p1.InstallCompleted = dr["InstallCompleted"].ToString();
                    p1.InstallVerifiedBy = dr["InstallVerifiedBy"].ToString();
                    p1.WelcomeLetterDone = dr["WelcomeLetterDone"].ToString();
                    p1.InstallRequestSaved = dr["InstallRequestSaved"].ToString();
                    p1.PanelSerials = dr["PanelSerials"].ToString();
                    p1.InverterSerial = dr["InverterSerial"].ToString();
                    p1.SecondInverterSerial = dr["SecondInverterSerial"].ToString();
                    p1.CertificateIssued = dr["CertificateIssued"].ToString();
                    p1.CertificateSaved = dr["CertificateSaved"].ToString();
                    p1.STCReceivedBy = dr["STCReceivedBy"].ToString();
                    p1.STCCheckedBy = dr["STCCheckedBy"].ToString();
                    p1.STCFormSaved = dr["STCFormSaved"].ToString();
                    p1.STCUploaded = dr["STCUploaded"].ToString();
                    p1.STCUploadNumber = dr["STCUploadNumber"].ToString();
                    p1.STCFU = dr["STCFU"].ToString();
                    p1.STCNotes = dr["STCNotes"].ToString();
                    p1.InstallationComment = dr["InstallationComment"].ToString();
                    p1.PVDNumber = dr["PVDNumber"].ToString();
                    p1.PVDStatusID = dr["PVDStatusID"].ToString();
                    p1.STCApplied = dr["STCApplied"].ToString();
                    p1.BalanceReceived = dr["BalanceReceived"].ToString();
                    p1.Witholding = dr["Witholding"].ToString();
                    p1.BalanceVerified = dr["BalanceVerified"].ToString();
                    p1.InvoicePaid = dr["InvoicePaid"].ToString();
                    p1.InstallerNotifiedMeter = dr["InstallerNotifiedMeter"].ToString();
                    p1.CustNotifiedMeter = dr["CustNotifiedMeter"].ToString();
                    p1.MeterElectrician = dr["MeterElectrician"].ToString();
                    p1.MeterInstallerFU = dr["MeterInstallerFU"].ToString();
                    p1.MeterInstallerNotes = dr["MeterInstallerNotes"].ToString();
                    p1.MeterJobBooked = dr["MeterJobBooked"].ToString();
                    p1.MeterCompleted = dr["MeterCompleted"].ToString();
                    p1.MeterInvoice = dr["MeterInvoice"].ToString();
                    p1.MeterInvoiceNo = dr["MeterInvoiceNo"].ToString();
                    p1.InstallerInvNo = dr["InstallerInvNo"].ToString();
                    p1.InstallerInvAmnt = dr["InstallerInvAmnt"].ToString();
                    p1.InstallerInvDate = dr["InstallerInvDate"].ToString();
                    p1.InstallerPayDate = dr["InstallerPayDate"].ToString();
                    p1.MeterElecInvNo = dr["MeterElecInvNo"].ToString();
                    p1.MeterElecInvAmnt = dr["MeterElecInvAmnt"].ToString();
                    p1.MeterElecInvDate = dr["MeterElecInvDate"].ToString();
                    p1.MeterElecPayDate = dr["MeterElecPayDate"].ToString();
                    p1.MeterElecFollowUp = dr["MeterElecFollowUp"].ToString();
                    p1.ElectricianInvoiceNotes = dr["ElectricianInvoiceNotes"].ToString();
                    p1.upsize_ts = dr["upsize_ts"].ToString();
                    p1.SQ = dr["SQ"].ToString();
                    p1.MP = dr["MP"].ToString();
                    p1.EB = dr["EB"].ToString();
                    p1.PD = dr["PD"].ToString();
                    p1.InvoiceStatusID = dr["InvoiceStatusID"].ToString();
                    p1.ProjectEnteredBy = dr["ProjectEnteredBy"].ToString();
                    p1.UpdatedBy = dr["UpdatedBy"].ToString();
                    p1.DepositeOption = dr["DepositeOption"].ToString();
                    p1.ST = dr["ST"].ToString();
                    p1.CE = dr["CE"].ToString();
                    p1.StatusComment = dr["StatusComment"].ToString();
                    p1.IsDeduct = dr["IsDeduct"].ToString();
                    p1.StockDeductBy = dr["StockDeductBy"].ToString();
                    p1.StockDeductDate = dr["StockDeductDate"].ToString();
                    p1.PaymentTypeID = dr["PaymentTypeID"].ToString();
                    p1.ApplicationDate = dr["ApplicationDate"].ToString();
                    p1.AppliedBy = dr["AppliedBy"].ToString();
                    p1.PurchaseNo = dr["PurchaseNo"].ToString();
                    p1.DocumentSentDate = dr["DocumentSentDate"].ToString();
                    p1.DocumentSentBy = dr["DocumentSentBy"].ToString();
                    p1.DocumentReceivedDate = dr["DocumentReceivedDate"].ToString();
                    p1.DocumentReceivedBy = dr["DocumentReceivedBy"].ToString();
                    p1.DocumentVerified = dr["DocumentVerified"].ToString();
                    p1.SentBy = dr["SentBy"].ToString();
                    p1.SentDate = dr["SentDate"].ToString();
                    p1.PostalTrackingNumber = dr["PostalTrackingNumber"].ToString();
                    p1.Remarks = dr["Remarks"].ToString();
                    p1.ReceivedDate = dr["ReceivedDate"].ToString();
                    p1.ReceivedBy = dr["ReceivedBy"].ToString();
                    p1.PaymentVerified = dr["PaymentVerified"].ToString();
                    p1.InvDoc = dr["InvDoc"].ToString();
                    p1.InvDocDoor = dr["InvDocDoor"].ToString();
                    p1.PreExtraWork = dr["PreExtraWork"].ToString();
                    p1.PreAmount = dr["PreAmount"].ToString();
                    p1.PreApprovedBy = dr["PreApprovedBy"].ToString();
                    p1.PaymentReceipt = dr["PaymentReceipt"].ToString();
                    p1.PR = dr["PR"].ToString();
                    p1.ActiveDate = dr["ActiveDate"].ToString();
                    p1.SalesComm = dr["SalesComm"].ToString();
                    p1.STCFormSign = dr["STCFormSign"].ToString();
                    p1.SerialNumbers = dr["SerialNumbers"].ToString();
                    p1.QuotationForm = dr["QuotationForm"].ToString();
                    p1.CustomerAck = dr["CustomerAck"].ToString();
                    p1.ComplianceCerti = dr["ComplianceCerti"].ToString();
                    p1.CustomerAccept = dr["CustomerAccept"].ToString();
                    p1.EWRNumber = dr["EWRNumber"].ToString();
                    p1.PatmentMethod = dr["PatmentMethod"].ToString();
                    p1.FlatPanels = dr["FlatPanels"].ToString();
                    p1.PitchedPanels = dr["PitchedPanels"].ToString();
                    p1.SurveyCerti = dr["SurveyCerti"].ToString();
                    p1.ElecDistAppDate = dr["ElecDistAppDate"].ToString();
                    p1.ElecDistAppBy = dr["ElecDistAppBy"].ToString();
                    p1.InstInvDoc = dr["InstInvDoc"].ToString();
                    p1.CertiApprove = dr["CertiApprove"].ToString();
                    p1.NewDate = dr["NewDate"].ToString();
                    p1.NewNotes = dr["NewNotes"].ToString();
                    p1.IsFormBay = dr["IsFormBay"].ToString();
                    //p1.financewith = dr["financewith"].ToString();
                    p1.inverterqty = dr["inverterqty"].ToString();
                    p1.inverterqty2 = dr["inverterqty2"].ToString();
                    p1.inverterqty3 = dr["inverterqty3"].ToString();

                    //p1.SecondInverterp1 = dr["SecondInverterp1"].ToString();
                    //p1.ElecDistributor = dr["ElecDistributor"].ToString();
                    p1.ElecRetailer = dr["ElecRetailer"].ToString();
                    p1.HouseType = dr["HouseType"].ToString();
                    p1.RoofType = dr["RoofType"].ToString();
                    p1.RoofAngle = dr["RoofAngle"].ToString();
                    p1.PanelBrandName = dr["PanelBrandName"].ToString();
                    //p1.Inverterp1Name = dr["Inverterp1Name"].ToString();
                    p1.Contact = dr["Contact"].ToString();
                    p1.InvoiceStatus = dr["InvoiceStatus"].ToString();
                    p1.SalesRepName = dr["SalesRepName"].ToString();
                    p1.InstallerName = dr["InstallerName"].ToString();
                    p1.DesignerName = dr["DesignerName"].ToString();
                    p1.ElectricianName = dr["ElectricianName"].ToString();
                    p1.FinanceWith = dr["FinanceWith"].ToString();
                    p1.PaymentType = dr["PaymentType"].ToString();
                    p1.StoreName = dr["StoreName"].ToString();
                    p1.Customer = dr["Customer"].ToString();

                    p1.FormbayId = dr["FormbayId"].ToString();
                    p1.unit_type = dr["unit_type"].ToString();
                    p1.unit_number = dr["unit_number"].ToString();
                    p1.street_number = dr["street_number"].ToString();
                    p1.street_address = dr["street_address"].ToString();
                    p1.street_name = dr["street_name"].ToString();
                    p1.street_type = dr["street_type"].ToString();
                    p1.street_suffix = dr["street_suffix"].ToString();
                    p1.mtcepaperwork = dr["mtcepaperwork"].ToString();
                    p1.LinkProjectID = dr["LinkProjectID"].ToString();
                    p1.FormbayInstallBookingDate = dr["FormbayInstallBookingDate"].ToString();
                    p1.beatquote = dr["beatquote"].ToString();
                    p1.beatquotedoc = dr["beatquotedoc"].ToString();
                    p1.nearmap = dr["nearmapcheck"].ToString();
                    p1.nearmapdoc = dr["nearmapdoc"].ToString();
                    p1.SalesType = dr["SalesType"].ToString();
                    p1.projecttype = dr["projecttype"].ToString();
                    p1.projectcancel = dr["projectcancel"].ToString();
                    p1.PVDStatus = dr["PVDStatus"].ToString();
                    p1.DocumentSentByName = dr["DocumentSentByName"].ToString();
                    p1.DocumentReceivedByName = dr["DocumentReceivedByName"].ToString();
                    p1.Empname1 = dr["Empname1"].ToString();
                    p1.FDA = dr["FDA"].ToString();
                    p1.notes = dr["notes"].ToString();
                    p1.paydate = dr["paydate"].ToString();
                    p1.SSActiveDate = dr["SSActiveDate"].ToString();
                    p1.SSCompleteDate = dr["SSCompleteDate"].ToString();
                    p1.QuickForm = dr["QuickForm"].ToString();
                    p1.IsClickCustomer = dr["IsClickCustomer"].ToString();

                    //p1.StockCategoryID = dr["StockCategoryID"].ToString();
                    //p1.StockItemID = dr["StockItemID"].ToString();
                    p1.HouseStayDate = dr["HouseStayDate"].ToString();
                    p1.HouseStayID = dr["HouseStayID"].ToString();
                    p1.ComplainceCertificate = dr["ComplainceCertificate"].ToString();
                    p1.quickformGuid = dr["quickformGuid"].ToString();
                    p1.GreenBotFlag = dr["GreenBotFlag"].ToString();
                    p1.VicAppReference = dr["VicAppReference"].ToString();
                    p1.VicDate = dr["VicDate"].ToString();
                    p1.VicRebateNote = dr["VicRebateNote"].ToString();
                    p1.VicRebate = dr["VicRebate"].ToString();
                    p1.VicRebate1 = dr["solarvicrebate"].ToString();
                    p1.VicLoanDisc = dr["solarvicloandisc"].ToString();
                    p1.RebateStatus = dr["RebateStatus"].ToString();
                    p1.PrjStatus = dr["ProjectStatus"].ToString();
                    //p1.STcYear = dr["Stcrate"].ToString();
                    //p1.panelQty = dr["panelQty"].ToString();
                    //p1.InvertQty1 = dr["InvertQty1"].ToString();
                    //p1.InvertQty2 = dr["InvertQty2"].ToString();
                    //p1.InvertQty3 = dr["InvertQty3"].ToString();
                }
            }
            catch (Exception ex)
            {
                p1.Message = "Error at" + ex.Message;
            }
        }
        else
        {
            p1.Message = "ProjectId can't blank";
        }
        return p1;

    }
    public class UpdateData
    {
        //public string projectId { get; set; }
        //public string pickListID { get; set; }
        //public string DeductNote { get; set; }
        //public string LocationId { get; set; }
        public string StatusMessage { get; set; }
    }
    public UpdateData tblProjects_UpdateDeductNote_FromService
    (string projectId, string projectnumber, string note,
        string pickupdate, string installerid, string pickupnote, string updatedby,
        string updateddate, string picklistid)
    {
        string msg = "";
        UpdateData i = new UpdateData();
        List<UpdateData> Data_Item1 = new List<UpdateData>();
        if (projectId != null && projectId != "")
        {
            int Exists = ClstblProjects.tblProjects_ExistsByProjectID(projectId);
            if (Exists == 1)
            {
                try
                {
                    DateTime dt = new DateTime(); 

                    if (pickupdate != null && pickupdate != "")
                    {
                        
                        //dt.Now.AddHours(14).ToShortDateString();
                        pickupdate = DateTime.Now.AddHours(14).ToString();
                    }
                    if (updateddate != null && updateddate != "")
                    {
                        updateddate = DateTime.Now.AddHours(14).ToString();
                    }
                   
                    if (note == null || note == "")
                    {
                        note ="";
                    }
                    bool s1 = ClstblProjects.tblProjects_UpdateDeductNote_FromService(projectId, projectnumber, note,
         pickupdate, installerid, pickupnote, updatedby,
         updateddate, picklistid);
                   
                    if (s1)
                    {
                        msg = "Success";
                    }
                    else
                    {
                        msg = "Error";
                    }
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                }

            }
            else
            {
                msg = "ProjectId DoesNot Exists";
            }
        }
        else
        {
            msg = "ProjectId Can't Be blank";
        }

        i.StatusMessage = msg;
        return i;
    }

    public UpdateData tblprojectsUpdateStatus(string projectId)
    {
        string msg = "";
        UpdateData i = new UpdateData();
        if (projectId != null && projectId != "")
        {
            int Exists = ClstblProjects.tblProjects_ExistsByProjectID(projectId);
            if (Exists == 1)
            {
                try
                {
                    bool s1 = ClstblProjects.tblprojectsUpdateStatus(projectId);
                    if (s1)
                    {
                        msg = "Success";
                    }
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                }
            }
            else
            {
                msg = "ProjectId DoesNot Exists";
            }
        }
        else
        {
            msg = "ProjectId Can't Be blank";
        }
        i.StatusMessage = msg;
        return i;
    }

}
