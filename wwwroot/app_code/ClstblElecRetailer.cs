using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblElecRetailer
{
    public string ElecRetailer;
    public string Active;
    public string Seq;
    public string NSW;
    public string SA;
    public string QLD;
    public string VIC;
    public string WA;
    public string ACT;
    public string TAS;
    public string NT;
    public string ElectricityProviderId;
}


public class ClstblElecRetailer
{
    public static SttblElecRetailer tblElecRetailer_SelectByElecRetailerID(String ElecRetailerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_SelectByElecRetailerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        if (ElecRetailerID != string.Empty)
            param.Value = ElecRetailerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblElecRetailer details = new SttblElecRetailer();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ElecRetailer = dr["ElecRetailer"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();
            details.NSW = dr["NSW"].ToString();
            details.SA = dr["SA"].ToString();
            details.QLD = dr["QLD"].ToString();
            details.VIC = dr["VIC"].ToString();
            details.WA = dr["WA"].ToString();
            details.ACT = dr["ACT"].ToString();
            details.TAS = dr["TAS"].ToString();
            details.NT = dr["NT"].ToString();
            details.ElectricityProviderId = dr["ElectricityProviderId"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblElecRetailer_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblElecRetailer_SelectQLD()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_SelectQLD";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblElecRetailer_Insert(String ElecRetailer, String Active, String Seq, String NSW, String SA, String QLD, String VIC, String WA, String ACT, String TAS, String NT, String ElectricityProviderId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailer";
        param.Value = ElecRetailer;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NSW";
        param.Value = NSW;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SA";
        param.Value = SA;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QLD";
        param.Value = QLD;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VIC";
        param.Value = VIC;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WA";
        param.Value = WA;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ACT";
        param.Value = ACT;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TAS";
        param.Value = TAS;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NT";
        param.Value = NT;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecticityProviderId";
        param.Value = ElectricityProviderId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param); 

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblElecRetailer_Update(string ElecRetailerID, String ElecRetailer, String Active, String Seq, String NSW, String SA, String QLD, String VIC, String WA, String ACT, String TAS, String NT,String ElectricityProviderId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        param.Value = ElecRetailerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailer";
        param.Value = ElecRetailer;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NSW";
        param.Value = NSW;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SA";
        param.Value = SA;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QLD";
        param.Value = QLD;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VIC";
        param.Value = VIC;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WA";
        param.Value = WA;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ACT";
        param.Value = ACT;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TAS";
        param.Value = TAS;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NT";
        param.Value = NT;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecticityProviderId";
        param.Value = ElectricityProviderId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        
            
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblElecRetailer_Delete(string ElecRetailerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        param.Value = ElecRetailerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }


    public static int tblElecRetailer_Exists(string ElecRetailer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailer";
        param.Value = ElecRetailer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblElecRetailer_ExistsById(string ElecRetailer, string ElecRetailerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailer";
        param.Value = ElecRetailer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        param.Value = ElecRetailerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblElecRetailer_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblElecRetailer_GetDataBySearch(string alpha ,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblElecRetailer_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
}