using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct SttblProjects
{
    public string PriceCat;
    public string EmailFlag;
    public string CustomerID;
    public string ContactID;
    public string EmployeeID;
    public string SalesRep;
    public string ProjectTypeID;
    public string ProjectStatusID;
    public string ProjectCancelID;
    public string ProjectOnHoldID;
    public string ProjectOpened;
    public string ProjectCancelled;
    public string ProjectNumber;
    public string OldProjectNumber;
    public string ManualQuoteNumber;
    public string Project;
    public string Projects;
    public string NextMaintenanceCall;
    public string InstallAddress;
    public string InstallCity;
    public string InstallState;
    public string InstallPostCode;
    public string ProjectNotes;
    public string AdditionalSystem;
    public string GridConnected;
    public string RebateApproved;
    public string ReceivedCredits;
    public string CreditEligible;
    public string MoreThanOneInstall;
    public string RequiredCompliancePaperwork;
    public string OutOfPocketDocs;
    public string OwnerGSTRegistered;
    public string OwnerABN;
    public string HouseTypeID;
    public string RoofTypeID;
    public string RoofAngleID;
    public string InstallBase;
    public string StockAllocationStore;
    public string StandardPack;
    public string PanelBrandID;
    public string PanelBrand;
    public string PanelModel;
    public string PanelOutput;
    public string PanelDetails;
    public string InverterDetailsID;
    public string SecondInverterDetailsID;
    public string ThirdInverterDetailsID;
    public string InverterBrand;
    public string InverterModel;
    public string InverterSeries;
    public string InverterOutput;
    public string SecondInverterOutput;
    public string ThirdInverterOutput;
    public string TotalInverterOutput;
    public string InverterCert;
    public string InverterDetails;
    public string SystemDetails;
    public string NumberPanels;
    public string PreviousNumberPanels;
    public string PanelConfigNW;
    public string PanelConfigOTH;
    public string SystemCapKW;
    public string STCMultiplier;
    public string STCZoneRating;
    public string STCNumber;
    public string STCValue;
    public string ElecRetailerID;
    public string ElecDistributorID;
    public string ElecDistApplied;
    public string ElecDistApplyMethod;
    public string ElecDistApplyBy;
    public string ElecDistApplySentFrom;
    public string ElecDistApproved;
    public string ElecDistApprovelRef;
    public string ElecDistOK;
    public string RegPlanNo;
    public string LotNumber;
    public string Asbestoss;
    public string MeterUG;
    public string MeterEnoughSpace;
    public string SplitSystem;
    public string CherryPicker;
    public string TravelTime;
    public string VariationOther;
    public string VarRoofType;
    public string VarRoofAngle;
    public string VarHouseType;
    public string VarAsbestos;
    public string VarMeterInstallation;
    public string VarMeterUG;
    public string VarTravelTime;
    public string HotWaterMeter;
    public string SmartMeter;
    public string VarSplitSystem;
    public string VarEnoughSpace;
    public string VarCherryPicker;
    public string VarOther;
    public string SpecialDiscount;
    public string collectioncharge;
    public string DepositRequired;
    public string TotalQuotePrice;
    public string PreviousTotalQuotePrice;
    public string InvoiceExGST;
    public string InvoiceGST;
    public string BalanceGST;
    public string FinanceWithID;
    public string ServiceValue;
    public string QuoteSent;
    public string QuoteSentNo;
    public string QuoteAccepted;
    public string SignedQuote;
    public string MeterBoxPhotosSaved;
    public string ElecBillSaved;
    public string ProposedDesignSaved;
    public string FollowUp;
    public string FollowUpNote;
    public string InvoiceNumber;
    public string InvoiceTag;
    public string InvoiceDoc;
    public string InvoiceDetail;
    public string InvoiceSent;
    public string InvoiceFU;
    public string InvoiceNotes;
    public string InvRefund;
    public string InvRefunded;
    public string InvRefundBy;
    public string DepositReceived;
    public string DepositAmount;
    public string ReceiptSent;
    public string SalesCommPaid;
    public string InstallBookingDate;
    public string MeterIncluded;
    public string MeterAppliedRef;
    public string MeterAppliedDate;
    public string MeterAppliedTime;
    public string MeterAppliedMethod;
    public string MeterApprovedDate;
    public string MeterApprovalNo;
    public string MeterPhase;
    public string OffPeak;
    public string NMINumber;
    public string MeterNumber1;
    public string meterupgrade;
    public string MeterNumber2;
    public string MeterNumber3;
    public string MeterNumber4;
    public string MeterFU;
    public string MeterNotes;
    public string OldSystemDetails;
    public string REXAppliedRef;
    public string REXAppliedDate;
    public string REXStatusID;
    public string REXApprovalNotes;
    public string REXApprovalFU;
    public string STCPrice;
    public string RECRebate;
    public string BalanceRequested;
    public string Installer;
    public string Designer;
    public string Electrician;
    public string InstallAM1;
    public string InstallPM1;
    public string InstallAM2;
    public string InstallPM2;
    public string InstallDays;
    public string STCFormsDone;
    public string InstallDocsReceived;
    public string InstallerNotified;
    public string CustNotifiedInstall;
    public string InstallerFollowUp;
    public string InstallerNotes;
    public string InstallerDocsSent;
    public string InstallCompleted;
    public string InstallVerifiedBy;
    public string WelcomeLetterDone;
    public string InstallRequestSaved;
    public string PanelSerials;
    public string InverterSerial;
    public string SecondInverterSerial;
    public string CertificateIssued;
    public string CertificateSaved;
    public string STCReceivedBy;
    public string STCCheckedBy;
    public string STCFormSaved;
    public string STCUploaded;
    public string STCUploadNumber;
    public string STCFU;
    public string STCNotes;
    public string InstallationComment;
    public string PVDNumber;
    public string PVDStatusID;
    public string STCApplied;
    public string BalanceReceived;
    public string Witholding;
    public string BalanceVerified;
    public string InvoicePaid;
    public string InstallerNotifiedMeter;
    public string CustNotifiedMeter;
    public string MeterElectrician;
    public string MeterInstallerFU;
    public string MeterInstallerNotes;
    public string MeterJobBooked;
    public string MeterCompleted;
    public string MeterInvoice;
    public string MeterInvoiceNo;
    public string InstallerInvNo;
    public string InstallerInvAmnt;
    public string InstallerInvDate;
    public string InstallerPayDate;
    public string MeterElecInvNo;
    public string MeterElecInvAmnt;
    public string MeterElecInvDate;
    public string MeterElecPayDate;
    public string MeterElecFollowUp;
    public string ElectricianInvoiceNotes;
    public string upsize_ts;
    public string SQ;
    public string MP;
    public string EB;
    public string PD;
    public string InvoiceStatusID;
    public string ProjectEnteredBy;
    public string UpdatedBy;
    public string DepositeOption;
    public string ST;
    public string CE;
    public string StatusComment;
    public string IsDeduct;
    public string StockDeductBy;
    public string StockDeductDate;
    public string PaymentTypeID;
    public string ApplicationDate;
    public string AppliedBy;
    public string PurchaseNo;
    public string DocumentSentDate;
    public string DocumentSentBy;
    public string DocumentReceivedDate;
    public string DocumentReceivedBy;
    public string DocumentVerified;
    public string SentBy;
    public string SentDate;
    public string PostalTrackingNumber;
    public string Remarks;
    public string ReceivedDate;
    public string ReceivedBy;
    public string PaymentVerified;
    public string InvDoc;
    public string InvDocDoor;
    public string PreExtraWork;
    public string PreAmount;
    public string PreApprovedBy;
    public string PaymentReceipt;
    public string PR;
    public string ActiveDate;
    public string SalesComm;
    public string STCFormSign;
    public string SerialNumbers;
    public string QuotationForm;
    public string CustomerAck;
    public string ComplianceCerti;
    public string CustomerAccept;
    public string EWRNumber;
    public string PatmentMethod;
    public string FlatPanels;
    public string PitchedPanels;
    public string SurveyCerti;
    public string ElecDistAppDate;
    public string ElecDistAppBy;
    public string InstInvDoc;
    public string CertiApprove;
    public string NewDate;
    public string NewNotes;
    public string IsFormBay;
    public string financewith;
    public string inverterqty;
    public string inverterqty2;
    public string inverterqty3;
    public string StockCategoryID;
    public string StockItemID;

    public string ElecDistributor;
    public string ElecRetailer;
    public string HouseType;
    public string RoofType;
    public string RoofAngle;
    public string PanelBrandName;
    public string InverterDetailsName;
    public string SecondInverterDetails;
    public string Contact;
    public string InvoiceStatus;
    public string SalesRepName;
    public string InstallerName;
    public string DesignerName;
    public string ElectricianName;
    public string FinanceWith;
    public string PaymentType;
    public string StoreName;
    public string Customer;
    public string FormbayId;
    public string unit_type;
    public string unit_number;
    public string street_number;
    public string street_address;
    public string street_name;
    public string street_type;
    public string street_suffix;
    public string mtcepaperwork;
    public string LinkProjectID;
    public string FormbayInstallBookingDate;
    public string beatquote;
    public string beatquotedoc;
    public string nearmap;
    public string nearmapdoc;
    public string SalesType;
    public string projecttype;
    public string projectcancel;
    public string PVDStatus;
    public string DocumentSentByName;
    public string DocumentReceivedByName;
    public string Empname1;
    public string FDA;
    public string readyactive;
    public string notes;
    public string paydate;
    public string SSCompleteDate;
    public string SSActiveDate;
    public string QuickForm;
    public string IsClickCustomer;
    public string HouseStayID;
    public string HouseStayDate;
    public string ComplainceCertificate;
    public string quickformGuid;
    public string GreenBotFlag;
    public string VicAppReference;
    public string VicDate;
    public string VicRebateNote;
    public string VicRebate;
    public string IsSMReady;
    public string STcYear;
    public string panelQty;
    public string InvertQty1;
    public string InvertQty2;
    public string InvertQty3;
    public string nearmapdoc1;
    public string nearmapdoc2;
    public string InstallEndDate;
    public string Expirydate;
    public string PdfFileNaame;
    public string mgrrdisc;
    public string TiltkitBrackets;
    public string OldSystemRemovalCost;
    public string DOB;
    public string DepositReceivedNew;
    public string varOtherBasicCost;
    public string varSpecialDiscountNotes;
    public string varManagerDiscountNotes;
    public string varOtherBasicCostNotes;

    //Old Panel Details
    public string oldPanelBrandId;
    public string oldPanelBrand;
    public string oldPanelModel;
    public string oldPanelOutput;
    public string oldNoOfPanels;
    public string oldSystemCapKW;

    public string oldPanelBrandManual;
    public string oldPanelModelManual;
    public string oldPanelOutputManual;
    public string oldNoOfPanelsManual;
    public string oldSystemCapKWManual;
    public string OldRemoveOldSystem;

    public string lastActiveEmp;

    //Retailer Details
    public string ExportDetail;
    public string KwExport;
    public string KwNonExport;
    public string GridConnectedSystem;
    public string FeedInTariff;
    public string ApproxExpectedPaybackPeriod;
}

public struct sttblprojectqtypanelinverter
{
    public string StockQuantitypanel;
    public string StockQuantityinverter;
}

public struct SttblProjects2
{
    public string Project2ID;
    public string ProjectLinkID;
    public string ProjectIncomplete;
    public string NoDocs;
    public string DocsDone;
    public string DocsOK;
    public string Promo1;
    public string Promo2;
    public string Promo3;
    public string PromoText;
    public string Promo1ID;
    public string Promo2ID;
    public string SPAInvoiceNumber;
    public string SPAPaid;
    public string SPAPaidBy;
    public string SPAPaidAmount;
    public string SPAPaidMethod;
    public string FirstDepositDate;
    public string InstallerNotesChanged;
    public string InstallerNotesChangedBy;
    public string FinanceWithDepositID;
    public string FinanceWithDeposit;
    public string SalesInvNo;
    public string SalesInvAmnt;
    public string SalesInvDate;
    public string SalesPayDate;
    public string SalesPayNotes;
    public string SalesPayFollowUp;
    public string ActiveCancel;
    public string SalesInvoiceDoc;
    public string SalesLandingPrice;
    public string ProjectInCompReason;
    public string SalesMarketPrice;
    public string UserCommision;
    public string EuroCommision;

    public string PromoOffer1;
    public string PromoOffer2;
    public string InspectorName;
    public string InspectionDate;
    


}

public struct SttblProjectQuotes
{
    public string ProjectQuoteID;
    public string ProjectID;
    public string ProjectNumber;
    public string ProjectQuoteDate;
    public string ProjectQuoteDoc;
    public string EmployeeID;
    public string QuoteDoc;

}

public class ClstblProjects
{

    public static bool tbl_projects_update_IsImportToExcel_sec(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projects_update_IsImportToExcel_sec";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_projects_update_IsImportToExcel_zero()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projects_update_IsImportToExcel_zero";


        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateHouseStays(string ProjectID, String HouseStayID, String HouseStayDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateHouseStays";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseStayID";
        if (HouseStayID != string.Empty)
            param.Value = HouseStayID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int16;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@HouseStayDate";
        if (HouseStayDate != string.Empty)
            param.Value = HouseStayDate;
        else
            param.Value = DBNull.Value;
        //param.Value = HouseStayDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_HouseStatys_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_HouseStatys_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_Selectinverterqty(string projectnumber, string loc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Selectinverterqty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectnumber";
        param.Value = projectnumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@loc";
        param.Value = loc;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectsGetDataBySearchquickformIsImportToExcel(string projectno, string installerid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataBySearchquickformIsImportToExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectno";
        if (projectno != string.Empty)
            param.Value = projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerid";//readyactive
        if (installerid != string.Empty)
            param.Value = installerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static bool tbl_projects_update_IsImportToExcel(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projects_update_IsImportToExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_projects_update_IsExport_zero()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projects_update_IsExport_zero";


        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tbl_projects_update_IsExport_zeroByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projects_update_IsExport_zeroByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblProjectsGetDataBySearchquickform(string projectno, string installerid, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataBySearchquickform";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectno";
        if (projectno != string.Empty)
            param.Value = projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerid";//readyactive
        if (installerid != string.Empty)
            param.Value = installerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (!string.IsNullOrEmpty(startdate))
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Date;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (!string.IsNullOrEmpty(enddate))
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Date;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblProjectsGetDataBySearchquickformIsImportToExcelDone(string projectno, string installerid, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataBySearchquickformIsImportToExcelDone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectno";
        if (projectno != string.Empty)
            param.Value = projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerid";//readyactive
        if (installerid != string.Empty)
            param.Value = installerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (!string.IsNullOrEmpty(startdate))
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Date;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (!string.IsNullOrEmpty(enddate))
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Date;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static bool tblProjects_update_IsQuickForm(string QuickForm, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_update_IsQuickForm";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuickForm";
        param.Value = QuickForm;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_update_IsGreenBot(string GreenBotFlag, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_update_IsGreenBot";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GreenBotFlag";
        param.Value = GreenBotFlag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_BridgeSelectFlag(string Flag, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_BridgeSelectFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Flag";
        param.Value = Flag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatePostInstMeterDetail(string ProjectID, String MeterAppliedTime, String MeterAppliedRef)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePostInstMeterDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedTime";
        param.Value = MeterAppliedTime;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedRef";
        param.Value = MeterAppliedRef;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static SttblProjects tblProjects_SelectByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjects details = new SttblProjects();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.ProjectTypeID = dr["ProjectTypeID"].ToString();
            details.ProjectStatusID = dr["ProjectStatusID"].ToString();
            details.ProjectCancelID = dr["ProjectCancelID"].ToString();
            details.ProjectOnHoldID = dr["ProjectOnHoldID"].ToString();
            details.ProjectOpened = dr["ProjectOpened"].ToString();
            details.ProjectCancelled = dr["ProjectCancelled"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.OldProjectNumber = dr["OldProjectNumber"].ToString();
            details.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
            details.Project = dr["Project"].ToString();
            details.readyactive = dr["readyactive"].ToString();
            details.NextMaintenanceCall = dr["NextMaintenanceCall"].ToString();
            details.InstallAddress = dr["InstallAddress"].ToString();
            details.InstallCity = dr["InstallCity"].ToString();
            details.InstallState = dr["InstallState"].ToString();
            details.InstallPostCode = dr["InstallPostCode"].ToString();
            details.ProjectNotes = dr["ProjectNotes"].ToString();
            details.AdditionalSystem = dr["AdditionalSystem"].ToString();
            details.GridConnected = dr["GridConnected"].ToString();
            details.RebateApproved = dr["RebateApproved"].ToString();
            details.ReceivedCredits = dr["ReceivedCredits"].ToString();
            details.CreditEligible = dr["CreditEligible"].ToString();
            details.MoreThanOneInstall = dr["MoreThanOneInstall"].ToString();
            details.RequiredCompliancePaperwork = dr["RequiredCompliancePaperwork"].ToString();
            details.OutOfPocketDocs = dr["OutOfPocketDocs"].ToString();
            details.OwnerGSTRegistered = dr["OwnerGSTRegistered"].ToString();
            details.OwnerABN = dr["OwnerABN"].ToString();
            details.HouseTypeID = dr["HouseTypeID"].ToString();
            details.RoofTypeID = dr["RoofTypeID"].ToString();
            details.RoofAngleID = dr["RoofAngleID"].ToString();
            details.InstallBase = dr["InstallBase"].ToString();
            details.StockAllocationStore = dr["StockAllocationStore"].ToString();
            details.StandardPack = dr["StandardPack"].ToString();
            details.PanelBrandID = dr["PanelBrandID"].ToString();
            details.PanelBrand = dr["PanelBrand"].ToString();
            details.PanelModel = dr["PanelModel"].ToString();
            details.PanelOutput = dr["PanelOutput"].ToString();
            details.PanelDetails = dr["PanelDetails"].ToString();
            details.InverterDetailsID = dr["InverterDetailsID"].ToString();
            details.SecondInverterDetailsID = dr["SecondInverterDetailsID"].ToString();
            details.ThirdInverterDetailsID = dr["ThirdInverterDetailsID"].ToString();
            details.InverterBrand = dr["InverterBrand"].ToString();
            details.InverterModel = dr["InverterModel"].ToString();
            details.InverterSeries = dr["InverterSeries"].ToString();
            details.InverterOutput = dr["InverterOutput"].ToString();
            details.SecondInverterOutput = dr["SecondInverterOutput"].ToString();
            details.ThirdInverterOutput = dr["ThirdInverterOutput"].ToString();
            details.TotalInverterOutput = dr["TotalInverterOutput"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.InverterDetails = dr["InverterDetails"].ToString();
            details.SystemDetails = dr["SystemDetails"].ToString();
            details.NumberPanels = dr["NumberPanels"].ToString();
            details.PreviousNumberPanels = dr["PreviousNumberPanels"].ToString();
            details.PanelConfigNW = dr["PanelConfigNW"].ToString();
            details.PanelConfigOTH = dr["PanelConfigOTH"].ToString();
            details.SystemCapKW = dr["SystemCapKW"].ToString();
            details.STCMultiplier = dr["STCMultiplier"].ToString();
            details.STCZoneRating = dr["STCZoneRating"].ToString();
            details.STCNumber = dr["STCNumber"].ToString();
            details.STCValue = dr["STCValue"].ToString();
            details.ElecRetailerID = dr["ElecRetailerID"].ToString();
            details.ElecDistributorID = dr["ElecDistributorID"].ToString();
            details.ElecDistApplied = dr["ElecDistApplied"].ToString();
            details.ElecDistApplyMethod = dr["ElecDistApplyMethod"].ToString();
            details.ElecDistApplyBy = dr["ElecDistApplyBy"].ToString();
            details.ElecDistApplySentFrom = dr["ElecDistApplySentFrom"].ToString();
            details.ElecDistApproved = dr["ElecDistApproved"].ToString();
            details.ElecDistApprovelRef = dr["ElecDistApprovelRef"].ToString();
            details.ElecDistOK = dr["ElecDistOK"].ToString();
            details.RegPlanNo = dr["RegPlanNo"].ToString();
            details.LotNumber = dr["LotNumber"].ToString();
            details.Asbestoss = dr["Asbestoss"].ToString();
            details.MeterUG = dr["MeterUG"].ToString();
            details.MeterEnoughSpace = dr["MeterEnoughSpace"].ToString();
            details.SplitSystem = dr["SplitSystem"].ToString();
            details.CherryPicker = dr["CherryPicker"].ToString();
            details.TravelTime = dr["TravelTime"].ToString();
            details.VariationOther = dr["VariationOther"].ToString();
            details.VarRoofType = dr["VarRoofType"].ToString();
            details.VarRoofAngle = dr["VarRoofAngle"].ToString();
            details.VarHouseType = dr["VarHouseType"].ToString();
            details.VarAsbestos = dr["VarAsbestos"].ToString();
            details.VarMeterInstallation = dr["VarMeterInstallation"].ToString();
            details.VarMeterUG = dr["VarMeterUG"].ToString();
            details.VarTravelTime = dr["VarTravelTime"].ToString();
            details.HotWaterMeter = dr["HotWaterMeter"].ToString();
            details.SmartMeter = dr["SmartMeter"].ToString();
            details.VarSplitSystem = dr["VarSplitSystem"].ToString();
            details.VarEnoughSpace = dr["VarEnoughSpace"].ToString();
            details.VarCherryPicker = dr["VarCherryPicker"].ToString();
            details.VarOther = dr["VarOther"].ToString();
            details.SpecialDiscount = dr["SpecialDiscount"].ToString();
            details.collectioncharge = dr["collectioncharge"].ToString();

            details.DepositRequired = dr["DepositRequired"].ToString();
            details.TotalQuotePrice = dr["TotalQuotePrice"].ToString();
            details.PreviousTotalQuotePrice = dr["PreviousTotalQuotePrice"].ToString();
            details.InvoiceExGST = dr["InvoiceExGST"].ToString();
            details.InvoiceGST = dr["InvoiceGST"].ToString();
            details.BalanceGST = dr["BalanceGST"].ToString();
            details.FinanceWithID = dr["FinanceWithID"].ToString();
            details.ServiceValue = dr["ServiceValue"].ToString();
            details.QuoteSent = dr["QuoteSent"].ToString();
            details.QuoteSentNo = dr["QuoteSentNo"].ToString();
            details.QuoteAccepted = dr["QuoteAccepted"].ToString();
            details.SignedQuote = dr["SignedQuote"].ToString();
            details.MeterBoxPhotosSaved = dr["MeterBoxPhotosSaved"].ToString();
            details.ElecBillSaved = dr["ElecBillSaved"].ToString();
            details.ProposedDesignSaved = dr["ProposedDesignSaved"].ToString();
            details.FollowUp = dr["FollowUp"].ToString();
            details.FollowUpNote = dr["FollowUpNote"].ToString();
            details.InvoiceNumber = dr["InvoiceNumber"].ToString();
            details.InvoiceTag = dr["InvoiceTag"].ToString();
            details.InvoiceDoc = dr["InvoiceDoc"].ToString();
            details.InvoiceDetail = dr["InvoiceDetail"].ToString();
            details.InvoiceSent = dr["InvoiceSent"].ToString();
            details.InvoiceFU = dr["InvoiceFU"].ToString();
            details.InvoiceNotes = dr["InvoiceNotes"].ToString();
            details.InvRefund = dr["InvRefund"].ToString();
            details.InvRefunded = dr["InvRefunded"].ToString();
            details.InvRefundBy = dr["InvRefundBy"].ToString();
            details.DepositReceived = dr["DepositReceived"].ToString();
            details.DepositAmount = dr["DepositAmount"].ToString();
            details.ReceiptSent = dr["ReceiptSent"].ToString();
            details.SalesCommPaid = dr["SalesCommPaid"].ToString();
            details.InstallBookingDate = dr["InstallBookingDate"].ToString();
            details.MeterIncluded = dr["MeterIncluded"].ToString();
            details.MeterAppliedRef = dr["MeterAppliedRef"].ToString();
            details.MeterAppliedDate = dr["MeterAppliedDate"].ToString();
            details.MeterAppliedTime = dr["MeterAppliedTime"].ToString();
            details.MeterAppliedMethod = dr["MeterAppliedMethod"].ToString();
            details.MeterApprovedDate = dr["MeterApprovedDate"].ToString();
            details.MeterApprovalNo = dr["MeterApprovalNo"].ToString();
            details.MeterPhase = dr["MeterPhase"].ToString();
            details.OffPeak = dr["OffPeak"].ToString();
            details.NMINumber = dr["NMINumber"].ToString();
            details.meterupgrade = dr["meterupgrade"].ToString();
            details.MeterNumber1 = dr["MeterNumber1"].ToString();
            details.MeterNumber2 = dr["MeterNumber2"].ToString();
            details.MeterNumber3 = dr["MeterNumber3"].ToString();
            details.MeterNumber4 = dr["MeterNumber4"].ToString();
            details.MeterFU = dr["MeterFU"].ToString();
            details.MeterNotes = dr["MeterNotes"].ToString();
            details.OldSystemDetails = dr["OldSystemDetails"].ToString();
            details.REXAppliedRef = dr["REXAppliedRef"].ToString();
            details.REXAppliedDate = dr["REXAppliedDate"].ToString();
            details.REXStatusID = dr["REXStatusID"].ToString();
            details.REXApprovalNotes = dr["REXApprovalNotes"].ToString();
            details.REXApprovalFU = dr["REXApprovalFU"].ToString();
            details.STCPrice = dr["STCPrice"].ToString();
            details.RECRebate = dr["RECRebate"].ToString();
            details.BalanceRequested = dr["BalanceRequested"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.InstallAM1 = dr["InstallAM1"].ToString();
            details.InstallPM1 = dr["InstallPM1"].ToString();
            details.InstallAM2 = dr["InstallAM2"].ToString();
            details.InstallPM2 = dr["InstallPM2"].ToString();
            details.InstallDays = dr["InstallDays"].ToString();
            details.STCFormsDone = dr["STCFormsDone"].ToString();
            details.InstallDocsReceived = dr["InstallDocsReceived"].ToString();
            details.InstallerNotified = dr["InstallerNotified"].ToString();
            details.CustNotifiedInstall = dr["CustNotifiedInstall"].ToString();
            details.InstallerFollowUp = dr["InstallerFollowUp"].ToString();
            details.InstallerNotes = dr["InstallerNotes"].ToString();
            details.InstallerDocsSent = dr["InstallerDocsSent"].ToString();
            details.InstallCompleted = dr["InstallCompleted"].ToString();
            details.InstallVerifiedBy = dr["InstallVerifiedBy"].ToString();
            details.WelcomeLetterDone = dr["WelcomeLetterDone"].ToString();
            details.InstallRequestSaved = dr["InstallRequestSaved"].ToString();
            details.PanelSerials = dr["PanelSerials"].ToString();
            details.InverterSerial = dr["InverterSerial"].ToString();
            details.SecondInverterSerial = dr["SecondInverterSerial"].ToString();
            details.CertificateIssued = dr["CertificateIssued"].ToString();
            details.CertificateSaved = dr["CertificateSaved"].ToString();
            details.STCReceivedBy = dr["STCReceivedBy"].ToString();
            details.STCCheckedBy = dr["STCCheckedBy"].ToString();
            details.STCFormSaved = dr["STCFormSaved"].ToString();
            details.STCUploaded = dr["STCUploaded"].ToString();
            details.STCUploadNumber = dr["STCUploadNumber"].ToString();
            details.STCFU = dr["STCFU"].ToString();
            details.STCNotes = dr["STCNotes"].ToString();
            details.InstallationComment = dr["InstallationComment"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.PVDStatusID = dr["PVDStatusID"].ToString();
            details.STCApplied = dr["STCApplied"].ToString();
            details.BalanceReceived = dr["BalanceReceived"].ToString();
            details.Witholding = dr["Witholding"].ToString();
            details.BalanceVerified = dr["BalanceVerified"].ToString();
            details.InvoicePaid = dr["InvoicePaid"].ToString();
            details.InstallerNotifiedMeter = dr["InstallerNotifiedMeter"].ToString();
            details.CustNotifiedMeter = dr["CustNotifiedMeter"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.MeterInstallerFU = dr["MeterInstallerFU"].ToString();
            details.MeterInstallerNotes = dr["MeterInstallerNotes"].ToString();
            details.MeterJobBooked = dr["MeterJobBooked"].ToString();
            details.MeterCompleted = dr["MeterCompleted"].ToString();
            details.MeterInvoice = dr["MeterInvoice"].ToString();
            details.MeterInvoiceNo = dr["MeterInvoiceNo"].ToString();
            details.InstallerInvNo = dr["InstallerInvNo"].ToString();
            details.InstallerInvAmnt = dr["InstallerInvAmnt"].ToString();
            details.InstallerInvDate = dr["InstallerInvDate"].ToString();
            details.InstallerPayDate = dr["InstallerPayDate"].ToString();
            details.MeterElecInvNo = dr["MeterElecInvNo"].ToString();
            details.MeterElecInvAmnt = dr["MeterElecInvAmnt"].ToString();
            details.MeterElecInvDate = dr["MeterElecInvDate"].ToString();
            details.MeterElecPayDate = dr["MeterElecPayDate"].ToString();
            details.MeterElecFollowUp = dr["MeterElecFollowUp"].ToString();
            details.ElectricianInvoiceNotes = dr["ElectricianInvoiceNotes"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.SQ = dr["SQ"].ToString();
            details.MP = dr["MP"].ToString();
            details.EB = dr["EB"].ToString();
            details.PD = dr["PD"].ToString();
            details.InvoiceStatusID = dr["InvoiceStatusID"].ToString();
            details.ProjectEnteredBy = dr["ProjectEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.DepositeOption = dr["DepositeOption"].ToString();
            details.ST = dr["ST"].ToString();
            details.CE = dr["CE"].ToString();
            details.StatusComment = dr["StatusComment"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();
            details.StockDeductBy = dr["StockDeductBy"].ToString();
            details.StockDeductDate = dr["StockDeductDate"].ToString();
            details.PaymentTypeID = dr["PaymentTypeID"].ToString();
            details.ApplicationDate = dr["ApplicationDate"].ToString();
            details.AppliedBy = dr["AppliedBy"].ToString();
            details.PurchaseNo = dr["PurchaseNo"].ToString();
            details.DocumentSentDate = dr["DocumentSentDate"].ToString();
            details.DocumentSentBy = dr["DocumentSentBy"].ToString();
            details.DocumentReceivedDate = dr["DocumentReceivedDate"].ToString();
            details.DocumentReceivedBy = dr["DocumentReceivedBy"].ToString();
            details.DocumentVerified = dr["DocumentVerified"].ToString();
            details.SentBy = dr["SentBy"].ToString();
            details.SentDate = dr["SentDate"].ToString();
            details.PostalTrackingNumber = dr["PostalTrackingNumber"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.ReceivedDate = dr["ReceivedDate"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.PaymentVerified = dr["PaymentVerified"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvDocDoor = dr["InvDocDoor"].ToString();
            details.PreExtraWork = dr["PreExtraWork"].ToString();
            details.PreAmount = dr["PreAmount"].ToString();
            details.PreApprovedBy = dr["PreApprovedBy"].ToString();
            details.PaymentReceipt = dr["PaymentReceipt"].ToString();
            details.PR = dr["PR"].ToString();
            details.ActiveDate = dr["ActiveDate"].ToString();
            details.SalesComm = dr["SalesComm"].ToString();
            details.STCFormSign = dr["STCFormSign"].ToString();
            details.SerialNumbers = dr["SerialNumbers"].ToString();
            details.QuotationForm = dr["QuotationForm"].ToString();
            details.CustomerAck = dr["CustomerAck"].ToString();
            details.ComplianceCerti = dr["ComplianceCerti"].ToString();
            details.CustomerAccept = dr["CustomerAccept"].ToString();
            details.EWRNumber = dr["EWRNumber"].ToString();
            details.PatmentMethod = dr["PatmentMethod"].ToString();
            details.FlatPanels = dr["FlatPanels"].ToString();
            details.PitchedPanels = dr["PitchedPanels"].ToString();
            details.SurveyCerti = dr["SurveyCerti"].ToString();
            details.ElecDistAppDate = dr["ElecDistAppDate"].ToString();
            details.ElecDistAppBy = dr["ElecDistAppBy"].ToString();
            details.InstInvDoc = dr["InstInvDoc"].ToString();
            details.CertiApprove = dr["CertiApprove"].ToString();
            details.NewDate = dr["NewDate"].ToString();
            details.NewNotes = dr["NewNotes"].ToString();
            details.IsFormBay = dr["IsFormBay"].ToString();
            details.financewith = dr["financewith"].ToString();
            details.inverterqty = dr["inverterqty"].ToString();
            details.inverterqty2 = dr["inverterqty2"].ToString();
            details.inverterqty3 = dr["inverterqty3"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();

            details.SecondInverterDetails = dr["SecondInverterDetails"].ToString();
            details.ElecDistributor = dr["ElecDistributor"].ToString();
            details.ElecRetailer = dr["ElecRetailer"].ToString();
            details.HouseType = dr["HouseType"].ToString();
            details.RoofType = dr["RoofType"].ToString();
            details.RoofAngle = dr["RoofAngle"].ToString();
            details.PanelBrandName = dr["PanelBrandName"].ToString();
            details.InverterDetailsName = dr["InverterDetailsName"].ToString();
            details.Contact = dr["Contact"].ToString();
            details.InvoiceStatus = dr["InvoiceStatus"].ToString();
            details.SalesRepName = dr["SalesRepName"].ToString();
            details.InstallerName = dr["InstallerName"].ToString();
            details.DesignerName = dr["DesignerName"].ToString();
            details.ElectricianName = dr["ElectricianName"].ToString();
            details.FinanceWith = dr["FinanceWith"].ToString();
            details.PaymentType = dr["PaymentType"].ToString();
            details.StoreName = dr["StoreName"].ToString();
            details.Customer = dr["Customer"].ToString();

            details.FormbayId = dr["FormbayId"].ToString();
            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_address = dr["street_address"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.street_suffix = dr["street_suffix"].ToString();
            details.mtcepaperwork = dr["mtcepaperwork"].ToString();
            details.LinkProjectID = dr["LinkProjectID"].ToString();
            details.FormbayInstallBookingDate = dr["FormbayInstallBookingDate"].ToString();
            details.beatquote = dr["beatquote"].ToString();
            details.beatquotedoc = dr["beatquotedoc"].ToString();
            details.nearmap = dr["nearmapcheck"].ToString();
            details.nearmapdoc = dr["nearmapdoc"].ToString();
            details.SalesType = dr["SalesType"].ToString();
            details.projecttype = dr["projecttype"].ToString();
            details.projectcancel = dr["projectcancel"].ToString();
            details.PVDStatus = dr["PVDStatus"].ToString();
            details.DocumentSentByName = dr["DocumentSentByName"].ToString();
            details.DocumentReceivedByName = dr["DocumentReceivedByName"].ToString();
            details.Empname1 = dr["Empname1"].ToString();
            details.FDA = dr["FDA"].ToString();
            details.notes = dr["notes"].ToString();
            details.paydate = dr["paydate"].ToString();
            details.SSActiveDate = dr["SSActiveDate"].ToString();
            details.SSCompleteDate = dr["SSCompleteDate"].ToString();
            details.QuickForm = dr["QuickForm"].ToString();
            details.IsClickCustomer = dr["IsClickCustomer"].ToString();


            details.HouseStayDate = dr["HouseStayDate"].ToString();
            details.HouseStayID = dr["HouseStayID"].ToString();
            details.ComplainceCertificate = dr["ComplainceCertificate"].ToString();
            details.quickformGuid = dr["quickformGuid"].ToString();
            details.GreenBotFlag = dr["GreenBotFlag"].ToString();
            details.VicAppReference = dr["VicAppReference"].ToString();
            details.VicDate = dr["VicDate"].ToString();
            details.VicRebateNote = dr["VicRebateNote"].ToString();
            details.VicRebate = dr["VicRebate"].ToString();
            details.IsSMReady = dr["IsSMReady"].ToString();
            details.STcYear = dr["Stcrate"].ToString();
            details.panelQty = dr["panelQty"].ToString();
            details.InvertQty1 = dr["InvertQty1"].ToString();
            details.InvertQty2 = dr["InvertQty2"].ToString();
            details.InvertQty3 = dr["InvertQty3"].ToString();
            details.nearmapdoc1 = dr["nearmapdoc1"].ToString();
            details.nearmapdoc2 = dr["nearmapdoc2"].ToString();
            details.InstallEndDate = dr["InstallEndDate"].ToString();
            details.mgrrdisc = dr["mgrrdisc"].ToString();
            details.PdfFileNaame = dr["nearmappdf"].ToString();
            details.EmailFlag = dr["EmailFlag"].ToString();
            details.PriceCat = dr["PriceCat"].ToString();
            details.OldSystemRemovalCost = dr["oldsystemremovalcost"].ToString();
            details.TiltkitBrackets = dr["tilikitbrackets"].ToString();
            details.DOB = dr["DOB"].ToString();
            details.DepositReceivedNew = dr["DepositReceivedNew"].ToString();
            details.varOtherBasicCost = dr["varOtherBasicCost"].ToString();
            details.varSpecialDiscountNotes = dr["varSpecialDiscountNotes"].ToString();
            details.varManagerDiscountNotes = dr["varManagerDiscountNotes"].ToString();
            details.varOtherBasicCostNotes = dr["varOtherBasicCostNotes"].ToString();

            details.oldPanelBrandId = dr["oldPanelBrandId"].ToString();
            details.oldPanelBrand = dr["oldPanelBrand"].ToString();
            details.oldPanelModel = dr["oldPanelModel"].ToString();
            details.oldPanelOutput = dr["oldPanelOutput"].ToString();
            details.oldNoOfPanels = dr["oldNoOfPanels"].ToString();
            details.oldSystemCapKW = dr["oldSystemCapKW"].ToString();

            details.oldPanelBrandManual = dr["oldPanelBrandManual"].ToString();
            details.oldPanelModelManual = dr["oldPanelModelManual"].ToString();
            details.oldPanelOutputManual = dr["oldPanelOutputManual"].ToString();
            details.oldNoOfPanelsManual = dr["oldNoOfPanelsManual"].ToString();
            details.oldSystemCapKWManual = dr["oldSystemCapKWManual"].ToString();
            details.OldRemoveOldSystem = dr["OldRemoveOldSystem"].ToString();

            details.lastActiveEmp = dr["lastActiveEmp"].ToString();

            details.ExportDetail = dr["ExportDetail"].ToString();
            details.KwExport = dr["KwExport"].ToString();
            details.KwNonExport = dr["KwNonExport"].ToString();
            details.GridConnectedSystem = dr["GridConnectedSystem"].ToString();
            details.FeedInTariff = dr["FeedInTariff"].ToString();
            details.ApproxExpectedPaybackPeriod = dr["ApproxExpectedPaybackPeriod"].ToString();
        }
        // return structure details
        return details;
    }



    public static sttblprojectqtypanelinverter tblProjects_SelectByProjectqty(string ProjectID, string LocationId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectqty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectnumber";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@loc";
        if (LocationId != string.Empty)
            param.Value = LocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        sttblprojectqtypanelinverter details = new sttblprojectqtypanelinverter();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockQuantitypanel = dr["StockQuantity"].ToString();

        }
        // return structure details
        return details;
    }

    public static sttblprojectqtypanelinverter tblProjects_SelectByProjectinverterqty(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectinverterqty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectnumber";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        sttblprojectqtypanelinverter details = new sttblprojectqtypanelinverter();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockQuantityinverter = dr["StockQuantity"].ToString();

        }
        // return structure details
        return details;
    }

    public static sttblprojectqtypanelinverter tblProjects_SelectByProjectqty_ByLocation(string ProjectID, string companylocationid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectqty_ByLocation";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectnumber";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@companylocationid";
        param.Value = companylocationid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        sttblprojectqtypanelinverter details = new sttblprojectqtypanelinverter();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockQuantitypanel = dr["StockQuantity"].ToString();

        }
        // return structure details
        return details;
    }

    public static sttblprojectqtypanelinverter tblProjects_SelectByProjectinverterqty_ByLocation(string ProjectID, string companylocationid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectinverterqty_ByLocation";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectnumber";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@companylocationid";
        param.Value = companylocationid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        sttblprojectqtypanelinverter details = new sttblprojectqtypanelinverter();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockQuantityinverter = dr["StockQuantity"].ToString();

        }
        // return structure details
        return details;
    }

    public static SttblProjects2 tblProjects2_SelectByProjectID(String ProjectLinkID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        SttblProjects2 details = new SttblProjects2();
        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.Project2ID = dr["Project2ID"].ToString();
            details.ProjectLinkID = dr["ProjectLinkID"].ToString();
            details.ProjectIncomplete = dr["ProjectIncomplete"].ToString();
            details.NoDocs = dr["NoDocs"].ToString();
            details.DocsDone = dr["DocsDone"].ToString();
            details.DocsOK = dr["DocsOK"].ToString();
            details.Promo1 = dr["Promo1"].ToString();
            details.Promo2 = dr["Promo2"].ToString();
            details.Promo3 = dr["Promo3"].ToString();
            details.PromoText = dr["PromoText"].ToString();
            details.Promo1ID = dr["Promo1ID"].ToString();
            details.Promo2ID = dr["Promo2ID"].ToString();
            details.SPAInvoiceNumber = dr["SPAInvoiceNumber"].ToString();
            details.SPAPaid = dr["SPAPaid"].ToString();
            details.SPAPaidBy = dr["SPAPaidBy"].ToString();
            details.SPAPaidAmount = dr["SPAPaidAmount"].ToString();
            details.SPAPaidMethod = dr["SPAPaidMethod"].ToString();
            details.FirstDepositDate = dr["FirstDepositDate"].ToString();
            details.InstallerNotesChanged = dr["InstallerNotesChanged"].ToString();
            details.InstallerNotesChangedBy = dr["InstallerNotesChangedBy"].ToString();
            details.FinanceWithDepositID = dr["FinanceWithDepositID"].ToString();
            details.FinanceWithDeposit = dr["FinanceWithDeposit"].ToString();
            details.SalesInvNo = dr["SalesInvNo"].ToString();
            details.SalesInvAmnt = dr["SalesInvAmnt"].ToString();
            details.SalesInvDate = dr["SalesInvDate"].ToString();
            details.SalesPayDate = dr["SalesPayDate"].ToString();
            details.SalesPayNotes = dr["SalesPayNotes"].ToString();
            details.SalesPayFollowUp = dr["SalesPayFollowUp"].ToString();
            details.ActiveCancel = dr["ActiveCancel"].ToString();
            details.SalesInvoiceDoc = dr["SalesInvoiceDoc"].ToString();
            details.SalesLandingPrice = dr["SalesLandingPrice"].ToString();
            details.ProjectInCompReason = dr["ProjectInCompReason"].ToString();
            details.SalesMarketPrice = dr["SalesMarketPrice"].ToString();
            details.EuroCommision = dr["EuroCommision"].ToString();
            details.UserCommision = dr["UserCommision"].ToString();

            details.PromoOffer1 = dr["PromoOffer1"].ToString();
            details.PromoOffer2 = dr["PromoOffer2"].ToString();

            details.InspectorName = dr["InspectorName"].ToString();
            details.InspectionDate = dr["InspectionDate"].ToString();

        }
        return details;
    }

    public static DataTable tblprojects_outstanding(string startdate, string enddate, string FinanceWithID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojects_outstanding";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        param.Value = startdate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        param.Value = enddate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        param.Value = FinanceWithID;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectGuid_ApprovedSTCStatus(string InstallBookingDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectGuid_ApprovedSTCStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        param.Value = InstallBookingDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectMove(string PanelBrandID, string InverterDetailsID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectMove";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PanelBrandID";
        param.Value = PanelBrandID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetailsID";
        param.Value = InverterDetailsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByPId(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByPId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectSTCTracker()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectSTCTracker";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectSTCTrackerSearch(string Contact, string ProjectNumber, string ManualQuoteNumber, string STCCheckedBy, string InstallCity, string InstallState, string InstallPostCode, string PVDNumber, string STCUploadNumber, string PVDStatusID, string datetype, string startdate, string enddate, string noPVD, string formbay, string ProjectStatusID, string SerialNo, string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectSTCTrackerSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCCheckedBy";
        if (STCCheckedBy != string.Empty)
            param.Value = STCCheckedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploadNumber";
        if (STCUploadNumber != string.Empty)
            param.Value = STCUploadNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@noPVD";
        if (noPVD != string.Empty)
            param.Value = noPVD;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbay";
        if (formbay != string.Empty)
            param.Value = formbay;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByEmployeeid(string employeeid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByEmployeeid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectInvoiceIssued()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectInvoiceIssued";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_getdateByInvoiceIssuedSearch(string InvoiceNumber, string SysInstaller, string MeterInstaller, string ManualQuoteNumber, string InvoiceStatusID, string InstallCity, string Contact, string ProjectStatusID, string FinanceWithID, string InstallPostCodeFrom, string InstallState, string startdate, string enddate, string DateType, string CompanyNumber, string InstallPostCodeTo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_getdateByInvoiceIssuedSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SysInstaller";
        if (SysInstaller != string.Empty)
            param.Value = SysInstaller;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstaller";
        if (MeterInstaller != string.Empty)
            param.Value = MeterInstaller;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceStatusID";
        if (InvoiceStatusID != string.Empty)
            param.Value = InvoiceStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_getdateByInvoiceIssuedSearchCount(string InvoiceNumber, string SysInstaller, string MeterInstaller, string ManualQuoteNumber, string InvoiceStatusID, string InstallCity, string Contact, string ProjectStatusID, string FinanceWithID, string InstallPostCodeFrom, string InstallState, string startdate, string enddate, string DateType, string CompanyNumber, string InstallPostCodeTo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_getdateByInvoiceIssuedSearchCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SysInstaller";
        if (SysInstaller != string.Empty)
            param.Value = SysInstaller;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstaller";
        if (MeterInstaller != string.Empty)
            param.Value = MeterInstaller;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceStatusID";
        if (InvoiceStatusID != string.Empty)
            param.Value = InvoiceStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_Select_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Select_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblProjectsGetDataBySearch(string employeeid, string ProjectTypeID, string ProjectStatusID, string ProjectNumber, string ManualQuoteNumber, string alpha, string ProjectCancelID, string InstallAddress, string InstallCity, string InstallState, string CustSourceID, string SalesTeamID, string CompanyNumber, string PanelModel, string InverterModel, string startdate, string enddate, string Client, string CustSourceSubID, string InstallPostCodeFrom, string InstallPostCodeTo, string SystemCapKWFrom, string SystemCapKWTo, string EmpSearch, string datetype, string noinstdate, string Promo1ID, string Promo2ID, string FinanceWithID, string ElecDistApprovelRef, string PurchaseNo, string DistApprovedDate, string meterupgrade, string readyactive, string RoofType, string HouseType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWFrom";
        if (SystemCapKWFrom != string.Empty)
            param.Value = SystemCapKWFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWTo";
        if (SystemCapKWTo != string.Empty)
            param.Value = SystemCapKWTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpSearch";
        if (EmpSearch != string.Empty)
            param.Value = EmpSearch;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@noinstdate";
        if (noinstdate != string.Empty)
            param.Value = noinstdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo1ID";
        if (Promo1ID != string.Empty)
            param.Value = Promo1ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo2ID";
        if (Promo2ID != string.Empty)
            param.Value = Promo2ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseNo";
        if (PurchaseNo != string.Empty)
            param.Value = PurchaseNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistApprovedDate";
        if (DistApprovedDate != string.Empty)
            param.Value = DistApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@meterupgrade";
        if (meterupgrade != string.Empty)
            param.Value = meterupgrade;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";//readyactive
        if (readyactive != string.Empty)
            param.Value = readyactive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        if (RoofType != string.Empty)
            param.Value = RoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        if (HouseType != string.Empty)
            param.Value = HouseType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectsGetDataBySearchForExcel2(string employeeid, string ProjectTypeID, string ProjectStatusID, string ProjectNumber, string ManualQuoteNumber, string alpha, string ProjectCancelID, string InstallAddress, string InstallCity, string InstallState, string CustSourceID, string SalesTeamID, string CompanyNumber, string PanelModel, string InverterModel, string startdate, string enddate, string Client, string CustSourceSubID, string InstallPostCodeFrom, string InstallPostCodeTo, string SystemCapKWFrom, string SystemCapKWTo, string EmpSearch, string datetype, string noinstdate, string Promo1ID, string Promo2ID, string FinanceWithID, string ElecDistApprovelRef, string PurchaseNo, string DistApprovedDate, string meterupgrade, string readyactive)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataBySearchForExcel2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWFrom";
        if (SystemCapKWFrom != string.Empty)
            param.Value = SystemCapKWFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWTo";
        if (SystemCapKWTo != string.Empty)
            param.Value = SystemCapKWTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpSearch";
        if (EmpSearch != string.Empty)
            param.Value = EmpSearch;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@noinstdate";
        if (noinstdate != string.Empty)
            param.Value = noinstdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo1ID";
        if (Promo1ID != string.Empty)
            param.Value = Promo1ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo2ID";
        if (Promo2ID != string.Empty)
            param.Value = Promo2ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseNo";
        if (PurchaseNo != string.Empty)
            param.Value = PurchaseNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistApprovedDate";
        if (DistApprovedDate != string.Empty)
            param.Value = DistApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@meterupgrade";
        if (meterupgrade != string.Empty)
            param.Value = meterupgrade;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";//readyactive
        if (readyactive != string.Empty)
            param.Value = readyactive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectsGetDataBySearchCount(string employeeid, string ProjectTypeID, string ProjectStatusID, string ProjectNumber, string ManualQuoteNumber, string alpha, string ProjectCancelID, string InstallAddress, string InstallCity, string InstallState, string CustSourceID, string SalesTeamID, string CompanyNumber, string PanelModel, string InverterModel, string startdate, string enddate, string Client, string CustSourceSubID, string InstallPostCodeFrom, string InstallPostCodeTo, string SystemCapKWFrom, string SystemCapKWTo, string EmpSearch, string datetype, string noinstdate, string Promo1ID, string Promo2ID, string FinanceWithID, string DistApprovedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataBySearchCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@noinstdate";
        if (noinstdate != string.Empty)
            param.Value = noinstdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWFrom";
        if (SystemCapKWFrom != string.Empty)
            param.Value = SystemCapKWFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWTo";
        if (SystemCapKWTo != string.Empty)
            param.Value = SystemCapKWTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpSearch";
        if (EmpSearch != string.Empty)
            param.Value = EmpSearch;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        param.Value = datetype;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo1ID";
        if (Promo1ID != string.Empty)
            param.Value = Promo1ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo2ID";
        if (Promo2ID != string.Empty)
            param.Value = Promo2ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistApprovedDate";
        if (DistApprovedDate != string.Empty)
            param.Value = DistApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblProjects_InstallBookingTracker(string employeeid, string Client, string ProjectNumber, string ManualQuoteNumber, string InstallAddress, string InstallCity, string InstallState, string InstallPostCodeFrom, string InstallPostCodeTo, string ProjectStatusID, string ProjectTypeID, string PanelModel, string InverterModel, string SystemCapKWFrom, string SystemCapKWTo, string FinanceWithID, string ElecDistAppDate, string datetype, string startdate, string enddate, String PurchaseNo, String isHistoric)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_InstallBookingTracker";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWFrom";
        if (SystemCapKWFrom != string.Empty)
            param.Value = SystemCapKWFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWTo";
        if (SystemCapKWTo != string.Empty)
            param.Value = SystemCapKWTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistAppDate";
        if (ElecDistAppDate != string.Empty)
            param.Value = ElecDistAppDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseNo";
        if (PurchaseNo != string.Empty)
            param.Value = PurchaseNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@isHistoric";
        if (isHistoric != string.Empty)
            param.Value = isHistoric;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_InstallBookingTracker1(string employeeid, string Client, string ProjectNumber, string ManualQuoteNumber, string InstallAddress, string InstallCity, string InstallState, string InstallPostCodeFrom, string InstallPostCodeTo, string ProjectStatusID, string ProjectTypeID, string PanelModel, string InverterModel, string SystemCapKWFrom, string SystemCapKWTo, string FinanceWithID, string ElecDistAppDate, string datetype, string startdate, string enddate, String PurchaseNo, String isHistoric, string rooftypeid, string housetypeid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_InstallBookingTrackerNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWFrom";
        if (SystemCapKWFrom != string.Empty)
            param.Value = SystemCapKWFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWTo";
        if (SystemCapKWTo != string.Empty)
            param.Value = SystemCapKWTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistAppDate";
        if (ElecDistAppDate != string.Empty)
            param.Value = ElecDistAppDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseNo";
        if (PurchaseNo != string.Empty)
            param.Value = PurchaseNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@isHistoric";
        if (isHistoric != string.Empty)
            param.Value = isHistoric;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@RoofTypeID";
        //if (rooftypeid != string.Empty)
        //    param.Value = rooftypeid;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@HouseTypeID";
        //if (housetypeid != string.Empty)
        //    param.Value = housetypeid;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectsGetDataByContractId(String userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataByContractId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByUCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByUCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_Select_First_ByUCustomerID(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Select_First_ByUCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByUserIdCust(String userid, String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByUserIdCust";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblProjects_Insert(String CustomerID, String ContactID, String EmployeeID, String SalesRep, String ProjectTypeID, String ProjectStatusID, String ProjectCancelID, String ProjectOnHoldID, String ProjectOpened, String ProjectCancelled, String OldProjectNumber, String ManualQuoteNumber, String Project, String NextMaintenanceCall, String InstallAddress, String InstallCity, String InstallState, String InstallPostCode, String ProjectNotes, String AdditionalSystem, String GridConnected, String RebateApproved, String ReceivedCredits, String CreditEligible, String MoreThanOneInstall, String RequiredCompliancePaperwork, String OutOfPocketDocs, String OwnerGSTRegistered, String OwnerABN, String HouseTypeID, String RoofTypeID, String RoofAngleID, String InstallBase, String StockAllocationStore, String StandardPack, String PanelBrandID, String PanelBrand, String PanelModel, String PanelOutput, String PanelDetails, String InverterDetailsID, String SecondInverterDetailsID, String InverterBrand, String InverterModel, String InverterSeries, String InverterOutput, String SecondInverterOutput, String TotalInverterOutput, String InverterCert, String InverterDetails, String SystemDetails, String NumberPanels, String PreviousNumberPanels, String PanelConfigNW, String PanelConfigOTH, String SystemCapKW, String STCMultiplier, String STCZoneRating, String STCNumber, String STCValue, String ElecRetailerID, String ElecDistributorID, String ElecDistApplied, String ElecDistApplyMethod, String ElecDistApplyBy, String ElecDistApplySentFrom, String ElecDistApproved, String ElecDistApprovelRef, String ElecDistOK, String RegPlanNo, String LotNumber, String Asbestoss, String MeterUG, String MeterEnoughSpace, String SplitSystem, String CherryPicker, String TravelTime, String VariationOther, String VarRoofType, String VarRoofAngle, String VarHouseType, String VarAsbestos, String VarMeterInstallation, String VarMeterUG, String VarTravelTime, String VarSplitSystem, String VarEnoughSpace, String VarCherryPicker, String VarOther, String SpecialDiscount, String DepositRequired, String TotalQuotePrice, String PreviousTotalQuotePrice, String InvoiceExGST, String InvoiceGST, String BalanceGST, String FinanceWithID, String ServiceValue, String QuoteSent, String QuoteSentNo, String QuoteAccepted, String SignedQuote, String MeterBoxPhotosSaved, String ElecBillSaved, String ProposedDesignSaved, String FollowUp, String FollowUpNote, String InvoiceNumber, String InvoiceTag, String InvoiceDoc, String InvoiceDetail, String InvoiceSent, String InvoiceFU, String InvoiceNotes, String InvRefund, String InvRefunded, String InvRefundBy, String DepositReceived, String DepositAmount, String ReceiptSent, String SalesCommPaid, String InstallBookingDate, String MeterIncluded, String MeterAppliedRef, String MeterAppliedDate, String MeterAppliedTime, String MeterAppliedMethod, String MeterApprovedDate, String MeterApprovalNo, String MeterPhase, String OffPeak, String NMINumber, String MeterNumber1, String MeterNumber2, String MeterNumber3, String MeterNumber4, String MeterFU, String MeterNotes, String REXAppliedRef, String REXAppliedDate, String REXStatusID, String REXApprovalNotes, String REXApprovalFU, String STCPrice, String RECRebate, String BalanceRequested, String Installer, String Designer, String Electrician, String InstallAM1, String InstallPM1, String InstallAM2, String InstallPM2, String InstallDays, String STCFormsDone, String InstallDocsReceived, String InstallerNotified, String CustNotifiedInstall, String InstallerFollowUp, String InstallerNotes, String InstallerDocsSent, String InstallCompleted, String InstallVerifiedBy, String WelcomeLetterDone, String InstallRequestSaved, String PanelSerials, String InverterSerial, String SecondInverterSerial, String CertificateIssued, String CertificateSaved, String STCReceivedBy, String STCCheckedBy, String STCFormSaved, String STCUploaded, String STCUploadNumber, String STCFU, String STCNotes, String InstallationComment, String PVDNumber, String PVDStatusID, String STCApplied, String BalanceReceived, String Witholding, String BalanceVerified, String InvoicePaid, String InstallerNotifiedMeter, String CustNotifiedMeter, String MeterElectrician, String MeterInstallerFU, String MeterInstallerNotes, String MeterJobBooked, String MeterCompleted, String MeterInvoice, String MeterInvoiceNo, String InstallerInvNo, String InstallerInvAmnt, String InstallerInvDate, String InstallerPayDate, String MeterElecInvNo, String MeterElecInvAmnt, String MeterElecInvDate, String MeterElecPayDate, String MeterElecFollowUp, String ElectricianInvoiceNotes, String upsize_ts, string ProjectEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectOnHoldID";
        if (ProjectOnHoldID != string.Empty)
            param.Value = ProjectOnHoldID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectOpened";
        if (ProjectOpened != string.Empty)
            param.Value = ProjectOpened;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelled";
        if (ProjectCancelled != string.Empty)
            param.Value = ProjectCancelled;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldProjectNumber";
        if (OldProjectNumber != string.Empty)
            param.Value = OldProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Project != string.Empty)
            param.Value = Project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NextMaintenanceCall";
        if (NextMaintenanceCall != string.Empty)
            param.Value = NextMaintenanceCall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNotes";
        if (ProjectNotes != string.Empty)
            param.Value = ProjectNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdditionalSystem";
        if (AdditionalSystem != string.Empty)
            param.Value = AdditionalSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GridConnected";
        if (GridConnected != string.Empty)
            param.Value = GridConnected;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RebateApproved";
        if (RebateApproved != string.Empty)
            param.Value = RebateApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedCredits";
        if (ReceivedCredits != string.Empty)
            param.Value = ReceivedCredits;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreditEligible";
        if (CreditEligible != string.Empty)
            param.Value = CreditEligible;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MoreThanOneInstall";
        if (MoreThanOneInstall != string.Empty)
            param.Value = MoreThanOneInstall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RequiredCompliancePaperwork";
        if (RequiredCompliancePaperwork != string.Empty)
            param.Value = RequiredCompliancePaperwork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OutOfPocketDocs";
        if (OutOfPocketDocs != string.Empty)
            param.Value = OutOfPocketDocs;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OwnerGSTRegistered";
        if (OwnerGSTRegistered != string.Empty)
            param.Value = OwnerGSTRegistered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OwnerABN";
        if (OwnerABN != string.Empty)
            param.Value = OwnerABN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        if (HouseTypeID != string.Empty)
            param.Value = HouseTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        if (RoofTypeID != string.Empty)
            param.Value = RoofTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofAngleID";
        if (RoofAngleID != string.Empty)
            param.Value = RoofAngleID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBase";
        if (InstallBase != string.Empty)
            param.Value = InstallBase;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockAllocationStore";
        if (StockAllocationStore != string.Empty)
            param.Value = StockAllocationStore;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StandardPack";
        if (StandardPack != string.Empty)
            param.Value = StandardPack;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrandID";
        if (PanelBrandID != string.Empty)
            param.Value = PanelBrandID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrand";
        if (PanelBrand != string.Empty)
            param.Value = PanelBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelOutput";
        if (PanelOutput != string.Empty)
            param.Value = PanelOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelDetails";
        if (PanelDetails != string.Empty)
            param.Value = PanelDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetailsID";
        if (InverterDetailsID != string.Empty)
            param.Value = InverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterDetailsID";
        if (SecondInverterDetailsID != string.Empty)
            param.Value = SecondInverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterBrand";
        if (InverterBrand != string.Empty)
            param.Value = InverterBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSeries";
        if (InverterSeries != string.Empty)
            param.Value = InverterSeries;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterOutput";
        if (InverterOutput != string.Empty)
            param.Value = InverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterOutput";
        if (SecondInverterOutput != string.Empty)
            param.Value = SecondInverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@TotalInverterOutput";
        if (TotalInverterOutput != string.Empty)
            param.Value = TotalInverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterCert";
        if (InverterCert != string.Empty)
            param.Value = InverterCert;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetails";
        if (InverterDetails != string.Empty)
            param.Value = InverterDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemDetails";
        if (SystemDetails != string.Empty)
            param.Value = SystemDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NumberPanels";
        if (NumberPanels != string.Empty)
            param.Value = NumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreviousNumberPanels";
        if (PreviousNumberPanels != string.Empty)
            param.Value = PreviousNumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelConfigNW";
        if (PanelConfigNW != string.Empty)
            param.Value = PanelConfigNW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelConfigOTH";
        if (PanelConfigOTH != string.Empty)
            param.Value = PanelConfigOTH;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKW";
        if (SystemCapKW != string.Empty)
            param.Value = SystemCapKW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCMultiplier";
        if (STCMultiplier != string.Empty)
            param.Value = STCMultiplier;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCZoneRating";
        if (STCZoneRating != string.Empty)
            param.Value = STCZoneRating;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumber";
        if (STCNumber != string.Empty)
            param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCValue";
        if (STCValue != string.Empty)
            param.Value = STCValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        if (ElecRetailerID != string.Empty)
            param.Value = ElecRetailerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributorID";
        if (ElecDistributorID != string.Empty)
            param.Value = ElecDistributorID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplied";
        if (ElecDistApplied != string.Empty)
            param.Value = ElecDistApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplyMethod";
        if (ElecDistApplyMethod != string.Empty)
            param.Value = ElecDistApplyMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplyBy";
        if (ElecDistApplyBy != string.Empty)
            param.Value = ElecDistApplyBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplySentFrom";
        if (ElecDistApplySentFrom != string.Empty)
            param.Value = ElecDistApplySentFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApproved";
        if (ElecDistApproved != string.Empty)
            param.Value = ElecDistApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistOK";
        if (ElecDistOK != string.Empty)
            param.Value = ElecDistOK;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RegPlanNo";
        if (RegPlanNo != string.Empty)
            param.Value = RegPlanNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LotNumber";
        if (LotNumber != string.Empty)
            param.Value = LotNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Asbestoss";
        if (Asbestoss != string.Empty)
            param.Value = Asbestoss;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterUG";
        if (MeterUG != string.Empty)
            param.Value = MeterUG;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterEnoughSpace";
        if (MeterEnoughSpace != string.Empty)
            param.Value = MeterEnoughSpace;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SplitSystem";
        if (SplitSystem != string.Empty)
            param.Value = SplitSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CherryPicker";
        if (CherryPicker != string.Empty)
            param.Value = CherryPicker;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TravelTime";
        if (TravelTime != string.Empty)
            param.Value = TravelTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VariationOther";
        if (VariationOther != string.Empty)
            param.Value = VariationOther;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofType";
        if (VarRoofType != string.Empty)
            param.Value = VarRoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofAngle";
        if (VarRoofAngle != string.Empty)
            param.Value = VarRoofAngle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarHouseType";
        if (VarHouseType != string.Empty)
            param.Value = VarHouseType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarAsbestos";
        if (VarAsbestos != string.Empty)
            param.Value = VarAsbestos;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterInstallation";
        if (VarMeterInstallation != string.Empty)
            param.Value = VarMeterInstallation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterUG";
        if (VarMeterUG != string.Empty)
            param.Value = VarMeterUG;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarTravelTime";
        if (VarTravelTime != string.Empty)
            param.Value = VarTravelTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarSplitSystem";
        if (VarSplitSystem != string.Empty)
            param.Value = VarSplitSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarEnoughSpace";
        if (VarEnoughSpace != string.Empty)
            param.Value = VarEnoughSpace;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarCherryPicker";
        if (VarCherryPicker != string.Empty)
            param.Value = VarCherryPicker;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarOther";
        if (VarOther != string.Empty)
            param.Value = VarOther;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SpecialDiscount";
        if (SpecialDiscount != string.Empty)
            param.Value = SpecialDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositRequired";
        if (DepositRequired != string.Empty)
            param.Value = DepositRequired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TotalQuotePrice";
        if (TotalQuotePrice != string.Empty)
            param.Value = TotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreviousTotalQuotePrice";
        if (PreviousTotalQuotePrice != string.Empty)
            param.Value = PreviousTotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceExGST";
        if (InvoiceExGST != string.Empty)
            param.Value = InvoiceExGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceGST";
        if (InvoiceGST != string.Empty)
            param.Value = InvoiceGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceGST";
        if (BalanceGST != string.Empty)
            param.Value = BalanceGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceValue";
        if (ServiceValue != string.Empty)
            param.Value = ServiceValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteSent";
        if (QuoteSent != string.Empty)
            param.Value = QuoteSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteSentNo";
        if (QuoteSentNo != string.Empty)
            param.Value = QuoteSentNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteAccepted";
        if (QuoteAccepted != string.Empty)
            param.Value = QuoteAccepted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignedQuote";
        if (SignedQuote != string.Empty)
            param.Value = SignedQuote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterBoxPhotosSaved";
        if (MeterBoxPhotosSaved != string.Empty)
            param.Value = MeterBoxPhotosSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecBillSaved";
        if (ElecBillSaved != string.Empty)
            param.Value = ElecBillSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProposedDesignSaved";
        if (ProposedDesignSaved != string.Empty)
            param.Value = ProposedDesignSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUp";
        if (FollowUp != string.Empty)
            param.Value = FollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUpNote";
        if (FollowUpNote != string.Empty)
            param.Value = FollowUpNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceTag";
        if (InvoiceTag != string.Empty)
            param.Value = InvoiceTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceDoc";
        if (InvoiceDoc != string.Empty)
            param.Value = InvoiceDoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceDetail";
        if (InvoiceDetail != string.Empty)
            param.Value = InvoiceDetail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceSent";
        if (InvoiceSent != string.Empty)
            param.Value = InvoiceSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceFU";
        if (InvoiceFU != string.Empty)
            param.Value = InvoiceFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNotes";
        if (InvoiceNotes != string.Empty)
            param.Value = InvoiceNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefund";
        if (InvRefund != string.Empty)
            param.Value = InvRefund;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefunded";
        if (InvRefunded != string.Empty)
            param.Value = InvRefunded;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefundBy";
        if (InvRefundBy != string.Empty)
            param.Value = InvRefundBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositReceived";
        if (DepositReceived != string.Empty)
            param.Value = DepositReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositAmount";
        if (DepositAmount != string.Empty)
            param.Value = DepositAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceiptSent";
        if (ReceiptSent != string.Empty)
            param.Value = ReceiptSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesCommPaid";
        if (SalesCommPaid != string.Empty)
            param.Value = SalesCommPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterIncluded";
        if (MeterIncluded != string.Empty)
            param.Value = MeterIncluded;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedRef";
        if (MeterAppliedRef != string.Empty)
            param.Value = MeterAppliedRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedDate";
        if (MeterAppliedDate != string.Empty)
            param.Value = MeterAppliedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedTime";
        if (MeterAppliedTime != string.Empty)
            param.Value = MeterAppliedTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedMethod";
        if (MeterAppliedMethod != string.Empty)
            param.Value = MeterAppliedMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterApprovedDate";
        if (MeterApprovedDate != string.Empty)
            param.Value = MeterApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterApprovalNo";
        if (MeterApprovalNo != string.Empty)
            param.Value = MeterApprovalNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterPhase";
        if (MeterPhase != string.Empty)
            param.Value = MeterPhase;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OffPeak";
        if (OffPeak != string.Empty)
            param.Value = OffPeak;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NMINumber";
        if (NMINumber != string.Empty)
            param.Value = NMINumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber1";
        if (MeterNumber1 != string.Empty)
            param.Value = MeterNumber1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber2";
        if (MeterNumber2 != string.Empty)
            param.Value = MeterNumber2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber3";
        if (MeterNumber3 != string.Empty)
            param.Value = MeterNumber3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber4";
        if (MeterNumber4 != string.Empty)
            param.Value = MeterNumber4;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterFU";
        if (MeterFU != string.Empty)
            param.Value = MeterFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNotes";
        if (MeterNotes != string.Empty)
            param.Value = MeterNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXAppliedRef";
        if (REXAppliedRef != string.Empty)
            param.Value = REXAppliedRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXAppliedDate";
        if (REXAppliedDate != string.Empty)
            param.Value = REXAppliedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXStatusID";
        if (REXStatusID != string.Empty)
            param.Value = REXStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXApprovalNotes";
        if (REXApprovalNotes != string.Empty)
            param.Value = REXApprovalNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXApprovalFU";
        if (REXApprovalFU != string.Empty)
            param.Value = REXApprovalFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCPrice";
        if (STCPrice != string.Empty)
            param.Value = STCPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RECRebate";
        if (RECRebate != string.Empty)
            param.Value = RECRebate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceRequested";
        if (BalanceRequested != string.Empty)
            param.Value = BalanceRequested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM1";
        if (InstallAM1 != string.Empty)
            param.Value = InstallAM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM1";
        if (InstallPM1 != string.Empty)
            param.Value = InstallPM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM2";
        if (InstallAM2 != string.Empty)
            param.Value = InstallAM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM2";
        if (InstallPM2 != string.Empty)
            param.Value = InstallPM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallDays";
        if (InstallDays != string.Empty)
            param.Value = InstallDays;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFormsDone";
        if (STCFormsDone != string.Empty)
            param.Value = STCFormsDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallDocsReceived";
        if (InstallDocsReceived != string.Empty)
            param.Value = InstallDocsReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotified";
        if (InstallerNotified != string.Empty)
            param.Value = InstallerNotified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotifiedInstall";
        if (CustNotifiedInstall != string.Empty)
            param.Value = CustNotifiedInstall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerFollowUp";
        if (InstallerFollowUp != string.Empty)
            param.Value = InstallerFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotes";
        if (InstallerNotes != string.Empty)
            param.Value = InstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerDocsSent";
        if (InstallerDocsSent != string.Empty)
            param.Value = InstallerDocsSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCompleted";
        if (InstallCompleted != string.Empty)
            param.Value = InstallCompleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallVerifiedBy";
        if (InstallVerifiedBy != string.Empty)
            param.Value = InstallVerifiedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WelcomeLetterDone";
        if (WelcomeLetterDone != string.Empty)
            param.Value = WelcomeLetterDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallRequestSaved";
        if (InstallRequestSaved != string.Empty)
            param.Value = InstallRequestSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelSerials";
        if (PanelSerials != string.Empty)
            param.Value = PanelSerials;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSerial";
        if (InverterSerial != string.Empty)
            param.Value = InverterSerial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterSerial";
        if (SecondInverterSerial != string.Empty)
            param.Value = SecondInverterSerial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CertificateIssued";
        if (CertificateIssued != string.Empty)
            param.Value = CertificateIssued;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CertificateSaved";
        if (CertificateSaved != string.Empty)
            param.Value = CertificateSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCReceivedBy";
        if (STCReceivedBy != string.Empty)
            param.Value = STCReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCCheckedBy";
        if (STCCheckedBy != string.Empty)
            param.Value = STCCheckedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFormSaved";
        if (STCFormSaved != string.Empty)
            param.Value = STCFormSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploaded";
        if (STCUploaded != string.Empty)
            param.Value = STCUploaded;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploadNumber";
        if (STCUploadNumber != string.Empty)
            param.Value = STCUploadNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFU";
        if (STCFU != string.Empty)
            param.Value = STCFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNotes";
        if (STCNotes != string.Empty)
            param.Value = STCNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallationComment";
        if (InstallationComment != string.Empty)
            param.Value = InstallationComment;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceReceived";
        if (BalanceReceived != string.Empty)
            param.Value = BalanceReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Witholding";
        if (Witholding != string.Empty)
            param.Value = Witholding;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceVerified";
        if (BalanceVerified != string.Empty)
            param.Value = BalanceVerified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaid";
        if (InvoicePaid != string.Empty)
            param.Value = InvoicePaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotifiedMeter";
        if (InstallerNotifiedMeter != string.Empty)
            param.Value = InstallerNotifiedMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotifiedMeter";
        if (CustNotifiedMeter != string.Empty)
            param.Value = CustNotifiedMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElectrician";
        if (MeterElectrician != string.Empty)
            param.Value = MeterElectrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstallerFU";
        if (MeterInstallerFU != string.Empty)
            param.Value = MeterInstallerFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstallerNotes";
        if (MeterInstallerNotes != string.Empty)
            param.Value = MeterInstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterJobBooked";
        if (MeterJobBooked != string.Empty)
            param.Value = MeterJobBooked;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterCompleted";
        if (MeterCompleted != string.Empty)
            param.Value = MeterCompleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInvoice";
        if (MeterInvoice != string.Empty)
            param.Value = MeterInvoice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInvoiceNo";
        if (MeterInvoiceNo != string.Empty)
            param.Value = MeterInvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvNo";
        if (InstallerInvNo != string.Empty)
            param.Value = InstallerInvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvAmnt";
        if (InstallerInvAmnt != string.Empty)
            param.Value = InstallerInvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvDate";
        if (InstallerInvDate != string.Empty)
            param.Value = InstallerInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerPayDate";
        if (InstallerPayDate != string.Empty)
            param.Value = InstallerPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecInvNo";
        if (MeterElecInvNo != string.Empty)
            param.Value = MeterElecInvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecInvAmnt";
        if (MeterElecInvAmnt != string.Empty)
            param.Value = MeterElecInvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecInvDate";
        if (MeterElecInvDate != string.Empty)
            param.Value = MeterElecInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecPayDate";
        if (MeterElecPayDate != string.Empty)
            param.Value = MeterElecPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecFollowUp";
        if (MeterElecFollowUp != string.Empty)
            param.Value = MeterElecFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianInvoiceNotes";
        if (ElectricianInvoiceNotes != string.Empty)
            param.Value = ElectricianInvoiceNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectEnteredBy";
        if (ProjectEnteredBy != string.Empty)
            param.Value = ProjectEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblProjects_Update(string ProjectID, String CustomerID, String ContactID, String EmployeeID, String SalesRep, String ProjectTypeID, String ProjectStatusID, String ProjectCancelID, String ProjectOnHoldID, String ProjectOpened, String ProjectCancelled, String OldProjectNumber, String ManualQuoteNumber, String Project, String NextMaintenanceCall, String InstallAddress, String InstallCity, String InstallState, String InstallPostCode, String ProjectNotes, String AdditionalSystem, String GridConnected, String RebateApproved, String ReceivedCredits, String CreditEligible, String MoreThanOneInstall, String RequiredCompliancePaperwork, String OutOfPocketDocs, String OwnerGSTRegistered, String OwnerABN, String HouseTypeID, String RoofTypeID, String RoofAngleID, String InstallBase, String StockAllocationStore, String StandardPack, String PanelBrandID, String PanelBrand, String PanelModel, String PanelOutput, String PanelDetails, String InverterDetailsID, String SecondInverterDetailsID, String InverterBrand, String InverterModel, String InverterSeries, String InverterOutput, String SecondInverterOutput, String TotalInverterOutput, String InverterCert, String InverterDetails, String SystemDetails, String NumberPanels, String PreviousNumberPanels, String PanelConfigNW, String PanelConfigOTH, String SystemCapKW, String STCMultiplier, String STCZoneRating, String STCNumber, String STCValue, String ElecRetailerID, String ElecDistributorID, String ElecDistApplied, String ElecDistApplyMethod, String ElecDistApplyBy, String ElecDistApplySentFrom, String ElecDistApproved, String ElecDistApprovelRef, String ElecDistOK, String RegPlanNo, String LotNumber, String Asbestoss, String MeterUG, String MeterEnoughSpace, String SplitSystem, String CherryPicker, String TravelTime, String VariationOther, String VarRoofType, String VarRoofAngle, String VarHouseType, String VarAsbestos, String VarMeterInstallation, String VarMeterUG, String VarTravelTime, String VarSplitSystem, String VarEnoughSpace, String VarCherryPicker, String VarOther, String SpecialDiscount, String DepositRequired, String TotalQuotePrice, String PreviousTotalQuotePrice, String InvoiceExGST, String InvoiceGST, String BalanceGST, String FinanceWithID, String ServiceValue, String QuoteSent, String QuoteSentNo, String QuoteAccepted, String SignedQuote, String MeterBoxPhotosSaved, String ElecBillSaved, String ProposedDesignSaved, String FollowUp, String FollowUpNote, String InvoiceNumber, String InvoiceTag, String InvoiceDoc, String InvoiceDetail, String InvoiceSent, String InvoiceFU, String InvoiceNotes, String InvRefund, String InvRefunded, String InvRefundBy, String DepositReceived, String DepositAmount, String ReceiptSent, String SalesCommPaid, String InstallBookingDate, String MeterIncluded, String MeterAppliedRef, String MeterAppliedDate, String MeterAppliedTime, String MeterAppliedMethod, String MeterApprovedDate, String MeterApprovalNo, String MeterPhase, String OffPeak, String NMINumber, String MeterNumber1, String MeterNumber2, String MeterNumber3, String MeterNumber4, String MeterFU, String MeterNotes, String REXAppliedRef, String REXAppliedDate, String REXStatusID, String REXApprovalNotes, String REXApprovalFU, String STCPrice, String RECRebate, String BalanceRequested, String Installer, String Designer, String Electrician, String InstallAM1, String InstallPM1, String InstallAM2, String InstallPM2, String InstallDays, String STCFormsDone, String InstallDocsReceived, String InstallerNotified, String CustNotifiedInstall, String InstallerFollowUp, String InstallerNotes, String InstallerDocsSent, String InstallCompleted, String InstallVerifiedBy, String WelcomeLetterDone, String InstallRequestSaved, String PanelSerials, String InverterSerial, String SecondInverterSerial, String CertificateIssued, String CertificateSaved, String STCReceivedBy, String STCCheckedBy, String STCFormSaved, String STCUploaded, String STCUploadNumber, String STCFU, String STCNotes, String InstallationComment, String PVDNumber, String PVDStatusID, String STCApplied, String BalanceReceived, String Witholding, String BalanceVerified, String InvoicePaid, String InstallerNotifiedMeter, String CustNotifiedMeter, String MeterElectrician, String MeterInstallerFU, String MeterInstallerNotes, String MeterJobBooked, String MeterCompleted, String MeterInvoice, String MeterInvoiceNo, String InstallerInvNo, String InstallerInvAmnt, String InstallerInvDate, String InstallerPayDate, String MeterElecInvNo, String MeterElecInvAmnt, String MeterElecInvDate, String MeterElecPayDate, String MeterElecFollowUp, String ElectricianInvoiceNotes, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectOnHoldID";
        if (ProjectOnHoldID != string.Empty)
            param.Value = ProjectOnHoldID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectOpened";
        if (ProjectOpened != string.Empty)
            param.Value = ProjectOpened;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelled";
        if (ProjectCancelled != string.Empty)
            param.Value = ProjectCancelled;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OldProjectNumber";
        if (OldProjectNumber != string.Empty)
            param.Value = OldProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project";
        if (Project != string.Empty)
            param.Value = Project;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NextMaintenanceCall";
        if (NextMaintenanceCall != string.Empty)
            param.Value = NextMaintenanceCall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNotes";
        if (ProjectNotes != string.Empty)
            param.Value = ProjectNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdditionalSystem";
        if (AdditionalSystem != string.Empty)
            param.Value = AdditionalSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GridConnected";
        if (GridConnected != string.Empty)
            param.Value = GridConnected;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RebateApproved";
        if (RebateApproved != string.Empty)
            param.Value = RebateApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedCredits";
        if (ReceivedCredits != string.Empty)
            param.Value = ReceivedCredits;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreditEligible";
        if (CreditEligible != string.Empty)
            param.Value = CreditEligible;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MoreThanOneInstall";
        if (MoreThanOneInstall != string.Empty)
            param.Value = MoreThanOneInstall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RequiredCompliancePaperwork";
        if (RequiredCompliancePaperwork != string.Empty)
            param.Value = RequiredCompliancePaperwork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OutOfPocketDocs";
        if (OutOfPocketDocs != string.Empty)
            param.Value = OutOfPocketDocs;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OwnerGSTRegistered";
        if (OwnerGSTRegistered != string.Empty)
            param.Value = OwnerGSTRegistered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OwnerABN";
        if (OwnerABN != string.Empty)
            param.Value = OwnerABN;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        if (HouseTypeID != string.Empty)
            param.Value = HouseTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        if (RoofTypeID != string.Empty)
            param.Value = RoofTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofAngleID";
        if (RoofAngleID != string.Empty)
            param.Value = RoofAngleID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBase";
        if (InstallBase != string.Empty)
            param.Value = InstallBase;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockAllocationStore";
        if (StockAllocationStore != string.Empty)
            param.Value = StockAllocationStore;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StandardPack";
        if (StandardPack != string.Empty)
            param.Value = StandardPack;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrandID";
        if (PanelBrandID != string.Empty)
            param.Value = PanelBrandID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrand";
        if (PanelBrand != string.Empty)
            param.Value = PanelBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelOutput";
        if (PanelOutput != string.Empty)
            param.Value = PanelOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelDetails";
        if (PanelDetails != string.Empty)
            param.Value = PanelDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetailsID";
        if (InverterDetailsID != string.Empty)
            param.Value = InverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterDetailsID";
        if (SecondInverterDetailsID != string.Empty)
            param.Value = SecondInverterDetailsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterBrand";
        if (InverterBrand != string.Empty)
            param.Value = InverterBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSeries";
        if (InverterSeries != string.Empty)
            param.Value = InverterSeries;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterOutput";
        if (InverterOutput != string.Empty)
            param.Value = InverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterOutput";
        if (SecondInverterOutput != string.Empty)
            param.Value = SecondInverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TotalInverterOutput";
        if (TotalInverterOutput != string.Empty)
            param.Value = TotalInverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterCert";
        if (InverterCert != string.Empty)
            param.Value = InverterCert;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterDetails";
        if (InverterDetails != string.Empty)
            param.Value = InverterDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemDetails";
        if (SystemDetails != string.Empty)
            param.Value = SystemDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NumberPanels";
        if (NumberPanels != string.Empty)
            param.Value = NumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreviousNumberPanels";
        if (PreviousNumberPanels != string.Empty)
            param.Value = PreviousNumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelConfigNW";
        if (PanelConfigNW != string.Empty)
            param.Value = PanelConfigNW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelConfigOTH";
        if (PanelConfigOTH != string.Empty)
            param.Value = PanelConfigOTH;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKW";
        if (SystemCapKW != string.Empty)
            param.Value = SystemCapKW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCMultiplier";
        if (STCMultiplier != string.Empty)
            param.Value = STCMultiplier;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCZoneRating";
        if (STCZoneRating != string.Empty)
            param.Value = STCZoneRating;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumber";
        if (STCNumber != string.Empty)
            param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCValue";
        if (STCValue != string.Empty)
            param.Value = STCValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecRetailerID";
        if (ElecRetailerID != string.Empty)
            param.Value = ElecRetailerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributorID";
        if (ElecDistributorID != string.Empty)
            param.Value = ElecDistributorID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplied";
        if (ElecDistApplied != string.Empty)
            param.Value = ElecDistApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplyMethod";
        if (ElecDistApplyMethod != string.Empty)
            param.Value = ElecDistApplyMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplyBy";
        if (ElecDistApplyBy != string.Empty)
            param.Value = ElecDistApplyBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplySentFrom";
        if (ElecDistApplySentFrom != string.Empty)
            param.Value = ElecDistApplySentFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApproved";
        if (ElecDistApproved != string.Empty)
            param.Value = ElecDistApproved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistOK";
        if (ElecDistOK != string.Empty)
            param.Value = ElecDistOK;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RegPlanNo";
        if (RegPlanNo != string.Empty)
            param.Value = RegPlanNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LotNumber";
        if (LotNumber != string.Empty)
            param.Value = LotNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Asbestoss";
        if (Asbestoss != string.Empty)
            param.Value = Asbestoss;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterUG";
        if (MeterUG != string.Empty)
            param.Value = MeterUG;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterEnoughSpace";
        if (MeterEnoughSpace != string.Empty)
            param.Value = MeterEnoughSpace;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SplitSystem";
        if (SplitSystem != string.Empty)
            param.Value = SplitSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CherryPicker";
        if (CherryPicker != string.Empty)
            param.Value = CherryPicker;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TravelTime";
        if (TravelTime != string.Empty)
            param.Value = TravelTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VariationOther";
        if (VariationOther != string.Empty)
            param.Value = VariationOther;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofType";
        if (VarRoofType != string.Empty)
            param.Value = VarRoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarRoofAngle";
        if (VarRoofAngle != string.Empty)
            param.Value = VarRoofAngle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarHouseType";
        if (VarHouseType != string.Empty)
            param.Value = VarHouseType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarAsbestos";
        if (VarAsbestos != string.Empty)
            param.Value = VarAsbestos;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterInstallation";
        if (VarMeterInstallation != string.Empty)
            param.Value = VarMeterInstallation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarMeterUG";
        if (VarMeterUG != string.Empty)
            param.Value = VarMeterUG;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarTravelTime";
        if (VarTravelTime != string.Empty)
            param.Value = VarTravelTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarSplitSystem";
        if (VarSplitSystem != string.Empty)
            param.Value = VarSplitSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarEnoughSpace";
        if (VarEnoughSpace != string.Empty)
            param.Value = VarEnoughSpace;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarCherryPicker";
        if (VarCherryPicker != string.Empty)
            param.Value = VarCherryPicker;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VarOther";
        if (VarOther != string.Empty)
            param.Value = VarOther;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SpecialDiscount";
        if (SpecialDiscount != string.Empty)
            param.Value = SpecialDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositRequired";
        if (DepositRequired != string.Empty)
            param.Value = DepositRequired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TotalQuotePrice";
        if (TotalQuotePrice != string.Empty)
            param.Value = TotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PreviousTotalQuotePrice";
        if (PreviousTotalQuotePrice != string.Empty)
            param.Value = PreviousTotalQuotePrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceExGST";
        if (InvoiceExGST != string.Empty)
            param.Value = InvoiceExGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceGST";
        if (InvoiceGST != string.Empty)
            param.Value = InvoiceGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceGST";
        if (BalanceGST != string.Empty)
            param.Value = BalanceGST;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceValue";
        if (ServiceValue != string.Empty)
            param.Value = ServiceValue;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteSent";
        if (QuoteSent != string.Empty)
            param.Value = QuoteSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteSentNo";
        if (QuoteSentNo != string.Empty)
            param.Value = QuoteSentNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteAccepted";
        if (QuoteAccepted != string.Empty)
            param.Value = QuoteAccepted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignedQuote";
        if (SignedQuote != string.Empty)
            param.Value = SignedQuote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterBoxPhotosSaved";
        if (MeterBoxPhotosSaved != string.Empty)
            param.Value = MeterBoxPhotosSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecBillSaved";
        if (ElecBillSaved != string.Empty)
            param.Value = ElecBillSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProposedDesignSaved";
        if (ProposedDesignSaved != string.Empty)
            param.Value = ProposedDesignSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUp";
        if (FollowUp != string.Empty)
            param.Value = FollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FollowUpNote";
        if (FollowUpNote != string.Empty)
            param.Value = FollowUpNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNumber";
        if (InvoiceNumber != string.Empty)
            param.Value = InvoiceNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceTag";
        if (InvoiceTag != string.Empty)
            param.Value = InvoiceTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceDoc";
        if (InvoiceDoc != string.Empty)
            param.Value = InvoiceDoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceDetail";
        if (InvoiceDetail != string.Empty)
            param.Value = InvoiceDetail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceSent";
        if (InvoiceSent != string.Empty)
            param.Value = InvoiceSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceFU";
        if (InvoiceFU != string.Empty)
            param.Value = InvoiceFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNotes";
        if (InvoiceNotes != string.Empty)
            param.Value = InvoiceNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefund";
        if (InvRefund != string.Empty)
            param.Value = InvRefund;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefunded";
        if (InvRefunded != string.Empty)
            param.Value = InvRefunded;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvRefundBy";
        if (InvRefundBy != string.Empty)
            param.Value = InvRefundBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositReceived";
        if (DepositReceived != string.Empty)
            param.Value = DepositReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DepositAmount";
        if (DepositAmount != string.Empty)
            param.Value = DepositAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceiptSent";
        if (ReceiptSent != string.Empty)
            param.Value = ReceiptSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesCommPaid";
        if (SalesCommPaid != string.Empty)
            param.Value = SalesCommPaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterIncluded";
        if (MeterIncluded != string.Empty)
            param.Value = MeterIncluded;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedRef";
        if (MeterAppliedRef != string.Empty)
            param.Value = MeterAppliedRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedDate";
        if (MeterAppliedDate != string.Empty)
            param.Value = MeterAppliedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedTime";
        if (MeterAppliedTime != string.Empty)
            param.Value = MeterAppliedTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Time;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterAppliedMethod";
        if (MeterAppliedMethod != string.Empty)
            param.Value = MeterAppliedMethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterApprovedDate";
        if (MeterApprovedDate != string.Empty)
            param.Value = MeterApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterApprovalNo";
        if (MeterApprovalNo != string.Empty)
            param.Value = MeterApprovalNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterPhase";
        if (MeterPhase != string.Empty)
            param.Value = MeterPhase;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OffPeak";
        if (OffPeak != string.Empty)
            param.Value = OffPeak;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NMINumber";
        if (NMINumber != string.Empty)
            param.Value = NMINumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber1";
        if (MeterNumber1 != string.Empty)
            param.Value = MeterNumber1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber2";
        if (MeterNumber2 != string.Empty)
            param.Value = MeterNumber2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber3";
        if (MeterNumber3 != string.Empty)
            param.Value = MeterNumber3;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNumber4";
        if (MeterNumber4 != string.Empty)
            param.Value = MeterNumber4;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterFU";
        if (MeterFU != string.Empty)
            param.Value = MeterFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterNotes";
        if (MeterNotes != string.Empty)
            param.Value = MeterNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXAppliedRef";
        if (REXAppliedRef != string.Empty)
            param.Value = REXAppliedRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXAppliedDate";
        if (REXAppliedDate != string.Empty)
            param.Value = REXAppliedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXStatusID";
        if (REXStatusID != string.Empty)
            param.Value = REXStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXApprovalNotes";
        if (REXApprovalNotes != string.Empty)
            param.Value = REXApprovalNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@REXApprovalFU";
        if (REXApprovalFU != string.Empty)
            param.Value = REXApprovalFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCPrice";
        if (STCPrice != string.Empty)
            param.Value = STCPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RECRebate";
        if (RECRebate != string.Empty)
            param.Value = RECRebate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceRequested";
        if (BalanceRequested != string.Empty)
            param.Value = BalanceRequested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Designer";
        if (Designer != string.Empty)
            param.Value = Designer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Electrician";
        if (Electrician != string.Empty)
            param.Value = Electrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM1";
        if (InstallAM1 != string.Empty)
            param.Value = InstallAM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM1";
        if (InstallPM1 != string.Empty)
            param.Value = InstallPM1;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAM2";
        if (InstallAM2 != string.Empty)
            param.Value = InstallAM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPM2";
        if (InstallPM2 != string.Empty)
            param.Value = InstallPM2;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallDays";
        if (InstallDays != string.Empty)
            param.Value = InstallDays;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFormsDone";
        if (STCFormsDone != string.Empty)
            param.Value = STCFormsDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallDocsReceived";
        if (InstallDocsReceived != string.Empty)
            param.Value = InstallDocsReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotified";
        if (InstallerNotified != string.Empty)
            param.Value = InstallerNotified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotifiedInstall";
        if (CustNotifiedInstall != string.Empty)
            param.Value = CustNotifiedInstall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerFollowUp";
        if (InstallerFollowUp != string.Empty)
            param.Value = InstallerFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotes";
        if (InstallerNotes != string.Empty)
            param.Value = InstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerDocsSent";
        if (InstallerDocsSent != string.Empty)
            param.Value = InstallerDocsSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCompleted";
        if (InstallCompleted != string.Empty)
            param.Value = InstallCompleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallVerifiedBy";
        if (InstallVerifiedBy != string.Empty)
            param.Value = InstallVerifiedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WelcomeLetterDone";
        if (WelcomeLetterDone != string.Empty)
            param.Value = WelcomeLetterDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallRequestSaved";
        if (InstallRequestSaved != string.Empty)
            param.Value = InstallRequestSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelSerials";
        if (PanelSerials != string.Empty)
            param.Value = PanelSerials;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSerial";
        if (InverterSerial != string.Empty)
            param.Value = InverterSerial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SecondInverterSerial";
        if (SecondInverterSerial != string.Empty)
            param.Value = SecondInverterSerial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CertificateIssued";
        if (CertificateIssued != string.Empty)
            param.Value = CertificateIssued;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CertificateSaved";
        if (CertificateSaved != string.Empty)
            param.Value = CertificateSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCReceivedBy";
        if (STCReceivedBy != string.Empty)
            param.Value = STCReceivedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCCheckedBy";
        if (STCCheckedBy != string.Empty)
            param.Value = STCCheckedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFormSaved";
        if (STCFormSaved != string.Empty)
            param.Value = STCFormSaved;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploaded";
        if (STCUploaded != string.Empty)
            param.Value = STCUploaded;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploadNumber";
        if (STCUploadNumber != string.Empty)
            param.Value = STCUploadNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCFU";
        if (STCFU != string.Empty)
            param.Value = STCFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNotes";
        if (STCNotes != string.Empty)
            param.Value = STCNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallationComment";
        if (InstallationComment != string.Empty)
            param.Value = InstallationComment;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceReceived";
        if (BalanceReceived != string.Empty)
            param.Value = BalanceReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Witholding";
        if (Witholding != string.Empty)
            param.Value = Witholding;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BalanceVerified";
        if (BalanceVerified != string.Empty)
            param.Value = BalanceVerified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePaid";
        if (InvoicePaid != string.Empty)
            param.Value = InvoicePaid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotifiedMeter";
        if (InstallerNotifiedMeter != string.Empty)
            param.Value = InstallerNotifiedMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustNotifiedMeter";
        if (CustNotifiedMeter != string.Empty)
            param.Value = CustNotifiedMeter;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElectrician";
        if (MeterElectrician != string.Empty)
            param.Value = MeterElectrician;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstallerFU";
        if (MeterInstallerFU != string.Empty)
            param.Value = MeterInstallerFU;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInstallerNotes";
        if (MeterInstallerNotes != string.Empty)
            param.Value = MeterInstallerNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterJobBooked";
        if (MeterJobBooked != string.Empty)
            param.Value = MeterJobBooked;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterCompleted";
        if (MeterCompleted != string.Empty)
            param.Value = MeterCompleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInvoice";
        if (MeterInvoice != string.Empty)
            param.Value = MeterInvoice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterInvoiceNo";
        if (MeterInvoiceNo != string.Empty)
            param.Value = MeterInvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvNo";
        if (InstallerInvNo != string.Empty)
            param.Value = InstallerInvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvAmnt";
        if (InstallerInvAmnt != string.Empty)
            param.Value = InstallerInvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerInvDate";
        if (InstallerInvDate != string.Empty)
            param.Value = InstallerInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerPayDate";
        if (InstallerPayDate != string.Empty)
            param.Value = InstallerPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecInvNo";
        if (MeterElecInvNo != string.Empty)
            param.Value = MeterElecInvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecInvAmnt";
        if (MeterElecInvAmnt != string.Empty)
            param.Value = MeterElecInvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecInvDate";
        if (MeterElecInvDate != string.Empty)
            param.Value = MeterElecInvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecPayDate";
        if (MeterElecPayDate != string.Empty)
            param.Value = MeterElecPayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterElecFollowUp";
        if (MeterElecFollowUp != string.Empty)
            param.Value = MeterElecFollowUp;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianInvoiceNotes";
        if (ElectricianInvoiceNotes != string.Empty)
            param.Value = ElectricianInvoiceNotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        if (upsize_ts != string.Empty)
            param.Value = upsize_ts;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_STC(string ProjectNumber, string PVDNumber, string PVDStatusID, string ManualQuoteNumber, string STCApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_STC";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_Delete(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblProjects_InserttblProjects2(String ProjectLinkID, String InstallerNotesChangedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_InserttblProjects2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNotesChangedBy";
        if (InstallerNotesChangedBy != string.Empty)
            param.Value = InstallerNotesChangedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblTrackProjStatus_Insert(String ProjectStatusID, String ProjectID, String EmployeeID, String NumberPanels)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTrackProjStatus_Insert";

        //HttpContext.Current.Response.Write("ProjectStatusID" + ProjectStatusID + "ProjectID" + ProjectID + "EmployeeID" + EmployeeID + "NumberPanels" + NumberPanels );
        //HttpContext.Current.Response.End();
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NumberPanels";
        if (NumberPanels != string.Empty)
            param.Value = NumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblProjects_UpdateProjectNumber(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateProjectNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjectVariations_Select(string ProjectVariationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectVariations_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectVariationID";
        param.Value = ProjectVariationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectMtceCall_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMtceCall_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectMtceCall_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMtceCall_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectMtceStatus_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMtceStatus_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectMtceStatus_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMtceStatus_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblProjects_InserttblProjectMaintenance(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_InserttblProjectMaintenance";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblProjects_UpdateInvoiceDoc(string ProjectID, string ProjectNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvoiceDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateWelcomeLetter(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateWelcomeLetter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects2_UpdateFinanceWithDeposit(string ProjectLinkID, string FinanceWithDepositID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdateFinanceWithDeposit";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithDepositID";
        if (FinanceWithDepositID != string.Empty)
            param.Value = FinanceWithDepositID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateEmployee(string CustomerID, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateEmployee";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;


        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjects_CountProjectStatus(String ProjectEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_CountProjectStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectEnteredBy";
        if (ProjectEnteredBy != string.Empty)
            param.Value = ProjectEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_PreInstCount(String ProjectEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_PreInstCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectEnteredBy";
        if (ProjectEnteredBy != string.Empty)
            param.Value = ProjectEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_CountAmount(String ProjectEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_CountAmount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectEnteredBy";
        if (ProjectEnteredBy != string.Empty)
            param.Value = ProjectEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_WeeklyTarget(String ProjectEnteredBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_WeeklyTarget";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectEnteredBy";
        if (ProjectEnteredBy != string.Empty)
            param.Value = ProjectEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_Count(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_Count";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectStatus_CountStatus(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_CountStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_PreInstCountStatus(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_PreInstCountStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_SelectTop1(String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectTop1";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblPostCodes_SelectSydney()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_SelectSydney";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectStatus_StatusSM()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_StatusSM";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectStatus_StatusSalesRep()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_StatusSalesRep";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectStatus_StatusDSalesRep()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_StatusDSalesRep";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectStatus_StatusInstMgr()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_StatusInstMgr";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_StatusInstallMgr()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_StatusInstallMgr";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_StatusInstallMgrCancel()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_StatusInstallMgrCancel";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_Mtce()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_Mtce";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_InstMgr()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_InstMgr";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjects_UpdateQuoteSentDate(string ProjectID, string QuoteSent)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateQuoteSentDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteSent";
        param.Value = QuoteSent;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateQuoteSent(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateQuoteSent";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateProjectStatus(string ProjectID, String ProjectStatusID, string ProjectCancelID, string ProjectOnHoldID, string StatusComment)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateProjectStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectOnHoldID";
        if (ProjectOnHoldID != string.Empty)
            param.Value = ProjectOnHoldID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusComment";
        param.Value = StatusComment;
        param.DbType = DbType.String;
        param.Size = 80000;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects2_UpdateDocsDone(string ProjectLinkID, string DocsDone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdateDocsDone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocsDone";
        param.Value = DocsDone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects2_UpdateNoDocs(string ProjectLinkID, string NoDocs)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdateNoDocs";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectLinkID";
        param.Value = ProjectLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoDocs";
        param.Value = NoDocs;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjects_SelectWarehouse(string IsDeduct, string alpha, string ProjectNumber, string InstallCity, string InstallState, string startdate, string enddate, string DateType, string StoreLocation, string Historic)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectWarehouse";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StoreLocation";
        if (StoreLocation != string.Empty)
            param.Value = StoreLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Historic";
        if (Historic != string.Empty)
            param.Value = Historic;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectWarehouseAllocated(string IsDeduct, string alpha, string ProjectNumber, string InstallCity, string InstallState, string startdate, string enddate, string DateType, string StoreLocation, String ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectWarehouseAllocated";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StoreLocation";
        if (StoreLocation != string.Empty)
            param.Value = StoreLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateDeduct(string ProjectID, string StockDeductBy, string IsDeduct)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateDeduct";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductBy";
        if (StockDeductBy != string.Empty)
            param.Value = StockDeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateRevert(string ProjectID, string StockDeductBy, string IsDeduct)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductBy";
        if (StockDeductBy != string.Empty)
            param.Value = StockDeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects2_UpdateSalesInvNo(string ProjectID, string SalesInvNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects2_UpdateSalesInvNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesInvNo";
        param.Value = SalesInvNo;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblTrackProjStatus_SelectByProjectID(String ProjectID, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTrackProjStatus_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblTrackProjStatus_ForRefund(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTrackProjStatus_ForRefund";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectSalesInvoice(string Contact, string ProjectNumber, string ManualQuoteNumber, string SalesRep, string InstallCity, string InstallState, string InstallPostCode, string ProjectStatusID, string datetype, string startdate, string enddate, string Historic)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectSalesInvoice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Historic";
        param.Value = Historic;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        //HttpContext.Current.Response.Write(result);
        //HttpContext.Current.Response.End();
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectInstInvoice(string Contact, string ProjectNumber, string ManualQuoteNumber, string Installer, string InstallCity, string InstallState, string InstallPostCode, string datetype, string startdate, string enddate, string Historic, String MeterApplyRef)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectInstInvoice";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Historic";
        param.Value = Historic;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MeterApplyRef";
        if (MeterApplyRef != string.Empty)
            param.Value = MeterApplyRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectInstInvoiceInstaller(string Contact, string ProjectNumber, string ManualQuoteNumber, string Installer, string InstallCity, string InstallState, string InstallPostCode, string datetype, string startdate, string enddate, string Historic, String MeterApplyRef)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectInstInvoiceInstaller";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Contact";
        if (Contact != string.Empty)
            param.Value = Contact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Historic";
        param.Value = Historic;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param.ParameterName = "@MeterApplyRef";
        if (MeterApplyRef != string.Empty)
            param.Value = MeterApplyRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateMove(string ProjectID, string Installer, string InstallBookingDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateMove";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    /* ----------------- Project Quotes ----------------- */

    public static SttblProjectQuotes tblProjectQuotes_SelectByProjectQuoteID(String ProjectQuoteID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_SelectByProjectQuoteID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectQuoteID";
        param.Value = ProjectQuoteID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjectQuotes details = new SttblProjectQuotes();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProjectQuoteID = dr["ProjectQuoteID"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.ProjectQuoteDate = dr["ProjectQuoteDate"].ToString();
            details.ProjectQuoteDoc = dr["ProjectQuoteDoc"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.QuoteDoc = dr["QuoteDoc"].ToString();

        }
        // return structure details
        return details;
    }

    public static int tblProjectQuotes_Insert(String ProjectID, String ProjectNumber, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblProjectQuotes_UpdateProjectQuoteDoc(string ProjectQuoteID, string QuoteDoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_UpdateProjectQuoteDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectQuoteID";
        param.Value = ProjectQuoteID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteDoc";
        param.Value = QuoteDoc;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjectQuotes_UpdateQuoteDoc(string ProjectQuoteDoc, string QuoteDoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_UpdateQuoteDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectQuoteDoc";
        param.Value = ProjectQuoteDoc;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteDoc";
        param.Value = QuoteDoc;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjectQuotes_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblProjectQuotes_SelectTOPDocNoByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_SelectTOPDocNoByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //DataTable result = new DataTable();
        int result = 0;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectQuotes_SelectByProjectQuoteDoc(String ProjectQuoteDoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_SelectByProjectQuoteDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectQuoteDoc";
        param.Value = ProjectQuoteDoc;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectQuotes_SelectTop1(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectQuotes_SelectTop1";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    /* -------------------------------------------------- */

    public static DataTable tblProjects_CustomerSelect(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_CustomerSelect";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    /* ----------------- Customer Feedback ----------------- */

    public static int tblCustomerFeedback_Insert(String CustomerID, String ProjectID, string Feedback)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerFeedback_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Feedback";
        if (Feedback != string.Empty)
            param.Value = Feedback;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static DataTable tblCustomerFeedback_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerFeedback_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    /* ----------------------------------------------------- */

    public static bool tblProjects_UpdatePStatusByCustomerID(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePStatusByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjects_InvoiceSalesReport(string startdate, string enddate, String FinanceWithID, string InvoicePayMethodID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_InvoiceSalesReport";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePayMethodID";
        if (InvoicePayMethodID != string.Empty)
            param.Value = InvoicePayMethodID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectsGetDataBySearchAllNotPlanned(string employeeid, string ProjectTypeID, string ProjectStatusID, string ProjectNumber, string ManualQuoteNumber, string alpha, string ProjectCancelID, string InstallAddress, string InstallCity, string InstallState, string CustSourceID, string SalesTeamID, string CompanyNumber, string PanelModel, string InverterModel, string startdate, string enddate, string Client, string CustSourceSubID, string InstallPostCodeFrom, string InstallPostCodeTo, string SystemCapKWFrom, string SystemCapKWTo, string EmpSearch, string datetype, string noinstdate, string Promo1ID, string Promo2ID, string FinanceWithID, string ElecDistApprovelRef, string PurchaseNo, string DistApprovedDate, string mterupgrade, string readytoactive, string Housetype, string RoofType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetDataBySearchAllNotPlanned";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (employeeid != string.Empty)
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelID";
        if (ProjectCancelID != string.Empty)
            param.Value = ProjectCancelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallAddress";
        if (InstallAddress != string.Empty)
            param.Value = InstallAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeFrom";
        if (InstallPostCodeFrom != string.Empty)
            param.Value = InstallPostCodeFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCodeTo";
        if (InstallPostCodeTo != string.Empty)
            param.Value = InstallPostCodeTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWFrom";
        if (SystemCapKWFrom != string.Empty)
            param.Value = SystemCapKWFrom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKWTo";
        if (SystemCapKWTo != string.Empty)
            param.Value = SystemCapKWTo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpSearch";
        if (EmpSearch != string.Empty)
            param.Value = EmpSearch;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@noinstdate";
        if (noinstdate != string.Empty)
            param.Value = noinstdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo1ID";
        if (Promo1ID != string.Empty)
            param.Value = Promo1ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo2ID";
        if (Promo2ID != string.Empty)
            param.Value = Promo2ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FinanceWithID";
        if (FinanceWithID != string.Empty)
            param.Value = FinanceWithID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PurchaseNo";
        if (PurchaseNo != string.Empty)
            param.Value = PurchaseNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistApprovedDate";
        if (DistApprovedDate != string.Empty)
            param.Value = DistApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@meterupgrade";
        if (mterupgrade != string.Empty)
            param.Value = mterupgrade;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";
        if (mterupgrade != string.Empty)
            param.Value = readytoactive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        if (RoofType != string.Empty)
            param.Value = RoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@HouseTypeID";
        if (Housetype != string.Empty)
            param.Value = Housetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);


        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static bool tblProjects_UpdateFormbayId(string ProjectID, string FormbayId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateFormbayId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FormbayId";
        if (FormbayId != string.Empty)
            param.Value = FormbayId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblProjectMtceStatus_SelectTop4()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMtceStatus_SelectTop4";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectMtceStatus_SelectLast4()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMtceStatus_SelectLast4";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjects_Updatemtcepaperwork(string ProjectID, String mtcepaperwork)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatemtcepaperwork";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@mtcepaperwork";
        if (mtcepaperwork != string.Empty)
            param.Value = mtcepaperwork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateLinkProjectID(string ProjectID, String LinkProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateLinkProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LinkProjectID";
        if (LinkProjectID != string.Empty)
            param.Value = LinkProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblProjects_GetProjectStatusComplete(string Customerid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetProjectStatusComplete";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Customerid";
        param.Value = Customerid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_Searchby_FormbayDocs(string ProjectNumber, string companyname, string formbayid, string startdate, string enddate, string installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Searchby_FormbayDocs";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@companyname";
        if (companyname != string.Empty)
            param.Value = companyname;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbayid";
        if (formbayid != string.Empty)
            param.Value = formbayid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installer";
        if (installer != string.Empty)
            param.Value = installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_SelectFormbayDocsReport()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectFormbayDocsReport";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_FormbayJob()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_FormbayJob";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_FormbayJobbyProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_FormbayJobbyProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjects_UpdateInstallationStatus(string ProjectID, string installation_status)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInstallationStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installation_status";
        if (installation_status != string.Empty)
            param.Value = installation_status;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateFormbayInstallBookingDate(string ProjectID, string FormbayInstallBookingDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateFormbayInstallBookingDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FormbayInstallBookingDate";
        if (FormbayInstallBookingDate != string.Empty)
            param.Value = FormbayInstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_Update_ProjectStatusID(string ProjectID, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Update_ProjectStatusID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateInstallCompleteDate(string ProjectID, string InstallCompleted)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInstallCompleteDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCompleted";
        if (InstallCompleted != string.Empty)
            param.Value = InstallCompleted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateInstallBookingDate(string ProjectID, string InstallBookingDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInstallBookingDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateInverterSerial(string ProjectID, string InverterSerial)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInverterSerial";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSerial";
        if (InverterSerial != string.Empty)
            param.Value = InverterSerial;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdatePVDNumber(string ProjectID, string PVDNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePVDNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTCApplied(string ProjectID, string STCApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTCApplied";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSTCUploadNumber(string ProjectID, string STCUploadNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSTCUploadNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCUploadNumber";
        if (STCUploadNumber != string.Empty)
            param.Value = STCUploadNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdatePVDStatusID(string ProjectID, string PVDStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePVDStatusID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        if (PVDStatusID != string.Empty)
            param.Value = PVDStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjects_StockRevert(string IsDeduct, string alpha, string ProjectNumber, string InstallCity, string InstallState, string startdate, string enddate, string DateType, string StoreLocation, string RevertFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_StockRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCity";
        if (InstallCity != string.Empty)
            param.Value = InstallCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StoreLocation";
        if (StoreLocation != string.Empty)
            param.Value = StoreLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        if (RevertFlag != string.Empty)
            param.Value = RevertFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_GetTotalQuoteCreated(String EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetTotalQuoteCreated";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_GetTotalFirstDepRec(String EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetTotalFirstDepRec";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_GetFirstDep(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetFirstDep";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjects_UpdateNumberPanels(string ProjectID, string NumberPanels)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateNumberPanels";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NumberPanels";
        if (NumberPanels != string.Empty)
            param.Value = NumberPanels;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateStockDeductDate(string ProjectID, string StockDeductDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateStockDeductDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        if (StockDeductDate != string.Empty)
            param.Value = StockDeductDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblProjects_UpdateStockDeductBy(string ProjectID, string StockDeductBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateStockDeductBy";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductBy";
        if (StockDeductBy != string.Empty)
            param.Value = StockDeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_projectcancel_update(string id, string projectcancel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_projectcancel_update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectcancel";
        param.Value = projectcancel;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }


    public static bool tblProjects_Updateprecancelreason(string ProjectID, String precancelreason)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updateprecancelreason";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@precancelreason";
        if (precancelreason != string.Empty)
            param.Value = precancelreason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 2147483647;
        comm.Parameters.Add(param);



        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblProjectsGetCount()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectsGetCount";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_SelectByProjectID_picklist(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID_picklist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByProjectID_picklist2(String ProjectID, String RefundID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID_picklist2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RefundID";
        param.Value = RefundID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_SelectByProjectID_picklist3(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID_picklist3";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_SelectByProjectID_picklist_withtop1notes(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID_picklist_withtop1notes";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_PicklistItemDetail_LastPickListWise(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_LastPickListWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_SelectByProjectID_picklist_withtop1notesMaintenance(String ProjectID, string ProjectMaintenanceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID_picklist_withtop1notesMaintenance";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMaintenanceID";
        param.Value = ProjectMaintenanceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjects_Updatereadyactive(string ProjectID, String readyactive)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_Updatereadyactive";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@readyactive";
        if (readyactive != string.Empty)
            param.Value = readyactive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblproject_update_quickformFlag(string ProjectID, String quickformGuid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblproject_update_quickformFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@quickformGuid";
        if (quickformGuid != string.Empty)
            param.Value = quickformGuid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdateSQ_SignedQuoteByProjectID(string ProjectID, String SQ, string SignedQuote, string QuoteAccepted)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateSQ_SignedQuoteByProjectID  ";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SQ";
        if (SQ != string.Empty)
            param.Value = SQ;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SignedQuote";
        if (SignedQuote != string.Empty)
            param.Value = SignedQuote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@QuoteAccepted";
        if (QuoteAccepted != string.Empty)
            param.Value = QuoteAccepted;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblProjects_UpdatePanelsByStockItemID(string StockItemID, string PanelBrand, string PanelModel, string PanelOutput)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatePanelsByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrand";
        if (PanelBrand != string.Empty)
            param.Value = PanelBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelModel";
        if (PanelModel != string.Empty)
            param.Value = PanelModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelOutput";
        if (PanelOutput != string.Empty)
            param.Value = PanelOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }


    public static bool tblProjects_UpdateInvererByStockItemID(string StockItemID, string InverterBrand, string InverterModel, string InverterSeries, string InverterOutput)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateInvererByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterBrand";
        if (InverterBrand != string.Empty)
            param.Value = InverterBrand;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterModel";
        if (InverterModel != string.Empty)
            param.Value = InverterModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterSeries";
        if (InverterSeries != string.Empty)
            param.Value = InverterSeries;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterOutput";
        if (InverterOutput != string.Empty)
            param.Value = InverterOutput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblSTCYearWiseRate_SelectByYear(string YearOrder)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSTCYearWiseRate_SelectByYear";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@YearOrder";
        param.Value = YearOrder;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectApplicationTrackerSearch(string ProjectNumber, string ProjectTypeID, string InstallState, string ProjectStatusID, string startdate, string enddate, string Application, string datetype)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectApplicationTrackerSearch";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectTypeID";
        if (ProjectTypeID != string.Empty)
            param.Value = ProjectTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Application";
        if (Application != string.Empty)
            param.Value = Application;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblPVDStatus_SelectByPVDStatus(String PVDStatus)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatus_SelectByPVDStatus";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PVDStatus";
        param.Value = PVDStatus;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblPickListLog_ExistsByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickListLog_ExistsByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblPickListLog_Insert(String ProjectID, String Reason, String Note, String PickListDateTime, string InstallBookedDate, string InstallerID, string InstallerName, string DesignerID, string ElectricianID, String CreatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickListLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Note";
        if (Note != string.Empty)
            param.Value = Note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListDateTime";
        if (PickListDateTime != string.Empty)
            param.Value = PickListDateTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookedDate";
        if (InstallBookedDate != string.Empty)
            param.Value = InstallBookedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerID";
        if (DesignerID != string.Empty)
            param.Value = DesignerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianID";
        if (ElectricianID != string.Empty)
            param.Value = ElectricianID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tbl_PickListLog_SelectByPId(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_SelectByPId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PickListLog_SelectByPickId(string PickId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_SelectByPickId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Select_CountforPickList(string PickId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Select_CountforPickList";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_PickListLog_DeleteByID(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_DeleteByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool deletepicklistData(string Date)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "deletepicklistData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@bookdate";
        if (Date != string.Empty)
            param.Value = Date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblApplicationNotes_delete (string Date)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblApplicationNotes_delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = Date;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_DeleteByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_DeleteByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tbl_PicklistItemDetail_SelectbyPickId(string PickID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_SelectbyPickId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickID";
        param.Value = PickID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblWallete_Diiferent_Count(string PickID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWallete_Diiferent_Count";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PicklistItemDetail_SelectByPId(string PickID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_SelectByPId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    #region POSTINST TAB

    public static DataTable tbl_StockPickUpDetail_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpDetail_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_StockPickUpReturnDetail_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpReturnDetail_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_StockPickUpDetail_Insert(String ProjectID, string ProjectNumber, String PickUpDate, String InstallerNameID, String InstallerName, String PickUpNote, String UpdatedDate, String UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpDetail_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpDate";
        if (PickUpDate != string.Empty)
            param.Value = PickUpDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNameID";
        if (InstallerNameID != string.Empty)
            param.Value = InstallerNameID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpNote";
        if (PickUpNote != string.Empty)
            param.Value = PickUpNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (UpdatedDate != string.Empty)
            param.Value = UpdatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_StockPickUpReturnDetail_Insert(String ProjectID, string ProjectNumber, String PickUpReturnDate, String PickUpReturnNote, String InstallerID, String Installer, String UpdatedDate, String UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpReturnDetail_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpReturnDate";
        if (PickUpReturnDate != string.Empty)
            param.Value = PickUpReturnDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpReturnNote";
        if (PickUpReturnNote != string.Empty)
            param.Value = PickUpReturnNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (UpdatedDate != string.Empty)
            param.Value = UpdatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_StockPickUpDetail_ExistsByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpDetail_ExistsByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tbl_StockPickUpReturnDetail_ExistsByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpReturnDetail_ExistsByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tbl_StockPickUpDetail_UpdateByProjectID(String ProjectID, string ProjectNumber, String PickUpDate, String InstallerNameID, String InstallerName, String PickUpNote, String UpdatedDate, String UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpDetail_UpdateByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpDate";
        if (PickUpDate != string.Empty)
            param.Value = PickUpDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerNameID";
        if (InstallerNameID != string.Empty)
            param.Value = InstallerNameID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpNote";
        if (PickUpNote != string.Empty)
            param.Value = PickUpNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (UpdatedDate != string.Empty)
            param.Value = UpdatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_StockPickUpReturnDetail_UpdateByProjectID(String ProjectID, string ProjectNumber, String PickUpReturnDate, String PickUpReturnNote, String InstallerID, String Installer, String UpdatedDate, String UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpReturnDetail_UpdateByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpReturnDate";
        if (PickUpReturnDate != string.Empty)
            param.Value = PickUpReturnDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpReturnNote";
        if (PickUpReturnNote != string.Empty)
            param.Value = PickUpReturnNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (UpdatedDate != string.Empty)
            param.Value = UpdatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_StockPickUpDetail_DeleteByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpDetail_DeleteByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_StockPickUpReturnDetail_DeleteByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_StockPickUpReturnDetail_DeleteByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblPickUpChangeReason_SelectByPId(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickUpChangeReason_SelectByPId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblPickUpChangeReason_DeleteByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickUpChangeReason_DeleteByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblPickUpChangeReason_Insert(String ProjectID, string ProjectNumber, string ReasonID, string Reason, string Notes, string PickUpDate, string PInstallerID, string PickUpNote, string UpdatedBy, string UpdatedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickUpChangeReason_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReasonID";
        if (ReasonID != string.Empty)
            param.Value = ReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpDate";
        if (PickUpDate != string.Empty)
            param.Value = PickUpDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PInstallerID";
        if (PInstallerID != string.Empty)
            param.Value = PInstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpNote";
        if (PickUpNote != string.Empty)
            param.Value = PickUpNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (UpdatedDate != string.Empty)
            param.Value = UpdatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    #endregion

    #region StockUpdate Popup

    public static int tblStockUpdate_Insert(String ProjectID, string ProjectNumber, string stockupdate_statusid, string stockupdate_status, string stockupdate_date, string stockupdate_note, string stockupdate_projno, string InstallBookingDate, string InstallerID, string DesignerID, string ElectricianID, String UpdatedDate, String UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockUpdate_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockupdate_statusid";
        if (stockupdate_statusid != string.Empty)
            param.Value = stockupdate_statusid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockupdate_status";
        if (stockupdate_status != string.Empty)
            param.Value = stockupdate_status;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockupdate_date";
        if (stockupdate_date != string.Empty)
            param.Value = stockupdate_date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockupdate_note";
        if (stockupdate_note != string.Empty)
            param.Value = stockupdate_note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockupdate_projno";
        if (stockupdate_projno != string.Empty)
            param.Value = stockupdate_projno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookingDate";
        if (InstallBookingDate != string.Empty)
            param.Value = InstallBookingDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerID";
        if (DesignerID != string.Empty)
            param.Value = DesignerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianID";
        if (ElectricianID != string.Empty)
            param.Value = ElectricianID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (UpdatedDate != string.Empty)
            param.Value = UpdatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockUpdate_DeleteByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockUpdate_DeleteByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PicklistItemsDetail_Delete(string PickId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemsDetail_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListItemId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }


    public static int tbl_PicklistItemDetail_Insert(string PickId, string StockItemId, string Picklistitem, string OrderQuantity, string Picklistlocation, string PicklistCategoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemId";
        if (StockItemId != string.Empty)
            param.Value = StockItemId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Picklistitem";
        if (Picklistitem != string.Empty)
            param.Value = Picklistitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        if (OrderQuantity != string.Empty)
            param.Value = OrderQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Picklistlocation";
        if (Picklistlocation != string.Empty)
            param.Value = Picklistlocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistCategoryId";
        if (PicklistCategoryId != string.Empty)
            param.Value = PicklistCategoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_PicklistItemDetail_Update(string PickId, string StockCategoryId, string StockItemId, string StockItemName, string Qty)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_Update";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryId";
        if (StockCategoryId != string.Empty)
            param.Value = StockCategoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@StockItemId";
        if (StockItemId != string.Empty)
            param.Value = StockItemId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@StockItemName";
        if (StockItemName != string.Empty)
            param.Value = StockItemName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Qty";
        if (Qty != string.Empty)
            param.Value = Qty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;

        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_ReasonandNotes(string PickId, string Reason, string Note)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_ReasonandNotes";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Note";
        if (Note != string.Empty)
            param.Value = Note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);




        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_InstallBookedDate(string PickId, string InstallBookedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_InstallBookedDate";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookedDate";
        if (InstallBookedDate != string.Empty)
            param.Value = InstallBookedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_IsToponeByProjectId(string ProjectID, string IsTopone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_IsToponeByProjectId";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTopone";
        param.Value = IsTopone;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tbl_PickListLog_upateCompanyid(string PickListId, string Companyid, string Projectnumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_upateCompanyid";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Companyid";
        param.Value = Companyid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Projectnumber";
        param.Value = Projectnumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tbl_PickListLog_Update_IsToponeByPickListId(string PickId, string IsTopone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_IsToponeByPickListId";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTopone";
        param.Value = IsTopone;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_updateInstallerAndEcDetails(string PickId, string installerid, string Installername, string designerid, string ElecId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_updateInstallerAndEcDetails";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@picklIstId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerId";
        if (installerid != string.Empty)
            param.Value = installerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Installername";
        if (Installername != string.Empty)
            param.Value = Installername;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerId";
        if (designerid != string.Empty)
            param.Value = designerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecId";
        if (ElecId != string.Empty)
            param.Value = ElecId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblProjects_ItemDetailsWise(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_ItemDetailsWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    #endregion
    public static DataTable spexistsPicklIstdata(string StockId, string AccreditionNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "spexistsPicklIstdata";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockItemId";
        param.Value = StockId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@accno";
        param.Value = AccreditionNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();

        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_SelectbyInstaller(string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectbyInstaller";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int Inserttbl_Walletdata_Picklistwise(string stockId, string Acno, string qty, string picklistId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Inserttbl_Walletdata_Picklistwise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockItemId";
        if (stockId != string.Empty)
            param.Value = stockId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@acno";
        if (Acno != string.Empty)
            param.Value = Acno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@qty";
        if (qty != string.Empty)
            param.Value = qty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@picklistId";
        if (picklistId != string.Empty)
            param.Value = picklistId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;

    }

    public static int Inserttbl_tbl_Wallet_ItemDetail(string stockId, string Acno, string qty)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Insert_tbl_Wallet_ItemDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockItemId";
        if (stockId != string.Empty)
            param.Value = stockId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@accno";
        if (Acno != string.Empty)
            param.Value = Acno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@qty";
        if (qty != string.Empty)
            param.Value = qty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;

    }
    public static bool Update_tbl_PicklistItemDetail_Qty(string StockId, string qty, string pickId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Update_tbl_PicklistItemDetail_Qty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pickId";
        param.Value = pickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@orderQty";
        param.Value = qty;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public static bool tbl_PickListLog_Update_locationId_CompanyWise(string PickListId, string LocationId, string CompanyId, string ProjectName, string RoofType, string RooftypeId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_locationId_CompanyWise";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locationId";
        if (LocationId != string.Empty)
            param.Value = LocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project";
        if (ProjectName != string.Empty)
            param.Value = ProjectName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeId";
        if (RooftypeId != string.Empty)
            param.Value = RooftypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofType";
        if (RoofType != string.Empty)
            param.Value = RoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public static bool tbl_projects_updateqty(string itemid, string ProjectID, string itemqty)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projects_updateqty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@itemid";
        param.Value = itemid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@itemqty";
        param.Value = itemqty;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public static int tblPickListLog_ExistsByProjectID_CompanyId(string ProjectID, string CompanyId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPickListLog_ExistsByProjectID_CompanyId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        param.Value = CompanyId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
    public static bool Updatetbl_tbl_Wallet_ItemDetail(string StockId, string qty, string acno)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Updatetbl_tbl_Wallet_ItemDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockItemId";
        param.Value = StockId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@qty";
        param.Value = qty;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@acno";
        param.Value = acno;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }
    public static bool deletePicklistItemById(string PickIdlist)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemsDetail_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@deletePicklistItemById";
        param.Value = PickIdlist;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public static bool tbl_PicklistItemsDetail_remove(string PickIdlist)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemsDetail_remove";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListItemId";
        param.Value = PickIdlist;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public static bool tbl_PickListLog_Update_InstallCompleteDate(string PrjId, string CompanyId, string InstallDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_InstallCompleteDate";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectId";
        param.Value = PrjId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallCompDate";
        if (InstallDate != string.Empty)
            param.Value = InstallDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }
    public static bool tblProjects_STCValues(string ProjectID, string RECRebate, string STCZoneRating, string SystemCapKW, string STCNumber, string PanelBrandID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_STCValues";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RECRebate";
        if (RECRebate != string.Empty)
            param.Value = RECRebate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCZoneRating";
        if (STCZoneRating != string.Empty)
            param.Value = STCZoneRating;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SystemCapKW";
        if (SystemCapKW != string.Empty)
            param.Value = SystemCapKW;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNumber";
        if (STCNumber != string.Empty)
            param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PanelBrandID";
        if (PanelBrandID != string.Empty)
            param.Value = PanelBrandID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }
    public static DataTable tbl_PickListLog_SelectByPId_CompnayID(string ProjectID, string CompId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_SelectByPId_CompnayID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        param.Value = CompId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable sp_GetMinstock(string Stockitemid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "sp_GetMinstock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Stockitemid";
        param.Value = Stockitemid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable sp_GetMinstock_Inverter(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "sp_GetMinstock_Inverter";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectnumber";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItemsLocation_SelectLiveQty(string stockitemid, string companylocationid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_SelectLiveQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemid";
        param.Value = stockitemid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@companylocationid";
        if (companylocationid != string.Empty)
            param.Value = companylocationid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItemsLocation_SelectLiveQty_Qty_Pickist(string stockitemid, string companylocationid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsLocation_SelectLiveQty_Qty_Pickist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemid";
        param.Value = stockitemid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@companylocationid";
        if (companylocationid != string.Empty)
            param.Value = companylocationid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_PicklistNotes_Select(string picklistid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistNotes_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@picklistid";
        param.Value = picklistid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tbl_PicklistNotes_Insert(string picklistid, string Projectid, string Notes)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistNotes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@picklistid";
        if (picklistid != string.Empty)
            param.Value = picklistid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Projectid";
        if (Projectid != string.Empty)
            param.Value = Projectid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;

    }
    public static bool tbl_PicklistNotes_delete(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistNotes_delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public static DataTable tblProjects_SelectOrderQty(string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectOrderQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectOrderQty_ETA(string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectOrderQty_ETA";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrderItems_SelectByStockItemIDandCompanyLocationID(string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_SelectByStockItemIDandCompanyLocationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_WholesaleOrderItems_SelectByStockItemIDandCompanyLocationID(string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectByStockItemIDandCompanyLocationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_SelectWholesaleQty(string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectWholesaleQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@CompanyLocationID";
        //param.Value = CompanyLocationID;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable GetItemCountompanyWise(string StockItemID, string CompanyID, string Loc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetItemCountompanyWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        //param.Value = StockItemID;
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Loc";
        if (Loc != string.Empty)
            param.Value = Loc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Comapany_Id";
        if (CompanyID != string.Empty)
            param.Value = CompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_PickListLog_SelectBy_CompnayID(string CompId, string StockItemId, string LocId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PickListLog_SelectBy_CompnayID";

        DbParameter param = comm.CreateParameter();

        param.ParameterName = "@CompanyID";

        if (CompId != string.Empty)
            param.Value = CompId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@stockItemId";
        if (StockItemId != string.Empty)
            param.Value = StockItemId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locId";
        if (LocId != string.Empty)
            param.Value = LocId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static SttblProjects tblProjects_SelectByProjectIDForSolarMinor(string ProjectID)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjects details = new SttblProjects();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.ProjectTypeID = dr["ProjectTypeID"].ToString();
            details.ProjectStatusID = dr["ProjectStatusID"].ToString();
            details.ProjectCancelID = dr["ProjectCancelID"].ToString();
            details.ProjectOnHoldID = dr["ProjectOnHoldID"].ToString();
            details.ProjectOpened = dr["ProjectOpened"].ToString();
            details.ProjectCancelled = dr["ProjectCancelled"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.OldProjectNumber = dr["OldProjectNumber"].ToString();
            details.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
            details.Project = dr["Project"].ToString();
            details.readyactive = dr["readyactive"].ToString();
            details.NextMaintenanceCall = dr["NextMaintenanceCall"].ToString();
            details.InstallAddress = dr["InstallAddress"].ToString();
            details.InstallCity = dr["InstallCity"].ToString();
            details.InstallState = dr["InstallState"].ToString();
            details.InstallPostCode = dr["InstallPostCode"].ToString();
            details.ProjectNotes = dr["ProjectNotes"].ToString();
            details.AdditionalSystem = dr["AdditionalSystem"].ToString();
            details.GridConnected = dr["GridConnected"].ToString();
            details.RebateApproved = dr["RebateApproved"].ToString();
            details.ReceivedCredits = dr["ReceivedCredits"].ToString();
            details.CreditEligible = dr["CreditEligible"].ToString();
            details.MoreThanOneInstall = dr["MoreThanOneInstall"].ToString();
            details.RequiredCompliancePaperwork = dr["RequiredCompliancePaperwork"].ToString();
            details.OutOfPocketDocs = dr["OutOfPocketDocs"].ToString();
            details.OwnerGSTRegistered = dr["OwnerGSTRegistered"].ToString();
            details.OwnerABN = dr["OwnerABN"].ToString();
            details.HouseTypeID = dr["HouseTypeID"].ToString();
            details.RoofTypeID = dr["RoofTypeID"].ToString();
            details.RoofAngleID = dr["RoofAngleID"].ToString();
            details.InstallBase = dr["InstallBase"].ToString();
            details.StockAllocationStore = dr["StockAllocationStore"].ToString();
            details.StandardPack = dr["StandardPack"].ToString();
            details.PanelBrandID = dr["PanelBrandID"].ToString();
            details.PanelBrand = dr["PanelBrand"].ToString();
            details.PanelModel = dr["PanelModel"].ToString();
            details.PanelOutput = dr["PanelOutput"].ToString();
            details.PanelDetails = dr["PanelDetails"].ToString();
            details.InverterDetailsID = dr["InverterDetailsID"].ToString();
            details.SecondInverterDetailsID = dr["SecondInverterDetailsID"].ToString();
            details.ThirdInverterDetailsID = dr["ThirdInverterDetailsID"].ToString();
            details.InverterBrand = dr["InverterBrand"].ToString();
            details.InverterModel = dr["InverterModel"].ToString();
            details.InverterSeries = dr["InverterSeries"].ToString();
            details.InverterOutput = dr["InverterOutput"].ToString();
            details.SecondInverterOutput = dr["SecondInverterOutput"].ToString();
            details.ThirdInverterOutput = dr["ThirdInverterOutput"].ToString();
            details.TotalInverterOutput = dr["TotalInverterOutput"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.InverterDetails = dr["InverterDetails"].ToString();
            details.SystemDetails = dr["SystemDetails"].ToString();
            details.NumberPanels = dr["NumberPanels"].ToString();
            details.PreviousNumberPanels = dr["PreviousNumberPanels"].ToString();
            details.PanelConfigNW = dr["PanelConfigNW"].ToString();
            details.PanelConfigOTH = dr["PanelConfigOTH"].ToString();
            details.SystemCapKW = dr["SystemCapKW"].ToString();
            details.STCMultiplier = dr["STCMultiplier"].ToString();
            details.STCZoneRating = dr["STCZoneRating"].ToString();
            details.STCNumber = dr["STCNumber"].ToString();
            details.STCValue = dr["STCValue"].ToString();
            details.ElecRetailerID = dr["ElecRetailerID"].ToString();
            details.ElecDistributorID = dr["ElecDistributorID"].ToString();
            details.ElecDistApplied = dr["ElecDistApplied"].ToString();
            details.ElecDistApplyMethod = dr["ElecDistApplyMethod"].ToString();
            details.ElecDistApplyBy = dr["ElecDistApplyBy"].ToString();
            details.ElecDistApplySentFrom = dr["ElecDistApplySentFrom"].ToString();
            details.ElecDistApproved = dr["ElecDistApproved"].ToString();
            details.ElecDistApprovelRef = dr["ElecDistApprovelRef"].ToString();
            details.ElecDistOK = dr["ElecDistOK"].ToString();
            details.RegPlanNo = dr["RegPlanNo"].ToString();
            details.LotNumber = dr["LotNumber"].ToString();
            details.Asbestoss = dr["Asbestoss"].ToString();
            details.MeterUG = dr["MeterUG"].ToString();
            details.MeterEnoughSpace = dr["MeterEnoughSpace"].ToString();
            details.SplitSystem = dr["SplitSystem"].ToString();
            details.CherryPicker = dr["CherryPicker"].ToString();
            details.TravelTime = dr["TravelTime"].ToString();
            details.VariationOther = dr["VariationOther"].ToString();
            details.VarRoofType = dr["VarRoofType"].ToString();
            details.VarRoofAngle = dr["VarRoofAngle"].ToString();
            details.VarHouseType = dr["VarHouseType"].ToString();
            details.VarAsbestos = dr["VarAsbestos"].ToString();
            details.VarMeterInstallation = dr["VarMeterInstallation"].ToString();
            details.VarMeterUG = dr["VarMeterUG"].ToString();
            details.VarTravelTime = dr["VarTravelTime"].ToString();
            details.HotWaterMeter = dr["HotWaterMeter"].ToString();
            details.SmartMeter = dr["SmartMeter"].ToString();
            details.VarSplitSystem = dr["VarSplitSystem"].ToString();
            details.VarEnoughSpace = dr["VarEnoughSpace"].ToString();
            details.VarCherryPicker = dr["VarCherryPicker"].ToString();
            details.VarOther = dr["VarOther"].ToString();
            details.SpecialDiscount = dr["SpecialDiscount"].ToString();
            details.collectioncharge = dr["collectioncharge"].ToString();

            details.DepositRequired = dr["DepositRequired"].ToString();
            details.TotalQuotePrice = dr["TotalQuotePrice"].ToString();
            details.PreviousTotalQuotePrice = dr["PreviousTotalQuotePrice"].ToString();
            details.InvoiceExGST = dr["InvoiceExGST"].ToString();
            details.InvoiceGST = dr["InvoiceGST"].ToString();
            details.BalanceGST = dr["BalanceGST"].ToString();
            details.FinanceWithID = dr["FinanceWithID"].ToString();
            details.ServiceValue = dr["ServiceValue"].ToString();
            details.QuoteSent = dr["QuoteSent"].ToString();
            details.QuoteSentNo = dr["QuoteSentNo"].ToString();
            details.QuoteAccepted = dr["QuoteAccepted"].ToString();
            details.SignedQuote = dr["SignedQuote"].ToString();
            details.MeterBoxPhotosSaved = dr["MeterBoxPhotosSaved"].ToString();
            details.ElecBillSaved = dr["ElecBillSaved"].ToString();
            details.ProposedDesignSaved = dr["ProposedDesignSaved"].ToString();
            details.FollowUp = dr["FollowUp"].ToString();
            details.FollowUpNote = dr["FollowUpNote"].ToString();
            details.InvoiceNumber = dr["InvoiceNumber"].ToString();
            details.InvoiceTag = dr["InvoiceTag"].ToString();
            details.InvoiceDoc = dr["InvoiceDoc"].ToString();
            details.InvoiceDetail = dr["InvoiceDetail"].ToString();
            details.InvoiceSent = dr["InvoiceSent"].ToString();
            details.InvoiceFU = dr["InvoiceFU"].ToString();
            details.InvoiceNotes = dr["InvoiceNotes"].ToString();
            details.InvRefund = dr["InvRefund"].ToString();
            details.InvRefunded = dr["InvRefunded"].ToString();
            details.InvRefundBy = dr["InvRefundBy"].ToString();
            details.DepositReceived = dr["DepositReceived"].ToString();
            details.DepositAmount = dr["DepositAmount"].ToString();
            details.ReceiptSent = dr["ReceiptSent"].ToString();
            details.SalesCommPaid = dr["SalesCommPaid"].ToString();
            details.InstallBookingDate = dr["InstallBookingDate"].ToString();
            details.MeterIncluded = dr["MeterIncluded"].ToString();
            details.MeterAppliedRef = dr["MeterAppliedRef"].ToString();
            details.MeterAppliedDate = dr["MeterAppliedDate"].ToString();
            details.MeterAppliedTime = dr["MeterAppliedTime"].ToString();
            details.MeterAppliedMethod = dr["MeterAppliedMethod"].ToString();
            details.MeterApprovedDate = dr["MeterApprovedDate"].ToString();
            details.MeterApprovalNo = dr["MeterApprovalNo"].ToString();
            details.MeterPhase = dr["MeterPhase"].ToString();
            details.OffPeak = dr["OffPeak"].ToString();
            details.NMINumber = dr["NMINumber"].ToString();
            details.meterupgrade = dr["meterupgrade"].ToString();
            details.MeterNumber1 = dr["MeterNumber1"].ToString();
            details.MeterNumber2 = dr["MeterNumber2"].ToString();
            details.MeterNumber3 = dr["MeterNumber3"].ToString();
            details.MeterNumber4 = dr["MeterNumber4"].ToString();
            details.MeterFU = dr["MeterFU"].ToString();
            details.MeterNotes = dr["MeterNotes"].ToString();
            details.OldSystemDetails = dr["OldSystemDetails"].ToString();
            details.REXAppliedRef = dr["REXAppliedRef"].ToString();
            details.REXAppliedDate = dr["REXAppliedDate"].ToString();
            details.REXStatusID = dr["REXStatusID"].ToString();
            details.REXApprovalNotes = dr["REXApprovalNotes"].ToString();
            details.REXApprovalFU = dr["REXApprovalFU"].ToString();
            details.STCPrice = dr["STCPrice"].ToString();
            details.RECRebate = dr["RECRebate"].ToString();
            details.BalanceRequested = dr["BalanceRequested"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.InstallAM1 = dr["InstallAM1"].ToString();
            details.InstallPM1 = dr["InstallPM1"].ToString();
            details.InstallAM2 = dr["InstallAM2"].ToString();
            details.InstallPM2 = dr["InstallPM2"].ToString();
            details.InstallDays = dr["InstallDays"].ToString();
            details.STCFormsDone = dr["STCFormsDone"].ToString();
            details.InstallDocsReceived = dr["InstallDocsReceived"].ToString();
            details.InstallerNotified = dr["InstallerNotified"].ToString();
            details.CustNotifiedInstall = dr["CustNotifiedInstall"].ToString();
            details.InstallerFollowUp = dr["InstallerFollowUp"].ToString();
            details.InstallerNotes = dr["InstallerNotes"].ToString();
            details.InstallerDocsSent = dr["InstallerDocsSent"].ToString();
            details.InstallCompleted = dr["InstallCompleted"].ToString();
            details.InstallVerifiedBy = dr["InstallVerifiedBy"].ToString();
            details.WelcomeLetterDone = dr["WelcomeLetterDone"].ToString();
            details.InstallRequestSaved = dr["InstallRequestSaved"].ToString();
            details.PanelSerials = dr["PanelSerials"].ToString();
            details.InverterSerial = dr["InverterSerial"].ToString();
            details.SecondInverterSerial = dr["SecondInverterSerial"].ToString();
            details.CertificateIssued = dr["CertificateIssued"].ToString();
            details.CertificateSaved = dr["CertificateSaved"].ToString();
            details.STCReceivedBy = dr["STCReceivedBy"].ToString();
            details.STCCheckedBy = dr["STCCheckedBy"].ToString();
            details.STCFormSaved = dr["STCFormSaved"].ToString();
            details.STCUploaded = dr["STCUploaded"].ToString();
            details.STCUploadNumber = dr["STCUploadNumber"].ToString();
            details.STCFU = dr["STCFU"].ToString();
            details.STCNotes = dr["STCNotes"].ToString();
            details.InstallationComment = dr["InstallationComment"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.PVDStatusID = dr["PVDStatusID"].ToString();
            details.STCApplied = dr["STCApplied"].ToString();
            details.BalanceReceived = dr["BalanceReceived"].ToString();
            details.Witholding = dr["Witholding"].ToString();
            details.BalanceVerified = dr["BalanceVerified"].ToString();
            details.InvoicePaid = dr["InvoicePaid"].ToString();
            details.InstallerNotifiedMeter = dr["InstallerNotifiedMeter"].ToString();
            details.CustNotifiedMeter = dr["CustNotifiedMeter"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.MeterInstallerFU = dr["MeterInstallerFU"].ToString();
            details.MeterInstallerNotes = dr["MeterInstallerNotes"].ToString();
            details.MeterJobBooked = dr["MeterJobBooked"].ToString();
            details.MeterCompleted = dr["MeterCompleted"].ToString();
            details.MeterInvoice = dr["MeterInvoice"].ToString();
            details.MeterInvoiceNo = dr["MeterInvoiceNo"].ToString();
            details.InstallerInvNo = dr["InstallerInvNo"].ToString();
            details.InstallerInvAmnt = dr["InstallerInvAmnt"].ToString();
            details.InstallerInvDate = dr["InstallerInvDate"].ToString();
            details.InstallerPayDate = dr["InstallerPayDate"].ToString();
            details.MeterElecInvNo = dr["MeterElecInvNo"].ToString();
            details.MeterElecInvAmnt = dr["MeterElecInvAmnt"].ToString();
            details.MeterElecInvDate = dr["MeterElecInvDate"].ToString();
            details.MeterElecPayDate = dr["MeterElecPayDate"].ToString();
            details.MeterElecFollowUp = dr["MeterElecFollowUp"].ToString();
            details.ElectricianInvoiceNotes = dr["ElectricianInvoiceNotes"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.SQ = dr["SQ"].ToString();
            details.MP = dr["MP"].ToString();
            details.EB = dr["EB"].ToString();
            details.PD = dr["PD"].ToString();
            details.InvoiceStatusID = dr["InvoiceStatusID"].ToString();
            details.ProjectEnteredBy = dr["ProjectEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.DepositeOption = dr["DepositeOption"].ToString();
            details.ST = dr["ST"].ToString();
            details.CE = dr["CE"].ToString();
            details.StatusComment = dr["StatusComment"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();
            details.StockDeductBy = dr["StockDeductBy"].ToString();
            details.StockDeductDate = dr["StockDeductDate"].ToString();
            details.PaymentTypeID = dr["PaymentTypeID"].ToString();
            details.ApplicationDate = dr["ApplicationDate"].ToString();
            details.AppliedBy = dr["AppliedBy"].ToString();
            details.PurchaseNo = dr["PurchaseNo"].ToString();
            details.DocumentSentDate = dr["DocumentSentDate"].ToString();
            details.DocumentSentBy = dr["DocumentSentBy"].ToString();
            details.DocumentReceivedDate = dr["DocumentReceivedDate"].ToString();
            details.DocumentReceivedBy = dr["DocumentReceivedBy"].ToString();
            details.DocumentVerified = dr["DocumentVerified"].ToString();
            details.SentBy = dr["SentBy"].ToString();
            details.SentDate = dr["SentDate"].ToString();
            details.PostalTrackingNumber = dr["PostalTrackingNumber"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.ReceivedDate = dr["ReceivedDate"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.PaymentVerified = dr["PaymentVerified"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvDocDoor = dr["InvDocDoor"].ToString();
            details.PreExtraWork = dr["PreExtraWork"].ToString();
            details.PreAmount = dr["PreAmount"].ToString();
            details.PreApprovedBy = dr["PreApprovedBy"].ToString();
            details.PaymentReceipt = dr["PaymentReceipt"].ToString();
            details.PR = dr["PR"].ToString();
            details.ActiveDate = dr["ActiveDate"].ToString();
            details.SalesComm = dr["SalesComm"].ToString();
            details.STCFormSign = dr["STCFormSign"].ToString();
            details.SerialNumbers = dr["SerialNumbers"].ToString();
            details.QuotationForm = dr["QuotationForm"].ToString();
            details.CustomerAck = dr["CustomerAck"].ToString();
            details.ComplianceCerti = dr["ComplianceCerti"].ToString();
            details.CustomerAccept = dr["CustomerAccept"].ToString();
            details.EWRNumber = dr["EWRNumber"].ToString();
            details.PatmentMethod = dr["PatmentMethod"].ToString();
            details.FlatPanels = dr["FlatPanels"].ToString();
            details.PitchedPanels = dr["PitchedPanels"].ToString();
            details.SurveyCerti = dr["SurveyCerti"].ToString();
            details.ElecDistAppDate = dr["ElecDistAppDate"].ToString();
            details.ElecDistAppBy = dr["ElecDistAppBy"].ToString();
            details.InstInvDoc = dr["InstInvDoc"].ToString();
            details.CertiApprove = dr["CertiApprove"].ToString();
            details.NewDate = dr["NewDate"].ToString();
            details.NewNotes = dr["NewNotes"].ToString();
            details.IsFormBay = dr["IsFormBay"].ToString();
            details.financewith = dr["financewith"].ToString();
            details.inverterqty = dr["inverterqty"].ToString();
            details.inverterqty2 = dr["inverterqty2"].ToString();
            details.inverterqty3 = dr["inverterqty3"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();

            details.SecondInverterDetails = dr["SecondInverterDetails"].ToString();
            details.ElecDistributor = dr["ElecDistributor"].ToString();
            details.ElecRetailer = dr["ElecRetailer"].ToString();
            details.HouseType = dr["HouseType"].ToString();
            details.RoofType = dr["RoofType"].ToString();
            details.RoofAngle = dr["RoofAngle"].ToString();
            details.PanelBrandName = dr["PanelBrandName"].ToString();
            details.InverterDetailsName = dr["InverterDetailsName"].ToString();
            details.Contact = dr["Contact"].ToString();
            details.InvoiceStatus = dr["InvoiceStatus"].ToString();
            details.SalesRepName = dr["SalesRepName"].ToString();
            details.InstallerName = dr["InstallerName"].ToString();
            details.DesignerName = dr["DesignerName"].ToString();
            details.ElectricianName = dr["ElectricianName"].ToString();
            details.FinanceWith = dr["FinanceWith"].ToString();
            details.PaymentType = dr["PaymentType"].ToString();
            details.StoreName = dr["StoreName"].ToString();
            details.Customer = dr["Customer"].ToString();

            details.FormbayId = dr["FormbayId"].ToString();
            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_address = dr["street_address"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.street_suffix = dr["street_suffix"].ToString();
            details.mtcepaperwork = dr["mtcepaperwork"].ToString();
            details.LinkProjectID = dr["LinkProjectID"].ToString();
            details.FormbayInstallBookingDate = dr["FormbayInstallBookingDate"].ToString();
            details.beatquote = dr["beatquote"].ToString();
            details.beatquotedoc = dr["beatquotedoc"].ToString();
            details.nearmap = dr["nearmapcheck"].ToString();
            details.nearmapdoc = dr["nearmapdoc"].ToString();
            details.SalesType = dr["SalesType"].ToString();
            details.projecttype = dr["projecttype"].ToString();
            details.projectcancel = dr["projectcancel"].ToString();
            details.PVDStatus = dr["PVDStatus"].ToString();
            details.DocumentSentByName = dr["DocumentSentByName"].ToString();
            details.DocumentReceivedByName = dr["DocumentReceivedByName"].ToString();
            details.Empname1 = dr["Empname1"].ToString();
            details.FDA = dr["FDA"].ToString();
            details.notes = dr["notes"].ToString();
            details.paydate = dr["paydate"].ToString();
            details.SSActiveDate = dr["SSActiveDate"].ToString();
            details.SSCompleteDate = dr["SSCompleteDate"].ToString();
            details.QuickForm = dr["QuickForm"].ToString();
            details.IsClickCustomer = dr["IsClickCustomer"].ToString();


            details.HouseStayDate = dr["HouseStayDate"].ToString();
            details.HouseStayID = dr["HouseStayID"].ToString();
            details.ComplainceCertificate = dr["ComplainceCertificate"].ToString();
            details.quickformGuid = dr["quickformGuid"].ToString();
            details.GreenBotFlag = dr["GreenBotFlag"].ToString();
            details.VicAppReference = dr["VicAppReference"].ToString();
            details.VicDate = dr["VicDate"].ToString();
            details.VicRebateNote = dr["VicRebateNote"].ToString();
            details.VicRebate = dr["VicRebate"].ToString();
            //details.IsSMReady = dr["IsSMReady"].ToString();
            details.STcYear = dr["Stcrate"].ToString();
            details.panelQty = dr["panelQty"].ToString();
            details.InvertQty1 = dr["InvertQty1"].ToString();
            details.InvertQty2 = dr["InvertQty2"].ToString();
            details.InvertQty3 = dr["InvertQty3"].ToString();
            details.nearmapdoc1 = dr["nearmapdoc1"].ToString();
            details.nearmapdoc2 = dr["nearmapdoc2"].ToString();


        }
        // return structure details
        return details;
    }
    #region ForService
    public static int tblProjects_ExistsByProjectID(string projectId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_ExistsByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = projectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
    public static bool tblProjects_UpdateDeductNote_FromService(
        string projectId, string projectnumber, string note,
        string pickupdate, string installerid, string pickupnote, string updatedby,
        string updateddate, string picklistid)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateDeductNote_FromService";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        param.Value = projectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (projectnumber != string.Empty)
            param.Value = projectnumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@note";
        if (note != string.Empty)
            param.Value = note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpDate";
        if (pickupdate != string.Empty)
            param.Value = pickupdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@installerId";
        if (installerid != string.Empty)
            param.Value = installerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@pickupnote";
        if (pickupnote != string.Empty)
            param.Value = pickupnote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@updatedby";
        if (updatedby != string.Empty)
            param.Value = updatedby;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        // param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (updateddate != string.Empty)
            param.Value = updateddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        // param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@picklistid";
        if (picklistid != string.Empty)
            param.Value = picklistid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        // param.Size = 200;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }

    public static bool tblprojectsUpdateStatus(string ProjectId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojectsUpdateStatus";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectId";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    #endregion ForService

    public static bool tbl_picklistlog_UpdateInverter(string PickId, string Inverter)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_picklistlog_UpdateInverter";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IInverter";
        param.Value = Inverter;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tbl_picklistlog_UpdateIPanel(string PickId, string Panel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_picklistlog_UpdateIPanel";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IPanel";
        param.Value = Panel;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tblprojects_UpdateCancelDate(string projectid, string CancelDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojects_UpdateCancelDate";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        param.Value = projectid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelDate";
        param.Value = CancelDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tblprojects_UpdateActiveDate(string projectid, string CancelDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojects_UpdateActiveDate";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        param.Value = projectid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelDate";
        param.Value = CancelDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static int tblProjectStatusUpdateData_Insert(
       string projectid, string projectno, string cust_id, string contact_id, string project_hold_reasonid, string project_hold_Reason, string Project_OnHoldDate, string project_cancel_Id, string project_cancelReason, string project_pre_cancel_Id, string Project_Cancel_Date, string Project_Pre_cancelReason, string Project_PreCancelDate, string updatedby)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatusUpdateData_Insert";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        param.Value = projectid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectno";
        if (projectno != null && projectno != "")
            param.Value = projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cust_id";
        if (cust_id != string.Empty && cust_id != "")
            param.Value = cust_id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contact_id";
        if (contact_id != null && contact_id != "")
            param.Value = contact_id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project_hold_reasonid";
        if (project_hold_reasonid != null && project_hold_reasonid != "")
            param.Value = project_hold_reasonid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project_hold_Reason";
        if (project_hold_Reason != null && project_hold_Reason != "")
            param.Value = project_hold_Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project_OnHoldDate";
        if (Project_OnHoldDate != null && Project_OnHoldDate != "")
            param.Value = Project_OnHoldDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project_cancel_Id";
        if (project_cancel_Id != null && project_cancel_Id != "")
            param.Value = project_cancel_Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project_cancelReason";
        if (project_cancelReason != null && project_cancelReason != "")
            param.Value = project_cancelReason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project_pre_cancel_Id";
        if (@project_pre_cancel_Id != null && @project_pre_cancel_Id != "")
            param.Value = @project_pre_cancel_Id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        //param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project_Cancel_Date";
        if (Project_Cancel_Date != null && Project_Cancel_Date != "")
            param.Value = Project_Cancel_Date;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project_Pre_cancelReason";
        if (Project_Pre_cancelReason != null && Project_Pre_cancelReason != "")
            param.Value = Project_Pre_cancelReason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Project_PreCancelDate";
        if (Project_PreCancelDate != null && Project_PreCancelDate != "")
            param.Value = Project_PreCancelDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        //param.Size = 500;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@updatedby";
        if (updatedby != string.Empty)
            param.Value = updatedby;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        // param.Size = 200;
        comm.Parameters.Add(param);



        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

        try
        {

        }
        catch (Exception ex)
        {
        }
        return id;

    }

    public static bool tblprojects_UpdateActiveOldDate(string projectid, string CancelDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblprojects_UpdateActiveOldDate";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = projectid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectCancelDate";
        param.Value = CancelDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tblProjects_UpdatMeterNo(string projectid, string meterno)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdatMeterNo";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projecetid";
        param.Value = projectid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@meterNo1";
        if (meterno != string.Empty)
            param.Value = meterno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }

   public static bool tblProjects_UpdateDate(string projectid, string projectnotes,string ElecDistApprovelRef,string ElecDistApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateDate";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projecetid";
        param.Value = projectid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@projectnotes";
        if (projectnotes != string.Empty)
            param.Value = projectnotes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApprovelRef";
        if (ElecDistApprovelRef != string.Empty)
            param.Value = ElecDistApprovelRef;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistApplied";
        if (ElecDistApplied  != string.Empty)
            param.Value = ElecDistApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
       
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
    public  static int tblApplicationNotes_Insert(string projectid, string projecnumber, string Notes,string Addedby)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblApplicationNotes_Insert";

        DbParameter param = comm.CreateParameter();

        param.ParameterName = "@projectId";

        if (projectid != string.Empty)
            param.Value = projectid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (projecnumber != string.Empty)
            param.Value = projecnumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@addedby";
        if (Addedby != string.Empty)
            param.Value = Addedby;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

            try
            {

            }
            catch (Exception ex)
            {
            }
            return id;
        }
    public static DataTable tblApplicationNotes_getData(String ProjectId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblApplicationNotes_getData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        param.Value = ProjectId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    public static bool tbl_picklistlog_updatePicklistType(string ID, string PicklistType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_picklistlog_updatePicklistType";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistType";
        param.Value = PicklistType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (ID != null && ID!="")
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }
    public static DataTable GetDraftPicklistCount(string StockItemID,string Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetDraftPicklistCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stockitemid";
        //param.Value = StockItemID;
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Picklistlocation";
        //param.Value = StockItemID;
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjects_UpdateRetailerDetails(string ProjectID, string ExportDetail, string KwExport, string KwNonExport, string GridConnectedSystem, string FeedInTariff, string ApproxExpectedPaybackPeriod)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_UpdateRetailerDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExportDetail";
        if (ExportDetail != string.Empty)
            param.Value = ExportDetail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@KwExport";
        if (KwExport != string.Empty)
            param.Value = KwExport;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@KwNonExport";
        if (KwNonExport != string.Empty)
            param.Value = KwNonExport;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@GridConnectedSystem";
        if (GridConnectedSystem != string.Empty)
            param.Value = GridConnectedSystem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@FeedInTariff";
        if (FeedInTariff != string.Empty)
            param.Value = FeedInTariff;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@ApproxExpectedPaybackPeriod";
        if (ApproxExpectedPaybackPeriod != string.Empty)
            param.Value = ApproxExpectedPaybackPeriod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblProjectAcknowledgement_Insert(string DocType, string ProjectId, string Name, string FullAddress, string Mobile, string Email, string CreateBy, string CreatedOn)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectAcknowledgement_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DocType";
        if (DocType != string.Empty)
            param.Value = DocType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@ProjectId";
        if (ProjectId != string.Empty)
            param.Value = ProjectId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Name";
        if (Name != string.Empty)
            param.Value = Name;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@FullAddress";
        if (FullAddress != string.Empty)
            param.Value = FullAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@Mobile";
        if (Mobile != string.Empty)
            param.Value = Mobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@Email";
        if (Email != string.Empty)
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreateBy";
        if (CreateBy != string.Empty)
            param.Value = CreateBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@CreatedOn";
        if (CreatedOn != string.Empty)
            param.Value = CreatedOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblProjectAcknowledgement_SelectByProjectID(string ProjectID, string DocType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectAcknowledgement_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocType";
        if (DocType != string.Empty)
            param.Value = DocType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectAcknowledgement_DocID(string DocID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectAcknowledgement_DocID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DocID";
        if (DocID != string.Empty)
            param.Value = DocID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblProjectAcknowledgement_UpdateYesNo(string id, string Yes, string No)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectAcknowledgement_UpdateYesNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Yes";
        if (Yes != string.Empty)
            param.Value = Yes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@No";
        if (No != string.Empty)
            param.Value = No;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }
}

