using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblSalesTeams
	{
		public string SalesTeam;
		public string Active;
		public string Seq;

	}


public class ClstblSalesTeams
{
	public static SttblSalesTeams tblSalesTeams_SelectBySalesTeamID (String SalesTeamID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblSalesTeams_SelectBySalesTeamID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@SalesTeamID";
		param.Value = SalesTeamID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblSalesTeams details = new SttblSalesTeams();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.SalesTeam = dr["SalesTeam"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblSalesTeams_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblSalesTeams_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblSalesTeams_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSalesTeams_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
	public static int tblSalesTeams_Insert ( String SalesTeam, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblSalesTeams_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@SalesTeam";
		param.Value = SalesTeam;
		param.DbType = DbType.String;
		param.Size = 30;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblSalesTeams_Update (string SalesTeamID, String SalesTeam, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblSalesTeams_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@SalesTeamID";
		param.Value = SalesTeamID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SalesTeam";
		param.Value = SalesTeam;
		param.DbType = DbType.String;
		param.Size = 30;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblSalesTeams_Delete (string SalesTeamID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblSalesTeams_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@SalesTeamID";
		param.Value = SalesTeamID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}



    public static int tblSalesTeams_Exists(string SalesTeam)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSalesTeams_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeam";
        param.Value = SalesTeam;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblSalesTeams_ExistsById(string SalesTeam, string SalesTeamID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSalesTeams_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SalesTeam";
        param.Value = SalesTeam;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblSalesTeams_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSalesTeams_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblSalesTeams_GetDataBySearch(string alpha,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSalesTeams_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblSalesTeams_bySalesTeamType(string type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSalesTeams_bySalesTeamType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@type";
        param.Value = type;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblSalesTeams_SelectDFI()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblSalesTeams_SelectDFI";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}