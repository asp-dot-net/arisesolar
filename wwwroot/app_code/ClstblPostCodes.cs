using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblPostCodes
{
    public string PostCode;
    public string Suburb;
    public string State;
    public string POBoxes;
    public string Area;
    public string upsize_ts;

}


public class ClstblPostCodes
{
    public static SttblPostCodes tblPostCodes_SelectByPostCodeID(String PostCodeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_SelectByPostCodeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PostCodeID";
        param.Value = PostCodeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblPostCodes details = new SttblPostCodes();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.PostCode = dr["PostCode"].ToString();
            details.Suburb = dr["Suburb"].ToString();
            details.State = dr["State"].ToString();
            details.POBoxes = dr["PO Boxes"].ToString();
            details.Area = dr["Area"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblPostCodes_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblPostCodes_Insert(String PostCode, String Suburb, String State, String POBoxes, String Area)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PostCode";
        param.Value = PostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Suburb";
        param.Value = Suburb;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        param.Value = State;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@POBoxes";
        param.Value = POBoxes;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        param.Value = Area;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblPostCodes_Update(string PostCodeID, String PostCode, String Suburb, String State, String POBoxes, String Area)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PostCodeID";
        param.Value = PostCodeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostCode";
        param.Value = PostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Suburb";
        param.Value = Suburb;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        param.Value = State;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@POBoxes";
        param.Value = POBoxes;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        param.Value = Area;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblPostCodes_InsertUpdate(Int32 PostCodeID, String PostCode, String Suburb, String State, String POBoxes, String Area, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PostCodeID";
        param.Value = PostCodeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostCode";
        param.Value = PostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Suburb";
        param.Value = Suburb;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        param.Value = State;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@POBoxes";
        param.Value = POBoxes;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Area";
        param.Value = Area;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        param.Value = upsize_ts;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblPostCodes_Delete(string PostCodeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPostCodes_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PostCodeID";
        param.Value = PostCodeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblPostCodes_GetDataBySearch(string PostCode, string Suburb, string State)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblPostCodes_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PostCode";
        if (PostCode != string.Empty)
            param.Value = PostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Suburb";
        if (Suburb != string.Empty)
            param.Value = Suburb;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}