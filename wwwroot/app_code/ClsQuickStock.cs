﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsQuickStock
/// </summary>
public class ClsQuickStock
{
    public struct SttblStockQuantity
    {
        public string StockLiveQty;
        public string ArisePickListQty;
        public string SMPickListQty;
        public string WholesaleQty;
        public string Total;
        public string OrderQty;
        public string ETAQty;
        public string MinQty;
    }

    public ClsQuickStock()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region Temp Code PickList Send in QuickStock
    public static DataTable tbl_Picklist_GetDataByID(string PicklilstID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Picklist_GetDataByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (PicklilstID != string.Empty)
            param.Value = PicklilstID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PicklistItemDetails_GetDataByID(string PicklilstID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetails_GetDataByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (PicklilstID != string.Empty)
            param.Value = PicklilstID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }
    #endregion

    public static int tblPickListLog_ExistsByProjectID_CompanyId(string ProjectID, string CompanyId)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tblPickListLog_ExistsByProjectID_CompanyId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        param.Value = CompanyId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = 0;
        result = Convert.ToInt32(DataAccessQuickStock.ExecuteScalar(comm));
        return result;
    }

    #region PickLost Create Code
    public static int tblPickListLog_Insert(String ProjectID, String Reason, String Note, String PickListDateTime, string InstallBookedDate, string InstallerID, string InstallerName, string DesignerID, string ElectricianID, String CreatedBy)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tblPickListLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Note";
        if (Note != string.Empty)
            param.Value = Note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListDateTime";
        if (PickListDateTime != string.Empty)
            param.Value = PickListDateTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookedDate";
        if (InstallBookedDate != string.Empty)
            param.Value = InstallBookedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerID";
        if (DesignerID != string.Empty)
            param.Value = DesignerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianID";
        if (ElectricianID != string.Empty)
            param.Value = ElectricianID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccessQuickStock.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_picklistlog_updatePicklistType(string ID, string PicklistType)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_picklistlog_updatePicklistType";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistType";
        param.Value = PicklistType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (ID != null && ID != "")
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccessQuickStock.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }

    public static bool tbl_PickListLog_Update_IsToponeByProjectId(string ProjectID, string IsTopone)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_IsToponeByProjectId";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTopone";
        param.Value = IsTopone;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_IsToponeByPickListId(string PickId, string IsTopone)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_IsToponeByPickListId";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsTopone";
        param.Value = IsTopone;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_upateCompanyid(string PickListId, string Companyid, string Projectnumber)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_upateCompanyid";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListId";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Companyid";
        param.Value = Companyid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Projectnumber";
        param.Value = Projectnumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_updateInstallerAndEcDetails(string PickId, string installerid, string Installername, string designerid, string ElecId)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_updateInstallerAndEcDetails";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@picklIstId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerId";
        if (installerid != string.Empty)
            param.Value = installerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Installername";
        if (Installername != string.Empty)
            param.Value = Installername;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerId";
        if (designerid != string.Empty)
            param.Value = designerid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecId";
        if (ElecId != string.Empty)
            param.Value = ElecId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_locationId_CompanyWise(string PickListId, string LocationId, string CompanyId, string ProjectName, string RoofType, string RooftypeId)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_locationId_CompanyWise";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickListId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@locationId";
        if (LocationId != string.Empty)
            param.Value = LocationId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyId";
        if (CompanyId != string.Empty)
            param.Value = CompanyId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@project";
        if (ProjectName != string.Empty)
            param.Value = ProjectName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeId";
        if (RooftypeId != string.Empty)
            param.Value = RooftypeId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofType";
        if (RoofType != string.Empty)
            param.Value = RoofType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tbl_picklistlog_UpdateIPanel(string PickId, string Panel)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_picklistlog_UpdateIPanel";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IPanel";
        param.Value = Panel;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tbl_picklistlog_UpdateInverter(string PickId, string Inverter)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_picklistlog_UpdateInverter";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IInverter";
        param.Value = Inverter;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static int tbl_PicklistItemDetail_Insert(string PickId, string StockItemId, string Picklistitem, string OrderQuantity, string Picklistlocation, string PicklistCategoryId)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickId";
        if (PickId != string.Empty)
            param.Value = PickId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemId";
        if (StockItemId != string.Empty)
            param.Value = StockItemId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Picklistitem";
        if (Picklistitem != string.Empty)
            param.Value = Picklistitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        if (OrderQuantity != string.Empty)
            param.Value = OrderQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Picklistlocation";
        if (Picklistlocation != string.Empty)
            param.Value = Picklistlocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PicklistCategoryId";
        if (PicklistCategoryId != string.Empty)
            param.Value = PicklistCategoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccessQuickStock.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tbl_PickListLog_SelectByPId_CompnayID(string ProjectID, string CompId)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_SelectByPId_CompnayID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        param.Value = CompId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PicklistItemDetail_GetDataByPickID(string PicklilstID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_GetDataByPickID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklilstID";
        if (PicklilstID != string.Empty)
            param.Value = PicklilstID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        }
        catch(Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblPickListLog_Update(string PicklistID, string Reason, string Note, string InstallBookedDate, string InstallerID, string InstallerName, string DesignerID, string ElectricianID, string CreatedBy)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tblPickListLog_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PicklistID";
        if (PicklistID != string.Empty)
            param.Value = PicklistID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Note";
        if (Note != string.Empty)
            param.Value = Note;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookedDate";
        if (InstallBookedDate != string.Empty)
            param.Value = InstallBookedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerName";
        if (InstallerName != string.Empty)
            param.Value = InstallerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DesignerID";
        if (DesignerID != string.Empty)
            param.Value = DesignerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElectricianID";
        if (ElectricianID != string.Empty)
            param.Value = ElectricianID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccessQuickStock.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);

    }

    public static bool tbl_PicklistItemsDetail_Delete(string PickId)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PicklistItemsDetail_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PickListItemId";
        param.Value = PickId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;

        result = DataAccessQuickStock.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_DeleteByID(string ID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_DeleteByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccessQuickStock.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    #endregion

    public static int tblMaintainHistory_ExistsBySectionID(string SectionID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tblMaintainHistory_ExistsBySectionID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SectionID";
        param.Value = SectionID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = 0;
        result = Convert.ToInt32(DataAccessQuickStock.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tbl_PicklistLog_GetDeleteFlagByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PicklistLog_GetDeleteFlagByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_PicklistItemDetail_LastPickListWise(String ProjectID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PicklistItemDetail_LastPickListWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_Picklistlog_LastPickListWise(string ProjectID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_Picklistlog_LastPickListWise";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static SttblStockQuantity Sp_GetPanelCount_ByStockItemLocationID(string StockItemID, string CompanyLocationID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "Sp_GetPanelCount_ByStockItemLocationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable result = new DataTable();
        try
        {
            result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        }
        catch(Exception ex) { }

        // wrap retrieved data into a SttblStockQuantity object
        SttblStockQuantity Details = new SttblStockQuantity();
        if (result.Rows.Count > 0)
        {
            // Get the first table row
            DataRow dr = result.Rows[0];

            // Get StockQuantity details
            Details.StockLiveQty = dr["StockLiveQty"].ToString();
            Details.ArisePickListQty = dr["ArisePickListQty"].ToString();
            Details.SMPickListQty = dr["SMPickListQty"].ToString();
            Details.WholesaleQty = dr["WholesaleQty"].ToString();
            Details.Total = dr["Total"].ToString();
            Details.OrderQty = dr["OrderQty"].ToString();
            Details.ETAQty = dr["ETAQty"].ToString();
            Details.MinQty = dr["MinQty"].ToString();
        }
        // return structure details
        return Details;
    }

    public static DataTable tblPickUpChangeReason_SelectByPId(string ProjectID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tblPickUpChangeReason_SelectByPId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        
        try
        {
            result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_StockPickUpDetail_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_StockPickUpDetail_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        
        try
        {
            result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_StockPickUpReturnDetail_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_StockPickUpReturnDetail_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblPickUpChangeReason_Insert(String ProjectID, string ProjectNumber, string ReasonID, string Reason, string Notes, string PickUpDate, string PInstallerID, string PickUpNote, string UpdatedBy, string UpdatedDate)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tblPickUpChangeReason_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReasonID";
        if (ReasonID != string.Empty)
            param.Value = ReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reason";
        if (Reason != string.Empty)
            param.Value = Reason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpDate";
        if (PickUpDate != string.Empty)
            param.Value = PickUpDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PInstallerID";
        if (PInstallerID != string.Empty)
            param.Value = PInstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickUpNote";
        if (PickUpNote != string.Empty)
            param.Value = PickUpNote;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedDate";
        if (UpdatedDate != string.Empty)
            param.Value = UpdatedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccessQuickStock.ExecuteScalar(comm));
        return id;
    }

    public static string tblStockItems_GetFixItemIDByStockItemID(string CompanyID, string StockItemID)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tblStockItems_GetFixItemIDByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyID";
        if (CompanyID != string.Empty)
            param.Value = CompanyID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        string id = "";
        try
        {
            id = DataAccessQuickStock.ExecuteScalar(comm);
        }
        catch (Exception ex)
        {
            
        }
        return id;
    }

    public static bool tbl_PickListLog_Update_InstallBookedDateByProjectID(string ProjectID, string InstallBookedDate)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_InstallBookedDateByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallBookedDate";
        if (InstallBookedDate != string.Empty)
            param.Value = InstallBookedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccessQuickStock.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_PickListCategory_GetData()
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListCategory_GetData";

        DataTable result = new DataTable();
        try
        {
            result = DataAccessQuickStock.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_PickListLog_UpdatePickListCategory(string ID, string PickListCategory)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_UpdatePickListCategory";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PickListCategory";
        if (PickListCategory != string.Empty)
            param.Value = PickListCategory;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccessQuickStock.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }

    public static bool tbl_PickListLog_Update_stockWith(string ID, string stockWith)
    {
        DbCommand comm = DataAccessQuickStock.CreateCommand();
        comm.CommandText = "tbl_PickListLog_Update_stockWith";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        if (ID != string.Empty)
            param.Value = ID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockWith";
        if (stockWith != string.Empty)
            param.Value = stockWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccessQuickStock.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
}