using System;
using System.Data;
using System.Data.Common;

public struct SttblStockItems
{
    public string StockItemID;
    public string StockCategoryID;
    public string StockCategory;
    public string StockItem;
    public string StockManufacturer;
    public string StockModel;
    public string StockSeries;
    public string StockSize;
    public string ShortName;
    public string StockDescription;
    public string StockCode;
    public string InverterCert;
    public string StockQuantity;
    public string StockLocation;
    public string MinLevel;
    public string Reserved;
    public string PlanA;
    public string PlanB;
    public string PlanC;
    public string MinPrice;
    public string CostPrice;
    public string CustomerPrice;
    public string Active;
    public string Active1;
    public string SalesTag;
    public string DeleteTag;
    public string LastStockTake;
    public string PrevStockTake;
    public string upsize_ts;
    public string IsDashboard;
    public string FixStockItemID;
    public string FileName;
    public string FileUploadedBy;
    public string LastFileUploadedDate;
    public string RespSup;
    public string ExpiryDate;
}

public class ClstblStockItems
{
    public static SttblStockItems tblStockItems_SelectByStockItemID(String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockItems details = new SttblStockItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockItemID = dr["StockItemID"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockCategory = dr["StockCategory"].ToString();
            details.StockItem = dr["StockItem"].ToString();
            details.StockManufacturer = dr["StockManufacturer"].ToString();
            details.StockModel = dr["StockModel"].ToString();
            details.StockSeries = dr["StockSeries"].ToString();
            details.StockSize = dr["StockSize"].ToString();
            details.ShortName = dr["ShortName"].ToString();
            details.StockDescription = dr["StockDescription"].ToString();
            details.StockCode = dr["StockCode"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.StockQuantity = dr["StockQuantity"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();
            details.MinLevel = dr["MinLevel"].ToString();
            details.Reserved = dr["Reserved"].ToString();
            details.PlanA = dr["PlanA"].ToString();
            details.PlanB = dr["PlanB"].ToString();
            details.PlanC = dr["PlanC"].ToString();
            details.MinPrice = dr["MinPrice"].ToString();
            details.CostPrice = dr["CostPrice"].ToString();
            details.CustomerPrice = dr["CustomerPrice"].ToString();
            details.Active = dr["Active"].ToString();
            details.Active1 = dr["Active1"].ToString();
            details.SalesTag = dr["SalesTag"].ToString();
            details.DeleteTag = dr["DeleteTag"].ToString();
            details.LastStockTake = dr["LastStockTake"].ToString();
            details.PrevStockTake = dr["PrevStockTake"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.IsDashboard = dr["IsDashboard"].ToString();
            details.FixStockItemID = dr["FixStockItemID"].ToString();
            details.FileName = dr["FileName"].ToString();
            details.FileUploadedBy = dr["FileUploadedBy"].ToString();
            details.LastFileUploadedDate = dr["LastFileUploadedDate"].ToString();
            details.RespSup = dr["RespSup"].ToString();
            details.ExpiryDate = dr["ExpiryDate"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblStockItems_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectElect(string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectElect";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectElectMount(string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectElectMount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectAll()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectAll";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItems_SelectbyAsc(string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectbyAsc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_Select_ByCategory_Location(string StockCategoryID, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Select_ByCategory_Location";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_Select_Category(string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Select_Category";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectByCategoryID_CompanyLocationID(string StockCategoryID, string LocationFrom, string LocationTo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByCategoryID_CompanyLocationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        param.Value = LocationFrom;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        param.Value = LocationTo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItems_SelectByItemID_CompanyLocationID(string StockItemID, string LocationFrom, string LocationTo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByItemID_CompanyLocationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationFrom";
        param.Value = LocationFrom;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationTo";
        param.Value = LocationTo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_GetMaxStockCode()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_GetMaxStockCode";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblStockItems_Insert(String StockCategoryID, String StockItem, String StockManufacturer, String StockModel, String StockSeries, String MinLevel, String Active, String SalesTag, String StockDescription, string StockSize, string IsDashboard)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        param.Value = StockItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        param.Value = StockSeries;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MinLevel";
        if (MinLevel != string.Empty)
            param.Value = MinLevel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        param.Value = SalesTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDescription";
        param.Value = StockDescription;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDashboard";
        param.Value = IsDashboard;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblStockItems_InsertWithFixStockItemID(String StockCategoryID, String StockItem, String StockManufacturer, String StockModel, String StockSeries, String MinLevel, String Active, String SalesTag, String StockDescription, string StockSize, string IsDashboard, string FixStockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_InsertWithFixStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        param.Value = StockItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        param.Value = StockSeries;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MinLevel";
        if (MinLevel != string.Empty)
            param.Value = MinLevel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        param.Value = SalesTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDescription";
        param.Value = StockDescription;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDashboard";
        param.Value = IsDashboard;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FixStockItemID";
        if (FixStockItemID != string.Empty)
            param.Value = FixStockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblStockItems_InsertModuleFile(String StockCategoryID, String StockItem, String StockManufacturer, String StockModel, String StockSeries, String MinLevel, String Active, String SalesTag, String StockDescription, string StockSize, string IsDashboard,String ApprovedDate,String ExpiryDate,String FireTested, String ACPower)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_InsertModuleFile";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        if (StockManufacturer != string.Empty)
            param.Value = StockManufacturer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        if (StockSeries != string.Empty)
            param.Value = StockSeries;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MinLevel";
        if (MinLevel != string.Empty)
            param.Value = MinLevel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        param.Value = SalesTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDescription";
        if (StockDescription != string.Empty)
            param.Value = StockDescription;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDashboard";
        param.Value = IsDashboard;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedDate";
        if (ApprovedDate != string.Empty)
            param.Value = ApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpiryDate";
        if (ExpiryDate != string.Empty)
            param.Value = ExpiryDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FireTested";
        if (FireTested != string.Empty)
            param.Value = FireTested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ACPower";
        if (ACPower != string.Empty)
            param.Value = ACPower;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockItems_Update(string StockItemID, String StockCategoryID, String StockItem, String StockManufacturer, String StockModel, String StockSeries, String MinLevel, String Active, String SalesTag, String StockDescription, string StockSize, string IsDashboard)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        param.Value = StockItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        param.Value = StockSeries;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MinLevel";
        if (MinLevel != string.Empty)
            param.Value = MinLevel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        if (SalesTag != string.Empty)
            param.Value = SalesTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDescription";
        param.Value = StockDescription;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDashboard";
        param.Value = IsDashboard;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_UpdateFileName(string StockItemID,string FileName,string FileUploadedBy,string LastFileUploadedDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_UpdateFileName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FileName";
        if (FileName != string.Empty)
            param.Value = FileName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FileUploadedBy";
        if (FileUploadedBy != string.Empty)
            param.Value = FileUploadedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LastFileUploadedDate";
        if (LastFileUploadedDate != string.Empty)
            param.Value = LastFileUploadedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_UpdateWithFixStockItemID(string StockItemID, String StockCategoryID, String StockItem, String StockManufacturer, String StockModel, String StockSeries, String MinLevel, String Active, String SalesTag, String StockDescription, string StockSize, string IsDashboard,string FixStockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_UpdateWithFixStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        param.Value = StockItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        param.Value = StockSeries;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MinLevel";
        if (MinLevel != string.Empty)
            param.Value = MinLevel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        if (SalesTag != string.Empty)
            param.Value = SalesTag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDescription";
        param.Value = StockDescription;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        if (StockSize != string.Empty)
            param.Value = StockSize;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDashboard";
        param.Value = IsDashboard;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FixStockItemID";
        if (FixStockItemID != string.Empty)
            param.Value = FixStockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);        

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_UpdateByFile( String StockManufacturer, String StockModel, String ApprovedDate, String ExpiryDate, String FireTested,String ACPower,String StockSeries)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_UpdateByFile";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedDate";
        if (ApprovedDate != string.Empty)
            param.Value = ApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpiryDate";
        if (ExpiryDate != string.Empty)
            param.Value = ExpiryDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FireTested";
        if (FireTested != string.Empty)
            param.Value = FireTested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ACPower";
        if (ACPower != string.Empty)
            param.Value = ACPower;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        if (StockSeries != string.Empty)
            param.Value = StockSeries;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_UpdateByFileForModule(String StockManufacturer, String StockModel, String ApprovedDate, String ExpiryDate, String FireTested)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_UpdateByFileForModule";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedDate";
        if (ApprovedDate != string.Empty)
            param.Value = ApprovedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpiryDate";
        if (ExpiryDate != string.Empty)
            param.Value = ExpiryDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FireTested";
        if (FireTested != string.Empty)
            param.Value = FireTested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_Update_StockQuantity(string StockItemID, String StockQuantity)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Update_StockQuantity";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        if (StockQuantity != string.Empty)
            param.Value = StockQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_Update_StockCode(string StockItemID, String StockCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Update_StockCode";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        param.Value = StockCode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockItems_Update_ItemDetail(string StockItemID, String StockItem, string StockManufacturer, string StockModel, string StockSeries, string Active, string SalesTag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Update_ItemDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        param.Value = StockItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        param.Value = StockSeries;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        param.Value = SalesTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static string tblStockItems_InsertUpdate(Int32 StockItemID, String StockCategoryID, String StockItem, String StockManufacturer, String StockModel, String StockSeries, String StockSize, String ShortName, String StockDescription, String StockCode, String InverterCert, String StockQuantity, String StockLocation, String MinLevel, String Reserved, String PlanA, String PlanB, String PlanC, String MinPrice, String CostPrice, String CustomerPrice, String Active, String Active1, String SalesTag, String DeleteTag, String LastStockTake, String PrevStockTake, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        param.Value = StockItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockManufacturer";
        param.Value = StockManufacturer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSeries";
        param.Value = StockSeries;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockSize";
        param.Value = StockSize;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ShortName";
        param.Value = ShortName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDescription";
        param.Value = StockDescription;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        param.Value = StockCode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InverterCert";
        param.Value = InverterCert;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockQuantity";
        param.Value = StockQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MinLevel";
        param.Value = MinLevel;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Reserved";
        param.Value = Reserved;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PlanA";
        param.Value = PlanA;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PlanB";
        param.Value = PlanB;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PlanC";
        param.Value = PlanC;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MinPrice";
        param.Value = MinPrice;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CostPrice";
        param.Value = CostPrice;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerPrice";
        param.Value = CustomerPrice;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active1";
        param.Value = Active1;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        param.Value = SalesTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeleteTag";
        param.Value = DeleteTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LastStockTake";
        param.Value = LastStockTake;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PrevStockTake";
        param.Value = PrevStockTake;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        param.Value = upsize_ts;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblStockItems_Delete(string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblStockItemsGetDataBySearch(string StockItem, string StockModel, string StockCategoryID, string CompanyLocationID, string State, string Active, string SalesTag,string ExpiryDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemsGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItem";
        if (StockItem != string.Empty)
            param.Value = StockItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        if (StockModel != string.Empty)
            param.Value = StockModel;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        //param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTag";
        if (SalesTag != string.Empty)
            param.Value = SalesTag;
        else
            param.Value = DBNull.Value;
        //param.Value = SalesTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpiryDate";
        if (ExpiryDate != string.Empty)
            param.Value = ExpiryDate;
        else
            param.Value = DBNull.Value;
        //param.Value = SalesTag;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
          result= DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectByStockItemIDWithFalseSalesTag(string StockItemID,String LocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByStockItemIDWithFalseSalesTag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LocationID";
        param.Value = LocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    //==================================tblStockItemHeadMaster

    public static DataTable tblStockItemHeadMaster_Select_StockQuantity()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemHeadMaster_Select_StockQuantity";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    //====================================tblStockItemInventoryHistory

    public static int tblStockItemInventoryHistory_Insert(String HeadId, String StockItemID, String CompanyLocationID, String CurrentStock, String Stock, String UserId, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HeadId";
        param.Value = HeadId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentStock";
        if (CurrentStock != string.Empty)
            param.Value = CurrentStock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Stock";
        if (Stock != string.Empty)
            param.Value = Stock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblStockSerialNo_Insert(String StockItemID, String CompanyLocationID, String CurrentStock, String Stock, String UserId, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentStock";
        if (CurrentStock != string.Empty)
            param.Value = CurrentStock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Stock";
        if (Stock != string.Empty)
            param.Value = Stock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static DataTable tblStockItemInventoryHistory_SelectByProjectId(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_SelectByProjectId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItemInventoryHistory_SelectStock(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_SelectStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItemInventoryHistory_NewSelectStock(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_NewSelectStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItemInventoryHistory_SelectDeduct(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_SelectDeduct";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockItemInventoryHistory_UpdateMove(string ProjectID, string NewProjectID, string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_UpdateMove";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewProjectID";
        param.Value = NewProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockItemInventoryHistory_UpdateRevert(string id, string RevertFlag, string RevertDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        if(RevertFlag !=string.Empty)
        param.Value = RevertFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertDate";
        if (RevertDate != string.Empty)
            param.Value = RevertDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockItemInventoryHistory_ProjectIDStockItemID(string ProjectID, String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_ProjectIDStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItemInventoryHistory_revert(string ProjectID, String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItemInventoryHistory_revert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    /* ---------------------------------------- Stock Usage ---------------------------------------- */

    public static DataTable tblStockItems_SelectUsage(string StockLocation, string StockCategoryID, string Installer, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectUsage";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockItems_SelectQtyByLocation(string CompanyLocationID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectQtyByLocation";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockItems_SelectUsageDetails(string StockItemID,string StockLocation, string StockCategoryID, string Installer, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectUsageDetails";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockItem_Modal_Exist(string StockModel)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItem_Modal_Exist";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblStockItem_Modal_Exist2(string StockModel,string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItem_Modal_Exist2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockModel";
        param.Value = StockModel;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int wholesaleitemitemNpanels(String wholesaleorderPanelID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "wholesaleitemitemNpanels";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@wholesaleorderPanelID";
        param.Value = wholesaleorderPanelID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblStockSerialNo_Getdata_ByStockItemID(string stockItemID, string stockLocationId)
    {
        throw new NotImplementedException();
    }

    public static SttblStockItems tblStockItems_SelectByStockItemIDForSolarMinor(String StockItemID)
    {
        DbCommand comm = DataAccessSolarMiner.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockItems details = new SttblStockItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockItemID = dr["StockItemID"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockCategory = dr["StockCategory"].ToString();
            details.StockItem = dr["StockItem"].ToString();
            details.StockManufacturer = dr["StockManufacturer"].ToString();
            details.StockModel = dr["StockModel"].ToString();
            details.StockSeries = dr["StockSeries"].ToString();
            details.StockSize = dr["StockSize"].ToString();
            details.ShortName = dr["ShortName"].ToString();
            details.StockDescription = dr["StockDescription"].ToString();
            details.StockCode = dr["StockCode"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.StockQuantity = dr["StockQuantity"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();
            details.MinLevel = dr["MinLevel"].ToString();
            details.Reserved = dr["Reserved"].ToString();
            details.PlanA = dr["PlanA"].ToString();
            details.PlanB = dr["PlanB"].ToString();
            details.PlanC = dr["PlanC"].ToString();
            details.MinPrice = dr["MinPrice"].ToString();
            details.CostPrice = dr["CostPrice"].ToString();
            details.CustomerPrice = dr["CustomerPrice"].ToString();
            details.Active = dr["Active"].ToString();
            details.Active1 = dr["Active1"].ToString();
            details.SalesTag = dr["SalesTag"].ToString();
            details.DeleteTag = dr["DeleteTag"].ToString();
            details.LastStockTake = dr["LastStockTake"].ToString();
            details.PrevStockTake = dr["PrevStockTake"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.IsDashboard = dr["IsDashboard"].ToString();
            details.FixStockItemID = dr["FixStockItemID"].ToString();
            details.FileName = dr["FileName"].ToString();
            details.FileUploadedBy = dr["FileUploadedBy"].ToString();
            details.LastFileUploadedDate = dr["LastFileUploadedDate"].ToString();
            //details.RespSup = dr["RespSup"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblStockItems tblStockItems_SelectByFixStockItemID(String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByFixStockItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockItems details = new SttblStockItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockItemID = dr["StockItemID"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockCategory = dr["StockCategory"].ToString();
            details.StockItem = dr["StockItem"].ToString();
            details.StockManufacturer = dr["StockManufacturer"].ToString();
            details.StockModel = dr["StockModel"].ToString();
            details.StockSeries = dr["StockSeries"].ToString();
            details.StockSize = dr["StockSize"].ToString();
            details.ShortName = dr["ShortName"].ToString();
            details.StockDescription = dr["StockDescription"].ToString();
            details.StockCode = dr["StockCode"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.StockQuantity = dr["StockQuantity"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();
            details.MinLevel = dr["MinLevel"].ToString();
            details.Reserved = dr["Reserved"].ToString();
            details.PlanA = dr["PlanA"].ToString();
            details.PlanB = dr["PlanB"].ToString();
            details.PlanC = dr["PlanC"].ToString();
            details.MinPrice = dr["MinPrice"].ToString();
            details.CostPrice = dr["CostPrice"].ToString();
            details.CustomerPrice = dr["CustomerPrice"].ToString();
            details.Active = dr["Active"].ToString();
            details.Active1 = dr["Active1"].ToString();
            details.SalesTag = dr["SalesTag"].ToString();
            details.DeleteTag = dr["DeleteTag"].ToString();
            details.LastStockTake = dr["LastStockTake"].ToString();
            details.PrevStockTake = dr["PrevStockTake"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.IsDashboard = dr["IsDashboard"].ToString();
            details.FixStockItemID = dr["FixStockItemID"].ToString();
            details.FileName = dr["FileName"].ToString();
            details.FileUploadedBy = dr["FileUploadedBy"].ToString();
            details.LastFileUploadedDate = dr["LastFileUploadedDate"].ToString();
            details.RespSup = dr["RespSup"].ToString();
            details.ExpiryDate = dr["ExpiryDate"].ToString();
        }
        // return structure details
        return details;
    }


    public static DataTable tblStockItems_Select_Category_BySalesTeg(string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_Select_Category_BySalesTeg";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        param.Value = StockCategoryID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}