using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct SttblWebDownload
{
    public string WebDownloadID;
    public string Customer;
    public string ContFirst;
    public string ContLast;
    public string ContEmail;
    public string CustPhone;
    public string ContMobile;
    public string Address;
    public string City;
    public string State;
    public string PostCode;
    public string Source;
    public string System;
    public string Roof;
    public string Angle;
    public string Story;
    public string HouseAge;
    public string Notes;
    public string EntryDate;
    public string PreferredTime;
    public string Uploaded;
    public string Duplicate;
    public string NotDuplicate;
    public string SalesTeamID;
    public string EmployeeID;
    public string LinkID;
    public string FormatFixed;
    public string gclid;
    public string userIp;
    public string PageUrl;
}

public class ClstblWebDownload
{

    public static SttblWebDownload tblWebDownload_SelectbyWebDownloadID(String WebDownloadID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_SelectbyWebDownloadID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblWebDownload details = new SttblWebDownload();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.WebDownloadID = dr["WebDownloadID"].ToString();
            details.Customer = dr["Customer"].ToString();
            details.ContFirst = dr["ContFirst"].ToString();
            details.ContLast = dr["ContLast"].ToString();
            details.ContEmail = dr["ContEmail"].ToString();
            details.CustPhone = dr["CustPhone"].ToString();
            details.ContMobile = dr["ContMobile"].ToString();
            details.Address = dr["Address"].ToString();
            details.City = dr["City"].ToString();
            details.State = dr["State"].ToString();
            details.PostCode = dr["PostCode"].ToString();
            details.Source = dr["Source"].ToString();
            details.System = dr["System"].ToString();
            details.Roof = dr["Roof"].ToString();
            details.Angle = dr["Angle"].ToString();
            details.Story = dr["Story"].ToString();
            details.HouseAge = dr["HouseAge"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.EntryDate = dr["EntryDate"].ToString();
            details.PreferredTime = dr["PreferredTime"].ToString();
            details.Uploaded = dr["Uploaded"].ToString();
            details.Duplicate = dr["Duplicate"].ToString();
            details.NotDuplicate = dr["NotDuplicate"].ToString();
            details.SalesTeamID = dr["SalesTeamID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.LinkID = dr["LinkID"].ToString();
            details.FormatFixed = dr["FormatFixed"].ToString();
            details.gclid = dr["gclid"].ToString();
            details.userIp = dr["UserIp"].ToString();
            details.PageUrl = dr["PageUrl"].ToString();
        }
        // return structure details
        return details;
    }
    public static SttblWebDownload tblWebDownload_SelectbyCustomerLinkID(String CustomerLinkID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_SelectbyCustomerLinkID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerLinkID";
        param.Value = CustomerLinkID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblWebDownload details = new SttblWebDownload();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.WebDownloadID = dr["WebDownloadID"].ToString();
            details.Customer = dr["Customer"].ToString();
            details.ContFirst = dr["ContFirst"].ToString();
            details.ContLast = dr["ContLast"].ToString();
            details.ContEmail = dr["ContEmail"].ToString();
            details.CustPhone = dr["CustPhone"].ToString();
            details.ContMobile = dr["ContMobile"].ToString();
            details.Address = dr["Address"].ToString();
            details.City = dr["City"].ToString();
            details.State = dr["State"].ToString();
            details.PostCode = dr["PostCode"].ToString();
            details.Source = dr["Source"].ToString();
            details.System = dr["System"].ToString();
            details.Roof = dr["Roof"].ToString();
            details.Angle = dr["Angle"].ToString();
            details.Story = dr["Story"].ToString();
            details.HouseAge = dr["HouseAge"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.EntryDate = dr["EntryDate"].ToString();
            details.PreferredTime = dr["PreferredTime"].ToString();
            details.Uploaded = dr["Uploaded"].ToString();
            details.Duplicate = dr["Duplicate"].ToString();
            details.NotDuplicate = dr["NotDuplicate"].ToString();
            details.SalesTeamID = dr["SalesTeamID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.LinkID = dr["LinkID"].ToString();
            details.FormatFixed = dr["FormatFixed"].ToString();
        }
        // return structure details
        return details;
    }

    public static DataTable tblWebDownload_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblWebDownload_Insert(string Customer, string ContFirst, string ContLast, string ContEmail, string CustPhone, string ContMobile, string Address, string City, string State, string PostCode, string Source, string System, string Roof, string Angle, string Story, string HouseAge, string Notes, string SubSource, string empid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Customer";
        if (Customer != string.Empty)
            param.Value = Customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustPhone";
        if (CustPhone != string.Empty)
            param.Value = CustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Address";
        if (Address != string.Empty)
            param.Value = Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@City";
        if (City != string.Empty)
            param.Value = City;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PostCode";
        if (PostCode != string.Empty)
            param.Value = PostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@System";
        if (System != string.Empty)
            param.Value = System;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Roof";
        if (Roof != string.Empty)
            param.Value = Roof;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Angle";
        if (Angle != string.Empty)
            param.Value = Angle;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Story";
        if (Story != string.Empty)
            param.Value = Story;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@HouseAge";
        if (HouseAge != string.Empty)
            param.Value = HouseAge;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SubSource";
        if (SubSource != string.Empty)
            param.Value = SubSource;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@empid";
        if (empid != string.Empty)
            param.Value = empid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblWebDownload_Update_Duplicate(string WebDownloadID, String Duplicate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Update_Duplicate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Duplicate";
        param.Value = Duplicate;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblWebDownload_Update_NotDuplicate(string WebDownloadID, String NotDuplicate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Update_NotDuplicate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NotDuplicate";
        param.Value = NotDuplicate;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblWebDownload_Update_Uploaded(string WebDownloadID, String Uploaded)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Update_Uploaded";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Uploaded";
        param.Value = Uploaded;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblWebDownload_GetDataBySearch(string alpha, string State, string SalesTeamID, string startdate, string enddate, string Source, string dup, string webdupe)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_GetDataBySearch";

        //HttpContext.Current.Response.Write("alpha" + alpha + "State" + State + "SalesTeamID" + SalesTeamID + "startdate" + startdate + "enddate" + enddate + "Source" + Source + "dup" + dup);
        //HttpContext.Current.Response.End();
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
        {
            param.Value = alpha;
        }
        else
        {
            param.Value = DBNull.Value;
        }
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Source";
        if (Source != string.Empty)
            param.Value = Source;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dup";
        if (dup != string.Empty)
            param.Value = dup;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@webduplication";
        if (webdupe != string.Empty)
            param.Value = webdupe;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblCustSourceSub_SelectBySourceSub(string CustSourceSub)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectBySourceSub";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustSource_SelectBySource(string CustSource)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSource_SelectBySource";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSource";
        param.Value = CustSource;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblWebDownload_Delete(string WebDownloadID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblWebDownload_SelectSource()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_SelectSource";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblWebDownload_Update_ReadFlag(string WebDownloadID, String ReadFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Update_ReadFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReadFlag";
        if (ReadFlag != string.Empty)
            param.Value = ReadFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblWebDownload__noti_Update_ReadFlag(string EmployeeID, String ReadFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload__noti_Update_ReadFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReadFlag";
        if (ReadFlag != string.Empty)
            param.Value = ReadFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblWebDownload_Update_CustomerLinkID(string WebDownloadID, String CustomerLinkID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Update_CustomerLinkID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerLinkID";
        if (CustomerLinkID != string.Empty)
            param.Value = CustomerLinkID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable aspnetusers_SalesManager_SalesRep(string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnetusers_SalesManager_SalesRep";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblWebDownload_GetLeadCount(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_GetLeadCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblWebDownload_LeadService()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_LeadService";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable GettblWebDownload()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GettblWebDownload";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblWebDownload_ClearSpace_from_Mobile_Phone(string WebDownloadID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_ClearSpace_from_Mobile_Phone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);

    }
    public static bool tblWebDownload_IsDeleteflag(string WebDownloadID, string flag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_IsDeleteflag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param=comm.CreateParameter();
        param.ParameterName = "@flag";
        param.Value = flag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblWebDownload_Delete_forAdmin(string WebDownloadID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWebDownload_Delete_forAdmin";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WebDownloadID";
        param.Value = WebDownloadID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}



    
    
    