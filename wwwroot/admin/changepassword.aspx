﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="changepassword.aspx.cs" Inherits="admin_changepassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--Favicon starts here -->
    <!--Favicon ends here -->
    <!--CSS files starts here -->
    <link rel="stylesheet" type="text/css" href="css/global.css" />
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="assets/style/ie/ie7.css" /><![endif]-->
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="assets/style/ie/ie6.css" /><![endif]-->
    <!--CSS files ends here -->
    <!--<script type="text/javascript" src="http://getfirebug.com/releases/lite/1.2/firebug-lite-compressed.js"></script>-->
    
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="main">

                <div id="globalTabContent">
                    <h2 style="font-size: 18px; margin-bottom: 15px; margin-left: 17px;">Change Password</h2>
                    <div id="tab-1" class="tabs-container">
                      

                        <div class="globalContainerCA">
                            <ul class="addNewPage" style="width: 100%;">
                                <asp:Panel ID="Panel1" runat="server" CssClass="PanAddUpdate">
                                   <asp:ChangePassword ID="ChangePassword1" runat="server" CancelDestinationPageUrl="~/admin/adminfiles/dashboard.aspx"
                                        ContinueDestinationPageUrl="" Width="100%" SuccessText="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='tick'><tr><td style='padding-left:30px;'>Password has been changed successfully.</td></tr></table>"
                                        SuccessTitleText="&nbsp;" CssClass="BlackTxt" ConfirmPasswordCompareErrorMessage="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='tick'><tr><td style='padding-left:30px;'>&nbsp;The Confirm New Password must match the New Password entry.</td></tr></table>"
                                        ChangePasswordFailureText="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='cross'><tr><td style='padding-left:30px;'>&nbsp;Password incorrect or New Password invalid.</td></tr></table>"
                                        ConfirmPasswordRequiredErrorMessage="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='cross'><tr><td style='padding-left:30px;'>&nbsp;Confirm New Password is required.</td></tr></table>"
                                        NewPasswordRegularExpressionErrorMessage="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='cross'><tr><td style='padding-left:30px;'>&nbsp;Please enter a different password.</td></tr></table>"
                                        NewPasswordRequiredErrorMessage="<table cellpadding='0' cellspacing='0' border='0' width='100%' class='cross'><tr><td style='padding-left:30px;'>&nbsp;New Password is required.</td></tr></table>"
                                        ContinueButtonType="Image" ContinueButtonImageUrl="~/admin/images/btn_countinue.gif" OnContinueButtonClick="ChangePassword1_ContinueButtonClick">
                                        <ChangePasswordTemplate>

                                            <p>
                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                            </p>
                                            <li>
                                                <label style="float: left; width:150px;"><strong>Current Password</strong></label>
                                                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                                <asp:RegularExpressionValidator Width="100%" Style="display: inline-block; padding-bottom: 0px; text-align: center;" ID="RegularExpressionValidator3"
                                                    runat="server" ControlToValidate="CurrentPassword" Display="Dynamic" ErrorMessage="Minimum password length is 6"
                                                     ValidationExpression=".{6}.*" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="CurrentPassword"
                                                     Display="Dynamic" SetFocusOnError="True" ErrorMessage="* Required"  ValidationGroup="ConfirmNewPassword" ></asp:RequiredFieldValidator>
                                            </li>
                                            <li>
                                                <label style="float: left;  width:150px;"><strong>New Password</strong></label>
                                                <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                                <asp:RegularExpressionValidator Width="100%" Style="display: inline-block; text-align: center;" 
                                                    ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ControlToValidate="NewPassword"
                                                     ErrorMessage="Minimum password length is 6"
                                                    ValidationExpression=".{6}.*" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NewPassword"
                                                     Display="Dynamic" SetFocusOnError="True" ErrorMessage="* Required"  ValidationGroup="ConfirmNewPassword" ></asp:RequiredFieldValidator>
                                            </li>
                                            <li>
                                                <label style="float: left;  width:150px;"><strong>Confirm New Password</strong></label>
                                                <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                              <%--<asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                    ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                                    ValidationGroup="ChangePassword1"></asp:CompareValidator>--%>
                                                   <asp:CompareValidator Width="100%" Style="display: inline-block; text-align: center;"
                                                     ID="CompareValidator1" runat="server" Display="Dynamic" ControlToCompare="NewPassword"
                                                    ControlToValidate="ConfirmNewPassword" ErrorMessage="Password MisMatch"
                                                    SetFocusOnError="True" ValidationGroup="ConfirmNewPassword"></asp:CompareValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ConfirmNewPassword"
                                                     Display="Dynamic" SetFocusOnError="True" ErrorMessage="* Required" 
                                                     ValidationGroup="ConfirmNewPassword" ></asp:RequiredFieldValidator>
                                            </li>
                                            <ul>
                                                <label style="float: left;  width:150px;">&nbsp;</label>
                                                <li class="floatLeft">
                                                    <p>
                                                         <asp:ImageButton ID="ChangePasswordPushButton" runat="server" ImageUrl="~/admin/images/save.gif"
                                                            CommandName="ChangePassword" ValidationGroup="ConfirmNewPassword" 
                                                             CausesValidation="true" OnClientClick="btnClick()" />
                                                    </p>
                                                </li>
                                            </ul>

                                        </ChangePasswordTemplate>
                                    </asp:ChangePassword>
                                </asp:Panel>
                            </ul>
                         
                            <div class="clear">
                            </div>
                        </div>

                    </div>
                     </div>
                   
               
            </div>
        </div>
    </form>
</body>
</html>
