using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_templates_loginmaster : System.Web.UI.MasterPage
{
    protected static string Siteurl;
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
       StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        //Siteurl = SiteConfiguration.GetURL();
        SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
    }
}
