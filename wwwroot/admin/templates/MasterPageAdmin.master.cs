using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class BossAdmin_MasterPageAdmin : System.Web.UI.MasterPage
{
    protected static string Siteurl;
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
		if (!Request.IsAuthenticated)
        {
            Response.Redirect("~/Default.aspx");
        }
        //   StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery);
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        if (!IsPostBack)
        {
            //StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            //Siteurl = st.siteurl;
            SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
            System.IO.FileInfo image = new System.IO.FileInfo(Request.PhysicalApplicationPath + "/userfiles/sitelogo/" + st.sitelogoupload);
            bool isfileUploaded = image.Exists;
        }

        bool Authenticated = PageAuthentication();
        if (!Authenticated)
        {
            Response.Redirect("~/Default.aspx");
        }

        switch (pagename.ToLower())
        {
            //case "dashboard":
            //    dash.Visible = true;
            //    Dashboard.Attributes.Add("class", "activePriNav");
            //    break;

            //case "adminutilities":
            //    divutility.Visible = true;
            //    hyputilities.CssClass = "menuOn";
            //    break;

            //case "adminsiteconfiguration":
            //     divutility.Visible = true;
            //     hypsiteconfig.CssClass = "menuOn";
            //    break;

            //case "announcement":
            //    divannouncement.Visible = true;
            //    hypannounctment.CssClass = "menuOn";
            //    liannouncement.Attributes.Add("class", "activePriNav");
            //    break;

            //case "news":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "newsdetail":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "demo":
            //    divdemo.Visible = true;
            //    liDemo.Attributes.Add("class", "activePriNav");
            //    hypdemo.CssClass = "menuOn";
            //    break;
        }


        if (pagename != "profile")
        {
            // secpage.Attributes.Add("class", "scrollable padder shdoimg");
        }
        else
        {
            //   secpage.Attributes.Add("class", "scrollable shdoimg");
        }


        //////title

        if (pagename == "profile")
        {
            pagetitle.Text = "AriseSolar | Profile";
        }
        if (pagename == "dashboard")
        {
            pagetitle.Text = "AriseSolar | Dashboard";
        }
        if (pagename == "setting")
        {
            pagetitle.Text = "AriseSolar | Setting";
        }
        if (pagename == "profile")
        {
            pagetitle.Text = "AriseSolar | Profile";
        }
        if (pagename == "help")
        {
            pagetitle.Text = "AriseSolar | Help";
        }
        if (pagename == "project")
        {
            pagetitle.Text = "AriseSolar | Project";
        }
        if (pagename == "installations")
        {
            pagetitle.Text = "AriseSolar | Installation";
        }
        if (pagename == "invoicesissued")
        {
            pagetitle.Text = "AriseSolar | Invoices Issued";
        }
        if (pagename == "invoicespaid")
        {
            pagetitle.Text = "AriseSolar | Invoices Paid";
        }
        if (pagename == "lead")
        {
            pagetitle.Text = "AriseSolar | Lead";
        }
        if (pagename == "lteamcalendar")
        {
            pagetitle.Text = "AriseSolar | Team Calender";
        }
        if (pagename == "installercalendar")
        {
            pagetitle.Text = "AriseSolar | Installer Calender";
        }


        #region master
        if (pagename == "companylocations")
        {
            pagetitle.Text = "AriseSolar | Company Locations";
        }
        if (pagename == "custsource")
        {
            pagetitle.Text = "AriseSolar | Company Source";
        }
        if (pagename == "custsourcesub")
        {
            pagetitle.Text = "AriseSolar | Company Source Sub";
        }
        if (pagename == "CustType")
        {
            pagetitle.Text = "AriseSolar | Company Type";
        }
        if (pagename == "elecdistributor")
        {
            pagetitle.Text = "AriseSolar | Elec Distributor";
        }
        if (pagename == "elecretailer")
        {
            pagetitle.Text = "AriseSolar | Elec Retailer";
        }
        if (pagename == "employee")
        {
            pagetitle.Text = "AriseSolar | Employee";
        }
        if (pagename == "FinanceWith")
        {
            pagetitle.Text = "AriseSolar | Finance With";
        }
        if (pagename == "housetype")
        {
            pagetitle.Text = "AriseSolar | House Type";
        }
        if (pagename == "leadcancelreason")
        {
            pagetitle.Text = "AriseSolar | Lead Cancel Reason";
        }
        if (pagename == "mtcereason")
        {
            pagetitle.Text = "AriseSolar | Mtce Reasons";
        }
        if (pagename == "mtcereasonsub")
        {
            pagetitle.Text = "AriseSolar | Mtce Reasons Sub";
        }
        if (pagename == "projectcancel")
        {
            pagetitle.Text = "AriseSolar | Project Cancel";
        }
        if (pagename == "projectonhold")
        {
            pagetitle.Text = "AriseSolar | Project On-Hold";
        }
        if (pagename == "projectstatus")
        {
            pagetitle.Text = "AriseSolar | Project Status";
        }
        if (pagename == "projecttrackers")
        {
            pagetitle.Text = "AriseSolar | Project Trackers";
        }
        if (pagename == "projecttypes")
        {
            pagetitle.Text = "AriseSolar | Project Types";
        }
        if (pagename == "projectvariations")
        {
            pagetitle.Text = "AriseSolar | Project Variations";
        }
        if (pagename == "promotiontype")
        {
            pagetitle.Text = "AriseSolar | Promotion Type";
        }
        if (pagename == "prospectcategory")
        {
            pagetitle.Text = "AriseSolar | Prospect Categories";
        }
        if (pagename == "roofangles")
        {
            pagetitle.Text = "AriseSolar | Roof Angles";
        }
        if (pagename == "roottye")
        {
            pagetitle.Text = "AriseSolar | Root Type";
        }
        if (pagename == "salesteams")
        {
            pagetitle.Text = "AriseSolar | Sales Team";
        }
        if (pagename == "StockCategory")
        {
            pagetitle.Text = "AriseSolar | Stock Category";
        }
        if (pagename == "transactiontypes")
        {
            pagetitle.Text = "AriseSolar | Transaction Types";
        }
        if (pagename == "paymenttype")
        {
            pagetitle.Text = "AriseSolar | Finance Payment Type";
        }
        if (pagename == "invcommontype")
        {
            pagetitle.Text = "AriseSolar | Invoice Type";
        }
        if (pagename == "postcodes")
        {
            pagetitle.Text = "AriseSolar | Post Codes";
        }
        if (pagename == "custinstusers")
        {
            pagetitle.Text = "AriseSolar | Cust/Inst Users";
        }
        if (pagename == "newsletter")
        {
            pagetitle.Text = "AriseSolar | News Letter";
        }
        if (pagename == "logintracker")
        {
            pagetitle.Text = "AriseSolar | Login Tracker";
        }
        if (pagename == "leftempprojects")
        {
            pagetitle.Text = "AriseSolar | Left Employee Projects";
        }
        if (pagename == "lteamtime")
        {
            pagetitle.Text = "AriseSolar | Manage L-Team App Time";
        }
        if (pagename == "refundoptions")
        {
            pagetitle.Text = "AriseSolar | Refund Options";
        }

        #endregion

        if (pagename == "company")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "AriseSolar | Company";
        }
        if (pagename == "contacts")
        {
            pagetitle.Text = "AriseSolar | Contact";
        }

        if (pagename == "leadtracker")
        {
            pagetitle.Text = "AriseSolar | Lead Tracker";
        }
        if (pagename == "gridconnectiontracker")
        {
            pagetitle.Text = "AriseSolar | GridConnection Tracker.";
        }

        if (pagename == "lteamtracker")
        {
            pagetitle.Text = "AriseSolar | Team Lead Tracker";
        }
        if (pagename == "stctracker")
        {
            pagetitle.Text = "AriseSolar | STC Tracker";
        }
        if (pagename == "instinvoice")
        {
            pagetitle.Text = "AriseSolar | Inst Invoice Tracker";
        }
        if (pagename == "salesinvoice")
        {
            pagetitle.Text = "AriseSolar | Sales Invoice Tracker";
        }
        if (pagename == "installbookingtracker")
        {
            pagetitle.Text = "AriseSolar | Install Booking Tracker";
        }
        if (pagename == "ApplicationTracker")
        {
            pagetitle.Text = "AriseSolar | Application Tracker";
        }
        if (pagename == "accrefund")
        {
            pagetitle.Text = "AriseSolar | Refund";
        }
        if (pagename == "formbay")
        {
            pagetitle.Text = "AriseSolar | FormBay";
        }
        if (pagename == "stockitem")
        {
            pagetitle.Text = "AriseSolar | Stock Item";
        }
        if (pagename == "stocktransfer")
        {
            pagetitle.Text = "AriseSolar | Stock Transfer";
        }
        if (pagename == "stockorder")
        {
            pagetitle.Text = "AriseSolar | Stock Order";
        }
        if (pagename == "stockdatasheet")
        {
            pagetitle.Text = "AriseSolar | Data Sheet";
        }

        if (pagename == "Wholesale")
        {
            pagetitle.Text = "AriseSolar | Wholesale";
        }
        if (pagename == "wholesalededuct")
        {
            pagetitle.Text = "AriseSolar | Wholesale Deduct";
        }
        if (pagename == "stockdeduct")
        {
            pagetitle.Text = "AriseSolar | Stock Deduct";
        }
        if (pagename == "stockin")
        {
            pagetitle.Text = "AriseSolar | Stock In";
        }
        if (pagename == "WholesaleIn")
        {
            pagetitle.Text = "AriseSolar | Wholesale In";
        }
        if (pagename == "stockusage")
        {
            pagetitle.Text = "AriseSolar | Stock Usage";
        }
        if (pagename == "stockorderreport")
        {
            pagetitle.Text = "AriseSolar | Stock Order Report";
        }

        if (pagename == "promosend")
        {
            pagetitle.Text = "AriseSolar | Promo Send";
        }

        if (pagename == "lead")
        {
            pagetitle.Text = "AriseSolar | Lead";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "AriseSolar | Maintenance";
        }
        if (pagename == "casualmtce")
        {
            pagetitle.Text = "AriseSolar| Casual Maintenance";
        }
        if (pagename == "noinstalldate")
        {
            pagetitle.Text = "AriseSolar | NoInstall Date Report";
        }
        if (pagename == "installdate")
        {
            pagetitle.Text = "AriseSolar | Install Date Report";
        }
        if (pagename == "panelscount")
        {
            pagetitle.Text = "AriseSolar | Panel Graph";
        }
        if (pagename == "accountreceive")
        {
            pagetitle.Text = "AriseSolar | Account Receive Report";
        }
        if (pagename == "paymentstatus")
        {
            pagetitle.Text = "AriseSolar | Payment Status Report";
        }
        if (pagename == "formbaydocsreport")
        {
            pagetitle.Text = "AriseSolar | Formbay Docs Report";
        }
        if (pagename == "weeklyreport")
        {
            pagetitle.Text = "AriseSolar | Weekly Report";
        }
        if (pagename == "leadassignreport")
        {
            pagetitle.Text = "AriseSolar | Lead Assign Report";
        }
        if (pagename == "stockreport")
        {
            pagetitle.Text = "AriseSolar | Stock Report";
        }
        if (pagename == "custproject")
        {
            pagetitle.Text = "AriseSolar | Customer System Detail";
        }
        if (pagename == "customerdetail")
        {
            pagetitle.Text = "AriseSolar | Customer System Detail";
        }
        if (pagename == "instavailable")
        {
            pagetitle.Text = "AriseSolar | Installer Available";
        }
        if (pagename == "printinstallation")
        {
            pagetitle.Text = "AriseSolar | Installations";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "AriseSolar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "AriseSolar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "AriseSolar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "AriseSolar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "AriseSolar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "AriseSolar | Maintenance";
        }
        if (pagename == "stocktransferreportdetail")
        {
            pagetitle.Text = "AriseSolar | Stock Transfer Report";
        }
        if (pagename == "promotracker")
        {
            pagetitle.Text = "AriseSolar | Promo Tracker";
        }
        if (pagename == "unittype")
        {
            pagetitle.Text = "AriseSolar | Unit Type";
        }
        if (pagename == "streettype")
        {
            pagetitle.Text = "AriseSolar | Street Type";
        }
        if (pagename == "streetname")
        {
            pagetitle.Text = "AriseSolar | Street Name";
        }
        if (pagename == "promooffer")
        {
            pagetitle.Text = "AriseSolar | Promo Offer";
        }
        if (pagename == "updateformbayId")
        {
            pagetitle.Text = "AriseSolar | updateformbayId";
        }
        if (pagename == "pickup")
        {
            pagetitle.Text = "AriseSolar | Pick Up";
        }
        if (pagename == "paywaytracker")
        {
            pagetitle.Text = "AriseSolar | PayWay";
        }
        if (pagename == "quickform")
        {
            pagetitle.Text = "AriseSolar | Quick Form";
        }
        if (pagename == "clickcustomer")
        {
            pagetitle.Text = "AriseSolar | Click Customer";
        }
        if (pagename == "ViewLeads")
        {
            pagetitle.Text = "AriseSolar | ViewLeads";

        }

        if (pagename == "Customer")
        {
            pagetitle.Text = "AriseSolar | New Customer";
        }
        if (pagename == "NewContact")
        {
            pagetitle.Text = "AriseSolar | New Contact";
        }
        if (pagename == "LeadTrackerNew")
        {
            pagetitle.Text = "AriseSolar | New Lead Tracker";
        }

        if (pagename == "ProjectNew")
        {
            pagetitle.Text = "AriseSolar | New Projects";
        }

        if (pagename == "SalesInfo")
        {
            pagetitle.Text = "AriseSolar | Project Details";
        }

        if (pagename == "CustomerNew")
        {
            pagetitle.Text = "AriseSolar | Customer Details";
        }

        if (pagename == "LeadReport")
        {
            pagetitle.Text = "AriseSolar | Lead Report";
        }
    }

    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        Session["userid"] = "";
        Session.Abandon();
        Response.Redirect("~/admin/Default.aspx");
    }

    public bool PageAuthentication()
    {
        string[] rolename = Roles.GetRolesForUser(); // Get Loggedin User Roles

        SiteMapNode siteMap = SiteMap.CurrentNode; // Currunt Page Nodes

        bool AuthFlag = false;

        if (siteMap == null)
        {
            AuthFlag = true;
        }
        else if (Request.QueryString.ToString().Length != 0)
        {
            AuthFlag = true;
        }
        else
        {
            for (int i = 0; i < rolename.Length; i++)
            {
                string RoleName = rolename[i];

                if (siteMap.Roles.Contains(RoleName))
                {
                    AuthFlag = true;
                }

                
            }
        }

        return AuthFlag;
    }
}


