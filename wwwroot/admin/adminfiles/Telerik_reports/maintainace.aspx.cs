﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EurosolarReporting;
using Telerik.Reporting.Services.WebApi;
using Telerik.ReportViewer.WebForms;
using Telerik.Reporting;

public partial class mailtemplate_telerik_maintainace : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        string ProjectID = "217746";
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        //dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notesMaintenance(ProjectID, ProjectMaintenanceID);

        SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintainanceByProjectID(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Nmaintainance();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblmaintainaceno", true)[0] as Telerik.Reporting.TextBox;
        lblinvoiceno.Value = "Maintenance Contract - " + stPro.InvoiceNumber;

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        

        Telerik.Reporting.TextBox txtInstallBookingDate = rpt.Items.Find("txtInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceCost = rpt.Items.Find("txtMtceCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceDiscount = rpt.Items.Find("txtMtceDiscount", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceBalance = rpt.Items.Find("txtMtceBalance", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox textBox24 = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox30 = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox29 = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox16 = rpt.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox18 = rpt.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox34 = rpt.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;

        table3.DataSource = dt;
        table4.DataSource = dt;
        table8.DataSource = dt;
        table6.DataSource = dt;
        table1.DataSource = dt;
        txtInstallBookingDate.Value = Convert.ToDateTime(stPro.InstallBookingDate).ToString("dd MMM yyyy");
        try
        {
            textBox24.Value = st.ProjectID;
        }
        catch { }
        try
        {
            textBox16.Value = st.ProjectID;
        }
        catch { }
        try
        {
            textBox18.Value = st.ProjectID;
        }
        catch { }
        try
        {
            textBox34.Value = st.ProjectID;
        }
        catch { }
        try
        {
            textBox30.Value = st.OpenDate;
        }
        catch { }
        try
        {
            textBox29.Value = st.CurrentPhoneContact;
        }
        catch { }
        try
        {
            txtMtceCost.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceCost);
        }
        catch { }
        try
        {
            txtMtceDiscount.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceDiscount);
        }
        catch { }
        try
        {
            txtMtceBalance.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceBalance);
        }
        catch { }

        ir.ReportDocument = rpt;
        ReportViewer1.ReportSource = ir;
        ReportViewer1.RefreshReport();

    }
}