﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using EurosolarReporting;
using Telerik.Reporting.Services.WebApi;
using Telerik.ReportViewer.WebForms;
using System.IO;
using System.Configuration;

public partial class Telerik_reports_NewPaymentReceipt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //int intUserId = int.Parse(Session["UserId"].ToString());
        int intUserId = int.Parse("1");
        //int intCompanyId = int.Parse(Request.QueryString["intCompanyId"].ToString());
        int intCompanyId = int.Parse("76");

        Telerik.Reporting.Report rpt = new EurosolarReporting.new_paymentreceipt();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();


        ir.ReportDocument = rpt;
        ReportViewer1.ReportSource = ir;
        ReportViewer1.RefreshReport();
    }
}