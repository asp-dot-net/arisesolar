﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Theme="admin" CodeFile="installations.aspx.cs" Inherits="admin_adminfiles_view_installations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

       <script>
           var focusedElementId = "";
           var prm = Sys.WebForms.PageRequestManager.getInstance();
           function BeginRequestHandler(sender, args) {
               $('.loading-container').css('display', 'block');
           }
           function EndRequestHandler(sender, args) {
               //hide the modal popup - the update progress
               //$('.splash').css('display', 'none');
           }
           prm.add_pageLoaded(pageLoaded);
           prm.add_beginRequest(BeginRequestHandler);
           prm.add_endRequest(EndRequestHandler);
           function pageLoaded() {


               $('.loading-container').css('display', 'none');
               $('body').removeClass('modal-open');
               $('.modal-backdrop').remove();
               $("[data-toggle=tooltip]").tooltip();

           }

        </script>

        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            function pageLoaded() {
                $(".myvalinstallation").select2({
                    //placeholder: "select",
                    allowclear: true
                });

                //$('.i-checks').iCheck({
                //    checkboxClass: 'icheckbox_square-green',
                //    radioClass: 'iradio_square-green'
                //});
            }
        </script>
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Installations</h5>
              <div id="hbreadcrumb" class="pull-right"></div>
               </div>
    <div class="page-body padtopzero">    
    <section class="row m-b-md">
        <div class="content animate-panel printorder">            
                        <div class="col-lg-12">
                            <div class="hpanel marbtmzero">
                             
                              
                                     
                                         <div class="searchfinal">  
                    						<div class="widget-body shadownone brdrgray">                                         		
                                            <div class="form-horizontal">
                                                <div class="row printorder">
                                                    <div class="col-md-3">
                                                        <div class="btnmarzero">
                                                        <span class="arrowbutton">
                                                           <asp:LinkButton ID="btnpreviousweek" runat="server" Text="Previous Week" OnClick="btnpreviousweek_Click" CssClass="fc-prev-button fc-button fc-state-default fc-corner-left btnarrowbox"><i class="fa fa-angle-left"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnnextweek" runat="server" Text="Next Week" OnClick="btnnextweek_Click" CssClass="fc-next-button fc-button fc-state-default fc-corner-right btnarrowbox"><i class="fa fa-angle-right"></i></asp:LinkButton></span>
                                                                    <asp:Button ID="btntoday" CssClass="btn btn-dark" runat="server" Text="Today" OnClick="btntoday_Click" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div style="text-align: center;">
                                                            <div class="form-group">
                                                                <h5>
                                                                    <b><asp:Literal ID="ltdate" runat="server"></asp:Literal></b></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        
                                                    </div>
                                                </div>
                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                                    <div class="dataTables_filter col-sm-12 Responsive-search form-inline searchareadiv printorder">
                                                        <div class="row">
                                                            <div class="">
                                                                <%-- <div class="dataTables_filter Responsive-search">--%>
                                                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Company Locations" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                                                    WatermarkText="Project" />
                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                                    UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                            </div>
                                                                            <div class="input-group col-sm-1">
                                                                                <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalinstallation">
                                                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="input-group col-sm-1">
                                                                                <asp:DropDownList ID="ddlType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalinstallation">
                                                                                    <asp:ListItem Value="">Project Type</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Project</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Maintenance</asp:ListItem>
                                                                                    <asp:ListItem Value="3">Casual Mtce</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="input-group col-sm-2">
                                                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalinstallation">
                                                                                    <asp:ListItem Value="">Installer</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="input-group col-sm-1">
                                                                                <asp:DropDownList ID="ddlisformbay" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalinstallation">
                                                                                    <asp:ListItem Value="0">Is QuickForm</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="input-group">
                                                                                <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                            </div>

                                                                            <div class="input-group printorder pull-right">
                                                                                <div class="">

                                                                                  <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i> Print</a>

                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                                <%--</div>--%>
                                                            </div>




                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                            <div class="clear"></div>
                                        	</div>
                                        </div>                                        
                                    
                            </div>
                        </div>                  
        </div>
    </section>
   
                                   
    
        <div class="panel panel-default teamcalendar instalattiontable" style="overflow:auto; min-width:500px" >
            <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#BDC3C8" class="table">
                <tbody>
                    <tr>
                        <asp:Repeater ID="RptDays" runat="server" OnItemDataBound="RptDays_ItemDataBound">
                            <ItemTemplate>
                                <td valign="top">
                                    <table width="100%" cellpadding="0" cellspacing="0" class="table table-striped table-bordered teamcaltable">
                                        <thead>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:HiddenField ID="hdnDate" runat="server" Value='<%# Eval("CDate")%>' />
                                                    <%# Eval("CDate")%>
                                                    <%# Eval("CDay") %>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="title">
                                                <td width="50%">Job(s)
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:Label runat="server" ID="lblcount"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="title">
                                                <td width="50%">Project
                                                </td>
                                                <td width="20%" align="center">Time&nbsp;Bkd
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="rptinstaller1" runat="server" OnItemDataBound="rptinstaller1_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr class="brdergray">
                                                        <td valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:HiddenField ID="hdnBookingDate" runat="server" Value='<%# Eval("BookingDate")%>' />
                                                                        <asp:HiddenField ID="hdnProjectID" runat="server" Value='<%# Eval("ProjectID")%>' />
                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" title="Detail"
                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>' Target="_blank"><%#Eval("Project")%></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%# Eval("InstallState")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%# Eval("ProjectNumber")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <strong><%# Eval("contact")%></strong>
                                                                        <br />
                                                                        <div runat="server" id="divisformbay" visible="false">
                                                                            <asp:CheckBox ID="chkformbay" runat="server" />
                                                                            <label for='<%# Container.FindControl("chkformbay").ClientID %>' onclick="return false;">
                                                                                <span></span>
                                                                            </label>
                                                                            &nbsp;IsFormbay
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <asp:Repeater ID="rptinstaller2" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <%# Eval("BookingAmPm")%>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <%-- <tr>
                                                                                                <td colspan="2" class="nopad">
                                                                                                    <div class="brdbtm">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>--%>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                            <asp:Repeater ID="rptMtce" runat="server" OnItemDataBound="rptMtce_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr class="brdergray">
                                                        <td valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <%--<asp:Image ID="imgMtce" ImageUrl="~/images/icon_maintenance.png" runat="server" Height="10px" Width="10px" />--%>

                                                                        <asp:HiddenField ID="hdnProjectIDM" runat="server" Value='<%# Eval("ProjectID")%>' />
                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" title="Detail"
                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>' Target="_blank"><%#Eval("Project")%></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%# Eval("InstallState")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%# Eval("ProjectID")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <strong><%# Eval("contact")%></strong>
                                                                        <br />
                                                                        <div runat="server" id="divisformbay" visible="false">
                                                                            <asp:CheckBox ID="chkformbay" runat="server" />
                                                                            <label for='<%# Container.FindControl("chkformbay").ClientID %>' onclick="return false;">
                                                                                <span></span>
                                                                            </label>
                                                                            &nbsp;IsFormbay
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                                <tr>
                                                                    <td align="center">&nbsp;
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="nopad">
                                                            <div class="brdbtm">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>


                                            <asp:Repeater ID="rptcauMtce" runat="server" OnItemDataBound="rptcauMtce_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr class="brdergray">
                                                        <td valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <strong>CM: </strong>
                                                                        <asp:HiddenField ID="hdnProjectIDC" runat="server" Value='<%# Eval("ProjectID")%>' />

                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-original-title="Detail View" class="tooltips"
                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("Project")%></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%# Eval("InstallState")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%# Eval("ProjectNumber")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <%# Eval("contact")%>
                                                                        <br />
                                                                        <div runat="server" id="divisformbay" visible="false">
                                                                            <asp:CheckBox ID="chkformbay" runat="server" />
                                                                            <label for='<%# Container.FindControl("chkformbay").ClientID %>' onclick="return false;">
                                                                                <span></span>
                                                                            </label>
                                                                            &nbsp;IsFormbay
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                                <tr>
                                                                    <td align="center">&nbsp;
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="nopad">
                                                            <div class="brdbtm">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </tbody>
                                    </table>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </tbody>
            </table>
        </div>
   
    <div class="contactbottomarea" runat="server" id="divprintinstall" style="display: none;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="page-wrap">
                                <h1>Install Booking qld</h1>
                                <div class="dates">
                                    <div class="datesleft">
                                        From:
                                        <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                                    </div>
                                    <div class="datesright">
                                        To:
                                        <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="installer">
                                    <table width="100%" class="tableth" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="20%"></td>
                                            <td width="60%" align="center">Installer :
                                                <asp:Label ID="lblInstaller" runat="server"></asp:Label></td>
                                            <td width="10%" align="center" class="titleright">Reguler<br />
                                                Allocation. </td>
                                            <td width="10%" align="center" class="titleright">Extra's</td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:Repeater ID="RptDays1" runat="server" OnItemDataBound="RptDays1_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="perdate" id="testdiv" runat="server">
                                            <asp:HiddenField ID="hdnDate" runat="server" Value='<%# Eval("CDate")%>' />
                                            <%# Eval("CDate")%>
                                            <%# Eval("CDay") %>
                                        </div>
                                        <asp:Repeater ID="rptinstaller1" runat="server">
                                            <ItemTemplate>
                                                <table width="100%" class="content" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="20%">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <asp:HiddenField ID="hdnBookingDate" runat="server" Value='<%# Eval("BookingDate")%>' />
                                                                    <asp:HiddenField ID="hdnProjectID" runat="server" Value='<%# Eval("ProjectID")%>' />
                                                                    <th align="left" colspan="2"><%# Eval("contact")%></th>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100"><%# Eval("ProjectNumber")%></td>
                                                                    <td><%# Eval("Customer")%></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="25%">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th align="left" colspan="2">Booking Time: 
                                                                                 <%# Eval("BookingAmPm")%>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100" align="right"><%# Eval("ContMobile")%></td>
                                                                    <td align="right"><%#Eval("Project")%></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10%">&nbsp;</td>
                                                        <td width="23%">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td><%# Eval("HouseType")%></td>
                                                                    <td><%# Eval("RoofType")%></td>
                                                                    <td><%# Eval("RoofAngle")%></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3"><%#Eval("SystemDetails")%></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="right" width="70">
                                                                        <div class="smallbox"></div>
                                                                    </td>
                                                                    <td align="right">
                                                                        <div class="largebox"></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater ID="rptMtce" runat="server" OnItemDataBound="rptMtce_ItemDataBound">
                                            <ItemTemplate>
                                                <tr class="brdergray">
                                                    <td valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left">
                                                                    <%--<asp:Image ID="imgMtce" ImageUrl="~/images/icon_maintenance.png" runat="server" Height="10px" Width="10px" />--%>
                                                                    <asp:HiddenField ID="hdnProjectIDM" runat="server" Value='<%# Eval("ProjectID")%>' />
                                                                    <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" title="Detail"
                                                                        NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>' Target="_blank"><%#Eval("Project")%></asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <%# Eval("InstallState")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <%# Eval("ProjectID")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <strong><%# Eval("contact")%></strong>
                                                                    <br />
                                                                    <div runat="server" id="divisformbay" visible="false">
                                                                        <asp:CheckBox ID="chkformbay" runat="server" />
                                                                        <label for='<%# Container.FindControl("chkformbay").ClientID %>' onclick="return false;">
                                                                            <span></span>
                                                                        </label>
                                                                        &nbsp;IsFormbay
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                    <td valign="top" align="right">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>
                                                                <td align="center">&nbsp;
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="nopad">
                                                        <div class="brdbtm">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>


                                        <asp:Repeater ID="rptcauMtce" runat="server" OnItemDataBound="rptcauMtce_ItemDataBound">
                                            <ItemTemplate>
                                                <tr class="brdergray">
                                                    <td valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left">
                                                                    <strong>CM: </strong>
                                                                    <asp:HiddenField ID="hdnProjectIDC" runat="server" Value='<%# Eval("ProjectID")%>' />

                                                                    <asp:HyperLink ID="hypDetail" runat="server" data-original-title="Detail View" class="tooltips"
                                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("Project")%></asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <%# Eval("InstallState")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <%# Eval("ProjectNumber")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <%# Eval("contact")%>
                                                                    <br />
                                                                    <div runat="server" id="divisformbay" visible="false">
                                                                        <asp:CheckBox ID="chkformbay" runat="server" />
                                                                        <label for='<%# Container.FindControl("chkformbay").ClientID %>' onclick="return false;">
                                                                            <span></span>
                                                                        </label>
                                                                        &nbsp;IsFormbay
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                    <td valign="top" align="right">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>
                                                                <td align="center">&nbsp;
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="nopad">
                                                        <div class="brdbtm">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
         </ContentTemplate>
     <Triggers>
        <%--<asp:AsyncPostBackTrigger ControlID="btnpreviousweek" />
        <asp:AsyncPostBackTrigger ControlID="btnnextweek" />--%>
    </Triggers>
</asp:UpdatePanel>
    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (divprintinstall.ClientID) %>').innerHTML;
            var html = '<html><head>' +
             '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
        '</head><body style="background:#ffffff;">' +
        PageHTML +
        '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
        "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
    </script>

</asp:Content>

