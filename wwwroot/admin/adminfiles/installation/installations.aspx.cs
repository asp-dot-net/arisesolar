﻿using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Text;

public partial class admin_adminfiles_view_installations : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int offset = DateTime.Now.DayOfWeek - DayOfWeek.Monday;
            DateTime lastMonday = DateTime.Now.AddDays(-offset);
            BindWeekDates(lastMonday);

            ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
            ddlSearchState.DataMember = "State";
            ddlSearchState.DataTextField = "State";
            ddlSearchState.DataValueField = "State";
            ddlSearchState.DataBind();

            if (Roles.IsUserInRole("Installer"))
            {
                ddlInstaller.Visible = false;
            }
            else
            {
                ddlInstaller.Visible = true;
            }
            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataBind();

        }
    }
    public void BindWeekDates(DateTime startdate)
    {
        ArrayList values = new ArrayList();
        for (int i = 0; i < 7; i++)
        {
            values.Add(new PositionData(startdate.AddDays(i).ToShortDateString(), CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(startdate.AddDays(i).DayOfWeek)));
        }
       

        RptDays.DataSource = values;
        RptDays.DataBind();

        //RptDays1.DataSource = values;
        //RptDays1.DataBind();

        RepeaterItem item0 = RptDays.Items[0];
        string hdnDate0 = ((HiddenField)item0.FindControl("hdnDate")).Value;
        RepeaterItem item6 = RptDays.Items[6];
        string hdnDate6 = ((HiddenField)item6.FindControl("hdnDate")).Value;

        //RepeaterItem item01 = RptDays1.Items[0];
        //string hdnDate0 = ((HiddenField)item01.FindControl("hdnDate")).Value;
        //RepeaterItem item6 = RptDays.Items[6];
        //string hdnDate6 = ((HiddenField)item6.FindControl("hdnDate")).Value;

        ltdate.Text = string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate0)) + " - " + string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate6));

        lblStartDate.Text = string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate0));
        lblEndDate.Text = string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate6));
        lblInstaller.Text = ddlInstaller.SelectedItem.Text;
    }
    public class PositionData
    {
        private string CDate;
        private string CDay;
        public PositionData(string CDate, string CDay)
        {
            this.CDate = CDate;
            this.CDay = CDay;
        }
        public string CDATE
        {
            get
            {
                return CDate;
            }
        }
        public string CDAY
        {
            get
            {
                return CDay;
            }
        }
    }

    protected void btntoday_Click(object sender, EventArgs e)
    {
        BindWeekDates(DateTime.Now);
    }

    protected void btnpreviousweek_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[0];
            string startdate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(startdate).AddDays(-7));
        }
    }
    protected void btnnextweek_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[6];
            string enddate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(enddate).AddDays(1));
        }
    }

    protected void RptDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string userid = "";
        int count = 0;
        if (Roles.IsUserInRole("Installer"))
        {
            userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        }
        else
        {
            userid = "";
        }
        SttblEmployees stuser = ClstblEmployees.tblEmployees_SelectByUserId(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());

        HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
     //  Response.Write(hdnDate.Value + "=" + txtSearch.Text + "=" + ddlSearchState.SelectedValue + "=" + ddlInstaller.SelectedValue + "=" + userid + "=" + ddlisformbay.SelectedValue);
      //  dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
        if (ddlType.SelectedValue == "1")
        {
            DataTable dt = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {

                dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {

                    if (stuser.EmpNicName.ToLower() == "karen")
                    {
                        dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                    else
                    {
                       
                        dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                }
            }

            Repeater rptinstaller1 = (Repeater)e.Item.FindControl("rptinstaller1");
            rptinstaller1.DataSource = dt;
            rptinstaller1.DataBind();

            Label lblcount = (Label)e.Item.FindControl("lblcount");
            if (dt.Rows.Count > 0)
            {
                lblcount.Text = dt.Rows.Count.ToString();
            }
            else
            {
                lblcount.Text = "-";
            }
        }
        else if (ddlType.SelectedValue == "2")
        {
            DataTable dtMtce = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    if (stuser.EmpNicName.ToLower() == "karen")
                    {
                        dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                    else
                    {
                        dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                }
            }

            Repeater rptMtce = (Repeater)e.Item.FindControl("rptMtce");
            if (dtMtce.Rows.Count > 0)
            {
                rptMtce.DataSource = dtMtce;
                rptMtce.DataBind();
            }
            else
            {
            }

            Label lblcount = (Label)e.Item.FindControl("lblcount");
            if (dtMtce.Rows.Count > 0)
            {
                lblcount.Text = dtMtce.Rows.Count.ToString();
            }
            else
            {
                lblcount.Text = "-";
            }
        }
        else if (ddlType.SelectedValue == "3")
        {
            DataTable dtMtceCau = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    if (stuser.EmpNicName.ToLower() == "karen")
                    {
                        dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                    else
                    {
                        dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                }
            }

            Repeater rptcauMtce = (Repeater)e.Item.FindControl("rptcauMtce");
            if (dtMtceCau.Rows.Count > 0)
            {
                rptcauMtce.DataSource = dtMtceCau;
                rptcauMtce.DataBind();
            }
            else
            {
            }
            Label lblcount = (Label)e.Item.FindControl("lblcount");
            if (dtMtceCau.Rows.Count > 0)
            {
                lblcount.Text = dtMtceCau.Rows.Count.ToString();
            }
            else
            {
                lblcount.Text = "-";
            }



        }
        else
        {

            DataTable dt = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {

                dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue.ToString(), userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    if (stuser.EmpNicName.ToLower() == "karen")
                    {
                        dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue.ToString(), userid, ddlisformbay.SelectedValue);
                    }
                    else
                    {
                        dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue.ToString(), userid, ddlisformbay.SelectedValue);
                    }
                }
            }

            Repeater rptinstaller1 = (Repeater)e.Item.FindControl("rptinstaller1");
            rptinstaller1.DataSource = dt;
            rptinstaller1.DataBind();



            Repeater rptMtce = (Repeater)e.Item.FindControl("rptMtce");
            DataTable dtMtce = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    //if (stuser.EmpNicName.ToLower() == "karen")
                    //{
                    //    dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                    //else
                    //{
                    //    dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                }
            }

            if (dtMtce.Rows.Count > 0)
            {
                rptMtce.DataSource = dtMtce;
                rptMtce.DataBind();
            }
            else
            {
            }


            Repeater rptcauMtce = (Repeater)e.Item.FindControl("rptcauMtce");
            DataTable dtMtceCau = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    //if (stuser.EmpNicName.ToLower() == "karen")
                    //{
                    //    dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                    //else
                    //{
                    //    dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                }
            }

            if (dtMtceCau.Rows.Count > 0)
            {
                rptcauMtce.DataSource = dtMtceCau;
                rptcauMtce.DataBind();
            }
            else
            {
            }

            Label lblcount = (Label)e.Item.FindControl("lblcount");
            if (dt.Rows.Count > 0)
            {
                lblcount.Text = (Convert.ToInt32(dt.Rows.Count) + Convert.ToInt32(dtMtce.Rows.Count) + Convert.ToInt32(dtMtceCau.Rows.Count)).ToString();
            }
            else
            {
                lblcount.Text = "-";
            }
        }
    }
    protected void rptinstaller1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnBookingDate = (HiddenField)e.Item.FindControl("hdnBookingDate");
            HiddenField hdnProjectID = (HiddenField)e.Item.FindControl("hdnProjectID");

            CheckBox chkformbay = (CheckBox)e.Item.FindControl("chkformbay");
            System.Web.UI.HtmlControls.HtmlContainerControl divisformbay = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("divisformbay");

            DataTable dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate_BookingAmPm(hdnBookingDate.Value, hdnProjectID.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue);
            Repeater rptinstaller2 = (Repeater)e.Item.FindControl("rptinstaller2");
            rptinstaller2.DataSource = dt;
            rptinstaller2.DataBind();


            if (hdnProjectID.Value != string.Empty)
            {
                SttblProjects stProjects = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
                if (stProjects.FormbayId != string.Empty)
                {
                    if (stProjects.IsFormBay == "1")
                    {
                        chkformbay.Checked = true;
                        divisformbay.Visible = true;
                    }
                    else
                    {
                        chkformbay.Checked = false;
                        divisformbay.Visible = false;
                    }
                }
                else
                {
                    divisformbay.Visible = false;
                }
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        int offset = DateTime.Now.DayOfWeek - DayOfWeek.Monday;
        DateTime Monday = DateTime.Now.AddDays(-offset);
        BindWeekDates(Monday);
    }

    protected void btnClearAll_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        ddlSearchState.SelectedValue = "";
        txtSearch.Text = string.Empty;
        ddlType.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
    }

    protected void btnGoPrint_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/installation/printinstallation.aspx");
    }
    protected void rptMtce_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlContainerControl divisformbay = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("divisformbay");
        HiddenField hdnProjectIDM = (HiddenField)e.Item.FindControl("hdnProjectIDM");
        CheckBox chkformbay = (CheckBox)e.Item.FindControl("chkformbay");


        if (hdnProjectIDM.Value != string.Empty)
        {
            SttblProjects stProjects = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectIDM.Value);
            if (stProjects.FormbayId != string.Empty)
            {
                if (stProjects.IsFormBay == "1")
                {
                    chkformbay.Checked = true;
                    divisformbay.Visible = true;
                }
                else
                {
                    chkformbay.Checked = false;
                    divisformbay.Visible = false;
                }
            }
            else
            {
                divisformbay.Visible = false;
            }

        }
    }
    protected void rptcauMtce_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlContainerControl divisformbay = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("divisformbay");
        HiddenField hdnProjectIDC = (HiddenField)e.Item.FindControl("hdnProjectIDC");
        CheckBox chkformbay = (CheckBox)e.Item.FindControl("chkformbay");

        if (hdnProjectIDC.Value != string.Empty)
        {
            SttblProjects stProjects = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectIDC.Value);
            if (stProjects.FormbayId != string.Empty)
            {
                if (stProjects.IsFormBay == "1")
                {
                    chkformbay.Checked = true;
                    divisformbay.Visible = true;
                }
                else
                {
                    chkformbay.Checked = false;
                    divisformbay.Visible = false;
                }
            }
            else
            {
                divisformbay.Visible = false;
            }

        }
    }

    protected void RptDays1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string userid = "";
        if (Roles.IsUserInRole("Installer"))
        {
            userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        }
        else
        {
            userid = "";
        }
        SttblEmployees stuser = ClstblEmployees.tblEmployees_SelectByUserId(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());

        HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
        if (ddlType.SelectedValue == "1")
        {
            DataTable dt = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {

                dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {

                    if (stuser.EmpNicName.ToLower() == "karen")
                    {
                        dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                    else
                    {
                        dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                }
            }

            Repeater rptinstaller1 = (Repeater)e.Item.FindControl("rptinstaller1");
            rptinstaller1.DataSource = dt;
            rptinstaller1.DataBind();

        }
        else if (ddlType.SelectedValue == "2")
        {
            DataTable dtMtce = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    if (stuser.EmpNicName.ToLower() == "karen")
                    {
                        dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                    else
                    {
                        dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                }
            }

            Repeater rptMtce = (Repeater)e.Item.FindControl("rptMtce");
            if (dtMtce.Rows.Count > 0)
            {
                rptMtce.DataSource = dtMtce;
                rptMtce.DataBind();
            }
            else
            {
            }
        }
        else if (ddlType.SelectedValue == "3")
        {
            DataTable dtMtceCau = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    if (stuser.EmpNicName.ToLower() == "karen")
                    {
                        dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                    else
                    {
                        dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    }
                }
            }

            Repeater rptcauMtce = (Repeater)e.Item.FindControl("rptcauMtce");
            if (dtMtceCau.Rows.Count > 0)
            {
                rptcauMtce.DataSource = dtMtceCau;
                rptcauMtce.DataBind();
            }
            else
            {
            }
        }
        else
        {

            DataTable dt = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {

                dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue.ToString(), userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    //if (stuser.EmpNicName.ToLower() == "karen")
                    //{
                    //    dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue.ToString(), userid, ddlisformbay.SelectedValue);
                    //}
                    //else
                    //{
                    //    dt = ClstInstallations.tblProjects_GetData_ByInstallBookingDate(hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue.ToString(), userid, ddlisformbay.SelectedValue);
                    //}
                }
            }

            Repeater rptinstaller1 = (Repeater)e.Item.FindControl("rptinstaller1");
            rptinstaller1.DataSource = dt;
            rptinstaller1.DataBind();



            Repeater rptMtce = (Repeater)e.Item.FindControl("rptMtce");
            DataTable dtMtce = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    //if (stuser.EmpNicName.ToLower() == "karen")
                    //{
                    //    dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                    //else
                    //{
                    //    dtMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("0", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                }
            }

            if (dtMtce.Rows.Count > 0)
            {
                rptMtce.DataSource = dtMtce;
                rptMtce.DataBind();
            }
            else
            {
            }

            Repeater rptcauMtce = (Repeater)e.Item.FindControl("rptcauMtce");
            DataTable dtMtceCau = new DataTable();
            if (Roles.IsUserInRole("Installer"))
            {
                dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
            }
            else
            {
                if (stuser.EmpNicName != string.Empty)
                {
                    //if (stuser.EmpNicName.ToLower() == "karen")
                    //{
                    //    dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, "TAS", ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                    //else
                    //{
                    //    dtMtceCau = ClstblProjectMaintenance.tblProjectMaintenance_SelectByDate("1", hdnDate.Value, txtSearch.Text, ddlSearchState.SelectedValue, ddlInstaller.SelectedValue, userid, ddlisformbay.SelectedValue);
                    //}
                }
            }

            if (dtMtceCau.Rows.Count > 0)
            {
                rptcauMtce.DataSource = dtMtceCau;
                rptcauMtce.DataBind();
            }
            else
            {
            }
        }
    }
}