﻿<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="searchdata.aspx.cs" Inherits="admin_adminfiles_searchdata" MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
                    </ContentTemplate>
    </asp:UpdatePanel>
            <script type="text/javascript">
                
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);


            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
                </script>
            <section class="row m-b-md">
                
                <div class="page-body padtopzero">
                <div class="col-sm-12 minhegihtarea">
                    <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>SEARCH
          <asp:Literal runat="server" ID="Literal1"></asp:Literal>
                </h5>
            </div>
                    <div class="contactsarea">
                        <div id="divleft" runat="server">
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                                </div>
                                <div class="alert alert-info" id="PanAlreadExists" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this Address already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                                <div class="icon-remove-sign" id="divTeamTime" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Appointment Time over.</strong>
                                </div>
                            </div>
                            <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                                <ProgressTemplate>
                                    <asp:Image ID="imgloading" ImageUrl="~/admin/images/loading.gif" Style="z-index: 1000001111;" AlternateText="processing"
                                        runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />

                            <div class="clear"></div>
                            <div class="contacttoparea" id="PanSearch" runat="server">
                                <span id="searchbar" runat="server" class="imgbtn"></span>
                                <div class="widget-body shadownone brdrgray">
                                <div class="toppaneldiv">
                                    <div class="form-inline">
                                        <div class="padd10all">
                                            <div class="dataTables_length showdata col-sm-8">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                    aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%-- <td valign="middle" id="tdAll2" class="paddtop3td" runat="server">&nbsp;View All</td>--%>
                                                        </tr>
                                                    </table>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                    </div>
                            </div>
                            <div>&nbsp;</div>
                            <div class="content padtopzero marbtm50 finalgrid">
                                <div class="leftgrid">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="PanGrid" id="PanGrid" runat="server">
                                            <div class="table-responsive xscroll  noPagination">
                                            <asp:GridView ID="GridView1" DataKeyNames="CustomerID" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                runat="server" OnPageIndexChanging="GridView1_PageIndexChanging" AllowSorting="true" OnSorting="GridView1_Sorting" 
                                                
                                                OnDataBound="GridView1_DataBound" OnRowCreated="GridView1_RowCreated"   OnRowDataBound="GridView1_RowDataBound"
                                                AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                <Columns>
                                                    
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                         <%--   <asp:HiddenField ID="hndcustid" runat="server" Value="<%# Eval("compid") %>" > </asp:HiddenField >--%>
                                                             <asp:HiddenField runat="server" ID="hndProjectID" Value='<%#Eval("ProjectID")%>' />
                                                             <asp:HiddenField runat="server" ID="hndContactID" Value='<%#Eval("ContactID")%>' />
                                                              <asp:HiddenField runat="server" ID="hdnid" Value='<%#Eval("CustomerID")%>' />
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                                   <img id='img1' src="../../images/icon_plus.png" />
                                                              <%--  <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />--%>
                                                            </a>
                                                         
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                                        <ItemTemplate>
                                                              <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"
                                                                    Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/CustomerNew.aspx?m=comp&compid="+Eval("CustomerID") %>'>
                                                                       <%#Eval("Customer")%></asp:HyperLink>
                                                         <%--   <asp:Label ID="Label2" runat="server" Width="200px"><%#Eval("Customer")%></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'
                                                                Width="180px"><%#Eval("StreetAddress")%> </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Location"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="180px"><%#Eval("Location")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                                    
                                                    <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label5" runat="server" Width="100px"><%#Eval("CustPhone")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label7" runat="server" Width="100px"><%#Eval("ContMobile")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label8" runat="server" Width="100px"><%#Eval("ContEmail")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SalesRepName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label9" runat="server" Width="100px"><%#Eval("SalesRepName")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                  
                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                        <ItemTemplate>

                                                            <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                                <td colspan="98%" class="details">
                                                                    <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                        <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                            <tr class="GridviewScrollItem">
                                                                                <td width="180px"><b>Company No</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblcompanyno" runat="server" Width="130px"></asp:Label>
                                                                                </td>
                                                                                                                                                           

                                                                                <td width="180px" runat="server" >
                                                                                    <b>Lead Status</b>
                                                                                </td>
                                                                                <td runat="server">
                                                                                    <asp:Label ID="lblleadstatus" runat="server" Width="130px"> </asp:Label>
                                                                                </td>
                                                                                  <td><b>Next Followup Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblnextfollowupdate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr runat="server" id="tdtype" class="GridviewScrollItem">
                                                                              
                                                                                <td><b>Desp</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lbldesp" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Project Type</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblprojecttype" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Project Number</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblprojectno" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                            <tr runat="server">
                                                                              
                                                                                <td><b>Manual No</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblmanualno" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Project Opened Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblprjopendate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>System Details</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblsys_detail" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>

                                                                             <tr runat="server">
                                                                              
                                                                                <td><b>House Type</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblhousetype" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Roof Type</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblrooftype" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Roof Angle</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblroofangle" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                             <tr runat="server">
                                                                              
                                                                                <td><b>Eletrical Distributor</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lbleletricaldist" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Retailer</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblretialer" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>NMI</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblNMI" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>



                                                                            <tr runat="server">
                                                                              
                                                                                <td><b>RPN</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblRPN" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>LN </b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblLN" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Dist AppliedDate</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lbldistappliedDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                            <tr id="Tr1" runat="server">
                                                                              
                                                                                <td><b>Approcal Ref</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblApprocalRef" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Dist Approced Date </b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDistApprocedDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Payment Option</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblpaymentoption" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                            <tr id="Tr2" runat="server">
                                                                              
                                                                                <td><b>Deposit Option</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lbldepositoption" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Finance Payment Type </b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblfinancepay_type" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Total Cost</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lbltotal_cost" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>

                                                                             <tr id="Tr3" runat="server">
                                                                              
                                                                                <td><b>Paid to Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblpaiddate" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Bal Owing </b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblbal_owing" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Quote Accepted</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblquote_accept" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>

                                                                             <tr id="Tr5" runat="server">
                                                                              
                                                                                <td><b>Last Created QuoteDate</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lbllastcreatedQuoteDate" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Depoist Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDepoistDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Active Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblActiveDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>

                                                                             <tr id="Tr4" runat="server">
                                                                              
                                                                                <td><b>Application Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lbllApplicationDate" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                               
                                                                                <td><b>Document Received Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDocumentReceivedDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                 <td>

                                                                                 </td>
                                                                                 <td></td>
                                                                                
                                                                            </tr>

                                                                             <tr id="Tr6" runat="server">
                                                                              
                                                                                <td><b>Document Received By</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDocumentReceivedBy" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Document Verified</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDocumentVerified" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Sent By</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblSentBy" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                             <tr id="Tr7" runat="server">
                                                                              
                                                                                <td><b>Sent Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblSentDate" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Pay Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPayDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Total paid</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblTotalpaid" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                            <tr id="Tr8" runat="server">
                                                                              
                                                                                <td><b>Pay by</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPayby" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Red by</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblRedby" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Purchase No</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPurchaseNo" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                              <tr id="Tr9" runat="server">
                                                                              
                                                                                <td><b>Document Sent Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDocumentSentDate" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Document Sent By</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblDocumentSentBy" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Postal Tracking Number</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPostalTrackingNumber" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                             <tr id="Tr10" runat="server">
                                                                              
                                                                                <td><b> Install Booking date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInstallBookingdate" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Installer</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInstaller" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Is Formbay</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblISformbay" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                             <tr id="Tr11" runat="server">
                                                                              
                                                                                <td><b>Formbay ID</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblFormbayID" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Install Completed</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInstallCompleted" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Install DocRec</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInstallDocRec" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                            <tr id="Tr12" runat="server">
                                                                              
                                                                                <td><b>Certificate Issued </b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblCertificateIssued" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Meter Time</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblMeterTime" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>MeterApply Ref</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblMeterApplyRef" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>



                                                                              <tr id="Tr13" runat="server">
                                                                              
                                                                                <td><b>Inspector Name </b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInspectroName" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Inspection Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInspectionDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>STC Applied Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblSTCAppliedDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                            
                                                                              <tr id="Tr14" runat="server">
                                                                              
                                                                                <td><b>PVD Status </b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPVDStatus" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>PVD Number</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblPVDNumber" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Inv No</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInvNo" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                             <tr id="Tr15" runat="server">
                                                                              
                                                                                <td><b> Inv Amnt</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInvAmnt" runat="server" Width="100px" ></asp:Label>
                                                                                </td>

                                                                                <td><b>Inv Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInvDate" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                <td><b>Advance Amount</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblAdvanceAmount" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                            </tr>


                                                                             <tr id="Tr16" runat="server">
                                                                              
                                                                                <td><b>Advance Pay Date</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblAdvancePayDate" runat="server" Width="100px"></asp:Label>
                                                                                </td>

                                                                                <td><b>Notes</b></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblNotes" runat="server" Width="100px"> </asp:Label>
                                                                                </td>
                                                                                
                                                                                
                                                                            </tr>
                                                                           


                                                                          


                                                                           
                                                                        </table>

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <%--<RowStyle BackColor="#EFF3FB" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />--%>

                                            </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                        <asp:HiddenField runat="server" ID="hdncountdata" />
                                        <div class="paginationnew1" runat="server" id="divnopage">
                                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                                                <div class="pagination paginationGrid">
                                                                    <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                                                    <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                                            <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                                                    <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

</asp:Content>


