using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

public partial class admin_adminfiles_reports_noinstalldate : System.Web.UI.Page
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
          // CompareValidatorStartDate.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            BindGrid(0);

            BindDropDown();
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
        }
    }

    public void BindDropDown()
    {
        ddlSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectADC();
        ddlSearchStatus.DataMember = "ProjectStatus";
        ddlSearchStatus.DataTextField = "ProjectStatus";
        ddlSearchStatus.DataValueField = "ProjectStatusID";
        ddlSearchStatus.DataBind();

        ddlState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlState.DataValueField = "State";
        ddlState.DataTextField = "State";
        ddlState.DataMember = "State";
        ddlState.DataBind();
    }

    protected DataTable GetGridData()
    {
       
        DataTable dt = new DataTable();
        dt = Reports.tblProjects_NoInstallDate_Report(ddlState.SelectedValue, ddlSearchStatus.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), txtpcodefrom.Text, txtpcodeto.Text);
        if (dt.Rows.Count > 0)
        {
            lblTotalJob.Text = dt.Rows.Count.ToString();
            lblTotalPanel.Text = dt.Rows[0]["totalpanels"].ToString();
            lblRemBal.Text = dt.Rows[0]["rembal"].ToString();
            try
            {
            }
            catch { }
            try
            {
            }
            catch { }
        }
        else
        {
            lblTotalJob.Text = "0";
            lblTotalPanel.Text = "0";
            lblRemBal.Text = "0";
        }

        
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            //divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue !="All")
{
            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
            {
                //========label Hide
                divnopage.Visible = false;
            }
            else
            {
               divnopage.Visible = true;
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
        }
		else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }
    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    ddlSearchStatus.SelectedValue = "";
    //    ddlState.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtpcodeto.Text = string.Empty;
    //    txtpcodefrom.Text = string.Empty;

    //    BindGrid(0);
    //}

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = Reports.tblProjects_NoInstallDate_Report(ddlState.SelectedValue, ddlSearchStatus.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), txtpcodefrom.Text, txtpcodeto.Text);

        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "NoInstallReport" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 5, 6, 7, 8, 10 };
            string[] arrHeader = { "ProjectNumber", "Project", "Panels", "QuotePrice", "Opened" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchStatus.SelectedValue = "";
        ddlState.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtpcodeto.Text = string.Empty;
        txtpcodefrom.Text = string.Empty;

        BindGrid(0);
    }
}