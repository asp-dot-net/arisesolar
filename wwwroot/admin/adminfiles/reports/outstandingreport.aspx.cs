﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_outstandingreport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //BindDropDown();
            
        }
    }
    //protected override object LoadPageStateFromPersistenceMedium()
    //{
    //    string viewStateID = (string)((Pair)base.LoadPageStateFromPersistenceMedium()).Second;
    //    string stateStr = (string)Cache[viewStateID];
    //    if (stateStr == null)
    //    {
    //        string fn = Path.Combine(this.Request.PhysicalApplicationPath, @"App_Data/ViewState/" + viewStateID);
    //        //or using var fn = this.Server.MapPath(@"~/App_Data/ViewState/" + viewStateID);
    //        stateStr = File.ReadAllText(fn);
    //    }
    //    return new ObjectStateFormatter().Deserialize(stateStr);
    //}
    //protected override void SavePageStateToPersistenceMedium(object state)
    //{
    //    string value = new ObjectStateFormatter().Serialize(state);
    //    string viewStateID = (DateTime.Now.Ticks + (long)this.GetHashCode()).ToString();
    //    string fn = Path.Combine(this.Request.PhysicalApplicationPath, @"App_Data/ViewState/" + viewStateID);
    //    //ThreadPool.QueueUserWorkItem(File.WriteAllText(fn, value));
    //    File.WriteAllText(fn, value);
    //    Cache.Insert(viewStateID, value);
    //    base.SavePageStateToPersistenceMedium(viewStateID);
    //}
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string fin = "";
        DataTable dtfin = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        if (dtfin.Rows.Count > 0)
        {
            if (ddlFinanceWith.SelectedValue.ToString() == "1")
            {
                fin = "1";
            }
            if (ddlFinanceWith.SelectedValue.ToString() == "2")
            {
                foreach (DataRow dr in dtfin.Rows)
                {
                    if (dr["FinanceWithID"].ToString() != "1")
                    {
                        fin += dr["FinanceWithID"] + ",";
                    }
                }
                fin = fin.Substring(0, fin.Length - 1);
            }
        }
        DateTime start = Convert.ToDateTime(txtStartDate.Text);
        DateTime End = Convert.ToDateTime(txtEndDate.Text);
        DataTable dt = ClstblProjects.tblprojects_outstanding(start.AddMonths(0).ToShortDateString(), End.AddMonths(0).ToShortDateString(), fin);

        if(dt.Rows.Count>0)
        {
            lbtnExport.Visible = true;
        }
        else
        {
            lbtnExport.Visible = false;
        }

        GridView1.DataSource = dt;
        GridView1.DataBind();
       // for (int i = 0; i < GridView1.Columns.Count; i++)
        //{
       
        //}
        //GridView1

        //Response.Write(GridView1.ControlStyleCreated + "<br>");
        //foreach (GridView row in GridView1.Columns)
        //{
        //    Response.Write(row.AllowPaging + "<br>");


        //    //TempGridView.Columns.Add(bfield);
        //}

        //GridView1.Columns[1].ItemStyle.CssClass = "center-text";
        //GridView1.Columns[1].HeaderStyle.CssClass = "center-text";
        //if (dt.Rows.Count > 0)
        //{
        //    DataTable dtproject = dt.AsEnumerable().GroupBy(r => new { projectid = r.Field<Int32>("projectid") }).Select(r => r.First()).CopyToDataTable();
        //    StringBuilder starthtml= new StringBuilder("<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\" class=\"table table-bordered table-hover\">");
        //    StringBuilder generateReport = new StringBuilder("");
        //    StringBuilder headerhtml = new StringBuilder("<tr><td>Project Number</td><td>Project Cost</td>");
        //    StringBuilder footerhtml = new StringBuilder("<tr><td><b>Total</b></td><td>&nbsp;</td>");
        //    int i = 0;
        //    DataTable dttime = dt.AsEnumerable().GroupBy(r => new { omonth = r.Field<Int32>("omonth"), oyear = r.Field<Int32>("oyear") }).Select(r => r.First()).OrderBy(r => r.Field<Int32>("oyear")).ThenBy(r => r.Field<Int32>("omonth")).CopyToDataTable();
        //    //DataView dv = dttime.DefaultView;
        //    //dv.Sort = "oyear, omonth ASC";

        //    foreach (DataRow drpro in dtproject.Rows)
        //    {
        //        i++;
        //        generateReport.Append("<tr><td>" + drpro["projectname"].ToString() + "</td><td>" + drpro["qamount"].ToString() + "</td>");
        //        int proid = Convert.ToInt32(drpro["projectid"].ToString());
        //        int instmonth = 0;
        //        if (drpro["installbookingdate"].ToString() != "")
        //        {
        //            instmonth = Convert.ToDateTime(drpro["installbookingdate"].ToString()).Month;
        //        }
        //        foreach (DataRow drtime in dttime.Rows)
        //        {
        //            int month = Convert.ToInt32(drtime["omonth"].ToString());
        //            int year = Convert.ToInt32(drtime["oyear"].ToString());
        //            if (i == 1)
        //            {
        //                //==========Header============
        //                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
        //                string strMonthName = mfi.GetMonthName(Convert.ToInt32(drtime["omonth"].ToString())).ToString();
        //                headerhtml.Append("<td>" + strMonthName + " - " + drtime["oyear"].ToString() + "</td>");
        //                //==========Header============
        //                //==========Footer============
        //                string tamouont = "";
        //                if (dttime.Rows.Count > 1)
        //                {
        //                    tamouont = (from DataRow dr in dt.AsEnumerable()
        //                                where (dr["omonth"].ToString() == month.ToString() && dr["oyear"].ToString() == year.ToString() && Convert.ToDateTime(dr["installbookingdate"].ToString()).Month == month)
        //                                select Convert.ToInt32(dr["amount"])).Sum().ToString();
        //                }
        //                else
        //                {
        //                    tamouont = (from DataRow dr in dt.AsEnumerable()
        //                                where (dr["omonth"].ToString() == month.ToString() && dr["oyear"].ToString() == year.ToString())
        //                                select Convert.ToInt32(dr["amount"])).Sum().ToString();
        //                }
        //                footerhtml.Append("<td><b>" + tamouont + "</b></td>");
        //                //==========Footer============
        //            }
        //            var amount = (from DataRow dr in dt.AsEnumerable()
        //                          where (dr["omonth"].ToString() == month.ToString() && dr["oyear"].ToString() == year.ToString() && dr["projectid"].ToString() == proid.ToString())
        //                          select Convert.ToInt32(dr["amount"])).Sum().ToString();
        //            generateReport.Append("<td>");
        //            generateReport.Append(amount.ToString());
        //            generateReport.Append("</td>");
        //        }
        //        generateReport.Append("</tr>");
        //    }
        //    footerhtml.Append("</tr>");
        //    headerhtml.Append("</tr>");
        //    string htmlcontent = starthtml.ToString() + headerhtml.ToString() + generateReport.ToString() + footerhtml.ToString() + "</table>";
        //    Literal1.Text = htmlcontent;
        //}
    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string fin = "";
        DataTable dtfin = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        if (dtfin.Rows.Count > 0)
        {
            if (ddlFinanceWith.SelectedValue.ToString() == "1")
            {
                fin = "1";
            }
            if (ddlFinanceWith.SelectedValue.ToString() == "2")
            {
                foreach (DataRow dr in dtfin.Rows)
                {
                    if (dr["FinanceWithID"].ToString() != "1")
                    {
                        fin += dr["FinanceWithID"] + ",";
                    }
                }
                fin = fin.Substring(0, fin.Length - 1);
            }
        }
        DateTime start = Convert.ToDateTime(txtStartDate.Text);
        DateTime End = Convert.ToDateTime(txtEndDate.Text);
        DataTable dtRecords = ClstblProjects.tblprojects_outstanding(start.AddMonths(0).ToShortDateString(), End.AddMonths(0).ToShortDateString(), fin);

        GridView TempGridView = new GridView();
        foreach (DataColumn column in dtRecords.Columns)
        {
            //Response.Write(column.ColumnName+"<br>");

            BoundField bfield = new BoundField();
            bfield.HeaderText = column.ColumnName;
            bfield.DataField = column.ColumnName;
            //TempGridView.Columns.Add(bfield);
        }

        TempGridView.DataSource = dtRecords;
        TempGridView.DataBind();
        //Response.End();
        TempGridView.AllowPaging = false;

        PrepareGridViewForExport(TempGridView);
        string attachment = "attachment; filename=Outstanding Report"+ System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();

        HtmlTextWriter htw = new HtmlTextWriter(sw);
        TempGridView.RenderControl(htw);

        Response.Write(sw.ToString());
        Response.End();
    }
    protected void lbtn2Export_Click(object sender, EventArgs e)
    {
        Response.Clear();

        Response.AddHeader("content-disposition", "attachment;filename = OutstandingExcelReport_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls");
    


        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite =
        new HtmlTextWriter(stringWrite);

        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();

    }

    private void PrepareGridViewForExport(Control gv)
    {
        LinkButton lb = new LinkButton();
        Literal l = new Literal();
        string name = String.Empty;

        for (int i = 0; i < gv.Controls.Count; i++)
        {
            if (gv.Controls[i].GetType() == typeof(LinkButton))
            {
                l.Text = (gv.Controls[i] as LinkButton).Text;
                Response.Write(l.Text);
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(HyperLink))
            {
                l.Text = (gv.Controls[i] as HyperLink).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(DropDownList))
            {
                l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(CheckBox))
            {
                l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(HiddenField))
            {
                l.Text = "";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            if (gv.Controls[i].HasControls())
            {
                PrepareGridViewForExport(gv.Controls[i]);
            }
        }
        //Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }

}