﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="outstandingreport.aspx.cs" Culture="en-GB" UICulture="en-GB"
    Inherits="admin_adminfiles_reports_outstandingreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').css('display', 'none');
        }

        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.redreq').click(function () {
                formValidate();
            });
        }



    </script>

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Outstanding Report</h5>
    </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <%--   <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        }
    </script>--%>
    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" visible="false" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>
        


    </asp:Panel>
    <div class="page-body padtopzero">
        <div class=" animate-panel padbtmzero padtopzero">
            <div>
                <div class="hpanel marbtmzero">
                    <div class="">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div>
                                        <div class="dataTables_filter col-sm-12 Responsive-search printorder">
                                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                                <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="input-group" id="divCustomer" runat="server">
                                                                <asp:DropDownList ID="ddlFinanceWith" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                    <asp:ListItem Value="">Finance With</asp:ListItem>
                                                                    <asp:ListItem Value="1">Cash</asp:ListItem>
                                                                    <asp:ListItem Value="2">Finance</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>


                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtStartDate" ErrorMessage="" ValidationGroup="search"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtEndDate" ErrorMessage="" ValidationGroup="search"></asp:RequiredFieldValidator>
                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                            </div>
                                                            <div class="input-group">
                                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info redreq btnsearchicon" ValidationGroup="search" Text="Search" OnClick="btnSearch_Click" />
                                                            </div>
                                                            <div class="printorder">
                                                                <div id="tdExport" class="pull-right" runat="server">
                                                                    <%--  <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                                    <asp:LinkButton ID="lbtnExport" CssClass="btn btn-success btn-xs Excel" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                                                        CausesValidation="false" OnClick="lbtn2Export_Click"><i class="fa fa-file-excel-o"></i> Excel</asp:LinkButton>
                                                                    <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>
                                                                    <%--  </ol>--%>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="finalgrid">
        <div class="">
            <div >
                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="Div2" runat="server">
                    <div>
                        <div class="row" id="PanAddUpdate" runat="server">
                            <div class="col-md-12">
                                <div class="">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div >
                                                <%--<div class="form-group tableblack ">--%>
                                                <div class="table-responsive">
                                                    <%--<asp:Literal ID="Literal1" runat="server"></asp:Literal>--%>
                                                    <asp:GridView ID="GridView1" runat="server"
                                                        CssClass="table table-bordered table-hover outstandingtable"
                                                        CellPadding="5" CellSpacing="0">
                                                    </asp:GridView>
                                                </div>
                                                <%--</div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    
    <%-- </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>

