﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_leadassignreport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDropDown();

            rptSalesRep.DataSource = Reports.tblEmployees_Select_TeamWise(ddlTeam.SelectedValue);
            rptSalesRep.DataBind();
        }
    }

    public void BindDropDown()
    {
        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlState.DataValueField = "State";
        ddlState.DataTextField = "State";
        ddlState.DataMember = "State";
        ddlState.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        rptSalesRep.DataSource = Reports.tblEmployees_Select_TeamWise(ddlTeam.SelectedValue);
        rptSalesRep.DataBind();
    }
    protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    {
        ddlTeam.SelectedValue = "";
        ddlState.SelectedValue = "";

        rptSalesRep.DataSource = Reports.tblEmployees_Select_TeamWise("");
        rptSalesRep.DataBind();
    }
    protected void rptSalesRep_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndEmployeeID = (HiddenField)e.Item.FindControl("hndEmployeeID");
            Repeater rptOnline = (Repeater)e.Item.FindControl("rptOnline");
            Repeater rptOthers = (Repeater)e.Item.FindControl("rptOthers");
            Repeater rptTeleMktg = (Repeater)e.Item.FindControl("rptTeleMktg");

            rptOnline.DataSource = Reports.tblProjects_LeadAssign_ReportOnline(hndEmployeeID.Value, ddlState.SelectedValue, txtStartDate.Text.Trim(),txtEndDate.Text.Trim());
            rptOnline.DataBind();

            rptOthers.DataSource = Reports.tblProjects_LeadAssignOther_Report(hndEmployeeID.Value, ddlState.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            rptOthers.DataBind();

            rptTeleMktg.DataSource = Reports.tblProjects_LeadAssign_Report("3", hndEmployeeID.Value, ddlState.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            rptTeleMktg.DataBind();

            DataTable dtOnline = Reports.tblProjects_Count_Online(ddlTeam.SelectedValue, ddlState.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            lblOnline.Text = dtOnline.Rows[0]["total"].ToString();

            DataTable dtOther = Reports.tblCustomers_Count_LeadOther(ddlTeam.SelectedValue, ddlState.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            lblOther.Text = dtOther.Rows[0]["total"].ToString();

            DataTable dtTelemktg = Reports.tblCustomers_Count_Lead("3", ddlTeam.SelectedValue, ddlState.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            lblTeleMktg.Text = dtTelemktg.Rows[0]["total"].ToString();
        }
    }
}