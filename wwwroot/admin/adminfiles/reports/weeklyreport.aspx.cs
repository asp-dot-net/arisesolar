﻿using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_weeklyreport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlYear.DataSource = Reports.Reports_Select_Year();
            ddlYear.DataValueField = "Year";
            ddlYear.DataTextField = "Year";
            ddlYear.DataMember = "Year";
            ddlYear.DataBind();

            int year = DateTime.Now.Year;
            ddlYear.SelectedValue = Convert.ToString(year);

            Bindweek();
            ddlWeek.SelectedValue = (FirstDayOfWeek(DateTime.Now).ToShortDateString());
            
            txtStartDate.Text = (FirstDayOfWeek(DateTime.Now).ToShortDateString());
           
            txtEndDate.Text = Convert.ToDateTime(txtStartDate.Text).AddDays(6).ToString();

            BindState();
            BindTotal();
        }
    }

    public void BindTotal()
    {
        if (ddlWeek.SelectedValue != string.Empty)
        {
            //DateTime dtstartdate = Convert.ToDateTime(ddlWeek.SelectedValue);
            //DateTime dtenddate = dtstartdate.AddDays(6);
            //string startdate = Convert.ToString(dtstartdate);
            //string enddate = Convert.ToString(dtenddate);

            string startdate = txtStartDate.Text;
            string enddate = txtEndDate.Text;
            string year = ddlYear.SelectedValue;

            DataTable dtPI = Reports.tblProjects_PanelsCount_PanelInstalled(startdate, enddate, year);
            lblTotalPI.Text = dtPI.Rows[0]["total"].ToString();

            DataTable dtJC = Reports.tblProjects_PanelsCount_JobCompleted(startdate, enddate, year);
            lblJobCompleted.Text = dtJC.Rows[0]["total"].ToString();

            DataTable dtTS = Reports.tblProjects_PanelsCount_TotalSales(startdate, enddate, year);
            try
            {
                lblTotalSales.Text = SiteConfiguration.ChangeCurrencyVal(dtTS.Rows[0]["total"].ToString());
            }
            catch { }

            DataTable dtWOIF = Reports.tblProjects_PanelsCount_WeekOutstandingIF(startdate, enddate, year);
            try
            {
                lblWOIF.Text = SiteConfiguration.ChangeCurrencyVal(dtWOIF.Rows[0]["total"].ToString());
            }
            catch { }

            DataTable dtWOEF = Reports.tblProjects_PanelsCount_WeekOutstandingEF(startdate, enddate, year);
            try
            {
                lblWOEF.Text = SiteConfiguration.ChangeCurrencyVal(dtWOEF.Rows[0]["total"].ToString());
            }
            catch { }

            DataTable dtTOIF = Reports.tblProjects_PanelsCount_TotalOutstandingIF(startdate, enddate, year);
            try
            {
                lblTOIF.Text = SiteConfiguration.ChangeCurrencyVal(dtTOIF.Rows[0]["total"].ToString());
            }
            catch { }

            DataTable dtTOEF = Reports.tblProjects_PanelsCount_TotalOutstandingEF(startdate, enddate, year);
            try
            {
                lblTOEF.Text = SiteConfiguration.ChangeCurrencyVal(dtTOEF.Rows[0]["total"].ToString());
            }
            catch { }

            DataTable dtPR = Reports.tblProjects_PanelsCount_PanelRemainingToInstall(startdate, enddate, year);
            lblPanelsRemainingToInstall.Text = dtPR.Rows[0]["total"].ToString();

            DataTable dtRSTC = Reports.tblProjects_PanelsCount_RemainingSTC(startdate, enddate, year);
            lblRemainingSTC.Text = dtRSTC.Rows[0]["total"].ToString();

            DataTable dtRP = Reports.tblProjects_PanelsCount_RemainingPaperwork(startdate, enddate, year);
            lblRemainingPaperwork.Text = dtRP.Rows[0]["total"].ToString();
        }
    }

    public void BindState()
    {
        rptStatePI.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptStatePI.DataBind();

        rptJobCompleted.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptJobCompleted.DataBind();

        rptTotalSales.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptTotalSales.DataBind();

        rptWOIF.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptWOIF.DataBind();

        rptWOEF.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptWOEF.DataBind();

        rptTOIF.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptTOIF.DataBind();

        rptTOEF.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptTOEF.DataBind();

        rptPanelsRemainingToInstall.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptPanelsRemainingToInstall.DataBind();

        rptRemainingSTC.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptRemainingSTC.DataBind();

        rptRemainingPaperwork.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptRemainingPaperwork.DataBind();

        rptFinanceTO.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        rptFinanceTO.DataBind();

        rptFinanceWO.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        rptFinanceWO.DataBind();
    }

    public DateTime FirstDayOfWeek(DateTime date1)
    {
        DateTime weekdate = date1;
        while (weekdate.DayOfWeek != DayOfWeek.Monday)
        {
            weekdate = weekdate.AddDays(-1);
        }
        return weekdate;
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlYear.SelectedValue != string.Empty)
        {
            Bindweek();

            int year = DateTime.Now.Year;
            if (ddlYear.SelectedValue == Convert.ToString(year))
            {
                Bindweek();
                ddlWeek.SelectedValue = (FirstDayOfWeek(DateTime.Now).ToShortDateString());
                txtStartDate.Text = ddlWeek.SelectedValue;
                txtEndDate.Text = Convert.ToDateTime(txtStartDate.Text).AddDays(6).ToString();
            }
            else
            {
                txtStartDate.Text = string.Empty;
                txtEndDate.Text = string.Empty;
            }
        }
        else
        {
            ddlWeek.SelectedValue = "";
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
        }
    }
    public void Bindweek()
    {
        if (ddlYear.SelectedValue != string.Empty)
        {
            ddlWeek.Items.Clear();
            ListItem lst1 = new ListItem();
            lst1.Value = "0";
            lst1.Text = "Week";
            ddlWeek.Items.Add(lst1);

            for (int i = 1; i <= 52; i++)
            {
                DateTime dtstart = GetStartofWeekByWeekNo(i, Convert.ToInt32(ddlYear.SelectedValue));
                DateTime dtenddate = dtstart.AddDays(6);
                ListItem lst = new ListItem();
                lst.Value = dtstart.ToShortDateString();
                lst.Text = i.ToString() + " week - " + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtstart)) + " - " + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtenddate));

                ddlWeek.Items.Add(lst);
            }
        }
    }
    private DateTime GetStartofWeekByDate(DateTime Day)
    {
        int awayFromWeekStart = (Convert.ToInt32(Day.DayOfWeek) - 1);
        return Day.AddDays(-awayFromWeekStart);
    }
    private DateTime GetStartofWeekByWeekNo(int WeekNo, int year)
    {
        int curYear = year;
        DateTime startOfYear = new DateTime(curYear, 1, 1);
        DateTime returnDate;
        returnDate = GetStartofWeekByDate(startOfYear);
        returnDate = returnDate.AddDays(7 * (WeekNo - 1));
        return returnDate;
    }

    protected void rptStatePI_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            //DateTime dtstart = Convert.ToDateTime(ddlWeek.SelectedValue);
            //DateTime dtenddate = dtstart.AddDays(6);

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_PanelInstalledState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void rptRemainingPaperwork_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_RemainingPaperworkState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void rptRemainingSTC_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_RemainingSTCState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void rptPanelsRemainingToInstall_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_PanelRemainingToInstallState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void rptTOEF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_TotalOutstandingEFState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void rptTOIF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");
            Repeater rptFinanceHndTO = (Repeater)e.Item.FindControl("rptFinanceHndTO");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_TotalOutstandingIFState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();

            rptFinanceHndTO.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
            rptFinanceHndTO.DataBind();
        }
    }
    protected void rptFinanceHndTO_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = ((HiddenField)((Repeater)sender).Parent.FindControl("hndState"));
            HiddenField hndFin = (HiddenField)e.Item.FindControl("hndFin");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            Repeater rptTotalTO = (Repeater)e.Item.FindControl("rptTotalTO");
            DataTable dt = Reports.tblProjects_PanelsCount_TotalOutstandingIFStateFinance(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue, hndFin.Value);
            rptTotalTO.DataSource = dt;
            rptTotalTO.DataBind();
        }
    }
    protected void rptWOEF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_WeekOutstandingEFState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void rptWOIF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");
            Repeater rptFinanceHndWO = (Repeater)e.Item.FindControl("rptFinanceHndWO");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_WeekOutstandingIFState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();

            rptFinanceHndWO.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
            rptFinanceHndWO.DataBind();
        }
    }
    protected void rptFinanceHndWO_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = ((HiddenField)((Repeater)sender).Parent.FindControl("hndState"));
            HiddenField hndFin = (HiddenField)e.Item.FindControl("hndFin");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            Repeater rptTotalWO = (Repeater)e.Item.FindControl("rptTotalWO");

            DataTable dt = Reports.tblProjects_PanelsCount_WeekOutstandingIFStateFinance(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue, hndFin.Value);
            rptTotalWO.DataSource = dt;
            rptTotalWO.DataBind();
        }
    }
    protected void rptTotalSales_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_TotalSalesState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void rptJobCompleted_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DateTime dtstart = Convert.ToDateTime(txtStartDate.Text.Trim());
            DateTime dtenddate = Convert.ToDateTime(txtEndDate.Text.Trim());

            DataTable dt = Reports.tblProjects_PanelsCount_JobCompletedState(Convert.ToString(dtstart.ToShortDateString()), Convert.ToString(dtenddate.ToShortDateString()), hndState.Value, ddlYear.SelectedValue);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlWeek.SelectedValue != string.Empty)
        {
            txtStartDate.Text = ddlWeek.SelectedValue;
            txtEndDate.Text = Convert.ToDateTime(txtStartDate.Text).AddDays(6).ToString();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ddlYear.SelectedValue != string.Empty && ddlWeek.SelectedValue != string.Empty)
        {
            BindState();
            BindTotal();
        }
    }
}