﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_instdocsreport : System.Web.UI.Page
{
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();
            
            BindGrid(0);
        }
    }
    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();
        dt = ClsProjectSale.tblInstallerExtraDocs_SelectByInst(ddlInstaller.SelectedValue, ddlProjectNumber.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
             PanGrid.Visible = false;
            if (ddlProjectNumber.SelectedValue != string.Empty)
            {
                divDocs.Visible = true;
            }
            else
            {
                divDocs.Visible = false;
            }
        }
        else
        {
            PanGrid.Visible = true;
            if (ddlProjectNumber.SelectedValue != string.Empty)
            {
                divDocs.Visible = true;
            }
            else
            {
                divDocs.Visible = false;
            }
            rptInstDocs.DataSource = dt;
            rptInstDocs.DataBind();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = true;
        if (ddlProjectNumber.SelectedValue != string.Empty)
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ddlProjectNumber.SelectedValue);
            if (st.ST != string.Empty)
            {
                lblST.Visible = true;
                lblST.Text = st.ST;
                //lblST.NavigateUrl = pdfURL + "ST/" + st.ST;
                lblST.NavigateUrl = SiteConfiguration.GetDocumnetPath("ST", st.ST);
                //lblST.NavigateUrl = "~/userfiles/ST/" + st.ST;
            }
            else
            {
                lblST.Visible = false;
            }
            if (st.InstInvDoc != string.Empty)
            {
                lblInstInvDoc.Visible = true;
                lblInstInvDoc.Text = st.InstInvDoc;
                //lblInstInvDoc.NavigateUrl = pdfURL + "InstInvDoc/" + st.InstInvDoc;
                lblInstInvDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("InstInvDoc", st.InstInvDoc);
                //lblInstInvDoc.NavigateUrl = "~/userfiles/InstInvDoc/" + st.InstInvDoc;
            }
            else
            {
                lblInstInvDoc.Visible = false;
            }
        }
        BindGrid(0);
    }

   
    protected void ddlInstaller_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListItem item1 = new ListItem();
        item1.Text = "Project Number";
        item1.Value = "";
        ddlProjectNumber.Items.Clear();
        ddlProjectNumber.Items.Add(item1);

        if (ddlInstaller.SelectedValue != string.Empty)
        {
            ddlProjectNumber.DataSource = ClsProjectSale.tblProjects_SelectProjectNumByInst(ddlInstaller.SelectedValue);
            ddlProjectNumber.DataValueField = "ProjectID";
            ddlProjectNumber.DataTextField = "Project";
            ddlProjectNumber.DataMember = "Project";
            ddlProjectNumber.DataBind();
        }
    }
    protected void rptInstDocs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndID = (HiddenField)e.Item.FindControl("hndID");
            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc");

            SttblInstallerExtraDocs st = ClsProjectSale.tblInstallerExtraDocs_SelectByID(hndID.Value);
            //hypDoc.NavigateUrl = "~/userfiles/instotherdocs/" + st.QuoteDoc;
            //hypDoc.NavigateUrl = pdfURL + "instotherdocs/" + st.DocName;
            hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("instotherdocs", st.DocName);
        }
    }

    protected void btnClearAll_Click (object sender, EventArgs e)
    {

        ddlInstaller.SelectedValue = "";
        ddlProjectNumber.SelectedValue = "";

        PanGrid.Visible = false;
    }
}