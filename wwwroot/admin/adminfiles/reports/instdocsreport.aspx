<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="instdocsreport.aspx.cs" Inherits="admin_adminfiles_reports_instdocsreport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="tdExport" class="pull-right" runat="server">
                    <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                        <%-- <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                            CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">Installer Docs Report
                </h2>
            </div>
        </div>
    </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>
          <script>
              var prm = Sys.WebForms.PageRequestManager.getInstance();
              prm.add_pageLoaded(pageLoaded);
              //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
              prm.add_beginRequest(beginrequesthandler);
              // raised after an asynchronous postback is finished and control has been returned to the browser.
              prm.add_endRequest(endrequesthandler);
              function beginrequesthandler(sender, args) {
                  //shows the modal popup - the update progress
                  $('.loading-container').css('display', 'block');
              }
              function endrequesthandler(sender, args) {
                  //hide the modal popup - the update progress
                  $('.loading-container').css('display', 'none');
              }

            </script>
    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="panel-body animate-panel" style="padding-bottom: 0px!important;">
        <div class="row">
        <div class="col-md-12">
            <div class="messesgarea">
                <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>           
             <div class="contacttoparea hpanel">
                    <div class="form-inline  Responsive-search">
                        <div class="padd10all floatleft panel-body">
                <div class="form-group spical">
                    <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                        aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                        <asp:ListItem Value="" Text="Installer"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror" ControlToValidate="ddlInstaller"
                        Display="Dynamic" ValidationGroup="search"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group spical">
                    <asp:DropDownList ID="ddlProjectNumber" runat="server" AppendDataBoundItems="true"
                        aria-controls="DataTables_Table_0" CssClass="myval">
                        <asp:ListItem Value="" Text="Project Number"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                </div>
                <div class="form-group">
                    <asp:LinkButton ID="btnClearAll" runat="server"
                        CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-info"><i class="fa-eraser fa"></i>Clear </asp:LinkButton>
                </div>
                            </div>
                        </div>
                 
            </div>
            </div>
            </div>
        </div>
        
    </asp:Panel>

    <div class="panel-body animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">

                        <div class="panel-body">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="table table-striped table-bordered" id="PanGrid" runat="server">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6 col-sm-6" id="divDocs" runat="server">
                                                    <div class="form-group dateimgarea">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                <b>Signed Paperwork: </b>
                                                            </label>
                                                        </span><span class="dateimg">
                                                            <asp:HyperLink ID="lblST" runat="server" Target="_blank"></asp:HyperLink>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group dateimgarea">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                <b>Invoice Doc: </b>
                                                            </label>
                                                        </span><span class="dateimg">
                                                            <asp:HyperLink ID="lblInstInvDoc" runat="server" Target="_blank"></asp:HyperLink>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6" id="divOtherDocs" runat="server">
                                                    <div class="form-group dateimgarea">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                <b>Other Docs</b>
                                                            </label>
                                                        </span><span class="dateimg">
                                                            <asp:Repeater ID="rptInstDocs" runat="server" OnItemDataBound="rptInstDocs_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <div>
                                                                        <asp:HiddenField ID="hndID" runat="server" Value='<%#Eval("ID") %>' />
                                                                        <%#Eval("Installer") %>
                                                                        <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                            <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/icon_adddocs.png" />
                                                                        </asp:HyperLink>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <SeparatorTemplate>
                                                                    <br />
                                                                </SeparatorTemplate>
                                                            </asp:Repeater>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>



