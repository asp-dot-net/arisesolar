﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="PickListTracker.aspx.cs" Inherits="admin_adminfiles_reports_PickListTracker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>PickList Tracker</h5>
    </div>


    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                }
                function endrequesthandler(sender, args) {
                }
                function pageLoaded() {

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.redreq').click(function () {
                        formValidate();
                    });
                }
            </script>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {
                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }

            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="content animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in tShis view</strong>
                            </div>
                        </div>
                    </div>
                    <div class=" animate-panel padbtmzero padtopzero">
                        <div>
                            <div class="hpanel marbtmzero">
                                <div class="searchfinal">
                                    <div class="widget-body shadownone brdrgray">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="row">
                                                <div>
                                                    <div class="dataTables_filter col-sm-12 Responsive-search printorder">
                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div class="input-group col-sm-1 martop5">
                                                                            <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtProjectNumber"
                                                                                WatermarkText="Project Num" />
                                                                            <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage=""
                                                                        ValidationExpression="^\d+$,"></asp:RegularExpressionValidator>--%>
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                                        </div>
                                                                        <div class="input-group col-sm-1 martop5" style="display:none">
                                                                            <asp:TextBox ID="txtStockItem" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtStockItem"
                                                                                WatermarkText="StockItem" />
                                                                        </div>
                                                                        <div class="input-group col-sm-1 martop5" id="divpromo2" runat="server">
                                                                            <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="">Installer</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                                            <asp:DropDownList ID="ddlStockAllocationStore" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="">Location</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" id="div2" runat="server">
                                                                            <asp:DropDownList ID="ddlPicklist" runat="server" AppendDataBoundItems="true"
                                                                              aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="" >Select Picklist</asp:ListItem>
                                                                                <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" id="div4" runat="server">
                                                                            <asp:DropDownList ID="ddlDeduct" runat="server" AppendDataBoundItems="true"
                                                                              aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="" >Select Deduct</asp:ListItem>
                                                                                <asp:ListItem Value="1" >Yes</asp:ListItem>
                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                         <div class="input-group col-sm-1" id="div5" runat="server">
                                                                            <asp:DropDownList ID="ddlpcktype" runat="server" AppendDataBoundItems="true"
                                                                              aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="" >Picklist Type</asp:ListItem>
                                                                                <asp:ListItem Value="1" >Draft</asp:ListItem>
                                                                                <asp:ListItem Value="2">PickList</asp:ListItem>
                                                                                

                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" id="div3" runat="server">
                                                                            <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                              aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="" >Select Date</asp:ListItem>
                                                                                <asp:ListItem Value="1" Selected="True">Picklist Created Date</asp:ListItem>
                                                                                <asp:ListItem Value="2">Install Booking Date</asp:ListItem>
                                                                                <asp:ListItem Value="3">Deduct Date</asp:ListItem>

                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker1 col-sm-1">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker1 col-sm-1">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" CssClass="" ErrorMessage=""></asp:RequiredFieldValidator>

                                                                        </div>
                                                                        <div class="input-group">
                                                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="detail" CssClass="btn btn-info redreq btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                        </div>
                                                                        <div class="form-group" >
                                                                            <asp:Button ID="lnkdeleteNew" runat="server"   Text="DeletePck"  OnClick="lnkdeleteNew_Click"></asp:Button>
                                                                            </div>
                                                                         <div class="form-group">
                                                                            <asp:Button ID="lnkupdateType" runat="server"  Text="UpdateType" CssClass="btn btn-info redreq"   OnClick="lnkupdateType_Click"></asp:Button>
                                                                            </div>
                                                                         <%--<div class="form-group">
                                                                            <asp:Button ID="lnkDel" runat="server"  Text="DeletePickList" CssClass="btn btn-info redreq"   OnClick="lnkDel_Click"></asp:Button>
                                                                            </div>--%>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <%-- <div class="datashowbox">--%>
                                            <div class="row ">
                                                <div>
                                                    <div class="dataTables_length showdata col-sm-3 printorder">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div id="tdExport" class="pull-right" runat="server">
                                                            <%--  <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                            <asp:LinkButton ID="lbtnExport" CssClass="btn btn-success btn-xs Excel" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                                                CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel</asp:LinkButton>
                                                            <!--<a href="javascript:window.print();" class="printicon">
                       
                        <i class="fa fa-print"></i>
                    </a>-->


                                                            <%--<a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>--%>
                                                            <%--  </ol>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--</div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
<%--                <asp:Panel ID="panel" runat="server" CssClass="hpanel marbtm15">
                    <div class="widget-body shadownone brdrgray">
                        <div class="panel-body padallzero">
                            <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage col-sm-12">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="paddingleftright10" style="font-size: medium;display:none">
                                                    <b>Total Amount:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                                </td>
                                                <td>&nbsp;&nbsp;</td>
                                                <td class="paddingleftright10" style="font-size: medium;display:none">
                                                    <b>Total Panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                                </td>
                                                <td>&nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>--%>
                <asp:Panel ID="panel1" runat="server" CssClass=" padleftzero padrightzero finalgrid">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div id="PanGrid" runat="server">
                            <div class="table-responsive  printArea">

                                <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                    OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <center>

                                              <%--  <label for='<%# ((GridViewRow)Container).FindControl("chkheader").ClientID %>' runat="server" id="lblchk123">
                                                                   <asp:CheckBox ID="chkheader" runat="server" AutoPostBack="true" OnCheckedChanged="chkheader_CheckedChanged" />
                                                        <span class="text">&nbsp;</span>
                                                         </label>--%>


                                                       <%--   <span class="checkbox checkbox-info">     --%>   
                                                   <label for='<%# ((GridViewRow)Container).FindControl("chkdeletewhole").ClientID %>' runat="server" id="lblchk1223">                              
                                                             <asp:CheckBox ID="chkdeletewhole" runat="server"  AutoPostBack="true" OnCheckedChanged="chkdeletewhole_CheckedChanged" />
                                                           
                                                          <span class="text">&nbsp;</span>
                                                         </label>
                                                    <%-- </span>--%>
                                                      </center>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label234" runat="server" Width="30px" class="checkbox checkbox-info">

                                                <label for='<%# ((GridViewRow)Container).FindControl("cbxDelete").ClientID %>' runat="server" id="lblchk452643">
                                                    <asp:CheckBox ID="cbxDelete" runat="server" />
                                                    <span class="text">&nbsp;</span>
                                                </label>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="ID" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70px"  HeaderStyle-HorizontalAlign="Center" SortExpression="ID"
                                            >
                                            <ItemTemplate>
                                                <%#Eval("ID")%>
                                                 <asp:HiddenField ID="hdnPrjId" runat="server" Value='<%# Eval("ProjectID")%>'/>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PickList Type" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70px"  HeaderStyle-HorizontalAlign="Center" SortExpression="pcktype"
                                            >
                                            <ItemTemplate>
                                                <%#Eval("pcktype")%>
                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Projectnumber" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="Projectnumber"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                <%--<%#Eval("Projectnumber")%>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      <asp:TemplateField HeaderText="ProjectStatus" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ProjectSTatus"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                            <ItemTemplate>
                                                <%#Eval("ProjectSTatus")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                         <asp:TemplateField HeaderText="InstallPostCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="InstallPostCode"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                            <ItemTemplate>
                                                <%#Eval("InstallPostCode")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="companylocation"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                            <ItemTemplate>
                                                <%#Eval("companylocation")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Installer Name" ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center"  HeaderStyle-HorizontalAlign="Center" SortExpression="InstallerName">
                                            <ItemTemplate>
                                                <%#Eval("InstallerName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Generated Date" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Center" SortExpression="PickListDateTime"
                                            >
                                            <ItemTemplate>
                                                <%#Eval("PickListDateTime")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ActiveDate"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("InstallBookedDate")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Deduct Date" ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center"  HeaderStyle-HorizontalAlign="Center" SortExpression="dedeucton">
                                            <ItemTemplate>
                                                <%#Eval("dedeucton")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="System Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" SortExpression="SystemDetails">
                                            <ItemTemplate>
                                                <%#Eval("SystemDetails")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reason" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Reason"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("Reason")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreatedBy" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="CreatedBy"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("CreatedBy")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      <%--  <asp:TemplateField HeaderText="Deduct On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="dedeucton"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="110px">
                                            <ItemTemplate>
                                                <%#Eval("dedeucton")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Download" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-CssClass="gridheadertext" HeaderStyle-Width="50px" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                            <asp:LinkButton ID="btnduplicate" runat="server" CommandName="Select" CausesValidation="false" Text="Download"
                                                   data-placement="top" data-original-title="DownloadPickList" data-toggle="tooltip" Visible="false">
                                                     </asp:LinkButton>
                                                 
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       
                                        
                                    </Columns>
                                    <AlternatingRowStyle />

                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left; padding-top: 8px"></asp:Label>
                                        <div class="pagination printorder">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid printorder" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

            </div>
              <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdel" runat="server" OnClick="lnkdel_Click" class="btn btn-danger" CausesValidation="false" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hndpicklistid" runat="server" />

        <!--End Danger Modal Templates-->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
             <asp:PostBackTrigger ControlID="GridView1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

