﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_leadtrackreport : System.Web.UI.Page
{
    decimal totallead = 0;
    decimal total45 = 0;
    decimal totalgoogle = 0;
    decimal total150 = 0;
    decimal totalsalary = 0;
    decimal totalphonecost = 0;
    decimal costtotal = 0;
    decimal totalsold = 0;
    decimal totalcancelled = 0;
    decimal netsold = 0;
    decimal totalpanelcost = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDropDown();

            int year = DateTime.Now.Year;
            ddlYear.SelectedValue = year.ToString();

            Bindweek();
            ddlWeek.SelectedValue = (FirstDayOfWeek(DateTime.Now).ToShortDateString());

            txtStartDate.Text = (FirstDayOfWeek(DateTime.Now).ToShortDateString());
            txtEndDate.Text = Convert.ToDateTime(txtStartDate.Text).AddDays(6).ToString();

            rptSalesRep.DataSource = Reports.tblEmployees_Select_TeamWise(ddlTeam.SelectedValue);
            rptSalesRep.DataBind();
        }
    }

    public void BindDropDown()
    {
        ddlYear.DataSource = Reports.Reports_Select_Year();
        ddlYear.DataValueField = "Year";
        ddlYear.DataTextField = "Year";
        ddlYear.DataMember = "Year";
        ddlYear.DataBind();

        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();
    }
    public DateTime FirstDayOfWeek(DateTime date1)
    {
        DateTime weekdate = date1;
        while (weekdate.DayOfWeek != DayOfWeek.Monday)
        {
            weekdate = weekdate.AddDays(-1);
        }
        return weekdate;
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlYear.SelectedValue != string.Empty)
        {
            Bindweek();

            int year = DateTime.Now.Year;
            if (ddlYear.SelectedValue == Convert.ToString(year))
            {
                Bindweek();
                ddlWeek.SelectedValue = (FirstDayOfWeek(DateTime.Now).ToShortDateString());
                txtStartDate.Text = ddlWeek.SelectedValue;
                txtEndDate.Text = Convert.ToDateTime(txtStartDate.Text).AddDays(6).ToString();
            }
            else
            {
                txtStartDate.Text = string.Empty;
                txtEndDate.Text = string.Empty;
            }
        }
        else
        {
            ddlWeek.SelectedValue = "";
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
        }
    }
    public void Bindweek()
    {
        if (ddlYear.SelectedValue != string.Empty)
        {
            ddlWeek.Items.Clear();
            ListItem lst1 = new ListItem();
            lst1.Value = "";
            lst1.Text = "Week";
            ddlWeek.Items.Add(lst1);

            for (int i = 1; i <= 52; i++)
            {
                DateTime dtstart = GetStartofWeekByWeekNo(i, Convert.ToInt32(ddlYear.SelectedValue));
                DateTime dtenddate = dtstart.AddDays(6);
                ListItem lst = new ListItem();
                lst.Value = dtstart.ToShortDateString();
                lst.Text = i.ToString() + " week - " + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtstart)) + " - " + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtenddate));

                ddlWeek.Items.Add(lst);
            }
        }
    }
    private DateTime GetStartofWeekByDate(DateTime Day)
    {
        int awayFromWeekStart = (Convert.ToInt32(Day.DayOfWeek) - 1);
        return Day.AddDays(-awayFromWeekStart);
    }
    private DateTime GetStartofWeekByWeekNo(int WeekNo, int year)
    {
        int curYear = year;
        DateTime startOfYear = new DateTime(curYear, 1, 1);
        DateTime returnDate;
        returnDate = GetStartofWeekByDate(startOfYear);
        returnDate = returnDate.AddDays(7 * (WeekNo - 1));
        return returnDate;
    }
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlWeek.SelectedValue != string.Empty)
        {
            txtStartDate.Text = ddlWeek.SelectedValue;
            txtEndDate.Text = Convert.ToDateTime(txtStartDate.Text).AddDays(6).ToString();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        rptSalesRep.DataSource = Reports.tblEmployees_Select_TeamWise(ddlTeam.SelectedValue);
        rptSalesRep.DataBind();
    }
    protected void rptSalesRep_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndEmployeeID = (HiddenField)e.Item.FindControl("hndEmployeeID");

            TextBox txtSalary = (TextBox)e.Item.FindControl("txtSalary");
            TextBox txtPhoneCost = (TextBox)e.Item.FindControl("txtPhoneCost");

            Literal lblTotalCost = (Literal)e.Item.FindControl("lblTotalCost");
            Literal lblLead45 = (Literal)e.Item.FindControl("lblLead45");
            Literal lblGoogle150 = (Literal)e.Item.FindControl("lblGoogle150");
            Literal lblNetPanelSold = (Literal)e.Item.FindControl("lblNetPanelSold");
            Literal lblPanelCost = (Literal)e.Item.FindControl("lblPanelCost");

            Repeater rptOthers = (Repeater)e.Item.FindControl("rptOthers");
            Repeater rptOnline = (Repeater)e.Item.FindControl("rptOnline");
            Repeater rptPanelSold = (Repeater)e.Item.FindControl("rptPanelSold");
            Repeater rptCancelledPanel = (Repeater)e.Item.FindControl("rptCancelledPanel");

            DataTable dtOthers = Reports.tblProjects_LeadAssignOther_ReportDateWise(hndEmployeeID.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            rptOthers.DataSource = dtOthers;
            rptOthers.DataBind();
            totallead = totallead + Convert.ToDecimal(dtOthers.Rows[0]["total"].ToString());
            lblLeadsTotal.Text = totallead.ToString();

            DataTable dtOnline = Reports.tblProjects_LeadAssign_ReportDateWise(hndEmployeeID.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            rptOnline.DataSource = dtOnline;
            rptOnline.DataBind();
            totalgoogle = totalgoogle + Convert.ToDecimal(dtOnline.Rows[0]["total"].ToString());
            lblGoogleTotal.Text = totalgoogle.ToString();

            DataTable dtSold = Reports.tblProjects_LeadTrack_PanelSold(hndEmployeeID.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlTeam.SelectedValue);
            rptPanelSold.DataSource = dtSold;
            rptPanelSold.DataBind();
            totalsold = totalsold + Convert.ToDecimal(dtSold.Rows[0]["total"].ToString());
            lblSoldTotal.Text = totalsold.ToString();

            DataTable dtCancelled = Reports.tblProjects_LeadTrack_PanelCancelled(hndEmployeeID.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlTeam.SelectedValue); ;
            rptCancelledPanel.DataSource = dtCancelled;
            rptCancelledPanel.DataBind();
            totalcancelled = totalcancelled + Convert.ToDecimal(dtCancelled.Rows[0]["total"].ToString());
            lblCancelledTotal.Text = totalcancelled.ToString();

            if (lblLead45.Text != string.Empty)
            {
                total45 = total45 + Convert.ToDecimal(lblLead45.Text);
                lbl45Total.Text = total45.ToString();
            }
            if (lblGoogle150.Text != string.Empty)
            {
                total150 = total150 + Convert.ToDecimal(lblGoogle150.Text);
                lbl150Total.Text = total150.ToString();
            }

            if (lblLead45.Text != string.Empty)
            {
                if (txtSalary.Text == string.Empty && txtPhoneCost.Text == string.Empty && lblGoogle150.Text == string.Empty)
                {
                    try
                    {
                        lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text)).ToString();
                    }
                    catch { }
                }
                if (txtSalary.Text == string.Empty && txtPhoneCost.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    try
                    {
                        lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text)).ToString();
                    }
                    catch { }
                }
                if (txtSalary.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text.Trim())).ToString();
                }
                if (txtPhoneCost.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtPhoneCost.Text.Trim())).ToString();
                }
                if (txtSalary.Text != string.Empty && txtPhoneCost.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text.Trim()) + Convert.ToDecimal(txtPhoneCost.Text.Trim())).ToString();
                }
            }

            if (lblTotalCost.Text != string.Empty)
            {
                costtotal = costtotal + Convert.ToDecimal(lblTotalCost.Text);
                lblCostTotal.Text = costtotal.ToString();
            }

            lblNetPanelSold.Text = (Convert.ToDecimal(dtSold.Rows[0]["total"].ToString()) - Convert.ToDecimal(dtCancelled.Rows[0]["total"].ToString())).ToString();
            if (lblNetPanelSold.Text != string.Empty)
            {
                netsold = netsold + Convert.ToDecimal(lblNetPanelSold.Text);
                lblNetPanelTotal.Text = netsold.ToString();
            }

            if (lblTotalCost.Text != string.Empty && lblNetPanelSold.Text != string.Empty)
            {
                if (Convert.ToDecimal(lblTotalCost.Text) > 0 && Convert.ToDecimal(lblNetPanelSold.Text) > 0)
                {
                    try
                    {
                        lblPanelCost.Text = SiteConfiguration.ChangeCurrencyVal((Convert.ToDecimal(lblTotalCost.Text) / Convert.ToDecimal(lblNetPanelSold.Text)).ToString());
                        totalpanelcost = totalpanelcost + Convert.ToDecimal(lblPanelCost.Text);
                        lblPanelCostTotal.Text = Convert.ToString(totalpanelcost);
                    }
                    catch { }
                }
                else
                {
                    lblPanelCost.Text = "0";
                }
            }
            else
            {
                lblPanelCostTotal.Text = "0";
            }
        }
    }
    protected void rptOthers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndOther = (HiddenField)e.Item.FindControl("hndOther");
            Literal lblLead45 = ((Literal)((Repeater)sender).Parent.FindControl("lblLead45"));
            Repeater rptSalesRep = ((Repeater)((Repeater)sender).Parent.FindControl("rptOthers"));

            if (txt45.Text != string.Empty)
            {
                lblLead45.Text = (Convert.ToDecimal(hndOther.Value) * Convert.ToDecimal(txt45.Text.Trim())).ToString();
            }
        }
    }
    protected void rptOnline_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndOnlines = (HiddenField)e.Item.FindControl("hndOnlines");
            Literal lblGoogle150 = ((Literal)((Repeater)sender).Parent.FindControl("lblGoogle150"));

            if (txt150.Text != string.Empty)
            {
                lblGoogle150.Text = (Convert.ToDecimal(hndOnlines.Value) * Convert.ToDecimal(txt150.Text.Trim())).ToString();
            }
        }
    }
    protected void btn45_Click(object sender, EventArgs e)
    {
        rptSalesRep.DataSource = Reports.tblEmployees_Select_TeamWise(ddlTeam.SelectedValue);
        rptSalesRep.DataBind();
    }
    protected void btn150_Click(object sender, EventArgs e)
    {
        rptSalesRep.DataSource = Reports.tblEmployees_Select_TeamWise(ddlTeam.SelectedValue);
        rptSalesRep.DataBind();
    }
    protected void txtSalary_TextChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((TextBox)sender).ClientID);

        RepeaterItem item = rptSalesRep.Items[index];
        Literal lblTotalCost = (Literal)item.FindControl("lblTotalCost");
        Literal lblLead45 = (Literal)item.FindControl("lblLead45");
        Literal lblGoogle150 = (Literal)item.FindControl("lblGoogle150");
        TextBox txtSalary = (TextBox)item.FindControl("txtSalary");
        TextBox txtPhoneCost = (TextBox)item.FindControl("txtPhoneCost");

        if (txtSalary.Text != string.Empty)
        {
            if (txtPhoneCost.Text != string.Empty)
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(txtSalary.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
            }
            else
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(txtSalary.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(txtSalary.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text)).ToString();
                }
            }
        }
        else
        {
            if (txtPhoneCost.Text != string.Empty)
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
            }
            else
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text)).ToString();
                }
            }
        }

        TextBox txtbox = (sender as TextBox);
        RepeaterItem item1 = txtbox.NamingContainer as RepeaterItem;

        TextBox txtSalary1 = (TextBox)item.FindControl("txtSalary");
        Literal lblTotalCost1 = (Literal)item.FindControl("lblTotalCost");
        Literal lblPanelCost = (Literal)item.FindControl("lblPanelCost");
        Literal lblNetPanelSold = (Literal)item.FindControl("lblNetPanelSold");

        decimal totalsalary1 = 0;
        if (txtSalary1.Text != string.Empty)
        {
            foreach (RepeaterItem itms in rptSalesRep.Items)
            {
                TextBox txtSalaryT = (TextBox)itms.FindControl("txtSalary");
                if (txtSalaryT.Text != string.Empty)
                {
                    totalsalary1 += Convert.ToDecimal(txtSalaryT.Text.Trim());
                }
            }
            lblSalaryTotal.Text = totalsalary1.ToString();
        }

        decimal costtotal1 = 0;
        if (lblTotalCost1.Text != string.Empty)
        {
            foreach (RepeaterItem itms in rptSalesRep.Items)
            {
                Literal lblTotalCost1T = (Literal)itms.FindControl("lblTotalCost");
                if (lblTotalCost1T.Text != string.Empty)
                {
                    costtotal1 += Convert.ToDecimal(lblTotalCost1T.Text.Trim());
                }
            }
            lblCostTotal.Text = costtotal1.ToString();
        }

        if (lblTotalCost1.Text != string.Empty && lblNetPanelSold.Text != string.Empty)
        {
            if (Convert.ToDecimal(lblTotalCost1.Text) > 0 && Convert.ToDecimal(lblNetPanelSold.Text) > 0)
            {
                try
                {
                    lblPanelCost.Text = SiteConfiguration.ChangeCurrencyVal((Convert.ToDecimal(lblTotalCost1.Text) / Convert.ToDecimal(lblNetPanelSold.Text)).ToString());
                    totalpanelcost = totalpanelcost + Convert.ToDecimal(lblTotalCost1.Text);
                    lblPanelCostTotal.Text = Convert.ToString(totalpanelcost);
                }
                catch { }
            }
            else
            {
                lblPanelCost.Text = "0";
            }
        }
        else
        {
            lblPanelCostTotal.Text = "0";
        }        
    }
    protected void txtPhoneCost_TextChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((TextBox)sender).ClientID);
        RepeaterItem item = rptSalesRep.Items[index];
        Literal lblTotalCost = (Literal)item.FindControl("lblTotalCost");
        Literal lblLead45 = (Literal)item.FindControl("lblLead45");
        Literal lblGoogle150 = (Literal)item.FindControl("lblGoogle150");
        TextBox txtSalary = (TextBox)item.FindControl("txtSalary");
        TextBox txtPhoneCost = (TextBox)item.FindControl("txtPhoneCost");

        if (txtPhoneCost.Text != string.Empty)
        {
            if (txtSalary.Text != string.Empty)
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(txtSalary.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
            }
            else
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtPhoneCost.Text)).ToString();
                }
            }
        }
        else
        {
            if (txtSalary.Text != string.Empty)
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(txtSalary.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text) + Convert.ToDecimal(txtSalary.Text)).ToString();
                }
            }
            else
            {
                if (lblLead45.Text != string.Empty && lblGoogle150.Text == string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text)).ToString();
                }
                if (lblLead45.Text == string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblGoogle150.Text)).ToString();
                }
                if (lblLead45.Text != string.Empty && lblGoogle150.Text != string.Empty)
                {
                    lblTotalCost.Text = (Convert.ToDecimal(lblLead45.Text) + Convert.ToDecimal(lblGoogle150.Text)).ToString();
                }
            }
        }

        TextBox txtbox = (sender as TextBox);
        RepeaterItem item1 = txtbox.NamingContainer as RepeaterItem;

        TextBox txtPhoneCost1 = (TextBox)item.FindControl("txtPhoneCost");
        Literal lblTotalCost1 = (Literal)item.FindControl("lblTotalCost");
        Literal lblPanelCost = (Literal)item.FindControl("lblPanelCost");
        Literal lblNetPanelSold = (Literal)item.FindControl("lblNetPanelSold");

        decimal totalphonecost1 = 0;
        if (txtPhoneCost1.Text != string.Empty)
        {
            foreach (RepeaterItem itms in rptSalesRep.Items)
            {
                TextBox txtPhoneCostT = (TextBox)itms.FindControl("txtPhoneCost");
                if (txtPhoneCostT.Text != string.Empty)
                {
                    totalphonecost1 += Convert.ToDecimal(txtPhoneCostT.Text.Trim());
                }
            }
            lblPhoneCostTotal.Text = totalphonecost1.ToString();
        }

        decimal costtotal1 = 0;
        if (lblTotalCost1.Text != string.Empty)
        {
            foreach (RepeaterItem itms in rptSalesRep.Items)
            {
                Literal lblTotalCost1T = (Literal)itms.FindControl("lblTotalCost");
                if (lblTotalCost1T.Text != string.Empty)
                {
                    costtotal1 += Convert.ToDecimal(lblTotalCost1T.Text.Trim());
                }
            }
            lblCostTotal.Text = costtotal1.ToString();
        }

        if (lblTotalCost1.Text != string.Empty && lblNetPanelSold.Text != string.Empty)
        {
            if (Convert.ToDecimal(lblTotalCost1.Text) > 0 && Convert.ToDecimal(lblNetPanelSold.Text) > 0)
            {
                try
                {
                    lblPanelCost.Text = SiteConfiguration.ChangeCurrencyVal((Convert.ToDecimal(lblTotalCost1.Text) / Convert.ToDecimal(lblNetPanelSold.Text)).ToString());
                    totalpanelcost = totalpanelcost + Convert.ToDecimal(lblTotalCost1.Text);
                    lblPanelCostTotal.Text = Convert.ToString(totalpanelcost);
                }
                catch { }
            }
            else
            {
                lblPanelCost.Text = "0";
            }
        }
        else
        {
            lblPanelCostTotal.Text = "0";
        }
    }
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])",
                        RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }
}