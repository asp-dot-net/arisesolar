﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="stocktransferreportdetail.aspx.cs" Inherits="admin_adminfiles_reports_stocktransferreportdetail"
    Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Transfer Report
          <div id="tdExport" class="pull-right printorder" runat="server">

              <%--  <asp:LinkButton ID="lbtnExport" CssClass="excelicon" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                        CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>
                    <a href="javascript:window.print();" class="printicon">
                       
                        <i class="fa fa-print"></i>
                    </a>--%>
          </div>
                </h5>

            </div>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {

                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });

                    $(".ddlvalstock").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                }


            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch" CssClass="printorder">
                    <div class="animate-panelmessesgarea padbtmzero printorder">
                        <div class="messesgarea">

                            <%-- <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>--%>
                        </div>
                    </div>
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter Responsive-search printorder">
                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div class="inlineblock">
                                                        <div>
                                                            <div class="input-group col-sm-1">
                                                                <asp:TextBox ID="txtStockItem" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtStockItem"
                                                                    WatermarkText="StockItem" />
                                                            </div>

                                                            <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                                <asp:DropDownList ID="ddlsearchtransferfrom" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="ddlvalstock myval" Width="150px">
                                                                    <asp:ListItem Value="">Location From</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1" id="div1" runat="server">
                                                                <asp:DropDownList ID="ddlsearchtransferto" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Location To</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1" id="div2" runat="server">
                                                                <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                    <asp:ListItem Value="1">Received Date</asp:ListItem>
                                                                    <asp:ListItem Value="2">Transfer Date</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                            </div>
                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>p
                                                                </span>
                                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                            </div>
                                                            <%--      </div>
                                                        </div>
                                                        <div class="inlineblock">
                                                            <div class="col-sm-12">--%>
                                                            <div class="input-group col-sm-1" id="div3" runat="server">
                                                                <asp:DropDownList ID="ddlshowdata" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="1">Show Details</asp:ListItem>
                                                                    <asp:ListItem Value="2">Hide Details</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group">
                                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div align="right">
                                <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                            </div>
                        </div>
                    </div>




                </asp:Panel>

                <div class="contactbottomarea printarea">
                    <div class="row" id="PanAddUpdate" runat="server">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="usageheader stocktransfer">
                                        <h2>Stock Transfer Report</h2>
                                    </div>
                                    <asp:Repeater ID="rptModules" runat="server" OnItemDataBound="rptModules_ItemDataBound">
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="stockusage stockbox" style="width: 500px!important;">

                                                        <table width="100%" border="0" cellspacing="5" cellpadding="0" class="table table-border table-striped">
                                                            <tr>
                                                                <th align="left"><strong>From:</strong> </th>
                                                                <td align="right">
                                                                    <asp:Label ID="lblFrom" runat="server"><%# Eval("LocationFrom") %></asp:Label></td>
                                                                <th align="left">&nbsp;&nbsp;&nbsp;&nbsp;<strong>To:</strong></th>
                                                                <td align="right">
                                                                    <asp:Label ID="lblTo" runat="server"><%# Eval("LocationTo") %></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><strong>Start Date:</strong> </td>
                                                                <td align="right">
                                                                    <asp:Label ID="lblstartdate" runat="server"></asp:Label></td>
                                                                <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<strong>End Date:</strong></td>
                                                                <td align="right">
                                                                    <asp:Label ID="lblenddate" runat="server"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div>
                                            </div>


                                            <table class="stockitem stockitemtable" width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>
                                                    <td colspan="4"></td>
                                                </tr>
                                                <tr>
                                                    <th width="70%" align="left">
                                                        <h4>Stock Item </h4>
                                                    </th>
                                                    <th width="20%" align="left">
                                                        <h4>Location</h4>
                                                    </th>
                                                    <th width="10%" align="left">
                                                        <h4>Transfer Qty</h4>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td width="50%" align="left"><%# Eval("StockItem") %>
                                                        <br />
                                                        <table>
                                                            <tr>
                                                                <td width="5%" align="left"></td>
                                                                <td width="30%" align="left">

                                                                    <%-- <asp:HiddenField runat="server" ID="hdnStockCode" Value='<%# Eval("StockCode") %>' />--%>
                                                                    <asp:HiddenField runat="server" ID="hdnStockTransferID" Value='<%# Eval("StockTransferID") %>' />
                                                                    <asp:Repeater runat="server" ID="rptdet">
                                                                        <ItemTemplate>
                                                                            <%# Eval("TrackingNumber") %>  - 
                                                                                     <%# DataBinder.Eval(Container.DataItem, "TransferDate", "{0:dd MMM yyyy}") %>  -  
                                                                                     <%# Eval("TransferQty") %>
                                                                        </ItemTemplate>
                                                                        <SeparatorTemplate>
                                                                            <br />
                                                                        </SeparatorTemplate>

                                                                    </asp:Repeater>

                                                                </td>



                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="Center"><%# Eval("LocationFrom") %> - <%# Eval("LocationTo") %></td>
                                                    <td width="50%" class="center-text"><%# Eval("TransferQty") %></td>

                                                </tr>

                                            </table>


                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>



                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

