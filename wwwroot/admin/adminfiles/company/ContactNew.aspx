﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContactNew.aspx.cs" Inherits="admin_adminfiles_company_ContactNew" 
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ui-autocomplete-loading {
            background: white url("../../../images/indicator.gif") right center no-repeat;
        }
    </style>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);
            //alert(divname + "--" + trname + "--" + 'img' + divname);
            // alert(div+"--"+img+"--"+tr);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>
    <script type="text/javascript">
        //$(document).ready(function () {
        //            doMyAction();
        //        });

        function doMyAction1() {

            //HighlightControlToValidate();
            <%--$('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });--%>

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        }
        <%--function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }--%>

        function doMyAction() {
            //HighlightControlToValidate();
            <%--$('#<%=ibtnUpdateStatus.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnCancelLead.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnEditDetail.ClientID %>').click(function () {
                formValidate();
                var drpdiv = document.getElementsByClassName("drpValidate");
                var flag = 0;

                for (i = 0; i < drpdiv.length; i++) {
                    if ($(drpdiv[i]).find(".myval").val() == "") {
                        $(drpdiv[i]).addClass("errormassage");
                        flag = flag + 1;
                    }
                }
                if (flag > 0) {
                    return false;
                }

            });--%>


            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            //gridviewScroll();
        }
    </script>


    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Contact</h5>
        <div id="fdfs" class="pull-right" runat="server">
            <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
            </ol>
        </div>
    </div>


    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').removeClass('loading-inactive');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').addClass('loading-inactive');
        }

        function pageLoaded() {
            $('.loading-container').addClass('loading-inactive');
            //  CompanyAddress(); roshni
            // gridviewScroll();
            //alert($(".search-select").attr("class"));
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });


            $("[data-toggle=tooltip]").tooltip();
            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

           <%-- $("#<%=txtformbayUnitNo.ClientID %>").keyup(function () {
                //$('#lblName').text($('#txtName').val());
           
                document.getElementById("<%= txtStreetAddress.ClientID %>").value = $("#<%=txtformbayUnitNo.ClientID %>").val();
            });--%>
        }
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="paneldefault" runat="server" DefaultButton="btnSearch">
                                        <div class="row">
                                            <div class="inlineblock ">
                                                <div class="col-sm-12">

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:DropDownList ID="ddlSalesRep" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control m-b" placeholder="Company"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtCompany"
                                                            WatermarkText="Company" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server" UseContextKey="true" TargetControlID="txtCompany"
                                                            ServicePath="~/Search.asmx" ServiceMethod="GetCompanyList" EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtContFirst" runat="server" CssClass="form-control" placeholder="Contact First"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtContFirst"
                                                            WatermarkText="Contact First" />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtContLast" runat="server" CssClass="form-control" placeholder="Contact Last"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtContLast"
                                                            WatermarkText="Contact Last" />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtMobile" runat="server" ValidationGroup="searchcontact" CssClass="form-control" placeholder="Mobile"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtMobile"
                                                                WatermarkText="Mobile" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtMobile">
                                                            </cc1:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtPhone" runat="server" placeholder="Phone" CssClass="form-control"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtPhone" WatermarkText="Phone" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                            Enabled="True" FilterType="Numbers" TargetControlID="txtPhone">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" CssClass="form-control"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderEmail" runat="server" TargetControlID="txtEmail"
                                                            WatermarkText="Email" />
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:TextBox ID="txtStreet" runat="server" placeholder="Street" CssClass="form-control"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtStreet" WatermarkText="Street" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtStreet" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                        </cc1:AutoCompleteExtender>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" placeholder="City" ></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtCity"
                                                            WatermarkText="City" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtCity" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                        </cc1:AutoCompleteExtender>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtPostCode" runat="server" placeholder="Post Code"
                                                            CssClass="form-control "></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                                                            TargetControlID="txtPostCode" WatermarkText="Post Code" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtPostCode" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtProjectNo" CssClass="form-control" runat="server" placeholder="Project No." />
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtProjectNo"
                                                            WatermarkText="Project No." />
                                                        <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtProjectNo" FilterMode="ValidChars" FilterType="Numbers">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="input-group martop5">
                                                        <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search"
                                                            OnClick="btnSearch_Click" />
                                                    </div>
                                                    
                                                    <div class="input-group martop5">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" CausesValidation="false" CssClass="btn btn-primary"
                                                            OnClick="btnClearAll_Click"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div class="datashowbox martop5">
                                <div class="row">
                                    <div class="dataTables_length showdata col-sm-6">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="tdExport" class="pull-right btnexelicon" runat="server">
                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                            <%-- <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>--%>
                                            <%-- </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                    <div class="">
                        <div id="PanGrid" runat="server" class="leadtracker finalgrid">
                            <div class="table-responsive xscroll">
                                <asp:GridView ID="GridView1" DataKeyNames="ContactID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="20px">
                                            <ItemTemplate>
                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("ContactID") %>','tr<%# Eval("ContactID") %>');">
                                                    <%--  <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />--%>
                                                    <%--  <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                    <img id='imgdiv<%# Eval("ContactID") %>' src="../../../images/icon_plus.png" />
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="Left" SortExpression="Contact" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="40px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFullName" runat="server"><%#Eval("FullName")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Company" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="Left" SortExpression="Company" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomer" runat="server" Width="200px" CssClass="gridmainspan">
                                                  <asp:Label ID="lblCust" runat="server" Visible='<%#Eval("Flag").ToString() == "0" ? true:false %>' style="color:#337ab7;"><%#Eval("Customer")%></asp:Label>
                                                <asp:HyperLink ID="lnkCustomer" runat="server" data-toggle="tooltip" data-placement="top" Visible='<%#Eval("Flag").ToString() == "1" ? true:false %>' 
                                                    data-original-title="Detail" Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/CustomerNew.aspx?m=comp&compid="+Eval("CustomerID") %>'>
                                                    <%#Eval("Customer")%></asp:HyperLink>
                                                <%--<div class="contacticonedit">
                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="EditDetail" CommandArgument='<%#Eval("CustomerID") %>' CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                    </asp:LinkButton>
                                                </div>--%>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="Left" SortExpression="EmpName" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpName" runat="server"><%#Eval("EmpName")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Suburb" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="Left" SortExpression="StreetCity" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStreetCity" runat="server"><%#Eval("StreetCity")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="P/Cd" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="center-text" SortExpression="ContMobile" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStreetPostCode" runat="server"><%#Eval("StreetPostCode")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Lead Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="Left" SortExpression="CustType" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustType" runat="server"><%#Eval("CustType")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Project No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="center-text" SortExpression="ProjectStatus" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProject" runat="server">
                                                    <asp:Label ID="lblPro" runat="server" Visible='<%#Eval("Flag").ToString() == "0" ? true:false %>' style="color:#337ab7;"><%#Eval("ProjectStatus")%></asp:Label>
                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'
                                                    Visible='<%#Eval("Flag").ToString() == "1" ? true:false %>' >
                                                    <%#Eval("ProjectStatus")%></asp:HyperLink>
                                            </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Quote Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="center-text" SortExpression="QuoteDate" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuoteDate" runat="server"><%#Eval("QuoteDate")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="NextFollowUp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="center-text" SortExpression="NextFollowupDate" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNextFollowupDate" runat="server"><%#Eval("NextFollowupDate")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Justify"
                                           SortExpression="Description" ItemStyle-HorizontalAlign="Justify" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDescription" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>'>
                                                    <%#Eval("Description")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                            <ItemTemplate>
                                                <tr id='tr<%# Eval("ContactID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                    <td colspan="98%" class="details">
                                                        <div id='div<%# Eval("ContactID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                            <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                <tr class="GridviewScrollItem">
                                                                    <td width="10%"><b>Contact Email</b>
                                                                    </td>
                                                                    <td width="90%">
                                                                        <asp:Label ID="lblContEmail" runat="server"><%#Eval("ContEmail")%></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />
                                    <RowStyle />
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>




            <script type="text/javascript">

                function gridviewScroll() {
                    $('#<%=GridView1.ClientID%>').gridviewScroll({
                        width: $("#content").width() - 25,
                        height: 6000,
                        freezesize: 0
                    });
                }

            </script>

            <div class="loaderPopUP">
                <script>
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_pageLoaded(pageLoadedpro);
                    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(beginrequesthandler);
                    // raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(endrequesthandler);

                    function beginrequesthandler(sender, args) {
                        //shows the modal popup - the update progress
                        $('.loading-container').css('display', 'block');

                    }
                    function endrequesthandler(sender, args) {
                        //hide the modal popup - the update progress
                    }

                    function pageLoadedpro() {
                        $('.datetimepicker1').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                        $('.loading-container').css('display', 'none');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $("[data-toggle=tooltip]").tooltip();

                        //$('#datetimepicker7').datetimepicker({
                        //    format: 'DD/MM/YYYY',
                        //    useCurrent: false //Important! See issue #1075
                        //}).keypress(function (event) {
                        //    if (event.keyCode != 8) {
                        //        event.preventDefault();
                        //    }
                        //});

                        //var today = new Date();
                        //var dd = String(today.getDate()).padStart(2, '0');
                        //var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        //var yyyy = today.getFullYear();

                        //today = dd + '/' + mm + '/' + yyyy;
                        ////document.write(today);
                        //$('#datetimepicker7').data("DateTimePicker").minDate(today);
                    }


                </script>


                <script type="text/javascript">
                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });
                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });


                </script>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
