using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_mtcecall : System.Web.UI.Page
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PanGrid.Visible = false;


            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindGrid(0);
            BindDropDown();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            //if (Convert.ToBoolean(st_emp.showexcel) == true)
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
        }
    }

    public void BindDropDown()
    {
        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlProjectMtceReasonID.DataSource = ClstblProjectMtceReason.tblProjectMtceReason_SelectASC();
        ddlProjectMtceReasonID.DataValueField = "ProjectMtceReasonID";
        ddlProjectMtceReasonID.DataMember = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataTextField = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataBind();

        ddlemployee.DataSource = ClstblContacts.tblemployee_Mctefilter();
        ddlemployee.DataValueField = "EmployeeID";
        ddlemployee.DataMember = "fullname";
        ddlemployee.DataTextField = "fullname";
        ddlemployee.DataBind();
    }
    protected DataTable GetGridData()
    {
        string view = "1";
        if (chkViewAll.Checked)
        {
            view = "";
        }
        //Response.Write(txtCustomer.Text + "=txtCustomer<br/>" + txtmobile.Text + "=txtmobile<br/>" + txtstreet.Text + "=txtstreet<br/>" + txtCity.Text + "=txtCity<br/>" + txtProjectNumber.Text + "=pNumber<br/>" + ddlInstaller.SelectedValue + "=ddlInstaller<br/>" + txtStartDate.Text + "=txtStartDate<br/>" + txtEndDate.Text + "=txtEndDate<br/>" + ddlSearchState.SelectedValue + "=ddlSearchState<br/>" + txtSearchPostCode.Text + "=txtSearchPostCode<br/>" + ddlDate.SelectedValue + "=ddlDate<br/>" + view + "=view<br/>" + ddlcalldate.SelectedValue + "=ddlcalldate<br/>" + ddlpropDate.SelectedValue + "=ddlpropDate<br/>" + chkWarranty.Checked.ToString() + "=chkWarranty<br/>" + ddlProjectMtceReasonID.SelectedValue + "=ddlProjectMtceReasonID<br/>");
        //Response.End();
        DataTable dt = ClstblProjectMaintenance.tblProjectMaintenance_SelectCasual(txtCustomer.Text,txtmobile.Text,txtstreet.Text,txtCity.Text,"0", txtProjectNumber.Text, 
            ddlInstaller.SelectedValue, ddlemployee.SelectedValue,txtemail.Text,txtStartDate.Text, txtEndDate.Text, ddlSearchState.SelectedValue, 
            txtSearchPostCode.Text, datasearch.SelectedValue, ddlDate.SelectedValue, ddlcalldate.SelectedValue, ddlpropDate.SelectedValue, chkWarranty.Checked.ToString(),
            ddlProjectMtceReasonID.SelectedValue);
        if (dt.Rows.Count == 0)
        {
        }
        return dt;
    }
    //protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    //{     

    //    if (e.CommandName.ToLower() == "invoiceadd")
    //    {
    //        string ProjectID = e.CommandArgument.ToString();
    //        Response.Redirect("~/admin/adminfiles/invoice/invcommon.aspx?id=" + ProjectID + "&type=4");
    //    }
    //    if (e.CommandName.ToLower() == "invoiceupdate")
    //    {
    //        string ProjectID = e.CommandArgument.ToString();
    //        Response.Redirect("~/admin/adminfiles/invoice/updateinvcomm.aspx?id=" + ProjectID + "&type=4");
    //    }

    //    if (e.CommandName.ToLower() == "detail")
    //    {
    //        string MaintenanceID = e.CommandArgument.ToString();
    //        Response.Redirect("~/admin/adminfiles/company/mtcecalldetail.aspx?id=" + MaintenanceID);
    //    }
    //}
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
           // divnopage.Visible = false;

        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                   ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lbtnInvAdd = (LinkButton)e.Row.FindControl("lbtnInvAdd");
            LinkButton lbtnInvUpdate = (LinkButton)e.Row.FindControl("lbtnInvUpdate");
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");

            string ProjectID = hndProjectID.Value;
            DataTable dt = ClstblInvoiceCommon.tblInvoiceCommon_SelectByProjectID("4", ProjectID);
            if (dt.Rows.Count > 0)
            {
                lbtnInvAdd.Visible = false;
                lbtnInvUpdate.Visible = true;
            }
            else
            {
                lbtnInvAdd.Visible = true;
                lbtnInvUpdate.Visible = false;
            }
        }
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    txtProjectNumber.Text = string.Empty;
    //    ddlInstaller.SelectedValue = "";
    //    ddlSearchState.SelectedValue = "";
    //    ddlDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtSearchPostCode.Text = string.Empty;

    //    ddlcalldate.SelectedValue = "";
    //    ddlpropDate.SelectedValue = "";
    //    chkWarranty.Checked = false;
    //    ddlProjectMtceReasonID.SelectedValue = "";

    //    BindGrid(0);
    //}
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string view = "1";
        if (chkViewAll.Checked)
        {
            view = "";
        }
        DataTable dt = ClstblProjectMaintenance.tblProjectMaintenance_SelectCasual(txtCustomer.Text, txtmobile.Text, txtstreet.Text, txtCity.Text, "0", txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlemployee.SelectedValue, txtemail.Text,txtStartDate.Text, txtEndDate.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, ddlDate.SelectedValue, view, ddlcalldate.SelectedValue, ddlpropDate.SelectedValue, chkWarranty.Checked.ToString(), ddlProjectMtceReasonID.SelectedValue);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Mtce" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 1, 2, 20, 21, 29, 30, 31, 28, 3, 4, 5, 24, 23, 6, 7, 8, 9 };
            string[] arrHeader = { "CallDate", "Call Desc", "Project", "System Details", "Contact", "Orig Installer", "Mtr Inst.", "Tech Assign", "Prop Date", "CompleteDate", "WorkDone", "PostCode", "InstallCity", "Actual Cost", "Discount", "Cust Price", "ServiceCost" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        ddlemployee.SelectedValue = "";
        txtemail.Text = "";
        ddlSearchState.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchPostCode.Text = string.Empty;
        txtCustomer.Text = string.Empty;
        txtmobile.Text = string.Empty;
        txtstreet.Text = string.Empty;
        txtCity.Text = string.Empty;

        ddlcalldate.SelectedValue = "";
        ddlpropDate.SelectedValue = "";
        chkWarranty.Checked = false;
        ddlProjectMtceReasonID.SelectedValue = "";

        BindGrid(0);
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);

        if (e.CommandName.ToLower() == "addpayment")
        {           
            hndProInvID.Value = e.CommandArgument.ToString();           
            InvoicePayments1.Visible = true;
            InvoicePayments1.GetInvPayClickByProject(hndProInvID.Value);
        }


        if (e.CommandName.ToLower() == "addfollowupnote")
        {
            hndCustomerID.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();

            ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(hndCustomerID.Value);
            ddlContact.DataMember = "Contact";
            ddlContact.DataTextField = "Contact";
            ddlContact.DataValueField = "ContactID";
            ddlContact.DataBind();
            DataTable dt_c = ClstblContacts.tblContacts_SelectByCustId(hndCustomerID.Value);
            if (dt_c.Rows.Count > 0)
            {
                ddlContact.SelectedValue = dt_c.Rows[0]["ContactID"].ToString();
            }

            DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(hndCustomerID.Value);
            if (dt.Rows.Count > 0)
            {
                divgrdFollowUp.Visible = true;
                grdFollowUp.DataSource = dt;
                grdFollowUp.DataBind();
            }
            else
            {
                divgrdFollowUp.Visible = false;
            }
            BindScript();
        }
        if (e.CommandName.ToLower() == "invoiceadd")
        {
            string ProjectID = e.CommandArgument.ToString();
           string type="4";
            invcommon1.GetInvClickByProject (ProjectID,type);
            invcommon1.Visible = true;

            Session["ProjectID"] = ProjectID;
            Session["type"] = 4;
            //Response.Redirect("~/admin/adminfiles/invoice/invcommon.aspx?id=" + ProjectID + "&type=4");
        }
        if (e.CommandName.ToLower() == "invoiceupdate")
        {
            string ProjectID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/invoice/updateinvcomm.aspx?id=" + ProjectID + "&type=4");
        }

        if (e.CommandName.ToLower() == "detail")
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            string MaintenanceID = commandArgs[0];
            string ProjectID = commandArgs[1];
           
            Response.Redirect("~/admin/adminfiles/company/mtcecalldetail.aspx?id=" + MaintenanceID + "&proid="+ ProjectID);
        }
    }
    protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lbtnInvAdd = (LinkButton)e.Row.FindControl("lbtnInvAdd");
            LinkButton lbtnInvUpdate = (LinkButton)e.Row.FindControl("lbtnInvUpdate");
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");

            string ProjectID = hndProjectID.Value;
            DataTable dt = ClstblInvoiceCommon.tblInvoiceCommon_SelectByProjectID("4", ProjectID);
            if (dt.Rows.Count > 0)
            {
                lbtnInvAdd.Visible = false;
                lbtnInvUpdate.Visible = true;
            }
            else
            {
                lbtnInvAdd.Visible = true;
                lbtnInvUpdate.Visible = false;
            }
        }
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;
                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
       // BindGrid(0);
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }
    protected void chkViewAll_OnCheckedChanged(object sender, EventArgs e)
    {
            BindGrid(0);
    }
    protected void GridView1_RowDataBound2(object sender, GridViewRowEventArgs e)
    {

    }
    protected void GridView1_RowDataBound3(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lbtnInvAdd = (LinkButton)e.Row.FindControl("lbtnInvAdd");
            LinkButton lbtnInvUpdate = (LinkButton)e.Row.FindControl("lbtnInvUpdate");
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");

            string ProjectID = hndProjectID.Value;
            try
            {
                DataTable dt = ClstblInvoiceCommon.tblInvoiceCommon_SelectByProjectID("4", ProjectID);
                if (dt.Rows.Count > 0)
                {
                    lbtnInvAdd.Visible = false;
                    lbtnInvUpdate.Visible = true;
                }
                else
                {
                    lbtnInvAdd.Visible = true;
                    lbtnInvUpdate.Visible = false;
                }
            }
            catch { }
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void ddlManager_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        if (ddlManager.SelectedValue == "1")
        {
            divNextDate.Visible = true;
        }
        else
        {
            divNextDate.Visible = false;
        }
        BindScript();
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }


    protected void ibtnAdd_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        string CustomerID = hndCustomerID.Value;
        string Description = txtDescription.Text;
        //string FollowupDate = txtFollowupDate.Text;
        string NextFollowupDate = txtNextFollowupDate.Text;

        if (ddlManager.SelectedValue == "1")
        {
            NextFollowupDate = txtNextFollowupDate.Text;
        }
        if (ddlManager.SelectedValue == "2")
        {
            NextFollowupDate = DateTime.Now.AddHours(14).ToShortDateString();
        }

        string ContactID = ddlContact.SelectedValue.ToString();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;

        int success = ClstblCustInfo.tblCustInfo_Insert(CustomerID, Description, NextFollowupDate, ContactID, CustInfoEnteredBy, ddlManager.SelectedValue);
        if (Convert.ToString(success) != string.Empty)
        {
            ModalPopupExtender2.Show();
            ddlContact.SelectedValue = "";
            //txtFollowupDate.Text = string.Empty;
            txtNextFollowupDate.Text = string.Empty;
            txtDescription.Text = string.Empty;
            SetAdd1();
        }
        else
        {
            ModalPopupExtender2.Show();
            SetError1();
        }
        BindGrid(0);
    }
}