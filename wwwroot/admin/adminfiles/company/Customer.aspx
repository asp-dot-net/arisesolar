﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Customer.aspx.cs" Inherits="admin_adminfiles_company_Customer"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" MaintainScrollPositionOnPostback="true" Culture="en-GB" UICulture="en-GB"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="chkaddval" value="0" />
    <input type="hidden" id="chkaddval1" value="0" />
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endrequesthandler);
        function endrequesthandler(sender, args) {
            $('input[type=file]').bootstrapFileInput();
        }
    </script>
    <script src="../../assets/js/jquery.min.js"></script>
    <style>
        input[type=checkbox]:checked,
        input[type=radio]:checked,
        input[type=checkbox]:focus,
        input[type=radio]:focus {
            outline: none !important;
        }

            input[type=checkbox]:checked ~ .text:before,
            input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
                display: inline-block;
                content: '\f00c';
                background-color: #f5f8fc;
                -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                border-color: #333333;
            }

        input[type=checkbox]:hover ~ .text :before,
        input[type=radio]:hover ~ .text :before, input[type=radio]:hover ~ label:before {
            border-color: #737373;
        }

        input[type=checkbox]:active ~ .text :before,
        input[type=radio]:active ~ .text :before, input[type=radio]:active ~ label :before {
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
        }


        input[type=checkbox] ~ .text,
        input[type=radio] ~ .text, input[type=radio] ~ label {
            position: relative;
            z-index: 11;
            display: inline-block;
            margin: 0;
            line-height: 20px;
            min-height: 18px;
            min-width: 18px;
            font-weight: normal;
        }

            input[type=checkbox] ~ .text:before,
            input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                font-family: fontAwesome;
                font-weight: bold;
                font-size: 13px;
                color: #333333;
                content: "\a0";
                background-color: #fafafa;
                border: 1px solid #c8c8c8;
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                border-radius: 0;
                display: inline-block;
                text-align: center;
                vertical-align: middle;
                height: 18px;
                line-height: 16px;
                min-width: 18px;
                margin-right: 5px;
                margin-bottom: 2px;
                -webkit-transition: all 0.3s ease;
                -moz-transition: all 0.3s ease;
                -o-transition: all 0.3s ease;
                transition: all 0.3s ease;
            }

            input[type=checkbox] ~ .text:hover:before,
            input[type=radio] ~ .text:hover:before, input[type=radio] ~ label:hover:before {
                border-color: #737373;
            }

            input[type=checkbox] ~ .text:active:before,
            input[type=radio] ~ .text:active:before, input[type=radio] ~ label:active:before {
                -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            }

            input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                border-radius: 100%;
                font-size: 10px;
                font-family: FontAwesome;
                line-height: 17px;
                height: 19px;
                min-width: 19px;
            }

        input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
            content: "\f111";
        }
    </style>
    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (printimage.ClientID) %>').innerHTML;
            var html = '<html><head>' +
                '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                '</head><body style="background:#ffffff;">' +
                PageHTML +
                '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });


            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //gridviewScroll();
        });

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


        function doMyAction() {
            $('#<%=btnAdd.ClientID %>').click(function (e) {
                formValidate();

            });
            $('#<%=btnUpdate.ClientID %>').click(function (e) {
                formValidate();

            });

            $("[data-toggle=tooltip]").tooltip();

            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

        }

    </script>


    <script>
        var focusedElementId = "";
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        function BeginRequestHandler(sender, args) {

            var fe = document.activeElement;
            if (fe != null) {
                focusedElementId = fe.id;
            } else {
                focusedElementId = "";
            }
        }

        prm.add_pageLoaded(pageLoaded);
        prm.add_beginRequest(BeginRequestHandler);

        function pageLoaded() {
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });

            $("[data-toggle=tooltip]").tooltip();

            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

        }


    </script>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
                //  divexpandcollapse("", "");
            }
        }
    </script>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        function pageLoaded() {
            $(".myvalcomp").select2({
                //placeholder: "select",
                allowclear: true
            });



        }
    </script>
    <script>

        function address() {

            //=============Street 
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
            //alert($("#<%=txtformbaystreetname.ClientID %>").val())
            $.ajax({
                type: "POST",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        //alert("Hiiiii")
                        $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                        $("#chkaddval").val("1");
                    }
                    else {
                        $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                        $("#chkaddval").val("0");
                    }
                }
            });
            var mydataval = $("#chkaddval").val();
            if (mydataval == "1") {
                return true;
            }
            else {
                return false;
            }

        }

        function validatestreetAddress(source, args) {
            var mydataval;
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

            $.ajax({
                type: "POST",
                url: "Customer.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data.d == true) {
                        <%--alert($("#<%=txtformbaystreetname.ClientID %>").val());--%>
                    }
                    else {

                        //  alert("ELSE");
                        $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                        $("#chkaddval").val("0");
                        mydataval = $("#chkaddval").val("0");
                        strretnamevelid(mydataval);
                        $("#<%=txtformbaystreetname.ClientID %>").val("");
                    }
                }
            });
            
            function strretnamevelid(mydataval) {
                if (mydataval == "1") {
                    alert('efd');
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }
        
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Customer
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                        <asp:LinkButton ID="lnkclose" runat="server" CssClass="btn btn-maroon" CausesValidation="false" Visible="false"
                            OnClick="lnkclose_Click"><i class="fa fa-backward"></i> Close</asp:LinkButton>
                    </ol>
                </div>
            </div>
            <div class="page-body padtopzero">
                <div class="finaladdupdate">
                    <div id="PanAddUpdate" runat="server" visible="false">
                        <div class="animate-panel">
                            <div class="form-horizontal row">

                                <div class="col-md-6">
                                    <div class="animate-panel padtopzero">
                                        <div class="well with-header  companyddform addform">
                                            <div class="header bordered-blue">
                                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                <h4 id="Haddcomp" runat="server">
                                                    <asp:Label runat="server" ID="lbladdcompany" Text="Add New Customer"></asp:Label>
                                                </h4>
                                            </div>

                                            <div class="form-group spicaldivin" id="divMrMs" runat="server">
                                                <asp:Label ID="Label37" runat="server" class="col-sm-12">Name</asp:Label>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <span class="mrdiv">
                                                                <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                    placeholder="Salutation"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span class="fistname">
                                                                <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                    placeholder="First Name"></asp:TextBox>
                                                                <%-- AutoPostBack="true" OnTextChanged="txtContFirst_TextChanged"--%>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                <%--   <asp:RequiredFieldValidator ID="reqvalCategoryName" runat="server" ControlToValidate="txtContFirst"
                                                                            ErrorMessage="" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtContFirst"
                                                                            ErrorMessage="" CssClass="" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span class="lastname">
                                                                <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                    placeholder="Last Name"></asp:TextBox>
                                                                <%--OnTextChanged="txtContLast_TextChanged" AutoPostBack="true"--%>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="txtContLast" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <asp:Label ID="Label22" runat="server" class="col-sm-12">Customer</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtCompany" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCompany"
                                                        ErrorMessage="" Display="None" CssClass="errormassage" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="This value is required." CssClass=""
                                                                                 ControlToValidate="txtCompany" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="form-group spicaldivin" id="div1" runat="server">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            Mobile
                                                            <span class="mrdiv">
                                                                <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" class="form-control modaltextbox"></asp:TextBox>
                                                                <%--OnTextChanged="txtContMobile_TextChanged" AutoPostBack="true" --%>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile" SetFocusOnError="true" Display="Dynamic"
                                                                    ValidationGroup="company1" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)" ForeColor="Red"
                                                                    ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                            </span>
                                                        </div>
                                                     
                                                        <div class="col-md-4">
                                                            Phone
                                                            <span class="fistname">
                                                                <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                        <%--OnTextChanged="txtCustPhone_TextChanged" AutoPostBack="true"--%>
                                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                        ValidationGroup="company1" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                        ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>--%>
                                                            </span>
                                                        </div>
                                                        
                                                        <div class="col-md-4">
                                                            Alt Phone
                                                            <span class="lastname">
                                                                <asp:TextBox ID="txtCustAltPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCustAltPhone"
                                                            ValidationGroup="company1" Display="Dynamic" ErrorMessage="Please enter valid number"
                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group spicaldivin" id="div2" runat="server">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        
                                                        <div class="col-md-8">
                                                            Email
                                                            <span class="mrdiv">
                                                                <asp:TextBox ID="txtContEmail" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                                <%--OnTextChanged="txtContEmail_TextChanged" AutoPostBack="true"--%>
                                                                <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                                    ValidationGroup="company1" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address" ForeColor="Red"
                                                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.])*\w+\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass="" SetFocusOnError="true"
                                                                    ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>
                                                       
                                                        <div class="col-md-4">
                                                            Type
                                                            <span class="fistname">
                                                                <asp:DropDownList ID="ddlCustTypeID" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                    <%--    <asp:ListItem Value="" Text="Select"></asp:ListItem>--%>
                                                                </asp:DropDownList>
                                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                        ControlToValidate="ddlCustTypeID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group spicaldivin">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group spicaldivin" runat="server" style="margin-bottom: 13px!important; margin-left:2px!important;">
                                                                <label>Solar Type </label>
                                                                <div>
                                                                    <div class="radio radio-info radio-inline" style="margin-bottom: 7px; padding-left: 0px;">
                                                                    <label for="<%=rblResCom1.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                                        <span class="text">Res&nbsp;</span>
                                                                    </label>
                                                                    <label for="<%=rblResCom2.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                                        <span class="text">Com&nbsp;</span>
                                                                    </label>
                                                                   </div>
                                                                </div>
                                                           </div>
                                                        </div>
                                                       
                                                        <div class="col-md-6">
                                                            <div class="form-group spicaldivin" id="div3" runat="server" style="margin-bottom: 13px!important;">
                                                                <label>Area </label>
                                                                <div>
                                                                    <div class="radio radio-info radio-inline" style="margin-bottom: 7px; padding-left: 0px;">
                                                                        <asp:RadioButtonList ID="rblArea" CausesValidation="true" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                            <asp:ListItem Value="1" Selected="True">Metro</asp:ListItem>
                                                                            <asp:ListItem Value="2">Regional</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" runat="server" CssClass=""
                                                                            ControlToValidate="rblArea" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                                <div class="clear"></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group spicaldivin" id="div20" runat="server">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            Source
                                                            <span class="mrdiv">
                                                                <asp:DropDownList ID="ddlCustSourceID" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCustSourceID_SelectedIndexChanged"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                </asp:DropDownList>
                                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                        ControlToValidate="ddlCustSourceID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                        </div>
                                                       
                                                        <div class="col-md-4" id="divSubSource" runat="server">
                                                            Sub Source
                                                            <span class="fistname">
                                                                <asp:DropDownList ID="ddlCustSourceSubID" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="graybgarea col-md-12" runat="server" id="divformbayaddress" visible="false">
                                    <div class="form-group spicaldivin textareabox" style="margin-bottom: 15px;">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="padding-right: 4px;">
                                                    <asp:CheckBox ID="chkformbayid" runat="server" Checked="false" Class="i-checks" OnCheckedChanged="chkformbayid_CheckedChanged"
                                                        AutoPostBack="true" BorderStyle="Solid" />
                                                    <label for="<%=chkformbayid.ClientID %>"><span></span></label>
                                                </td>
                                                <td>Is Frombay Address </td>
                                            </tr>
                                        </table>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="animate-panel padtopzero">
                                        <div class="well with-header companyddform  addform">
                                            <div class="header bordered-blue">
                                                <h4 id="Haddcompadd" runat="server">
                                                    <asp:Label runat="server" ID="lblcompanyaddress" Text="Add Customer Address"></asp:Label></h4>
                                            </div>
                                            <div id="Div6" class="form-group spicaldivin streatfield" visible="false" runat="server">
                                                <span class="name">
                                                    <label class="control-label">Street Address Line<span class="symbol required"></span> </label>
                                                </span>
                                                <span>
                                                    <asp:HiddenField ID="hndaddress" runat="server" />
                                                    <asp:TextBox ID="txtstreetaddressline" runat="server" MaxLength="50" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtstreetaddressline_TextChanged"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="rfvstreetaddressline" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                        ControlToValidate="txtstreetaddressline" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    <%-- <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                ClientValidationFunction="ChkFun"></asp:CustomValidator>--%>
                                                </span>
                                                <asp:Label ID="lblexistame" runat="server" Visible="false"></asp:Label>
                                                <asp:HiddenField ID="hndstreetno" runat="server" />
                                                <asp:HiddenField ID="hndstreetname" runat="server" />
                                                <asp:HiddenField ID="hndstreettype" runat="server" />
                                                <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                <asp:HiddenField ID="hndunittype" runat="server" />
                                                <asp:HiddenField ID="hndunitno" runat="server" />
                                                <div id="validaddressid" style="display: none">
                                                    <img src="../../../images/check.png" alt="check">Address is valid.
                                                </div>
                                                <div id="invalidaddressid" style="display: none">
                                                    <img src="../../../images/x.png" alt="cross">
                                                    Address is invalid.
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group" id="div7" runat="server" visible="false">
                                                <asp:Label ID="Label9" runat="server" class="col-md-12">Street Address</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" Enabled="false" class="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 marginbtm15" visible="false" id="divUnitno" runat="server">
                                                    <asp:Label ID="Label10" runat="server">Unit No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtformbayUnitNo" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-6" id="divunittype" runat="server">
                                                    <asp:Label ID="Label11" runat="server">Unit Type</asp:Label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" 
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                            <%--AutoPostBack="true" OnSelectedIndexChanged="ddlformbayunittype_SelectedIndexChanged"--%>
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="* Required" CssClass="" InitialValue="Unit Type"
                                                            ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="marginbtm15 col-md-4" visible="false" id="divStreetno" runat="server">
                                                    <asp:Label ID="Label12" runat="server">Street No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 spicaldivin col-md-4 streatfield" visible="false" id="divstname" runat="server">
                                                    <asp:Label ID="Label2" CssClass="marbtmzero" runat="server">Street Name</asp:Label>

                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>
                                                       
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetStreetNameList" CompletionListCssClass="autocompletedrop"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                            ErrorMessage="Enter Valid Street" Display="Dynamic" CssClass="requiredfield"
                                                            ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <div id="Divvalidstreetname" style="display: none">
                                                            <%--<img src="../../../images/check.png" alt="check">--%>
                                                            <i class="fa fa-check"></i>Address is valid.
                                                        </div>
                                                        <div id="DivInvalidstreetname" style="display: none">
                                                            <%--<img src="../../../images/x.png" alt="cross">--%>
                                                            <i class="fa fa-close"></i>Address is invalid.
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="marginbtm15 col-md-4" visible="false" id="divsttype" runat="server">
                                                    <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                    <div id="Div8" class="drpValidate" runat="server">
                                                        <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp" >
                                                            <%--AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged"--%>
                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="marginbtm15 col-md-4" id="seq" runat="server">
                                                    <asp:Label ID="lblMobile" runat="server">Street City</asp:Label>
                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="ddlStreetCity" runat="server" MaxLength="50" Enabled="false" OnTextChanged="ddlStreetCity_TextChanged" AutoPostBack="true"
                                                            class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>

                                                        <cc1:AutoCompleteExtender ID="AutoCompletestreet" CompletionListCssClass="autocompletedrop" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetCitiesList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-4" id="Div9" runat="server">
                                                    <asp:Label ID="Label15" runat="server">Street State</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtStreetState" runat="server" MaxLength="50" Enabled="false" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-4" id="Div10" runat="server">
                                                    <asp:Label ID="Label16" runat="server">Street Post Code</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="50" Enabled="false" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtStreetPostCode" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <%-- <asp:RegularExpressionValidator ID="regularexpressionvalidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                                    ValidationGroup="company1" Display="dynamic" ErrorMessage="please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="requiredfieldvalidator6" runat="server" ErrorMessage="this value is required." CssClass="comperror"
                                                    ValidationGroup="company1" ControlToValidate="txtStreetPostCode" Display="dynamic"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group spicaldivin" id="div4" runat="server">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            Country
                                                            <span class="mrdiv">
                                                                <asp:TextBox ID="txtCountry" Enabled="false" runat="server" MaxLength="50" value="Australia"
                                                                    class="form-control modaltextbox"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                       
                                                        <div class="col-md-4">
                                                            ABN
                                                            <span class="fistname">
                                                                <asp:TextBox ID="txtCustWebSiteLink" runat="server" MaxLength="100" Text="Australia" 
                                                                    class="form-control modaltextbox"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                        
                                                        <div class="col-md-4">
                                                            Fax
                                                            <span class="fistname">
                                                                <asp:TextBox ID="txtCustFax" runat="server" MaxLength="20" class="form-control modaltextbox"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="div5" runat="server">
                                                <asp:Label ID="Label8" runat="server" class="col-sm-12">Notes</asp:Label>
                                                <div class="col-sm-12" >
                                                    <asp:TextBox ID="txtCustNotes" runat="server" TextMode="MultiLine" Columns="4" Rows="2" class="form-control modaltextbox" Height="120px"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-8 col-sm-offset-5">
                                        <asp:Button CssClass="btn btn-primary  redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" ValidationGroup="company1" />
                                        <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                            Text="Save" Visible="false" ValidationGroup="company1" />
                                        <asp:Button CssClass="btn btn-default btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                <ProgressTemplate>
                    
                </ProgressTemplate>
            </asp:UpdateProgress>
            <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />--%>

                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div id="divleft" runat="server">
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server"><i class="icon-ok-sign"></i>&nbsp;Transaction Successful! </div>
                                <div class="alert alert-danger" id="PanError" runat="server">
                                    <i class="icon-remove-sign"></i>&nbsp;
                <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server"><i class="icon-remove-sign"></i>&nbsp;Record with this name already exists. </div>
                                <%--  <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view
                            </div>--%>
                                <div class="alert alert-info" id="PAnAddress" runat="server"><i class="icon-info-sign"></i>&nbsp;Record with this Address already exists. </div>
                                <div class="alert alert-info" id="divTeamTime" runat="server"><i class="icon-info-sign"></i>&nbsp;Appointment Time over. </div>
                            </div>

                            <div class="searchfinal">
                                <div class="widget-body shadownone brdrgray">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="dataTables_filter">
                                            <div class="dataTables_filter Responsive-search row">
                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                                    <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="inlineblock">
                                                                    <div class="col-md-12">
                                                                        <div class="input-group col-sm-1 martop5">
                                                                            <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Type</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group col-sm-1 martop5" style="width: 146px;">
                                                                            <asp:DropDownList ID="ddlSearchRec" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Rec/Com</asp:ListItem>
                                                                                <asp:ListItem Value="1">Residential</asp:ListItem>
                                                                                <asp:ListItem Value="2">Commercial</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group col-sm-1 martop5" style="width:100px!important;">
                                                                            <asp:DropDownList ID="ddlSearchArea" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Area</asp:ListItem>
                                                                                <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                                <asp:ListItem Value="2">Regional</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group col-sm-1 autocompletedropdown martop5" style="width: 200px">
                                                                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Customer Name" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                                                WatermarkText="Customer Name" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                                ServiceMethod="GetCompanyList"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        </div>

                                                                        <div class="input-group col-sm-1 martop5" style="width: 200px">
                                                                            <asp:TextBox ID="txtSearchStreet" runat="server" placeholder="Street" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtSearchStreet"
                                                                                WatermarkText="Street" />
                                                                        </div>

                                                                        <div class="input-group col-sm-1 martop5" style="width: 200px">
                                                                            <asp:TextBox ID="txtSerachCity" runat="server" placeholder="City" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtSerachCity"
                                                                                WatermarkText="City" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                            </cc1:AutoCompleteExtender>
                                                                        </div>

                                                                        <div class="input-group col-sm-1 martop5">
                                                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">State</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group col-sm-1 martop5" style="width: 138px">
                                                                            <asp:TextBox ID="txtSearchPostCode" runat="server" placeholder="Post Code" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchPostCode"
                                                                                WatermarkText="Post Code" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                            </cc1:AutoCompleteExtender>
                                                                        </div>

                                                                        <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span></span>
                                                                            <asp:TextBox ID="txtStartDate" placeholder="From" runat="server" class="form-control">

                                                                            </asp:TextBox>
                                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="dynamic"
                                                                                            ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                            <asp:TextBox ID="txtEndDate" placeholder="To" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                Display="Dynamic" ValidationGroup="company1"></asp:CompareValidator>
                                                                        </div>
                                                                        <div class="input-group martop5">
                                                                            <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                        </div>
                                                                        <div class="form-group martop5">
                                                                            <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="datashowbox">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanSearch" runat="server">
                                            <div class="row ">
                                                <div class="dataTables_length showdata col-sm-8">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                    aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%-- <td valign="middle" id="tdAll2" class="paddtop3td" runat="server">&nbsp;View All</td>--%>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-md-4">

                                                    <div class="pull-right martop7 marleft8">
                                                        <div class="checkbox-info ">
                                                            <!-- <a href="#" class="btn btn-darkorange btn-xs Excel"><i class="fa fa-check-square-o"></i> View All</a>-->

                                                            <span valign="top" id="tdAll1" class="paddtop3td alignchkbox btnviewallorange" runat="server" align="right">
                                                               <%-- <label for="<%=chkViewAll.ClientID %>" class="btn btn-darkorange btn-xs">
                                                                    <asp:CheckBox ID="chkViewAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkViewAll_OnCheckedChanged" />
                                                                    <span class="text">&nbsp;View All</span>&nbsp;
                                                           
                                                                </label>--%>
                                                                <asp:LinkButton ID="lbtnExport" runat="server" CausesValidation="false" class="btn btn-success btn-xs Excel" data-original-title="Excel Export" 
                                                                    data-placement="left" data-toggle="tooltip" OnClick="lbtnExport_Click" title="" Visible="false"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                            </span>

                                                        </div>
                                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%><%-- </ol>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

                <div class="content padtopzero marbtm50 finalgrid" id="divgrid" runat="server">
                    <div id="leftgrid" runat="server">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div id="PanGrid" runat="server" visible="false">
                                <div class="table-responsive xscroll  noPagination">
                                    <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        OnRowDataBound="GridView1_RowDataBound" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_OnRowCommand"
                                        OnRowCreated="GridView1_RowCreated" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="20px">
                                                <ItemTemplate>
                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                        <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />
                                                        <%--    <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer Name" SortExpression="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerID") %>' />
                                                    <asp:Label ID="Label2" runat="server" Width="150px"><%#Eval("Customer")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="StreetAddress"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'
                                                        Width="180px"><%#Eval("StreetAddress")%> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Location"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="180px"><%#Eval("Location")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ContPhone"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Width="100px"><%#Eval("ContPhone")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ContMobile"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" Width="100px"><%#Eval("ContMobile")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                <ItemTemplate>

                                                    <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn btn-primary btn-xs"
                                                        Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/CustomerNew.aspx?m=comp&compid="+Eval("CustomerID") %>'> <i class="fa fa-link"></i> Detail</asp:HyperLink>


                                                    <asp:LinkButton Visible="false" runat="server" ID="lnkphoto" CommandName="lnkphoto" CssClass="btn btn-warning btn-xs" CommandArgument='<%#Eval("CustomerID")%>' CausesValidation="false" data-toggle="tooltip" data-placement="top" title="" data-original-title="Image"> <i class="fa fa-image"></i> Image</asp:LinkButton>
                                                    <%--  <asp:HyperLink ID="gvbtndetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"
                                                        Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/customerimage.aspx?id="+Eval("CustomerID") %>'> <i class="fa fa-eye"></i></asp:HyperLink>--%>


                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"> <i class="fa fa-edit"></i> Edit </asp:LinkButton>

                                                    <!--DELETE Modal Templates-->

                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                        CommandName="Delete" CommandArgument='<%#Eval("CustomerID")%>'>
                                                        <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                <ItemTemplate>
                                                    <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                        <td colspan="98%" class="details">
                                                            <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                    <tr class="GridviewScrollItem">
                                                                        <td width="180px"><b>Customer Number</b></td>
                                                                        <td runat="server" id="tdcompnum">
                                                                            <asp:Label ID="Label1" runat="server" Width="130px"> <%#Eval("CompanyNumber")%> </asp:Label></td>
                                                                        <td width="180px" runat="server" id="tdassignto"><b>Assigned To</b></td>
                                                                        <td runat="server" id="tdassignto1">
                                                                            <asp:Label ID="Label7" runat="server" Width="130px"><%#Eval("AssignedTo")%> </asp:Label></td>
                                                                    </tr>
                                                                    <tr runat="server" id="tdtype">
                                                                        <td><b>Type</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label8" runat="server" Width="100px"><%#Eval("CustType")%> </asp:Label></td>
                                                                        <td><b>Res Com</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblres" runat="server" Width="100px"><%#Eval("ResCom")%></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Email</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblEmail" runat="server" Width="100px"><%#Eval("ContEmail")%></asp:Label></td>
                                                                        <td><b>Notes</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblNotes" runat="server" Width="100px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Source</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label9" runat="server" Width="100px"><%#Eval("CustSource")%> </asp:Label></td>
                                                                        <td><b>Sub Source</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblSubSource" runat="server" Width="100px"> <%#Eval("CustSourceSub")%></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle />
                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <div class="hpanel">
                            
                            <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderName" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divName" OkControlID="btnOKName" TargetControlID="btnNULLData1">
                            </cc1:ModalPopupExtender>
                            <div id="divName" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="myModalLabel">Duplicate Name</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                    summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline"><b>The following Location(s) have a<br />
                                                                    similar name. Check for Duplicates.</b> </h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <asp:Button ID="btnDupeName" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeName_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnNotDupeName" runat="server" OnClick="btnNotDupeName_Onclick" CausesValidation="false"
                                                                    CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKName" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptName" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptName_PageIndexChanging" ShowFooter="true"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Contacts">
                                                                    <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                            <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                            <EmptyDataRowStyle Font-Bold="True" />
                                                            <RowStyle CssClass="GridviewScrollItem" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnNULLData2" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderMobile" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divMobileCheck"
                                OkControlID="btnOKMobile" TargetControlID="btnNULLData2">
                            </cc1:ModalPopupExtender>
                            <div id="divMobileCheck" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="H1">Duplicate Mobile</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body ">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                    summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b> </h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">

                                                            <td>
                                                                <asp:Button ID="btnDupeMobile" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeMobile_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnDupeNotMobile" runat="server" OnClick="btnNotDupeMobile_Onclick"
                                                                    CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKMobile" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptMobile" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptMobile_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Contacts">
                                                                    <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                            <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                            <EmptyDataRowStyle Font-Bold="True" />
                                                            <RowStyle CssClass="GridviewScrollItem" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnNULLDataPhone" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderPhone" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divPhoneCheck"
                                OkControlID="btnOKPhone" TargetControlID="btnNULLDataPhone">
                            </cc1:ModalPopupExtender>
                            <div id="divPhoneCheck" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="H2">Duplicate Phone</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                    summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <asp:Button ID="btnDupePhone" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupePhone_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnNotDupePhone" runat="server" OnClick="btnNotDupePhone_Onclick"
                                                                    CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKPhone" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptPhone" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptPhone_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Customer">
                                                                    <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("CustPhone")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                            <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                            <EmptyDataRowStyle Font-Bold="True" />
                                                            <RowStyle CssClass="GridviewScrollItem" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnNULLData3" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderEmail" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divEmailCheck"
                                OkControlID="btnOKEmail" TargetControlID="btnNULLData3">
                            </cc1:ModalPopupExtender>
                            <div id="divEmailCheck" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="H3">Duplicate Email</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <asp:Button ID="btnDupeEmail" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeEmail_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnNotDupeEmail" runat="server" OnClick="btnNotDupeEmail_Onclick"
                                                                    CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKEmail" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptEmail" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Contacts">
                                                                    <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="Button2" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
                                OkControlID="btnOKAddress" TargetControlID="Button2">
                            </cc1:ModalPopupExtender>
                            <div id="divAddressCheck" runat="server">
                                <div class="modal-dialog" style="left: -145px">
                                    <div class="modal-content" style="min-width: 880px; overflow: scroll; height: 600px;">
                                        <div class="modal-header">
                                            <div style="float: right">
                                                <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" cssclass="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                                    Close

                                                </button>

                                            </div>
                                            <h4 class="modal-title" id="H4">Duplicate Address/Email/Mobile</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h4 class="noline"><b>There is a Contact in the database already who has this Address/Email/Mobile.
                                                    <br />
                                                                    This looks like a Duplicate Entry.</b></h4>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="tablescrolldiv">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Customers">
                                                                    <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mobile">
                                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email">
                                                                    <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField runat="server" ID="hdnupdateaddress" />
                            <asp:Button ID="Button1" Style="display: none;" runat="server" />

                            <asp:Button ID="btnNULLData12" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                                CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData12">
                            </cc1:ModalPopupExtender>
                            <div id="div_popup" runat="server" class="modal_popup" style="display: none">
                                <%--style="width: 900px; overflow-x: scroll;display:none;"--%>
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <div align="right" class="popheadclosebtn">

                                                <!-- <a href="javascript:printContent();" class="printicon marright15"> <i class="icon-print printpage fa fa-print"></i> </a> -->
                                                <a href="javascript:printContent();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>
                                                <button id="ibtnCancelStatus" runat="server" onclick="ibtnCancel1_Click" type="button" class="btn btn-primary btn-xs" data-dismiss="modal" causesvalidation="false"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span> </button>
                                            </div>
                                            <h4 class="modal-title" id="H5">View Image</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <div class="panel-body formareapop" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group dateimgarea" id="printimage" runat="server">
                                                                <div class="lightBoxGallery">
                                                                    <asp:Repeater ID="RepeaterImages" runat="server" OnItemCommand="RepeaterImages_ItemCommand">
                                                                        <ItemTemplate>
                                                                            <div class="galleryimage" style="border: 2px solid #000;">
                                                                                <asp:HyperLink ID="hyp1" runat="server" NavigateUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' title="Image from Unsplash" data-gallery="">
                                                                                    <asp:Image ID="imgcustomer" runat="server" BorderStyle="Outset" ImageUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' CssClass="wdth100" />
                                                                                </asp:HyperLink>
                                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CausesValidation="false" OnClientClick="return ComfirmDelete(event,this)"
                                                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" class="btndeleted" CommandArgument='<%# Eval("CustImageID") %>'> <i class="fa fa-times-circle-o"></i> </asp:LinkButton>
                                                                                <div style='clear: both; page-break-before: always;'></div>
                                                                                <%--   <a href="#" class="btndeleted"><i class="fa fa-times-circle-o"></i></a>--%>
                                                                            </div>
                                                                            <br />

                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Delete</div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />
            <asp:HiddenField ID="hdndelete1" runat="server" />

            <!--End Danger Modal Templates-->
            <asp:HiddenField runat="server" ID="hdncountdata" />


            <div class="loaderPopUP">
                <script>
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_pageLoaded(pageLoadedpro);
                    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(beginrequesthandler);
                    // raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(endrequesthandler);

                    function beginrequesthandler(sender, args) {
                        //shows the modal popup - the update progress
                        $('.loading-container').css('display', 'block');

                    }
                    function endrequesthandler(sender, args) {
                        //hide the modal popup - the update progress
                    }

                    function pageLoadedpro() {

                        $('.loading-container').css('display', 'none');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $("[data-toggle=tooltip]").tooltip();
                        $('.datetimepicker1').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                    }
                </script>

            </div>
        </ContentTemplate>

        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lnkAdd" />--%>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <%--<asp:PostBackTrigger ControlID="chkViewAll" />--%>
            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
            <%--<asp:PostBackTrigger ControlID="TabContainer1" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

