<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="lteamtracker.aspx.cs"
    Inherits="admin_adminfiles_company_lteamtracker" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
    <script type="text/javascript">
    
        function doMyAction() {
          
           
            $('#<%=ibtnEditDetail.ClientID %>').click(function () {
            
                formValidate();
                //DropDown Validation

                var drpdiv = document.getElementsByClassName("drpValidate");
                var flag = 0;

                for (i = 0; i < drpdiv.length; i++) {


                    if ($(drpdiv[i]).find(".myval").val() == "") {

                        $(drpdiv[i]).addClass("errormassage");

                        flag = flag + 1;
                    }

                }

                if (flag > 0) {
                    e.preventDefault();
                }
                //End DropDown Validation

            });


            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
         //gridviewScroll();
        }
        function ChkFun(source, args) {
            validateAddress();
            var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

            if (elem == '1') {
                args.IsValid = true;
            }
            else {

                args.IsValid = false;
            }
        }
        function CompanyAddress() {
            $("#<%=txtStreetAddressline.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "address.aspx",
                        dataType: "jsonp",
                        data: {
                            s: 'auto',
                            term: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    //                    log(ui.item ?
                    //					"Selected: " + ui.item.label :
                    //					"Nothing selected, input was " + this.value);
                },
                open: function () {
                    //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    // $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
        }
        function getParsedAddress() {
            // alert("hii");
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'address',
                    term: $("#<%=txtStreetAddressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data != null) {
                        document.getElementById("<%= txtStreetAddress.ClientID %>").value = data.StreetLine;
                        document.getElementById("<%= txtStreetPostCode.ClientID %>").value = data.Postcode;
                        document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                        document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                        document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                        document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                        document.getElementById("<%= ddlStreetCity.ClientID %>").value = data.Suburb;
                        document.getElementById("<%= txtStreetState.ClientID %>").value = data.State;
                        document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                        document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                    }
                    validateAddress();
                }
            });
        }

        function validateAddress() {
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'validate',
                    term: $("#<%=txtStreetAddressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data == true) {
                        document.getElementById("validaddressid").style.display = "block";
                        document.getElementById("invalidaddressid").style.display = "none";

                        document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                    }
                    else {
                        document.getElementById("validaddressid").style.display = "none";
                        document.getElementById("invalidaddressid").style.display = "block";
                        document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                    }
                }
            });
        }


    </script>


    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.

        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function address() {
            //=============Street 
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval").val("1");
                            }
                            else {
                                $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval").val("0");
                            }
                        }
                    });
                    // select2 - container-active

                }




                function validatestreetAddress(source, args) {
                    var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            //alert(data.d);
                            if (data.d == true) {
                                $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval").val("1");
                            }
                            else {
                                $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval").val("0");
                            }
                        }
                    });

                    var mydataval = $("#chkaddval").val();
                    if (mydataval == "1") {
                        args.IsValid = true;
                    }
                    else {
                        args.IsValid = false;
                    }
                }

    </script>

    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Team Lead Tracker</h5>
            </div>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $("[data-toggle=tooltip]").tooltip();
                    // gridviewScroll();
                    //alert($(".search-select").attr("class"));


                    $('#<%=ibtnEditDetail.ClientID %>').click(function (e) {

                        formValidate();
                        //DropDown Validation

                        var drpdiv = document.getElementsByClassName("drpValidate");
                        var flag = 0;

                        for (i = 0; i < drpdiv.length; i++) {


                            if ($(drpdiv[i]).find(".myval").val() == "") {

                                $(drpdiv[i]).addClass("errormassage");

                                flag = flag + 1;
                            }

                        }

                        if (flag > 0) {
                            e.preventDefault();
                        }
                        //End DropDown Validation
                    });
                 
                }
            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>




                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel runat="server" ID="PanelCreateEdit" DefaultButton="btnSearch">
                                        <div class="row">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="inlineblock">
                                                            <div class="col-sm-12">
                                                                <div class="input-group col-sm-1">
                                                                    <asp:TextBox ID="txtCustomerSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtCustomerSearch"
                                                                        WatermarkText="Contact" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtCustomerSearch" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                    </cc1:AutoCompleteExtender>
                                                                </div>

                                                                <div class="input-group col-sm-1">
                                                                    <asp:TextBox ID="txtsuburb" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtsuburb"
                                                                        WatermarkText="Suburb" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtsuburb" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                    </cc1:AutoCompleteExtender>
                                                                </div>

                                                                <div class="input-group col-sm-1">
                                                                    <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchPostCode"
                                                                        WatermarkText="Post Code" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                    </cc1:AutoCompleteExtender>
                                                                </div>

                                                                <div class="input-group col-sm-2" id="divCustomer" runat="server">
                                                                    <asp:DropDownList ID="ddloutdoor" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                        <asp:ListItem Value="">Team Out Door</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-2" id="div1" runat="server">
                                                                    <asp:DropDownList ID="ddlcloser" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">Team Closer</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group" id="div2" runat="server" style="width: 135px;">
                                                                    <asp:DropDownList ID="ddlcustleadstatus" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">Cust Lead Status</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="inlineblock ">
                                                            <div class="col-sm-12">


                                                                <div class="input-group col-sm-2" style="width: 180px" id="div3" runat="server">
                                                                    <asp:DropDownList ID="ddlAppTime" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">App Time</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-2" style="width: 212px" id="div4" runat="server">
                                                                    <asp:DropDownList ID="ddlSelectDate" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                        <asp:ListItem Value="1">CustEntered</asp:ListItem>
                                                                        <asp:ListItem Value="2">App Date</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                </div>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                    <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                </div>
                                                                <div class="input-group">
                                                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div class="datashowbox">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="dataTables_length showdata">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="padtopzero">
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-9 printorder">
                                            <div id="tdExport" class="pull-right btnexelicon" runat="server">

                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </span>
                 
                    <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>
                                                <%--  </ol>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                    <div class="">
                        <div id="PanGrid" runat="server" class="leadtracker finalgrid">
                            <div class="table-responsive xscroll">
                                <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
                                    OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                <asp:HyperLink ID="hypcompdetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%></asp:HyperLink>
                                                <div class="contacticonedit spcialcontacticonedit btneditmain">
                                                    <asp:LinkButton ID="gvbtnUpdate" CommandName="EditDetail" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-info btn-xs"
                                                        CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                                             <i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Team Outdoor Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                            <ItemTemplate>
                                                <%#Eval("D2DEmpName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="App Date" ItemStyle-VerticalAlign="Top" ItemStyle-Width="90px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" SortExpression="D2DAppDate">
                                            <ItemTemplate>
                                                <%#Eval("D2DAppDate","{0:dd MMM yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="App Time" ItemStyle-VerticalAlign="Top" ItemStyle-Width="90px"
                                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="D2DAppTime">
                                            <ItemTemplate>
                                                <%#Eval("Time")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Suburb" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="StreetCity" ItemStyle-Width="110px">
                                            <ItemTemplate>
                                                <%#Eval("StreetCity")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="StreetPostCode" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <%#Eval("StreetPostCode")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Eval("CustNotes")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Assign To Sales Rep" ItemStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnemployeeid" runat="server" Value='<%# Eval("EmployeeID") %>' />
                                                <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" class="myval input-sm"
                                                    AppendDataBoundItems="true">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-group pull-right">
                                <table id="tblassign" class="printorder" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top: 20px;">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnAssidnLead" runat="server" Text="Assign lead" CssClass="btn btn-s-md btn-danger btn-rounded"
                                                OnClick="btnAssidnLead_Click" />
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <asp:HiddenField ID="hndEditCustID" runat="server" />

                </asp:Panel>

            </div>

            <asp:Button ID="btnNULLEdit" Style="display: none;" runat="server" />


            <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLEdit">
            </cc1:ModalPopupExtender>

             <div class="modal_popup" id="divEdit" runat="server" style="display: none;">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="color-line"></div>
                                                                <div class="modal-header">
                                                                     <div style="float: right">
                                                                    <asp:LinkButton ID="btnCancelEdit" CausesValidation="false" OnClick="btnCancelEdit_Click" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                                                    Close
                                                                    </asp:LinkButton>
                                                                         </div>
                                                                    <h4 class="modal-title" id="H3">Update Customer Detail</h4>
                                                                </div>
                                                                <div class="modal-body paddnone" style="overflow-y: scroll; height: 500px;">
                                                                    <div class="panel-body">
                                                                        <div class="formainline formnew">

                                                                            <div class="alert alert-info" id="lbleror" runat="server" visible="false">
                                                                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this address aleardy Exist</strong>
                                                                            </div>

                                                                            <div class="clear"></div>
                                                                            <div class="form-group spical">
                                                                                <span class="col-md-12">
                                                                                    <label>
                                                                                        Name:
                                                                                    </label>
                                                                                </span>
                                                                                <span>
                                                                                    <div class="onelindiv">
                                                                                        <span class="mrdiv col-md-4">
                                                                                            <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                                                placeholder="Salutation"></asp:TextBox>
                                                                                        </span>
                                                                                        <span class="fistname col-md-4">
                                                                                            <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                                                placeholder="First Name"></asp:TextBox>

                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                                                                ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                                        </span>
                                                                                        <span class="lastname col-md-4">
                                                                                            <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                                                placeholder="Last Name"></asp:TextBox>
                                                                                        </span>
                                                                                    </div>
                                                                                </span>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div id="Div6" class="form-group spicaldivin" runat="server" visible="false">
                                                                                <span class="name col-md-12">
                                                                                    <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                                                                </span><span class="col-md-12">
                                                                                    <asp:HiddenField ID="hndaddress" runat="server" />
                                                                                    <asp:TextBox ID="txtStreetAddressline" runat="server" MaxLength="50" CssClass="form-control" onblur="getParsedAddress();"></asp:TextBox>
                                                                                    <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                                                        ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company"
                                                                                        ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass="comperror"
                                                                                        ControlToValidate="txtStreetAddressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>
                                                                                </span>
                                                                                <asp:HiddenField ID="hndstreetno" runat="server" />
                                                                                <asp:HiddenField ID="hndstreetname" runat="server" />
                                                                                <asp:HiddenField ID="hndstreettype" runat="server" />
                                                                                <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                                                <asp:HiddenField ID="hndunittype" runat="server" />
                                                                                <asp:HiddenField ID="hndunitno" runat="server" />
                                                                                <div id="validaddressid" style="display: none">
                                                                                    <img src="../../../images/check.png" alt="check">Address is valid.
                                                                                </div>
                                                                                <div id="invalidaddressid" style="display: none">
                                                                                    <img src="../../../images/x.png" alt="cross">
                                                                                    Address is invalid.
                                                                                </div>

                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                        <ContentTemplate>--%>


                                                                            <div class="form-group spicaldivin">
                                                                                <span class="name col-md-12">
                                                                                    <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                                                                </span><span class="col-md-12">
                                                                                    <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                                                        ControlToValidate="txtStreetAddress" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                                                </span>
                                                                                <div class="clear"></div>
                                                                            </div>



                                                                            <div class="form-group spicaldivin">
                                                                                <span class="col-md-6">
                                                                                    <label class="control-label">Unit No<span class="symbol required"></span> </label>
                                                                                    <asp:TextBox ID="txtformbayUnitNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayUnitNo_TextChanged"></asp:TextBox>
                                                                                </span>
                                                                                <span class="col-md-6">
                                                                                    <label class="control-label">Unit Type<span class="symbol required"></span> </label>

                                                                                    <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnTextChanged="ddlformbayunittype_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                        <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                        ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>

                                                                                </span>
                                                                                <div class="clear"></div>
                                                                                </span>
                                                                            </div>
                                                                            <div class="form-group spicaldivin">
                                                                                <span class="form-group col-md-4">
                                                                                    <label class="control-label" style="width: 150px;">Street No</label>
                                                                                    <asp:TextBox ID="txtformbayStreetNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                                                        ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                                </span>
                                                                                <span class="col-md-4">
                                                                                    <div class="autocompletedropdown">
                                                                                        <label class="control-label" style="width: 150px;">Street Name<span class="symbol required"></span> </label>
                                                                                        <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>

                                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                                            UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                                            ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>

                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass="comperror"
                                                                                            ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                                                        <div id="Divvalidstreetname" style="display: none">
                                                                                            <img src="../../../images/check.png" alt="check">Address is valid.
                                                                                        </div>
                                                                                        <div id="DivInvalidstreetname" style="display: none">
                                                                                            <img src="../../../images/x.png" alt="cross">
                                                                                            Address is invalid.
                                                                                        </div>
                                                                                    </div>
                                                                                </span>
                                                                                <span class="col-md-4">
                                                                                    <div class="marginbtm15" id="divsttype" runat="server">
                                                                                        <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                                                        <div id="Div7" class="drpValidate" runat="server">
                                                                                            <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                                                                aria-controls="DataTables_Table_0" AutoPostBack="true" CssClass="myval" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                                                                <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                                                                ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="editdetail" InitialValue=""> </asp:RequiredFieldValidator>

                                                                                        </div>
                                                                                    </div>
                                                                                </span>
                                                                                <div class="clear"></div>
                                                                            </div>

                                                                            <%--    </ContentTemplate>
                                                                    </asp:UpdatePanel>--%>

                                                                            <div class="form-group spicaldivin">
                                                                                <span class="col-md-4">

                                                                                    <label class="control-label" style="width: 150px;">Street City<span class="symbol required"></span> </label>

                                                                                    <asp:TextBox ID="ddlStreetCity" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                                        OnTextChanged="ddlStreetCity_TextChanged" MaxLength="50"></asp:TextBox>


                                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                                        UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="comperror"
                                                                                        ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                                </span>

                                                                                <span class="col-md-4">

                                                                                    <label class="control-label" style="width: 150px;">Street State<span class="symbol required"></span> </label>

                                                                                    <asp:TextBox ID="txtStreetState" runat="server" MaxLength="10" CssClass="form-control"
                                                                                        Enabled="false"></asp:TextBox>

                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                        ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                                </span>

                                                                                <span class="col-md-4">
                                                                                    <label class="control-label" style="width: 180px;">Street Post Code<span class="symbol required"></span> </label>
                                                                                    <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="10" CssClass="form-control"
                                                                                        Enabled="false"></asp:TextBox>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                                                        ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>

                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                        ValidationGroup="editdetail" ControlToValidate="txtStreetPostCode" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                </span>
                                                                                <div class="clear"></div>
                                                                                </span>
                                                                            </div>
                                                                            <div class="form-group spicaldivin">
                                                                                <span class="col-md-12">
                                                                                    <label>
                                                                                        Email:
                                                                                    </label>
                                                                                    <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                                                    <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                                                        ValidationGroup="editdetail" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>

                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass="comperror"
                                                                                        ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                                                </span>
                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="form-group spical">
                                                                                <span class="col-md-6">
                                                                                    <label>
                                                                                        Mobile:
                                                                                    </label>
                                                                                    <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile"
                                                                                        ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                                                        ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>



                                                                                </span>
                                                                                <span class="col-md-6">
                                                                                    <span>
                                                                                        <label>Phone</label>
                                                                                    </span>
                                                                                    <span>
                                                                                        <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                                                            ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                                                            ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>

                                                                                    </span>
                                                                                </span>
                                                                                <div class="clear"></div>
                                                                            </div>

                                                                            <div class="form-group spical" style="margin-top: 10px; width: 100%;">
                                                                                <span class="col-md-6">
                                                                                    <label style="width: 100%;">Solar Type</label>
                                                                                    <div class="radio radio-info radio-inline">
                                                                                        <label for="<%=rblResCom1.ClientID %>">
                                                                                            <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                                                            <span class="text">Res&nbsp;</span>
                                                                                        </label>

                                                                                        <label for="<%=rblResCom2.ClientID %>">
                                                                                            <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                                                            <span class="text">Com&nbsp;</span>
                                                                                        </label>

                                                                                    </div>
                                                                                </span>
                                                                                <span class="col-md-6">
                                                                                    <label style="width: 100%;">Area</label>

                                                                                    <div class="radio radio-info radio-inline">
                                                                                        <label for="<%=rblArea1.ClientID %>">
                                                                                            <asp:RadioButton runat="server" ID="rblArea1" GroupName="ar" />
                                                                                            <span class="text">Metro&nbsp;</span>
                                                                                        </label>

                                                                                        <label for="<%=rblArea2.ClientID %>">
                                                                                            <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" />
                                                                                            <span class="text">Regional&nbsp;</span>
                                                                                        </label>
                                                                                    </div>
                                                                                </span>
                                                                                <div class="clear"></div>
                                                                            </div>

                                                                            <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                                                                <asp:Button ID="ibtnEditDetail" runat="server" Text="Update" OnClick="ibtnEditDetail_Click"
                                                                                    CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="editdetail" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

            <!--<script type="text/javascript">
                $(document).ready(function () {
                    doMyAction();
                });
                function doMyAction() {
                    $(".myvallteamtracker").select2({
                        placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                }
            </script>-->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />


        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

