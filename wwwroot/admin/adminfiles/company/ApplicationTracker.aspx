<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="ApplicationTracker.aspx.cs" Inherits="admin_adminfiles_company_ApplicationTracker" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ui-autocomplete-loading {
            background: white url("../../../images/indicator.gif") right center no-repeat;
        }

        .bootstrap-datetimepicker-widget {
            z-index: 99999 !important;
        }

        .GridviewScrollItem table.DetailTableWrapping tr td span {
            white-space: normal;
        }
    </style>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);
            //alert(divname + "--" + trname + "--" + 'img' + divname);
            // alert(div+"--"+img+"--"+tr);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>

    <script type="text/javascript">
        //$(document).ready(function () {
        //    doMyAction();
        //});

        function doMyAction() {

            HighlightControlToValidate();

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {

                callMultiCheckbox();
            });
        }
        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#b94a48");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="lbtnExport" />


        </Triggers>
    </asp:UpdatePanel>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Applicaton Tracker</h5>
        <div id="fdfs" class="pull-right" runat="server">
            <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
            </ol>
        </div>
    </div>



    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.

        function pageLoaded() {

        }

    </script>
    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="animate-panel" style="padding-bottom: 0px!important;">
                <div class="messesgarea">
                </div>
            </div>
        </asp:Panel>

        <div class="searchfinal" onkeypress="DefaultEnter(event);">
            <div class="widget-body shadownone brdrgray">
                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div class="dataTables_filter">
                        <asp:Panel ID="paneldefault" runat="server" DefaultButton="btnSearch">
                            <div class="row">

                                <div class="inlineblock ">
                                    <div class="col-sm-12">
                                        <div class="input-group col-sm-1 martop5" id="div4" runat="server">
                                            <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtProjectNumber"
                                                WatermarkText="ProjectNumber" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectNumber" FilterType="Numbers" />
                                        </div>

                                        <div class="input-group col-sm-1 martop5" runat="server">
                                            <asp:DropDownList ID="ddlProjectTypeID" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                <asp:ListItem Value="" Text="Project Type"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="input-group col-sm-1 martop5" id="div6" runat="server">
                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">State</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group spical multiselect martop5" style="width: 142px">
                                            <dl class="dropdown">
                                                <dt>
                                                    <a href="#">
                                                        <span class="hida" id="spanselect">Select</span>
                                                        <p class="multiSel"></p>
                                                    </a>
                                                </dt>
                                                <dd id="ddproject" runat="server">
                                                    <div class="mutliSelect" id="mutliSelect">
                                                        <ul>
                                                            <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                        <%--  <span class="checkbox-info checkbox">--%>
                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                            <span></span>
                                                                        </label>
                                                                        <%-- </span>--%>
                                                                        <label class="chkval">
                                                                            <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                        </label>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="input-group col-sm-1 martop5" id="div2" runat="server">
                                            <asp:DropDownList ID="ddlApplication" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="0">Select Application</asp:ListItem>
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">No</asp:ListItem>


                                            </asp:DropDownList>
                                        </div>
                                        <div class="input-group col-sm-1 martop5" id="div7" runat="server">
                                            <asp:DropDownList ID="ddlSelectDate" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1">Deposit Date</asp:ListItem>
                                                <asp:ListItem Value="2">Active Date</asp:ListItem>
                                                <asp:ListItem Value="3">Opened Date</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>


                                        <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                        </div>
                                        <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                        </div>

                                        <div class="input-group col-sm-1 martop5" style="width: 78px; display: none">

                                            <table class="datedpikartable checkbox-info checkbox">
                                                <tr>
                                                    1
                                                    <td class="btnviewallorange">


                                                        <label for="<%=chkApplication.ClientID %>" class="btn btn-magenta">
                                                            <asp:CheckBox ID="chkApplication" runat="server" Checked="false" />
                                                            <span class="text">Application</span>
                                                        </label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>

                                        </div>

                                        <div class="input-group martop5">
                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                        </div>
                                        <div class="input-group martop5">
                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </asp:Panel>
                    </div>
                </div>

                <div class="datashowbox martop5">
                    <div class="row">
                        <div class="dataTables_length showdata col-sm-6">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="padtopzero">
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div id="tdExport" class="pull-right btnexelicon" runat="server">
                                <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                <%-- </ol>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);
            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').removeClass('loading-inactive');
            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
                $('.loading-container').addClass('loading-inactive');
            }

            function pageLoaded() {
                $('.loading-container').addClass('loading-inactive');
                //  CompanyAddress(); roshni
                // gridviewScroll();
                //alert($(".search-select").attr("class"));
                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });


                $("[data-toggle=tooltip]").tooltip();
                //gridviewScroll();
                $("[id*=GridView1] td").bind("click", function () {
                    var row = $(this).parent();
                    $("[id*=GridView1] tr").each(function () {
                        if ($(this)[0] != row[0]) {
                            $("td", this).removeClass("selected_row");
                        }
                    });
                    $("td", row).each(function () {
                        if (!$(this).hasClass("selected_row")) {
                            $(this).addClass("selected_row");
                        } else {
                            $(this).removeClass("selected_row");
                        }
                    });
                });

                $("[id*=grdFollowUp] td").bind("click", function () {
                    var row = $(this).parent();
                    $("[id*=grdFollowUp] tr").each(function () {
                        if ($(this)[0] != row[0]) {
                            $("td", this).removeClass("selected_row");
                        }
                    });
                    $("td", row).each(function () {
                        if (!$(this).hasClass("selected_row")) {
                            $(this).addClass("selected_row");
                        } else {
                            $(this).removeClass("selected_row");
                        }
                    });
                });
            }
        </script>

        <asp:Panel ID="panel1" runat="server" CssClass="hpanel hpanel panel-body padtopzero">
            <div class="finalgrid">
                <div class="row">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <div id="PanGrid" runat="server">
                        <div class="xscroll table-responsive">
                            <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging"
                                OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                <%--  <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />--%>
                                                <%--  <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="20px" HeaderText="Project No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="ProjectNumber">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hndprojectid" runat="server" Value='<%# Eval("ProjectID") %>' />

                                            <asp:Label ID="lblProject9" runat="server">
                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>

                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project Type" ItemStyle-Width="20px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ProjectType"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProjectType" runat="server"><%#Eval("ProjectType")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project Status" ItemStyle-Width="20px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="projectstatus"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProjectType1" runat="server"><%#Eval("projectstatus")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="First Name" ItemStyle-Width="25px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="Contact"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server"><%#Eval("ContFirst")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name" ItemStyle-Width="25px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="Contact"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName1" runat="server"><%#Eval("ContLast")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Mobile" ItemStyle-Width="20px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ContMobile"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMobile" runat="server"><%#Eval("ContMobile")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Address" ItemStyle-Width="20px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="center-left" SortExpression="CustAddress"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddress" runat="server"><%#Eval("CustAddress")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Suburb" ItemStyle-Width="20px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="center-left" SortExpression="StreetCity"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddress45" runat="server"><%#Eval("StreetCity")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State" ItemStyle-Width="10px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="StreetState"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="StreetState" runat="server"><%#Eval("StreetState")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Postal Code" ItemStyle-Width="10px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="StreetPostCode"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPostCode" runat="server"><%#Eval("StreetPostCode")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Elec.Dist" ItemStyle-Width="22px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ElecDist"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplRefNo4558" runat="server"><%#Eval("ElecDist")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ElecRetailer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="left-text" SortExpression="ElecRet"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblElecRet" runat="server"><%#Eval("ElecRet")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NumberPanels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="NumberPanels">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl1" runat="server" Width="80px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("NumberPanels")%>' CssClass="tooltipwidth"><%#Eval("NumberPanels")%> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NMI" ItemStyle-Width="28px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="MeterNumber1"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <asp:Label ID="lblNMI" runat="server"><%#Eval("NMINumber")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PeakMtr.No" ItemStyle-Width="28px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="NMINumber"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNMI545" runat="server"><%#Eval("MeterNumber1")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Application Refference No" ItemStyle-Width="22px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ElecDistApprovelRef"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplRefNo" runat="server"><%#Eval("ElecDistApprovelRef")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- <asp:TemplateField HeaderText="Installation Notes" ItemStyle-Width="20px" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="InstallerNotes"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProjectType1787" runat="server"><%#Eval("InstallerNotes")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Installation Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="InstallerNotes" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl1545" runat="server" Width="80px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>' CssClass="tooltipwidth"><%#Eval("InstallerNotes")%> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div class="contacticonedit">
                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="EditDetail" CommandArgument='<%#Eval("ProjectID") %>' CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                </asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div class="contacticonedit">
                                                <asp:LinkButton ID="gvBtnAddNote" runat="server" CommandName="AddNote" CommandArgument='<%#Eval("ProjectID") %>' CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="AddNote">
                                                            <i class="fa fa-edit"></i> Note
                                                </asp:LinkButton>
                                               <asp:HyperLink ID="lblmp" Text="MeterPhoto" runat="server" Target="_blank"></asp:HyperLink>
                                                
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                        <ItemTemplate>
                                            <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="left-text">
                                                <td colspan="98%" class="details">
                                                    <div id='div<%# Eval("ProjectID") %>' style="display: none;">
                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover DetailTableWrapping">
                                                            <tr style="text-wrap: normal;">
                                                                <td style="width: 15%;"><b>Project Notes</b>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:Label ID="Label6" runat="server"><%#Eval("ProjectNotes")%></asp:Label>
                                                                </td>
                                                                <td style="width: 15%;">
                                                                    <b>Installer Notes</b>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:Label ID="Labelcustphone" runat="server"><%#Eval("InstallerNotes")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>OldSystemDetails</b></td>
                                                                    <td>
                                                                     <asp:Label ID="Label15" runat="server"><%#Eval("OldSystemDetails")%></asp:Label>
                                                                </td>
                                                                <td><b>SystemCapaCity</b></td>
                                                               <td >    
                                                                <asp:Label ID="Label14" runat="server"><%#Eval("Cap")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td runat="server" id="tdsource"><b>Panel Model</b>
                                                                </td>
                                                                <td runat="server" id="tdsource1">
                                                                    <asp:Label ID="Label111" runat="server"><%#Eval("PanelModel")%></asp:Label>
                                                                </td>
                                                                <td><b>Inverter Model</b>
                                                                </td>

                                                                <td>
                                                                    <asp:Label ID="Label121" runat="server"><%#Eval("InverterModel")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Peak Meter No:</b>
                                                                </td>
                                                                <td>
                                                                    <%--<asp:Label ID="lblFollowUpNote1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("MeterNumber1")%>'></asp:Label>--%>
                                                                    <asp:Label ID="lblFollowUpNote1" runat="server"><%#Eval("MeterNumber1")%></asp:Label>
                                                                </td>
                                                                <td><b>Off-Peak Meters:</b>
                                                                </td>
                                                                <td>
                                                                    <%--<asp:Label ID="Label44" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("MeterNumber2")%>'></asp:Label>--%>

                                                                    <asp:Label ID="Label4" runat="server"><%#Eval("MeterNumber2")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Meter Phase 1-2-3:</b>
                                                                </td>
                                                                <td>
                                                                    <%--<asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("MeterPhase")%>'></asp:Label>--%>
                                                                    <asp:Label ID="Label5" runat="server"><%#Eval("MeterPhase")%></asp:Label>
                                                                </td>
                                                                <td><b>Email:</b>
                                                                </td>
                                                                <td>
                                                                    <%--<asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ContEmail")%>'></asp:Label>--%>
                                                                    <asp:Label ID="Label1" runat="server"><%#Eval("ContEmail")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Lot Number:</b>
                                                                </td>
                                                                <td>
                                                                    <%--<asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("MeterPhase")%>'></asp:Label>--%>
                                                                    <asp:Label ID="Label7" runat="server"><%#Eval("LotNumber")%></asp:Label>
                                                                </td>
                                                                <td><b>Reg Plan No.:</b>
                                                                </td>
                                                                <td>
                                                                    <%--<asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ContEmail")%>'></asp:Label>--%>
                                                                    <asp:Label ID="Label8" runat="server"><%#Eval("RegPlanNo")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>InvQty1</b>
                                                                    </td>
                                                                <td>
                                                                    <asp:Label ID="Label10" runat="server"><%#Eval("inverterqty")%></asp:Label>
                                                                </td>
                                                                <td><b>InvQty2</b>
                                                                   
                                                                        </td>
                                                                <td>
                                                                    <asp:Label ID="Label11" runat="server"><%#Eval("inverterqty2")%></asp:Label>
                                                                </td>
                                                                
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td><b>InvQty3</b></td>
                                                                <td>
                                                                    <asp:Label ID="Label12" runat="server"><%#Eval("inverterqty3")%></asp:Label>
                                                                </td>
                                                                <td><b>InverterOutPut</b></td>
                                                                <td>
                                                                    <asp:Label ID="Label13" runat="server"><%#Eval("InverterOutput")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>System Details</b></td>
                                                                <td>
                                                                    <asp:Label ID="Label16" runat="server"><%#Eval("SystemDetails")%></asp:Label>
                                                                </td>
                                                                <td><b>Inverter Brand</b></td>
                                                                <td>
                                                                    <asp:Label ID="Label17" runat="server"><%#Eval("InverterBrand")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>

                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <AlternatingRowStyle />
                                <RowStyle />
                                <PagerTemplate>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    <div class="pagination">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                    </div>
                                </PagerTemplate>
                                <PagerStyle CssClass="paginationGrid" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                            </asp:GridView>
                        </div>
                        <div class="paginationnew1" runat="server" id="divnopage">
                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>


    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="div1" TargetControlID="btnNULLEdit">
    </cc1:ModalPopupExtender>

    <div class="modal_popup" id="div1" runat="server" style="display: none;">
        <div class="modal-dialog" style="width: 250px;">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div style="float: right">
                        <asp:LinkButton ID="LinkButton5" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                                                    Close
                        </asp:LinkButton>
                    </div>
                    <h4 class="modal-title" id="H3">Update Refund</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline formnew">

                            <div class="clear"></div>

                            <div class="form-group spicaldivin">

                                <div class="formainline">
                                    <div class="form-group" style="width: 187px;">
                                        <label>Pay Method&nbsp;</label>
                                        <div class="drpValidate">
                                            <asp:DropDownList ID="ddlPayMethod" runat="server" Style="z-index: 99;" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlPayMethod"
                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group" id="divElecDistApproved" runat="server">
                                        <span class="name">
                                            <asp:Label ID="Label3" runat="server" class="control-label">
                                           Paid Date</asp:Label></span>
                                        <div class="input-group date datetimepicker1">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtPaidDate" runat="server" class="form-control" Width="150px">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPaidDate"
                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group modaldesc" style="width: 187px;">
                                        <label>Remarks&nbsp;</label>
                                        <div>
                                            <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" Width="187px" Height="60px"></asp:TextBox>
                                            <br />
                                            <asp:RequiredFieldValidator ID="reqvaltxtNotesName" runat="server" ControlToValidate="txtRemarks"
                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                        <asp:Button ID="ibtnAddComment" runat="server" Text="Update"
                                            ValidationGroup="AddNotes" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Button ID="Button1" Style="display: none;" runat="server" />


    <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLEdit">
    </cc1:ModalPopupExtender>

    <div id="divEdit" runat="server" style="display: none; width: 100%" class="modal_popup">
        <div class="modal-dialog" style="width: 60%">
            <div class="modal-content">
                <div class="color-line "></div>
                <div class="modal-header">
                    <div style="float: right">
                        <asp:Button ID="btnCancelEdit" Width="65px" CausesValidation="false" data-dismiss="modal" runat="server" CssClass="btn btn-danger btncancelicon" Text="Close" />


                    </div>
                    <h4 class="modal-title" id="H4">Update Application Details</h4>
                </div>
                <div class="modal-body" style="overflow-y: scroll; height: 500px; padding: 28px;">

                    <div>

                        <div id="divSitedetails" runat="server">
                            <div class="widget flat radius-bordered borderone">
                                <div class="widget-header bordered-bottom bordered-blue">
                                    <span class="widget-caption">Site Details</span>
                                </div>
                                <div class="widget-body">
                                    <asp:Panel runat="server" ID="Pan3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            House Type
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlHouseType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                            AppendDataBoundItems="true">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Roof Type
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlRoofType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                            AppendDataBoundItems="true">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>


                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Roof Angle
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlRoofAngle" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                            AppendDataBoundItems="true">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Elec Dist
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlElecDist" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                            AppendDataBoundItems="true">
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>

                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Elec Retailer
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlElecRetailer" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                            AppendDataBoundItems="true">
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group selpen position_2">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Meter Upgrade
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlmeterupgrade" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval form-control" Width="200">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group selpen position_2">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            NMI Number
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtNMINumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group selpen position_2">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Reg Plan No
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtRegPlanNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group selpen position_2">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Lot Num
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtLotNum" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtLotNum"
                                                            ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." Style="color: red;"
                                                            ValidationGroup="sale" ControlToValidate="txtLotNum" Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                        </div>

                        <div id="divDistAppDetail" runat="server">
                            <div class="widget flat radius-bordered borderone">
                                <div class="widget-header bordered-bottom bordered-blue">
                                    <span class="widget-caption">Distributor Application Detail</span>
                                </div>
                                <div class="widget-body">
                                    <asp:Panel runat="server" ID="Pan4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group position_4">
                                                    <span class="name disblock">
                                                        <asp:Label ID="Label23" runat="server" class="control-label">
                                                Dist Applied</asp:Label>
                                                    </span>
                                                    <div class="input-group date datetimepicker1 col-sm-12">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtElecDistApplied" runat="server" class="form-control">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Employee:
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlEmployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group selpen position_2">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Approval Ref:
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtApprovalRef" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Applied By:
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlElecDistApplyMethod" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group selpen position_2">
                                                    <span class="name disblock">
                                                        <label class="control-label">
                                                            Dist Approved By:
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlElecDistAppBy" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>

                                                <div class="form-group position_4">
                                                    <span class="name disblock">
                                                        <asp:Label ID="Label2" runat="server" class="  control-label">
                                               Dist Approved Date</asp:Label>
                                                    </span>
                                                    <div class="input-group date datetimepicker1 ">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtElecDistAppDate" runat="server" class="form-control">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div id="divMeterDetails" runat="server">
                            <div class="widget flat radius-bordered borderone">
                                <div class="widget-header bordered-bottom bordered-blue">
                                    <span class="widget-caption">Meter Details</span>
                                </div>
                                <div class="widget-body">
                                    <asp:Panel runat="server" ID="Pan6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group selpen">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Peak Meter No:
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtPeakMeterNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group selpen">
                                                    <%--<span class="name paddtop2px floatleft">
                                                            <label class="control-label">
                                                                
                                                            </label>
                                                            </span>--%><span class="checkbox-info checkbox">



                                                                <label for="<%=chkEnoughMeterSpace.ClientID %>">
                                                                    <asp:CheckBox ID="chkEnoughMeterSpace" runat="server" />
                                                                    <span class="text">&nbsp;Enough Meter Space:</span>
                                                                </label>
                                                            </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group selpen">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Off-Peak Meters:
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtOffPeakMeters" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group selpen">
                                                    <%--<span class="name paddtop2px floatleft">
                                                            <label class="control-label">
                                                                
                                                            </label>
                                                        </span>--%><span class="checkbox-info checkbox">

                                                            <label for="<%=chkIsSystemOffPeak.ClientID %>">
                                                                <asp:CheckBox ID="chkIsSystemOffPeak" runat="server" />
                                                                <span class="text">&nbsp;Is System Off-Peak:</span>
                                                            </label>
                                                        </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group selpen">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Meter Phase 1-2-3:
                                                        </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtMeterPhase" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="txtMeterPhase" Style="color: red;"
                                                            ValidationGroup="sale" MinimumValue="1" MaximumValue="3" Type="Integer" Text="Enter 1, 2 or 3."
                                                            Display="Dynamic" />
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                        </div>
                        <div class="form-group marginleft center-text" style="margin-top: 10px;">
                            <asp:HiddenField runat="server" ID="hdnprojid" />
                            <asp:Button ID="ibtnEditDetail" runat="server" Text="Update" OnClick="ibtnEditDetail_Click"
                                CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="editdetail" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <asp:Button ID="btnNULLEdit" Style="display: none;" runat="server" />
    <asp:HiddenField ID="hndEditCustID" runat="server" />

    <script type="text/javascript">

        function gridviewScroll() {
            $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 25,
                height: 6000,
                freezesize: 0
            });
        }

    </script>

    <div class="loaderPopUP">
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
            }

            function pageLoadedpro() {
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $("[data-toggle=tooltip]").tooltip();

            }
        </script>

        <script type="text/javascript">
            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

        </script>

    </div>

    <cc1:ModalPopupExtender ID="ModalPopupExtenderNote" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divModalNote" TargetControlID="Button12" CancelControlID="btncancelpvd">
    </cc1:ModalPopupExtender>
    <div id="divModalNote" runat="server" style="display: none" class="modal_popup">
        <div class="modal-dialog" style="width: 700px;">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="justify-content: center;">
                    <h4 class="modal-title">Add Notes</h4>
                    <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                </div>
                <div class="modal-body paddnone">
                    <div class="card">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Approval Ref:
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtApprovalRef1" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group ">
                                        <span class="name disblock">
                                            <asp:Label ID="Label9" runat="server" class="control-label">
                                                Dist Applied</asp:Label>
                                        </span>
                                        <div class="input-group date datetimepicker1 col-sm-12">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtDistApplied" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                MeterNumber:
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="Txtmeternumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Project Notes
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtprojectNotes" runat="server" MaxLength="300" CssClass="form-control" TextMode="MultiLine" Rows="5" Columns="10"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Application Notes
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtApplicationNotes" runat="server" MaxLength="200" CssClass="form-control" TextMode="MultiLine" Rows="5" Columns="10"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panel3" runat="server" CssClass="xsroll">
                        <div id="PanGridNotes" runat="server" visible="false">
                            <div class="card" style="max-height: 230px; overflow: auto">
                                <div class="card-block">
                                    <div class="table-responsive BlockStructure">
                                        <asp:GridView ID="GridViewNote" DataKeyNames="id" runat="server" CssClass="tooltip-demo table table-bordered nowrap dataTable" OnRowDataBound="GridViewNote_RowDataBound" AutoGenerateColumns="false" OnRowCommand="GridViewNote_RowCommand" OnRowDeleting="GridViewNote_RowDeleting">
                                            <Columns>
                                                <%--<asp:TemplateField ItemStyle-Width="20px">
                                                                            <ItemTemplate>--%>
                                                <%--<a href="JavaScript:divexpandcollapse('div<%# Eval("WholesaleOrderID") %>','tr<%# Eval("WholesaleOrderID") %>');">--%>
                                                <%--<asp:Image ID="imgdiv" runat="server" ImageUrl="../../../images/icon_plus.png" />

                                                                                </a>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>


                                                <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="Date">
                                                    <ItemTemplate>

                                                        <asp:HiddenField ID="hdnprojectID" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                        <asp:Label ID="lblNoteDate" runat="server" Width="50px"><%#Eval("AddedDate","{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AddedBy" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                    SortExpression="AddedBy">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblNoteDate788" runat="server" Width="50px"><%#Eval("addedby")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Note" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Note">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Note" runat="server" Width="80px">
                                                                                <%#Eval("Note")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField HeaderText="Added By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Added By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblusernm" runat="server" Width="80px">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderStyle-Width="10px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-mini" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("id")%>'>
                                                                    <i class="fa fa-trash"></i> Delete
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer" style="justify-content: center;">
                    <asp:Button ID="btnSaveNotes" runat="server" ValidationGroup="WholesaleNotes" type="button" class="btn btn-danger POPupLoader" Text="Save" OnClick="btnSaveNotes_Click"></asp:Button>
                    <asp:Button ID="Button11" runat="server" type="button" class="btn btn-danger" data-dismiss="modal" Text="Cancel"></asp:Button>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="Button12" Style="display: none;" runat="server" />

</asp:Content>
