﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeadTrackerNew.aspx.cs" Inherits="admin_adminfiles_company_LeadTrackerNew"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/NewProject.ascx" TagPrefix="uc1" TagName="NewProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ui-autocomplete-loading {
            background: white url("../../../images/indicator.gif") right center no-repeat;
        }
    </style>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);
            //alert(divname + "--" + trname + "--" + 'img' + divname);
            // alert(div+"--"+img+"--"+tr);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>
    <script type="text/javascript">
        //$(document).ready(function () {
        //            doMyAction();
        //        });

        function doMyAction1() {

            HighlightControlToValidate();
            <%--$('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });--%>

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {

                callMultiCheckbox();
            });
        }
        <%--function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }--%>

        function doMyAction() {
            HighlightControlToValidate();
            <%--$('#<%=ibtnUpdateStatus.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnCancelLead.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnEditDetail.ClientID %>').click(function () {
                formValidate();
                var drpdiv = document.getElementsByClassName("drpValidate");
                var flag = 0;

                for (i = 0; i < drpdiv.length; i++) {
                    if ($(drpdiv[i]).find(".myval").val() == "") {
                        $(drpdiv[i]).addClass("errormassage");
                        flag = flag + 1;
                    }
                }
                if (flag > 0) {
                    return false;
                }

            });--%>


            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            //gridviewScroll();
        }

        function ChkFun(source, args) {
            validateAddress();
            var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

            if (elem == '1') {
                args.IsValid = true;
            }
            else {

                args.IsValid = false;
            }
        }

        function validateAddress() {
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'validate',
                    term: $("#<%=txtStreetAddressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data == true) {
                        document.getElementById("validaddressid").style.display = "block";
                        document.getElementById("invalidaddressid").style.display = "none";

                        document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                    }
                    else {
                        document.getElementById("validaddressid").style.display = "none";
                        document.getElementById("invalidaddressid").style.display = "block";
                        document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                    }
                }
            });
        }

        function validatestreetAddress(source, args) {
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

            $.ajax({
                type: "POST",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //alert(data.d);
                    if (data.d == true) {
                        $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                        $("#chkaddval").val("1");
                    }
                    else {
                        $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                        $("#chkaddval").val("0");
                    }
                }
            });

            var mydataval = $("#chkaddval").val();
            if (mydataval == "1") {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#b94a48");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>


    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Lead Tracker</h5>
        <div id="fdfs" class="pull-right" runat="server">
            <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                <asp:HyperLink NavigateUrl="~/admin/adminfiles/company/Customer.aspx?Mode=AddCustomerFormLeadTracker" runat="server"
                    CssClass="btn btn-default purple" Target="_blank"> <i class="fa fa-plus"></i> Add Customer
                </asp:HyperLink>
            </ol>
        </div>
    </div>


    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').removeClass('loading-inactive');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').addClass('loading-inactive');
        }

        function pageLoaded() {
            $('.loading-container').addClass('loading-inactive');
            //  CompanyAddress(); roshni
            // gridviewScroll();
            //alert($(".search-select").attr("class"));
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });


            $("[data-toggle=tooltip]").tooltip();
            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

           <%-- $("#<%=txtformbayUnitNo.ClientID %>").keyup(function () {
                //$('#lblName').text($('#txtName').val());
           
                document.getElementById("<%= txtStreetAddress.ClientID %>").value = $("#<%=txtformbayUnitNo.ClientID %>").val();
            });--%>
        }
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch" DefaultButton="btnSearch">
                    <div class="animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                        </div>
                    </div>

                    <div class="searchfinal" onkeypress="DefaultEnter(event);">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="paneldefault" runat="server" DefaultButton="btnSearch">
                                        <div class="row">
                                            <div class="inlineblock ">
                                                <div class="col-sm-12">
                                                    <div class="input-group col-sm-1 martop5" id="divCustomer" runat="server">
                                                        <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                            <asp:ListItem Value="">Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                        <asp:DropDownList ID="ddlSearchStatus" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Status</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div2" runat="server">
                                                        <asp:DropDownList ID="ddlSearchSource" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Source</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div3" runat="server">
                                                        <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="tdTeam" runat="server">
                                                        <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Team</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="empcategory" runat="server" visible="false">
                                                        <asp:DropDownList ID="ddlEmpCategory" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                            <asp:ListItem Value="" Text="Select Emp Category"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="tdsalerep" runat="server">
                                                        <asp:DropDownList ID="ddlSalesRepSearch" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b" placeholder="Contact"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtContactSearch"
                                                            WatermarkText="Contact" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                        </cc1:AutoCompleteExtender>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control m-b" placeholder="Mobile No."></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtMobile"
                                                            WatermarkText="Mobile No." />
                                                        <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtMobile" FilterMode="ValidChars" FilterType="Numbers">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtProjectNumber"
                                                            WatermarkText="Project No." />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control m-b" placeholder="Email"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtEmail"
                                                            WatermarkText="Email" />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtSearchStreet" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchStreet"
                                                            WatermarkText="Street" />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtsuburb" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtsuburb"
                                                            WatermarkText="Suburb" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtsuburb" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                        </cc1:AutoCompleteExtender>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div6" runat="server">
                                                        <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchPostCode"
                                                            WatermarkText="Post Code" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                        </cc1:AutoCompleteExtender>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div7" runat="server">
                                                        <asp:DropDownList ID="ddlSelectDate" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">CustEntered</asp:ListItem>
                                                            <asp:ListItem Value="2">NextFollowUp</asp:ListItem>
                                                            <%--<asp:ListItem Value="3">NewDate-Lead</asp:ListItem>
                                                            <asp:ListItem Value="4">NewDate-Project</asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>

                                                    <%--<div class="form-group spical multiselect martop5" style="width: 142px">
                                                        <dl class="dropdown">
                                                            <dt>
                                                                <a href="#">
                                                                    <span class="hida" id="spanselect">Select</span>
                                                                    <p class="multiSel"></p>
                                                                </a>
                                                            </dt>
                                                            <dd id="ddproject" runat="server">
                                                                <div class="mutliSelect" id="mutliSelect">
                                                                    <ul>
                                                                        <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                    <label class="chkval">
                                                                                        <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                    </label>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>--%>

                                                    <%--<div class="input-group col-sm-2 martop5" style="width: 70px;" id="div4" runat="server">
                                                        <asp:DropDownList ID="ddlRejectOrNot" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Reject</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>--%>

                                                    <div class="input-group martop5">
                                                        <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search"
                                                            OnClick="btnSearch_Click" />
                                                    </div>
                                                    <%--<div class="input-group martop5">
                                                        <asp:Button ID="btnSendMail" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="SendMail" />
                                                    </div>--%>
                                                    <div class="input-group martop5">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" CausesValidation="false" CssClass="btn btn-primary"
                                                            OnClick="btnClearAll_Click"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div class="datashowbox martop5">
                                <div class="row">
                                    <div class="dataTables_length showdata col-sm-6">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="tdExport" class="pull-right btnexelicon" runat="server">
                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                             <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                CausesValidation="false" OnClick="lbtnExport_Click" Visible="false"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            <%-- </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel runat="server" ID="PanelAssign">
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="panelHeader" runat="server">
                                        <div class="row">
                                            <div class="inlineblock ">
                                                <div class="col-sm-12">
                                                    <div class=""  runat="server" >
                                                        <div class="input-group col-sm-9">
                                                            <b style="font-size:15px!important;">Total: </b>
                                                            <asp:Label ID="lblTotal" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Unhandled: </b>
                                                            <asp:Label ID="lblUnhandled" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Projects: </b>
                                                            <asp:Label ID="lblProject" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Sales: </b>
                                                            <asp:Label ID="lblSales" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Cancel: </b>
                                                            <asp:Label ID="lblCancel" runat="server" Font-Size="15px" />
                                                        </div>
                                                        
                                                        <div class="input-group col-sm-1" style="text-align:right" id="DivAssignEmp" 
                                                            runat="server" visible="false">
                                                            <asp:DropDownList ID="ddlSalesRep1" runat="server" CssClass="myval"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1" style="text-align:right" id="DivAssignEmp1"
                                                            runat="server" visible="false">
                                                            <asp:Button ID="btnNeawAssignLead" runat="server" Text="Assign Emp" CssClass="btn btn-s-md btn-danger btn-rounded"
                                                                OnClick="btnNeawAssignLead_Click" CausesValidation="false"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                    <div class="">
                        <div id="PanGrid" runat="server" class="leadtracker finalgrid">
                            <div class="table-responsive xscroll">
                                <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging"
                                     OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="20px">
                                            <ItemTemplate>
                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                    <%--  <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />--%>
                                                    <%--  <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                    <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer" HeaderStyle-CssClass="FrozenCell">
                                            <ItemTemplate>
                                                <span>
                                                    <asp:Label ID="Label1" runat="server" CssClass="postionrelative" Style="display: initial" Width="200px">
                                                        <asp:HyperLink ID="hypcompdetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank" CssClass="name2"
                                                            NavigateUrl='<%# "~/admin/adminfiles/company/CustomerNew.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%> </asp:HyperLink>
                                                        <div class="contacticonedit spcialcontacticonedit btneditmain">
                                                            <asp:LinkButton ID="gvbtnUpdate" CommandName="EditDetail" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-info btn-xs"
                                                                CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                                             <i class="fa fa-edit"></i></asp:LinkButton>
                                                        </div>
                                                    </asp:Label></span>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="center-text" SortExpression="ContMobile" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server"><%#Eval("ContMobile")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="StreetAddress"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'
                                                    Width="120px"><%#Eval("StreetAddress")%> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Suburb" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="StreetCity">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Width="80px"><%#Eval("StreetCity")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="StreetState">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Width="50px"><%#Eval("StreetState")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Next Follow-Up" ItemStyle-VerticalAlign="Top" ItemStyle-CssClass="tdspecialclass spicalheghtnew"
                                            HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" SortExpression="NextFollowupDate" ItemStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNextFollowUpDate" runat="server" Width="150px" CssClass="gridmainspan" Style="display: initial">
                                                    <span class="name" style="width: 55%; float: left;"><%#Eval("NextFollowupDate","{0:dd MMM yyyy}")%></span>
                                                    <div class="contacticonedit">
                                                        <asp:LinkButton ID="lbtnFollowUpNote" CommandName="AddFollowupNote" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-azure btn-xs"
                                                            CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add FollowUp">
                                                              <i class="fa fa-calendar"></i></asp:LinkButton>
                                                    </div>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top"
                                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFollowUpNote" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>'
                                                    Width="250px"><%#Eval("Description")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ProjectNumber" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ResponseMsg"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'
                                                >
                                                               <%#Eval("PS") %></asp:HyperLink>

                                               <%-- <asp:LinkButton ID="gvlnkAddProject" CommandName="AddProject" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn-sm"
                                                    CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add Project"
                                                    Visible='<%#Eval("PS").ToString() == "" ? true:false %>'>
                                                             <i class="fa fa-plus"></i></asp:LinkButton>--%>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Source" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="CustSourceSub"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustSourceSub" runat="server" Width="80px"><%#Eval("Source")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Lead Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="CustEntered">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Width="80px"><%# DataBinder.Eval(Container.DataItem, "CustEntered", "{0:dd MMM yyyy}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Quote Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="Left" ItemStyle-HorizontalAlign="Left" SortExpression="QuoteDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuoteDate" runat="server"><%# DataBinder.Eval(Container.DataItem, "QuoteDate", "{0:dd MMM yyyy}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="SalesRep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="Left" ItemStyle-HorizontalAlign="Left" SortExpression="SalesRep" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSalesRep" runat="server"><%# DataBinder.Eval(Container.DataItem, "SalesRep")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" HeaderText="" Visible="false">
                                            <HeaderTemplate>
                                                <center>
                                                    
                                                    <span class="euroradio"> 
                                                        <label for='<%# ((GridViewRow)Container).FindControl("chkreadHeader").ClientID %>' runat="server" id="lblchkreadHeader" >
                                                            <asp:CheckBox ID="chkreadHeader" runat="server" OnCheckedChanged="chkreadHeader_CheckedChanged" AutoPostBack="true" />
                                                            <span class="text">&nbsp;</span>
                                                        </label>
                                                    </span>
                                               </center>
                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <asp:HiddenField ID="hndProjectNo" Value='<%#Eval("ProjectNumber") %>' runat="server" />
                                                <asp:HiddenField ID="hndCustomerID" Value='<%#Eval("CustomerID") %>' runat="server" />

                                                <label for='<%# ((GridViewRow)Container).FindControl("chkread").ClientID %>' runat="server" id="lblchkread">
                                                    <asp:CheckBox ID="chkread" runat="server" Visible='<%#Eval("CustTypeID").ToString()== "3" ? false : true %>' />
                                                    <span class="text">&nbsp;</span>
                                                </label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                            <ItemTemplate>
                                                <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                    <td colspan="98%" class="details">
                                                        <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                            <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                <tr class="GridviewScrollItem">
                                                                    <td width="15%"><b>PCode</b>
                                                                    </td>
                                                                    <td width="35%">
                                                                        <asp:Label ID="Label6" runat="server"><%#Eval("StreetPostCode")%></asp:Label>
                                                                    </td>
                                                                    <td width="15%">
                                                                        <b>Phone</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Labelcustphone" runat="server" Width="80px"><%#Eval("ContPhone")%></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td runat="server" id="tdsource"><b>Source</b>
                                                                    </td>
                                                                    <td runat="server" id="tdsource1">
                                                                        <asp:Label ID="Label111" runat="server"><%#Eval("CustSource")%></asp:Label>
                                                                    </td>
                                                                    <td width="15%"><b>Response Msg</b>
                                                                    </td>

                                                                    <td width="35%">
                                                                        <asp:Label ID="Label121" runat="server"><%#Eval("ResponseMsg")%></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Email</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblFollowUpNote1" runat="server" data-toggle="tooltip" data-placement="top"
                                                                            data-original-title='<%#Eval("ContEmail")%>'><%#Eval("ContEmail")%></asp:Label>
                                                                    </td>
                                                                    <td><b>Notes</b>
                                                                    </td>

                                                                    <td>
                                                                        <asp:Label ID="Label44" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />
                                    <RowStyle />
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>




            <script type="text/javascript">

                function gridviewScroll() {
                    $('#<%=GridView1.ClientID%>').gridviewScroll({
                        width: $("#content").width() - 25,
                        height: 6000,
                        freezesize: 0
                    });
                }

            </script>

            <div class="loaderPopUP">
                <script>
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_pageLoaded(pageLoadedpro);
                    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(beginrequesthandler);
                    // raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(endrequesthandler);

                    function beginrequesthandler(sender, args) {
                        //shows the modal popup - the update progress
                        $('.loading-container').css('display', 'block');

                    }
                    function endrequesthandler(sender, args) {
                        //hide the modal popup - the update progress
                    }

                    function pageLoadedpro() {
                        $('.datetimepicker1').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                        $('.loading-container').css('display', 'none');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $("[data-toggle=tooltip]").tooltip();

                        $('#datetimepicker7').datetimepicker({
                            format: 'DD/MM/YYYY',
                            useCurrent: false //Important! See issue #1075
                        }).keypress(function (event) {
                            if (event.keyCode != 8) {
                                event.preventDefault();
                            }
                        });

                        var today = new Date();
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();

                        today = dd + '/' + mm + '/' + yyyy;
                        //document.write(today);
                        $('#datetimepicker7').data("DateTimePicker").minDate(today);
                    }


                </script>


                <script type="text/javascript">
                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });
                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });


                </script>
            </div>

            <!-- Edit Customer popup -->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLEdit">
            </cc1:ModalPopupExtender>

            <div class="modal_popup" id="divEdit" runat="server" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="btnCancelEdit" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                                                    Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H3" style="padding-top: 4px"><b>Update Customer Detail</b></h4>
                        </div>
                        <div class="modal-body paddnone" style="overflow-y: scroll; height: 500px;">
                            <div class="panel-body">
                                <div class="formainline formnew">

                                    <div class="alert alert-info" id="lbleror" runat="server" visible="false">
                                        <i class="icon-info-sign"></i><strong>&nbsp;Record with this address aleardy Exist</strong>
                                    </div>
                                    <div class="alert alert-info" id="lbleror1" runat="server" visible="false">
                                        <i class="icon-info-sign"></i><strong>&nbsp;Record with this Email or Mobile aleardy Exist</strong>
                                    </div>

                                    <div class="clear"></div>
                                    <div class="form-group spical">
                                        <span class="col-md-12">
                                            <label>
                                                Name:
                                            </label>
                                        </span>
                                        <span>
                                            <div class="onelindiv">
                                                <span class="mrdiv col-md-4">
                                                    <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                        placeholder="Salutation"></asp:TextBox>
                                                </span>
                                                <span class="fistname col-md-4">
                                                    <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                        placeholder="First Name"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                        ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                </span>
                                                <span class="lastname col-md-4">
                                                    <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                        placeholder="Last Name"></asp:TextBox>
                                                </span>
                                            </div>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="Div5" class="form-group spicaldivin" runat="server" visible="false">
                                        <span class="name col-md-12">
                                            <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                        </span><span class="col-md-12">
                                            <asp:HiddenField ID="hndaddress" runat="server" />
                                            <asp:TextBox ID="txtStreetAddressline" runat="server" MaxLength="50" CssClass="form-control" onblur="getParsedAddress();"></asp:TextBox>
                                            <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company"
                                                ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="comperror"
                                                ControlToValidate="txtStreetAddressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>
                                        </span>
                                        <asp:HiddenField ID="hndstreetno" runat="server" />
                                        <asp:HiddenField ID="hndstreetname" runat="server" />
                                        <asp:HiddenField ID="hndstreettype" runat="server" />
                                        <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                        <asp:HiddenField ID="hndunittype" runat="server" />
                                        <asp:HiddenField ID="hndunitno" runat="server" />
                                        <div id="validaddressid" style="display: none">
                                            <img src="../../../images/check.png" alt="check">Address is valid.
                                        </div>
                                        <div id="invalidaddressid" style="display: none">
                                            <img src="../../../images/x.png" alt="cross">
                                            Address is invalid.
                                        </div>

                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group spicaldivin">
                                        <span class="name col-md-12">
                                            <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                        </span><span class="col-md-12">
                                            <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                ControlToValidate="txtStreetAddress" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>
                                        <div class="clear"></div>
                                    </div>



                                    <div class="form-group spicaldivin">
                                        <span class="col-md-6">
                                            <label class="control-label">Unit No<span class="symbol required"></span> </label>
                                            <asp:TextBox ID="txtformbayUnitNo" runat="server" CssClass="form-control" name="address"></asp:TextBox>
                                        </span>
                                        <span class="col-md-6">
                                            <label class="control-label">Unit Type<span class="symbol required"></span> </label>

                                            <asp:DropDownList ID="ddlformbayunittype" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="">Unit Type</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>

                                        </span>
                                        <div class="clear"></div>

                                    </div>
                                    <div class="form-group spicaldivin">
                                        <span class="form-group col-md-4">
                                            <label class="control-label" style="width: 150px;">Street No</label>
                                            <asp:TextBox ID="txtformbayStreetNo" runat="server" CssClass="form-control" name="address"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>
                                        <span class="col-md-4">
                                            <div class="autocompletedropdown">
                                                <label class="control-label" style="width: 150px;">Street Name<span class="symbol required"></span> </label>
                                                <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control"></asp:TextBox>

                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                    ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                    ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass="comperror"
                                                    ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                <div id="Divvalidstreetname" style="display: none">
                                                    <img src="../../../images/check.png" alt="check">Address is valid.
                                                </div>
                                                <div id="DivInvalidstreetname" style="display: none">
                                                    <img src="../../../images/x.png" alt="cross">
                                                    Address is invalid.
                                                </div>
                                            </div>
                                        </span>
                                        <span class="col-md-4">
                                            <div class="marginbtm15" id="divsttype" runat="server">
                                                <asp:Label ID="Label18" runat="server">Street Type</asp:Label>
                                                <div id="Div8" class="drpValidate" runat="server">
                                                    <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Street Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="editdetail" InitialValue=""> </asp:RequiredFieldValidator>

                                                </div>
                                            </div>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group spicaldivin">
                                        <span class="col-md-4">

                                            <label class="control-label" style="width: 150px;">Street City<span class="symbol required"></span> </label>

                                            <%--OnTextChanged="ddlStreetCity_TextChanged" AutoPostBack="true"--%>
                                            <asp:TextBox ID="ddlStreetCity" runat="server" CssClass="form-control"
                                                MaxLength="50" onchange="BindState(this.value)"></asp:TextBox>


                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass="comperror"
                                                ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>

                                        <span class="col-md-4">

                                            <label class="control-label" style="width: 150px;">Street State<span class="symbol required"></span> </label>

                                            <asp:TextBox ID="txtStreetState" runat="server" MaxLength="10" CssClass="form-control"
                                                Enabled="false"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>

                                        <span class="col-md-4">
                                            <label class="control-label" style="width: 180px;">Street Post Code<span class="symbol required"></span> </label>
                                            <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="10" CssClass="form-control"
                                                Enabled="false"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                ValidationGroup="editdetail" ControlToValidate="txtStreetPostCode" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </span>
                                        <div class="clear"></div>
                                        </span>
                                    </div>
                                    <%-- <div class="form-group spicaldivin">--%>
                                    <div class="form-group spical">
                                        <span class="col-md-12">
                                            <label class="control-label">
                                                Email:<span class="symbol required"></span>
                                            </label>
                                        </span><span class="col-md-12">
                                            <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                ValidationGroup="editdetail" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="" CssClass="comperror"
                                                ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="form-group spical">
                                        <span class="col-md-6">
                                            <label>
                                                Mobile:
                                            </label>
                                            <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile"
                                                ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>

                                        </span>
                                        <span class="col-md-6">
                                            <span>
                                                <label>Phone</label>
                                            </span>
                                            <span>
                                                <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                    ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                    ValidationExpression="^(07|03|08|02)[\d]{8}$"></asp:RegularExpressionValidator>--%>

                                            </span>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group spical" style="margin-top: 10px; width: 100%;">
                                        <span class="col-md-6">
                                            <label style="width: 100%;">Solar Type</label>
                                            <div class="radio radio-info radio-inline">
                                                <label for="<%=rblResCom1.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                    <span class="text">Res&nbsp;</span>
                                                </label>

                                                <label for="<%=rblResCom2.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                    <span class="text">Com&nbsp;</span>
                                                </label>

                                            </div>
                                        </span>
                                        <span class="col-md-6">
                                            <label style="width: 100%;">Area</label>

                                            <div class="radio radio-info radio-inline">
                                                <label for="<%=rblArea1.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblArea1" GroupName="ar" />
                                                    <span class="text">Metro&nbsp;</span>
                                                </label>

                                                <label for="<%=rblArea2.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" />
                                                    <span class="text">Regional&nbsp;</span>
                                                </label>
                                            </div>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                        <asp:Button ID="ibtnUpdateDetail" runat="server" Text="Update" OnClick="ibtnUpdateDetail_Click"
                                            CssClass="btn btn-primary savewhiteicon btnsaveicon" CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNULLEdit" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndEditCustID" runat="server" />
            <!------------------------->

            <!-- ModalPopup Add Folloup -->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderFollowUpNew" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelFollowUp" DropShadow="false" PopupControlID="divAddFollowUp"
                TargetControlID="btnNULLStatus">
            </cc1:ModalPopupExtender>
            <div id="divAddFollowUp" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog " style="margin-left: -300px">
                    <div class="modal-content  modal-lg" style="width: 900px; max-height: 500px; min-height: 300px;">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelFollowUp" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                           Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                <b>Add FollowUp</b></h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">

                                <div class="row col-md-12">
                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="Label8" runat="server" class="control-label">
                                                                Contact
                                            </asp:Label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlContact" runat="server"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <%--<asp:ListItem Value="">Select</asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="Label3" runat="server" class="control-label">
                                                                Select
                                            </asp:Label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlManager" runat="server" AppendDataBoundItems="true"
                                                CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1" Selected="True">Follow Up</asp:ListItem>
                                                <asp:ListItem Value="2">Manager</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="LabelDate" runat="server" class="control-label">
                                                                Next Followup Date
                                            </asp:Label>
                                        </span><span>
                                            <div class="input-group date" id="datetimepicker7">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNextFollowupDate"
                                                    ValidationGroup="FollowUp" ErrorMessage="Required Date" CssClass="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-md-12">
                                    <div class="form-group col-md-8">
                                        <span class="name">
                                            <asp:Label ID="Label10" runat="server" class="control-label">
                                                                Description
                                            </asp:Label>
                                        </span><span>
                                            <asp:TextBox ID="txtDescription" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="Label19" runat="server" class="control-label">
                                                                Reject
                                            </asp:Label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlreject" runat="server" AppendDataBoundItems="true"
                                                CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-md-12">
                                    <div class="form-group col-md-12 text-center">
                                        <asp:LinkButton class="btn btn-primary savewhiteicon btnsaveicon" ID="btnAdd" runat="server"
                                            ValidationGroup="FollowUp" Text="Save" OnClick="btnAdd_Click" />
                                    </div>
                                </div>

                                <asp:HiddenField ID="hndMode" runat="server" />
                                <asp:HiddenField ID="hndFollowEditCustInfoID" runat="server" />
                                <div class="row col-md-12">
                                    <div class="row finalgrid" id="divFollowUpDetails" runat="server" visible="false">
                                        <div class="col-md-12" runat="server" id="divFollowUp">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 10%; text-align: center">#</th>
                                                        <th style="width: 25%; text-align: center">Contact</th>
                                                        <th style="width: 15%; text-align: center">Next Followup Date</th>
                                                        <th style="width: 30%; text-align: justify">Description</th>
                                                        <th style="width: 10%; text-align: center">Reject</th>
                                                        <th style="width: 10%; text-align: center">Action</th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="rptFollowUpDetails" runat="server" OnItemCommand="rptFollowUpDetails_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 10%; text-align: center">
                                                                <asp:Label ID="lblRowNumber" Text='<%# Container.ItemIndex + 1 %>' runat="server" />
                                                            </td>

                                                            <td style="text-align: center">
                                                                <%#Eval("Contact") %>
                                                            </td>

                                                            <td style="text-align: center">
                                                                <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                            </td>

                                                            <td style="width: 30%; text-align: justify;">
                                                                <%-- <asp:Label Text='<%#Eval("Description") %>' runat="server" 
                                                                data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>'/>--%>

                                                                <%#Eval("Description") %>
                                                            </td>

                                                            <td style="text-align: center;">
                                                                <%#Eval("Reject").ToString()=="1"?"Yes":Eval("Reject").ToString()=="0"?"No":"" %>
                                                            </td>

                                                            <td style="text-align: center;">
                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" Visible='<%#Eval("firstrecord").ToString()=="1"?true:false%>' CommandName="Select"
                                                                    CommandArgument='<%#Eval("CustInfoID") + ";" + Eval("CustomerID")%>' CausesValidation="false">
                                                                     <i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-md-12">
                                    <div class="tablescrolldiv xscroll" style="overflow: auto; margin-bottom: 10px;">
                                        <%--<div id="divgrdFollowUp" runat="server" class="table-responsive xscroll">--%>
                                        <div id="divgrdFollowUp" runat="server" class="content padtopzero marbtm50 finalgrid">
                                            <asp:GridView ID="grdFollowUp" DataKeyNames="CustInfoID" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                                                AutoGenerateColumns="false" CssClass="tooltip-demo text-center GridviewScrollItem table table-striped table-bordered table-hover">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="40px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%#Eval("Contact") %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="150px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Next Followup Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="150px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%#Eval("Description") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reject" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%#Eval("Reject").ToString()=="1"?"Yes":Eval("Reject").ToString()=="0"?"No":"" %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false"
                                                                Visible='<%#Eval("firstrecord").ToString()=="1"?true:false%>'>
                                                                     <i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                <EmptyDataRowStyle Font-Bold="True" />
                                            </asp:GridView>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndCustomerID" runat="server" />
            <!------------------------->

            <!-- ModalPopup Add New Project -->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddProject" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelProject" DropShadow="false" PopupControlID="divAddProject"
                TargetControlID="btnNULLProject">
            </cc1:ModalPopupExtender>
            <div id="divAddProject" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog " style="margin-left: -300px">
                    <div class="modal-content  modal-lg" style="width: 900px; min-height: 300px;">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelProject" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                           Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="myModalAdd" style="padding-top: 4px">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                                <b>Add New Project</b></h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">

                                <div class="row col-md-12">
                                    <div class="row finalgrid" id="div9" runat="server">
                                        <div class="col-md-12" runat="server" id="div10">
                                            <uc1:NewProject runat="server" ID="AddProject1" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:Button ID="btnNULLProject" Style="display: none;" runat="server" />
            <asp:HiddenField runat="server" ID="hndAddProjectCustomerID" />
            <!------------------------->

            <script type="text/javascript">
                function BindState(value) {
                    var arr = value.split('|')

                    //alert(arr[1]);
                    $("#<%=ddlStreetCity.ClientID%>").val(arr[0].trim());
                    $("#<%=txtStreetState.ClientID%>").val(arr[1].trim());
                    $("#<%=txtStreetPostCode.ClientID%>").val(arr[2].trim());
                }
            </script>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
