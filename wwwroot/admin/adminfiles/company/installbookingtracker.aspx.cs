using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;

public partial class admin_adminfiles_company_installbookingtracker : System.Web.UI.Page
{
    decimal totalpanels = 0;
    static DataView dv;
    protected static int custompageIndex;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string openModal = "false";

    protected void Page_Load(object sender, EventArgs e)
    {
        //projectpreinst.UserControlButtonClicked += new includes_controls_projectpreinst.customHandler(MyParentMethod);
        if (openModal == "true")
        {
            //btnUpdatePreInst
            CheckBox chkElecDistOK = projectpreinst.FindControl("chkElecDistOK") as CheckBox;
            DropDownList ddlInstaller = projectpreinst.FindControl("ddlInstaller") as DropDownList;

            TextBox txtInstallCity = projectpreinst.FindControl("txtInstallCity") as TextBox;
            ImageButton btnCreateSTCForm = projectpreinst.FindControl("btnCreateSTCForm") as ImageButton;
            ImageButton btnCustAddress = projectpreinst.FindControl("btnCustAddress") as ImageButton;
            ImageButton btnUpdateAddress = projectpreinst.FindControl("btnUpdateAddress") as ImageButton;
            Button btnMove = projectpreinst.FindControl("btnMove") as Button;
            Button btnUpdatePreInst = projectpreinst.FindControl("btnUpdatePreInst") as Button;
          
            // HiddenField hdnclosepopup = projectpreinst.FindControl("hdnclosepopup") as HiddenField;

            if (btnMove.Text != "" || btnUpdateAddress.ImageUrl != "" || btnCreateSTCForm.ImageUrl != "" || btnCustAddress.ImageUrl != "" || chkElecDistOK.Checked == true || chkElecDistOK.Checked == false || ddlInstaller.SelectedValue.ToString() != "" || ddlInstaller.SelectedValue.ToString() == "" || txtInstallCity.Text != "" || txtInstallCity.Text == "")
            {
                ModalPopupExtender1.Show();

            }
            //if(hdnclosepopup.Value=="1")
            //{
            //    ModalPopupExtender1.Hide();
            //}
        }


        if (!IsPostBack)
        {
            ModalPopupExtender1.Hide();
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            custompageIndex = 1;

            //includes_controls_projectpreinst objclass1 = new includes_controls_projectpreinst();
            //objclass1.datevalidator();
            //projectpreinst.datevalidator1();
            // UserControl projectpreinst = (UserControl)FindControl("projectpreinst");

            //TextBox txtInstallBookingDate = (TextBox)projectpreinst.FindControl("txtInstallBookingDate");
            //RangeValidator DateRange = (RangeValidator)projectpreinst.FindControl("DateRange");
            //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
            //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());

            //if ((Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("DSalesRep")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")))
            //{
            //    tdExport.Visible = false;
            //}

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);




            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            BindSearchDropdown();
            //BindGrid(0);
        }
    }

   
    public void BindSearchDropdown()
    {
        ddlProjectTypeID.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectTypeID.DataMember = "ProjectType";
        ddlProjectTypeID.DataTextField = "ProjectType";
        ddlProjectTypeID.DataValueField = "ProjectTypeID";
        ddlProjectTypeID.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlFinanceOption.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlFinanceOption.DataValueField = "FinanceWithID";
        ddlFinanceOption.DataTextField = "FinanceWith";
        ddlFinanceOption.DataMember = "FinanceWith";
        ddlFinanceOption.DataBind();

        ddlSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        ddlSearchStatus.DataValueField = "ProjectStatusID";
        ddlSearchStatus.DataTextField = "ProjectStatus";
        ddlSearchStatus.DataMember = "ProjectStatus";
        ddlSearchStatus.DataBind();

        ddlrooftype.DataSource = ClstblRoofTypes.tblRoofTypes_Select();
        ddlrooftype.DataValueField = "RoofTypeID";
        ddlrooftype.DataTextField = "RoofType";
        ddlrooftype.DataMember = "RoofType";
        ddlrooftype.DataBind();

        ddlhousetype.DataSource = ClstblHouseType.tblHouseType_Select();
        ddlhousetype.DataValueField = "HouseTypeID";
        ddlhousetype.DataTextField = "HouseType";
        ddlhousetype.DataMember = "HouseType";
        ddlhousetype.DataBind();
    }

    protected DataTable GetGridData()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string employeeId = st.EmployeeID;

        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance"))
        {
            employeeId = "";
        }
        DataTable dt = ClstblProjects.tblProjects_InstallBookingTracker1(employeeId, txtClient.Text, txtProjectNumber.Text.Trim(), txtMQNsearch.Text, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCodeFrom.Text.Trim(), txtSearchPostCodeTo.Text.Trim(), ddlSearchStatus.SelectedValue, ddlProjectTypeID.SelectedValue, txtPModel.Text, txtIModel.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlFinanceOption.SelectedValue, ddlElecDistAppDate.SelectedValue, ddldatetype.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), txtpurchaseNo.Text,ddlhistoric.SelectedValue,ddlrooftype.SelectedValue,ddlhousetype.SelectedValue);

        if (dt.Rows.Count > 0)
        {
            lblTotalPanels.Text = dt.Compute("Sum(NumberPanels)", "").ToString();
        }
        else
        {
            lblTotalPanels.Text = "0";
        }
        // ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            // divTotal.Visible = false;
            SetNoRecords();
            //PanNoRecord.Visible = true;

            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start

        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;

        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        ModalPopupExtender1.Hide();
    }
    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    txtClient.Text = string.Empty;
    //    txtProjectNumber.Text = string.Empty;
    //    txtMQNsearch.Text = string.Empty;
    //    txtSearchStreet.Text = string.Empty;
    //    txtSerachCity.Text = string.Empty;
    //    ddlSearchState.SelectedValue = "";
    //    txtSearchPostCodeFrom.Text = string.Empty;
    //    txtSearchPostCodeTo.Text = string.Empty;
    //    ddlSearchStatus.SelectedValue = "";
    //    ddlProjectTypeID.SelectedValue = "";
    //    txtPModel.Text = string.Empty;
    //    txtIModel.Text = string.Empty;
    //    txtSysCapacityFrom.Text = string.Empty;
    //    txtSysCapacityTo.Text = string.Empty;
    //    ddlFinanceOption.SelectedValue = string.Empty;
    //    ddlElecDistAppDate.SelectedValue = string.Empty;
    //    ddlInstallBookingDate.SelectedValue = string.Empty;
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtpurchaseNo.Text = string.Empty;

    //    BindGrid(0);
    //}

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //Response.Write(userid);
        //Response.End();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string employeeId = st.EmployeeID;

        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance"))
        {
            employeeId = "";
        }
        DataTable dt = ClstblProjects.tblProjects_InstallBookingTracker1(employeeId, txtClient.Text, txtProjectNumber.Text.Trim(), txtMQNsearch.Text, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCodeFrom.Text.Trim(), txtSearchPostCodeTo.Text.Trim(), ddlSearchStatus.SelectedValue, ddlProjectTypeID.SelectedValue, txtPModel.Text, txtIModel.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlFinanceOption.SelectedValue, ddlElecDistAppDate.SelectedValue, ddldatetype.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), txtpurchaseNo.Text, ddlhistoric.SelectedValue, ddlrooftype.SelectedValue, ddlhousetype.SelectedValue);
        // DataTable dt = ClstblProjects.tblProjects_InstallBookingTracker1(employeeId, txtClient.Text, txtProjectNumber.Text.Trim(), txtMQNsearch.Text, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCodeFrom.Text.Trim(), txtSearchPostCodeTo.Text.Trim(), ddlSearchStatus.SelectedValue, ddlProjectTypeID.SelectedValue, txtPModel.Text, txtIModel.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlFinanceOption.SelectedValue, ddlElecDistAppDate.SelectedValue, ddldatetype.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), txtpurchaseNo.Text, chkishistoric.Checked.ToString(),ddlrooftype.SelectedValue,ddlhousetype.SelectedValue);
        //Response.Write(dt.Rows.Count);
        //Response.End();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                   .Select(x => x.ColumnName)
                                   .ToArray();

        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "InstallBookingTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 9, 3, 4, 5, 6, 12, 7, 8, 13, 14, 2, 20, 18, 19, 10, 22, 17, 21, 11, 16, 26, 27, 28, 29 };
            string[] arrHeader = { "Projectno", "P/CD", "Contact", "Mobile", "Phone", "System Details", "Project Notes", "Install Notes", "House Type", "Roof Type", "Project", "EmployeeName", "State", "Status", "ManualPrjNo", "InstallBookDate", "InstallCompleteDate", "DepositRecDate", "SystemCap", "Price", "IntallerName", "Elec Retailer", "NMI Number", "ElecDistApprovelRef " };
            //only change file extension to .xls for excel file
            //Response.Write(ColList[2]);
            //Response.End();
            
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }


    //protected void GridView1_DataBound(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridView1.BottomPagerRow;
    //    Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
    //    lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
    //    int[] page = new int[7];
    //    page[0] = GridView1.PageIndex - 2;
    //    page[1] = GridView1.PageIndex - 1;
    //    page[2] = GridView1.PageIndex;
    //    page[3] = GridView1.PageIndex + 1;
    //    page[4] = GridView1.PageIndex + 2;
    //    page[5] = GridView1.PageIndex + 3;
    //    page[6] = GridView1.PageIndex + 4;
    //    for (int i = 0; i < 7; i++)
    //    {
    //        if (i != 3)
    //        {
    //            if (page[i] < 1 || page[i] > GridView1.PageCount)
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Visible = false;
    //            }
    //            else
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Text = Convert.ToString(page[i]);
    //                lnkbtn.CommandName = "PageNo";
    //                lnkbtn.CommandArgument = lnkbtn.Text;

    //            }
    //        }
    //    }
    //    if (GridView1.PageIndex == 0)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
    //        lnkbtn.Visible = false;

    //    }
    //    if (GridView1.PageIndex == GridView1.PageCount - 1)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
    //        lnkbtn.Visible = false;

    //    }
    //    Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
    //    if (dv.ToTable().Rows.Count > 0)
    //    {
    //        int iTotalRecords = dv.ToTable().Rows.Count;
    //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //        if (iEndRecord > iTotalRecords)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        if (iStartsRecods == 0)
    //        {
    //            iStartsRecods = 1;
    //        }
    //        if (iEndRecord == 0)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //    }
    //    else
    //    {
    //        ltrPage.Text = "";
    //    }
    //}
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BindGrid(0);
    }
    protected void lnkjob_Click(object sender, EventArgs e)
    {

        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnProjectID = (HiddenField)item1.FindControl("hdnProjectID");
        HiddenField hdncust = (HiddenField)item1.FindControl("hdncust");
        hdn_custpopup.Value = hdncust.Value;
        DropDownList ddlInstaller = projectpreinst.FindControl("ddlInstaller") as DropDownList;
        DropDownList ddlDesigner = projectpreinst.FindControl("ddlDesigner") as DropDownList;
        DropDownList ddlElectrician = projectpreinst.FindControl("ddlElectrician") as DropDownList;
        DropDownList ddlformbaystreettype = projectpreinst.FindControl("ddlformbaystreettype") as DropDownList;
        DropDownList ddlformbayunittype = projectpreinst.FindControl("ddlformbayunittype") as DropDownList;
        Label lblloc = projectpreinst.FindControl("lblloc") as Label;
        Label lblInstaller = projectpreinst.FindControl("lblInstaller") as Label;
        Label lbldesign = projectpreinst.FindControl("lbldesign") as Label;
        Label lblec = projectpreinst.FindControl("lblec") as Label;
        DropDownList ddlStockAllocationStore = projectpreinst.FindControl("ddlStockAllocationStore") as DropDownList;
        Button btnaddnew = projectpreinst.FindControl("btnaddnew") as Button;
        ddlInstaller.Visible = true;
        ddlDesigner.Visible = true;
        ddlElectrician.Visible = true;
        ddlStockAllocationStore.Visible = true;
        ddlformbayunittype.Visible = true;
        ddlformbaystreettype.Visible = true;
        lbldesign.Visible = true;
        lblInstaller.Visible = true;
        lblec.Visible = true;
        lblloc.Visible = true;
        btnaddnew.Visible = true;
        DropDownList ddlinstall2 = projectpreinst.FindControl("ddlinstall2") as DropDownList;
        DropDownList ddldesign2 = projectpreinst.FindControl("ddldesign2") as DropDownList;
        DropDownList ddlec2 = projectpreinst.FindControl("ddlec2") as DropDownList;

        ddlec2.Visible = true;
        ddlinstall2.Visible = true;
        ddlec2.Visible = true;

        if (hdnProjectID.Value != string.Empty)
        {

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
            hdnInvoiceProjectid.Value = hdnProjectID.Value;
            //HiddenField hdnclosepopup = projectpreinst.FindControl("hdnclosepopup") as HiddenField;
            //hdnclosepopup.Value="0";
            ModalPopupExtender1.Show();
            openModal = "true";
            div_popup.Visible = true;
            //RangeValidator dateVal = (RangeValidator)RequestFormView.FindControl("RangeValidator1");
            //if (dateVal != null)
            //{

            //    dateVal.MinimumValue = Common.SessionDate().ToString(pattern);
            //    dateVal.MaximumValue = Common.SessionDate().AddYears(10).ToString(pattern);

            //}

            projectpreinst.GetPreInstClickByProject(hdnProjectID.Value);


        }
        else
        {
            div_popup.Visible = false;
            ModalPopupExtender1.Hide();
            openModal = "false";
        }

    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtClient.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtMQNsearch.Text = string.Empty;
        txtSearchStreet.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        txtSearchPostCodeFrom.Text = string.Empty;
        txtSearchPostCodeTo.Text = string.Empty;
        ddlSearchStatus.SelectedValue = "";
        ddlProjectTypeID.SelectedValue = "";
        txtPModel.Text = string.Empty;
        txtIModel.Text = string.Empty;
        txtSysCapacityFrom.Text = string.Empty;
        txtSysCapacityTo.Text = string.Empty;
        ddlFinanceOption.SelectedValue = string.Empty;
        ddlElecDistAppDate.SelectedValue = string.Empty;
        ddldatetype.SelectedValue = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtpurchaseNo.Text = string.Empty;
        ddlhousetype.SelectedValue = "";
        ddlrooftype.SelectedValue = "";
        ddlhistoric.SelectedValue = "1";
        BindGrid(0);
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvrow = e.Row;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    //protected void GridView1_DataBound1(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridView1.BottomPagerRow;
    //}

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    
}