using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_mtcecalldetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/printmtce.aspx", txtWriter);

        //    liDetail.Text = txtWriter.ToString();
        //}
        BindProjectMtce();
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/company/mtcecall.aspx");
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnCopy_Click(object sender, EventArgs e)
    {

    }

    protected void txtMtceDiscount_TextChanged(object sender, EventArgs e)
    {
        if (txtMtceDiscount.Text != string.Empty)
        {
            decimal balance = Convert.ToDecimal(txtMtceCost.Text) - Convert.ToDecimal(txtMtceDiscount.Text);
            txtMtceBalance.Text = Convert.ToString(balance);
        }
    }

    protected void lnkmaintainance_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string mtceid = Request.QueryString["id"];
        //Response.Write(mtceid);
        //Response.End();
        if (ProjectID != "")
        {
            Telerik_reports.generate_maintenance(ProjectID, mtceid);
        }
    }
    public void BindProjectMtce()
    {

        if ((Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Maintenance")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
        {
            divrightbtn.Visible = true;
        }
        else
        {
            divrightbtn.Visible = false;
        }

        ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        ddlSelectRecords.DataBind();

        //if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        //{
        //    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //    SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        //    string CEmpType = stEmpC.EmpType;
        //    string CSalesTeamID = stEmpC.SalesTeamID;

        //    if (Request.QueryString["proid"] != string.Empty)
        //    {
        //        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        //        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        //        string EmpType = stEmp.EmpType;
        //        string SalesTeamID = stEmp.SalesTeamID;

        //        if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
        //        {
        //            PanAddUpdate.Enabled = true;
        //        }
        //        else
        //        {
        //            PanAddUpdate.Enabled = false;
        //        }
        //    }
        //}
        BindDropDown();

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {

            SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintenanceID(Request.QueryString["id"]);
            ddlProjectMtceReasonSubID.SelectedValue = st.ProjectMtceReasonSubID;

            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            string CustomerID = stPro.CustomerID;
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
            lblCustomer.Text = stCust.Customer;
            lblProject.Text = stPro.Project;
            DataTable dtContact = ClstblContacts.tblContacts_SelectTop1ByCustId(CustomerID);
            if (dtContact.Rows.Count > 0)
            {
                lblContact.Text = dtContact.Rows[0]["ContFirst"].ToString() + " " + dtContact.Rows[0]["ContLast"].ToString();
                lblMobile.Text = dtContact.Rows[0]["ContMobile"].ToString();
            }
            try
            {
                lblInstallDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            lblSalesRep.Text = stPro.SalesRepName;
            if (stPro.Installer != string.Empty)
            {
                lblInstaller.Text = stPro.InstallerName;
            }
            else
            {
                lblInstaller.Text = "N/A";
            }
            if (stPro.Electrician != string.Empty)
            {
                lblMeterElec.Text = stPro.ElectricianName;
            }
            else
            {
                lblMeterElec.Text = "N/A";
            }
            if (stPro.PanelBrandID != string.Empty && stPro.InverterDetailsID != string.Empty)
            {
                lblSystem.Text = stPro.SystemDetails;
            }

            //BindGrid(0);
        

       
        lnkmaintainance.Visible = false;
       
            ddlSelectRecords.Visible = false;
            //divMtceDetail.Visible = true;
            PanAddUpdate.Visible = true;
            string ProjectMaintenanceID = Request.QueryString["id"];
            string id = ProjectMaintenanceID;
            //hdnmtceid.Text = id;
            //SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintenanceID(id);
            try
            {
                ddlProjectMtceReasonID.SelectedValue = st.ProjectMtceReasonID;
            }
            catch
            {
            }
            try
            {
                ddlProjectMtceReasonSubID.SelectedValue = st.ProjectMtceReasonSubID;
            }
            catch { }
            try
            {
                ddlProjectMtceCallID.SelectedValue = st.ProjectMtceCallID;
            }
            catch { }
            try
            {
                ddlProjectMtceStatusID.SelectedValue = st.ProjectMtceStatusID;
            }
            catch { }
            try
            {
                ddlInstallerAssigned.SelectedValue = st.InstallerID;
            }
            catch { }

            try
            {
                txtOpenDate.Text = Convert.ToDateTime(st.OpenDate).ToShortDateString();
            }
            catch { }

            try
            {
                txtPropResolutionDate.Text = Convert.ToDateTime(st.PropResolutionDate).ToShortDateString();
            }
            catch { }
            try
            {
                txtCompletionDate.Text = Convert.ToDateTime(st.CompletionDate).ToShortDateString();
            }
            catch { }


            txtCustomerInput.Text = st.CustomerInput;
            txtFaultIdentified.Text = st.FaultIdentified;
            txtActionRequired.Text = st.ActionRequired;
            txtWorkDone.Text = st.WorkDone;
            txtCurrentPhoneContact.Text = st.CurrentPhoneContact;
            chkWarranty.Checked = Convert.ToBoolean(st.Warranty);
            try
            {
                txtMtceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceCost);
            }
            catch { }
            try
            {
                txtServiceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.ServiceCost);
            }
            catch { }

            try
            {
                txtMtceDiscount.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceDiscount);
            }
            catch { }
            try
            {
                txtMtceBalance.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceBalance);
            }
            catch { }

            ddlMtceRecBy.SelectedValue = st.MtceRecBy;
            ddlFPTransTypeID.SelectedValue = st.FPTransTypeID;
           
            txtMtceBalance.Enabled = false;


           
            lnkBack.Visible = true;
           
            btnCopy.Visible = false;
            lnkmaintainance.Visible = true;

        }


    }
    public void BindDropDown()
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlProjectMtceReasonID.Items.Clear();
        ddlProjectMtceReasonID.Items.Add(item1);

        ddlProjectMtceReasonID.DataSource = ClstblProjectMtceReason.tblProjectMtceReason_SelectASC();
        ddlProjectMtceReasonID.DataValueField = "ProjectMtceReasonID";
        ddlProjectMtceReasonID.DataMember = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataTextField = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataBind();

        //ddlProjectMtceReasonID.SelectedValue = "2";

        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlProjectMtceReasonSubID.Items.Clear();
        ddlProjectMtceReasonSubID.Items.Add(item2);

        ddlProjectMtceReasonSubID.DataSource = ClstblProjectMtceReasonSub.tblProjectMtceReasonSub_SelectASC();
        ddlProjectMtceReasonSubID.DataValueField = "ProjectMtceReasonSubID";
        ddlProjectMtceReasonSubID.DataMember = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataTextField = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataBind();

        ddlProjectMtceReasonSubID.SelectedValue = "2";

        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlEmployee.Items.Clear();
        ddlEmployee.Items.Add(item3);

        ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataMember = "fullname";
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataBind();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        ddlEmployee.SelectedValue = stEmp.EmployeeID;

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlInstallerAssigned.Items.Clear();
        ddlInstallerAssigned.Items.Add(item4);

        ddlInstallerAssigned.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstallerAssigned.DataValueField = "ContactID";
        ddlInstallerAssigned.DataMember = "Contact";
        ddlInstallerAssigned.DataTextField = "Contact";
        ddlInstallerAssigned.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlProjectMtceCallID.Items.Clear();
        ddlProjectMtceCallID.Items.Add(item5);

        ddlProjectMtceCallID.DataSource = ClstblProjects.tblProjectMtceCall_SelectASC();
        ddlProjectMtceCallID.DataValueField = "ProjectMtceCallID";
        ddlProjectMtceCallID.DataMember = "ProjectMtceCall";
        ddlProjectMtceCallID.DataTextField = "ProjectMtceCall";
        ddlProjectMtceCallID.DataBind();

        ddlProjectMtceCallID.SelectedValue = "4";

        ListItem item6 = new ListItem();
        item6.Text = "Select";
        item6.Value = "";
        ddlProjectMtceStatusID.Items.Clear();
        ddlProjectMtceStatusID.Items.Add(item6);

        ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectASC();
        ddlProjectMtceStatusID.DataValueField = "ProjectMtceStatusID";
        ddlProjectMtceStatusID.DataMember = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataTextField = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataBind();

        ddlProjectMtceStatusID.SelectedValue = "2";

        ListItem item7 = new ListItem();
        item7.Text = "Select";
        item7.Value = "";
        ddlFPTransTypeID.Items.Clear();
        ddlFPTransTypeID.Items.Add(item7);

        ddlFPTransTypeID.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
        ddlFPTransTypeID.DataValueField = "FPTransTypeID";
        ddlFPTransTypeID.DataMember = "FPTransType";
        ddlFPTransTypeID.DataTextField = "FPTransType";
        ddlFPTransTypeID.DataBind();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlMtceRecBy.Items.Clear();
        ddlMtceRecBy.Items.Add(item8);

        ddlMtceRecBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlMtceRecBy.DataValueField = "EmployeeID";
        ddlMtceRecBy.DataMember = "EmpNicName";
        ddlMtceRecBy.DataTextField = "EmpNicName";
        ddlMtceRecBy.DataBind();
    }
}