using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_ApplicationTracker : System.Web.UI.Page
{
    protected static string Siteurl;
    protected static string address;
    static DataView dv;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string openModal = "false";
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (openModal == "true")
        //{
        //    ModalPopupExtenderEdit.Show();
        //}

        // BindGrid(0);
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        HidePanels();
        if (!IsPostBack)
        {
            BindDropDown();
            custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            //if (Convert.ToBoolean(st_emp.showexcel) == true)
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}


            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            BindGrid(0);
        }

    }

    public void BindDropDown()
    {
        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        ddlProjectTypeID.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectTypeID.DataMember = "ProjectType";
        ddlProjectTypeID.DataTextField = "ProjectType";
        ddlProjectTypeID.DataValueField = "ProjectTypeID";
        ddlProjectTypeID.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

    }

    protected DataTable GetGridData()
    {
        //BindDataCount();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        DataTable dt = new DataTable();

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        dt = ClstblProjects.tblProjects_SelectApplicationTrackerSearch(txtProjectNumber.Text, ddlProjectTypeID.SelectedValue, ddlSearchState.SelectedValue, selectedItem, txtStartDate.Text, txtEndDate.Text, ddlApplication.SelectedValue,ddlSelectDate.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {

        PanGrid.Visible = true;
        DataTable dt = GetGridData();

        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            //Response.Write(dt.Rows.Count);
            //Response.End();
            HidePanels();

            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();



            //PanNoRecord.Visible = false;


            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {

                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {

                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;

                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    //Response.Write(iTotalRecords);
                    //Response.End();
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
        //Response.Write(dt.Rows.Count);
        //Response.End();
    }

    public void BindDropDown2(string projid)
    {
        SttblProjects stProj = ClstblProjects.tblProjects_SelectByProjectID(projid);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProj.CustomerID);

        //String Inverter1ID;

        DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(stProj.InstallState);
        string State = "0";

        if (dtState.Rows.Count > 0)
        {
            State = dtState.Rows[0]["CompanyLocationID"].ToString();
        }

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlHouseType.Items.Clear();
        ddlHouseType.Items.Add(item4);

        ddlHouseType.DataSource = ClstblHouseType.tblHouseType_SelectASC();
        ddlHouseType.DataValueField = "HouseTypeID";
        ddlHouseType.DataMember = "HouseType";
        ddlHouseType.DataTextField = "HouseType";
        ddlHouseType.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlRoofType.Items.Clear();
        ddlRoofType.Items.Add(item5);

        ddlRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        ddlRoofType.DataValueField = "RoofTypeID";
        ddlRoofType.DataMember = "RoofType";
        ddlRoofType.DataTextField = "RoofType";
        ddlRoofType.DataBind();

        ListItem item6 = new ListItem();
        item6.Text = "Select";
        item6.Value = "";
        ddlRoofAngle.Items.Clear();
        ddlRoofAngle.Items.Add(item6);

        ddlRoofAngle.DataSource = ClstblRoofAngles.tblRoofAngles_SelectASC();
        ddlRoofAngle.DataValueField = "RoofAngleID";
        ddlRoofAngle.DataMember = "RoofAngle";
        ddlRoofAngle.DataTextField = "RoofAngle";
        ddlRoofAngle.DataBind();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        System.Web.UI.WebControls.ListItem item11 = new System.Web.UI.WebControls.ListItem();
        item11.Text = "Select";
        item11.Value = "";
        ddlElecDistApplyMethod.Items.Clear();
        ddlElecDistApplyMethod.Items.Add(item11);

        ddlElecDistApplyMethod.DataSource = ClsProjectSale.tblElecDistApplyMethod_SelectASC();
        ddlElecDistApplyMethod.DataValueField = "ElecDistApplyMethodID";
        ddlElecDistApplyMethod.DataMember = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataTextField = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataBind();

        System.Web.UI.WebControls.ListItem item12 = new System.Web.UI.WebControls.ListItem();
        item12.Text = "Select";
        item12.Value = "";
        ddlEmployee.Items.Clear();
        ddlEmployee.Items.Add(item12);

        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
            if (SalesTeam != string.Empty)
            {
                ddlEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataMember = "fullname";
            ddlEmployee.DataTextField = "fullname";
            ddlEmployee.DataBind();
            ddlEmployee.SelectedValue = stEmp.EmployeeID;
        }
        catch { }


        System.Web.UI.WebControls.ListItem item13 = new System.Web.UI.WebControls.ListItem();
        item13.Text = "Select";
        item13.Value = "";
        ddlElecDistAppBy.Items.Clear();
        ddlElecDistAppBy.Items.Add(item13);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlElecDistAppBy.DataValueField = "EmployeeID";
            ddlElecDistAppBy.DataMember = "fullname";
            ddlElecDistAppBy.DataTextField = "fullname";
            ddlElecDistAppBy.DataBind();
        }
        catch
        { }


        string EleDist = "";
        if (stProj.InstallState == "NSW")
        {
            EleDist = "select * from tblElecDistributor where NSW=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "SA")
        {
            EleDist = "select * from tblElecDistributor where SA=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "QLD")
        {
            EleDist = "select * from tblElecDistributor where QLD=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "VIC")
        {
            EleDist = "select * from tblElecDistributor where VIC=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "WA")
        {
            EleDist = "select * from tblElecDistributor where WA=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "ACT")
        {
            EleDist = "select * from tblElecDistributor where ACT=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "TAS")
        {
            EleDist = "select * from tblElecDistributor where TAS=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "NT")
        {
            EleDist = "select * from tblElecDistributor where NT=1 order by ElecDistributor asc";
        }

        DataTable dtElecDist = ClsProjectSale.ap_form_element_execute(EleDist);
        if (dtElecDist.Rows.Count > 0)
        {
            ListItem item7 = new ListItem();
            item7.Text = "Select";
            item7.Value = "";
            ddlElecDist.Items.Clear();
            ddlElecDist.Items.Add(item7);

            ddlElecDist.DataSource = dtElecDist;
            ddlElecDist.DataValueField = "ElecDistributorID";
            ddlElecDist.DataMember = "ElecDistributor";
            ddlElecDist.DataTextField = "ElecDistributor";
            ddlElecDist.DataBind();
        }

        string EleRet = "";
        if (stProj.InstallState == "NSW")
        {
            EleRet = "select * from tblElecRetailer where NSW=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "SA")
        {
            EleRet = "select * from tblElecRetailer where SA=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "QLD")
        {
            EleRet = "select * from tblElecRetailer where QLD=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "VIC")
        {
            EleRet = "select * from tblElecRetailer where VIC=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "WA")
        {
            EleRet = "select * from tblElecRetailer where WA=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "ACT")
        {
            EleRet = "select * from tblElecRetailer where ACT=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "TAS")
        {
            EleRet = "select * from tblElecRetailer where TAS=1 order by ElecRetailer asc";
        }
        if (stProj.InstallState == "NT")
        {
            EleRet = "select * from tblElecRetailer where NT=1 order by ElecRetailer asc";
        }

        DataTable dtElecRet = ClsProjectSale.ap_form_element_execute(EleRet);
        if (dtElecRet.Rows.Count > 0)
        {
            ListItem item8 = new ListItem();
            item8.Text = "Select";
            item8.Value = "";
            ddlElecRetailer.Items.Clear();
            ddlElecRetailer.Items.Add(item8);

            ddlElecRetailer.DataSource = dtElecRet;
            ddlElecRetailer.DataValueField = "ElecRetailerID";
            ddlElecRetailer.DataMember = "ElecRetailer";
            ddlElecRetailer.DataTextField = "ElecRetailer";
            ddlElecRetailer.DataBind();
        }

    }

    public void BindPOPup(string projid)
    {

        if (!string.IsNullOrEmpty(projid))
        {            
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(projid);
            hdnprojid.Value = projid;

            if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("SalesRep"))
            {
                ddlEmployee.Enabled = false;
                ddlElecDistAppBy.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }
                }
            }
            BindDropDown2(projid);
            chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
            chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);
            ddlElecDist.SelectedValue = st.ElecDistributorID;
            txtApprovalRef.Text = st.ElecDistApprovelRef;
            ddlElecRetailer.SelectedValue = st.ElecRetailerID;
            txtRegPlanNo.Text = st.RegPlanNo;
            ddlHouseType.SelectedValue = st.HouseTypeID;
            ddlRoofType.SelectedValue = st.RoofTypeID;
            ddlRoofAngle.SelectedValue = st.RoofAngleID;
            txtLotNum.Text = st.LotNumber;
            txtNMINumber.Text = st.NMINumber;
            txtPeakMeterNo.Text = st.MeterNumber1;
            txtOffPeakMeters.Text = st.MeterNumber2;
            txtMeterPhase.Text = st.MeterPhase;
            try
            {
                ddlmeterupgrade.SelectedValue = st.meterupgrade;
            }
            catch
            {
            }

           
            try
            {
                txtElecDistApplied.Text = Convert.ToDateTime(st.ElecDistApplied).ToShortDateString();
            }
            catch { }
            ddlElecDistApplyMethod.SelectedValue = st.ElecDistApplyMethod;

            SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(projid);

            if (st.ElecDistAppBy == string.Empty)
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                try
                {
                    ddlElecDistAppBy.SelectedValue = stEmp.EmployeeID;
                }
                catch { }
            }
            else
            {
                try
                {
                    ddlElecDistAppBy.SelectedValue = st.ElecDistAppBy;
                }
                catch { }
            }
            try
            {
                txtElecDistAppDate.Text = Convert.ToDateTime(st.ElecDistAppDate).ToShortDateString();
            }
            catch { }

            if (Roles.IsUserInRole("Installation Manager"))
            {
                if (st.ProjectStatusID == "3")
                {
                    if (st.ElecDistributorID == "12")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                    else if (st.ElecDistributorID == "13")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                        txtLotNum.Enabled = false;
                        txtRegPlanNo.Enabled = false;
                    }
                    else
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                }
            }
            if (st.ProjectTypeID == "8")
            {
                divSitedetails.Visible = false;
                divDistAppDetail.Visible = false;
            }          
        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);

    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        //   BindScript();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        //BindScript();
    }
    private void HidePanels()
    {
        //PanSuccess.Visible = false;
        //PanError.Visible = false;
        //PanNoRecord.Visible = false;

        //PanSearch.Visible = true;
    }

    protected void btnAssidnLead_Click(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        DropDownList ddlSalesRep;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();


            ddlSalesRep = (DropDownList)gridRow.FindControl("ddlSalesRep");

            if (ddlSalesRep.SelectedValue.ToString() != "")
            {
                bool suc = ClstblCustomers.tblCustomers_Update_Assignemployee(id, ddlSalesRep.SelectedValue.ToString(), stEmp.EmployeeID, DateTime.Now.AddHours(14).ToString(), Convert.ToString(true));
                //  Response.Write(suc + "</br>");
                ClstblProjects.tblProjects_UpdateEmployee(id, ddlSalesRep.SelectedValue.ToString());
                ClstblContacts.tblContacts_UpdateEmployee(id, ddlSalesRep.SelectedValue.ToString());
            }
        }
        // Response.End();
        BindGrid(0);
    }

    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    ddlSalesRepSearch.SelectedValue = "";
    //    ddlSearchType.SelectedValue = "3";
    //    ddlSearchSource.SelectedValue = "";
    //    ddlSearchStatus.SelectedValue = "2";
    //    ddlSearchSubSource.SelectedValue = "";
    //    ddlSearchState.SelectedValue = "";
    //    ddlTeam.SelectedValue = "";
    //    ddlSelectDate.SelectedValue = "";
    //    txtSearchPostCode.Text = string.Empty;
    //    txtsuburb.Text = string.Empty;
    //    txtContactSearch.Text = string.Empty;
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtSearchStreet.Text = string.Empty;

    //    BindGrid(0);
    //}

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        DataTable dt = new DataTable();

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //dt = ClstblProjects.tblProjects_SelectApplicationTrackerSearch(txtProjectNumber.Text, ddlProjectTypeID.SelectedValue, ddlSearchState.SelectedValue, selectedItem, txtStartDate.Text, txtEndDate.Text, chkApplication.Checked.ToString(),ddlSelectDate.SelectedValue);
        dt = GetGridData();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "ApplicationTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy") + ".xls";
            //int[] ColList = { 2, 14, 3, 4, 5, 6, 7, 22, 23, 19, 10, 11, 12, 20, 21, 13 };
            int[] ColList = { 4,7,6,27,28,11,13,30,14,15,25,34,31,16,22,17,18,19,20,21,23,24,29,32,33 };
            
            string[] arrHeader = { "Projectno","ProjectType","ProjectSTatus","Fname","Lname","Mobile",
                "Address"," Subrb","State","PostalCode","ElecDist","ElecRet",
                "NoofPanel","NMI","PeakMeterNo","ApplicationRefNo","ProjectNotes","InstallerNotes",
                "PanelModel","InverterModel","OffPeakMeters","MeterPhase","Email","Lotno",
                "RegPlanNo"};
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink ltProjectStatus = (HyperLink)e.Row.FindControl("lblmp");
            HiddenField lblState = (HiddenField)e.Row.FindControl("hndprojectid");
            if (lblState.Value != null && lblState.Value != "")
            {
                SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(lblState.Value);
                ltProjectStatus.NavigateUrl = SiteConfiguration.GetDocumnetPath("MP", st.MP);
            }
        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";

        if (e.CommandName.ToLower() == "editdetail")
        {
            // ModalPopupExtender1.Show();
            ModalPopupExtenderEdit.Show();
            //ModalPopupExtender2.Show();
            hndEditCustID.Value = e.CommandArgument.ToString();
            BindDropDown2(hndEditCustID.Value);
            BindPOPup(hndEditCustID.Value);


            BindScript();
            BindGrid(0);
        }
        if (e.CommandName.ToString() == "AddNote")
        {
            HiddenField1.Value = e.CommandArgument.ToString();
            ModalPopupExtenderNote.Show();
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(HiddenField1.Value);
             try
            {
                if (st.ElecDistApplied != null && st.ElecDistApplied != "")
                {
                    txtDistApplied.Text = Convert.ToDateTime(st.ElecDistApplied).ToShortDateString();
                }
            }
           
            catch(Exception ex) { }
            txtApprovalRef1.Text = st.ElecDistApprovelRef;
            txtprojectNotes.Text = st.ProjectNotes; Txtmeternumber.Text = st.MeterNumber1;
        }


    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        //Response.Write(dv.ToTable().Rows.Count);
        //Response.End();
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;

            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }

            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {

        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);

    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());

    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void ibtnEditDetail_Click(object sender, EventArgs e)
    {

        string HouseTypeID = ddlHouseType.SelectedValue;
        string RoofTypeID = ddlRoofType.SelectedValue;
        string RoofAngleID = ddlRoofAngle.SelectedValue;
        string ElecDistributorID = ddlElecDist.SelectedValue;
        string NMINumber = txtNMINumber.Text;
        string RegPlanNo = txtRegPlanNo.Text;
        string ElecRetailerID = ddlElecRetailer.SelectedValue;
        string LotNumber = txtLotNum.Text;
        string ElecDistApprovelRef = txtApprovalRef.Text;
        string ElecDistApplied = txtElecDistApplied.Text.Trim();
        string ElecDistAppBy = ddlElecDistAppBy.SelectedValue;
        string ElecDistApplyMethod = ddlElecDistApplyMethod.SelectedValue;
        string ElecDistAppDate = txtElecDistAppDate.Text.Trim();
        string Employee = ddlEmployee.SelectedValue;
        string MeterUpgrade = ddlmeterupgrade.SelectedValue;
        string projectID = hdnprojid.Value;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        string MeterNumber1 = txtPeakMeterNo.Text;
        string MeterNumber2 = txtOffPeakMeters.Text;
        string MeterPhase = txtMeterPhase.Text;
        string MeterEnoughSpace = Convert.ToString(chkEnoughMeterSpace.Checked);
        string OffPeak = Convert.ToString(chkIsSystemOffPeak.Checked);

        if (!string.IsNullOrEmpty(projectID))
        {
            bool successSale = ClsProjectSale.tblProjects_UpdateSaleForAppDetail(projectID, ElecDistributorID, ElecDistApprovelRef,
                ElecRetailerID, RegPlanNo, HouseTypeID, RoofTypeID, RoofAngleID, LotNumber, NMINumber, ElecDistApplied,
                ElecDistApplyMethod, ElecDistAppDate, ElecDistAppBy, MeterUpgrade,"", UpdatedBy);
            bool sc = ClsProjectSale.tblProjects_UpdateSale_Appl(projectID, MeterNumber1, MeterNumber2, MeterPhase, MeterEnoughSpace, OffPeak);



            ModalPopupExtenderEdit.Hide();

            SetAdd1();
            BindGrid(0);
        }

    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        ddlProjectTypeID.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }



    protected void btnSaveNotes_Click(object sender, EventArgs e)
    {
        string projectid = HiddenField1.Value;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        if (projectid != null && projectid != "")
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(projectid);
            //string Elecdist = "";
            if (txtApplicationNotes.Text != null && txtApplicationNotes.Text != "")
            {

                int Id = ClstblProjects.tblApplicationNotes_Insert(projectid, st.ProjectNumber, txtApplicationNotes.Text, st_emp.EmployeeID);
                DataTable dt = bindNotes(projectid);
            }
            string notes = txtprojectNotes.Text;
            if (txtprojectNotes.Text != "")
            {
                string original = Convert.ToString(txtprojectNotes.Text);
                int Count = Convert.ToInt32(txtprojectNotes.Text.Length);
                //var valid = new char[] {
                //'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                //'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
                //'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                //'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8',
                //'9', '0', '.', '_','$','+','[',']','\n'};
                //        var result = string.Join("",
                //            (from x in original.ToCharArray()
                //             where valid.Contains(x)
                //             select x.ToString())
                //                .ToArray());

                //txtProjectNotes.Text = result. Replace("\\r\\n", "<br/>");
                // }
                //txtProjectNotes.Text = result.ToString();
                if (original.Contains(',') || original.Contains('"') || original.Contains("''") || original.Contains('*'))
                {
                    string Data = original;

                    Data = original.Replace(',', ' ').Trim();
                    Data = Data.Replace('"', ' ').Trim();
                    Data = Data.Replace(',', ' ').Trim();
                    Data = Data.Replace('*', ' ').Trim();
                    Data = Data.Replace("'", " ").Trim();
                    notes = Data;
                }

                if (Count > 300)
                {

                    MsgError("Do not Enter MoreThen 300 Chars..");
                    string st1 = original.Substring(0, 300);
                    txtprojectNotes.Text = st1;
                    notes = st1;
                   // return;
                }

            }
            bool suc = ClstblProjects.tblProjects_UpdateDate(projectid, notes, txtApprovalRef1.Text, txtElecDistAppDate.Text);
            bool s1 = ClstblProjects.tblProjects_UpdatMeterNo(projectid,Txtmeternumber.Text);
            if (suc)
            {
                SetAdd1();
                txtprojectNotes.Text = "";
                txtApplicationNotes.Text = "";
                Txtmeternumber.Text = "";
            }
            BindGrid(0);
        }
        ModalPopupExtenderNote.Hide();
    }
    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }
    public DataTable bindNotes(string ProjectID)
    {
        DataTable dt = ClstblProjects.tblApplicationNotes_getData(ProjectID);
        if(dt.Rows.Count> 0)
        {
            PanGridNotes.Visible = true;
            GridViewNote.DataSource = dt;
            GridViewNote.DataBind();
        }
        else
        {
            PanGridNotes.Visible = false;
            SetNoRecords();
        }
        return dt;
    }
    protected void GridViewNote_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void GridViewNote_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Delete")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];

            HiddenField hdnprojectID = (HiddenField)row.FindControl("hdnprojectID");
            string ID = e.CommandArgument.ToString();
            //txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();

            if (ID != "")
            {
                bool succ = ClstblProjects.tblApplicationNotes_delete(ID);
                if (succ)
                {
                    SetAdd1();
                    BindGrid(0);
                    bindNotes(hdnprojectID.Value);
                }
            }
           
            bindNotes(hdnprojectID.Value);
            ModalPopupExtenderNote.Show();
            BindGrid(0);
        }
    }

    protected void GridViewNote_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}