﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_LeadTrackerNew : System.Web.UI.Page
{
    protected static string Siteurl;
    public DataView dv;

    delegate void DelUserControlMethod(string name);
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindFilterDropdown();

            try
            {
                ddlSelectDate.SelectedValue = "1";
            }
            catch (Exception ex) { }
            txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));
            txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));

            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Administrator"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = true;
                DivAssignEmp.Visible = true;
                DivAssignEmp1.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 3].Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
                lbtnExport.Visible = true;
            }
            else if (Roles.IsUserInRole("SalesRep"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = false;
                GridView1.Columns[GridView1.Columns.Count - 3].Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }

            if(Roles.IsUserInRole("Administrator"))
            {
                ddlTeam.Enabled = true;
            }

            BindEditCustomerDropdown();
            BindSalesRepDropdown();
            BindGrid(0);
        }
        DelUserControlMethod delUserControlMethod = new DelUserControlMethod(BindResult);
        //AddProject1.CallingPageMethod = delUserControlMethod;
    }

    public void BindSalesRepDropdown()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
        }

        if (SalesTeam != "")
        {
            ddlTeam.SelectedValue = SalesTeam;
        }

        DataTable dt = new DataTable();
        if (Roles.IsUserInRole("Sales Manager"))
        {
            if (SalesTeam != string.Empty)
            {
                dt = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            dt = ClstblEmployees.tblEmployees_SelectASC_Include();
        }

        ddlSalesRepSearch.DataSource = dt;
        ddlSalesRepSearch.DataMember = "fullname";
        ddlSalesRepSearch.DataTextField = "fullname";
        ddlSalesRepSearch.DataValueField = "EmployeeID";
        ddlSalesRepSearch.DataBind();

        try
        {
            ddlSalesRepSearch.SelectedValue = st.EmployeeID;
        }
        catch(Exception ex) { }

        ddlSalesRep1.DataSource = dt;
        ddlSalesRep1.DataTextField = "fullname";
        ddlSalesRep1.DataValueField = "EmployeeID";
        ddlSalesRep1.DataBind();
    }

    public void BindFilterDropdown()
    {
        ddlSearchType.DataSource = ClstblCustType.tblCustType_SelectActive();
        ddlSearchType.DataMember = "CustType";
        ddlSearchType.DataTextField = "CustType";
        ddlSearchType.DataValueField = "CustTypeID";
        ddlSearchType.DataBind();
        try
        {
            ddlSearchType.SelectedValue = "1";
        }
        catch (Exception ex)
        {
            SetError1();
        }

        ddlSearchStatus.DataSource = ClstblContacts.tblContLeadStatus_SelectByAsc();
        ddlSearchStatus.DataMember = "ContLeadStatus";
        ddlSearchStatus.DataTextField = "ContLeadStatus";
        ddlSearchStatus.DataValueField = "ContLeadStatusID";
        ddlSearchStatus.DataBind();
        try
        {
            ddlSearchStatus.SelectedValue = "2";
        }
        catch (Exception ex)
        {
            SetError1();
        }

        ddlSearchSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlSearchSource.DataMember = "CustSource";
        ddlSearchSource.DataTextField = "CustSource";
        ddlSearchSource.DataValueField = "CustSourceID";
        ddlSearchSource.DataBind();

        ddlSearchSubSource.DataSource = ClstblCustSourceSub.tblCustSourceSub_SelectActive();
        ddlSearchSubSource.DataMember = "CustSourceSub";
        ddlSearchSubSource.DataTextField = "CustSourceSub";
        ddlSearchSubSource.DataValueField = "CustSourceSubID";
        ddlSearchSubSource.DataBind();
        
        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();
    }

    public void BindEditCustomerDropdown()
    {
        DataTable dt = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataSource = dt;
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();

        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();
    }
    
    #region BindGrid Code
    protected DataTable GetGridData()
    {
        string RoleName = "";
        string[] Role = Roles.GetRolesForUser();

        for (int i = 0; i < Role.Length; i++)
        {
            if (Role[i] != "crm_Customer")
            {
                RoleName = Role[i].ToString();
            }
        }

        DataTable dt = ClstblSalesinfo.tblCustomer_GetData_LeadTracker(ddlSearchType.SelectedValue, ddlSearchStatus.SelectedValue, ddlSearchSource.SelectedValue, txtContactSearch.Text, txtMobile.Text, txtProjectNumber.Text, txtEmail.Text, RoleName, ddlSalesRepSearch.SelectedValue, txtSearchStreet.Text, txtsuburb.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, ddlSelectDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlTeam.SelectedValue, ddlSearchSubSource.SelectedValue);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        BindTot(dt);
        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            
            //PanNoRecord.Visible = false;
            //if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            //{
            //    if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
            //    {
            //        //========label Hide
            //        divnopage.Visible = false;
            //    }
            //    else
            //    {
            //        divnopage.Visible = true;
            //        int iTotalRecords = dv.ToTable().Rows.Count;
            //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            //        if (iEndRecord > iTotalRecords)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        if (iStartsRecods == 0)
            //        {
            //            iStartsRecods = 1;
            //        }
            //        if (iEndRecord == 0)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            //    }
            //}
            //else
            //{
            //    if (ddlSelectRecords.SelectedValue == "All")
            //    {
            //        divnopage.Visible = true;
            //        ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
            //    }
            //}
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            //GridViewRow gvrow = GridView1.BottomPagerRow;
            //Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

            GridViewRow gvrow = e.Row;
            Label lblcurrentpage = (Label)gvrow.FindControl("CurrentPage");

            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }

            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }

            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;

                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditDetail")
        {
            string CustomerID = e.CommandArgument.ToString();
            ResetEditForm();

            hndEditCustID.Value = CustomerID;
            DataTable dtDetails = ClstblSalesinfo.SP_LeadTracker_tblCustomer_GetCustomerByCustomerID(CustomerID);

            if (dtDetails.Rows.Count > 0)
            {
                txtContMr.Text = dtDetails.Rows[0]["ContMr"].ToString();
                txtContFirst.Text = dtDetails.Rows[0]["ContFirst"].ToString();
                txtContLast.Text = dtDetails.Rows[0]["ContLast"].ToString();

                txtformbayUnitNo.Text = dtDetails.Rows[0]["unit_number"].ToString();
                string UnitType = dtDetails.Rows[0]["unit_type"].ToString();
                ddlformbayunittype.SelectedValue = UnitType != "" ? UnitType : "";

                txtformbayStreetNo.Text = dtDetails.Rows[0]["street_number"].ToString();
                txtformbaystreetname.Text = dtDetails.Rows[0]["street_name"].ToString();
                string StreetType = dtDetails.Rows[0]["street_type"].ToString();
                ddlformbaystreettype.SelectedValue = StreetType != "" ? StreetType : "";

                txtStreetAddress.Text = dtDetails.Rows[0]["StreetAddress"].ToString();
                ddlStreetCity.Text = dtDetails.Rows[0]["StreetCity"].ToString();
                txtStreetState.Text = dtDetails.Rows[0]["StreetState"].ToString();
                txtStreetPostCode.Text = dtDetails.Rows[0]["StreetPostCode"].ToString();

                txtContEmail.Text = dtDetails.Rows[0]["ContEmail"].ToString().Trim();

                txtContMobile.Text = dtDetails.Rows[0]["ContMobile"].ToString();
                txtCustPhone.Text = dtDetails.Rows[0]["CustPhone"].ToString();

                if (dtDetails.Rows[0]["ResCom"].ToString() == "1")
                {
                    rblResCom1.Checked = true;
                }
                if (dtDetails.Rows[0]["ResCom"].ToString() == "2")
                {
                    rblResCom2.Checked = true;
                }

                if (dtDetails.Rows[0]["Area"].ToString() != string.Empty)
                {
                    if (dtDetails.Rows[0]["Area"].ToString() == "1")
                    {
                        rblArea1.Checked = true;
                    }
                    if (dtDetails.Rows[0]["Area"].ToString() == "2")
                    {
                        rblArea2.Checked = true;
                    }
                }
                //streetaddress();
            }

            ModalPopupExtenderEdit.Show();
            BindScript();
        }

        if (e.CommandName == "AddFollowupNote")
        {
            string CustomerID = e.CommandArgument.ToString();
            hndCustomerID.Value = CustomerID;

            ResetFollowup();
            BindContact(CustomerID);

            DataTable dt_c = ClstblContacts.tblContacts_SelectByCustId(hndCustomerID.Value);
            if (dt_c.Rows.Count > 0)
            {
                grdFollowUpBind(CustomerID);
            }

            hndMode.Value = "";
            ModalPopupExtenderFollowUpNew.Show();
            BindScript();
        }

        if (e.CommandName == "AddProject")
        {
            string CustomerID = e.CommandArgument.ToString();
            hndAddProjectCustomerID.Value = CustomerID;

            //AddProject1.BindProjectByManualCustomer(CustomerID);

            ModalPopupExtenderAddProject.Show();
            BindScript();
        }
    }

    #endregion

    #region Funcation
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    #endregion

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(GridView1.PageSize);
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindScript();
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        try
        {
            ddlSearchType.SelectedValue = "1";
        }
        catch (Exception ex)
        {
            SetError1();
        }
        try
        {
            ddlSearchStatus.SelectedValue = "2";
        }
        catch (Exception ex)
        {
            SetError1();
        }

        txtContactSearch.Text = string.Empty;
        txtMobile.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtSearchStreet.Text = string.Empty;
        txtsuburb.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        txtSearchPostCode.Text = string.Empty;
        ddlSelectDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindScript();
        BindGrid(0);
    }

    #region Edit Code
    protected void ddlStreetCity_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        if (ddlStreetCity.Text != string.Empty)
        {
            //DataTable dtStreet = ClstblCustomers.tblPostCodes_SelectBy_PostCodeID(ddlStreetCity.Text);
            //if (dtStreet.Rows.Count > 0)
            //{
            //    txtStreetState.Text = dtStreet.Rows[0]["State"].ToString();
            //    txtStreetPostCode.Text = dtStreet.Rows[0]["PostCode"].ToString();
            //}
            //streetaddress();
            string[] cityarr = ddlStreetCity.Text.Split('|');
            if (cityarr.Length > 1)
            {
                ddlStreetCity.Text = cityarr[0].Trim();
                txtStreetState.Text = cityarr[1].Trim();
                txtStreetPostCode.Text = cityarr[2].Trim();
            }
            //checkExistsAddress();
        }
        BindScript();
    }
    
    protected void ResetEditForm()
    {
        txtStreetAddress.Text = string.Empty;
        txtContMr.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtformbayUnitNo.Text = string.Empty;
        ddlformbayunittype.SelectedValue = "";
        txtformbayStreetNo.Text = string.Empty;
        txtformbaystreetname.Text = string.Empty;
        ddlformbaystreettype.SelectedValue = "";
        ddlStreetCity.Text = string.Empty;
        txtStreetState.Text = string.Empty;
        txtStreetPostCode.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtCustPhone.Text = string.Empty;

        rblResCom1.Checked = false;
        rblResCom2.Checked = false;
        rblArea1.Checked = false;
        rblArea2.Checked = false;
    }

    protected static string address;
    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtStreetAddress.Text = address;
    }
    
    public void BindContact(string CustomerID)
    {
        ListItem item1 = new ListItem();
        //item1.Text = "Select";
        //item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);
        ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(CustomerID);
        ddlContact.DataMember = "Contact";
        ddlContact.DataTextField = "Contact";
        ddlContact.DataValueField = "ContactID";
        ddlContact.DataBind();
    }
    
    public void ResetFollowup()
    {
       //ddlContact.SelectedValue = "";
        ddlManager.SelectedValue = "1";
        txtNextFollowupDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        ddlreject.SelectedValue = "2";
    }

    public void grdFollowUpBind(string CustomerID)
    {
        DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(CustomerID);
        //if (dt.Rows.Count > 0)
        //{
        //    divgrdFollowUp.Visible = true;
        //    grdFollowUp.DataSource = dt;
        //    grdFollowUp.DataBind();
        //}
        //else
        //{
        //    divgrdFollowUp.Visible = false;
        //}

        if (dt.Rows.Count > 0)
        {
            divFollowUpDetails.Visible = true;
            rptFollowUpDetails.DataSource = dt;
            rptFollowUpDetails.DataBind();
            if (dt.Rows.Count > 5)
            {
                divFollowUp.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divFollowUp.Attributes.Add("style", "overflow-y: scroll");
            }

        }
        else
        {
            divFollowUpDetails.Visible = false;
        }
    }
    #endregion

    protected void ibtnUpdateDetail_Click(object sender, EventArgs e)
    {
        streetaddress();
        string ResCom = "0";
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }

        string Area = "0";
        if (rblArea1.Checked == true)
        {
            Area = "1";
        }
        if (rblArea2.Checked == true)
        {
            Area = "2";
        }

        string formbayUnitNo = txtformbayUnitNo.Text;
        string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string StreetAddress = formbayunittype + " " + formbayUnitNo + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;

        DataTable dtDetails = ClstblSalesinfo.SP_LeadTracker_tblCustomer_GetCustomerByCustomerID(hndEditCustID.Value);
        string id = hndEditCustID.Value;
        if (dtDetails.Rows.Count > 0)
        {
            // int existaddress = ClstblCustomers.tblCustomers_Exits_Address(formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, StreetCity, StreetState, StreetPostCode);
            DataTable dtaddress = ClstblCustomers.tblCustomers_Exits_Select_StreetAddressByID(id, StreetAddress, StreetCity, StreetState, StreetPostCode);
            //DataTable dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID(id, txtContMobile.Text.Trim(), txtContEmail.Text);
            //DataTable dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID2(id, txtContMobile.Text.Trim(), txtContEmail.Text);
            string mobiletxt = txtContMobile.Text.Trim();
            string emailtxt = txtContEmail.Text.Trim();
            if (dtaddress.Rows.Count == 0)
            {
                int dtMobileEmailExist = 0;
                int flag1 = 0;
                int flag2 = 0;
                string email = dtDetails.Rows[0]["ContEmail"].ToString().Trim();
                string mobile = dtDetails.Rows[0]["ContMobile"].ToString();
                if (mobiletxt != mobile)
                {
                    if (mobiletxt != "0400000000")
                        flag1 = 1;
                }
                if (emailtxt != email)
                {
                    if (emailtxt != "no-reply@arisesolar.com.au")
                        flag2 = 1;
                }
                if (flag1 == 1 || flag2 == 1)
                {
                    if (flag1 == 0)
                    { mobiletxt = "0400000000"; }
                    if (flag2 == 0)
                    { emailtxt = "no-reply@arisesolar.com.au"; }

                    dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID2(id, mobiletxt, emailtxt);
                }
                if (dtMobileEmailExist == 0)
                {
                    //Response.Write(existaddress);
                    // Response.End();
                    lbleror.Visible = false;
                    lbleror1.Visible = false;
                    string ContactID = dtDetails.Rows[0]["ContactID"].ToString();
                    bool succ = ClstblCustomers.tblCustomer_Update_Address(hndEditCustID.Value, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    bool suc = ClstblContacts.tblContacts_UpdateCustDetail(ContactID, txtContMr.Text, txtContFirst.Text, txtContLast.Text, txtContMobile.Text.Trim(), txtContEmail.Text);
                    bool add = ClstblCustomers.tblCustomers_UpdateAddress(hndEditCustID.Value, StreetAddress, ddlStreetCity.Text, txtStreetState.Text, txtStreetPostCode.Text, txtContFirst.Text + " " + txtContLast.Text);
                    bool Suc1 = ClstblCustomers.tblCustomers_UpdateInContact(hndEditCustID.Value, ResCom, txtCustPhone.Text, Area);

                    bool suc_EmailMobile = ClstblContacts.tblCustomers_Update_CustEmailMobile(hndEditCustID.Value, txtContEmail.Text.Trim(), txtContMobile.Text.Trim());

                    if (suc && succ && add && Suc1)
                    {
                        ModalPopupExtenderEdit.Hide();
                        BindGrid(0);
                        BindScript();
                        SetAdd1();
                    }
                }
                else
                {
                    lbleror1.Visible = true;
                    ModalPopupExtenderEdit.Show();
                }
            }
            else
            {
                lbleror.Visible = true;
                ModalPopupExtenderEdit.Show();
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string CustomerID = hndCustomerID.Value;
        string Description = txtDescription.Text;
        string Reject = ddlreject.SelectedValue;

        string NextFollowupDate = "";
        if (txtNextFollowupDate.Text != "")
        {
            NextFollowupDate = Convert.ToDateTime(txtNextFollowupDate.Text).AddHours(14).ToShortDateString();
        }

        string ContactID = ddlContact.SelectedValue.ToString();
        string Manager = ddlManager.SelectedValue;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;

        if (hndMode.Value != "Update")  // This Code is Add New Record
        {
            int success = ClstblCustInfo.tblCustInfo_Insert(CustomerID, Description, NextFollowupDate, ContactID, CustInfoEnteredBy, Manager);
            bool update = ClstblCustInfo.tblCustInfo_UpdateReject(success.ToString(), Reject);

            if (Convert.ToString(success) != string.Empty)
            {
                ResetFollowup();
                BindScript();
                SetAdd1();
                //grdFollowUpBind(CustomerID);
                BindGrid(0);
            }
            else
            {
                BindScript();
                grdFollowUpBind(CustomerID);
                SetError1();
                //BindGrid(0);
                ModalPopupExtenderFollowUpNew.Show();
            }
        }
        else // This Code is Update Record
        {
            string CustInfoID = hndFollowEditCustInfoID.Value;

            if(CustInfoID != "")
            {
                bool success = ClstblSalesinfo.tblCustInfo_UpdateFollowUp(ContactID, Description, NextFollowupDate, Manager, CustInfoID);
                bool update = ClstblCustInfo.tblCustInfo_UpdateReject(CustInfoID, Reject);

                if (success)
                {
                    ResetFollowup();
                    BindScript();
                    SetAdd1();
                    grdFollowUpBind(CustomerID);
                    //ModalPopupExtenderFollowUpNew.Show();
                    BindGrid(0);
                }
                else
                {
                    BindScript();
                    ResetFollowup();
                    grdFollowUpBind(CustomerID);
                    SetError1();
                    BindGrid(0);
                    //ModalPopupExtenderFollowUpNew.Show();
                }
            }
            else
            {
                SetError1();
            }
        }

        hndMode.Value = "";
    }

    protected void rptFollowUpDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string arg = e.CommandArgument.ToString();
        string [] Argument = arg.Split(';');

        string CustInfoID = Argument[0].ToString();
        string CustomerID = Argument[1].ToString();

        if (e.CommandName == "Select")
        {
            hndMode.Value = "Update";
            hndFollowEditCustInfoID.Value = CustInfoID;

            DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(CustomerID);

            if (dt.Rows[0]["ContactID"].ToString() != null && dt.Rows[0]["ContactID"].ToString() != "")
            {
                try
                {
                    ddlContact.SelectedValue = dt.Rows[0]["ContactID"].ToString();
                }
                catch(Exception ex) { }
            }

            if (dt.Rows[0]["FollowupType"].ToString() != null && dt.Rows[0]["FollowupType"].ToString() != "")
            {
                try
                {
                    ddlManager.SelectedValue = dt.Rows[0]["FollowupType"].ToString();
                }
                catch (Exception ex) { }
            }

            if (dt.Rows[0]["NextFollowupDate"].ToString() != null && dt.Rows[0]["NextFollowupDate"].ToString() != "")
            {
                try
                {
                    txtNextFollowupDate.Text = Convert.ToDateTime(dt.Rows[0]["NextFollowupDate"].ToString()).ToShortDateString();
                }
                catch (Exception ex) { }
            }

            if (dt.Rows[0]["Reject"].ToString() != null && dt.Rows[0]["Reject"].ToString() != "")
            {
                try
                {
                    ddlManager.SelectedValue = dt.Rows[0]["Reject"].ToString();
                }
                catch (Exception ex) { }
            }
            txtDescription.Text = dt.Rows[0]["Description"].ToString();

            ModalPopupExtenderFollowUpNew.Show();
        }
    }

    public void BindResult(string name)
    {
        BindGrid(0);
    }

    protected void chkreadHeader_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chksalesheader = (CheckBox)GridView1.HeaderRow.FindControl("chkreadHeader");

        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkread = (CheckBox)row.FindControl("chkread");

                if (chksalesheader.Checked == true)
                {
                    chkread.Checked = true;
                }
                else
                {
                    chkread.Checked = false;
                }
            }
        }
    }

    protected void btnNeawAssignLead_Click(object sender, EventArgs e)
    {
        string ProjectNo = "";
        string CustomerID = "";
        string EmployeeID = ddlSalesRep1.SelectedValue;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string UpdatedTime = DateTime.Now.AddHours(14).ToShortDateString();

        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkread = (CheckBox)row.FindControl("chkread");

                if (chkread.Checked == true)
                {
                    HiddenField hndProjectNo = (HiddenField)row.FindControl("hndProjectNo");
                    HiddenField hndCustomerID = (HiddenField)row.FindControl("hndCustomerID");

                    if(hndProjectNo.Value != "")
                    {
                        ProjectNo += "," + hndProjectNo.Value;
                    }

                    if (hndCustomerID.Value != "")
                    {
                        CustomerID += "," + hndCustomerID.Value;
                    }
                }
            }
        }

        if(ProjectNo != "")
        {
            ProjectNo = ProjectNo.Substring(1);
        }
        if (CustomerID != "")
        {
            CustomerID = CustomerID.Substring(1);
        }

        try
        {
            if (EmployeeID != "")
            {
                bool SucCustomerID =false;
                if (CustomerID != "")
                {
                    string[] CustID = CustomerID.Split(',');
                    for (int i = 0; i < CustID.Length; i++)
                    {
                        string Cid = CustID[i];
                        DataTable dt = ClstblSalesinfo.tblCustomer_GetDataByIDforAssignLead(Cid);

                        //string ContID = dt.Rows[0]["ContactID"].ToString();
                        //string LeadStatus = dt.Rows[0]["LeadStatus"].ToString();
                        string EmpID = dt.Rows[0]["EmployeeID"].ToString();
                        //string Leam = dt.Rows[0]["Team"].ToString();
                        //string EmadDate = dt.Rows[0]["LeadDate"].ToString();
                        string CustSource = dt.Rows[0]["CustSource"].ToString();
                        string CustSourceSub = dt.Rows[0]["CustSourceSub"].ToString();
                        //string CustType = dt.Rows[0]["CustType"].ToString();
                        string CustEntered = dt.Rows[0]["CustEntered"].ToString();
                        string RoleName = dt.Rows[0]["RoleName"].ToString();

                        int exists = ClsLead.tblLeadHistory_ExistsByCustomerID(Cid);

                        string Msg = "";
                        if (RoleName == "Sales Manager") // New Lead
                        {
                            if(exists == 0)
                            {
                                Msg = "Assign New Lead";
                                int Uns1 = ClsLead.tblLeadHistory_Insert(Cid, "New", CustSource, CustSourceSub, Msg, EmployeeID, CustEntered, userid);
                            }
                            else
                            {
                                Msg = "Transfer in from " + EmployeeID + " to " + EmpID;
                                int Uns3 = ClsLead.tblLeadHistory_Insert(Cid, "Transfer In", CustSource, CustSourceSub, Msg, EmployeeID, UpdatedTime, userid);
                            }
                        }
                        else // Transfer In / out
                        {
                            Msg = "Transfer in from " + EmployeeID + " to " + EmpID;
                            int Uns3 = ClsLead.tblLeadHistory_Insert(Cid, "Transfer In", CustSource, CustSourceSub, Msg, EmployeeID, UpdatedTime, userid);

                            if (Convert.ToDateTime(CustEntered) > Convert.ToDateTime("22/10/2020"))
                            {
                                Msg = "Transfer Out from " + EmpID + " to " + EmployeeID;
                                int Uns2 = ClsLead.tblLeadHistory_Insert(Cid, "Transfer Out", CustSource, CustSourceSub, Msg, EmpID, UpdatedTime, userid);
                            }
                        }

                        //int exists = ClstblContacts.tbl_leadData_Track_ExistsByContactID_CustomerID(Cid, ContID);

                        //if (exists == 0)
                        //{
                          //  int Uns1 = ClstblContacts.tbl_leadData_Track_Insert(Cid, ContID, LeadStatus, Team, UpdatedBy, EmpID, LeadDate, UpdatedTime, EmpID);
                        //}

                        //int Uns = 0;
                        //if (exists == 0)
                        //{
                        //    if(Convert.ToDateTime(CustEntered) < Convert.ToDateTime("19/09/2020"))
                        //    {
                        //        Uns = ClstblContacts.tbl_leadData_Track_Insert(Cid, ContID, LeadStatus, Team, UpdatedBy, EmployeeID, LeadDate, UpdatedTime, EmpID);
                        //    }
                        //    else
                        //    {
                        //        Uns = ClstblContacts.tbl_leadData_Track_Insert(Cid, ContID, LeadStatus, Team, UpdatedBy, EmployeeID, LeadDate, UpdatedTime, EmployeeID);
                        //    }
                        //}
                        //else
                        //{
                        //    Uns = ClstblContacts.tbl_leadData_Track_Insert(Cid, ContID, LeadStatus, Team, UpdatedBy, EmployeeID, LeadDate, UpdatedTime, EmpID);
                        //}

                        //if (Uns > 0)
                        //{
                        //    bool Update = ClstblSalesinfo.tbl_leadData_Track_UpdateExtra(Uns.ToString(), CustSource, CustSourceSub, CustType);
                        //}
                    }

                    SucCustomerID = ClstblSalesinfo.tblCustomer_tblContact_Update_EmployeeIDByID(CustomerID, EmployeeID);
                    //bool SucCustEmpID = ClstblSalesinfo.tbl_leadData_Track_Update_CustEmpIDByID(CustomerID, EmployeeID);
                }

                bool SucProjectNo;
                if (ProjectNo != "")
                {
                    SucProjectNo = ClstblSalesinfo.tblProjects_Update_EmployeeIDByProjectNumber(ProjectNo, EmployeeID);
                }

                if (SucCustomerID)
                {
                    BindGrid(0);
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
            }
            else
            {
                MsgError("Please Select SalesRep");
            }
        }
        catch(Exception ex)
        {
            SetError1();
        }

        ddlSalesRep1.SelectedValue = "";


        // Default
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                .Select(x => x.ColumnName)
                                .ToArray();

            string FileName = "LeadTrackerNew" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 2, 3, 4, 5, 6, 7, 8,
                              21, 11, 10, 18, 19,
                              12, 13, 14, 15, 16, 17,
                              23, 24, 25, 26, 27, 20, 30 };

            string[] arrHeader = { "Company Name", "Mobile", "Address", "Suburb", "State", "Next Follow-Up Date", "Description",
                                    "ProjectNumber", "ResponseMsg", "Lead Date", "Quote Date", "SalesRep",
                                    "PCode", "Phone", "Source", "Sub Source", "Email", "Notes",
                                    "Follow-Up Date", "ProjectOpenDate", "Team", "Cust Type", "Lead Status", "ProjectStatus", "User IP" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
            MsgError("Error while downloading Excel..");
        }
        }

    protected void BindTot(DataTable dt)
    {
        if(dt.Rows.Count > 0)
        {
            decimal avg = 100;

            decimal TotalLead = Convert.ToDecimal(dt.Rows.Count.ToString());

            string UnhandledLead = dt.Select("ContLeadStatus = 'Unhandled'").Length.ToString();
            decimal UnhandledAvg = ((Convert.ToDecimal(UnhandledLead) / Convert.ToDecimal(TotalLead)) * avg);

            string Project = dt.Select("ProjectStatusID = '2'").Length.ToString();
            decimal ProjectAvg = ((Convert.ToDecimal(Project) / Convert.ToDecimal(TotalLead)) * avg);

            string Sales = dt.Select("ProjectStatusID  IN ('3','5','8','10','19','11','12','13','14')").Length.ToString();
            decimal SalesAvg = ((Convert.ToDecimal(Sales) / Convert.ToDecimal(TotalLead)) * avg);

            string Cancel = dt.Select("ContLeadStatus = 'Cancelled'").Length.ToString();
            decimal CancelAvg = ((Convert.ToDecimal(Cancel) / Convert.ToDecimal(TotalLead)) * avg);


            lblTotal.Text = dt.Rows.Count.ToString();

            lblUnhandled.Text = UnhandledLead != "0" ? UnhandledLead + "/" + Math.Round(UnhandledAvg, 2) : "0";

            lblProject.Text = Project != "0" ? Project + "/" + Math.Round(ProjectAvg, 2) : "0";

            lblSales.Text = Sales != "0" ? Sales + "/" + Math.Round(SalesAvg, 2) : "0";

            lblCancel.Text = Cancel != "0" ? Cancel + "/" + Math.Round(CancelAvg, 2) : "0";

        }
        else
        {
            lblUnhandled.Text = "0";
            lblProject.Text = "0";
            lblSales.Text = "0";
            lblCancel.Text = "0";
            lblTotal.Text = "0";
        }
    }
}