<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="stctracker.aspx.cs" Inherits="admin_adminfiles_company_stctracker" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/projectstc.ascx" TagPrefix="uc1" TagName="projectstc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <style>
        .tooltip {
            min-width: 200px;
        }

        .braktext {
            white-space: normal !important;
        }
    </style>
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedSTC);
        function pageLoadedSTC() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });

        }

        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <script>
        function pageLoaded() {



            $(document).ready(function () {

                $('#<%=GridView1.ClientID%>').tableHeadFixer({ 'right': 1, 'foot': true, 'head': false });
            });
        }
    </script>


    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <div class="row-title">
                    <asp:Label ID="lbltesting" runat="server" Width="100%"></asp:Label>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Literal ID="litmessage" runat="server" Text="Transaction Failed."></asp:Literal></strong>
                </div>
                <%--<div class="alert alert-danger" id="Div5" runat="server" visible="false">
                   <asp:Label runat="server" ID="lbl1"></asp:Label>
                </div>--%>
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>STC Tracker</h5>
                <div id="tdExport1" class="pull-right" runat="server">
                    <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                        <asp:Button class="btn btn-palegreen" ID="Button1" runat="server" Text="QuickForm" OnClick="btnquickfrom_Click" />
                        <asp:HyperLink ID="hypNavi" CssClass="btn btn-info" runat="server" NavigateUrl="~/admin/adminfiles/upload/pvdstatus.aspx"> <i class="fa fa-chevron-circle-down"></i> Import Data</asp:HyperLink><%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
                    </ol>
                </div>
            </div>


            <asp:HiddenField ID="hdnid" runat="server" />
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <div class="row">
                                            <div class="dataTables_filter Responsive-search ">
                                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="inlineblock martop5">
                                                                <div class="col-sm-12">
                                                                    <div class="input-group col-sm-1">
                                                                        <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender16" runat="server" TargetControlID="txtContactSearch"
                                                                            WatermarkText="Contact" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender9" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                        </cc1:AutoCompleteExtender>
                                                                    </div>

                                                                    <div class="input-group col-sm-1">
                                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtProjectNumber"
                                                                            WatermarkText="Project Num" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                            ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </div>

                                                                    <div class="input-group " style="width: 155px">
                                                                        <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                                            WatermarkText="Manual Num" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    </div>

                                                                    <div class="input-group" id="divCustomer" runat="server" style="width: 200px;">
                                                                        <asp:DropDownList ID="ddlSTCCheckedBy" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                            <asp:ListItem Value="">STC Checked By</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1">
                                                                        <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                                            WatermarkText="Suburb" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                        </cc1:AutoCompleteExtender>
                                                                    </div>


                                                                    <div class="input-group" id="div1" runat="server" style="width: 120px">
                                                                        <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1">
                                                                        <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtSearchPostCode"
                                                                            WatermarkText="PCode" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                        </cc1:AutoCompleteExtender>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search"
                                                                            ControlToValidate="txtSearchPostCode" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </div>

                                                                    <div class="form-group spical multiselect" style="width: 160px">
                                                                        <dl class="dropdown">
                                                                            <dt>
                                                                                <a href="#">
                                                                                    <span class="hida" id="spanselect">Select</span>
                                                                                    <p class="multiSel"></p>
                                                                                </a>
                                                                            </dt>
                                                                            <dd id="ddproject" runat="server">
                                                                                <div class="mutliSelect" id="mutliSelect">
                                                                                    <ul>
                                                                                        <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                                            <ItemTemplate>
                                                                                                <li>
                                                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                    <%-- </span>--%>
                                                                                                    <label class="chkval">
                                                                                                        <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </div>
                                                                            </dd>
                                                                        </dl>
                                                                    </div>

                                                                    <div class="input-group " style="width: 160px">
                                                                        <asp:TextBox ID="txtSTCApprovalNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSTCApprovalNo"
                                                                            WatermarkText="STC Approval No" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSTCApprovalNo" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPVDNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                        </cc1:AutoCompleteExtender>
                                                                    </div>




                                                                    <div class="clear">
                                                                        <div class="input-group martop5 col-sm-1">
                                                                            <asp:TextBox ID="txtSTCUpload" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSTCUpload"
                                                                                WatermarkText="STC Upload #" />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                                ControlToValidate="txtSTCUpload" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                        </div>
                                                                        <div class="input-group martop5" id="div2" runat="server" style="width: 150px">
                                                                            <asp:DropDownList ID="ddlSTCStatus" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                                <asp:ListItem Value="">STC Status</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group martop5" id="div3" runat="server" style="width: 150px">
                                                                            <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                                <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                                <asp:ListItem Value="1">Install Complete</asp:ListItem>
                                                                                <asp:ListItem Value="2">STC Applied</asp:ListItem>
                                                                                <asp:ListItem Value="3">Install Date</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                        </div>

                                                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                        </div>

                                                                        <div class="input-group martop5" id="div4" runat="server" style="width: 128px">
                                                                            <asp:DropDownList ID="ddlisquickform" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                                <asp:ListItem Value="">All</asp:ListItem>
                                                                                <asp:ListItem Value="1">Manual</asp:ListItem>
                                                                                <asp:ListItem Value="2">QuickForm</asp:ListItem>
                                                                                <asp:ListItem Value="3">GreenBot</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group martop5" id="div5" runat="server" style="width: 128px">
                                                                            <asp:DropDownList ID="ddlIsPVD" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                                <asp:ListItem Value="3">PVD Y/N</asp:ListItem>
                                                                                <asp:ListItem Value="2" Selected="True">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="1">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group martop5" id="div6" runat="server" style="width: 128px">
                                                                            <asp:DropDownList ID="ddlSerialNo" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                                <asp:ListItem Value="">Serial No</asp:ListItem>
                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group martop5" runat="server" style="width: 200px">
                                                                            <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myvalstctracker">
                                                                                <asp:ListItem Value="">Installer</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group col-sm-1 martop5" style="width: 78px; display: none">

                                                                            <table class="datedpikartable checkbox-info checkbox">
                                                                                <tr>
                                                                                    <td class="btnviewallorange">


                                                                                        <label for="<%=chkNoPVD.ClientID %>" class="btn btn-magenta">
                                                                                            <asp:CheckBox ID="chkNoPVD" runat="server" Checked="true" />
                                                                                            <span class="text">No PVD</span>
                                                                                        </label>
                                                                                    </td>
                                                                                    <td></td>
                                                                                </tr>
                                                                            </table>

                                                                        </div>

                                                                        <div class="input-group martop5">

                                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                        </div>

                                                                        <div class="input-group martop5">
                                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div class="datashowbox martop5">
                                <div class="row">
                                    <div class="dataTables_length showdata col-sm-6">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="padtopzero">
                                                    <div id="tdExport" runat="server" style="padding-left: 10px">
                                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                            CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                        <%-- </ol>--%>
                                                    </div>
                                                </td>
                                                <td class="padtopzero" style="padding-left: 20px" runat="server" id="fddl">
                                                    <asp:DropDownList ID="ddlFatch" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Greenbot</asp:ListItem>
                                                        <asp:ListItem Value="2">BridgeSelect</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="padtopzero">
                                                    <div class="input-group" style="padding-left: 10px" runat="server" id="fbtn">
                                                        <asp:LinkButton ID="lbtnFatch" runat="server" OnClick="lbtnFatch_Click"
                                                            CausesValidation="false" CssClass="btn btn-primary btn-xs"><i class="fa-refresh fa"></i>Fetch</asp:LinkButton>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="animate-panel marbtmzero padbtmzero">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hpanel marbtmzero">
                                    <div>

                                        <script>
                                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                                            prm.add_pageLoaded(pageLoaded);
                                            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                                            prm.add_beginRequest(beginrequesthandler);
                                            // raised after an asynchronous postback is finished and control has been returned to the browser.
                                            prm.add_endRequest(endrequesthandler);
                                            function beginrequesthandler(sender, args) {
                                                //shows the modal popup - the update progress
                                                $('.loading-container').css('display', 'block');
                                            }
                                            function endrequesthandler(sender, args) {
                                                //hide the modal popup - the update progress

                                                $('.loading-container').css('display', 'none');

                                                $(".dropdown dt a").on('click', function () {
                                                    $(".dropdown dd ul").slideToggle('fast');
                                                });

                                                $(".dropdown dd ul li a").on('click', function () {
                                                    $(".dropdown dd ul").hide();
                                                });


                                                $(document).bind('click', function (e) {
                                                    var $clicked = $(e.target);
                                                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                                                });

                                            }
                                            function pageLoaded() {
                                                //alert($(".search-select").attr("class"));
                                                $('.loading-container').css('display', 'none');
                                                $('.datetimepicker1').datetimepicker({
                                                    format: 'DD/MM/YYYY'
                                                });

                                                $(".myvalstctracker").select2({
                                                    //placeholder: "select",
                                                    allowclear: true
                                                });
                                                $(".myval1").select2({
                                                    minimumResultsForSearch: -1
                                                });
                                                $('.mutliSelect input[type="checkbox"]').on('click', function () {
                                                    callMultiCheckbox();
                                                });

                                                if ($(".tooltips").length) {
                                                    $('.tooltips').tooltip();
                                                }

                                                callMultiCheckbox();
                                            }
                                        </script>
                                        <%--     <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                                            <ProgressTemplate>
                                                <div class="splash">
        <div class="color-line"></div>
        <div class="splash-title">
            
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                        <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                                            PopupControlID="updateprogress1" BackgroundCssClass="modalPopup" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                </asp:Panel>
            </div>
            <asp:Panel ID="panelHeader" runat="server" CssClass="marbtm15">
                <div class="widget-body shadownone brdrgray">
                    <div class="panel-body padallzero">
                        <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage col-sm-12">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td class="paddingleftright10" style="font-size: medium">
                                                <b>STCNumber:&nbsp;</b><asp:Literal ID="lblstcvalue" runat="server"></asp:Literal>
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td colspan="4" class="paddingleftright10" style="font-size: medium">
                                                <b>Total Number of panels:&nbsp;</b><asp:Literal ID="lbltotalpanel" runat="server"></asp:Literal>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%">
                            <tr style="display: flex; flex-wrap: wrap; padding-top: 10px">
                                <asp:Repeater runat="server" ID="rptHeaderStatus">
                                    <ItemTemplate>
                                        <td class="paddingleftright10" style="font-size: medium">
                                            <b><%# Eval("PVDStatus") %>:&nbsp;</b><%# Eval("Total") + "/" + Eval("STC") %>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        
                                    </ItemTemplate>
                                </asp:Repeater>

                            </tr>
                        </table>

                    </div>
                </div>
            </asp:Panel>


            <div id="PanGrid" runat="server">
                <asp:Panel ID="panel1" runat="server" CssClass="hpanel padtopzero xscroll finalgrid">
                    <div class="xscroll">
                        <div class="xscroll">
                            <div class="xscroll">
                                <div class="table-responsive xscroll">
                                    <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                        OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_RowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="ProjectNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProjectNumber" runat="server" Width="120px" Style="display: initial">
                                                        <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'> <%#Eval("ProjectNumber")%></asp:HyperLink>&nbsp;
                                                          <asp:HiddenField Value='<%#Eval("ProjectID") %>' runat="server" ID="hdnProjectID" />
                                                        <asp:HiddenField Value='<%#Eval("CustomerID") %>' runat="server" ID="hdncust" />
                                                        <asp:LinkButton ID="lnkjob" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="STC" OnClick="lnkjob_Click" CssClass="btn btn-azure btn-xs"><i class="fa fa-newspaper-o"></i> STC </asp:LinkButton>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Manual#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="ManualQuoteNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblManualQuoteNumber" runat="server" Width="80px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="SerialNo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="SerialNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSerialNo" runat="server"><%#Eval("SerialYN")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ProjectType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="ProjectType">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProjectType" runat="server"><%#Eval("ProjectType")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Quote" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left"
                                                SortExpression="ProjectOpened">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProjectOpened" runat="server" Width="90px"><%# DataBinder.Eval(Container.DataItem, "ProjectOpened", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="InstallDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left"
                                                SortExpression="InstallBookingDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallBookingDate" runat="server" Width="90px"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Address"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Address")%>'
                                                        Width="200px"><%#Eval("Address")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallPostCode" runat="server" Width="50px"><%#Eval("InstallPostCode")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Install Completed" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InstallCompleted">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallCompleted" runat="server" Width="90px"><%# DataBinder.Eval(Container.DataItem, "InstallCompleted", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="STC Saved" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStCYes" runat="server" Width="70px" Text="Yes" Visible='<%#Eval("STCFormSaved").ToString()=="True"?true:false %>'></asp:Label>
                                                            <asp:Label ID="lblStCNo" runat="server" Width="70px" Text="No" Visible='<%#Eval("STCFormSaved").ToString()=="False"?true:false %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cert." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCertYes" runat="server" Width="20px" Text="Yes" Visible='<%#Eval("CertificateSaved").ToString()=="True"?true:false %>'></asp:Label>
                                                            <asp:Label ID="lblCertNo" runat="server" Width="20px" Text="No" Visible='<%#Eval("CertificateSaved").ToString()=="False"?true:false %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Docs" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocsCheck" runat="server" Width="20px">
                                                                <asp:LinkButton ID="lbtnDocsCheck" runat="server" CommandName="DocsCheck" CommandArgument='<%#Eval("ProjectID") %>' data-original-title="Check" data-toggle="tooltip" data-placement="top"
                                                                    Visible='<%#Eval("DocsDone").ToString()=="False"?true:false %>' OnClientClick="return confirm('Are you sure you want to mark as checked? ');"> <img src="../../../images/icon_check.png" /></asp:LinkButton>
                                                            </asp:Label>
                                                            <asp:Label ID="lblDocsUncheck" runat="server" Width="20px">
                                                                <asp:LinkButton ID="lbtnDocsUncheck" runat="server" CommandName="DocsUncheck" CommandArgument='<%#Eval("ProjectID") %>' data-original-title="UnCheck" data-toggle="tooltip" data-placement="top"
                                                                    Visible='<%#Eval("DocsDone").ToString()=="True"?true:false %>' OnClientClick="return confirm('Are you sure you want to mark as unchecked? ');"> <img src="../../../images/icon_uncheck.png" /></asp:LinkButton>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="No Docs" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNoCheck" runat="server" Width="30px">
                                                                <asp:LinkButton ID="lbtnNoCheck" runat="server" CommandName="NoCheck" CommandArgument='<%#Eval("ProjectID") %>' data-original-title="Check" data-toggle="tooltip" data-placement="top"
                                                                    Visible='<%#Eval("NoDocs").ToString()=="False"?true:false %>' OnClientClick="return confirm('Are you sure you want to mark as checked? ');"> <img src="../../../images/icon_check.png" /></asp:LinkButton>
                                                            </asp:Label>
                                                            <asp:Label ID="lblNoUncheck" runat="server" Width="30px">
                                                                <asp:LinkButton ID="lbtnNoUncheck" runat="server" CommandName="NoUncheck" CommandArgument='<%#Eval("ProjectID") %>' data-original-title="UnCheck" data-toggle="tooltip" data-placement="top"
                                                                    Visible='<%#Eval("NoDocs").ToString()=="True"?true:false %>' OnClientClick="return confirm('Are you sure you want to mark as unchecked? ');"> <img src="../../../images/icon_uncheck.png" /></asp:LinkButton>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="STC U/L" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center"
                                                        SortExpression="STCUploaded">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSTCUploaded" runat="server" Width="90px"><%# DataBinder.Eval(Container.DataItem, "STCUploaded", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="STC App Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="STCApplied">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSTCApplied" runat="server" Width="90px"> <%# DataBinder.Eval(Container.DataItem, "STCApplied", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="U/L Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSTCUploadNumber" runat="server" Width="100px"><%#Eval("STCUploadNumber")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="PVD No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="PVDNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVDNumber" runat="server" Width="100px"><%#Eval("PVDNumber")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PVD Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="PVDStatus">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPVDStatus" runat="server" Width="100px"><%#Eval("PVDStatus")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ProjectStatus"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProjectStatus" runat="server" Width="120px"><%#Eval("ProjectStatus")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="KW" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="SystemCapKW"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSystemCapKW" runat="server" Width="60px"> <%#Eval("SystemCapKW")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="STC #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="STCNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSTCNumber" runat="server" Width="60px"> <%#Eval("STCNumber")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Installer Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InstallerName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallerName" runat="server" Width="60px"> <%#Eval("InstallerName")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="STC Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="STCNotes">
                                                <ItemTemplate>
                                                    <span id="spnfollowup" runat="server" data-toggle="tooltip" data-placement="top">
                                                        <asp:Label ID="lblStcNotes" runat="server" Width="300px" data-toggle="tooltip"> <%#Eval("STCNotes")%></asp:Label>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="STC Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="STCNotes"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <span id="spnfollowup" runat="server" data-toggle="tooltip" data-placement="top">
                                                        <%--<asp:Label ID="lblFollowUpNote" runat="server" data-original-title='<%# Eval("Description").ToString() %>' data-toggle="tooltip" data-placement="top"
                                                                Width="100px"><%# Eval("Description")%></asp:Label>--%>
                                                        <%--<asp:Label ID="lblStcNotes" runat="server" Width="300px" data-toggle="tooltip"> <%#Eval("STCNotes")%></asp:Label>--%>
                                                        <asp:Label ID="lblAddress1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("STCNotes")%>'
                                                            Width="200px"><%#Eval("STCNotes")%></asp:Label>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--      <asp:TemplateField HeaderText="Foll-Up" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="FollowUp">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFollowUp" runat="server" Width="90px"><%# DataBinder.Eval(Container.DataItem, "FollowUp", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="STC Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSTCNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("STCNotes")%>'
                                                                Width="150px"> <%#Eval("STCNotes")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                        </Columns>
                                        <AlternatingRowStyle />

                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1 xscroll" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>


            <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData1">
            </cc1:ModalPopupExtender>
            <div runat="server" id="div_popup" style="display: none;" class="modal_popup">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="color-line"></div>

                        <div class="modal-header text-center">
                            <button id="ibtnCancelStatus" runat="server" type="button" class="close width40" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="H1">
                                <asp:Label runat="server" ID="lbltitle" Text="STC"></asp:Label>
                            </h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">

                                <div class="tablescrolldiv">
                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div>
                                                    <div class="widget flat radius-bordered borderone">
                                                        <div class="widget-header bordered-bottom bordered-blue">
                                                            <span class="widget-caption">STC Status</span>
                                                        </div>
                                                        <div class="widget-body">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <span class="name">
                                                                            <asp:Label ID="Label2" runat="server" class="  control-label">
                                               STC Upload Date</asp:Label></span>
                                                                        <div class="input-group date datetimepicker1 ">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtSTCUploaded" runat="server" class="form-control" Width="200px">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <%--   <div class="form-group dateimgarea">
                                                <span class="name">
                                                    <label class="control-label">
                                                        
                                                    </label>
                                                </span><span class="dateimg">
                                                    <asp:TextBox ID="txtSTCUploaded" runat="server" Width="150px" CssClass="form-control"></asp:TextBox>
                                                    <asp:ImageButton ID="Image27" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                    <cc1:CalendarExtender ID="CalendarExtender27" runat="server" PopupButtonID="Image27"
                                                        TargetControlID="txtSTCUploaded" Format="dd/MM/yyyy">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator ValidationGroup="stc" ControlToValidate="txtSTCUploaded" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>--%>

                                                                    <div class="form-group">
                                                                        <span class="name">
                                                                            <asp:Label ID="Label1" runat="server" class="  control-label">
                                            STC Applied Date</asp:Label></span>
                                                                        <div class="input-group date datetimepicker1 ">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtSTCApplied" runat="server" class="form-control" Width="200px">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>



                                                                    <%-- <div class="form-group dateimgarea">
                                                <span class="name">
                                                    <label class="control-label">
                                                       
                                                    </label>
                                                </span><span class="dateimg">
                                                    <asp:TextBox ID="txtSTCApplied" runat="server" Width="150px" CssClass="form-control"></asp:TextBox>
                                                    <asp:ImageButton ID="Image28" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                    <cc1:CalendarExtender ID="CalendarExtender28" runat="server" PopupButtonID="Image28"
                                                        TargetControlID="txtSTCApplied" Format="dd/MM/yyyy">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator ValidationGroup="stc" ControlToValidate="txtSTCApplied" ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter valid date"
                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>--%>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <span class="name">
                                                                            <label class="control-label">
                                                                                STC Upload Number
                                                                            </label>
                                                                        </span><span style="width: 200px;">
                                                                            <asp:TextBox ID="txtSTCUploadNumber" runat="server" CssClass="form-control" MaxLength="10" Width="240px"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server" ValidationGroup="stc"
                                                                                ControlToValidate="txtSTCUploadNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                        </span>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <span class="name">
                                                                            <label class="control-label">
                                                                                PVD Number
                                                                            </label>
                                                                        </span><span style="width: 200px;">
                                                                            <asp:TextBox ID="txtPVDNumber" runat="server" CssClass="form-control" Width="240px"></asp:TextBox>
                                                                        </span>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group" style="width: 240px;">
                                                                        <span class="name disblock">
                                                                            <label class="control-label">
                                                                                PVD Status
                                                                            </label>
                                                                        </span><span style="width: 300px;">
                                                                            <asp:DropDownList ID="ddlPVDStatusID" runat="server" AppendDataBoundItems="true" Width="420px"
                                                                                aria-controls="DataTables_Table_0" class="myval form-control">
                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </span>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="width: 240px;">
                                                                        <span class="name disblock">
                                                                            <label class="control-label">
                                                                                QuickForm ID
                                                                            </label>
                                                                        </span><span style="width: 200px;">
                                                                            <asp:TextBox ID="txtformbayid" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </span>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group selpen">
                                                                        <span class="name">
                                                                            <label class="control-label">
                                                                                STC Notes:
                                                                            </label>
                                                                        </span><span>
                                                                            <asp:TextBox ID="txtSTCNotes" CssClass="form-control" runat="server" Width="100%" Height="78px" TextMode="MultiLine"></asp:TextBox>
                                                                        </span>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12 textcenterbutton center-text form-group">
                                                            <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateSTC" runat="server" OnClick="btnUpdateSTC_Click"
                                                                Text="Save" CausesValidation="true" ValidationGroup="stc" />
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <%--   <asp:PostBackTrigger ControlID="Button1" />--%>
            <%-- <asp:PostBackTrigger ControlID="projectstc" />--%>
        </Triggers>
    </asp:UpdatePanel>

    <div class="loaderPopUP">
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
            }

            function pageLoadedpro() {

                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $("[data-toggle=tooltip]").tooltip();

            }
        </script>
    </div>

    <script type="text/javascript">
        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');
        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });


        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
        $(document).ready(function () {

            doMyAction();
        });
        function doMyAction() {

            $(".myvalstctracker").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        }


        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }
    </script>


</asp:Content>




