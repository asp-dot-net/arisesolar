﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="mtcecalldetail.aspx.cs" Inherits="admin_adminfiles_company_mtcecalldetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">

            <div class="finalheader">
                <div class="small-header transition animated fadeIn">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div id="hbreadcrumb" class="pull-right printorder">
                                <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" OnClick="lnkBack_Click" CssClass="btn btn-info"><i class="fa fa-chevron-left"></i>Back</asp:LinkButton>
                            </div>

                            <h2 class="font-light m-b-xs printorder">Maintenance Detail
                            </h2>

                        </div>
                    </div>
                </div>
            </div>

            <%--  <h3 class="m-b-xs text-black printorder">Maintenance Detail</h3>--%><%--
            <div class="contactsarea">
                <div class="addcontent printpage printorder pull-right">
                </div>
                <div class="bodymianbg">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="searcharea paddtopnone printpage">
                                <div class="clear">
                                </div>
                            </div>
                            <div class="panel panel-default " id="PanGrid" runat="server">
                                <div class="table-responsive" style="margin-bottom: 60px">
                                    <asp:Literal ID="liDetail" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--%>
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea ">
            <div class="contactsarea padd_all_set">
                <div class="contacttoparea">
                    <div class="form-inline">
                        <div class="paddall15 searchfinal Responsive-search" style="margin-bottom: 25px;">
                            <div class="row">
                                <div class="col-md-2">
                                    <table border="0" cellspacing="0" cellpadding="0" class="showdata">
                                        <tr>
                                            <td>


                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>

                                            </td>

                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-10">
                                    <div class="addcontent" id="divrightbtn" runat="server" style="display: inline-block; float: right;">

                                        <asp:LinkButton ID="lnkmaintainance" runat="server" OnClick="lnkmaintainance_Click" CssClass="martopzero" CausesValidation="false">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/admin/images/btn_maintainance.jpg" />
                                        </asp:LinkButton>

                                        <%--<asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" class="addcontact" CssClass="btn btn-default purple" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i>Add</asp:LinkButton>--%>
                                        <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" OnClick="lnkBack_Click" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                    <div class="widget flat radius-bordered borderone">
                        <div class="widget-header bordered-bottom bordered-blue">
                            <span class="widget-caption">Maintenance Call Details</span>
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group wdth230">
                                        <span class="name">
                                            <asp:Label ID="Label23" runat="server" class="control-label">
                                              Call Date</asp:Label></span>
                                        <div class="input-group date datetimepicker1">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                             <asp:TextBox ID="txtOpenDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                            <%-- <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator15" Controltovalidate="txtOpenDate" Display="Dynamic" errormessage="" ValidationGroup="req" />--%>
                                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage=""
                                                ControlToValidate="txtOpenDate" Display="Dynamic" ValidationGroup="req"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>

                                    <%--   <div class="form-group dateimgarea">
                                        <span class="name">
                                            <label class="control-label">Call Date </label>
                                        </span><span class="dateimg">
                                            <asp:TextBox ID="txtOpenDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                            <asp:ImageButton ID="Image3" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image3"
                                                TargetControlID="txtOpenDate" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                            <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtOpenDate" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                        </span>
                                        <div class="clear"></div>
                                    </div>--%>

                                    <div class="form-group">
                                        <span class="name">
                                            <asp:Label ID="Label19" runat="server" class="name disblock control-label">
                                                Call Reason:</asp:Label></span>
                                        <div class="firstselect row">
                                            <div class="col-md-6">
                                                <div class="wdth230">
                                                    <asp:DropDownList ID="ddlProjectMtceReasonID" Enabled="false" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalMTCE">
                                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                    </asp:DropDownList>
                                                   
                                                </div>
                                            </div>




                                            <div class="col-md-6">
                                                <div class="wdth230">
                                                    <asp:DropDownList ID="ddlProjectMtceReasonSubID" Enabled="false" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalMTCE">
                                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlProjectMtceReasonSubID" Display="Dynamic" ValidationGroup="req"></asp:RequiredFieldValidator>
                                                    
                                                </div>
                                            </div>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="* Required" CssClass="reqerror"
                                                            ControlToValidate="ddlLocation" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>

                                    <%--<div class="form-group row">
                                                <span class="name disblock col-md-12">
                                                    <label class="control-label">Call Reason: </label>
                                                </span><span><span class="firstselect col-md-6">
                                                    <asp:DropDownList ID="ddlProjectMtceReasonID" runat="server" CssClass="form-control search-select"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select </asp:ListItem>
                                                    </asp:DropDownList>
                                                </span><span class="secondselect col-md-6">
                                                    <asp:DropDownList ID="ddlProjectMtceReasonSubID" runat="server" CssClass="form-control search-select"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                </span></span>
                                                <div class="clear"></div>
                                            </div>--%>
                                    <div class="row">
                                        <div class="form-group spicalcontact col-md-6">
                                            <span class="name">
                                                <label class="control-label">Customer: </label>
                                            </span>
                                            <span class="paddtop5 valuebox wdth230">
                                                <asp:Label ID="lblCustomer" Width="200px" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicalcontact col-md-6">
                                            <span class="name">
                                                <label class="control-label">Project : </label>
                                            </span><span class="paddtop5 valuebox wdth230">
                                                <asp:Label ID="lblProject" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group dateimgarea checkboxmain3">
                                        <br />
                                        <span class="name paddtop9none">
                                            <label for="<%=chkWarranty.ClientID %>">
                                                <asp:CheckBox ID="chkWarranty" runat="server"/>
                                                <span class="text">&nbsp;Warranty</span>
                                            </label>
                                        </span>
                                        <div class="clear"></div>
                                    </div>


                                    <div class="form-group wdth230" style="padding-top: 12px;">
                                        <span class="name">
                                            <label class="control-label">Employee </label>
                                        </span><span>
                                              
                                            <asp:DropDownList ID="ddlEmployee" Enabled="false" runat="server" Width="200px" AppendDataBoundItems="true"
                                                CssClass="myvalMTCE">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <span class="name">
                                                <label class="control-label">Contact : </label>
                                            </span><span class="paddtop5 valuebox wdth230">
                                                <asp:Label ID="lblContact" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicalcontact col-md-6">
                                            <span class="name">
                                                <label class="control-label">Mobile </label>
                                            </span><span class="paddtop5 valuebox wdth230">
                                                <asp:Label ID="lblMobile" runat="server"></asp:Label>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group  ">

                                        <span class="name">
                                            <label class="control-label">New</label>
                                        </span><span>
                                            <asp:TextBox runat="server" ID="txtCurrentPhoneContact" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget flat radius-bordered borderone">
                        <div class="widget-header bordered-bottom bordered-blue">
                            <span class="widget-caption">Installation Details</span>
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-md-3">

                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">Sales Rep </label>
                                        </span><span class="paddtop9 valuebox wdth230">
                                            <asp:Label ID="lblSalesRep" runat="server"></asp:Label>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">Install Date </label>
                                        </span><span class="paddtop9 valuebox wdth230">
                                            <asp:Label ID="lblInstallDate" runat="server"></asp:Label>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">Installer </label>
                                        </span><span class="paddtop9 valuebox wdth230">
                                            <asp:Label ID="lblInstaller" runat="server"></asp:Label>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">Meter Elec </label>
                                        </span><span class="paddtop9 valuebox wdth230">
                                            <asp:Label ID="lblMeterElec" runat="server"></asp:Label>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">System </label>
                                        </span><span class="paddtop9 valuebox">
                                            <asp:Label ID="lblSystem" runat="server"></asp:Label>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget flat radius-bordered borderone">
                        <div class="widget-header bordered-bottom bordered-blue">
                            <span class="widget-caption">Work Details</span>
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">Customer's Description of Work to be Done </label>
                                        </span><span class="fullspan">
                                            <asp:TextBox ID="txtCustomerInput" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">Description of Fault Identified </label>
                                        </span><span class="fullspan">
                                            <asp:TextBox ID="txtFaultIdentified" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">Description of Actions Required to Fix </label>
                                        </span><span class="fullspan">
                                            <asp:TextBox ID="txtActionRequired" runat="server" CssClass="form-control pull-left" Width="405px" Enabled="false"></asp:TextBox>
                                            <asp:Button ID="btnCopy" class="btn btn-dark-grey marleft10" runat="server" CausesValidation="false" OnClick="btnCopy_Click" Text="Copy" Visible="false"/>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group ">
                                        <span class="name">
                                            <label class="control-label">Tech Assigned to this Job </label>
                                        </span><span class="fullhaft">
                                            <asp:DropDownList ID="ddlInstallerAssigned" runat="server" CssClass="myvalMTCE"
                                                AppendDataBoundItems="true" Enabled="false">
                                                <asp:ListItem Value="">Select </asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <span class="name">
                                            <label class="control-label">Call Method </label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlProjectMtceCallID" runat="server" CssClass="myvalMTCE"
                                                AppendDataBoundItems="true" Enabled="false">
                                                <asp:ListItem Value=""> Select</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" style="width: 230px">
                                        <span class="name">
                                            <asp:Label ID="Label1" runat="server" class="name disblock  control-label">
                                                Prop Fix Date</asp:Label></span>
                                        <div class="input-group date datetimepicker1">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtPropResolutionDate" runat="server" class="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>




                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="width: 230px">
                                        <span class="name">
                                            <asp:Label ID="Label2" runat="server" class="name disblock  control-label">
                                               Fixed</asp:Label></span>
                                        <div class="input-group date datetimepicker1">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtCompletionDate" runat="server" class="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="display: none">
                                    <div class="form-group" style="width: 240px">
                                        <span class="name">
                                            <label class="control-label">Call Status </label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlProjectMtceStatusID" runat="server" CssClass="myvalMTCE"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value=""> Select</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" style="width: 245px">
                                        <span class="name disblock">
                                            <label class="control-label">Mtce Cost </label>
                                        </span><span class="dollarsign">
                                            <asp:TextBox ID="txtMtceCost" Enabled="false" runat="server" MaxLength="20" CssClass="form-control floatleft" Style="width: 230px"></asp:TextBox>
                                            <span class="doller">$</span>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                ControlToValidate="txtMtceCost" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group marginspicleftright" style="width: 245px">
                                        <span class="name disblock">
                                            <label class="control-label">Discount </label>
                                        </span><span class="dollarsign">
                                            <asp:TextBox ID="txtMtceDiscount" Enabled="false" runat="server" AutoPostBack="true" CssClass="form-control floatleft" MaxLength="15" OnTextChanged="txtMtceDiscount_TextChanged" Style="width: 230px"></asp:TextBox>
                                            <span class="doller">$</span>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMtceDiscount"
                                                Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="display: none">
                                    <div class="form-group " style="width: 240px">
                                        <span class="name">
                                            <label class="control-label">Paid By </label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlFPTransTypeID" runat="server" CssClass="myvalMTCE"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="">Select </asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group" style="width: 245px">
                                        <span class="name disblock">
                                            <label class="control-label">Service Cost </label>
                                        </span><span class="dollarsign">
                                            <asp:TextBox ID="txtServiceCost" Enabled="false" runat="server" MaxLength="20" CssClass="form-control floatleft" Style="width: 230px"></asp:TextBox>
                                            <span class="doller">$</span>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ControlToValidate="txtServiceCost" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="width: 245px">
                                        <span class="name disblock">
                                            <label class="control-label">Balance </label>
                                        </span><span class="dollarsign">
                                            <asp:TextBox ID="txtMtceBalance"  runat="server" MaxLength="15" Enabled="false" CssClass="form-control floatleft" Style="width: 230px"></asp:TextBox>
                                            <span class="doller">$</span>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="display: none">
                                    <div class="form-group" style="width: 240px">
                                        <span class="name">
                                            <label class="control-label">Rec By </label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlMtceRecBy" runat="server" CssClass="myvalMTCE"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="">Select </asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Work Done</h5>
                                    <div class="form-group">
                                        <span class="fullspan">
                                            <asp:TextBox ID="txtWorkDone" Enabled="false" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <%--<div class="row">
                                    <div class="col-md-12 textcenterbutton marbottom20 center-text">
                                        <div class="col-md-12">
                                            <asp:Button class="btn btn-primary addwhiteicon redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="projmtce"
                                                Text="Add" CausesValidation="true" />
                                            <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                Text="Save" CausesValidation="true" ValidationGroup="req" Visible="true" />
                                            <asp:Button class="btn btn-purple resetbutton btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                CausesValidation="false" Text="Reset" />
                                            <asp:Button class="btn btn-dark-grey calcelwhiteicon btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                CausesValidation="false" Text="Cancel" />
                                        </div>
                                    </div>
                                </div>--%>
                        </div>
                    </div>


                </asp:Panel>
            </div>
        </div>
    </section>
</asp:Content>

