﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalesInfo.aspx.cs" Inherits="admin_adminfiles_company_SalesInfo"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB" Async="true" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanelGrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                }
            </script>

            <style type="text/css">
                .selected_row {
                    background-color: #A1DCF2 !important;
                }

                .sbtn {
                    background-color: #53a93f !important;
                    border-color: #53a93f !important;
                    color: #fff;
                }

                .dnone {
                    display: none !important;
                }

                /*.widget{
                    margin:10px!important;
                }*/
            </style>
            <script src="../../assets/js/jquery.min.js"></script>

            <script type="text/javascript">
                $(function () {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                });


            </script>
            <style>
                .ui-autocomplete-loading {
                    background: white url("../../../images/indicator.gif") right center no-repeat;
                }
            </style>
            <script>

                function formValidate() {

                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
            </script>

            <script type="text/javascript">


                function InstallAutopostback() {

                    $("#<%=txtInstallAddressline.ClientID %>").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: "address.aspx",
                                dataType: "jsonp",
                                data: {
                                    s: 'auto',
                                    term: request.term
                                },
                                success: function (data) {
                                    response(data);
                                }
                            });
                        },
                        minLength: 3,
                        select: function (event, ui) {
                            //                    log(ui.item ?
                            //					"Selected: " + ui.item.label :
                            //					"Nothing selected, input was " + this.value);
                        },
                        open: function () {
                            //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                        },
                        close: function () {
                            // $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                        }
                    });
                }

                function getDetailInstallParsedAddress() {
                    $.ajax({
                        url: "address.aspx",
                        dataType: "jsonp",
                        data: {
                            s: 'address',
                            term: $("#<%=txtInstallAddressline.ClientID %>").val()
                        },
                        success: function (data) {
                            if (data != null) {
                                document.getElementById("<%= txtInstallAddress.ClientID %>").value = data.StreetLine;
                                document.getElementById("<%= txtInstallPostCode.ClientID %>").value = data.Postcode;
                                document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                                document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                                document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                                document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                                document.getElementById("<%= txtInstallCity.ClientID %>").value = data.Suburb;
                                document.getElementById("<%= txtInstallState.ClientID %>").value = data.State;
                                document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                                document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                            }
                            validateAddress();
                        }
                    });
                }

                function validateProjectAddress() {
                    $.ajax({
                        url: "address.aspx",
                        dataType: "jsonp",
                        data: {
                            s: 'validate',
                            term: $("#<%=txtInstallAddressline.ClientID %>").val()
                        },
                        success: function (data) {
                            if (data == true) {
                                document.getElementById("validaddressid").style.display = "block";
                                document.getElementById("invalidaddressid").style.display = "none";

                                document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                            }
                            else {
                                document.getElementById("validaddressid").style.display = "none";
                                document.getElementById("invalidaddressid").style.display = "block";
                                document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                            }
                        }
                    });
                }
                function ChkFun(source, args) {
                    validateProjectAddress();
                    var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

                    if (elem == '1') {
                        args.IsValid = true;
                    }
                    else {

                        args.IsValid = false;
                    }
                }
            </script>
            <script>
                function checkTextAreaMaxLength(textBox, e, length) {

                    var mLen = textBox["MaxLength"];
                    if (null == mLen)
                        mLen = length;

                    var maxLength = parseInt(mLen);
                    if (!checkSpecialKeys(e)) {
                        if (textBox.value.length > maxLength - 1) {
                            if (window.event)//IE
                                e.returnValue = false;
                            else//Firefox
                                e.preventDefault();
                        }
                    }
                }
                function checkSpecialKeys(e) {
                    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                        return false;
                    else
                        return true;
                }


            </script>

            <script type="text/javascript">
                //Function to disable validator
                function disableValidator() {
                    var valOldSysteDetails = document.getElementById("<%=RequiredFieldValidatorOldSystemDetails.ClientID%>");
                    // var e = document.getElementById(ddlProjectTypeID);
                    var e = document.getElementById('<%= ddlProjectTypeID.ClientID%>');
                    var selval = e.options[e.selectedIndex].value;
                    if (selval == "5" || selval == "3") {
                        ValidatorEnable(valOldSysteDetails, true);
                    }
                    else {
                        ValidatorEnable(valOldSysteDetails, false
                        );
                    }
                }

                function disableValidatorRemoveOldSystem() {
                    //var RqOldRemoveOldSystem = document.getElementById("<%=RqOldRemoveOldSystem.ClientID%>");

                    var ddlOldPanel = document.getElementById('<%= ddlOldPanel.ClientID%>');
                    var txtOldPanelManual = document.getElementById('<%= txtOldPanelManual.ClientID%>');

                    var RqOldPanelManual = document.getElementById('<%= RqOldPanelManual.ClientID%>');
                    var RqOldPanel = document.getElementById('<%= RqOldPanel.ClientID%>');

                    var selval = ddlOldPanel.options[ddlOldPanel.selectedIndex].value;
                    var txtval = txtOldPanelManual.value;

                    if (selval != "") {
                        ValidatorEnable(RqOldPanelManual, false);
                    }
                    else {
                        ValidatorEnable(RqOldPanelManual, true);
                    }
                    if (txtval != "") {
                        ValidatorEnable(RqOldPanel, false);
                    }
                    else {
                        ValidatorEnable(RqOldPanel, true);
                    }
                }

                function disableValidatorNoOfPanels() {

                    var txtOldNumberOfPanels = document.getElementById('<%= txtOldNumberOfPanels.ClientID%>');
                    var txtOldPanelManual = document.getElementById('<%= txtOldNumberOfPanelsManual.ClientID%>');

                    var RqtxtOldNumberOfPanels = document.getElementById('<%= RqtxtOldNumberOfPanels.ClientID%>');
                    var RqtxtOldNumberOfPanelsManual = document.getElementById('<%= RqtxtOldNumberOfPanelsManual.ClientID%>');

                    var txtval = txtOldNumberOfPanels.value;
                    var txtval1 = txtOldPanelManual.value;

                    if (txtval != "") {
                        ValidatorEnable(RqtxtOldNumberOfPanelsManual, false);
                    }
                    else {
                        ValidatorEnable(RqtxtOldNumberOfPanelsManual, true);
                    }
                    if (txtval1 != "") {
                        ValidatorEnable(RqtxtOldNumberOfPanels, false);
                    }
                    else {
                        ValidatorEnable(RqtxtOldNumberOfPanels, true);
                    }
                }
            </script>

            <%--Inverter Panels Bind Code By Suresh--%>
            <script type="text/javascript">

                //Function - Panels Dropdown Changes
                function PanelsChanges() {
                    $.ajax({
                        type: "Get",
                        url: "AjaxCallMathods.aspx/StockData",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",

                        success: function (data) {
                            alert("Success")
                        }
                        , error: function (error) {
                            alert("error")
                        }
                    });
                }

                function validate() {
                    var extension = $("#<%=fuDocument.ClientID%>").val().split('.').pop().toLowerCase();
                    var ddlDocument = $("#<%=ddlDocument.ClientID%>").val();
                    var errorMsg = $("#errorMsg");

                    if (ddlDocument == "1") {
                        if ($.inArray(extension, ['pdf', 'PDF']) == -1) {
                            errorMsg.html("pdf only..")
                            $("#<%=fuDocument.ClientID%>").val("");
                            return false;
                        }
                        else {
                            errorMsg.html("")
                        }
                    }
                    else {
                        errorMsg.html("")
                    }
                }
            </script>




            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Customer
                    <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:ImageButton ID="btnCheckActive" runat="server" ImageUrl="~/images/btn_check_active2.png"
                            CausesValidation="false" CssClass="btnimagecheckactivity margintopminus" OnClick="btnCheckActive_Click" />

                    </ol>
                </div>
            </div>

            <div class="page-body">

                <div class="messesgarea">
                    <div class="alert alert-success" id="PanSuccess" runat="server"><i class="icon-ok-sign"></i>&nbsp;Transaction Successful! </div>
                    <div class="alert alert-danger" id="PanError" runat="server">
                        <i class="icon-remove-sign"></i>&nbsp;
                <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                    </div>
                    <div class="alert alert-danger" id="PanAlreadExists" runat="server"><i class="icon-remove-sign"></i>&nbsp;Record with this name already exists. </div>
                    <div class="alert alert-danger" id="PanAddressValid" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Address is not verified. Please verify the address first.</strong>
                    </div>
                </div>

                <asp:Panel runat="server" ID="PanGridProject">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="paddall">
                                        <div class="row">
                                            <div class="form-group" style="padding-left: 12px!important; padding-right: 10px!important;">
                                                <div class="contactbottomarea finalgrid">
                                                    <div class="tableblack tableminpadd">
                                                        <div class="table-responsive" id="PanGrid" runat="server">
                                                            <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server"
                                                                OnPageIndexChanging="GridView1_PageIndexChanging" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                                AllowSorting="true" OnRowDataBound="GridView1_RowDataBound" AutoGenerateColumns="false"
                                                                OnSorting="GridView1_Sorting" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnRowCommand="GridView1_RowCommand">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="brdrgrayleft">
                                                                        <ItemTemplate>
                                                                            <%#Eval("ProjectNumber")%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="140px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                                            <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                            <%#Eval("Address")%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%----%>
                                                                            <asp:LinkButton ID="hypStatus" runat="server" CommandName="Status" CommandArgument='<%#Eval("ProjectID") %>'
                                                                                CausesValidation="false"><%#Eval("ProjectStatus")%></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="100px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Updated By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#Eval("updatedbyempname")%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="150px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn-primary btn btn-xs"
                                                                                NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>' Target="_blank">
                                          <%--  <img src="<%=SiteURL%>/images/icon_detail.png" />--%>
                                            <i class="fa fa-link"></i>Detail
                                                                            </asp:HyperLink>
                                                                            <%--<asp:HyperLink ID="gvbtnDetail2" runat="server" data-toggle="tooltip" data-placement="top" Visible="false" data-original-title="Detail" CssClass="btn-primary btn btn-xs"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/ECompany.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                          <img src="<%=SiteURL%>/images/icon_detail.png" />
                                            <i class="fa fa-link"></i>Detail
                                                    </asp:HyperLink>--%>

                                                                            <!--DELETE Modal Templates-->
                                                                            <%--  <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                        CommandName="Delete" CommandArgument='<%#Eval("ProjectID")%>'>
                                            <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>--%>

                                                                            <!--END DELETE Modal Templates-->


                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <div class="tabintab companytab">
                    <div class="panel panel-default">
                        <div class="panel-heading printpage" style="padding-bottom: 18px;">
                            <span style="display: inline-block; padding-top: 6px;">
                                <asp:Literal ID="ltproject" runat="server"></asp:Literal></span>

                            <%-- <div class="pull-right fontsize13">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/btn_check_active2.png"
                                    CausesValidation="false" OnClick="btnCheckActive_Click" CssClass="btnimagecheckactivity margintopminus" />
                                <asp:LinkButton ID="lnkclose" CausesValidation="false" runat="server"
                                    OnClick="lnkclose_Click" CssClass="btn btn-maroon"><i class="fa fa-backward"></i>Back</asp:LinkButton>
                            </div>--%>
                        </div>

                        <cc1:TabContainer ID="TabContainer" runat="server">

                            <cc1:TabPanel ID="TabSales" runat="server" HeaderText="Sales">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanAdd">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="paddall">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="graybgarea">
                                                                        <div class="row">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <span class="name disblock">
                                                                                        <label class="control-label">
                                                                                            Project Type<span class="symbol required"></span>
                                                                                        </label>
                                                                                    </span>
                                                                                    <div class="drpValidate">

                                                                                        <asp:DropDownList ID="ddlProjectTypeID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="True"
                                                                                            OnSelectedIndexChanged="ddlProjectTypeID_SelectedIndexChanged" AppendDataBoundItems="True">
                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required."
                                                                                            ControlToValidate="ddlProjectTypeID" Display="Dynamic" ValidationGroup="project"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2" id="divRemoveOldSystem" runat="server">
                                                                                <div class="form-group">
                                                                                    <span class="name disblock">
                                                                                        <label class="control-label">
                                                                                            Remove Old System ?<span class="symbol required"></span>
                                                                                        </label>
                                                                                    </span>
                                                                                    <div class="drpValidate">
                                                                                        <asp:DropDownList ID="ddlOldRemoveOldSystem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                            AppendDataBoundItems="True" AutoPostBack="true" OnSelectedIndexChanged="ddlOldRemoveOldSystem_SelectedIndexChanged">
                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="RqOldRemoveOldSystem" runat="server"
                                                                                            ValidationGroup="SalesTabReq1" ControlToValidate="ddlOldRemoveOldSystem" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2" runat="server" id="divProjectOpened">
                                                                                <div class="form-group dateimgarea">
                                                                                    <span class="name">
                                                                                        <label class="control-label">
                                                                                            Proj. Opened
                                                                                        </label>
                                                                                    </span><span class="dateimg">
                                                                                        <asp:TextBox ID="txtProjectOpened" runat="server" Enabled="False" class="form-control"></asp:TextBox>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2" runat="server" id="divManualQuoteNo">
                                                                                <div class="form-group">
                                                                                    <span class="name">
                                                                                        <label class="control-label" style="font-size: 11.5px">
                                                                                            Man Quote No.
                                                                                        </label>
                                                                                    </span><span>
                                                                                        <asp:TextBox ID="txtManualQuoteNumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2 form-group" id="divSalesRep" runat="server">
                                                                                <span class="name">
                                                                                    <label class="control-label">
                                                                                        Sales Rep<span class="symbol required"></span>
                                                                                    </label>
                                                                                </span><span>
                                                                                    <asp:DropDownList ID="ddlSalesRep" runat="server" Enabled="False" aria-controls="DataTables_Table_0" class="myval"
                                                                                        AppendDataBoundItems="True">
                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required."
                                                                                        ControlToValidate="ddlSalesRep" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                </span>
                                                                                <div class="clear">
                                                                                </div>

                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <span class="name disblock">
                                                                                        <label class="control-label">
                                                                                            Contact<span class="symbol required"></span>
                                                                                        </label>
                                                                                    </span><span>
                                                                                        <asp:DropDownList ID="ddlContact" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                                            AppendDataBoundItems="True">
                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                                                                            ControlToValidate="ddlContact" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <div runat="server" class="form-group" id="divoldproject">
                                                                                    <span class="name">
                                                                                        <label class="control-label">
                                                                                            Old Project No.
                                                                                        </label>
                                                                                    </span><span>
                                                                                        <asp:DropDownList runat="server" ID="ddllinkprojectid" AppendDataBoundItems="True" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="ddllinkprojectid" Display="Dynamic" Visible="False" ValidationGroup="project"></asp:RequiredFieldValidator>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="graybgarea">
                                                                        <div class="row">

                                                                            <div class="col-md-2" id="Div1" visible="False" runat="server">
                                                                                <div class="form-group">
                                                                                    <div class="marginbtm10">
                                                                                        <asp:HiddenField ID="hndaddress" runat="server" />
                                                                                        <asp:TextBox ID="txtInstallAddressline" runat="server" MaxLength="50" CssClass="form-control" onblur="getDetailInstallParsedAddress();"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                                                            ControlToValidate="txtInstallAddressline" Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                                                                                        <asp:CustomValidator ID="CustomInstallAddressline" runat="server" Display="Dynamic" CssClass="requiredfield" ValidationGroup="detail"
                                                                                            ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <span class="name">
                                                                                        <label class="control-label">
                                                                                            Install Site
                                                                                        </label>
                                                                                    </span>
                                                                                    <div id="Div2" class="marginbtm10" visible="False" runat="server">
                                                                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                                        <asp:TextBox ID="TextBox1" runat="server" MaxLength="50" CssClass="form-control" onblur="getDetailInstallParsedAddress();"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required."
                                                                                            ControlToValidate="txtInstallAddressline" Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                                                                                        <asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic" CssClass="requiredfield" ValidationGroup="detail"
                                                                                            ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                                                    </div>
                                                                                    <div class="marginbtm10">

                                                                                        <asp:Panel runat="server" ID="PanInstallAddress">
                                                                                            <asp:TextBox ID="txtInstallAddress" runat="server" MaxLength="200" Enabled="False" CssClass="form-control"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required."
                                                                                                ControlToValidate="txtInstallAddress" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>
                                                                                        </asp:Panel>

                                                                                    </div>

                                                                                    <asp:HiddenField ID="hndstreetno" runat="server" />
                                                                                    <asp:HiddenField ID="hndstreetname" runat="server" />
                                                                                    <asp:HiddenField ID="hndstreettype" runat="server" />
                                                                                    <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                                                    <asp:HiddenField ID="hndunittype" runat="server" />
                                                                                    <asp:HiddenField ID="hndunitno" runat="server" />
                                                                                    <div id="validaddressid" style="display: none">
                                                                                        <i class="fa fa-check"></i>
                                                                                        Address is valid.
                                                                                    </div>
                                                                                    <div id="invalidaddressid" style="display: none">
                                                                                        <i class="fa fa-close"></i>
                                                                                        Address is invalid.
                                                                                    </div>

                                                                                </div>


                                                                                <div class="onelindiv marginbtm10 row">
                                                                                    <span class="col-md-6">
                                                                                        <asp:TextBox ID="txtformbayUnitNo" runat="server" placeholder="Unit No" CssClass="form-control" AutoPostBack="True" OnTextChanged="txtformbayUnitNo_TextChanged"></asp:TextBox></span>
                                                                                    <span class="col-md-6">
                                                                                        <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlformbayunittype_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="onelindiv marginbtm10 row">
                                                                                    <span class="col-md-3">
                                                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" placeholder="Street No" AutoPostBack="True" OnTextChanged="txtformbayStreetNo_TextChanged" CssClass="form-control"></asp:TextBox></span>
                                                                                    <span class="col-md-4">
                                                                                        <asp:TextBox ID="txtformbaystreetname" runat="server" placeholder="Street Name" AutoPostBack="True" OnTextChanged="txtformbaystreetname_TextChanged" CssClass="form-control"></asp:TextBox>
                                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                                            UseContextKey="True" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                                            EnableCaching="False" CompletionInterval="10" CompletionSetCount="20" DelimiterCharacters="" Enabled="True" />
                                                                                        <asp:CustomValidator ID="Customstreetadd" runat="server" Display="Dynamic" CssClass="requiredfield"
                                                                                            ClientValidationFunction="GetStreetNameList"></asp:CustomValidator>
                                                                                    </span>
                                                                                    <span class="col-md-5">
                                                                                        <asp:DropDownList ID="ddlformbaystreettype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged" AppendDataBoundItems="True" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                                        </asp:DropDownList></span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="marginbtm10 row">
                                                                                    <span class="col-md-6">
                                                                                        <asp:TextBox ID="txtInstallCity" runat="server" OnTextChanged="txtInstallCity_OnTextChanged" AutoPostBack="True"
                                                                                            MaxLength="50" class="form-control"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfvinstall" runat="server" ErrorMessage="This value is required."
                                                                                            ControlToValidate="txtInstallCity" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteSearch" MinimumPrefixLength="2" runat="server"
                                                                                            UseContextKey="True" TargetControlID="txtInstallCity" ServicePath="~/Search.asmx"
                                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                                            EnableCaching="False" CompletionInterval="10" CompletionSetCount="20" DelimiterCharacters="" Enabled="True" />
                                                                                    </span>
                                                                                    <span class="col-md-3">
                                                                                        <asp:TextBox ID="txtInstallState" Enabled="False" runat="server" CssClass="form-control"></asp:TextBox></span>
                                                                                    <span class="col-md-3">
                                                                                        <asp:TextBox ID="txtInstallPostCode" Enabled="False" runat="server" CssClass="form-control"></asp:TextBox></span></span>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group textareaboxheight">
                                                                                    <span class="name">
                                                                                        <label class="control-label">
                                                                                            Project Notes
                                                                                        </label>
                                                                                    </span><span>
                                                                                        <asp:TextBox ID="txtProjectNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="170px"
                                                                                            onkeyDown="checkTextAreaMaxLength(this,event,'350');"></asp:TextBox>
                                                                                        <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtProjectNotes" FilterMode="InvalidChars" InvalidChars="&quot;'" ID="ctl001" Enabled="True">
                                                                                        </cc1:FilteredTextBoxExtender>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <span class="name">
                                                                                        <label class="control-label">
                                                                                            Old System Details
                                                                                        </label>
                                                                                        <br />
                                                                                    </span><span class="with44">
                                                                                        <asp:TextBox ID="txtOldSystemDetails" CssClass="form-control" runat="server"
                                                                                            Height="170px" TextMode="MultiLine"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorOldSystemDetails" runat="server"
                                                                                            ValidationGroup="SalesTabReq1" ControlToValidate="txtOldSystemDetails" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <span class="name">
                                                                                        <label class="control-label">
                                                                                            Installer Notes
                                                                                        </label>
                                                                                        <br />
                                                                                    </span><span class="with44">
                                                                                        <asp:TextBox ID="txtInstallerNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="170px"></asp:TextBox>
                                                                                    </span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divHnADetails" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">House & Application Details</span>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="PanHnADetails">
                                                                                            <div class="row">
                                                                                                <div class="col-md-3">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                House Status
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:Label ID="Label25" runat="server"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlHouseStatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>

                                                                                                            <asp:HiddenField runat="server" ID="HiddenField3" />
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-3">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Completion Date
                                                                                                            </label>
                                                                                                            <div class="input-group date datetimepicker1 ">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtCompletionDate" runat="server" class="form-control"></asp:TextBox>
                                                                                                            </div>
                                                                                                            <div class="clear">
                                                                                                            </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-3">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Dist Applied 
                                                                                                            </label>
                                                                                                            <div class="input-group date datetimepicker1 ">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtDistApplied" runat="server" class="form-control"></asp:TextBox>
                                                                                                            </div>
                                                                                                            <div class="clear">
                                                                                                            </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-3">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Approval Ref
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtApprovalRef" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divPanelDetail" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">Products</span>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="Pan1">
                                                                                            <div class="row">
                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Select Panel
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:Label ID="labl" runat="server"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlPanels" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True" onchange="PanelsChange(this.value)">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>

                                                                                                            <asp:HiddenField runat="server" ID="hndPanelBrand" />
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Select Inverter
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:Label ID="Label1" runat="server"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlInverter" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True" onchange="InverterChange1(this.value)">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>

                                                                                                            <asp:HiddenField runat="server" ID="hndInverterBrand1" />
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Second Inverter
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:Label ID="Label2" runat="server"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlSecondInverter" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True" onchange="InverterChange2(this.value)">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                            <asp:HiddenField runat="server" ID="hndInverterBrand2" />
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Others
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:Label ID="Label3" runat="server"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlThirdInverter" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True" onchange="InverterChange3(this.value)">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                            <asp:HiddenField runat="server" ID="hndInverterBrand3" />
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Model
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtPanelModel" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Model
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtInverterModel" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Model
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtInverterModel2" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Model
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtModel3" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Watts/Panel 
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtWatts" Text="0" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Series 
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtSeries" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Series 
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtSeries2" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Series 
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtSeries3" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                No. of Panels
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtNoOfPanels" runat="server" MaxLength="10" CssClass="form-control"
                                                                                                                Text="0" onchange="NumberOfPanelsChange(this.value)"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" Style="color: red;"
                                                                                                                ControlToValidate="txtNoOfPanels" Display="Dynamic" ErrorMessage="Number Only"
                                                                                                                ValidationGroup="sale" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                No. of Inverter
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtNoOfInverter" runat="server" MaxLength="10" CssClass="form-control"
                                                                                                                Text="1"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Style="color: red;"
                                                                                                                ControlToValidate="txtNoOfInverter" Display="Dynamic" ErrorMessage="Number Only"
                                                                                                                ValidationGroup="sale" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                No. of Inverter
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtNoOfInverter2" runat="server" MaxLength="10" CssClass="form-control"
                                                                                                                Text="0"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Style="color: red;"
                                                                                                                ControlToValidate="txtNoOfInverter2" Display="Dynamic" ErrorMessage="Number Only"
                                                                                                                ValidationGroup="sale" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                No. of Inverter
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtNoOfInverter3" runat="server" MaxLength="10" CssClass="form-control"
                                                                                                                Text="0"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Style="color: red;"
                                                                                                                ControlToValidate="txtNoOfInverter3" Display="Dynamic" ErrorMessage="Number Only"
                                                                                                                ValidationGroup="sale" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                System Capacity
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtSystemCapacity" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                STC Price
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtSTCInverter" Text="0.00" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Zone Rt
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtZoneRt" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <asp:HiddenField runat="server" ID="hndInverterOutput1" Value="0.00" />
                                                                                                            <asp:HiddenField runat="server" ID="hndInverterOutput2" Value="0.00" />
                                                                                                            <asp:HiddenField runat="server" ID="hndInverterOutput3" Value="0.00" />
                                                                                                            <label class="control-label">
                                                                                                                Inverter Output
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtInverterOutput" runat="server" MaxLength="200" CssClass="form-control" Text="0.00"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                STC
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtSTC" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Rebate
                                                                                                            </label>
                                                                                                            <br />
                                                                                                        </span><span class="pricespan">
                                                                                                            <asp:TextBox ID="txtRebate" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left"></asp:TextBox>

                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRebate"
                                                                                                                ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                                                                ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                STC Mult
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtSTCMult" runat="server" Text="1" Enabled="False" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen" id="DivDoB" runat="server" visible="false">
                                                                                                        <span class="name">
                                                                                                            <asp:Label ID="Label28" runat="server" class="  control-label">
                                                                                                                    DOB</asp:Label></span>
                                                                                                        <div class="input-group date datetimepicker1 ">
                                                                                                            <span class="input-group-addon">
                                                                                                                <span class="fa fa-calendar"></span>
                                                                                                            </span>
                                                                                                            <asp:TextBox ID="txtDoB" runat="server" class="form-control" Width="125px">
                                                                                                            </asp:TextBox>
                                                                                                        </div>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>

                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divOldPanelDetails" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">Old Product Details</span>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="PanOldPanelDetails">
                                                                                            <div class="row">
                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Select Panel
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:Label ID="Label29" runat="server"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlOldPanel" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True" onchange="OldPanelsChange(this.value)">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                            <asp:RequiredFieldValidator ID="RqOldPanel" runat="server"
                                                                                                                ValidationGroup="SalesTabReq1" ControlToValidate="ddlOldPanel" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                            <asp:HiddenField runat="server" ID="hndOldPanelBrand" />
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Model
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldModel" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Watts/Panel 
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldWatts" Text="0" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                No. of Panels
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldNumberOfPanels" runat="server" MaxLength="10" CssClass="form-control"
                                                                                                                Text="0" onchange="OldNumberOfPanelsChange(this.value)"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server" Style="color: red;"
                                                                                                                ControlToValidate="txtOldNumberOfPanels" Display="Dynamic" ErrorMessage="Number Only"
                                                                                                                ValidationGroup="sale" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                            <asp:RequiredFieldValidator ID="RqtxtOldNumberOfPanels" runat="server" InitialValue="0"
                                                                                                                ValidationGroup="SalesTabReq1" ControlToValidate="txtOldNumberOfPanels" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                System Capacity
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldSystemCapacity" runat="server" MaxLength="200" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Panel
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldPanelManual" runat="server" CssClass="form-control" onchange="disableValidatorRemoveOldSystem()"></asp:TextBox>
                                                                                                            <asp:RequiredFieldValidator ID="RqOldPanelManual" runat="server"
                                                                                                                ValidationGroup="SalesTabReq1" ControlToValidate="txtOldPanelManual" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Model
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldModelManual" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Watts/Panel 
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldWattsManual" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                No. of Panels
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldNumberOfPanelsManual" runat="server" MaxLength="10" CssClass="form-control" onchange="disableValidatorNoOfPanels()"></asp:TextBox>
                                                                                                            <asp:RequiredFieldValidator ID="RqtxtOldNumberOfPanelsManual" runat="server"
                                                                                                                ValidationGroup="SalesTabReq1" ControlToValidate="txtOldNumberOfPanelsManual" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                System Capacity
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOldSystemCapacityManual" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divSitedetails" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">Site Details</span>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="Pan3">
                                                                                            <div class="row">
                                                                                                <div class="col-md-4">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                House Type
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:DropDownList ID="ddlHouseType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Roof Type
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:DropDownList ID="ddlRoofType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen" id="divFlatPanels">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                <asp:Label ID="lblFlatPanels" Text="Panel On Flat" runat="server"></asp:Label>:
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtFlatPanels" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtFlatPanels"
                                                                                                                ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen" id="divPitchedPanels">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                <asp:Label ID="lblPitchedPanels" Text="Panel on Pitched" runat="server"></asp:Label>:
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtPitchedPanels" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtPitchedPanels"
                                                                                                                ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Roof Angle
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:DropDownList ID="ddlRoofAngle" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Elec Dist
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:DropDownList ID="ddlElecDist" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen" id="divSurveyCerti" runat="server" visible="False">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Survey Certificate
                                                                                                            </label>
                                                                                                        </span><span class="radiogruopmain">
                                                                                                            <asp:RadioButtonList ID="rblSurveyCerti" runat="server" AppendDataBoundItems="True" RepeatDirection="Horizontal">
                                                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                                                                            </asp:RadioButtonList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen" id="divCertiApprove" runat="server" visible="False">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Certificate Approve
                                                                                                            </label>
                                                                                                        </span><span class="radiogruopmain">
                                                                                                            <asp:RadioButtonList ID="rblCertiApprove" runat="server" AppendDataBoundItems="True" RepeatDirection="Horizontal">
                                                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                                                                            </asp:RadioButtonList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Elec Retailer
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:DropDownList ID="ddlElecRetailer" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Meter Upgrade
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:DropDownList ID="ddlmeterupgrade" runat="server" AppendDataBoundItems="True"
                                                                                                                aria-controls="DataTables_Table_0" CssClass="myval form-control" Width="200px">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Peak Meter No:
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtPeakMeterNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Off-Peak Meters:
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtOffPeakMeters" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4">
                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                NMI Number
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtNMINumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Reg Plan No
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtRegPlanNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Lot Num
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtLotNum" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtLotNum"
                                                                                                                ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." Style="color: red;"
                                                                                                                ValidationGroup="sale" ControlToValidate="txtLotNum" Display="Dynamic" Visible="False"></asp:RequiredFieldValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Meter Phase 1-2-3:
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtMeterPhase" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                            <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="txtMeterPhase" Style="color: red;"
                                                                                                                ValidationGroup="sale" MinimumValue="1" MaximumValue="3" Type="Integer" Text="Enter 1, 2 or 3."
                                                                                                                Display="Dynamic" />
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <div class="form-group selpen">
                                                                                                            <span class="checkbox-info checkbox">
                                                                                                                <label for="<%=chkEnoughMeterSpace.ClientID %>">
                                                                                                                    <asp:CheckBox ID="chkEnoughMeterSpace" runat="server" />
                                                                                                                    <span class="text">&nbsp;Enough Meter Space:</span>
                                                                                                                </label>
                                                                                                            </span>
                                                                                                            <div class="clear">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group selpen">
                                                                                                        <span class="checkbox-info checkbox">
                                                                                                            <label for="<%=chkIsSystemOffPeak.ClientID %>">
                                                                                                                <asp:CheckBox ID="chkIsSystemOffPeak" runat="server" />
                                                                                                                <span class="text">&nbsp;Is System Off-Peak:</span>
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divPriceDetails" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">Price Details</span>

                                                                                        <div class="pull-right" style="margin-top: 3px; margin-right: 3px;">
                                                                                            <asp:LinkButton ID="lnkCalculate" runat="server" class="btn btn-primary btn-sm" OnClick="lnkCalculate_Click"
                                                                                                CausesValidation="False"><i class="btn-label fa fa-calculator"></i>Calculate</asp:LinkButton>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="PanelPrice">
                                                                                            <div class="row">
                                                                                                <div class="col-md-4">
                                                                                                    <div class="form-group" runat="server" id="DivPriceCategory" visible="False">
                                                                                                        <span class="name" style="float: left;">
                                                                                                            <label class="control-label disblock" style="font-size: 14px; padding-top: 6px; padding-right: 50px;">
                                                                                                                Price Category
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <span class="floatleftin">
                                                                                                            <asp:DropDownList ID="ddlPriseCategory" runat="server" aria-controls="DataTables_Table_0"
                                                                                                                CssClass="myval" AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                <asp:ListItem Value="1" Text="Price 1" />
                                                                                                                <asp:ListItem Value="2" Text="Price 2" />
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group">
                                                                                                        <span class="name" style="float: left;">
                                                                                                            <label class="control-label disblock" style="font-size: 14px; padding-top: 6px; padding-right: 50px;">
                                                                                                                Basic System Cost
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <span class="floatleftin ">
                                                                                                            <asp:TextBox ID="txtBasicSystemCost" Text="0.00" runat="server" MaxLength="15" Width="150px" AutoPostBack="true"
                                                                                                                OnTextChanged="txtBasicSystemCost_TextChanged" CssClass="form-control alignright floatleft dolarsingn"></asp:TextBox>

                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtBasicSystemCost"
                                                                                                                ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <!-- Start Code for Veriation Repeater Here -->
                                                                                                    <h4 class="colorblack martop15">Variations</h4>

                                                                                                    <asp:Repeater runat="server" ID="rptVeriation" OnItemDataBound="rptVeriation_ItemDataBound">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="display: flex;">
                                                                                                                <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type")%>' />
                                                                                                                <asp:HiddenField ID="hndVeriationID" runat="server" Value='<%#Eval("VeriationID") %>' />
                                                                                                                <asp:HiddenField ID="hndVaritionValue" runat="server" Value='<%#Eval("VaritionValue") %>' />
                                                                                                                <asp:HiddenField ID="hndNotes" runat="server" Value='<%#Eval("Notes") %>' />
                                                                                                                <div class="form-group" style="margin-right: 30px; width: 150px">
                                                                                                                    <asp:DropDownList ID="ddlVeriation" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVeriation_SelectedIndexChanged" AutoPostBack="true">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </div>
                                                                                                                <div class="form-group" style="margin-right: 30px; width: 150px">
                                                                                                                    <asp:TextBox ID="txtVariationValue" Text="0.00" runat="server" MaxLength="15" Width="150px"
                                                                                                                        CssClass="form-control alignright floatleft dolarsingn" OnTextChanged="txtVariationValue_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtVariationValue" runat="server"
                                                                                                                        ValidChars=".">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </div>

                                                                                                                <div class="form-group" style="margin-right: 30px;">
                                                                                                                    <asp:TextBox ID="txtNotes" TextMode="MultiLine" runat="server" MaxLength="350" Width="300px"
                                                                                                                        CssClass="form-control alignright floatleft dolarsingn"></asp:TextBox>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="InvalidChars" FilterType="Custom" TargetControlID="txtNotes" runat="server"
                                                                                                                        InvalidChars="'">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="InvalidChars" FilterType="Custom" TargetControlID="txtNotes" runat="server"
                                                                                                                        InvalidChars='"'>
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </div>

                                                                                                                <div class="form-group" style="margin-right: 30px;">
                                                                                                                    <asp:LinkButton ID="btnRemove" runat="server" CssClass="btn-danger btn btn-xs" OnClick="btnRemove_Click" CausesValidation="false">
                                                                                                                <i class="fa fa-close"></i> Remove
                                                                                                                    </asp:LinkButton>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:Repeater>

                                                                                                    <div class="form-group">
                                                                                                        <asp:LinkButton ID="btnAddVeriation" runat="server" CssClass="btn-primary btn btn-xs" OnClick="btnAddVeriation_Click" CausesValidation="False">
                                                                                                    <i class="fa fa-plus"></i> Add More
                                                                                                        </asp:LinkButton>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!-- End Code -->

                                                                                                    <div class="form-group">
                                                                                                        <span class="name" style="float: left;">
                                                                                                            <label class="control-label disblock" style="font-size: 14px; padding-top: 6px; padding-right: 53px;">
                                                                                                                Total System Cost
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <span class="floatleftin ">
                                                                                                            <asp:TextBox ID="txtTotalCost" runat="server" Enabled="False" CssClass="form-control alignright dolarsingn"
                                                                                                                Width="150px" Text="0.00"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group">
                                                                                                        <span class="name" style="float: left;">
                                                                                                            <label class="control-label disblock" style="font-size: 14px; padding-top: 6px; padding-right: 58px;">
                                                                                                                Deposit Required
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <span class="floatleftin ">
                                                                                                            <asp:TextBox ID="txtDepositRequired" runat="server" Text="0.00" MaxLength="15"
                                                                                                                Width="150px" CssClass="form-control alignright dolarsingn" AutoPostBack="true" OnTextChanged="txtDepositRequired_TextChanged"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group">
                                                                                                        <span class="name" style="float: left;">
                                                                                                            <label class="control-label disblock" style="font-size: 14px; padding-top: 6px; padding-right: 108px;">
                                                                                                                Bal to Pay
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <span class="floatleftin ">
                                                                                                            <asp:TextBox ID="txtBaltoPay" runat="server" Enabled="False" Text="0.00" Width="150px"
                                                                                                                CssClass="form-control alignright dolarsingn"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>


                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divFinanceDetails" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">Finance Details</span>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="PanelFinance">
                                                                                            <div class="row">
                                                                                                <div class="col-md-3">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Payment Option
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <span class="dateimg" style="width: 150px;">
                                                                                                            <asp:DropDownList ID="ddlPaymentOption" TabIndex="31" runat="server"
                                                                                                                aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                Width="312px" AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group" id="divDepositOption" runat="server">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Deposit Option
                                                                                                            </label>
                                                                                                        </span><span class="dateimg">
                                                                                                            <asp:DropDownList ID="ddlDepositOption" TabIndex="37" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                                                                Width="100px" AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>

                                                                                                <div class="col-md-3">
                                                                                                    <div class="form-group" id="divPaymentType" runat="server">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Finance Payment Type
                                                                                                            </label>
                                                                                                        </span><span class="dateimg">
                                                                                                            <asp:DropDownList ID="ddlPaymentType" TabIndex="37" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                AppendDataBoundItems="True">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Application Date
                                                                                                            </label>
                                                                                                        </span>
                                                                                                        <div class="input-group date datetimepicker1 ">
                                                                                                            <span class="input-group-addon">
                                                                                                                <span class="fa fa-calendar"></span>
                                                                                                            </span>
                                                                                                            <asp:TextBox ID="txtApplicationDate" runat="server" class="form-control"></asp:TextBox>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Purchase No.
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtPurchaseNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divOthersDetails" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">Others Details</span>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="PanelOthersDetails">
                                                                                            <div class="row">
                                                                                                <div class="col-md-4">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Promo Offer
                                                                                                            </label>
                                                                                                        </span><span class="dateimg">
                                                                                                            <asp:DropDownList ID="ddlPromo1ID" runat="server" TabIndex="32" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                AppendDataBoundItems="True" Style="width: 312px">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                House Status
                                                                                                            </label>
                                                                                                        </span><span class="dateimg">
                                                                                                            <asp:DropDownList ID="ddlPromo2ID" TabIndex="33" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                AppendDataBoundItems="True" Style="width: 312px">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-8">
                                                                                                    <div class="form-group ">

                                                                                                        <div class=" checkbox-info checkbox">
                                                                                                            <span class="fistname">
                                                                                                                <label for="<%=chkPromo3.ClientID %>">
                                                                                                                    <asp:CheckBox ID="chkPromo3" runat="server" TabIndex="34" />
                                                                                                                    <span class="text">Other &nbsp;</span>
                                                                                                                </label>
                                                                                                            </span>
                                                                                                        </div>

                                                                                                        <span class="dateimg">
                                                                                                            <asp:TextBox ID="txtPromoText" runat="server" MaxLength="200" Style="margin-top: 10px;"
                                                                                                                TextMode="MultiLine" Height="87px" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="graybgarea panel-body">
                                                                        <div class="row">
                                                                            <div id="divRetailerForm" runat="server">
                                                                                <div class="widget flat radius-bordered borderone">
                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                        <span class="widget-caption">Retailer Form</span>
                                                                                    </div>
                                                                                    <div class="widget-body">
                                                                                        <asp:Panel runat="server" ID="PanRetailerForm">
                                                                                            <div class="row">
                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Export Detail
                                                                                                            </label>
                                                                                                        </span><span class="dateimg">
                                                                                                            <asp:DropDownList ID="ddlExportDetail" runat="server" TabIndex="32" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                AppendDataBoundItems="True" Style="width: 312px" AutoPostBack="true" OnSelectedIndexChanged="ddlExportDetail_SelectedIndexChanged">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                <asp:ListItem Value="Fully Export">Fully Export</asp:ListItem>
                                                                                                                <asp:ListItem Value="Partial Export">Partial Export</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2" runat="server" id ="divKWExport">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                How many kw Export
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtKwExport" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2" runat="server" id ="divKWNonExport">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                How many Kw Non Export
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtKwNonExport" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name disblock">
                                                                                                            <label class="control-label">
                                                                                                                Grid Connected System
                                                                                                            </label>
                                                                                                        </span><span class="dateimg">
                                                                                                            <asp:DropDownList ID="ddlGridConnectedSystem" runat="server" TabIndex="32" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                AppendDataBoundItems="True" Style="width: 312px">
                                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-2">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Approx feed in tariff
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtFeedInTariff" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-3">
                                                                                                    <div class="form-group">
                                                                                                        <span class="name">
                                                                                                            <label class="control-label">
                                                                                                                Approx expected payback period (In Yrs)
                                                                                                            </label>
                                                                                                        </span><span>
                                                                                                            <asp:TextBox ID="txtApproxExpectedPaybackPeriod" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                                        </span>
                                                                                                        <div class="clear">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-5" runat="server" visible="False">
                                                                        <div class="graybgarea margintop50 spicaltextbox">

                                                                            <div class="form-group textareaboxheight">
                                                                                <span class="name">
                                                                                    <label class="control-label">
                                                                                        Installer Notes
                                                                                    </label>
                                                                                </span><span></span>
                                                                                <div class="clear">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group textareaboxheight">
                                                                                <span class="name">
                                                                                    <label class="control-label">
                                                                                        Notes for Installation Department
                                                                                    </label>
                                                                                </span><span>
                                                                                    <asp:TextBox ID="txtMeterInstallerNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                                                </span>
                                                                                <div class="clear">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div>
                                                                    <span></span>
                                                                </div>

                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-12 textcenterbutton">
                                                                    <div style="text-align: center;">
                                                                        <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                            Text="Save" ID="btnSave" OnClick="btnSave_Click" ValidationGroup="SalesTabReq1" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>

                            <cc1:TabPanel ID="TabQuote" runat="server" HeaderText="Quote">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanMainQuote">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanelQuote">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <div class="paddall">
                                                                    <div class="row">
                                                                        <div class="col-md-12">

                                                                            <div class="graybgarea panel-body">
                                                                                <div class="row">
                                                                                    <div id="DivQuoteDetails" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Quote Details</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanelQuoteDetails">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <asp:Label ID="Label5" runat="server" class="  control-label">
                                                                                                        First Quote Sent</asp:Label></span>
                                                                                                                <div class="input-group date datetimepicker1 ">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtQuoteSent" runat="server" class="form-control" Width="125px">
                                                                                                                    </asp:TextBox>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <asp:Label ID="Label6" runat="server" class="  control-label">
                                                                                                        Quote Accepted</asp:Label></span>
                                                                                                                <div class="input-group date datetimepicker1 ">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtQuoteAcceptedQuote" runat="server" class="form-control" Width="125px">
                                                                                                                    </asp:TextBox>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="form-group dateimgarea fileuploadmain col-md-4" style="margin-bottom: 0px;">
                                                                                                            <span>
                                                                                                                <span>
                                                                                                                    <label for="<%=chkSignedQuote.ClientID %>">
                                                                                                                        <asp:CheckBox ID="chkSignedQuote" runat="server" AutoPostBack="true" OnCheckedChanged="chkSignedQuote_CheckedChanged" />
                                                                                                                        <span class="text">Signed Quote Stored</span>
                                                                                                                    </label>

                                                                                                                </span>
                                                                                                                <span id="divSQ" runat="server" visible="false">
                                                                                                                    <asp:HyperLink ID="lblSQ" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                                                                    <span class="dateimg">
                                                                                                                        <span class="file-input btn btn-azure btn-file">
                                                                                                                            <asp:FileUpload ID="fuSQ" runat="server" />
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorSQ" runat="server" ErrorMessage="This value is required." ValidationGroup="DocSQUpload" ControlToValidate="fuSQ" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                                                                    <span class="dateimg">
                                                                                                                        <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                                                            Text="Upload" ID="btnSQUpload" ValidationGroup="DocSQUpload" OnClick="btnSQUpload_Click" /></span>
                                                                                                                </span>


                                                                                                            </span>
                                                                                                            <div class="clear">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="graybgarea panel-body col-md-6">
                                                                                    <div class="row" style="margin-left: -2px; margin-right: 1px;">
                                                                                        <div id="DivQuote" runat="server">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <asp:Panel runat="server" ID="PanBtnQuote">
                                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                                        <span class="widget-caption">Create Quote</span>
                                                                                                        <div class="pull-right" style="margin-top: 3px; margin-right: 3px;">
                                                                                                            <asp:LinkButton ID="btnCreateNewQoute" runat="server" class="btn btn-primary btn-sm" OnClick="btnCreateNewQoute_Click"
                                                                                                                CausesValidation="false"><i class="btn-label fa fa-file-pdf-o"></i>Generate Quote</asp:LinkButton>
                                                                                                           <%-- <asp:LinkButton ID="btnRetailerStatement" runat="server" class="btn btn-primary btn-sm" OnClick="btnRetailerStatement_Click"
                                                                                                                CausesValidation="false"><i class="btn-label fa fa-file-pdf-o"></i>Retailer Statement</asp:LinkButton>--%>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanQuote">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="row finalgrid" id="divQuotes" runat="server" visible="false">
                                                                                                                    <div class="col-md-12" runat="server" id="divquo">
                                                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                            <thead>
                                                                                                                                <tr>
                                                                                                                                    <th style="width: 25%; text-align: center">Quote Date
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Doc No.
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 15%; text-align: center">Signed
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Document
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Email
                                                                                                                                    </th>
                                                                                                                                </tr>
                                                                                                                            </thead>
                                                                                                                            <asp:Repeater ID="rptQuote" runat="server" OnItemDataBound="rptQuote_ItemDataBound" OnItemCommand="rptQuote_ItemCommand">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 25%; text-align: center">
                                                                                                                                            <%#Eval("ProjectQuoteDate", "{0:ddd - dd MMM yyyy}")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 20%; text-align: center">
                                                                                                                                            <%#Eval("ProjectQuoteDoc")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 15%; text-align: center;">
                                                                                                                                            <%-- <label for='<%# Container.FindControl("chkSerialNo").ClientID  %>' runat="server" id="lblchk123" >
                                                                                            <asp:CheckBox ID="chkSerialNo" runat="server" />
                                                                                            <span class="text">&nbsp;</span>
                                                                                        </label>--%>
                                                                                                                                            <%--<asp:Label runat="server" ID="lblsignchked" Visible="false"><i class="fa fa-check-square" style="width:20px;height:20px;"></i></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblsignUnChked"><i class="fa fa-square"></i></asp:Label>--%>
                                                                                                                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/check.png" Visible="false" />
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 25%; text-align: center;">
                                                                                                                                            <asp:HiddenField ID="hndProjectQuoteID" runat="server" Value='<%#Eval("ProjectQuoteID") %>' />
                                                                                                                                            <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                                                                                                <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/icon_document_downalod.png" />
                                                                                                                                            </asp:HyperLink>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 20%; text-align: center">
                                                                                                                                            <asp:LinkButton ID="btnSendAMail" runat="server" Visible="false" CommandName="MailAttachedPDf" CommandArgument='<%#Eval("ProjectQuoteID")%>' CssClass="addPreloader">
                                                                                               <i class="fa fa-envelope-o fa-lg"></i>
                                                                                           <%--<span class="typcn typcn-mail" ></span>--%>
                                                                                                                                            </asp:LinkButton>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:Repeater>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="row">
                                                                                                            <div class="col-md-4">
                                                                                                                <asp:LinkButton ID="lnksendmessage" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lnksendmessage_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>  Cust`s Sign Via SMS
                                                                                                                </asp:LinkButton>
                                                                                                            </div>

                                                                                                            <div class="col-md-4">
                                                                                                                <asp:LinkButton ID="lnkEmail" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lnkEmail_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>Cust`s Sign Via Email
                                                                                                                </asp:LinkButton>
                                                                                                            </div>

                                                                                                            <div class="col-md-4">
                                                                                                                <asp:LinkButton ID="lnksendbrochures" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lnksendbrochures_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>  Send Quote
                                                                                                                </asp:LinkButton>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="graybgarea panel-body col-md-6">
                                                                                    <div class="row" style="margin-right: 0px;">
                                                                                        <div id="DivDocument" runat="server">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <div class="widget-header bordered-bottom bordered-blue">
                                                                                                    <span class="widget-caption">Upload Documents</span>

                                                                                                </div>
                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanelDocument">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name disblock">
                                                                                                                        <label class="control-label">
                                                                                                                            Document Title<span class="symbol required"></span>
                                                                                                                        </label>
                                                                                                                    </span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <asp:DropDownList ID="ddlDocument" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                            AppendDataBoundItems="true" onChange="validate()">
                                                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDocument" runat="server" ErrorMessage="This value is required."
                                                                                                                            ControlToValidate="ddlDocument" Display="Dynamic" ValidationGroup="DocUpload"></asp:RequiredFieldValidator>
                                                                                                                    </div>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-8" style="margin-top: 12px!important">
                                                                                                                <div class="form-group dateimgarea fileuploadmain">
                                                                                                                    <div class="checkbox-info checkbox">
                                                                                                                        <span runat="server" class="floatleft" id="divMP">
                                                                                                                            <asp:HyperLink ID="lblDocument" runat="server" Target="_blank"></asp:HyperLink>
                                                                                                                            <span class="file-input btn btn-azure btn-file">
                                                                                                                                <asp:FileUpload ID="fuDocument" runat="server" onChange="validate()" />
                                                                                                                            </span>
                                                                                                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidatorFuDocument" runat="server" ControlToValidate="fuDocument"
                                                                                                                                ValidationGroup="DocUpload" ValidationExpression="^.+(.pdf|.PDF)$" ForeColor="Red"
                                                                                                                                Display="Dynamic" ErrorMessage=".pdf only" class="error_text"></asp:RegularExpressionValidator>--%>
                                                                                                                            <span id="errorMsg" style="color: red"></span>
                                                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*" ForeColor="Red"
                                                                                                                                ControlToValidate="fuDocument" Display="Dynamic" ValidationGroup="DocUpload"></asp:RequiredFieldValidator>
                                                                                                                        </span>

                                                                                                                        <span class="dateimg">
                                                                                                                            <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                                                                Text="Upload" ID="btnDocUpload" ValidationGroup="DocUpload" OnClick="btnDocUpload_Click" /></span>
                                                                                                                    </div>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="row finalgrid" id="DivDocumentView" runat="server" visible="false">
                                                                                                                    <div class="col-md-12" runat="server" id="divDocRpt">
                                                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                            <thead>
                                                                                                                                <tr>
                                                                                                                                    <th style="width: 80%; text-align: left">Document
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Download
                                                                                                                                    </th>
                                                                                                                                </tr>
                                                                                                                            </thead>
                                                                                                                            <asp:Repeater ID="rptDocumemt" runat="server" OnItemDataBound="rptDocumemt_ItemDataBound">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 80%; text-align: left">
                                                                                                                                            <%#Eval("Document")%>
                                                                                                                                        </td>

                                                                                                                                        <td style="width: 25%; text-align: center;">
                                                                                                                                            <asp:HiddenField ID="hndID" runat="server" Value='<%#Eval("ID") %>' />
                                                                                                                                            <asp:HyperLink ID="hypDoc1" runat="server" Target="_blank">
                                                                                                                                                <asp:Image ID="imgDoc1" runat="server" ImageUrl="~/images/icon_document_downalod.png" />
                                                                                                                                            </asp:HyperLink>
                                                                                                                                        </td>

                                                                                                                                    </tr>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:Repeater>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="graybgarea panel-body col-md-6">
                                                                                    <div class="row" style="margin-left: -2px; margin-right: 1px;">
                                                                                        <div id="DivExportControl" runat="server">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <asp:Panel runat="server" ID="PanBtnExportControl">
                                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                                        <span class="widget-caption">Create Export Control Form</span>
                                                                                                        <div class="pull-right" style="margin-top: 3px; margin-right: 3px;">
                                                                                                            <asp:LinkButton ID="btnCreateExportControlForm" runat="server" class="btn btn-primary btn-sm" OnClick="btnCreateExportControlForm_Click"
                                                                                                                CausesValidation="false"><i class="btn-label fa fa-file-pdf-o"></i>Generate Export Control Form</asp:LinkButton>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>

                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanExportControl">
                                                                                                         <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="row finalgrid" id="DivExportCont" runat="server" visible="false">
                                                                                                                    <div class="col-md-12" runat="server" id="divExpCont">
                                                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                            <thead>
                                                                                                                                <tr>
                                                                                                                                    <th style="width: 25%; text-align: center">Date
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Doc No.
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 15%; text-align: center">Signed
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Document
                                                                                                                                    </th>
                                                                                                                                    <%--<th style="width: 20%; text-align: center">Email
                                                                                                                                    </th>--%>
                                                                                                                                </tr>
                                                                                                                            </thead>
                                                                                                                            <asp:Repeater ID="rptExportControl" runat="server" OnItemCommand="rptExportControl_ItemCommand">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 25%; text-align: center">
                                                                                                                                            <%#Eval("CreatedOn", "{0:ddd - dd MMM yyyy}")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 20%; text-align: center">
                                                                                                                                            <%#Eval("id")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 15%; text-align: center;">
                                                                                                                                            <asp:Image ID="ImgEC" runat="server" ImageUrl="~/images/check.png" Visible='<%#Eval("Signature").ToString() == "1" ? true : false %>'/>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 25%; text-align: center;">
                                                                                                                                            <%--<asp:HiddenField ID="hndProjectQuoteID" runat="server" Value='<%#Eval("ProjectQuoteID") %>' />--%>
                                                                                                                                            <%--<asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                                                                                                <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/icon_document_downalod.png" />
                                                                                                                                            </asp:HyperLink>--%>
                                                                                                                                            <asp:ImageButton ID="imgDoc1" runat="server" ImageUrl="~/images/icon_document_downalod.png" CommandName="DownloadDoc" CommandArgument='<%#Eval("id")%>' />
                                                                                                                                        </td>
                                                                                                                                        <%--<td style="width: 20%; text-align: center">
                                                                                                                                            <asp:LinkButton ID="btnSendAMail" runat="server" Visible="false" CommandName="MailAttachedPDf" CommandArgument='<%#Eval("ProjectQuoteID")%>' CssClass="addPreloader">
                                                                                               <i class="fa fa-envelope-o fa-lg"></i>
                                                                                                                                            </asp:LinkButton>
                                                                                                                                        </td>--%>
                                                                                                                                    </tr>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:Repeater>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>

                                                                                                    <div class="row">
                                                                                                            <div class="col-md-4">
                                                                                                                <asp:LinkButton ID="lbtnExportControlSMS" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lbtnExportControlSMS_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>  Cust`s Sign Via SMS
                                                                                                                </asp:LinkButton>
                                                                                                            </div>

                                                                                                            <div class="col-md-4">
                                                                                                                <asp:LinkButton ID="lbtnExportControlEmail" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lbtnExportControlEmail_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>Cust`s Sign Via Email
                                                                                                                </asp:LinkButton>
                                                                                                            </div>

                                                                                                            <%--<div class="col-md-4">
                                                                                                                <asp:LinkButton ID="LinkButton8" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lnksendbrochures_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>  Send Quote
                                                                                                                </asp:LinkButton>
                                                                                                            </div>--%>
                                                                                                        </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                 <div class="graybgarea panel-body col-md-6">
                                                                                     <div class="row" style="margin-right: 0px;">
                                                                                         <div id="DivFeedinTariff" runat="server">
                                                                                             <div class="widget flat radius-bordered borderone">
                                                                                                 <asp:Panel runat="server" ID="PanBtnFeedinTariff">
                                                                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                                                                        <span class="widget-caption">Create Feed In Tariff</span>
                                                                                                        <div class="pull-right" style="margin-top: 3px; margin-right: 3px;">
                                                                                                            <asp:LinkButton ID="btnFeedinTariff" runat="server" class="btn btn-primary btn-sm" OnClick="btnFeedinTariff_Click"
                                                                                                                CausesValidation="false"><i class="btn-label fa fa-file-pdf-o"></i>Generate Feed In Tariff</asp:LinkButton>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>

                                                                                                  <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanFeedinTariff">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="row finalgrid" id="DivFeed" runat="server" visible="false">
                                                                                                                    <div class="col-md-12" runat="server" id="DivFT">
                                                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                            <thead>
                                                                                                                                <tr>
                                                                                                                                    <th style="width: 25%; text-align: center">Date
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Doc No.
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 15%; text-align: center">Signed
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%; text-align: center">Document
                                                                                                                                    </th>
                                                                                                                                    <%--<th style="width: 20%; text-align: center">Email
                                                                                                                                    </th>--%>
                                                                                                                                </tr>
                                                                                                                            </thead>
                                                                                                                            <asp:Repeater ID="rptFeedInTariff" runat="server" OnItemCommand="rptFeedInTariff_ItemCommand">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 25%; text-align: center">
                                                                                                                                            <%#Eval("CreatedOn", "{0:ddd - dd MMM yyyy}")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 20%; text-align: center">
                                                                                                                                            <%#Eval("id")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 15%; text-align: center;">
                                                                                                                                            <asp:Image ID="ImgFT" runat="server" ImageUrl="~/images/check.png" Visible='<%#Eval("Signature").ToString() == "1" ? true : false %>'/>
                                                                                                                                            <%--<asp:Image ID="Image2" runat="server" ImageUrl="~/images/check.png" Visible="false" />--%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 25%; text-align: center;">
                                                                                                                                           <%-- <asp:HiddenField ID="hndProjectQuoteID" runat="server" Value='<%#Eval("ProjectQuoteID") %>' />--%>
                                                                                                                                                <asp:ImageButton ID="imgDoc" runat="server" ImageUrl="~/images/icon_document_downalod.png" CommandName="DownloadDoc" CommandArgument='<%#Eval("id")%>' />
                                                                                                                                        </td>
                                                                                                                                        <%--<td style="width: 20%; text-align: center">
                                                                                                                                            <asp:LinkButton ID="btnSendAMail" runat="server" Visible="false" CommandName="MailAttachedPDf" CommandArgument='<%#Eval("ProjectQuoteID")%>' CssClass="addPreloader">
                                                                                               <i class="fa fa-envelope-o fa-lg"></i>
                                                                                                                                            </asp:LinkButton>
                                                                                                                                        </td>--%>
                                                                                                                                    </tr>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:Repeater>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>

                                                                                                      <div class="row">
                                                                                                            <div class="col-md-4">
                                                                                                                <asp:LinkButton ID="lbtnFeedInTariffSMS" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lbtnFeedInTariffSMS_Click">
                                                                                                                    <i class="glyphicon glyphicon-envelope"></i>  Cust`s Sign Via SMS
                                                                                                                </asp:LinkButton>
                                                                                                            </div>

                                                                                                            <div class="col-md-4">
                                                                                                                <asp:LinkButton ID="lbtnFeedInTariffEmail" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lbtnFeedInTariffEmail_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>Cust`s Sign Via Email
                                                                                                                </asp:LinkButton>
                                                                                                            </div>

                                                                                                            <%--<div class="col-md-4">
                                                                                                                <asp:LinkButton ID="LinkButton11" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false"
                                                                                                                    class="btn btn-labeled btn-success addPreloader" OnClick="lnksendbrochures_Click">
                                                                                                    <i class="glyphicon glyphicon-envelope"></i>  Send Quote
                                                                                                                </asp:LinkButton>
                                                                                                            </div>--%>
                                                                                                        </div>
                                                                                                </div>
                                                                                             </div>
                                                                                        </div>
                                                                                     </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="graybgarea panel-body" id="DivMainInvoiceDetails" runat="server">
                                                                                <div class="row">
                                                                                    <div id="DivInvoiceDetails" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Invoice Details</span>
                                                                                                <div class="pull-right" style="margin-top: 3px; margin-right: 3px;">

                                                                                                    <asp:LinkButton ID="btnOpenInvoice" runat="server" CssClass="btn btn-primary btn-xs" CausesValidation="false"
                                                                                                        OnClick="btnOpenInvoice_Click"><i class="fa fa-print"></i> Tax Invoice</asp:LinkButton>

                                                                                                    <asp:LinkButton ID="btnPrintReceipt" runat="server" CssClass="btn btn-primary btn-xs" CausesValidation="false"
                                                                                                        OnClick="btnPrintReceipt_Click"><i class="fa fa-print"></i> Payment Receipt</asp:LinkButton>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanInvoiceDetails">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Total Cost </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtTotalCostInvoice" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Total Paid </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtPaidDate" runat="server" Enabled="false" Text="0" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Bal Owing </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtBalOwing" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Payment Status </label>
                                                                                                                </span><span class="amtvaluebox">
                                                                                                                    <b>
                                                                                                                        <asp:Label ID="lblPaymentStatus" runat="server"></asp:Label></b>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-2">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Pay Date </label>
                                                                                                                </span>
                                                                                                                <div class="input-group date datetimepicker7">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtInvoicePayDate" runat="server" class="form-control" ValidationGroup="InvDetails">
                                                                                                                    </asp:TextBox>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayDate" runat="server"
                                                                                                                        ValidChars="/">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*" CssClass="" ForeColor="red"
                                                                                                                        ControlToValidate="txtInvoicePayDate" Display="Dynamic" ValidationGroup="InvDetails" InitialValue=""></asp:RequiredFieldValidator>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Total Paid </label>
                                                                                                                </span><span><%--AutoPostBack="true" OnTextChanged="txtInvoicePayTotal_TextChanged"--%>
                                                                                                                    <asp:TextBox ID="txtInvoicePayTotal" runat="server" class="form-control modaltextbox"
                                                                                                                        onchange="InvoicePayTotalChange(this.value)"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtInvoicePayTotal"
                                                                                                                        Display="Dynamic" ErrorMessage="Please enter a number" ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="InvDetails"></asp:RegularExpressionValidator>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayTotal" runat="server"
                                                                                                                        ValidChars=".,-">
                                                                                                                    </cc1:FilteredTextBoxExtender>

                                                                                                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control modaltextbox dnone"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-1">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">GST </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtInvoicePayGST" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                                                                                                        ControlToValidate="txtInvoicePayGST" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                                                        ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="InvDetails"></asp:RegularExpressionValidator>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayGST" runat="server"
                                                                                                                        ValidChars=".">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-1">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Pay By </label>
                                                                                                                </span><span><%--OnSelectedIndexChanged="ddlInvoicePayMethodID_SelectedIndexChanged" AutoPostBack="true" --%>
                                                                                                                    <asp:DropDownList ID="ddlInvoicePayMethodID" Width="100px" CssClass="myval"
                                                                                                                        runat="server" AppendDataBoundItems="true" onchange="InvoicePayMethodIDChange(this.value)">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    </asp:DropDownList>

                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPayBy" runat="server" ErrorMessage="" CssClass=""
                                                                                                                        ControlToValidate="ddlInvoicePayMethodID" Display="Dynamic" ValidationGroup="InvDetails" InitialValue=""></asp:RequiredFieldValidator>
                                                                                                                    <div style="height: 5px"></div>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2" id="DivTransactionCode" runat="server" style="display: none">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Transaction Code</label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtTransactionCode" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtTransactionCode"
                                                                                                                        WatermarkText="Transaction Code" />

                                                                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                                                                            ControlToValidate="txtReceiptNumber" ValidationGroup="InvDetails" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2" id="DivReceiptNo" runat="server" style="display: none">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Receipt No. </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtReceiptNumber" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtReceiptNumber"
                                                                                                                        WatermarkText="Receipt No" />

                                                                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                                                                            ControlToValidate="txtReceiptNumber" ValidationGroup="InvDetails" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-1">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">S/Chg </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtCCSurcharge" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtCCSurcharge"
                                                                                                                        Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$" ValidationGroup="InvDetails"></asp:RegularExpressionValidator>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtCCSurcharge" runat="server"
                                                                                                                        ValidChars=".">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-1">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Rec By </label>
                                                                                                                </span><span>
                                                                                                                    <asp:DropDownList ID="ddlRecBy" runat="server" AppendDataBoundItems="true" Width="120px" CssClass="myval"
                                                                                                                        Enabled="false">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2" style="margin-top: 23px!important;">
                                                                                                            <div class="form-group ">
                                                                                                                <asp:HiddenField ID="hndMode" runat="server" Value="" />
                                                                                                                <asp:HiddenField ID="hndInvoicePaymentID" runat="server" />
                                                                                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                                                    Text="Save" ID="btnInvoiceSave" OnClick="btnInvoiceSave_Click" ValidationGroup="InvDetails" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12" runat="server">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Notes</label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtInovoiceNotes" runat="server" class="form-control modaltextbox" placeholder="Notes" TextMode="MultiLine"></asp:TextBox>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="InvalidChars" FilterType="Custom" TargetControlID="txtInovoiceNotes" runat="server"
                                                                                                                        InvalidChars="'">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="InvalidChars" FilterType="Custom" TargetControlID="txtInovoiceNotes" runat="server"
                                                                                                                        InvalidChars='"'>
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12">
                                                                                                            <div class="row finalgrid" id="divPaymentDetails" runat="server" visible="false">
                                                                                                                <div class="col-md-12" runat="server" id="divPayment">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                        <thead>
                                                                                                                            <tr>
                                                                                                                                <th style="width: 15%; text-align: center">Pay Date</th>
                                                                                                                                <th style="width: 15%; text-align: center">Total Pay</th>
                                                                                                                                <th style="width: 10%; text-align: center">GST</th>
                                                                                                                                <th style="width: 15%; text-align: center">Pay By</th>
                                                                                                                                <th style="text-align: center">Receipt No</th>
                                                                                                                                <th style="width: 10%; text-align: center">S/Chg</th>
                                                                                                                                <th style="width: 15%; text-align: center">Rec By</th>
                                                                                                                                <th style="width: 15%; text-align: center">Verified By</th>
                                                                                                                                <th style="width: 15%; text-align: center">Notes</th>
                                                                                                                                <th style="width: 25%; text-align: center">Action</th>
                                                                                                                            </tr>
                                                                                                                        </thead>
                                                                                                                        <asp:Repeater ID="rptPaymentDetails" runat="server" OnItemCommand="rptPaymentDetails_ItemCommand">
                                                                                                                            <ItemTemplate>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 15%; text-align: center">
                                                                                                                                        <%# Eval("InvoicePayDate", "{0:dd-MMM-yyyy}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("InvoicePayTotal", "{0:0.00}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 10%; text-align: center;">
                                                                                                                                        <%# Eval("InvoicePayGST", "{0:0.00}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("InvoicePayMethod") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="text-align: center;">
                                                                                                                                        <%# Eval("ReceiptNumber") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 10%; text-align: center;">
                                                                                                                                        <%# Eval("CCSurcharge", "{0:0.00}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("RecBy") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("VerifiedByName") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <asp:Label Width="100px" runat="server"
                                                                                                                                            data-toggle="tooltip" data-placement="top" data-original-title='<%# Eval("PaymentNote") %>'><%# Eval("PaymentNote") %></asp:Label>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 25%; text-align: center;">
                                                                                                                                        <asp:LinkButton ID="btnEdit" runat="server" Visible='<%#Eval("VerifiedByName").ToString()=="" || Roles.IsUserInRole("Administrator") ? true:false%>' CommandName="Edit" CssClass="btn-primary btn btn-xs" CausesValidation="false"
                                                                                                                                            CommandArgument='<%#Eval("InvoicePaymentID")%>'>
                                                                                                                                    <i class="fa fa-pen"></i> Edit
                                                                                                                                        </asp:LinkButton>
                                                                                                                                        <asp:LinkButton ID="lbtnDelete" runat="server" Visible='<%#Eval("VerifiedByName").ToString()=="" || Roles.IsUserInRole("Administrator") ? true:false%>' CommandName="Delete" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                                                                                                            CommandArgument='<%#Eval("InvoicePaymentID")%>'>
                                                                                                                                    <i class="fa fa-trash"></i> Delete
                                                                                                                                        </asp:LinkButton>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:Repeater>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="graybgarea panel-body">
                                                                                <div class="row">
                                                                                    <div id="Div4" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Quote Details</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanelActiveDate">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <asp:Label ID="Label7" runat="server" class="  control-label">
                                                                                                        Deposit Rec</asp:Label></span>
                                                                                                                <div class="input-group date datetimepicker1 ">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtDepositReceived" runat="server" class="form-control" Width="125px">
                                                                                                                    </asp:TextBox>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <asp:Label ID="Label8" runat="server" class="control-label">
                                                                                                        Active Date</asp:Label></span>
                                                                                                                <div id="DivActiveDate" class="input-group date datetimepicker8" runat="server">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <%--<asp:TextBox ID="txtActiveDate" runat="server" class="form-control disble" Width="125px" onkeydown="event.preventDefault()">
                                                                                                            </asp:TextBox>--%>
                                                                                                                    <asp:TextBox ID="txtActiveDate" runat="server" class="form-control" Width="125px">
                                                                                                                    </asp:TextBox>
                                                                                                                    <div style="padding-top: 7px">
                                                                                                                        <asp:Label runat="server" ID="lblLastActiveEmp" Style="padding-left: 10px" />
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="form-group dateimgarea fileuploadmain col-md-4" style="margin-top: 26px!important;">
                                                                                                            <span>
                                                                                                                <span>
                                                                                                                    <label for="<%=chkactive.ClientID %>" class="control-label">
                                                                                                                        <asp:CheckBox ID="chkactive" runat="server" />
                                                                                                                        <span class="text">Ready to Active</span>
                                                                                                                    </label>

                                                                                                                </span>
                                                                                                            </span>
                                                                                                            <div class="clear">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12 textcenterbutton">
                                                                            <div style="text-align: center;">
                                                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                    Text="Save" ID="btnSaveActiveDate" OnClick="btnSaveActiveDate_Click" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnDocUpload" />
                                                <asp:PostBackTrigger ControlID="btnSQUpload" />
                                                <asp:PostBackTrigger ControlID="btnOpenInvoice" />
                                                <asp:PostBackTrigger ControlID="btnPrintReceipt" />
                                                <asp:PostBackTrigger ControlID="btnCreateNewQoute" />
                                                <asp:PostBackTrigger ControlID="rptExportControl" />
                                                <asp:PostBackTrigger ControlID="rptFeedInTariff" />
                                               <%-- <asp:PostBackTrigger ControlID="btnRetailerStatement" />--%>

                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>

                            <cc1:TabPanel ID="TabRefund" runat="server" HeaderText="Refund">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="Panel1">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanRefund">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <div class="paddall">
                                                                    <div class="row">
                                                                        <div class="col-md-12">

                                                                            <div class="graybgarea panel-body">
                                                                                <div class="row">
                                                                                    <div id="DivRefund" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Refund Details</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanRefund">

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Option
                                                                                                                    </label>
                                                                                                                </span>
                                                                                                                <span class="dateimg" style="width: 150px;">
                                                                                                                    <asp:DropDownList ID="ddlOption" runat="server" AppendDataBoundItems="true" Width="100px"
                                                                                                                        aria-controls="DataTables_Table_0" CssClass="form-control myval">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div6" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Amount
                                                                                                                    </label>
                                                                                                                </span><span class="dateimg">
                                                                                                                    <asp:TextBox ID="txtAmount" runat="server" MaxLength="7" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div10" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Bank Name
                                                                                                                    </label>
                                                                                                                </span><span class="dateimg">
                                                                                                                    <asp:TextBox ID="txtbankname" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Account Name
                                                                                                                    </label>
                                                                                                                </span>
                                                                                                                <span class="dateimg" style="width: 150px;">
                                                                                                                    <asp:TextBox ID="txtAccName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div11" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        BSBNo
                                                                                                                    </label>
                                                                                                                </span><span class="dateimg">
                                                                                                                    <asp:TextBox ID="txtBSBNo" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div13" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Account No
                                                                                                                    </label>
                                                                                                                </span><span class="dateimg">
                                                                                                                    <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Notes
                                                                                                                    </label>
                                                                                                                </span>
                                                                                                                <span class="dateimg" style="width: 150px;">
                                                                                                                    <asp:TextBox ID="txtNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Columns="8" Rows="5"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12">
                                                                                                            <div class="row finalgrid" id="DivGridRefund" runat="server" visible="false">
                                                                                                                <div class="col-md-12" runat="server" id="DivGridRedundData">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                        <thead>
                                                                                                                            <tr>
                                                                                                                                <th style="width: 15%; text-align: center">Option</th>
                                                                                                                                <th style="width: 15%; text-align: center">Amount</th>
                                                                                                                                <th style="width: 10%; text-align: center">Account No</th>
                                                                                                                                <th style="width: 15%; text-align: center">Account Name</th>
                                                                                                                                <th style="width: 10%; text-align: center">BSBNo</th>
                                                                                                                                <th style="width: 15%; text-align: center">Notes</th>
                                                                                                                                <th style="width: 15%; text-align: center">Previous Status</th>
                                                                                                                                <th style="width: 25%; text-align: center">Action</th>
                                                                                                                            </tr>
                                                                                                                        </thead>
                                                                                                                        <asp:HiddenField runat="server" ID="hndRefundID" />
                                                                                                                        <asp:HiddenField runat="server" ID="hndRefundMode" />
                                                                                                                        <asp:Repeater ID="rptRefund" runat="server" OnItemCommand="rptRefund_ItemCommand">
                                                                                                                            <ItemTemplate>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 15%; text-align: center">
                                                                                                                                        <%# Eval("OptionName") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%#Eval("Amount","{0:0.00}")%>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 10%; text-align: center;">
                                                                                                                                        <%#Eval("AccNo")%>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%#Eval("AccName")%>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 10%; text-align: center;">
                                                                                                                                        <%#Eval("BSBNo")%>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%#Eval("Notes")%>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%#Eval("ProjectStatus")%>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 25%; text-align: center;">
                                                                                                                                        <asp:LinkButton ID="btnEdit" runat="server" Visible='<%#Convert.ToString(Eval("CanChange"))=="1"?false:true%>' CommandName="Edit" CssClass="btn-primary btn btn-xs" CausesValidation="false"
                                                                                                                                            CommandArgument='<%#Eval("RefundID")%>'>
                                                                                                                                    <i class="fa fa-pen"></i> Edit
                                                                                                                                        </asp:LinkButton>
                                                                                                                                        <asp:LinkButton ID="lbtnDelete" runat="server" Visible='<%#Convert.ToString(Eval("CanChange"))=="1"?false:true%>'
                                                                                                                                            CommandName="Delete" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                                                                                                            CommandArgument='<%#Eval("RefundID")%>'>
                                                                                                                                    <i class="fa fa-trash"></i> Delete
                                                                                                                                        </asp:LinkButton>

                                                                                                                                        <asp:LinkButton runat="server" ID="lnkPrint" CssClass="btn btn-primary btn-xs Print" CommandArgument='<%#Eval("RefundID") %>'
                                                                                                                                            CommandName="Print" CausesValidation="false">
                                                                                                                                            <i class="fa fa-file-pdf-o"></i> Refund
                                                                                                                                        </asp:LinkButton>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:Repeater>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12 textcenterbutton">
                                                                            <div style="text-align: center;">
                                                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                    Text="Save" ID="btnrefundSave" OnClick="btnrefundSave_Click" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="rptRefund" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>

                            <cc1:TabPanel ID="TabBooking" runat="server" HeaderText="Booking">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="Panel3">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <div class="paddall">
                                                                    <section class="row m-b-md">
                                                                        <div class="col-sm-12 minhegihtarea stcpage popuoformbox">
                                                                            <div class="contactsarea">
                                                                                <div class="messesgarea">
                                                                                    <div class="alert alert-success" id="Div8" runat="server" visible="false">
                                                                                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="Div9" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Literal ID="litmessage" runat="server" Text="Transaction Failed."></asp:Literal></strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="DivInstalldetails" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;Please enter installation plan data.</strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="DivDocNotVerified" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;Your documents are not verified.</strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="divActive" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;Your Project is not Active.</strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="divVicAppRefRebate" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;Please enter Vic Rebate App Ref.</strong>
                                                                                    </div>
                                                                                    <div class="alert alert-success" id="divformbaymsg" runat="server" visible="false">
                                                                                        <i class="icon-ok-sign"></i><strong>&nbsp;<asp:Label runat="server" ID="lblformbaymsg"></asp:Label></strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="divinverter" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;Picklist Can not be Generated For this  Inverter.</strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="divpanel" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;Picklist Can not be Generated For this Panel.</strong>
                                                                                    </div>
                                                                                    <div class="alert alert-danger" id="divinstalldate" runat="server" visible="false">
                                                                                        <i class="icon-remove-sign"></i><strong>&nbsp;Your install date should be less then one month to generate picklist</strong>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                    <div class="finaladdupdate printorder">
                                                                        <div id="Div7" runat="server" visible="true">
                                                                            <div class="alert alert-danger" id="divminStockModel4" runat="server" visible="false">
                                                                                <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock,please Remove Item</strong>
                                                                            </div>
                                                                            <div class="alert alert-danger" id="DivPanel1" runat="server" visible="false">
                                                                                <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock For Panel,please Remove Item</strong>
                                                                            </div>
                                                                            <div class="alert alert-danger" id="DivInverter1" runat="server" visible="false">
                                                                                <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock  For Inverter,please Remove Item</strong>
                                                                            </div>
                                                                            <div class="alert alert-danger" id="divPanelQuantity" runat="server" visible="false">
                                                                                <i class="icon-remove-sign"></i><strong>&nbsp;Order Stock is less than Live Stock For Panel</strong>
                                                                            </div>
                                                                            <div class="alert alert-danger" id="divInverterQuantity" runat="server" visible="false">
                                                                                <i class="icon-remove-sign"></i><strong>&nbsp;Order Stock is less than Live Stock For Inverter</strong>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="graybgarea panel-body">
                                                                                    <div class="row">
                                                                                        <div id="DivSiteDetailsBK" runat="server">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <div class="widget-header bordered-bottom bordered-blue">
                                                                                                    <span class="widget-caption">Site Details</span>
                                                                                                </div>
                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanSiteDetails">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-6">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <label class="control-label">
                                                                                                                            Install Site
                                                                                                                        </label>
                                                                                                                    </span><span>
                                                                                                                        <asp:TextBox ID="txtBkInstallSite" runat="server" Enabled="false" Style="width: 100%!important; max-width: 100%!important;"></asp:TextBox>
                                                                                                                    </span>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-6">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <label class="control-label">
                                                                                                                            System Details
                                                                                                                        </label>
                                                                                                                    </span><span class="dateimg">
                                                                                                                        <asp:TextBox ID="txtBKSysDetails" runat="server" Enabled="false" Style="width: 100%!important; max-width: 100%!important;"></asp:TextBox>
                                                                                                                    </span>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div id="DivBookingDetails" runat="server">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <div class="widget-header bordered-bottom bordered-blue">
                                                                                                    <span class="widget-caption">Booking Details</span>
                                                                                                </div>
                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanBookingDetails">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-2">
                                                                                                                <div class="form-group">
                                                                                                                    <span></span>
                                                                                                                    <div class=" checkbox-info checkbox" style="padding-top: 20px;">
                                                                                                                        <span class="fistname">
                                                                                                                            <asp:Label ID="lbltesting" runat="server"></asp:Label>
                                                                                                                            <label for="<%=chkElecDistOK.ClientID %>" class="control-label">
                                                                                                                                <asp:CheckBox ID="chkElecDistOK" runat="server" />
                                                                                                                                <span class="text">Elec Dist OK  &nbsp;</span>
                                                                                                                            </label>
                                                                                                                        </span>
                                                                                                                    </div>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-2">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label9" runat="server" class="  control-label">
                                                                                                        Elec Dist Approved</asp:Label></span>
                                                                                                                    <div class="input-group date datetimepicker1 ">
                                                                                                                        <span class="input-group-addon">
                                                                                                                            <span class="fa fa-calendar"></span>
                                                                                                                        </span>
                                                                                                                        <asp:TextBox ID="txtElecDistApproved" runat="server" class="form-control">
                                                                                                                        </asp:TextBox>

                                                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEDA" runat="server" ErrorMessage=""
                                                                                                                            Visible="false" ValidationGroup="preinst" ControlToValidate="txtElecDistApproved"
                                                                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-2">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label10" runat="server" class="  control-label">
                                                                                                        Install</asp:Label></span>
                                                                                                                    <div class="input-group date datetimepicker1 ">
                                                                                                                        <span class="input-group-addon">
                                                                                                                            <span class="fa fa-calendar"></span>
                                                                                                                        </span>
                                                                                                                        <asp:TextBox ID="txtInstallBookingDate" runat="server" class="form-control">
                                                                                                                        </asp:TextBox>

                                                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorIBDate" runat="server" ControlToValidate="txtInstallBookingDate" CssClass=""
                                                                                                                            ValidationGroup="preinst" ErrorMessage="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-2">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label11" runat="server" class="  control-label">
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="input-group" style="padding-top: 5px!important">
                                                                                                                        <asp:LinkButton ID="btnRemoveInst" Style="width: 160px" runat="server" CausesValidation="false"
                                                                                                                            class="btn btn-labeled btn-danger" OnClick="btnRemoveInst_Click">
                                                                                                                        <i class="btn-label fa fa-trash"></i>Remove Install</asp:LinkButton>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div id="DivInstallerDetailsBK" runat="server">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <div class="widget-header bordered-bottom bordered-blue">
                                                                                                    <span class="widget-caption">Installer Details</span>
                                                                                                </div>
                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanInstallerDetails">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label12" runat="server" class="control-label">
                                                                                                                        Stock Allocation Store
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <asp:HiddenField ID="hndProjectID" runat="server" />
                                                                                                                        <asp:DropDownList ID="ddlStockAllocationStore" runat="server" AppendDataBoundItems="true"
                                                                                                                            aria-controls="DataTables_Table_0" class="myval" AutoPostBack="true" OnSelectedIndexChanged="ddlStockAllocationStore_SelectedIndexChanged">
                                                                                                                            <%--onchange="BindPanInverter(this.value);"--%>
                                                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-3">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label16" runat="server" class="  control-label">
                                                                                                                        Installer
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <%--onchange="InstallerChange(this.value);"--%>
                                                                                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                                                                            aria-controls="DataTables_Table_0" class="myval" AutoPostBack="true" OnSelectedIndexChanged="ddlInstaller_SelectedIndexChanged">
                                                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-3">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label13" runat="server" class="  control-label">
                                                                                                                        Designer
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <asp:DropDownList ID="ddlDesigner" runat="server" AppendDataBoundItems="true"
                                                                                                                            aria-controls="DataTables_Table_0" class="myval">
                                                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-3">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label14" runat="server" class="  control-label">
                                                                                                                        Electrician
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <asp:DropDownList ID="ddlElectrician" runat="server" AppendDataBoundItems="true"
                                                                                                                            aria-controls="DataTables_Table_0" class="myval">
                                                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="row">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label15" runat="server" class="control-label">
                                                                                                                        Project Notes
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <asp:TextBox ID="txtBKProjectNotes" runat="server" TextMode="MultiLine" Height="120px" Width="330px"
                                                                                                                            onkeyDown="checkTextAreaMaxLength(this,event,'350');"></asp:TextBox>
                                                                                                                        <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtBKProjectNotes" FilterMode="InvalidChars" InvalidChars="&quot;'">
                                                                                                                        </cc1:FilteredTextBoxExtender>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label17" runat="server" class="  control-label">
                                                                                                                        Installer Notes
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <asp:TextBox ID="txtBKInstallerNotes" runat="server" TextMode="MultiLine" Height="120px" Width="330px"></asp:TextBox>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <asp:Label ID="Label18" runat="server" class="control-label">
                                                                                                                        Notes for Installation Department
                                                                                                                        </asp:Label></span>
                                                                                                                    <div class="drpValidate">
                                                                                                                        <asp:TextBox ID="txtBKMeterInstallerNotes" runat="server" Width="314px"
                                                                                                                            Height="120px" TextMode="MultiLine"></asp:TextBox>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div id="DivPIDetails" runat="server" visible="false">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <div class="widget-header bordered-bottom bordered-blue">
                                                                                                    <span class="widget-caption">Panel/Inverter Details</span>
                                                                                                </div>
                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanPIDetails">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-6">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name ">
                                                                                                                        <label class="control-label">
                                                                                                                            Panel
                                                                                                                        </label>
                                                                                                                    </span>

                                                                                                                    <span>
                                                                                                                        <asp:TextBox ID="txtnoofpanel" runat="server" CssClass="form-control" Enabled="false" Visible="false"></asp:TextBox>
                                                                                                                    </span>
                                                                                                                    <span class="name ">
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            L
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; display: none; text-align: center;">
                                                                                                                            <%--   J--%>P
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            APL
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            SMPL
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            W
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 70px; text-align: center;">
                                                                                                                            T
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 70px; text-align: center;">
                                                                                                                            O
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 70px; text-align: center;">
                                                                                                                            ETA
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            M
                                                                                                                        </label>
                                                                                                                    </span>
                                                                                                                    <asp:HiddenField runat="server" ID="hndPanelID" />
                                                                                                                    <asp:HiddenField runat="server" ID="hndPanelLocation" />
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblPl" runat="server" Width="50px" CssClass="form-control" Visible="false"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblPl" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: none;">
                                                                                                                        <asp:Label ID="lblPj" runat="server" Width="50px" CssClass="form-control"></asp:Label>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblApl" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton ID="lblApl" runat="server" Width="50px" CssClass="form-control"
                                                                                                                            CausesValidation="false" OnClick="lblApl_Click"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%-- <asp:Label ID="lblSMPL" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblSMPL" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblPW" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblPW" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 70px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%-- <asp:Label ID="lblPt" runat="server" Width="65px" CssClass="form-control" Visible ="false"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblPt" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 70px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblPO" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblPO" Width="70px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 70px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblIo" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lnketap" Width="70px" CssClass="form-control"> </asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%-- <asp:Label ID="lblPm" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblPm" Width="70px" CssClass="form-control"> </asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="col-md-6">
                                                                                                                <div class="form-group">
                                                                                                                    <span class="name">
                                                                                                                        <label class="control-label">
                                                                                                                            Inverter
                                                                                                                        </label>
                                                                                                                    </span>
                                                                                                                    <span>
                                                                                                                        <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" Enabled="false" Visible="false"></asp:TextBox>
                                                                                                                    </span>
                                                                                                                    <span class="name ">
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            L
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center; display: none">
                                                                                                                            <%--J--%>P
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            APL
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            SMPL
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            W
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 70px; text-align: center;">
                                                                                                                            T
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 70px; text-align: center;">
                                                                                                                            O
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 70px; text-align: center;">
                                                                                                                            ETA
                                                                                                                        </label>
                                                                                                                        <label class="control-label" style="width: 50px; text-align: center;">
                                                                                                                            M
                                                                                                                        </label>
                                                                                                                    </span>
                                                                                                                    <asp:HiddenField runat="server" ID="hndInverterID" />
                                                                                                                    <asp:HiddenField runat="server" ID="hndInverterLocation" />
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <asp:LinkButton ID="lblIl" runat="server" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: none;">
                                                                                                                        <asp:Label ID="lblIj" runat="server" Width="50px" CssClass="form-control"></asp:Label>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblAriseInv" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton ID="lblAriseInv" runat="server" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblSmInv" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton ID="lblSmInv" runat="server" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblIW" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblIW" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 70px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <asp:LinkButton ID="lblIt" runat="server" Width="65px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 70px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblIo" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lblIo" Width="70px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 70px; text-align: center; margin-bottom: 5px; display: inline-block;">
                                                                                                                        <%--<asp:Label ID="lblIo" runat="server" Width="50px" CssClass="form-control"></asp:Label>--%>
                                                                                                                        <asp:LinkButton runat="server" ID="lnketaI" Width="70px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <span style="width: 50px; text-align: center;">
                                                                                                                        <asp:LinkButton ID="lblIm" runat="server" Width="50px" CssClass="form-control"></asp:LinkButton>
                                                                                                                    </span>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div id="DivPickList" runat="server">
                                                                                            <div class="widget flat radius-bordered borderone">
                                                                                                <div class="widget-header bordered-bottom bordered-blue">
                                                                                                    <span class="widget-caption">PickList Details</span>
                                                                                                </div>
                                                                                                <div class="widget-body" id="DivAddPickList" runat="server">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3">
                                                                                                            <span class="name disblock">
                                                                                                                <label>
                                                                                                                    Picklist Category</label>
                                                                                                            </span><span>
                                                                                                                <div class="drpValidate">
                                                                                                                    <asp:DropDownList ID="ddlPickListCategory" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                        AppendDataBoundItems="true">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                    <asp:RequiredFieldValidator ID="RqPickListCategory" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                                                                                                        ValidationGroup="AddPicklist" ControlToValidate="ddlPickListCategory" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                                </div>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                        <div class="col-md-3">
                                                                                                            <span class="name disblock">
                                                                                                                <label>
                                                                                                                    Stock With</label>
                                                                                                            </span><span>
                                                                                                                <div class="drpValidate">
                                                                                                                    <asp:DropDownList ID="ddlStockWith" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                        AppendDataBoundItems="true">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                        <asp:ListItem Value="1">With Installer</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">With Customer</asp:ListItem>
                                                                                                                        <asp:ListItem Value="3">With Transportation</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                                                                                                        ValidationGroup="AddPicklist" ControlToValidate="ddlStockWith" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                                </div>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12" style="position: relative">
                                                                                                            <asp:Repeater ID="rptPickList" runat="server" OnItemDataBound="rptPickList_ItemDataBound">
                                                                                                                <ItemTemplate>
                                                                                                                    <div class="row">
                                                                                                                        <div class="form-group col-md-3">
                                                                                                                            <span class="name disblock">
                                                                                                                                <label>
                                                                                                                                    Stock Category
                                                                                                                                </label>
                                                                                                                            </span><span>
                                                                                                                                <asp:HiddenField ID="hndStockCategoryID" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                                                                                <asp:HiddenField ID="PicklistitemId" runat="server" Value='<%# Eval("PicklistitemId") %>' />
                                                                                                                                <div class="drpValidate">
                                                                                                                                    <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged1"
                                                                                                                                        AutoPostBack="true">
                                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                                    </asp:DropDownList>

                                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                                                                                        ValidationGroup="AddPicklist" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                                                                                                </div>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-3">
                                                                                                                            <span class="name disblock">
                                                                                                                                <label>
                                                                                                                                    Stock Item</label>
                                                                                                                            </span><span>
                                                                                                                                <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                                                                                <div class="drpValidate">
                                                                                                                                    <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged" AutoPostBack="true"
                                                                                                                                        AppendDataBoundItems="true">
                                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                                                                                                                        ValidationGroup="AddPicklist" ControlToValidate="ddlStockItem" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                                                </div>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-2">
                                                                                                                            <span class="name disblock">
                                                                                                                                <label>
                                                                                                                                    Quantity</label>
                                                                                                                            </span><span style="width: 60%">
                                                                                                                                <asp:HiddenField ID="hndQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                                                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" Text='<%# Eval("Qty") %>'
                                                                                                                                    OnTextChanged="txtQuantity_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*" CssClass="" ForeColor="Red"
                                                                                                                                    ValidationGroup="AddPicklist" ControlToValidate="txtQuantity" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-2">
                                                                                                                            <span class="name disblock">
                                                                                                                                <label>
                                                                                                                                    Wal Qty</label>
                                                                                                                            </span>
                                                                                                                            <span style="width: 60%">
                                                                                                                                <asp:TextBox ID="txtWallateQty" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="col-md-1">
                                                                                                                            <div class="form-group" style="padding-top: 5px;">
                                                                                                                                <div class="padtop15">
                                                                                                                                    <span class="name" style="padding-top: 4px!important">
                                                                                                                                        <%--<asp:HiddenField ID="hdnStockOrderItemID" runat="server" Value='<%#Eval("StockOrderItemID") %>' />--%>
                                                                                                                                        <asp:CheckBox ID="chkdelete" runat="server" />
                                                                                                                                        <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                                                                                            <span></span>
                                                                                                                                        </label>
                                                                                                                                        <br />
                                                                                                                                        <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                                                                                        <asp:LinkButton ID="litremovePicklistItem" runat="server" CssClass="btn-danger btn btn-xs" OnClick="litremovePicklistItem_Click" CausesValidation="false">
                                                                                                                                            <i class="fa fa-close"></i> Remove
                                                                                                                                        </asp:LinkButton>
                                                                                                                                        <%--<asp:Button ID="litremovePicklistItem" runat="server" OnClick="litremovePicklistItem_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                                                                                            CausesValidation="false" /><br />--%>
                                                                                                                                    </span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                </ItemTemplate>
                                                                                                            </asp:Repeater>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row">

                                                                                                        <div class="col-md-5">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <asp:Label ID="Label20" runat="server" class="control-label">
                                                                                                                        Reason for generating PickList again
                                                                                                                    </asp:Label></span>
                                                                                                                <div class="drpValidate">
                                                                                                                    <asp:TextBox ID="txtPickListAgainReson" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-5">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <asp:Label ID="Label21" runat="server" class="control-label">
                                                                                                                        Notes for generating PickList again
                                                                                                                    </asp:Label></span>
                                                                                                                <div class="drpValidate">
                                                                                                                    <asp:TextBox ID="txtPicklistAgainNotes" TextMode="MultiLine" runat="server" Width="100%" Height="80px"></asp:TextBox>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12" style="position: relative">
                                                                                                            <asp:LinkButton ID="lnkAddItem" runat="server" CssClass="btn-primary btn btn-xs" OnClick="lnkAddItem_Click" CausesValidation="false">
                                                                                                                <i class="fa fa-plus"></i> Add Item
                                                                                                            </asp:LinkButton>

                                                                                                            <asp:LinkButton ID="btnSavePickList" runat="server" CssClass="btn btn-xs sbtn" OnClick="btnSavePickList_Click"
                                                                                                                ValidationGroup="AddPicklist">
                                                                                                                <i class="fa fa-save"></i> Save
                                                                                                            </asp:LinkButton>

                                                                                                            <asp:LinkButton ID="btnUpdatePicklist" Visible="false" runat="server" CssClass="btn btn-xs sbtn" OnClick="btnUpdatePicklist_Click" CausesValidation="false">
                                                                                                                <i class="fa fa-save"></i> Update
                                                                                                            </asp:LinkButton>

                                                                                                            <asp:LinkButton ID="lnkDownloadPicklist" runat="server" CssClass="btn btn-xs sbtn" OnClick="lnkDownloadPicklist_Click" CausesValidation="false">
                                                                                                                <i class="fa fa-save"></i> Download
                                                                                                            </asp:LinkButton>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="widget-body">
                                                                                                    <asp:Panel runat="server" ID="PanPickList">
                                                                                                        <div class="row finalgrid">
                                                                                                            <div class="col-md-12" runat="server" id="divpl" style="width: 100%">
                                                                                                                <table cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent"
                                                                                                                    style="overflow-x: scroll">
                                                                                                                    <thead>
                                                                                                                        <tr>
                                                                                                                            <th style="width: 5%; text-align: center;">ID
                                                                                                                            </th>
                                                                                                                            <th style="width: 5%; text-align: center;">Picklist Type
                                                                                                                            </th>
                                                                                                                            <th style="width: 5%; text-align: center;">Category
                                                                                                                            </th>
                                                                                                                            <th style="width: 5%; text-align: center;">Stock With
                                                                                                                            </th>
                                                                                                                            <th style="width: 5%; text-align: center;">Generated Date 
                                                                                                                            </th>
                                                                                                                            <%--PickList Generated Date--%>
                                                                                                                            <th style="width: 5%; text-align: center">Install Date
                                                                                                                            </th>
                                                                                                                            <%--InstallationBooked Date--%>
                                                                                                                            <th style="width: 10%; text-align: center">Installer Name  
                                                                                                                            </th>
                                                                                                                            <th style="width: 10%; text-align: center">System Details  
                                                                                                                            </th>
                                                                                                                            <th style="width: 20%; text-align: center">Reason
                                                                                                                            </th>
                                                                                                                            <%-- <th style="width: 20%; text-align: center">Note
                                                        </th>--%>
                                                                                                                            <th style="width: 5%; text-align: center">CreatedBy
                                                                                                                            </th>
                                                                                                                            <th style="width: 7%; text-align: center">Deduct On
                                                                                                                            </th>
                                                                                                                            <th style="text-align: center">&nbsp;
                                                                                                                            </th>
                                                                                                                        </tr>
                                                                                                                    </thead>
                                                                                                                    <asp:HiddenField ID="hndInstallerId" runat="server" />
                                                                                                                    <asp:HiddenField ID="hndPickListID" runat="server" />
                                                                                                                    <asp:HiddenField ID="hndPickListMode" runat="server" />
                                                                                                                    <asp:Repeater ID="rptPickListDetails" runat="server" OnItemCommand="rptPickListDetails_ItemCommand"
                                                                                                                        OnItemDataBound="rptPickListDetails_ItemDataBound">
                                                                                                                        <ItemTemplate>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 5%; text-align: center;">
                                                                                                                                    <%#Eval("ID")%>
                                                                                                                                </td>
                                                                                                                                <td style="width: 5%; text-align: center;">
                                                                                                                                    <%#Eval("pcktype")%>
                                                                                                                                </td>
                                                                                                                                <td style="width: 5%; text-align: center;">
                                                                                                                                    <%#Eval("PickListCategoryName")%>
                                                                                                                                </td>
                                                                                                                                <td style="width: 5%; text-align: center;">
                                                                                                                                    <%#Eval("stockWithName")%>
                                                                                                                                </td>
                                                                                                                                <td style="width: 5%; text-align: center;">
                                                                                                                                    <asp:HiddenField ID="hndpicklistid" runat="server" Value='<%#Eval("ID")%>' />
                                                                                                                                    <%#Eval("PickListDateTime", "{0:dd/MM/yyyy}")%>
                                                                                                                                </td>

                                                                                                                                <td style="width: 5%; text-align: center;">
                                                                                                                                    <%#Eval("InstallBookedDate", "{0:dd/MM/yyyy}")%>
                                                                                                                                </td>
                                                                                                                                <td style="width: 10%; word-break: break-word;">
                                                                                                                                    <%#Eval("InstallerName")%>
                                                                                                                                </td>
                                                                                                                                <td style="width: 10%;">
                                                                                                                                    <%--<%#Eval("PanelQty")+" X " + Eval("Panel") + " Plus " + Eval("InverterQty") + " X " + Eval("Inverter")%>--%>
                                                                                                                                    <%--<%#Eval("SystemDetail")%>--%>
                                                                                                                                    <asp:Label ID="lblstytemdetail" runat="server" Text='<%#Eval("SystemDetail")%>'></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 20%;">
                                                                                                                                    <%#Eval("Reason")%>
                                                                                                                                </td>

                                                                                                                                <td style="width: 5%; text-align: center;">
                                                                                                                                    <asp:Label ID="lblEmpName" Width="70px" runat="server" Text='<%#Eval("CreatedBy1")%>'></asp:Label>
                                                                                                                                </td>

                                                                                                                                <td style="width: 7%;">
                                                                                                                                    <%#Eval("DeductOn", "{0:dd/MM/yyyy}")%>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:LinkButton ID="btnUpdatepicklist" runat="server" CommandName="Edit" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-primary">
                                                                                                                                        <i class="fa fa-edit"></i>
                                                                                                                                    </asp:LinkButton>

                                                                                                                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-danger" Visible="false">
                                                                                                                                        <i class="fa fa-trash"></i>
                                                                                                                                    </asp:LinkButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:Repeater>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="row" id="DivBuuton" runat="server">
                                                                                <div class="col-md-12 textcenterbutton">
                                                                                    <div style="text-align: center;">
                                                                                        <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server" ValidationGroup="preinst"
                                                                                            Text="Save" ID="btnBookingDataSave" OnClick="btnBookingDataSave_Click" />

                                                                                        <asp:DropDownList ID="ddlbtn" runat="server" AppendDataBoundItems="true"
                                                                                            aria-controls="DataTables_Table_0" class="myval">
                                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                                            <%--<asp:ListItem Value="1">New PickList</asp:ListItem>--%>
                                                                                            <%--<asp:ListItem Value="2">GreenBot</asp:ListItem>
                                                                                            <asp:ListItem Value="3">QuickForm</asp:ListItem>--%>
                                                                                            <asp:ListItem Value="4">BridgeSelect</asp:ListItem>
                                                                                            <asp:ListItem Value="5">New GreenBot</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:Button ID="btnGo" runat="server" CausesValidation="false" CssClass="btn btn-info redreq"
                                                                                            OnClick="btnGo_Click" Text="Go" />

                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkDownloadPicklist" />
                                                <asp:PostBackTrigger ControlID="btnGo" />

                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>

                            <cc1:TabPanel ID="TabStock" runat="server" HeaderText="Stock">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanStockDetails">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanelStock">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <div class="paddall">
                                                                    <div class="row">
                                                                        <div class="col-md-12">

                                                                            <div class="graybgarea panel-body">
                                                                                <div class="row">
                                                                                    <div id="DivStockDetails" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Stock Pickup Detail</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanStock">

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div18" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Pickup Date
                                                                                                                    </label>
                                                                                                                    <div class="input-group date datetimepicker1">
                                                                                                                        <span class="input-group-addon">
                                                                                                                            <span class="fa fa-calendar"></span>
                                                                                                                        </span>
                                                                                                                        <asp:TextBox ID="txtpickupdate" runat="server" class="form-control">
                                                                                                                        </asp:TextBox>

                                                                                                                    </div>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Installer Name
                                                                                                                    </label>
                                                                                                                </span>
                                                                                                                <span class="dateimg" style="width: 150px;">
                                                                                                                    <asp:DropDownList ID="ddlInstallerPickUp" runat="server" AppendDataBoundItems="true"
                                                                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                                                                        <asp:ListItem Value="0">Installer Name</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div19" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Pickup Note
                                                                                                                    </label>
                                                                                                                </span><span class="dateimg">
                                                                                                                    <asp:TextBox ID="txtPickupNote" runat="server" CssClass="form-control" TextMode="MultiLine" Columns="8" Rows="5"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Pickup Return Date
                                                                                                                    </label>
                                                                                                                </span>
                                                                                                                <div class="input-group date datetimepicker1">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtPickupReturnDate" runat="server" class="form-control">
                                                                                                                    </asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Installer                                                                                                                   
                                                                                                                    </label>
                                                                                                                </span>
                                                                                                                <span class="dateimg" style="width: 150px;">
                                                                                                                    <asp:DropDownList ID="ddlInstallerPick" runat="server" AppendDataBoundItems="true"
                                                                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                                                                        <asp:ListItem Value="0">Installer</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div21" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <label class="control-label">
                                                                                                                        Pickup Return Note
                                                                                                                    </label>
                                                                                                                </span><span class="dateimg">
                                                                                                                    <asp:TextBox ID="txtPickupReturnNote" runat="server" CssClass="form-control" TextMode="MultiLine" Columns="8" Rows="5"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row" id="divChangePickUp" runat="server" style="margin-top: 5px;">
                                                                                                        <div class="col-sm-12">
                                                                                                            <div class="form-group">
                                                                                                                <%--<div class="row">
                                                                <div class="col-md-5 col-sm-5">
                                                                    <h5> Log</h5>
                                                                </div>
                                                            </div>--%>
                                                                                                                <div class="row finalgrid">
                                                                                                                    <div class="col-md-12" runat="server" id="divreasontochange">
                                                                                                                        <table width="100%" cellpadding="0" id="tblreasontochange" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                                                                                            <thead>
                                                                                                                                <tr>
                                                                                                                                    <th style="width: 10%; text-align: center">ID
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 15%; text-align: center">PickUpDate
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 30%; text-align: center">Reason
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 30%; text-align: center">Note
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 15%; text-align: center">CreatedBy
                                                                                                                                    </th>
                                                                                                                                </tr>
                                                                                                                            </thead>
                                                                                                                            <asp:Repeater ID="rptChangePickUp" runat="server">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <tr>
                                                                                                                                        <td style="width: 10%;">
                                                                                                                                            <%#Eval("picklistid")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 15%;">
                                                                                                                                            <%#Eval("PickUpDate", "{0:dd/MM/yyyy}")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 30%;">
                                                                                                                                            <%#Eval("Reason")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 30%;">
                                                                                                                                            <%#Eval("Notes")%>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 15%;">
                                                                                                                                            <%#Eval("EmpNicName")%>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:Repeater>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12 textcenterbutton">
                                                                            <div style="text-align: center;">
                                                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                    Text="Save" ID="btnStock" OnClick="btnStock_Click" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>

                            <cc1:TabPanel ID="TabPanelInstallDone" runat="server" HeaderText="Install Done">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanInstallDone">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanelInstallDone">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <div class="paddall">
                                                                    <div class="row">
                                                                        <div class="col-md-12">

                                                                            <div class="graybgarea panel-body">
                                                                                <div class="row">
                                                                                    <div id="DivInstallStatus" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Install Status</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanInstallStatus">

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group" id="div20" runat="server">
                                                                                                                <span class="name disblock">
                                                                                                                    <div class=" checkbox-info checkbox">
                                                                                                                        <span class="fistname">
                                                                                                                            <%--  --%>
                                                                                                                            <label for="<%=chkProjectIncomplete.ClientID %>" class="control-label">
                                                                                                                                <asp:CheckBox ID="chkProjectIncomplete" runat="server" OnCheckedChanged="chkProjectIncomplete_CheckedChanged" AutoPostBack="true" />
                                                                                                                                <span class="text">Project Incomplete  &nbsp;</span>
                                                                                                                            </label>
                                                                                                                        </span>
                                                                                                                    </div>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-8">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">
                                                                                                                        Project Incomplete Note
                                                                                                                    </label>
                                                                                                                    <br />
                                                                                                                </span><span class="with44">
                                                                                                                    <asp:TextBox ID="txtIncomplateReason" CssClass="form-control" runat="server"
                                                                                                                        Height="170px" TextMode="MultiLine"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="* Required" ForeColor="Red"
                                                                                                                        ValidationGroup="InstallReq" ControlToValidate="txtIncomplateReason" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                                </span>
                                                                                                                <div class="clear">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>

                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <div id="DivInstallationDetails" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Installation Detail</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanInstallationDetails">

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">
                                                                                                                        Install Completed
                                                                                                                    </label>
                                                                                                                    <div class="input-group date datetimepicker1">
                                                                                                                        <span class="input-group-addon">
                                                                                                                            <span class="fa fa-calendar"></span>
                                                                                                                        </span>
                                                                                                                        <asp:TextBox ID="txtInstallCompleted" runat="server" class="form-control">
                                                                                                                        </asp:TextBox>
                                                                                                                    </div>
                                                                                                                    <div class="clear">
                                                                                                                    </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>

                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <div id="DivPaymentStatus" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Invoice Details</span>
                                                                                                <div class="pull-right" style="margin-top: 3px; margin-right: 3px;">

                                                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary btn-xs" CausesValidation="false"
                                                                                                        OnClick="btnOpenInvoice_Click"><i class="fa fa-print"></i> Tax Invoice</asp:LinkButton>

                                                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-primary btn-xs" CausesValidation="false"
                                                                                                        OnClick="btnPrintReceipt_Click"><i class="fa fa-print"></i> Payment Receipt</asp:LinkButton>

                                                                                                    <asp:LinkButton ID="lnkSendInvoice" runat="server" CssClass="btn btn-primary btn-xs" CausesValidation="false"
                                                                                                        OnClick="lnkSendInvoice_Click"><i class="glyphicon glyphicon-envelope"></i>Send Invoice</asp:LinkButton>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanPaymentStatus">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Total Cost </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtTotalCostInvoiceInstall" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Total Paid </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtPaidDateInstall" runat="server" Enabled="false" Text="0" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Bal Owing </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtBalOwingInstall" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Payment Status </label>
                                                                                                                </span><span class="amtvaluebox">
                                                                                                                    <b>
                                                                                                                        <asp:Label ID="lblPaymentStatusInstall" runat="server"></asp:Label></b>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-2">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Pay Date </label>
                                                                                                                </span>
                                                                                                                <div class="input-group date datetimepicker1 ">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtInvoicePayDateInstall" runat="server" class="form-control">
                                                                                                                    </asp:TextBox>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayDateInstall" runat="server"
                                                                                                                        ValidChars="/">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Total Paid </label>
                                                                                                                </span><span><%--AutoPostBack="true" OnTextChanged="txtInvoicePayTotal_TextChanged"--%>
                                                                                                                    <asp:TextBox ID="txtInvoicePayTotalInstall" runat="server" class="form-control modaltextbox"
                                                                                                                        onchange="InvoicePayTotalChangeInstall(this.value)"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txtInvoicePayTotalInstall"
                                                                                                                        Display="Dynamic" ErrorMessage="Please enter a number" ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="InvDetailsInstall"></asp:RegularExpressionValidator>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayTotalInstall" runat="server"
                                                                                                                        ValidChars=".,-">
                                                                                                                    </cc1:FilteredTextBoxExtender>

                                                                                                                    <asp:TextBox ID="TextBox4" runat="server" class="form-control modaltextbox dnone"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-1">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">GST </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtInvoicePayGSTInstall" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                                                                                                        ControlToValidate="txtInvoicePayGSTInstall" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                                                        ValidationExpression="^-?\d*\.?\d+$" ValidationGroup="InvDetailsInstall"></asp:RegularExpressionValidator>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInvoicePayGSTInstall" runat="server"
                                                                                                                        ValidChars=".">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-1">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Pay By </label>
                                                                                                                </span><span><%--OnSelectedIndexChanged="ddlInvoicePayMethodID_SelectedIndexChanged" AutoPostBack="true" --%>
                                                                                                                    <asp:DropDownList ID="ddlInvoicePayMethodIDInstall" Width="100px" CssClass="myval"
                                                                                                                        runat="server" AppendDataBoundItems="true" onchange="InvoicePayMethodIDChangeInstall(this.value)">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    </asp:DropDownList>

                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                                                                                        ControlToValidate="ddlInvoicePayMethodIDInstall" Display="Dynamic" ValidationGroup="InvDetailsInstall" InitialValue=""></asp:RequiredFieldValidator>
                                                                                                                    <div style="height: 5px"></div>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2" id="DivTransactionCodeInstall" runat="server" style="display: none">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Transaction Code</label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtTransactionCodeInstall" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtTransactionCodeInstall"
                                                                                                                        WatermarkText="Transaction Code" />

                                                                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                                                                            ControlToValidate="txtReceiptNumber" ValidationGroup="InvDetails" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2" id="DivReceiptNoInstall" runat="server" style="display: none">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Receipt No. </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtReceiptNumberInstall" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtReceiptNumberInstall"
                                                                                                                        WatermarkText="Receipt No" />

                                                                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                                                                            ControlToValidate="txtReceiptNumber" ValidationGroup="InvDetails" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-1">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">S/Chg </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtCCSurchargeInstall" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ControlToValidate="txtCCSurchargeInstall"
                                                                                                                        Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$" ValidationGroup="InvDetailsInstall"></asp:RegularExpressionValidator>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtCCSurchargeInstall" runat="server"
                                                                                                                        ValidChars=".">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-2">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Rec By </label>
                                                                                                                </span><span>
                                                                                                                    <asp:DropDownList ID="ddlRecByInstall" runat="server" AppendDataBoundItems="true" Width="120px" CssClass="myval"
                                                                                                                        Enabled="false">
                                                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-1" style="margin-top: 23px!important;">
                                                                                                            <div class="form-group ">
                                                                                                                <asp:HiddenField ID="hndModeInstall" runat="server" Value="" />
                                                                                                                <asp:HiddenField ID="hndInvoicePaymentIDInstall" runat="server" />
                                                                                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                                                    Text="Save" ID="btnInvoiceSaveInstall" ValidationGroup="InvDetailsInstall" OnClick="btnInvoiceSaveInstall_Click" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12" runat="server">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Notes</label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtInvoiceNotesInstall" runat="server" class="form-control modaltextbox" placeholder="Notes" TextMode="MultiLine"></asp:TextBox>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="InvalidChars" FilterType="Custom" TargetControlID="txtInvoiceNotesInstall" runat="server"
                                                                                                                        InvalidChars="'">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="InvalidChars" FilterType="Custom" TargetControlID="txtInvoiceNotesInstall" runat="server"
                                                                                                                        InvalidChars='"'>
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12">
                                                                                                            <div class="row finalgrid" id="divPaymentDetailsInstall" runat="server" visible="false">
                                                                                                                <div class="col-md-12" runat="server" id="divPaymentInstall">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                        <thead>
                                                                                                                            <tr>
                                                                                                                                <th style="width: 15%; text-align: center">Pay Date</th>
                                                                                                                                <th style="width: 15%; text-align: center">Total Pay</th>
                                                                                                                                <th style="width: 10%; text-align: center">GST</th>
                                                                                                                                <th style="width: 15%; text-align: center">Pay By</th>
                                                                                                                                <th style="text-align: center">Receipt No</th>
                                                                                                                                <th style="width: 10%; text-align: center">S/Chg</th>
                                                                                                                                <th style="width: 15%; text-align: center">Rec By</th>
                                                                                                                                <th style="width: 15%; text-align: center">Verified By</th>
                                                                                                                                <th style="width: 15%; text-align: center">Notes</th>
                                                                                                                                <th style="width: 25%; text-align: center">Action</th>
                                                                                                                            </tr>
                                                                                                                        </thead>
                                                                                                                        <asp:Repeater ID="rptPaymentDetailsInstall" runat="server" OnItemCommand="rptPaymentDetailsInstall_ItemCommand">
                                                                                                                            <ItemTemplate>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 15%; text-align: center">
                                                                                                                                        <%# Eval("InvoicePayDate", "{0:dd-MMM-yyyy}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("InvoicePayTotal", "{0:0.00}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 10%; text-align: center;">
                                                                                                                                        <%# Eval("InvoicePayGST", "{0:0.00}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("InvoicePayMethod") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="text-align: center;">
                                                                                                                                        <%# Eval("ReceiptNumber") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 10%; text-align: center;">
                                                                                                                                        <%# Eval("CCSurcharge", "{0:0.00}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("RecBy", "{0:0.00}") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%# Eval("VerifiedByName") %>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <asp:Label Width="100px" runat="server"
                                                                                                                                            data-toggle="tooltip" data-placement="top" data-original-title='<%# Eval("PaymentNote") %>'><%# Eval("PaymentNote") %></asp:Label>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 25%; text-align: center;">
                                                                                                                                        <asp:LinkButton ID="btnEdit" runat="server" Visible='<%#Eval("VerifiedByName").ToString()=="" || Roles.IsUserInRole("Administrator") ? true:false%>' CommandName="Edit" CssClass="btn-primary btn btn-xs" CausesValidation="false"
                                                                                                                                            CommandArgument='<%#Eval("InvoicePaymentID")%>'>
                                                                                                                                    <i class="fa fa-pen"></i> Edit
                                                                                                                                        </asp:LinkButton>
                                                                                                                                        <asp:LinkButton ID="lbtnDelete" runat="server" Visible='<%#Eval("VerifiedByName").ToString()=="" || Roles.IsUserInRole("Administrator") ? true:false%>' CommandName="Delete" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                                                                                                            CommandArgument='<%#Eval("InvoicePaymentID")%>'>
                                                                                                                                    <i class="fa fa-trash"></i> Delete
                                                                                                                                        </asp:LinkButton>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:Repeater>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <div id="DivMeterApplicationDetail" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Meter Application Detail</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanMeterApplicationDetails">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Meter Apply Ref </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtMeterAppliedRef" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Inspector Name </label>
                                                                                                                </span><span>
                                                                                                                    <asp:TextBox ID="txtInspectorName" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Inspection Date</label>
                                                                                                                </span>
                                                                                                                <div class="input-group date datetimepicker1 ">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtInspectionDate" runat="server" class="form-control">
                                                                                                                    </asp:TextBox>
                                                                                                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers, Custom" TargetControlID="txtInspectionDate" runat="server"
                                                                                                                        ValidChars="/">
                                                                                                                    </cc1:FilteredTextBoxExtender>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group ">
                                                                                                                <span class="name">
                                                                                                                    <label class="control-label">Compliance Certificate </label>
                                                                                                                </span><span>
                                                                                                                    <asp:DropDownList ID="ddlcomcerti" runat="server" AppendDataBoundItems="true" Width="120px" CssClass="myval">
                                                                                                                        <asp:ListItem Value="-1">Select</asp:ListItem>
                                                                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </span>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>

                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12 textcenterbutton">
                                                                            <div style="text-align: center;">
                                                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" runat="server"
                                                                                    Text="Save" ID="btnSaveInstall" OnClick="btnSaveInstall_Click1" ValidationGroup="InstallReq" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>

                            <cc1:TabPanel ID="TabDocument" runat="server" HeaderText="Document">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanDocument">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <div class="paddall">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="graybgarea panel-body">
                                                                                <div class="row">
                                                                                    <div id="DivQuoteDucument" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Quote</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanViewQuote">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12">
                                                                                                            <div class="row finalgrid" id="divQuotesDoc" runat="server" visible="false">
                                                                                                                <div class="col-md-12" runat="server" id="divquoDoc">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                        <thead>
                                                                                                                            <tr>
                                                                                                                                <th style="width: 25%; text-align: center">Quote Date
                                                                                                                                </th>
                                                                                                                                <th style="width: 20%; text-align: center">Doc No.
                                                                                                                                </th>
                                                                                                                                <th style="width: 15%; text-align: center">Signed
                                                                                                                                </th>
                                                                                                                                <th style="width: 20%; text-align: center">Document
                                                                                                                                </th>
                                                                                                                                <th style="width: 20%; text-align: center">Email
                                                                                                                                </th>
                                                                                                                            </tr>
                                                                                                                        </thead>
                                                                                                                        <asp:Repeater ID="rptQuoteDoc" runat="server" OnItemDataBound="rptQuoteDoc_ItemDataBound">
                                                                                                                            <ItemTemplate>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 25%; text-align: center">
                                                                                                                                        <%#Eval("ProjectQuoteDate", "{0:ddd - dd MMM yyyy}")%>
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 20%; text-align: center">
                                                                                                                                        <%#Eval("ProjectQuoteDoc")%>
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 15%; text-align: center;">
                                                                                                                                        <%-- <label for='<%# Container.FindControl("chkSerialNo").ClientID  %>' runat="server" id="lblchk123" >
                                                                                            <asp:CheckBox ID="chkSerialNo" runat="server" />
                                                                                            <span class="text">&nbsp;</span>
                                                                                        </label>--%>
                                                                                                                                        <%--<asp:Label runat="server" ID="lblsignchked" Visible="false"><i class="fa fa-check-square" style="width:20px;height:20px;"></i></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblsignUnChked"><i class="fa fa-square"></i></asp:Label>--%>
                                                                                                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/check.png" Visible="false" />
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 25%; text-align: center;">
                                                                                                                                        <asp:HiddenField ID="hndProjectQuoteID" runat="server" Value='<%#Eval("ProjectQuoteID") %>' />
                                                                                                                                        <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                                                                                            <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/icon_document_downalod.png" />
                                                                                                                                        </asp:HyperLink>
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 20%; text-align: center">
                                                                                                                                        <asp:LinkButton ID="btnSendAMail" runat="server" Visible="false" CommandName="MailAttachedPDf" CommandArgument='<%#Eval("ProjectQuoteID")%>' CssClass="addPreloader" CausesValidation="false">
                                                                                               <i class="fa fa-envelope-o fa-lg"></i>
                                                                                           <%--<span class="typcn typcn-mail" ></span>--%>
                                                                                                                                        </asp:LinkButton>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:Repeater>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="graybgarea panel-body">
                                                                                <div class="row">
                                                                                    <div id="DivUploadedDocument" runat="server">
                                                                                        <div class="widget flat radius-bordered borderone">
                                                                                            <div class="widget-header bordered-bottom bordered-blue">
                                                                                                <span class="widget-caption">Uploaded Document</span>
                                                                                            </div>
                                                                                            <div class="widget-body">
                                                                                                <asp:Panel runat="server" ID="PanUploadedDocument">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12">
                                                                                                            <div class="row finalgrid" id="DivDocumentViewDoc" runat="server" visible="false">
                                                                                                                <div class="col-md-12" runat="server" id="divDocRptDoc">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                                                                        <thead>
                                                                                                                            <tr>
                                                                                                                                <th style="width: 80%; text-align: left">Document
                                                                                                                                </th>
                                                                                                                                <th style="width: 20%; text-align: center">Download
                                                                                                                                </th>
                                                                                                                            </tr>
                                                                                                                        </thead>
                                                                                                                        <asp:Repeater ID="rptDocumemtDoc" runat="server" OnItemDataBound="rptDocumemt_ItemDataBoundDoc">
                                                                                                                            <ItemTemplate>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 80%; text-align: left">
                                                                                                                                        <%#Eval("Document")%>
                                                                                                                                    </td>

                                                                                                                                    <td style="width: 25%; text-align: center;">
                                                                                                                                        <asp:HiddenField ID="hndID" runat="server" Value='<%#Eval("ID") %>' />
                                                                                                                                        <asp:HyperLink ID="hypDoc1" runat="server" Target="_blank">
                                                                                                                                            <asp:Image ID="imgDoc1" runat="server" ImageUrl="~/images/icon_document_downalod.png" />
                                                                                                                                        </asp:HyperLink>
                                                                                                                                    </td>

                                                                                                                                </tr>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:Repeater>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>



                    </div>

                </div>

            </div>

            <asp:Button ID="Button2" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddressExistency" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
                OkControlID="btnOKAddress" TargetControlID="Button2">
            </cc1:ModalPopupExtender>
            <div id="divAddressCheck" runat="server" style="display: none">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="ibtnCancel" runat="server" onclick="btnCancle_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close</button>
                            </div>
                            <h4 class="modal-title" id="H4">Duplicate Address</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                    <tbody>
                                        <tr align="center">
                                            <td>
                                                <h4 class="noline" style="color: black"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                                    This looks like a Duplicate Entry.</b></h4>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="tablescrolldiv">
                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                        <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server"
                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Customers">
                                                    <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground statuspopup"
                CancelControlID="ibtnCancelAddress" DropShadow="false" PopupControlID="divAddress"
                OkControlID="btnOKAddress" TargetControlID="btnNULLAddress">
            </cc1:ModalPopupExtender>
            <div id="divAddress" runat="server" style="display: none; z-index: 999!important;" class="modal_popup">
                <div class="modal-dialog" style="width: 350px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelAddress" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                   Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H2">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                                Update Address</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                    summary="">
                                    <tbody>
                                        <tr style="text-align: center">
                                            <td>
                                                <h3 class="noline"><b>Address is not verified. Please verify the address first.</b> </h3>
                                            </td>
                                        </tr>
                                        <tr style="text-align: center">
                                            <td>
                                                <asp:Button ID="btnYesAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnYesAddress_Click"
                                                    Text="Yes" CausesValidation="false" />
                                                <asp:Button ID="btnCancleAddress" runat="server" CausesValidation="false"
                                                    CssClass="btn btn-danger btn-rounded" Text="Cancle" />

                                                <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                    CssClass="btn" Text=" OK " /></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULLAddress" Style="display: none;" runat="server" />

            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Delete</div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CausesValidation="false">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>
            <asp:HiddenField ID="hdndelete" runat="server" />
            <asp:Button ID="btnNULLActive" Style="display: none;" runat="server" />

            <asp:HiddenField runat="server" ID="hndInstallBookingDate" />


            <!-- Check Active Popup -->
            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelActive" DropShadow="false" PopupControlID="divAddActive"
                OkControlID="btnOKActive" TargetControlID="btnNULLActive">
            </cc1:ModalPopupExtender>
            <div id="divAddActive" runat="server">
                <div class="modal-dialog" style="width: 350px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelActive" CausesValidation="false"
                                    runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                   Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H1">Project Status</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="form-group">
                                        <h3 class="martopzero">
                                            <asp:Label ID="lblActive" runat="server" Visible="true"></asp:Label></h3>
                                    </div>
                                    <div>
                                        <table class="form-group table table-bordered table-striped">
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblSignedQuote" runat="server"></asp:Label>
                                                    </b>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblDepositeRec" runat="server"></asp:Label>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblQuoteAccepted" runat="server"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblEleDest" runat="server"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr id="approveref" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblApprovalRef" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr id="NMINumber" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblNMINumber" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr id="meterupgrade" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblmeterupgrade" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr id="RegPlanNo" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblRegPlanNo" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr id="LotNum" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblLotNum" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr id="DocumentVerified" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblDocumentVerified" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <tr id="VicAppRefference" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblVicAppRefference" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>
                                            <%--<tr id="NSWDob" runat="server" visible="false">
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblNSWDob" runat="server" Visible="false"></asp:Label>
                                                    </b>

                                                </td>
                                            </tr>--%>
                                        </table>

                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="btnOKActive" Style="display: none; visible: false;" runat="server"
                                            CssClass="btn" Text=" OK " />
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button1" Style="display: none;" runat="server" />
            <!----------------------->

            <!-- Region For Stock New Changes Pick List log--->
            <asp:Button ID="Button5" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender4" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divStock" TargetControlID="Button5"
                CancelControlID="btnCancelStock">
            </cc1:ModalPopupExtender>
            <div id="divStock" class="modal_popup" runat="server" style="display: none">
                <div class="modal-dialog" style="width: 1000px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancelStock" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H2">Add PickList Stock Item
                                <asp:Label runat="server" ID="lblModelHeader" /></h4>
                        </div>
                        <div class="finaladdupdate printorder">
                            <div id="Div3" runat="server" visible="true">
                                <div class="alert alert-danger" id="div5" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock,please Remove Item</strong>
                                </div>
                                <div class="alert alert-danger" id="Div6Pnl" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock For Panel,please Remove Item</strong>
                                </div>
                                <div class="alert alert-danger" id="Div10inv" runat="server" visible="false">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Minimum Stock is less than Live Stock  For Inverter,please Remove Item</strong>
                                </div>

                            </div>
                            <div class="graybgarea" style="padding: 10px 15px;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group wdth200">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Installer
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlinstall2" runat="server" CausesValidation="false" AppendDataBoundItems="true"
                                                    AutoPostBack="true" aria-controls="DataTables_Table_0" class="myval" OnSelectedIndexChanged="ddlinstall2_SelectedIndexChanged">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <span style="float: left; width: 100px; padding-left: 10px;">
                                                <asp:Button ID="Button12" runat="server" class="btn btn-primary" Text="Cust Notes"
                                                    Visible="false" Style="float: left" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group wdth200">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Designer
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddldesign2" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <span style="float: left; width: 100px; padding-left: 10px;">
                                                <asp:Button ID="Button13" runat="server" class="btn btn-primary" Text="Compliance Cert"
                                                    Visible="false" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group wdth200">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Electrician
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlec2" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" style="position: relative">
                                        <asp:Repeater ID="rptattribute" runat="server" OnItemDataBound="rptattribute_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <span class="name disblock">
                                                            <label>
                                                                Stock Category
                                                            </label>
                                                        </span><span>
                                                            <asp:HiddenField ID="hndStockCategoryID" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                            <div class="drpValidate">
                                                                <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                    ValidationGroup="Add" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <span class="name disblock">
                                                            <label>
                                                                Stock Item</label>
                                                        </span><span>
                                                            <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                            <div class="drpValidate">
                                                                <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged2" AutoPostBack="true"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                    ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>
                                                    </div>

                                                    <div class="form-group col-md-1">
                                                        <span class="name disblock">
                                                            <label>
                                                                Quantity</label>
                                                        </span><span style="width: 60%">
                                                            <asp:HiddenField ID="hndQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                            <asp:HiddenField ID="hndqty1" runat="server" Value='<%# Eval("Qty") %>' />
                                                            <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-1">
                                                        <span class="name disblock">
                                                            <label>
                                                                Wal Qty</label>
                                                        </span>
                                                        <span style="width: 60%">
                                                            <asp:TextBox ID="txtwallateQty" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group" style="padding-top: 5px;">
                                                            <div class="padtop15">
                                                                <span class="name">
                                                                    <%--<asp:HiddenField ID="hdnStockOrderItemID" runat="server" Value='<%#Eval("StockOrderItemID") %>' />--%>
                                                                    <asp:CheckBox ID="chkdelete" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <br />
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:Button ID="litremove" runat="server" OnClick="litremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" /><br />

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div style="position: absolute; top: 0px; right: 160px;">
                                            <div class="form-group" style="margin-top: 23px;">
                                                <asp:Button ID="btnAddRow" runat="server" Text="Add" OnClick="btnAddRow_Click" CssClass="btn btn-info redreq"
                                                    CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Reason for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="txtResonePLA" runat="server" TextMode="MultiLine" Width="290px" Height="80px"
                                                    CssClass="form-control"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="" ControlToValidate="TextBox1"
                                            Display="Dynamic" ValidationGroup="picklistagain"></asp:RequiredFieldValidator>--%>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Note for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="txtNotesAgain" TextMode="MultiLine" runat="server" Width="290px" Height="80px"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBox2"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="picklistagain" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Install Date:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="TextBox5" runat="server" class="form-control" Width="125px" Enabled="false"></asp:TextBox>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: left; padding-left: 0px; padding-top: 3px;">
                                            <asp:Button ID="btnAdd" runat="server" Text="Save" CssClass="btn btn-info redreq"
                                                CausesValidation="false" OnClick="btnAdd_Click" />
                                        </div>
                                    </div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--  End Region For Stock New Changes--->

            <asp:Button ID="Button19" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender6" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="Div12" DropShadow="false" CancelControlID="LinkButton7" OkControlID="btnOKMobile" TargetControlID="Button19">
            </cc1:ModalPopupExtender>
            <div id="Div12" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Picklist For Draft</div>
                        <label id="Label19" runat="server"></label>
                        <div class="modal-body ">Want To Generate PickList For Draft?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkDraft" runat="server" CausesValidation="false" OnClick="lnkDraft_Click" class="btn btn-danger">OK</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton7" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>


            <!--Danger Modal Templates-->
            <asp:Button ID="Button3" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDeleteNew" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger1" DropShadow="false" CancelControlID="lnkcancel1" TargetControlID="Button3">
            </cc1:ModalPopupExtender>
            <div id="modal_danger1" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>
                        <div class="modal-title">Delete</div>
                        <label id="Label22" runat="server"></label>
                        <div class="modal-body ">
                            <asp:Label ID="lblMassage" runat="server" />
                        </div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkDeletePickList" runat="server" CausesValidation="false" class="btn btn-danger" OnClick="lnkDeletePickList_Click1">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkDeleteInstallation" runat="server" CausesValidation="false" class="btn btn-danger" OnClick="lnkDeleteInstallation_Click">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel1" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hndDelete" runat="server" />

            <!--End Danger Modal Templates-->

            <!--Danger Modal Templates-->
            <asp:Button ID="Button4" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderRefundDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="DivRefundDelete" DropShadow="false" CancelControlID="LinkButton2" TargetControlID="hndRefundDelete">
            </cc1:ModalPopupExtender>
            <div id="DivRefundDelete" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
                <asp:HiddenField ID="hndDeleteRefundID" runat="server" />
                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>

                        <div class="modal-title">Delete</div>
                        <label id="Label23" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkRefundDelete" runat="server" OnClick="lnkRefundDelete_Click" CausesValidation="false" class="btn btn-danger">OK</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>
            <asp:HiddenField ID="hndRefundDelete" runat="server" />


            <asp:Button ID="Button16" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderAPL" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="LinkButton4" DropShadow="false" PopupControlID="div14" TargetControlID="Button16">
            </cc1:ModalPopupExtender>
            <div runat="server" id="div14" class="modal_popup" style="width: 1290px!important; position: fixed!important; left: 100px!important; top: 90px!important;">
                <div class="popupdiv">
                    <div class="">
                        <div class="color-line"></div>
                        <div class="">
                            <div class="panel panel-default">
                                <div class="">
                                    <asp:Label runat="server" ID="Label24"></asp:Label>
                                    <div class="">
                                        <div class="modal-header">
                                            <div style="float: right">
                                                <asp:LinkButton ID="LinkButton4" CausesValidation="false"
                                                    runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                                 Close
                                                </asp:LinkButton>
                                            </div>

                                            <div class="">
                                                <h3 class="modal-title" id="H146">
                                                    <asp:Label runat="server" ID="Label26" Text="PickUp Details"></asp:Label></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;" id="Data" runat="server">
                                    <div class="row">
                                        <div class="">
                                            <div class="graybgarea">
                                                <div class="col-sm-12">
                                                    <div class=" col-sm-12 center-text form-group">
                                                        <div class="row finalgrid">
                                                            <div class="col-md-12" runat="server" id="div15">
                                                                <table width="100%" cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="width: 5%; text-align: center;">ID
                                                                            </th>
                                                                            <th style="width: 5%; text-align: center;">Project Number 
                                                                            </th>
                                                                            <th style="width: 5%; text-align: center;">Generated Date 
                                                                            </th>
                                                                            <th style="width: 5%; text-align: center">Install Date
                                                                            </th>
                                                                            <th style="width: 10%; text-align: center">Installer Name  
                                                                            </th>
                                                                            <th style="width: 20%; text-align: center">System Details  
                                                                            </th>
                                                                            <th style="width: 20%; text-align: center">Reason
                                                                            </th>

                                                                            <th style="width: 5%; text-align: center">CreatedBy
                                                                            </th>

                                                                            <th style="text-align: center">&nbsp;
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <asp:HiddenField ID="HiddenField4" runat="server" />
                                                                    <asp:Repeater ID="rptAPL" runat="server">
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="width: 5%;">
                                                                                    <%#Eval("ID")%>
                                                                                </td>
                                                                                <td style="width: 5%;">
                                                                                    <%#Eval("ProjectNumber")%>
                                                                                </td>

                                                                                <td style="width: 5%;">
                                                                                    <asp:HiddenField ID="hndpicklistid" runat="server" Value='<%#Eval("ID")%>' />
                                                                                    <%#Eval("PickListDateTime", "{0:dd/MM/yyyy}")%>
                                                                                </td>

                                                                                <td style="width: 5%;">
                                                                                    <%#Eval("InstallBookedDate", "{0:dd/MM/yyyy}")%>
                                                                                </td>

                                                                                <td style="width: 10%; word-break: break-word;">
                                                                                    <%#Eval("InstallerName")%>
                                                                                </td>
                                                                                <td style="width: 20%;">
                                                                                    <asp:Label ID="lblstytemdetail" runat="server"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 20%;">
                                                                                    <%#Eval("Reason")%>
                                                                                </td>

                                                                                <td style="width: 5%;">
                                                                                    <asp:HiddenField ID="hdnEmpId" runat="server" Value='<%#Eval("createdby")%>' />
                                                                                    <asp:HiddenField ID="hdnCompanyid" runat="server" Value='<%#Eval("Companyid")%>' />
                                                                                    <asp:Label ID="lblEmpName" Width="70px" runat="server"></asp:Label>
                                                                                </td>

                                                                                <td>
                                                                                    <%-- <asp:LinkButton ID="btnUpdatepicklist" Style="width: 70px" Visible="false" runat="server" OnClick="btnUpdatepicklist_Click" CommandName="Edit" CommandArgument='<%#Eval("Id")+","+ Eval("InstallerID") %>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                                        Edit
                                                                                    </asp:LinkButton>&nbsp;&nbsp;
                                                                                    <asp:LinkButton ID="btnwallet" Style="width: 70px" runat="server" OnClick="btnwallet_Click" CommandName="wallet" CommandArgument='<%#Eval("Id")+","+ Eval("InstallerID")%>' CausesValidation="false" class="btn btn-labeled btn-primary" Visible="false">
                                                                                        Wallet
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnpicklistDelete" Style="width: 70px" runat="server" CommandName="delete" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                                        Delete
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnNotes" Style="width: 70px" runat="server" Visible="false" CommandName="notes" CommandArgument='<%#Eval("Id")%>' CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                                        Notes
                                                                                    </asp:LinkButton>--%>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderStatus" runat="server" BackgroundCssClass="modalbackground statuspopup"
                CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="divUpdateStatus"
                OkControlID="btnOKStatus" TargetControlID="btnNULLStatus">
            </cc1:ModalPopupExtender>
            <div id="divUpdateStatus" runat="server" style="display: none; z-index: 999!important;" class="modal_popup">
                <div class="modal-dialog" style="width: 350px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                    Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="myModalLabel">
                                <asp:Label ID="Label27" runat="server" Text=""></asp:Label>
                                Update Status</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="form-group">
                                        <label>
                                            Status:
                                        </label>
                                        <asp:DropDownList ID="ddlProjectStatusID" runat="server" Width="150px" OnSelectedIndexChanged="ddlProjectStatusID_SelectedIndexChanged"
                                            AppendDataBoundItems="true" AutoPostBack="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="status" ControlToValidate="ddlProjectStatusID" Display="Dynamic"></asp:RequiredFieldValidator>

                                    </div>

                                    <div class="form-group" id="trHold" runat="server" visible="false">
                                        <label>
                                            OnHold Reason:
                                        </label>
                                        <asp:DropDownList ID="ddlProjectOnHoldID" runat="server" Width="150px" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="ddlProjectOnHoldID" Display="Dynamic" ValidationGroup="status"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group" id="trCancel" runat="server" visible="false">
                                        <label>
                                            Cancel Reason:
                                        </label>
                                        <asp:DropDownList ID="ddlProjectCancelID" runat="server" AppendDataBoundItems="true"
                                            Width="150px" aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ValidationGroup="status" ControlToValidate="ddlProjectCancelID" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group modaldesc" id="trComment" runat="server" visible="false">
                                        <label>
                                            Comment:
                                        </label>
                                        <asp:TextBox ID="txtComment" runat="server" Width="200px" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                    </div>
                                    <div class="form-group" id="precancel" runat="server">
                                        <div class="form-group modaldesc" id="ddlcancelReason" runat="server">
                                            <asp:DropDownList ID="ddlprecancel" runat="server" AppendDataBoundItems="true"
                                                Width="150px" aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1">Roof Space Issue</asp:ListItem>
                                                <asp:ListItem Value="2">Shading Issue</asp:ListItem>
                                                <asp:ListItem Value="3"> Not interested </asp:ListItem>
                                                <asp:ListItem Value="4">Financial issue</asp:ListItem>
                                                <asp:ListItem Value="5">Not applicable.</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                ControlToValidate="ddlprecancel" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group modaldesc" runat="server">
                                            <label>
                                                Pre Cancel Reason
                                            </label>
                                            <asp:TextBox ID="txtPreCancelReason" runat="server" Width="200px" TextMode="MultiLine" MaxLength="500"></asp:TextBox>

                                            <asp:HiddenField ID="hndProjectStatusId" runat="server" />

                                            <div class="form-group" id="Div16" runat="server" visible="false" style="display: none">
                                                <label>
                                                    Active date
                                                </label>
                                                <asp:TextBox ID="txtActiveDate1" CssClass="form-control datetimepicker1" Enabled="false" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group marginleft center-text">
                                    <asp:Button ID="ibtnUpdateStatus" runat="server" Text="Update" OnClick="ibtnUpdateStatus_Onclick"
                                        class="btn btn-primary savewhiteicon btnsaveicon" />
                                    <%--ValidationGroup="status" --%>
                                    <asp:Button ID="btnOKStatus" Style="display: none; visible: false;" runat="server"
                                        CssClass="btn" Text=" OK " />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndStatusProjectID" runat="server" />




        </ContentTemplate>

    </asp:UpdatePanel>



    <div class="loaderPopUP">
        <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                $('.loading-container').css('display', 'none');
                $('input[type=file]').bootstrapFileInput();
            }

            function pageLoadedpro() {

                $('#<%=btnSave.ClientID %>').click(function () {
                    formValidate();
                });

                $(".myvalinfo").select2({
                    //placeholder: "select",
                    allowclear: true
                });

                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });

                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });

                $('.datetimepicker7').datetimepicker({
                    format: 'DD/MM/YYYY',
                    useCurrent: false //Important! See issue #1075
                }).
                    keypress(function (event) {
                        if (event.keyCode != 8) {
                            event.preventDefault();
                        }
                    });

                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();

                today = dd + '/' + mm + '/' + yyyy;
                //document.write(today);
                //$('.datetimepicker7').data("DateTimePicker").minDate(today);

                $('.datetimepicker8').datetimepicker({
                    format: 'DD/MM/YYYY',
                    useCurrent: false //Important! See issue #1075
                }).
                    keypress(function (event) {
                        if (event.keyCode != 8) {
                            event.preventDefault();
                        }
                    });

                $('.datetimepicker8').data("DateTimePicker").minDate(today);

                $('.redreq').click(function () {
                    formValidate();
                });

                $('.redreq1').click(function () {
                    form_Validate();
                });

                $('.loading-container').css('display', 'none');

                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                // $('.loading-container').css('display', 'none');
            }

            function getimage(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#ctl001_Header_Image1')
                            .attr('src', e.target.result)
                            .width(128)
                            .height(128)
                            .title('');
                    };
                    reader.readAsDataURL(input.files[0]);

                }
            }

            function PanelsChange(ID) {

                var NumberPanels = $("#<%=txtNoOfPanels.ClientID %>").val();
                var IBDate = $("#<%=hndInstallBookingDate.ClientID %>").val();
                var ZoneRt = $("#<%=txtZoneRt.ClientID %>").val();
                var STCValue = $("#<%=txtSTCInverter.ClientID %>").val();

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/ItemData",
                    data: JSON.stringify({ ID: ID, NumberPanels: NumberPanels, IBDate: IBDate, ZoneRt: ZoneRt, STCValue: STCValue }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;

                            data = $.parseJSON(data);
                            //alert(data.StockModel);
                            //alert(data.StockWatts);
                            document.getElementById("<%= hndPanelBrand.ClientID %>").value = data.StockManufacturer;
                            document.getElementById("<%= txtPanelModel.ClientID %>").value = data.StockModel;
                            document.getElementById("<%= txtWatts.ClientID %>").value = data.StockWatts;
                            document.getElementById("<%= txtSystemCapacity.ClientID %>").value = data.StockCapacity;
                            document.getElementById("<%= txtSTC.ClientID %>").value = data.STC;
                            document.getElementById("<%= txtRebate.ClientID %>").value = data.Rebate;
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function NumberOfPanelsChange(Value) {

                var StockSize = $("#<%=txtWatts.ClientID %>").val();
                var IBDate = $("#<%=hndInstallBookingDate.ClientID %>").val();
                var ZoneRt = $("#<%=txtZoneRt.ClientID %>").val();
                var STCValue = $("#<%=txtSTCInverter.ClientID %>").val();

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/NumberPanels",
                    data: JSON.stringify({ NumberPanels: Value, StockSize: StockSize, IBDate: IBDate, ZoneRt: ZoneRt, STCValue: STCValue }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;

                            data = $.parseJSON(data);
                            //alert(data);
                            //alert(data.StockWatts);

                            document.getElementById("<%= txtSystemCapacity.ClientID %>").value = data.StockCapacity;
                            document.getElementById("<%= txtSTC.ClientID %>").value = data.STC;
                            document.getElementById("<%= txtRebate.ClientID %>").value = data.Rebate;
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function OldPanelsChange(ID) {

                var NumberPanels = $("#<%=txtOldNumberOfPanels.ClientID %>").val();
                var IBDate = $("#<%=hndInstallBookingDate.ClientID %>").val();
                var ZoneRt = "0";
                var STCValue = "0";

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/ItemData",
                    data: JSON.stringify({ ID: ID, NumberPanels: NumberPanels, IBDate: IBDate, ZoneRt: ZoneRt, STCValue: STCValue }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;

                            data = $.parseJSON(data);
                            document.getElementById("<%= hndOldPanelBrand.ClientID %>").value = data.StockManufacturer;
                            document.getElementById("<%= txtOldModel.ClientID %>").value = data.StockModel;
                            document.getElementById("<%= txtOldWatts.ClientID %>").value = data.StockWatts;
                            document.getElementById("<%= txtOldSystemCapacity.ClientID %>").value = data.StockCapacity;
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

                disableValidatorRemoveOldSystem();
            }

            function OldNumberOfPanelsChange(Value) {

                var StockSize = $("#<%=txtOldWatts.ClientID %>").val();
                var IBDate = $("#<%=hndInstallBookingDate.ClientID %>").val();
                var ZoneRt = "0";
                var STCValue = "0";

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/NumberPanels",
                    data: JSON.stringify({ NumberPanels: Value, StockSize: StockSize, IBDate: IBDate, ZoneRt: ZoneRt, STCValue: STCValue }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;

                            data = $.parseJSON(data);
                            //alert(data);
                            //alert(data.StockWatts);

                            document.getElementById("<%= txtOldSystemCapacity.ClientID %>").value = data.StockCapacity;
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

                disableValidatorNoOfPanels();
            }

            function InverterChange1(ID) {
                //alert(ID);
                <%--var NumberPanels = $("#<%=txtNoOfPanels.ClientID %>").val();
                var IBDate = $("#<%=hndInstallBookingDate.ClientID %>").val();
                var ZoneRt = $("#<%=txtZoneRt.ClientID %>").val();
                var STCValue = $("#<%=txtSTCInverter.ClientID %>").val();--%>

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/InverterChange",
                    data: JSON.stringify({ ID: ID }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;
                            data = $.parseJSON(data);

                            //alert(data);
                            //alert(data.StockWatts);

                            document.getElementById("<%= txtInverterModel.ClientID %>").value = data.StockModel;
                            document.getElementById("<%= txtSeries.ClientID %>").value = data.StockSeries;
                            document.getElementById("<%= hndInverterOutput1.ClientID %>").value = data.StockWatts;
                            document.getElementById("<%= txtNoOfInverter.ClientID %>").value = data.NoInverter;
                            document.getElementById("<%= hndInverterBrand1.ClientID %>").value = data.StockManufacturer;

                            var Output = "0.00";
                            var Output2 = parseFloat($("#<%=hndInverterOutput2.ClientID %>").val())
                            var Output3 = parseFloat($("#<%=hndInverterOutput3.ClientID %>").val())

                            Output = parseFloat(data.StockWatts) + Output2 + Output3;

                            document.getElementById("<%= txtInverterOutput.ClientID %>").value = Output;
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function InverterChange2(ID) {
                //alert(ID);
                <%--var NumberPanels = $("#<%=txtNoOfPanels.ClientID %>").val();
                var IBDate = $("#<%=hndInstallBookingDate.ClientID %>").val();
                var ZoneRt = $("#<%=txtZoneRt.ClientID %>").val();
                var STCValue = $("#<%=txtSTCInverter.ClientID %>").val();--%>

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/InverterChange",
                    data: JSON.stringify({ ID: ID }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;
                            data = $.parseJSON(data);

                            //alert(data);
                            //alert(data.StockWatts);

                            document.getElementById("<%= txtInverterModel2.ClientID %>").value = data.StockModel;
                            document.getElementById("<%= txtSeries2.ClientID %>").value = data.StockSeries;
                            document.getElementById("<%= hndInverterOutput2.ClientID %>").value = data.StockWatts;
                            document.getElementById("<%= txtNoOfInverter2.ClientID %>").value = data.NoInverter;
                            document.getElementById("<%= hndInverterBrand2.ClientID %>").value = data.StockManufacturer;

                            var Output = "0.00";
                            var Output1 = parseFloat($("#<%=hndInverterOutput1.ClientID %>").val())
                            var Output3 = parseFloat($("#<%=hndInverterOutput3.ClientID %>").val())

                            Output = parseFloat(data.StockWatts) + Output1 + Output3;

                            document.getElementById("<%= txtInverterOutput.ClientID %>").value = Output;
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function InverterChange3(ID) {
                //alert(ID);
                <%--var NumberPanels = $("#<%=txtNoOfPanels.ClientID %>").val();
                var IBDate = $("#<%=hndInstallBookingDate.ClientID %>").val();
                var ZoneRt = $("#<%=txtZoneRt.ClientID %>").val();
                var STCValue = $("#<%=txtSTCInverter.ClientID %>").val();--%>

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/InverterChange",
                    data: JSON.stringify({ ID: ID }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;
                            data = $.parseJSON(data);

                            //alert(data);
                            //alert(data.StockWatts);

                            document.getElementById("<%= txtModel3.ClientID %>").value = data.StockModel;
                            document.getElementById("<%= txtSeries3.ClientID %>").value = data.StockSeries;
                            document.getElementById("<%= hndInverterOutput3.ClientID %>").value = data.StockWatts;
                            document.getElementById("<%= txtNoOfInverter3.ClientID %>").value = data.NoInverter;
                            document.getElementById("<%= hndInverterBrand3.ClientID %>").value = data.StockManufacturer;

                            var Output = "0.00";
                            var Output1 = parseFloat($("#<%=hndInverterOutput1.ClientID %>").val())
                            var Output2 = parseFloat($("#<%=hndInverterOutput2.ClientID %>").val())

                            Output = parseFloat(data.StockWatts) + Output1 + Output2;

                            document.getElementById("<%= txtInverterOutput.ClientID %>").value = Output;
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function InvoicePayTotalChange(value) {
                //alert(value);
                var InvoicePayMethodID = $("#<%=ddlInvoicePayMethodID.ClientID %>").val();
                var Count = $("#<%=TextBox2.ClientID %>").val();
                //alert(Count);

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/InvoicePayTotalChange",
                    data: JSON.stringify({ value: value, InvoicePayMethodID: InvoicePayMethodID, Count: Count }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;
                            data = $.parseJSON(data);

                            //alert(data);
                            //alert(data.InvoicePayGST);

                            document.getElementById("<%= txtInvoicePayGST.ClientID %>").value = data.InvoicePayGST;
                            document.getElementById("<%= txtCCSurcharge.ClientID %>").value = data.CCSurcharge;

                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function InvoicePayTotalChangeInstall(value) {
                //alert(value);
                var InvoicePayMethodID = $("#<%=ddlInvoicePayMethodIDInstall.ClientID %>").val();
                var Count = $("#<%=TextBox4.ClientID %>").val();
                //alert(Count);

                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/InvoicePayTotalChange",
                    data: JSON.stringify({ value: value, InvoicePayMethodID: InvoicePayMethodID, Count: Count }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;
                            data = $.parseJSON(data);

                            //alert(data);
                            //alert(data.InvoicePayGST);

                            document.getElementById("<%= txtInvoicePayGSTInstall.ClientID %>").value = data.InvoicePayGST;
                            document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").value = data.CCSurcharge;

                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function InvoicePayMethodIDChange(ID) {
                //alert(value);
                var InvoicePayTotal = $("#<%=txtInvoicePayTotal.ClientID %>").val();

                var Count = $("#<%=TextBox2.ClientID %>").val();
                //alert(Count);

                if (ID == "7" || ID == "3" || ID == "2") {
                    //alert(ID);
                    //document.getElementById("DivReceiptNo").removeAttribute("style")
                    document.getElementById("<%= DivReceiptNo.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivTransactionCode.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivTransactionCode.ClientID %>").setAttribute("style", "display:none");
                }
                else if (ID == "6") {
                    document.getElementById("<%= DivTransactionCode.ClientID %>").removeAttribute("style");

                    document.getElementById("<%= DivReceiptNo.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivReceiptNo.ClientID %>").setAttribute("style", "display:none");
                }
                else {
                    //alert(ID);
                    //document.getElementById("DivReceiptNo").removeAttribute("style")
                    //document.getElementById("DivReceiptNo").setAttribute("style", "display: none");
                    document.getElementById("<%= DivReceiptNo.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivReceiptNo.ClientID %>").setAttribute("style", "display:none");

                    document.getElementById("<%= DivTransactionCode.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivTransactionCode.ClientID %>").setAttribute("style", "display:none");
                }

                if (ID == "3") {
                    $.ajax({
                        type: "POST",
                        url: "SalesInfo.aspx/InvoicePayMethodIDChange",
                        data: JSON.stringify({ ID: ID, InvoicePayTotal: InvoicePayTotal, Count: Count }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",

                        success: function (response) {
                            if (response != null && response.d != null) {
                                var data = response.d;
                                data = $.parseJSON(data);

                                //alert(data);
                                //alert(data.CCSurcharge);

                                document.getElementById("<%= txtCCSurcharge.ClientID %>").value = data.CCSurcharge;

                                if (data.RolesEnable == "true") {
                                    <%--document.getElementById("<%= txtCCSurcharge.ClientID %>").value = "0";--%>
                                    document.getElementById("<%= txtCCSurcharge.ClientID %>").removeAttribute("ReadOnly");
                                }
                                else {
                                    document.getElementById("<%= txtCCSurcharge.ClientID %>").removeAttribute("ReadOnly");
                                    document.getElementById("<%= txtCCSurcharge.ClientID %>").setAttribute("ReadOnly", "readonly");
                                }
                            }
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }
                else {
                    if (ID == "6") {
                        document.getElementById("<%= txtCCSurcharge.ClientID %>").value = "0";
                        document.getElementById("<%= txtCCSurcharge.ClientID %>").removeAttribute("ReadOnly");
                        //document.getElementById("<%= txtCCSurcharge.ClientID %>").setAttribute("ReadOnly", "true");
                    }
                    else {
                        document.getElementById("<%= txtCCSurcharge.ClientID %>").value = "";
                        document.getElementById("<%= txtCCSurcharge.ClientID %>").removeAttribute("ReadOnly");
                        document.getElementById("<%= txtCCSurcharge.ClientID %>").setAttribute("ReadOnly", "readonly");
                    }
                }

            }

            function InvoicePayMethodIDChangeInstall(ID) {
                //alert(value);
                var InvoicePayTotal = $("#<%=txtInvoicePayTotalInstall.ClientID %>").val();

                var Count = $("#<%=TextBox4.ClientID %>").val();
                //alert(Count);

                if (ID == "7" || ID == "3" || ID == "2") {
                    //alert(ID);
                    //document.getElementById("DivReceiptNo").removeAttribute("style")
                    document.getElementById("<%= DivReceiptNoInstall.ClientID %>").removeAttribute("style");

                    document.getElementById("<%= DivTransactionCodeInstall.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivTransactionCodeInstall.ClientID %>").setAttribute("style", "display:none");
                }
                else if (ID == "6") {
                    document.getElementById("<%= DivTransactionCodeInstall.ClientID %>").removeAttribute("style");

                    document.getElementById("<%= DivReceiptNoInstall.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivReceiptNoInstall.ClientID %>").setAttribute("style", "display:none");
                }
                else {
                    //alert(ID);
                    //document.getElementById("DivReceiptNo").removeAttribute("style")
                    //document.getElementById("DivReceiptNo").setAttribute("style", "display: none");
                    document.getElementById("<%= DivReceiptNoInstall.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivReceiptNoInstall.ClientID %>").setAttribute("style", "display:none");

                    document.getElementById("<%= DivTransactionCodeInstall.ClientID %>").removeAttribute("style");
                    document.getElementById("<%= DivTransactionCodeInstall.ClientID %>").setAttribute("style", "display:none");
                }

                if (ID == "3") {
                    $.ajax({
                        type: "POST",
                        url: "SalesInfo.aspx/InvoicePayMethodIDChange",
                        data: JSON.stringify({ ID: ID, InvoicePayTotal: InvoicePayTotal, Count: Count }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",

                        success: function (response) {
                            if (response != null && response.d != null) {
                                var data = response.d;
                                data = $.parseJSON(data);

                                //alert(data);
                                //alert(data.CCSurcharge);

                                document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").value = data.CCSurcharge;

                                if (data.RolesEnable == "true") {
                                    <%--document.getElementById("<%= txtCCSurcharge.ClientID %>").value = "0";--%>
                                    document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").removeAttribute("ReadOnly");
                                }
                                else {
                                    document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").removeAttribute("ReadOnly");
                                    document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").setAttribute("ReadOnly", "readonly");
                                }
                            }
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }
                else {
                    if (ID == "6") {
                        document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").value = "0";
                        document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").removeAttribute("ReadOnly");
                        //document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").setAttribute("ReadOnly", "true");
                    }
                    else {
                        document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").value = "";
                        document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").removeAttribute("ReadOnly");
                        document.getElementById("<%= txtCCSurchargeInstall.ClientID %>").setAttribute("ReadOnly", "readonly");
                    }
                }

            }

            <%--function BindPanInverter(ID) {
                var ProjectID = $("#<%=hndProjectID.ClientID %>").val();
                $.ajax({
                    type: "POST",
                    url: "SalesInfo.aspx/BindPanInverter",
                    data: JSON.stringify({ ID: ID, ProjectID: ProjectID }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        if (response != null && response.d != null) {
                            var data = response.d;
                            data = $.parseJSON(data);

                            //alert(data.PL);
                            //alert(data.CCSurcharge);

                            document.getElementById("<%= lblPl.ClientID %>").text = data.PL;
                            document.getElementById("<%= lblApl.ClientID %>").text = data.Apl;
                            document.getElementById("<%= lblSMPL.ClientID %>").text = data.SMPL;
                            document.getElementById("<%= lblPW.ClientID %>").text = data.PW;
                            document.getElementById("<%= lblPt.ClientID %>").text = data.PT;
                            document.getElementById("<%= lblPO.ClientID %>").text = data.PO;
                            document.getElementById("<%= lnketap.ClientID %>").text = data.ETA;
                            document.getElementById("<%= lblPm.ClientID %>").text = data.M;

                            if (data.divpanel == 'true') {
                                MyRedfun("Picklist Can not be Generated For this Panel.");
                            }

                            document.getElementById("<%= lblIl.ClientID %>").text = data.IL;
                            document.getElementById("<%= lblAriseInv.ClientID %>").text = data.IApl;
                            document.getElementById("<%= lblSmInv.ClientID %>").text = data.ISMPL;
                            document.getElementById("<%= lblIW.ClientID %>").text = data.IW;
                            document.getElementById("<%= lblIt.ClientID %>").text = data.IT;
                            document.getElementById("<%= lblIo.ClientID %>").text = data.IO;
                            document.getElementById("<%= lnketaI.ClientID %>").text = data.IETA;
                            document.getElementById("<%= lblIm.ClientID %>").text = data.IM;

                            if (data.divinverter == 'true') {
                                MyRedfun("Picklist Can not be Generated For this Inverter.");
                            }
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }--%>

        </script>
    </div>
</asp:Content>
