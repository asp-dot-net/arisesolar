using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Text.RegularExpressions;


public partial class admin_adminfiles_company_company : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;
    protected static string SortDir;
    protected static string SortFlag = "unsorted";
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string address;
    protected static bool streetname;
    string foldername = "customerimage";
    protected static string postaladdress;
    private Image objPrintImage;
    private FrameDimension objDimension;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    protected void Page_Load(object sender, EventArgs e)
    {

        HidePanels();
        
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        if (Convert.ToBoolean(st_emp.showexcel) == true)
        {
            lbtnExport.Visible = true;
           
        }
        else
        {
            lbtnExport.Visible = false;
        }

        //---------------Commented by deep
        if (Roles.IsUserInRole("leadgen") || Roles.IsUserInRole("Lead Manager"))
        {

            Response.Redirect("~/admin/adminfiles/company/ECompany.aspx");
            // acompany.HRef = SiteURL + "admin/adminfiles/company/Ecompany.aspx";
        }
        //---------------end Commented by deep


        //if (TabContainer1. != 1)
        //{
        //    //Response.Write();
        //    //Response.End();
        //    PanGrid.Visible = false;
        //    //contacts1.HideTabContainer();
        //   // companysummary1.HideTabContainer();
        //}
        //if (Roles.IsUserInRole("Administrator"))
        //{
        //    divformbayaddress.Visible = true;
        //}
        //else
        //{
        //    divformbayaddress.Visible = false;
        //}
        if (Roles.IsUserInRole("Accountant"))
        {
            lnkAdd.Visible = true;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            lnkAdd.Visible = false;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            lnkAdd.Visible = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            lnkAdd.Visible = true;
            TabConversation.Visible = true;
        }
        if (Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Installation Manager"))
        {
            lnkAdd.Visible = true;
        }
        if (Roles.IsUserInRole("SalesRep"))
        {
            tdAll1.Visible = false;
            //  tdAll2.Visible = false;
            //GridView1.Columns[6].Visible = false;
            //GridView1.Columns[7].Visible = false;
            //  GridView1.Columns[8].Visible = false;
        }
        if (Roles.IsUserInRole("STC"))
        {
            lnkAdd.Visible = false;
        }
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager")|| Roles.IsUserInRole("Installation Manager"))
        {
            GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
        }
        else




        {
            GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
        }
        //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
        //{
        //    tdExport.Visible = true;
        //}
        //else
        //{
        //    tdExport.Visible = false;
        //}
        if (Roles.IsUserInRole("DSalesRep"))
        {
            tdAll1.Visible = false;
            // tdAll2.Visible = false;

            rptName.Columns[2].Visible = false;
            rptMobile.Columns[2].Visible = false;
            rptEmail.Columns[2].Visible = false;
        }
        if (Roles.IsUserInRole("CompanyManager"))
        {
            lnkAdd.Visible = true;
            GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
        }
        if (!IsPostBack)
        {
            StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st1.siteurl;
            ddlCustSourceID.Items.Clear();
            ListItem item1 = new ListItem();
            item1.Value = "";
            item1.Text = "Select";
            ddlCustSourceID.Items.Add(item1);
            ddlCustSourceID.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
            ddlCustSourceID.DataMember = "CustSource";
            ddlCustSourceID.DataTextField = "CustSource";
            ddlCustSourceID.DataValueField = "CustSourceID";
            ddlCustSourceID.DataBind();
            ddlCustSourceID.SelectedValue = "1";
            ddlCustSourceID_SelectedIndexChanged(sender, e);

            ddlCustTypeID.Items.Clear();
            ListItem item = new ListItem();
            item.Value = "";
            item.Text = "Select";
            ddlCustTypeID.Items.Add(item);
            ddlCustTypeID.DataSource = ClstblCustType.tblCustType_SelectActive_COmpany();
            ddlCustTypeID.DataMember = "CustType";
            ddlCustTypeID.DataTextField = "CustType";
            ddlCustTypeID.DataValueField = "CustTypeID";
            ddlCustTypeID.DataBind();
            ddlCustTypeID.SelectedValue = "1";
           

            //TabContainer1_ActiveTabChanged(TabContainer1, e);
            //if (TabContainer1.ActiveTabIndex == 0)
            //{
            //    //  divright
            //    // Response.Write(TabContainer1.ActiveTabIndex);
            //    // Response.End();
            //    PanGrid.Visible = false;
            //}
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>loader();</script>");
            if (string.IsNullOrEmpty(Request.QueryString["m"]))
            {
                //Profile.eurosolar.companyid = "";
                //Profile.eurosolar.contactid = "";
                //Profile.eurosolar.projectid = "";
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
                {
                    ViewCompanyDetail(Request.QueryString["compid"]);
                }
                if (Request.QueryString["m"] == "pro")
                {
                    TabContainer1.ActiveTabIndex = 2;

                    project1.BindProjects();
                    project1.ViewProjectDetail();
                }
                else if (Request.QueryString["m"] == "proelec")
                {
                    TabContainer1.ActiveTabIndex = 2;
                    project1.BindProjects();
                    project1.ViewProjectDetail();

                }
                else if (Request.QueryString["m"] == "propre")
                {
                    TabContainer1.ActiveTabIndex = 2;
                    project1.BindProjects();
                    project1.ViewProjectDetail();
                }
                else if (Request.QueryString["m"] == "f")
                {
                    TabContainer1.ActiveTabIndex = 3;
                    info1.BindInfo();
                }
                else if (Request.QueryString["m"] == "comp")
                {
                    TabContainer1.ActiveTabIndex = 0;
                    //PanGrid.Visible = false;
                }
                else if (Request.QueryString["m"] == "c")
                {
                    TabContainer1.ActiveTabIndex = 1;
                    contacts1.BindContact();
                    contacts1.ViewContactDetail();
                }
            }
            BindDropDown();
            custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            // ddlCustTypeID.SelectedValue = "3";
            ddlCustSourceID.SelectedValue = "1";

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            // SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            //if (st.SalesTeamID != string.Empty)
            // {st.SalesTeamID == "10"

            if (Roles.IsUserInRole("Lead Manager"))
            {
                divD2DEmployee.Visible = true;
                divD2DAppDate.Visible = true;
                divD2DAppTime.Visible = true;
                ddlCustSourceID.SelectedValue = "1";
            }
            else
            {
                divD2DEmployee.Visible = false;
                divD2DAppDate.Visible = false;
                divD2DAppTime.Visible = false;
            }


            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            DataTable dtempteam = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dtempteam.Rows.Count > 0)
            {
                foreach (DataRow dr in dtempteam.Rows)
                {
                    if (dr["SalesTeamID"].ToString() == "4" || dr["SalesTeamID"].ToString() == "5")
                    {
                        //SttblCustSource st1 = ClstblCustSource.tblCustSource_SelectByCustSourceIDCustSource("E2E");
                        ddlCustSourceID.SelectedValue = "4";
                        ddlCustSourceID.Enabled = false;
                    }
                    else
                    {
                        ddlCustSourceID.Enabled = true;
                    }
                }
            }
            else
            {
                ddlCustSourceID.Enabled = true;
            }
            //if (st.SalesTeamID != string.Empty)
            //{
            //    if (st.SalesTeamID == "10" || st.SalesTeamID == "12")
            //    {
            //        SttblCustSource st1 = ClstblCustSource.tblCustSource_SelectByCustSourceIDCustSource("E2E");
            //        ddlCustSourceID.SelectedValue = st1.CustSourceID;
            //        ddlCustSourceID.Enabled = false;
            //    }
            //    else
            //    {
            //        ddlCustSourceID.Enabled = true;
            //    }
            //}
            //else
            //{
            //    ddlCustSourceID.Enabled = true;
            //}

            BindGrid(0);

            if (!string.IsNullOrEmpty(Request.QueryString["m"]) && !string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                SttblCustomers st_cust = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"].ToString());
                ltcompname.Text = " - " + st_cust.Customer;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["companyid"]))
            {
                ddlD2DEmployee_SelectedIndexChanged(sender, e);
                chkformbayid_CheckedChanged(sender, e);
                CompanyEditForm(Request.QueryString["companyid"]);
            }

        }


        // divleft.Visible = false;

    }

    public void BindDropDown()
    {
        try
        {
            ddlCustTypeID.Items.Clear();
            ListItem item = new ListItem();
            item.Value = "";
            item.Text = "Select";
            ddlCustTypeID.Items.Add(item);
            ddlCustTypeID.DataSource = ClstblCustType.tblCustType_SelectActive_COmpany();
            ddlCustTypeID.DataMember = "CustType";
            ddlCustTypeID.DataTextField = "CustType";
            ddlCustTypeID.DataValueField = "CustTypeID";
            ddlCustTypeID.DataBind();

            ddlCustTypeID.SelectedValue = "1";
        }
        catch
        {

        }

        //if (Roles.IsUserInRole("Administrator"))
        //{
        //    ddlCustSourceID.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        //}
        //else
        //{
        //    ddlCustSourceID.DataSource = ClstblCustSource.tblCustSource_SelectByNewLead();
        //}
        ddlCustSourceID.Items.Clear();
        ListItem item1 = new ListItem();
        item1.Value = "";
        item1.Text = "Select";
        ddlCustSourceID.Items.Add(item1);
        try
        {
            ddlCustSourceID.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
            ddlCustSourceID.DataMember = "CustSource";
            ddlCustSourceID.DataTextField = "CustSource";
            ddlCustSourceID.DataValueField = "CustSourceID";
            ddlCustSourceID.DataBind();
        }
        catch { }
        //ddlCustSourceID.SelectedValue = "3";

        ddlSearchType.DataSource = ClstblCustType.tblCustType_SelectActive();
        ddlSearchType.DataMember = "CustType";
        ddlSearchType.DataTextField = "CustType";
        ddlSearchType.DataValueField = "CustTypeID";
        ddlSearchType.DataBind();

        ddlSearchSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlSearchSource.DataMember = "CustSource";
        ddlSearchSource.DataTextField = "CustSource";
        ddlSearchSource.DataValueField = "CustSourceID";
        ddlSearchSource.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();
        ddlPostalformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlPostalformbaystreettype.DataMember = "StreetType";
        ddlPostalformbaystreettype.DataTextField = "StreetType";
        ddlPostalformbaystreettype.DataValueField = "StreetCode";
        ddlPostalformbaystreettype.DataBind();

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        ddlD2DEmployee.DataSource = ClstblEmployees.tblEmployees_SelectOutDoor(userid);
        ddlD2DEmployee.DataMember = "fullname";
        ddlD2DEmployee.DataTextField = "fullname";
        ddlD2DEmployee.DataValueField = "EmployeeID";
        ddlD2DEmployee.DataBind();

    }
    public void BindDataCount()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        string Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "";
        if (chkViewAll.Checked)
        {
            userid = "";
            Employeeid = "";
        }
        if (Roles.IsUserInRole("CompanyManager"))
        {
            userid = "";
            Employeeid = "";
        }
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";

        DataTable dtcount = ClstblCustomers.query_execute(Count);
        //Response.Write(dtcount);
        //Response.End();
        if (dtcount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmpType=" + EmpType1 + ")) and (C.EmployeeID in (select EmployeeID from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))))";

            }
        }
        if (Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("BookInstallation") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC"))
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
            }
            else
            {
                Employeeid = "";
            }
        }
        else
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
            }
            else
            {
                Employeeid = st.EmployeeID;
            }
        }

        string empid = "";
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Lead Manager"))
        {
            empid = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmployeeID!=1 and userid in (select userid from aspnet_membership where islockedout=0)and EmployeeID in (select Employeeid from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split('" + SalesTeamID1 + "',',')))))";
        }
        else
        {
            empid = "and (C.EmployeeID='" + Employeeid + "' or '" + Employeeid + "'=0)";
        }



        //string data = "select count(CustomerID) as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and (C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) and (C.EmployeeID='" + Employeeid + "' or '" + Employeeid + "'=0) and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "' <= C.CustEntered or '" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "' >=C.CustEntered  or '" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "'='') " + data1;
        string data = "select count(CustomerID) as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and (C.isformbayadd='" + ddladdressverification.SelectedValue + "' or '" + ddladdressverification.SelectedValue + "'='') and (C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) " + empid + " and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= C.CustEntered or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=C.CustEntered  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') " + data1;

       

        //and ((CustEntered between '" + txtStartDate.Text + "' and '" + txtEndDate.Text + "') or '" + txtStartDate.Text + "' IS NULL or '" + txtEndDate.Text + "' IS NULL)
        //DataTable dtcount = ClstblCustomers.tblCustomersGetDataCount(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
        //Response.Write(data);
        //Response.End();
        DataTable dt = ClstblCustomers.query_execute(data);
        
        if (dt.Rows.Count > 0)
        {

            countdata = Convert.ToInt32(dt.Rows[0]["countdata"]);
            //Response.Write(countdata);
            //Response.End();
            hdncountdata.Value = countdata.ToString();

        }
        #region MyRegionForPagination

        if (custompagesize != 0 && ddlSelectRecords.SelectedValue != "All")
        {
            lastpageindex = Convert.ToInt32(hdncountdata.Value) / Convert.ToInt32(ddlSelectRecords.SelectedValue);

            if (Convert.ToInt32(hdncountdata.Value) % Convert.ToInt32(ddlSelectRecords.SelectedValue) > 0)
            {
                lastpageindex = lastpageindex + 1;
            }

            int pageindexcustom1 = Convert.ToInt32(hdncountdata.Value) / Convert.ToInt32(ddlSelectRecords.SelectedValue);

            if (pageindexcustom1 > 0)
            {
                if (Convert.ToInt32(hdncountdata.Value) % Convert.ToInt32(ddlSelectRecords.SelectedValue) > 0)
                {
                    pageindexcustom1 = pageindexcustom1 + 1;
                }

                if (pageindexcustom1 >= custompageIndex)
                {

                }
                else
                {
                    custompageIndex = pageindexcustom1;
                }

                int PageIndex = 3;
                int StartPageIndex = custompageIndex - PageIndex;//10-3=7
                int EndPageIndex = custompageIndex + PageIndex;//10+3=13

                if (StartPageIndex < 1)
                {
                    EndPageIndex = EndPageIndex - StartPageIndex;
                }
                if (EndPageIndex > lastpageindex)
                {
                    StartPageIndex = (StartPageIndex - (EndPageIndex - lastpageindex)) + 1;
                }
                DataTable dt_page = new DataTable();
                dt_page.Columns.Add("ID");

                for (int i = 1; i <= lastpageindex; i++)
                {
                    if ((StartPageIndex < i && EndPageIndex > i))
                    {
                        DataRow dr = dt_page.NewRow();
                        dr["ID"] = i;
                        dt_page.Rows.Add(dr);

                    }
                }

                rptpage.DataSource = dt_page;
                rptpage.DataBind();

                if (custompageIndex == 1)
                {
                    lnkfirst.Visible = false;
                    lnkprevious.Visible = false;
                    lnknext.Visible = true;
                    lnklast.Visible = true;
                }
                if (custompageIndex > 1)
                {
                    lnkfirst.Visible = true;
                    lnkprevious.Visible = true;
                }
                if (custompageIndex == EndPageIndex)
                {
                    lnknext.Visible = false;
                    lnklast.Visible = false;
                }
                if (lastpageindex == custompageIndex)
                {
                    lnknext.Visible = false;
                    lnklast.Visible = false;
                }
                if (custompageIndex > 1 && custompageIndex < lastpageindex)
                {
                    lnkfirst.Visible = true;
                    lnkprevious.Visible = true;
                    lnknext.Visible = true;
                    lnklast.Visible = true;
                }
            }
            else
            {
                rptpage.DataSource = null;
                rptpage.DataBind();
                lnkfirst.Visible = false;
                lnkprevious.Visible = false;
                lnknext.Visible = false;
                lnklast.Visible = false;
            }
        }
        else
        {
            rptpage.DataSource = null;
            rptpage.DataBind();
            lnkfirst.Visible = false;
            lnkprevious.Visible = false;
            lnknext.Visible = false;
            lnklast.Visible = false;
        }
        #endregion
    }
    protected DataTable GetGridData()
    {
        BindDataCount();

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        if (chkViewAll.Checked)
        {
            userid = "";
            Employeeid = "";
        }
        if (Roles.IsUserInRole("CompanyManager"))
        {
            userid = "";
            Employeeid = "";
        }
        if (ddlSelectRecords.SelectedValue!="All")
        {
            startindex = (custompageIndex - 1) * Convert.ToInt32(ddlSelectRecords.SelectedValue) + 1;
        }
        else
        {
            startindex = 1;
        }
        

        //int endindex = (custompageIndex * custompagesize);
        //Response.Write(txtSearch.Text + "','" + "" + "','" + ddlSearchType.SelectedValue + "','" + ddlSearchSource.SelectedValue + "','" + ddlSearchRec.SelectedValue + "','" + ddlSearchArea.SelectedValue + "','" + ddlSearchState.SelectedValue + "','" + txtStartDate.Text + "','" + txtEndDate.Text + "','" + txtSearchPostCode.Text + "','" + "" + "','" + txtSearchCompanyNo.Text + "','" + txtSerachCity.Text + "','" + txtSearchStreet.Text + "','" + ddlSearchSubSource.SelectedValue + "','" + startindex.ToString() + "','" + custompagesize.ToString());
        DataTable dt = new DataTable();
        string datagrid = "", Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "", data2 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";
        DataTable dtcount = ClstblCustomers.query_execute(Count);

        //Response.Write(dtcount.Rows.Count+"<=<br/>");
        if (dtcount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                //Response.Write(dtEmpType.Rows.Count + "<=emp<br/>");
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                //Response.Write(dtSalesTeamID.Rows.Count + "<=<br/>");
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmpType=" + EmpType1 + ")) and (C.EmployeeID in (select EmployeeID from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))))";
            }
        }

        if (Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("BookInstallation") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC"))
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
                data2 = "and C.CustTypeID not in (1, 2) and C.CustomerID in (select CustomerID from tblProjects where ProjectStatusID!=2)";
                //dt = ClstblCustomers.tblCustomers_SelectRoleWise(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
            }
            else
            {
                //dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), custompagesize.ToString());
                Employeeid = "";
            }

            //Response.Write();
            //Response.End(); 
        }
        else
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
                data2 = "and C.CustTypeID not in (1, 2) and C.CustomerID in (select CustomerID from tblProjects where ProjectStatusID!=2)";
                //dt = ClstblCustomers.tblCustomers_SelectRoleWise(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
            }
            else
            {
                Employeeid = st.EmployeeID;
                //dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, Employeeid, ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString(), txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), custompagesize.ToString());
                //dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, userid, ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString(), txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), custompagesize.ToString());
            }
        }

        string empid = "";
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Lead Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            empid = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmployeeID!=1 and userid in (select userid from aspnet_membership where islockedout=0)and EmployeeID in (select Employeeid from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split('" + SalesTeamID1 + "',',')))))";
        }
        else
        {
            empid = "and (C.EmployeeID='" + Employeeid + "' or '" + Employeeid + "'=0)";
        }

        //datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) and (C.EmployeeID='" + Employeeid + "' or '" + Employeeid + "'=0) and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "' <= C.CustEntered or '" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "' >=C.CustEntered  or '" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";
        if (ddlSelectRecords.SelectedValue != "All")
        {
            if (ddlSelectRecords.SelectedValue == "50" || ddlSelectRecords.SelectedValue == "100")
            {
                datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) " + empid + "and (C.isformbayadd='" + ddladdressverification.SelectedValue + "' or '" + ddladdressverification.SelectedValue + "'='') and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= C.CustEntered or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=C.CustEntered  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + ddlSelectRecords.SelectedValue.ToString() + ">0 then(" + ddlSelectRecords.SelectedValue.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";
            }
            else
            {
                datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) " + empid + "and (C.isformbayadd='" + ddladdressverification.SelectedValue + "' or '" + ddladdressverification.SelectedValue + "'='') and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= C.CustEntered or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=C.CustEntered  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";
            }            
        }
        else
        {
            datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) " + empid + "and (C.isformbayadd='" + ddladdressverification.SelectedValue + "' or '" + ddladdressverification.SelectedValue + "'='') and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= C.CustEntered or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=C.CustEntered  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT (select count(CustomerID) from tblCustomers ) ROWS ONLY ;";
        }




        //Response.Write(custompagesize.ToString() + "==" + startindex.ToString());
        //Response.End();
        //Response.Write(datagrid);
        //Response.End();
        dt = ClstblCustomers.query_execute(datagrid);
        //Response.Write(dt.Rows.Count);
        //Response.End();
        //   ModalPopupExtender1.Hide();

        int sum = dt.Rows.Count;

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        int iTotalRecords = 0;
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        PanSearch.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);


        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            HidePanels();
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                //     PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            ltrPage.Text = "";
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();


            //   divnopage.Visible = true;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All" && ddlSelectRecords.SelectedValue != "50" && ddlSelectRecords.SelectedValue != "100")
            {
                //Response.Write("Hii");
                //Response.End();
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;

                }
                else
                {
                    foreach (RepeaterItem item in rptpage.Items)
                    {
                        LinkButton senderbtn = (LinkButton)item.FindControl("lnkpagebtn");
                        HiddenField postIdHidden = (HiddenField)item.FindControl("psotId");
                        LinkButton lnkpagebtn = (LinkButton)item.FindControl("lnkpagebtn");
                        if (Convert.ToInt32(postIdHidden.Value) == custompageIndex)
                        {
                            senderbtn.CssClass = "Linkbutton activepage";
                        }
                    }
                    divnopage.Visible = true;
                    iTotalRecords = Convert.ToInt32(hdncountdata.Value);
                    int iEndRecord = GridView1.PageSize * (custompageIndex);

                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }

                    //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " entries";
                }
            }
            else
            {
                //Response.Write("Hii");
                //Response.End();
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
                else
                {
                    if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                    {
                        //========label Hide
                        divnopage.Visible = false;

                    }
                    else
                    {
                        foreach (RepeaterItem item in rptpage.Items)
                        {
                            LinkButton senderbtn = (LinkButton)item.FindControl("lnkpagebtn");
                            HiddenField postIdHidden = (HiddenField)item.FindControl("psotId");
                            LinkButton lnkpagebtn = (LinkButton)item.FindControl("lnkpagebtn");
                            if (Convert.ToInt32(postIdHidden.Value) == custompageIndex)
                            {
                                senderbtn.CssClass = "Linkbutton activepage";
                            }
                        }
                        divnopage.Visible = true;
                        iTotalRecords = Convert.ToInt32(hdncountdata.Value);
                        int iEndRecord = GridView1.PageSize * (custompageIndex);
                        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                        if (iStartsRecods == 0)
                        {
                            iStartsRecods = 1;
                        }
                        if (iEndRecord > iTotalRecords)
                        {
                            iEndRecord = iTotalRecords;
                        }
                        if (iEndRecord == 0)
                        {
                            iEndRecord = iTotalRecords;
                        }


                        //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " entries";
                    }
                }
            }



            if (Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installer"))
            {
                GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 3].Visible = false;
                lnkAdd.Visible = false;
            }
            //   PanNoRecord.Visible = false;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {

                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    //divnopage.Visible = false;

                }
                else
                {

                    //divnopage.Visible = true;
                    //int iTotalRecords1 = dv.ToTable().Rows.Count;
                    // int iTotalRecords = Convert.ToInt32(hdncountdata.Value);
                    int iEndRecord = GridView1.PageSize * (custompageIndex);

                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    

                    //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    // divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        //Response.Write(userid);
        //Response.End();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = st.EmployeeID;

        string CustTypeID = ddlCustTypeID.SelectedValue;
        string CustSourceID = ddlCustSourceID.SelectedValue;
        string CustSourceSubID = ddlCustSourceSubID.SelectedValue;
        string ResCom = false.ToString();
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }
        string CustEnteredBy = st.EmployeeID;
        string Customer = txtCompany.Text;
        string StreetAddress = txtStreetAddress.Text;
        //string StreetCity = ddlStreetCity.Text;
        //string StreetState = txtStreetState.Text;
        //string StreetPostCode = txtStreetPostCode.Text;
        string PostalAddress = txtPostalAddress.Text;
        string PostalCity = ddlPostalCity.Text;
        string PostalState = txtPostalState.Text;
        string PostalPostCode = txtPostalPostCode.Text;
        string Country = "Australia";
        string CustPhone = txtCustPhone.Text;
        string CustAltPhone = txtCustAltPhone.Text;
        string CustFax = txtCustFax.Text;
        string CustNotes = txtCustNotes.Text;
        string CustWebSite = txtCustWebSiteLink.Text;
        string CustWebSiteLink = txtCustWebSiteLink.Text;
        string BranchLocation = txtStreetState.Text + " - " + ddlStreetCity.Text;
        string Area = false.ToString();
        int success = 0;
        if (rblArea.SelectedItem.Value == "1")
        {
            Area = "1";
        }
        if (rblArea.SelectedItem.Value == "2")
        {
            Area = "2";
        }
        //int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        //if (txtstreetaddressline.Text.Trim() != string.Empty)//chkformbayid.Checked.ToString() == true.ToString()
        //{
        //Response.Write(txtstreetaddressline.Text + "=" + StreetCity + "=" + StreetState + "=" + StreetPostCode);
        //Response.End();

        txtStreetAddress.Text = ddlformbayunittype.SelectedValue.Trim() + " " + txtformbayUnitNo.Text.Trim() + " " + txtformbayStreetNo.Text.Trim() + " " + txtformbaystreetname.Text.Trim() + " " + ddlformbaystreettype.SelectedValue.Trim();

        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_SelectCustomer2(txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
        //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);

        if (dt1.Rows.Count > 0)
        {
            PanSearch.Visible = false;
            ModalPopupExtenderAddress.Show();
            PanAddUpdate.Visible = true;
            DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomer2(txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
            rptaddress.DataSource = dt;
            rptaddress.DataBind();
            txtstreetaddressline.Focus();
        }
        else
        {

            //------------Existence of Street Address----------//

            if (chkformbayid.Checked.ToString() == false.ToString())
            {

                //int existaddress = ClstblCustomers.tblCustomers_ExitsStreet_address(txtstreetaddressline.Text);
                int existaddress = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
                if (existaddress != 1)
                {
                    success = ClstblCustomers.tblCustomers_Insert("", EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, "", "", "", "", "", "", Customer, BranchLocation, "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area);
                    int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), txtContMr.Text.Trim(), txtContFirst.Text.Trim(), txtContLast.Text.Trim(), txtContMobile.Text, txtContEmail.Text, EmployeeID, CustEnteredBy);
                    //Response.Write("Convert.ToString(success)" + Convert.ToString(success) + "Customer Entered on" + Customer Entered on + "DateTime.Now.AddHours(14).ToShortDateString()" + DateTime.Now.AddHours(14).ToShortDateString() + "DateTime.Now.AddHours(14).ToString()" + DateTime.Now.AddHours(14).ToString() + "Convert.ToString(sucContacts)" + Convert.ToString(sucContacts) + "CustEnteredBy" + CustEnteredBy + "1"  + 1);
                    //Response.End();

                    //Response.Write(Convert.ToString(success) + "<br>" + "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString() + "<br>" + DateTime.Now.AddHours(14).ToString() + "<br>" + Convert.ToString(sucContacts) + "<br>" + CustEnteredBy + "<br>" + "1");
                    //Response.End();
                    int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), CustEnteredBy, "1");
                    ClstblCustomers.tblCustomers_UpdateD2DEmp(Convert.ToString(success), ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);

                    bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtstreetaddressline.Text);


                    bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);

                    bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), txtpostaladdressline.Text);

                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(Convert.ToString(success), true.ToString());
                    }

                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(Convert.ToString(success), CustPhone);

                    if (txtD2DAppDate.Text != string.Empty)
                    {
                        bool succ1 = ClstblCustomers.tblCustomers_Updateappointment_status("0", Convert.ToString(success));
                        bool succ2 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status("0", Convert.ToString(success));
                    }

                    int suc = ClstblCustomers.tblCustLogReport_Insert(Convert.ToString(success));
                    string LogReport = "";
                    string a = "";

                    if (Convert.ToString(suc) != "")
                    {
                        int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                        if (exist == 1)
                        {
                        //    Response.Write("");
                        //    Response.End();
                            //a = "Update tblContacts set ContFirst='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                            LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                        if (exist2 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                        if (exist3 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                        if (exist4 == 1)
                        {
                            LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        ClsProjectSale.ap_form_element_execute(LogReport);


                        if (Convert.ToString(success) != "")
                        {
                            //PanSuccess.Visible = true;
                            SetAdd();
                            lnkAdd.Visible = true;
                            lnkBack.Visible = false;
                            lnkclose.Visible = false;
                        }
                        else
                        {
                            SetError();
                        }

                        BindGrid(0);
                    }
                }
                else
                {
                    PAnAddress.Visible = true;
                    BindGrid(0);
                    //lblexistame.Visible = true;
                    //lblexistame.Text = "Record with this Address already exists ";
                }

            }

            else
            {
                int existaddress = ClstblCustomers.tblCustomers_ExitsStreet_address(txtStreetAddress.Text);
                //int existaddress = ClstblCustomers.tblCustomers_ExitsStreet_address(txtstreetaddressline.Text);

                if (existaddress != 1)
                {
                    success = ClstblCustomers.tblCustomers_Insert("", EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, "", "", "", "", "", "", Customer, BranchLocation, "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area);

                    //Response.Write("YEs");
                    //Response.End();
                    int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), txtContMr.Text.Trim(), txtContFirst.Text.Trim(), txtContLast.Text.Trim(), txtContMobile.Text, txtContEmail.Text, EmployeeID, CustEnteredBy);
                    //Response.Write(Convert.ToString(success) + "<br>" + "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString() + "<br>" + DateTime.Now.AddHours(14).ToString() + "<br>" + Convert.ToString(sucContacts) + "<br>" + CustEnteredBy + "<br>" + "1");
                    //Response.End();
                    int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), CustEnteredBy, "1");
                    ClstblCustomers.tblCustomers_UpdateD2DEmp(Convert.ToString(success), ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);
                    //bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);

                    bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);

                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtStreetAddress.Text);
                    //bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtstreetaddressline.Text);


                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
                    bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), ddlPostalformbayunittype.SelectedValue, txtPostalformbayUnitNo.Text, txtPostalformbayStreetNo.Text, txtPostalformbaystreetname.Text, ddlPostalformbaystreettype.SelectedValue, hndstreetsuffix1.Value);

                    bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), txtpostaladdressline.Text);


                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(Convert.ToString(success), true.ToString());
                    }

                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(Convert.ToString(success), CustPhone);

                    if (txtD2DAppDate.Text != string.Empty)
                    {
                        bool succ1 = ClstblCustomers.tblCustomers_Updateappointment_status("0", Convert.ToString(success));
                        bool succ2 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status("0", Convert.ToString(success));
                    }

                    int suc = ClstblCustomers.tblCustLogReport_Insert(Convert.ToString(success));
                    string LogReport = "";

                    if (Convert.ToString(suc) != "")
                    {
                        int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                        if (exist == 1)
                        {
                            LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                        if (exist2 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                        if (exist3 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                        if (exist4 == 1)
                        {
                            LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        ClsProjectSale.ap_form_element_execute(LogReport);
                        if (Convert.ToString(success) != "")
                        {
                            SetAdd();
                            lnkAdd.Visible = true;
                            lnkBack.Visible = false;
                            lnkclose.Visible = false;
                        }
                        else
                        {
                            SetError();
                        }
                        BindGrid(0);
                    }
                }
                else
                {
                    PAnAddress.Visible = true;
                    BindGrid(0);
                    //lblexistame.Visible = true;
                    //lblexistame.Text = "Record with this Address already exists ";
                }
                // BindGrid(0);

                //Response.Write(success);
                //Response.End();
            }




            //------------Existence of Postal Address----------//


            //int sucno = ClstblCustomers.tblCompanyNumber_Insert(Convert.ToString(success));
            //DataTable dtCN = ClstblCustomers.tblCompanyNumber_Select(Convert.ToString(success));
            //string CompanyNumber = dtCN.Rows[0]["CompanyNumberID"].ToString();
            //bool sucCompNo = ClstblCustomers.tblCustomers_UpdateCompanyNumber(Convert.ToString(success), CompanyNumber);

        }
        //}
        //  BindScript();

    }
    public string GetErrorMessage(MembershipCreateStatus status)
    {
        switch (status)
        {
            case MembershipCreateStatus.DuplicateUserName:
                return "User with this name already exists. Please enter a different User Name.";

            case MembershipCreateStatus.DuplicateEmail:
                return "A E-mail with address already exists. Please enter a different e-mail address.";

            case MembershipCreateStatus.InvalidPassword:
                return "The password provided is invalid. Please enter a valid password value.";

            case MembershipCreateStatus.InvalidEmail:
                return "The e-mail address provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidAnswer:
                return "The password retrieval answer provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidQuestion:
                return "The password retrieval question provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidUserName:
                return "The user name provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.ProviderError:
                return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            case MembershipCreateStatus.UserRejected:
                return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            default:
                return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //BindScript();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        ;
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string id1 = "";
        if (!string.IsNullOrEmpty(Request.QueryString["companyid"]))
        {
            id1 = (Request.QueryString["companyid"]);
        }
        else
        {
            id1 = GridView1.SelectedDataKey.Value.ToString();
        }


        string EmployeeID = st.EmployeeID;
        string CustTypeID = ddlCustTypeID.SelectedValue;
        string CustSourceID = ddlCustSourceID.SelectedValue;
        string CustSourceSubID = ddlCustSourceSubID.SelectedValue;
        string ResCom = false.ToString();
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }
        //bool suc_mobile1 = ClstblContacts.tblContacts_UpdateContMobile(id1, txtContMobile.Text);
        string CustEnteredBy = st.EmployeeID;
        string StreetAddress = txtStreetAddress.Text = ddlformbayunittype.SelectedValue.Trim() + " " + txtformbayUnitNo.Text.Trim() + " " + txtformbayStreetNo.Text.Trim() + " " + txtformbaystreetname.Text.Trim() + " " + ddlformbaystreettype.SelectedValue.Trim();
        //string StreetAddress = txtStreetAddress.Text;
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string PostalAddress = txtPostalAddress.Text;
        string PostalCity = ddlPostalCity.Text;
        string PostalState = txtPostalState.Text;
        string PostalPostCode = txtPostalPostCode.Text;
        string Country = "Australia";
        string CustPhone = txtCustPhone.Text;
        string CustAltPhone = txtCustAltPhone.Text;
        string CustFax = txtCustFax.Text;
        string CustNotes = txtCustNotes.Text;
        string CustWebSiteLink = txtCustWebSiteLink.Text;
        string Area = false.ToString();
        bool success = false;
        if (rblArea.SelectedItem.Value == "1")
        {
            Area = "1";
        }
        if (rblArea.SelectedItem.Value == "2")
        {
            Area = "2";
        }
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID2(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
        //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
        //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
        //Response.Write(id1 + "  " + txtStreetAddress.Text + "  " + StreetCity.Trim() + "  " + StreetState.Trim() + "  " + StreetPostCode.Trim() + "  " + txtContMobile.Text.Trim() + "  " + txtContEmail.Text.Trim());
        //Response.End();
        if (dt1.Rows.Count > 0)
        {

            PanSearch.Visible = false;
            ModalPopupExtenderAddress.Show();
            PanAddUpdate.Visible = true;
            DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID2(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
            rptaddress.DataSource = dt;
            rptaddress.DataBind();
            txtstreetaddressline.Focus();
        }
        else
        {
            if (chkformbayid.Checked.ToString() == "False")
            {
                //int existaddress = ClstblCustomers.tblCustomers_ExitsByID_Street_address(id1, txtstreetaddressline.Text);
                int existaddress = ClstblCustomers.tblCustomers_ExitsByID_Address(id1, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
                if (existaddress != 1)
                {
                   // Response.End();
                    bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    //bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(id1, txtstreetaddressline.Text);
                    success = ClstblCustomers.tblCustomers_Update(id1, EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, CustEnteredBy, "", "", "", "", "", "", "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area, txtCompany.Text);

                    bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(id1, ddlPostalformbayunittype.SelectedValue, txtPostalformbayUnitNo.Text, txtPostalformbayStreetNo.Text, txtPostalformbaystreetname.Text, ddlPostalformbaystreettype.SelectedValue, hndstreetsuffix1.Value);
                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(id1, hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
                    bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(id1, txtpostaladdressline.Text);
                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(id1, true.ToString());
                    }

                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(id1, CustPhone);
                    bool suc_mobile = ClstblContacts.tblContacts_UpdateContMobile(id1, txtContMobile.Text);

                    string LogReport = "";
                    string LogReportName = "";
                    string LogReportEmail = "";
                    int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                    if (exist == 1)
                    {                        
                        LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                        LogReportName = "Update tblContacts set ContFirst='" + txtContFirst.Text + "' , ContLast='" + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    else
                    {
                        LogReportName = "Update tblContacts set ContFirst='" + txtContFirst.Text + "' , ContLast='" + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                        //Response.Write(LogReport1);
                        //Response.End();
                    }
                    int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                    if (exist2 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                    if (exist3 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    else
                    {
                        LogReportEmail = "Update tblContacts set ContEmail='"+txtContEmail.Text+"' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                    if (exist4 == 1)
                    {
                        LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    ClsProjectSale.ap_form_element_execute(LogReport);

                    ClsProjectSale.ap_form_element_execute(LogReportName);

                    ClsProjectSale.ap_form_element_execute(LogReportEmail);

                    ClstblCustomers.tblCustomers_UpdateD2DEmp(id1, ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);

                    //--- do not chage this code Start------
                    if (success)
                    {
                        SetUpdate();
                    }
                    else
                    {
                        SetError();
                    }
                    BindGrid(0);

                }
                else
                {
                    PAnAddress.Visible = true;
                    BindGrid(0);
                }
            }
            else
            {
                //Response.Write("else");
                //Response.End();
                int existaddress = ClstblCustomers.tblCustomers_ExitsByID_Streetaddress(id1, StreetAddress);
                if (existaddress != 1)
                {
                    bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    //bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(id1, txtstreetaddressline.Text);
                    success = ClstblCustomers.tblCustomers_Update(id1, EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, CustEnteredBy, "", "", "", "", "", "", "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area, txtCompany.Text);

                    bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(id1, hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
                    bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(id1, txtpostaladdressline.Text);
                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(id1, true.ToString());
                    }
                    //Response.Write(chkformbayid.Checked.ToString());
                    //Response.End();
                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(id1, CustPhone);
                    bool suc_mobile = ClstblContacts.tblContacts_UpdateContMobile(id1, txtContMobile.Text);

                    string LogReport = "";
                    int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                    if (exist == 1)
                    {
                        LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                    if (exist2 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                    if (exist3 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                    if (exist4 == 1)
                    {
                        LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    ClsProjectSale.ap_form_element_execute(LogReport);
                    ClstblCustomers.tblCustomers_UpdateD2DEmp(id1, ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);

                    //--- do not chage this code Start------
                    if (success)
                    {
                        SetUpdate();
                    }
                    else
                    {
                        SetError();
                    }
                    BindGrid(0);
                }
                else
                {
                    PanAddUpdate.Visible = true;
                    PanSearch.Visible = false;
                    ModalPopupExtenderAddress.Show();
                    DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
                    //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
                    rptaddress.DataSource = dt;
                    rptaddress.DataBind();
                    txtstreetaddressline.Focus();
                    //BindGrid(0);
                }
            }


        }
    }
    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Detail")
        {
            SetCancel();
            string custid = e.CommandArgument.ToString();

            //Profile.eurosolar.companyid = custid;
            ViewCompanyDetail(custid);

            lnkBack.Visible = true;
        }
        if (e.CommandName == "lnkphoto")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string custid = e.CommandArgument.ToString();
            //Response.Write(custid);
            //Response.End();
            DataTable dt = ClstblCustomers.tbl_CustImage_SelectByCustomerID(custid);
            RepeaterImages.DataSource = dt;
            RepeaterImages.DataBind();
            //Profile.eurosolar.companyid = custid;
        }
        BindGrid(0);
    }

    protected void BindUserControls()
    {
        divright.Visible = true;
        divgrid.Visible = false;
        divleft.Visible = false;
        leftgrid.Visible = false;
        companysummary1.BindSummary();
        //project1.BindProjects();
        //info1.BindInfo();
        //contacts1.BindContact();
        //bansi
        lnkclose.Visible = false;
    }

    public void ViewCompanyDetail(string custid)
    {
        if (Roles.IsUserInRole("Administrator"))
        {
            lnkBack.Visible = true;
        }
        if (Roles.IsUserInRole("Accountant"))
        {
            lnkAdd.Visible = false;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            lnkAdd.Visible = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {

        }
        if (Roles.IsUserInRole("Installer"))
        {
            lnkAdd.Visible = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            lnkAdd.Visible = true;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            lnkAdd.Visible = true;
        }
        if (Roles.IsUserInRole("Sales Manager"))
        {

        }
        if (Roles.IsUserInRole("STC"))
        {
            lnkAdd.Visible = false;
        }

        TabContainer1.ActiveTabIndex = 0;
        if (string.IsNullOrEmpty(Request.QueryString["m"]))
        {
            contacts1.HideTabContainer();

        }
        BindUserControls();

        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(custid);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;

        if (EmployeeID == st.EmployeeID)
        {
            contacts1.ShowContactAction();
            TabProject.Visible = true;
            TabInfo.Visible = true;

            if (st.CustTypeID == "11")
            {
                //  TabProject.Visible = false;
            }
            else
            {
                // TabProject.Visible = true;
            }
        }
        else
        {
            if (!Roles.IsUserInRole("Administrator"))
            {

                contacts1.HideContactAction();
                info1.HideInfoAction();
                if (Roles.IsUserInRole("SalesRep"))
                {
                    TabProject.Visible = false;
                    TabInfo.Visible = false;
                }
            }
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        //ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        //ddlformbaystreettype.DataMember = "StreetType";
        //ddlformbaystreettype.DataTextField = "StreetType";
        //ddlformbaystreettype.DataValueField = "StreetCode";
        //ddlformbaystreettype.DataBind();
        //ddlPostalformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        //ddlPostalformbaystreettype.DataMember = "StreetType";
        //ddlPostalformbaystreettype.DataTextField = "StreetType";
        //ddlPostalformbaystreettype.DataValueField = "StreetCode";
        //ddlPostalformbaystreettype.DataBind();

        lblcompanyaddress.Text = "Edit Company Address";
        lbladdcompany.Text = "Edit New Company";
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        ddlD2DEmployee_SelectedIndexChanged(sender, e);
        chkformbayid_CheckedChanged(sender, e);
        CompanyEditForm(id);

        //--- do not chage this code end------
    }

    public void CompanyEditForm(string id)
    {

        //DataTable dt_userid = ClstblCustomers.tbl_getUseridFromCustomerid(id);
        //DataRow userid = dt_userid.Rows[0];

        //    DataTable dtt = ClstblEmployees.tblEmployees_SelectOutDoor(userid["userid"].ToString());


        //ddlD2DEmployee.DataSource = dtt;
        //ddlD2DEmployee.DataMember = "fullname";
        //ddlD2DEmployee.DataTextField = "fullname";
        //ddlD2DEmployee.DataValueField = "EmployeeID";
        //ddlD2DEmployee.DataBind();

        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();

        ddlPostalformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlPostalformbayunittype.DataMember = "UnitType";
        ddlPostalformbayunittype.DataTextField = "UnitType";
        ddlPostalformbayunittype.DataValueField = "UnitType";
        ddlPostalformbayunittype.DataBind();

        //ddlformbaystreetname.DataSource = ClstblStreetType.tbl_formbaystreetname_SelectbyAsc();
        //ddlformbaystreetname.DataMember = "StreetName";
        //ddlformbaystreetname.DataTextField = "StreetName";
        //ddlformbaystreetname.DataValueField = "StreetName";
        //ddlformbaystreetname.DataBind();

        //ddlPostalformbaystreetname.DataSource = ClstblStreetType.tbl_formbaystreetname_SelectbyAsc();
        //ddlPostalformbaystreetname.DataMember = "StreetName";
        //ddlPostalformbaystreetname.DataTextField = "StreetName";
        //ddlPostalformbaystreetname.DataValueField = "StreetName";
        //ddlPostalformbaystreetname.DataBind();

        txtContMr.Focus();
        lblAddUpdate.Visible = false;
        Haddcompadd.Visible = true;
        Haddcomp.Visible = true;
        // divnopage.Visible = false;

        if (Roles.IsUserInRole("Administrator"))
        {
            ddlCustSourceID.Enabled = true;
            ddlCustSourceSubID.Enabled = true;
            //tdupdateaddress.Visible = true;
        }
        else
        {
            ddlCustSourceID.Enabled = false;
            ddlCustSourceSubID.Enabled = false;
            //tdupdateaddress.Visible = false;
        }

        ddlCustSourceSubID.DataSource = ClstblCustSourceSub.tblCustSourceSub_Select();
        ddlCustSourceSubID.DataMember = "CustSourceSub";
        ddlCustSourceSubID.DataTextField = "CustSourceSub";
        ddlCustSourceSubID.DataValueField = "CustSourceSubID";
        ddlCustSourceSubID.DataBind();
        hdnupdateaddress.Value = id;
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(id);
        //Response.Write(id);
        //Response.End();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(id);

        DataTable dtdatetime = ClstblCustomers.getUserRoles(id);






        if (dtCont.Rows.Count > 0)
        {
            txtContMr.Text = dtCont.Rows[0]["ContMr"].ToString();
            txtContFirst.Text = dtCont.Rows[0]["ContFirst"].ToString();
            txtContLast.Text = dtCont.Rows[0]["ContLast"].ToString();
            txtContEmail.Text = dtCont.Rows[0]["ContEmail"].ToString();
            txtContMobile.Text = dtCont.Rows[0]["ContMobile"].ToString();

            if (dtdatetime.Rows.Count > 0)
            {

                foreach (DataRow role in dtdatetime.Rows)
                {

                    if (role["rolename"].ToString() == "Lead Manager" || role["rolename"].ToString() == "leadgen")
                    {

                        //divD2DEmployee.Visible = true;
                        //divD2DAppDate.Visible = true;
                        //divD2DAppTime.Visible = true;

                        Response.Redirect("~/admin/adminfiles/company/ECompany.aspx?compid=" + id);
                    }
                }

            }

        }

        if (st.isformbayadd == "1")
        {
            chkformbayid.Checked = true;
        }
        else
        {
            chkformbayid.Checked = false;
        }

        txtformbayUnitNo.Text = st.unit_number;
        txtformbayStreetNo.Text = st.street_number;
        try
        {
            txtformbaystreetname.Text = st.street_name;
        }
        catch { }



        try
        {
            ddlformbaystreettype.SelectedValue = st.street_type;
        }
        catch { }
        try
        {
            ddlformbayunittype.SelectedValue = st.unit_type;
        }
        catch { }

        try
        {
            txtPostalformbaystreetname.Text = st.pstreet_name;
        }
        catch { }

        string stnam = txtPostalformbaystreetname.Text;
        streetname = Getstreetname(stnam);
        try
        {
            ddlPostalformbaystreettype.SelectedValue = st.street_type;
        }
        catch { }
        try
        {
            ddlPostalformbayunittype.SelectedValue = st.unit_type;
        }
        catch { }

        txtPostalformbayUnitNo.Text = st.unit_number;
        txtPostalformbayStreetNo.Text = st.street_number;

        try
        {
            ddlCustTypeID.SelectedValue = st.CustTypeID;
        }
        catch
        {
        }
        try
        {
            ddlCustSourceID.SelectedValue = st.CustSourceID;
        }
        catch
        {
        }
        try
        {

            if (st.CustSourceSubID != string.Empty)
            {

                ddlCustSourceSubID.SelectedValue = st.CustSourceSubID;
            }
        }
        catch
        {
        }
        //if (st.street_number != string.Empty)
        //{
        //    
        //}
        //try
        //{
        //  
        //}
        //catch
        //{
        //}
        //try
        //{
        //rblResCom.SelectedValue = st.ResCom;
        if (st.ResCom != string.Empty)
        {
            if (st.ResCom == "1")
            {
                rblResCom1.Checked = true;
            }
            if (st.ResCom == "2")
            {
                rblResCom2.Checked = true;
            }
        }
        else
        {
            rblResCom1.Checked = true;
        }
        //}
        //catch
        //{
        //}
        txtstreetaddressline.Text = st.street_address;
        txtpostaladdressline.Text = st.Postal_address;
        txtCompany.Text = st.Customer;
        txtCustPhone.Text = st.CustPhone;

        //txtStreetAddress.Text = st.StreetAddress;
        if (!string.IsNullOrEmpty(st.StreetAddress))
        {
            txtStreetAddress.Text = st.StreetAddress;
        }
        else
        {
            txtStreetAddress.Text = st.unit_type + " " + st.unit_number + " " + st.street_number + " " + st.street_name + " " + st.street_type;
        }

        ddlStreetCity.Text = st.StreetCity;
        txtStreetState.Text = st.StreetState;
        txtStreetPostCode.Text = st.StreetPostCode;
        //txtPostalAddress.Text = st.PostalAddress;
        if (!string.IsNullOrEmpty(st.StreetAddress))
        {
            txtPostalAddress.Text = st.StreetAddress;
        }
        else
        {
            txtPostalAddress.Text = st.unit_type + " " + st.unit_number + " " + st.street_number + " " + st.street_name + " " + st.street_type;
        }

        ddlPostalCity.Text = st.PostalCity;
        txtPostalState.Text = st.PostalState;
        txtPostalPostCode.Text = st.PostalPostCode;
        txtCustAltPhone.Text = st.CustAltPhone;
        txtCustFax.Text = st.CustFax;
        txtCustNotes.Text = st.CustNotes;
        txtCustWebSiteLink.Text = st.CustWebSiteLink;

        if (st.CustSourceID == "2")
        {
            ddlCustSourceSubID.Visible = true;
        }
        else
        {
            ddlCustSourceSubID.Visible = false;
        }
        if (st.Area != string.Empty)
        {
            //rblArea.SelectedValue = st.Area;
            if (st.Area == "1")
            {
                rblArea.SelectedIndex = 0;
            }
            if (st.Area == "2")
            {
                rblArea.SelectedIndex = 1;
            }
        }
        else
        {
            rblArea.ClearSelection();
        }
        try
        {
            ddlD2DEmployee.SelectedValue = st.D2DEmployee;
        }
        catch { }
        try
        {
            txtD2DAppDate.Text = Convert.ToDateTime(st.D2DAppDate).ToShortDateString();
        }
        catch { }


        ListItem item1 = new ListItem();
        item1.Text = "Time";
        item1.Value = "";
        ddlAppTime.Items.Clear();
        ddlAppTime.Items.Add(item1);

        if (st.D2DEmployee != string.Empty)
        {
            if (ddlD2DEmployee.SelectedValue != string.Empty)
            {
                ddlAppTime.DataSource = ClstblLTeamTime.tblLTeamTime_SelectByEmp_Edit(ddlD2DEmployee.SelectedValue, id);
                ddlAppTime.DataMember = "Time";
                ddlAppTime.DataTextField = "Time";
                ddlAppTime.DataValueField = "ID";
                ddlAppTime.DataBind();

                if (st.D2DAppTime != string.Empty)
                {

                    ddlAppTime.SelectedValue = st.D2DAppTime;
                }
            }
        }
        //try
        //{

        //}
        //catch { }

        //--- do not chage this code start------

        InitUpdate();
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        BindScript();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            // custompagesize = 0;
        }

        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            // custompagesize = Convert.ToInt32(ddlSelectRecords.SelectedValue.ToString());
        }
        BindGrid(0);
        BindScript();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();



        DataView sortedView = new DataView(dt);

        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////// Don't Change Start
        ////string SortDir = string.Empty;
        //    if (dir == SortDirection.Ascending)
        //    {
        //        dir = SortDirection.Descending;
        //        SortDir = "Desc";
        //    }
        //    else
        //    {
        //        dir = SortDirection.Ascending;
        //        SortDir = "Asc";
        //    }
        ////////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();


    }
    public SortDirection dir
    {
        get
        {

            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Descending;
            }

            else
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        PanGrid.Visible = true;

        PanSearch.Visible = true;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        //  PanGrid.Visible = false;
        PanSearch.Visible = false;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/company/Company.aspx");
        SetCancel();
        BindGrid(0);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        // PanAddUpdate.Visible = true;
        //ddlformbaystreetname.DataSource = ClstblStreetType.tbl_formbaystreetname_SelectbyAsc();
        //ddlformbaystreetname.DataMember = "StreetName";
        //ddlformbaystreetname.DataTextField = "StreetName";
        //ddlformbaystreetname.DataValueField = "StreetName";
        //ddlformbaystreetname.DataBind();
        //ddlPostalformbaystreetname.DataSource = ClstblStreetType.tbl_formbaystreetname_SelectbyAsc();
        //ddlPostalformbaystreetname.DataMember = "StreetName";
        //ddlPostalformbaystreetname.DataTextField = "StreetName";
        //ddlPostalformbaystreetname.DataValueField = "StreetName";
        //ddlPostalformbaystreetname.DataBind();

        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();
        ddlPostalformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlPostalformbayunittype.DataMember = "UnitType";
        ddlPostalformbayunittype.DataTextField = "UnitType";
        ddlPostalformbayunittype.DataValueField = "UnitType";
        ddlPostalformbayunittype.DataBind();

        txtContMr.Focus();
        ClearSelection();
        PanSearch.Visible = false;
        InitAdd();
        PAnAddress.Visible = false;
        Reset();
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        //divnopage.Visible = false;
        if (Roles.IsUserInRole("SalesRep"))
        {
            ddlCustSourceID.Items.Remove(ddlCustSourceID.Items.FindByText("Lead Source"));
        }
        chkformbayid_CheckedChanged(sender, e);
    }
    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetAdd()
    {

        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        // PanNoRecord.Visible = false;
        PanSearch.Visible = true;
        Reset();
        // HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //PanAddUpdate.Visible = false;
        //lnkAdd.Visible = true;
        //lnkBack.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();

        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = " ";

        //HidePanels();
        //PanAddUpdate.Visible = true;

        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = true;

        //lnkAdd.Visible = false;
        //lnkBack.Visible = true;
        ////lblAddUpdate.Text = "Add ";

        //rblArea.ClearSelection();
        rblResCom1.Checked = true;
    }
    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";

        //HidePanels();
        //PanAddUpdate.Visible = true;

        //btnAdd.Visible = false;
        //btnUpdate.Visible = true;
        //btnCancel.Visible = true;
        //btnReset.Visible = false;
        //lnkAdd.Visible = false;
        //lnkBack.Visible = true;


        //lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        //  PanNoRecord.Visible = false;
        PAnAddress.Visible = false;
        divTeamTime.Visible = false;
        //PanAlreadExists.Visible = false;
        //PanSuccess.Visible = false;
        //PanError.Visible = false;
        //PanNoRecord.Visible = false;
        //PanAddUpdate.Visible = false;
        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        //PanSearch.Visible = true;


        //tdupdateaddress.Visible = false;
    }
    public void Reset()
    {
        //tdupdateaddress.Visible = false;
        PanAddUpdate.Visible = true;

        ddlformbayunittype.SelectedValue = "";
        txtformbayUnitNo.Text = string.Empty;
        txtformbayStreetNo.Text = string.Empty;
        txtformbaystreetname.Text = string.Empty;
        ddlformbaystreettype.ClearSelection();

        ddlPostalformbayunittype.SelectedValue = "";
        txtPostalformbayUnitNo.Text = string.Empty;
        txtPostalformbayStreetNo.Text = string.Empty;
        txtPostalformbaystreetname.Text = string.Empty;
        ddlPostalformbaystreettype.SelectedValue = "";

        txtContMr.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtCustFax.Text = string.Empty;
        txtStreetAddress.Text = string.Empty;
        txtStreetState.Text = string.Empty;
        txtStreetPostCode.Text = string.Empty;
        txtPostalAddress.Text = string.Empty;
        txtPostalState.Text = string.Empty;
        txtPostalPostCode.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        txtCustAltPhone.Text = string.Empty;
        txtCustFax.Text = string.Empty;
        txtCustNotes.Text = string.Empty;
        txtCustWebSiteLink.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtstreetaddressline.Text = string.Empty;
        txtpostaladdressline.Text = string.Empty;
        txtCountry.Text = string.Empty;
        //txtuname.Text = string.Empty;
        //txtpassword.Text = string.Empty;
        //txtcpassword.Text = string.Empty;

        ddlCustSourceID_SelectedIndexChanged(ddlCustTypeID, new EventArgs());
        ddlStreetCity.Text = "";
        ddlPostalCity.Text = "";
        //rblResCom.ClearSelection();
        rblResCom1.Checked = true;
        rblResCom2.Checked = false;
        //rblArea.ClearSelection();
        rblArea.ClearSelection();
        //divCustomer.Visible = false;
        try
        {
            ddlCustTypeID.SelectedValue = "1";
        }
        catch { }

        //ddlCustSourceID.SelectedValue = "3";

        ddlD2DEmployee.Enabled = true;
        ddlD2DEmployee.SelectedValue = "";
        txtD2DAppDate.Text = string.Empty;
        ddlAppTime.SelectedValue = "";
        chkSameasAbove.Checked = false;

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        if (Roles.IsUserInRole("Lead Manager"))
        {
            divD2DEmployee.Visible = true;
            divD2DAppDate.Visible = true;
            divD2DAppTime.Visible = true;
            ddlCustSourceID.SelectedValue = "1";
        }
        else
        {
            divD2DEmployee.Visible = false;
            divD2DAppDate.Visible = false;
            divD2DAppTime.Visible = false;
        }


        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        DataTable dtempteam = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dtempteam.Rows.Count > 0)
        {
            foreach (DataRow dr in dtempteam.Rows)
            {
                if (dr["SalesTeamID"].ToString() == "10" || dr["SalesTeamID"].ToString() == "12")
                {
                    SttblCustSource st1 = ClstblCustSource.tblCustSource_SelectByCustSourceIDCustSource("E2E");
                    ddlCustSourceID.SelectedValue = st1.CustSourceID;
                    ddlCustSourceID.Enabled = false;
                }
                else
                {
                    ddlCustSourceID.Enabled = true;
                }
            }
        }
        else
        {
            ddlCustSourceID.Enabled = true;
        }
        txtContMr.Focus();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //hdncgharid.Value = string.Empty;
        ModalPopupExtenderAddress.Hide();
        //divAddressCheck.Visible = false;
        startindex = 1;
        BindGrid(0);
        BindScript();
        //PanGrid.Attributes.Add("class", "table-responsive datahidden");
    }
    protected void ddlCustSourceID_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        PanSearch.Visible = false;
        ddlCustSourceSubID.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Select Sub Source";
        lst.Value = "0";
        ddlCustSourceSubID.Items.Add(lst);

        if (ddlCustSourceID.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlCustSourceID.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlCustSourceSubID.Visible = true;
                ddlCustSourceSubID.DataSource = dt;
                ddlCustSourceSubID.DataMember = "CustSourceSub";
                ddlCustSourceSubID.DataTextField = "CustSourceSub";
                ddlCustSourceSubID.DataValueField = "CustSourceSubID";
                ddlCustSourceSubID.DataBind();
            }
            else
            {
                ddlCustSourceSubID.Visible = false;
            }
        }
        BindScript();
    }
    protected void ddlStreetCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (ddlStreetCity.Text != string.Empty)
        {
            DataTable dtStreet = ClstblCustomers.tblPostCodes_SelectBy_PostCodeID(ddlStreetCity.Text);
            if (dtStreet.Rows.Count > 0)
            {
                txtStreetState.Text = dtStreet.Rows[0]["State"].ToString();
                txtStreetPostCode.Text = dtStreet.Rows[0]["PostCode"].ToString();
            }
        }
        BindScript();
    }
    protected void ddlPostalCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (ddlPostalCity.Text != string.Empty)
        {
            DataTable dtPostal = ClstblCustomers.tblPostCodes_SelectBy_PostCodeID(ddlPostalCity.Text);
            if (dtPostal.Rows.Count > 0)
            {
                txtPostalState.Text = dtPostal.Rows[0]["State"].ToString();
                txtPostalPostCode.Text = dtPostal.Rows[0]["PostCode"].ToString();
            }
        }
        BindScript();
    }

    public void DupButton()
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        PanSearch.Visible = false;
        PanGrid.Visible = false;
    }


    protected void txtContLast_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        txtCompany.Text = txtContFirst.Text + " " + txtContLast.Text;


        if (txtContFirst.Text.Trim() != string.Empty || txtContLast.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text + ' ' + txtContLast.Text);
            rptName.DataSource = dt;
            rptName.DataBind();

            checkExistsName();
        }
        PanSearch.Visible = false;
        PanGrid.Visible = false;
        txtCompany.Focus();
        BindScript();
    }
    public void checkExistsName()
    {
        int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
        if (exist == 1)
        {
            ModalPopupExtenderName.Show();
            txtContLast.Focus();
        }
        DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text + ' ' + txtContLast.Text);
        rptName.DataSource = dt;
        rptName.DataBind();



    }
    protected void btnDupeName_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderName.Hide();
        HidePanels();
        rptName.DataSource = null;
        rptName.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        lnkAdd.Visible = true;
        PanGrid.Visible = true;
        BindScript();
    }
    protected void btnNotDupeName_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderName.Hide();
        DupButton();
        txtContMobile.Focus();
        BindScript();
    }

    protected void txtCustPhone_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;

        if (txtCustPhone.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblCustomers_ExistSelectPhone(txtCustPhone.Text);
            rptPhone.DataSource = dt;
            rptPhone.DataBind();

            checkExistsPhone();
        }
        txtContEmail.Focus();
        //Regex Phonepattern = new Regex(@"^(07|03|08)[\d]{8}$");
        //if (Phonepattern.IsMatch(txtCustPhone.Text))
        //{
        //    //txtContEmail.Focus();
        //    txtCustPhone.Attributes.Add("class", "form-control");
        //    txtCustPhone.Attributes.Remove("errormassage");
        //    ddlCustTypeID.Focus();
        //    //txtContMobile.CssClass.Replace("errormassage", "form-control"); 
        //}
        //else
        //{
        //    txtCustPhone.Focus();
        //    txtCustPhone.Attributes.Add("class", "form-control errormassage");
        //}
        PanSearch.Visible = false;
        PanGrid.Visible = false;
        BindScript();
    }
    public void checkExistsPhone()
    {
        int exist = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
        if (exist == 1)
        {
            ModalPopupExtenderPhone.Show();
        }
        DataTable dt = ClstblCustomers.tblCustomers_ExistSelectPhone(txtCustPhone.Text);
        rptPhone.DataSource = dt;
        rptPhone.DataBind();
    }
    protected void btnDupePhone_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderPhone.Hide();
        HidePanels();
        rptPhone.DataSource = null;
        rptPhone.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        lnkAdd.Visible = true;
        BindScript();
    }
    protected void btnNotDupePhone_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderPhone.Hide();
        DupButton();
        txtCustAltPhone.Focus();
        BindScript();
    }

    protected void txtContMobile_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        txtCustAltPhone.Text = txtContMobile.Text;

        if (txtContMobile.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblContacts_ExistSelectMobile(txtContMobile.Text);
            rptMobile.DataSource = dt;
            rptMobile.DataBind();

            checkExistsMobile();
        }
        PanSearch.Visible = false;
        Regex phoneNumpattern = new Regex(@"^04[\d]{8}$");
        if (phoneNumpattern.IsMatch(txtContMobile.Text))
        {
            txtContEmail.Focus();
            txtContMobile.Attributes.Add("class", "form-control");
            txtContMobile.Attributes.Remove("errormassage");
            //txtContMobile.CssClass.Replace("errormassage", "form-control"); 
        }
        else
        {
            txtContMobile.Focus();
            txtContMobile.Attributes.Add("class", "form-control errormassage");
        }

        BindScript();
    }
    public void checkExistsMobile()
    {
        int exist = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
        if (exist == 1)
        {
            ModalPopupExtenderMobile.Show();
        }
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectMobile(txtContMobile.Text);
        rptMobile.DataSource = dt;
        rptMobile.DataBind();
    }
    protected void btnDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderMobile.Hide();
        HidePanels();
        rptName.DataSource = null;
        rptName.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        lnkAdd.Visible = true;
    }
    protected void btnNotDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderMobile.Hide();
        DupButton();
        txtContEmail.Focus();
        BindScript();
    }

    protected void txtContEmail_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        txtCustPhone.Focus();
        if (txtContEmail.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblContacts_ExistSelectEmail(txtContEmail.Text);
            rptEmail.DataSource = dt;
            rptEmail.DataBind();

            checkExistsEmail();
        }
        PanSearch.Visible = false;

        Regex Emailpattern = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*");
        if (Emailpattern.IsMatch(txtContEmail.Text))
        {
            //txtContEmail.Focus();
            txtContEmail.Attributes.Add("class", "form-control");
            txtContEmail.Attributes.Remove("errormassage");
            //txtContMobile.CssClass.Replace("errormassage", "form-control"); 
        }
        else
        {
            txtContEmail.Focus();
            txtContEmail.Attributes.Add("class", "form-control errormassage");
        }
        BindScript();
    }
    public void checkExistsEmail()
    {
        int exist = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
        if (exist == 1)
        {
            ModalPopupExtenderEmail.Show();
        }
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectEmail(txtContEmail.Text);
        rptEmail.DataSource = dt;
        rptEmail.DataBind();
    }
    protected void btnDupeEmail_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        HidePanels();
        rptEmail.DataSource = null;
        rptEmail.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        lnkAdd.Visible = true;
    }
    protected void btnNotDupeEmail_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        DupButton();
        txtCustPhone.Focus();
        BindScript();
    }

    protected void ddlStreetCity_OnTextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        string[] cityarr = ddlStreetCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            ddlStreetCity.Text = cityarr[0].Trim();
            txtStreetState.Text = cityarr[1].Trim();
            txtStreetPostCode.Text = cityarr[2].Trim();
        }
        PanSearch.Visible = false;
        BindScript();
    }
    protected void ddlPostalCity_OnTextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        string[] cityarr = ddlPostalCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            ddlPostalCity.Text = cityarr[0].Trim();
            txtPostalState.Text = cityarr[1].Trim();
            txtPostalPostCode.Text = cityarr[2].Trim();
        }
        BindScript();
        rblArea.Focus();
    }

    protected void chkViewAll_OnCheckedChanged(object sender, EventArgs e)
    {
        // PanGrid.Visible = true;
        // Response.End();
        //  {
        //DataTable dt = GetGridData();
        //dv = new DataView(dt);
        // Response.End();
        BindGrid(0);

        //   }
        // BindScript();
    }
    protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            LinkButton gvbtnUpdate = (LinkButton)e.Row.FindControl("gvbtnUpdate");
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");
            HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");
            HiddenField hndCustomerID = (HiddenField)e.Row.FindControl("hndCustomerID");

            Label lblEmail = (Label)e.Row.FindControl("lblEmail");
            //Label lblNotes = (Label)e.Row.FindControl("lblNotes");
            Label lblres = (Label)e.Row.FindControl("lblres");
            Label lblSubSource = (Label)e.Row.FindControl("lblSubSource");
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(hndCustomerID.Value);
            //lblEmail.Text = st.ContEmail;
            //lblNotes.Text = "-";
            //if (st.CustNotes != "")
            //{
            //    lblNotes.Text = st.CustNotes;
            //}

            if (st.ResCom == "1")
            {
                lblres.Text = "Res";
            }
            else
            {
                lblres.Text = "Com";
            }

            if (st.CustSourceSubID != "0" && st.CustSourceSubID != "")
            {
                lblSubSource.Text = st.CustSourceSub;
            }



            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;

            if (hndEmployeeID.Value == EmployeeID)
            {
            }
            else
            {
                gvbtnUpdate.Visible = false;
                if ((Roles.IsUserInRole("Administrator")))
                {
                    gvbtnUpdate.Visible = true;
                }
                if ((Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")))
                {
                    string SalesTeamID = stEmp.SalesTeamID;
                    string EmpType = stEmp.EmpType;
                    SttblEmployees stSalesRep = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
                    //Response.Write(stSalesRep.EmpType + "==" + EmpType + "==" + stSalesRep.SalesTeamID +"=="+ SalesTeamID.ToString()+"<br/>");
                    if (stSalesRep.EmpType == EmpType && stSalesRep.SalesTeamID == SalesTeamID.ToString())
                    {
                        gvbtnUpdate.Visible = true;
                    }
                    else
                    {
                        gvbtnUpdate.Visible = false;
                    }
                }
                else
                {
                    if ((Roles.IsUserInRole("CompanyManager")))
                    {
                        gvbtnUpdate.Visible = true;
                    }
                }
            }

            if (Roles.IsUserInRole("Administrator"))
            {
                gvbtnDelete.Visible = true;
                if (Roles.IsUserInRole("SubAdministrator"))
                {
                    gvbtnDelete.Visible = false;
                }
            }
            else
            {
                gvbtnDelete.Visible = false;
            }




            if (Roles.IsUserInRole("SalesRep"))
            {
                System.Web.UI.HtmlControls.HtmlContainerControl tdassignto = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("tdassignto");
                System.Web.UI.HtmlControls.HtmlContainerControl tdassignto1 = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("tdassignto1");
                tdassignto.Visible = false;
                tdassignto1.Visible = false;
                System.Web.UI.HtmlControls.HtmlContainerControl tdcompnum = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("tdcompnum");
                tdcompnum.Attributes.Add("colspan", "3");


                System.Web.UI.HtmlControls.HtmlContainerControl tdtype = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("tdtype");
                tdtype.Visible = false;
            }

            //if (Roles.IsUserInRole("Installation Manager"))
            //{
            //    //Response.Write(st.EmployeeID);
            //    //Response.End();
            //    if (st.EmployeeID== hndEmployeeID.Value)
            //    {
            //        gvbtnDelete.Visible = true;
            //        gvbtnUpdate.Visible = true;
            //    }
            //    else
            //    {
            //        gvbtnDelete.Visible = false;
            //        gvbtnUpdate.Visible = false;
            //    }
            //}

        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        BindScript();
        //PanGrid.Visible = false;
        if (TabContainer1.ActiveTabIndex != 1)
        {
            // PanGrid.Visible = false;
            contacts1.HideTabContainer();
        }
        if (TabContainer1.ActiveTabIndex != 2)
        {
            //  PanGrid.Visible = false;
        }

        if (TabContainer1.ActiveTabIndex == 0)
        {
            //  PanGrid.Visible = false;
            companysummary1.BindSummary();
            //contacts1.Visible = false;
            //project1.Visible = false;
            //info1.Visible = false;
            //conversation1.Visible = false;
            //companysummary1.Visible = true;
        }
        else if (TabContainer1.ActiveTabIndex == 1)
        {
            // PanGrid.Visible = false;
            //Profile.eurosolar.contactid = string.Empty;
            contacts1.BindContact();
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            // PanGrid.Visible = false;
            //Profile.eurosolar.projectid = string.Empty;
            project1.BindProjects();
        }
        else if (TabContainer1.ActiveTabIndex == 3)
        {
            // PanGrid.Visible = false;
            info1.BindInfo();
        }
        else if (TabContainer1.ActiveTabIndex == 4)
        {
            // PanGrid.Visible = false;
            conversation1.BindData();
        }
    }
    public void ClearSelection()
    {
        //Profile.eurosolar.companyid = "";
        //Profile.eurosolar.contactid = "";
        //Profile.eurosolar.projectid = "";
        //Profile.eurosolar.maintenanceid = "";

        divleft.Visible = true;
        divright.Visible = false;
        lnkclose.Visible = false;
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("Installer"))
        {
            Response.Redirect("~/admin/adminfiles/installation/installations.aspx");
        }
        else
        {
            Response.Redirect("~/admin/adminfiles/company/testcompany.aspx");
            ClearSelection();
        }
    }


    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        if (chkViewAll.Checked)
        {
            userid = "";
            Employeeid = "";
        }
        DataTable dt = new DataTable();
        string datagrid = "", Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "", data2 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID=" + Employeeid + ")) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";
        DataTable dtcount = ClstblCustomers.query_execute(Count);
        if (dtcount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                //Response.Write(SalesTeamID);
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmpType=" + EmpType1 + ")) and (C.EmployeeID in (select EmployeeID from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))))";

            }
        }

        if (Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("BookInstallation") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC"))
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
                data2 = "and  C.CustTypeID!=3 and C.CustomerID in (select CustomerID from tblProjects where ProjectStatusID!=2)";
                //dt = ClstblCustomers.tblCustomers_SelectRoleWise(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
            }
            else
            {
                //dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), custompagesize.ToString());
                Employeeid = "";
            }
        }
        else
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
                data2 = "and  C.CustTypeID!=3 and C.CustomerID in (select CustomerID from tblProjects where ProjectStatusID!=2)";
                //dt = ClstblCustomers.tblCustomers_SelectRoleWise(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
            }
            else
            {

                Employeeid = st.EmployeeID;
                //dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, Employeeid, ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString(), txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), custompagesize.ToString());
                //dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, userid, ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString(), txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), custompagesize.ToString());
            }
        }

        string empid = "";
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Lead Manager"))
        {
            empid = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmployeeID!=1 and userid in (select userid from aspnet_membership where islockedout=0)and EmployeeID in (select Employeeid from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split('" + SalesTeamID1 + "',',')))))";
        }
        else
        {
            empid = "and (C.EmployeeID='" + Employeeid + "' or '" + Employeeid + "'=0)";
        }
        //datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) and (C.EmployeeID='" + "" + "' or '" + "" + "'=0) and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "' <= C.CustEntered or '" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "' >=C.CustEntered  or '" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC";
        datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata,C.PostalPostCode from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) " + empid + "and (C.isformbayadd='" + ddladdressverification.SelectedValue + "' or '" + ddladdressverification.SelectedValue + "'='') and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "' <= C.CustEntered or '" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "' >=C.CustEntered  or '" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC";
        //Response.Write(datagrid);
        //Response.End();
        dt = ClstblCustomers.query_execute(datagrid);
        //int startindex = (custompageIndex - 1) * custompagesize + 1;
        //int endindex = (custompageIndex * custompagesize);
        //if (Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("BookInstallation") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC"))
        //{
        //    if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
        //    {
        //        dt = ClstblCustomers.tblCustomers_SelectRoleWise(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
        //    }
        //    else
        //    {
        //        dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), endindex.ToString());
        //    }
        //}
        //else
        //{
        //    if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
        //    {
        //        dt = ClstblCustomers.tblCustomers_SelectRoleWise(txtSearch.Text, "", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
        //    }
        //    else
        //    {
        //        dt = ClstblCustomers.tblCustomersGetDataBySearch(txtSearch.Text, userid, ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString(), txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, startindex.ToString(), endindex.ToString());
        //    }
        //}
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Customer" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 2, 4, 9, 0, 15, 5, 11, 12, 7, 8, 6 };
            // int[] ColList2 = { 2, 4, 6, 0, 7, 8, 5, 9, 15, 11, 12 };
            string[] arrHeader = { "Company Number", "Company Name", "Address", "Location", "PostCodes", "MobileNo", "Phone", "Email", "Type", "Source", "Assigned To" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void rptEmail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderEmail.Show();
        rptEmail.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectEmail(txtContEmail.Text);
        rptEmail.DataSource = dt;
        rptEmail.DataBind();
    }
    protected void rptPhone_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderPhone.Show();
        rptPhone.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblCustomers_ExistSelectPhone(txtCustPhone.Text);
        rptPhone.DataSource = dt;
        rptPhone.DataBind();
    }
    protected void rptMobile_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderMobile.Show();
        rptMobile.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectMobile(txtContMobile.Text);
        rptMobile.DataSource = dt;
        rptMobile.DataBind();
    }
    protected void rptName_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderName.Show();
        rptName.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text + ' ' + txtContLast.Text);
        rptName.DataSource = dt;
        rptName.DataBind();
    }
    protected void ddlD2DEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanSearch.Visible = false;
        PanAddUpdate.Visible = true;

        ListItem item1 = new ListItem();
        item1.Text = "Time";
        item1.Value = "";
        ddlAppTime.Items.Clear();
        ddlAppTime.Items.Add(item1);

        if (ddlD2DEmployee.SelectedValue != string.Empty)
        {
            DataTable dt = ClstblLTeamTime.tblLTeamTime_SelectByEmp(ddlD2DEmployee.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlAppTime.DataSource = dt;
                ddlAppTime.DataMember = "Time";
                ddlAppTime.DataTextField = "Time";
                ddlAppTime.DataValueField = "ID";
                ddlAppTime.DataBind();
            }
            else
            {
                divTeamTime.Visible = true;
            }
        }
        BindScript();
    }
    protected void ddlSearchSource_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSearchSubSource.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
        ddlSearchSubSource.Items.Add(lst);

        if (ddlSearchSource.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlSearchSource.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlSearchSubSource.DataSource = dt;
                ddlSearchSubSource.DataMember = "CustSourceSub";
                ddlSearchSubSource.DataTextField = "CustSourceSub";
                ddlSearchSubSource.DataValueField = "CustSourceSubID";
                ddlSearchSubSource.DataBind();
            }
        }
        BindScript();
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        //GridViewRow gvrow = GridView1.BottomPagerRow;
        //Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        //lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        //int[] page = new int[7];
        //page[0] = GridView1.PageIndex - 2;
        //page[1] = GridView1.PageIndex - 1;
        //page[2] = GridView1.PageIndex;
        //page[3] = GridView1.PageIndex + 1;
        //page[4] = GridView1.PageIndex + 2;
        //page[5] = GridView1.PageIndex + 3;
        //page[6] = GridView1.PageIndex + 4;
        //for (int i = 0; i < 7; i++)
        //{
        //    if (i != 3)
        //    {
        //        if (page[i] < 1 || page[i] > GridView1.PageCount)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Visible = false;
        //        }
        //        else
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Text = Convert.ToString(page[i]);
        //            lnkbtn.CommandName = "PageNo";
        //            lnkbtn.CommandArgument = lnkbtn.Text;

        //        }
        //    }
        //}
        //if (GridView1.PageIndex == 0)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
        //    lnkbtn.Visible = false;

        //}
        //if (GridView1.PageIndex == GridView1.PageCount - 1)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //    lnkbtn.Visible = false;

        //}
        //Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        //if (dv.ToTable().Rows.Count > 0)
        //{
        //    int iTotalRecords = dv.ToTable().Rows.Count;
        //    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //    if (iEndRecord > iTotalRecords)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    if (iStartsRecods == 0)
        //    {
        //        iStartsRecods = 1;
        //    }
        //    if (iEndRecord == 0)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        //}
        //else
        //{
        //    ltrPage.Text = "";
        //}
        //BindScript();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.Pager)
        //{
        //    GridViewRow gvr = e.Row;
        //    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p1");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p2");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p4");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p5");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p6");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //}

    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        //GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //custompageIndex = GridView1.PageIndex + 1;

        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        // BindGrid(0);
    }
    protected void chkformbayid_CheckedChanged(object sender, EventArgs e)
    {

        PanAddUpdate.Visible = true;
        PanSearch.Visible = false;
        if (chkformbayid.Checked == true)
        {
            txtstreetaddressline.Enabled = true;
            txtStreetAddress.Enabled = false;
            ddlStreetCity.Enabled = false;
            AutoCompletestreet.Enabled = false;
            ddlPostalCity.Enabled = false;
            txtPostalAddress.Enabled = false;
            AutoCompleteSearch.Enabled = false;
            //Custompostaladdress.Enabled = true;
            rfvpostaladdress.Visible = false;
            txtpostaladdressline.Enabled = true;
            rfvstreetaddressline.Visible = true;
            //Customstreetadd.Visible = true;
            //Custompostaladdress.Visible = true;
            txtstreetaddressline.Attributes.Add("onblur", "getParsedAddress();");
        }
        else
        {
            divsttype.Visible = true;
            divstname.Visible = true;
            divunittype.Visible = true;
            divUnitno.Visible = true;
            divStreetno.Visible = true;
            divPostalformbayUnitNo.Visible = true;
            divPostalformbayStreetNo.Visible = true;
            divpostalformbaystreettype.Visible = true;
            divPostalformbaystreetname.Visible = true;
            divPostalformbayunittype.Visible = true;

            txtstreetaddressline.Enabled = false;

            //txtStreetAddress.Enabled = true;
            ddlStreetCity.Enabled = true;
            AutoCompletestreet.Enabled = true;
            ddlPostalCity.Enabled = true;
            //txtPostalAddress.Enabled = true;
            txtpostaladdressline.Enabled = false;
            AutoCompleteSearch.Enabled = true;
            //Custompostaladdress.Enabled = false;
            rfvpostaladdress.Visible = true;
            rfvstreetaddressline.Visible = false;
            //Customstreetadd.Visible = false;
            //Custompostaladdress.Visible = false;

            txtstreetaddressline.Attributes.Add("onblur", "");
        }
        BindScript();
    }

    protected void btnUpdateAddress_Click(object sender, EventArgs e)
    {
        if (hdnupdateaddress.Value != string.Empty)
        {

            //   ModalPopupExtender1.Show();
            //  div_popup.Visible = true;
            //ModalPopupExtenderName.Hide();
            divName.Visible = false;
            PanAddUpdate.Visible = true;
            BindScript();

            //ddlUnitType.DataSource = ClstblUnitType.tblUnitType_SelectAsc();
            //ddlUnitType.DataMember = "UnitTypeName";
            //ddlUnitType.DataTextField = "UnitTypeName";
            //ddlUnitType.DataValueField = "UnitTypeCode";
            //ddlUnitType.DataBind();

            //ddlstreetType.DataSource = ClstblStreetType.tblStreetType_SelectAsc();
            //ddlstreetType.DataMember = "StreetTypeName";
            //ddlstreetType.DataTextField = "StreetTypeName";
            //ddlstreetType.DataValueField = "StreetTypeCode";
            //ddlstreetType.DataBind();

            //SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(hdnupdateaddress.Value);
            //ddlUnitType.SelectedValue = st.unit_type.ToUpper().ToString();
            //txtUnitNo.Text = st.unit_number;
            //txtstreetno.Text = st.street_number;
            //txtStreetName.Text = st.street_name;
            //ddlstreetType.SelectedValue = st.street_type.ToUpper().ToString();

            //txtCity.Text = st.StreetCity;
            //txtState.Text = st.StreetState;
            //txtpostcode.Text = st.StreetPostCode;
        }
    }
    protected void btnUpdateAddressData_Click(object sender, EventArgs e)
    {
        if (hdnupdateaddress.Value != string.Empty)
        {
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(hdnupdateaddress.Value);
            //bool succ = ClstblCustomers.tblCustomer_Update_Address(hdnupdateaddress.Value, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, st.street_suffix);


            //bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(hdnupdateaddress.Value, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text);
            //   string addressline = ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text;

            // ClstblCustomers.tblCustomer_Update_Address(hdnupdateaddress.Value, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, st.street_suffix);
            // ClstblCustomers.tblCustomer_Update_Street_line(hdnupdateaddress.Value, addressline);

            // ClstblCustomers.tblCustomer_Update_PostalAddress(hdnupdateaddress.Value, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, st.street_suffix);

            //  ClstblCustomers.tblCustomer_Update_Postal_line(hdnupdateaddress.Value, addressline);
            //   string streetaddress = txtUnitNo.Text + txtStreetName.Text + " " + ddlstreetType.SelectedValue;
            //  ClstblCustomers.tblCustomers_UpdateAddressDetails(hdnupdateaddress.Value, streetaddress, txtCity.Text, txtState.Text, txtpostcode.Text, streetaddress, txtCity.Text, txtState.Text, txtpostcode.Text, st.CustPhone);
            PanAddUpdate.Visible = true;
        }
    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "dodropdown();", true);
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);

    }


    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderAddress.Hide();
    }
    protected void ibtnCancel1_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();

    }
    protected void RepeaterImages_ItemCommand(object source, RepeaterCommandEventArgs e)
    {


        //Response.Write("yes");

        if (e.CommandName == "Delete")
        {
            string id = e.CommandArgument.ToString();
            hdndelete1.Value = id;
            ModalPopupExtenderDelete.Show();
            BindGrid(0);
        }

        //Response.Write("delete" + id);
        //Response.End();

    }
    protected void txtstreetaddressline_TextChanged(object sender, EventArgs e)
    {
        //txtstreetaddressline.Attributes.Add("onblur", "getParsedAddress();");


        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        txtstreetaddressline.Focus();
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        txtStreetAddress.Text = txtformbayUnitNo.Text + " " + ddlformbayunittype.SelectedValue + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;

        if (txtstreetaddressline.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(txtStreetAddress.Text, StreetCity, StreetState, StreetPostCode);
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);

            rptaddress.DataSource = dt;
            rptaddress.DataBind();

            //checkExistsAddress();
        }
        PanSearch.Visible = false;
        BindScript();
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "getParsedAddress();", true);
    }
    public void checkExistsAddress()
    {

        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string StreetAddress = txtStreetAddress.Text;
        int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);

        if (dt1.Rows.Count > 0)
        {
            ModalPopupExtenderAddress.Show();
        }
        DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);
        rptaddress.DataSource = dt;
        rptaddress.DataBind();
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);

            //DataTable dt = new DataTable();
            //dt = GetGridData();
            //DataView dv = new DataView(dt);

            //dv.Sort = ViewState["Column"] + " " + ViewState["Sortorder"];
            //if (e.CommandName == ViewState["Column"].ToString())
            //{
            //    if (ViewState["Sortorder"].ToString() == "ASC")
            //        ViewState["Sortorder"] = "DESC";
            //    else
            //        ViewState["Sortorder"] = "ASC";
            //}
            //else
            //{
            //    ViewState["Column"] = e.CommandName;
            //    ViewState["Sortorder"] = "ASC";
            //}
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        lastpageindex = Convert.ToInt32(hdncountdata.Value) / Convert.ToInt32(ddlSelectRecords.SelectedValue);



        if (Convert.ToInt32(hdncountdata.Value) % Convert.ToInt32(ddlSelectRecords.SelectedValue) > 0)
        {
            lastpageindex = lastpageindex + 1;
        }
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion


    //protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtStreetAddress.Text = txtformbayUnitNo.Text + " " + ddlformbayunittype.SelectedValue + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text+" "+ddlformbaystreettype.SelectedValue;
    //}
    //protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtStreetAddress.Text = txtformbayUnitNo.Text + " " + ddlformbayunittype.SelectedValue + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
    //}
    //protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtStreetAddress.Text = txtformbayUnitNo.Text + " " + ddlformbayunittype.SelectedValue + " " + txtformbayStreetNo.Text + " " + ddlformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
    //}
    //protected void ddlformbaystreetname_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtStreetAddress.Text = txtformbayUnitNo.Text + " " + ddlformbayunittype.SelectedValue + " " + txtformbayStreetNo.Text + " " + ddlformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
    //}
    //protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtStreetAddress.Text = txtformbayUnitNo.Text + " " + ddlformbayunittype.SelectedValue + " " + txtformbayStreetNo.Text + " " + ddlformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
    //}
    //protected void txtPostalformbayUnitNo_TextChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtPostalAddress.Text = txtPostalformbayUnitNo.Text + " " + ddlPostalformbayunittype.SelectedValue + " " + txtPostalformbayStreetNo.Text + " " + txtPostalformbaystreetname.Text + " " + ddlPostalformbaystreettype.SelectedValue;
    //}
    //protected void ddlPostalformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtPostalAddress.Text = txtPostalformbayUnitNo.Text + " " + ddlPostalformbayunittype.SelectedValue + " " + txtPostalformbayStreetNo.Text + " " + txtPostalformbaystreetname.Text + " " + ddlPostalformbaystreettype.SelectedValue;
    //}
    //protected void txtPostalformbayStreetNo_TextChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtPostalAddress.Text = txtPostalformbayUnitNo.Text + " " + ddlPostalformbayunittype.SelectedValue + " " + txtPostalformbayStreetNo.Text + " " + txtPostalformbaystreetname.Text + " " + ddlPostalformbaystreettype.SelectedValue;
    //}
    //protected void ddlPostalformbaystreetname_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtPostalAddress.Text = txtPostalformbayUnitNo.Text + " " + ddlPostalformbayunittype.SelectedValue + " " + txtPostalformbayStreetNo.Text + " " + txtPostalformbaystreetname.Text + " " + ddlPostalformbaystreettype.SelectedValue;
    //}
    //protected void ddlPostalformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    txtPostalAddress.Text = txtPostalformbayUnitNo.Text + " " + ddlPostalformbayunittype.SelectedValue + " " + txtPostalformbayStreetNo.Text + " " + txtPostalformbaystreetname.Text + " " + ddlPostalformbaystreettype.SelectedValue;
    //}

    [WebMethod]
    public static bool Getstreetname(string streetname)
    {
        string data = "select StreetName from tbl_formbaystreetname where StreetName='" + streetname + "'";
        DataTable dt = ClstblCustomers.query_execute(data);
        string desc = string.Empty;
        if (dt.Rows.Count > 0)
        {
            desc = dt.Rows[0]["StreetName"].ToString();
            return true;
        }
        else
        {
            return false;
        }
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchType.SelectedValue = "";
        ddlSearchSource.SelectedValue = "";
        ddlSearchRec.SelectedValue = "";
        ddlSearchArea.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        ddladdressverification.SelectedValue = "";
        txtSearch.Text = string.Empty;
        txtSearchCompanyNo.Text = string.Empty;
        txtSearchPostCode.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchStreet.Text = string.Empty;
        ddlSearchSubSource.SelectedValue = "";
        BindScript();
        BindDropDown();
        BindGrid(0);

    }
    protected void chkSameasAbove_CheckedChanged1(object sender, EventArgs e)
    {

        //Response.Write(chkSameasAbove.Checked);
        //Response.End();
        PanSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        if (chkSameasAbove.Checked == true)
        {
            txtPostalAddress.Text = txtStreetAddress.Text;
            txtpostaladdressline.Text = txtstreetaddressline.Text;
            ddlPostalCity.Text = ddlStreetCity.Text;
            txtPostalState.Text = txtStreetState.Text;
            txtPostalPostCode.Text = txtStreetPostCode.Text;

            txtPostalformbaystreetname.Text = txtformbaystreetname.Text;
            txtPostalformbayStreetNo.Text = txtformbayStreetNo.Text;
            txtPostalformbayUnitNo.Text = txtformbayUnitNo.Text;
            ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
            ddlPostalformbayunittype.SelectedValue = ddlformbayunittype.SelectedValue;
            hndstreetname.Value = txtformbaystreetname.Text;
            hndstreetno1.Value = txtformbayStreetNo.Text;
            hndunitno1.Value = txtformbayUnitNo.Text;
            hndstreettype1.Value = ddlformbaystreettype.SelectedValue;
            hndunittype1.Value = ddlformbayunittype.SelectedValue;
            //hndstreetname.Value = hndstreetname1.Value;
            //hndstreetno1.Value = hndstreetno.Value;
            //hndunitno1.Value = hndunitno.Value;
            //hndstreettype1.Value = hndstreettype.Value;
            //hndunittype1.Value = hndunittype.Value;
            hndstreetsuffix1.Value = hndstreetsuffix.Value;
        }
        else
        {
            txtPostalAddress.Text = string.Empty;
            ddlPostalCity.Text = string.Empty;
            txtPostalState.Text = string.Empty;
            txtPostalPostCode.Text = string.Empty;
            txtpostaladdressline.Text = string.Empty;

            txtPostalformbaystreetname.Text = string.Empty;
            txtPostalformbayStreetNo.Text = string.Empty;
            txtPostalformbayUnitNo.Text = string.Empty;
            ddlPostalformbaystreettype.SelectedValue = "";
            ddlPostalformbayunittype.SelectedValue = "";

            hndstreetname1.Value = "";
            hndstreetno1.Value = "";
            hndstreetsuffix1.Value = "";
            hndunitno1.Value = "";
            hndstreettype1.Value = "";
            hndunittype1.Value = "";
        }
        BindScript();
        txtPostalformbayUnitNo.Focus();
    }

    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        //GridViewRow gvrow = GridView1.BottomPagerRow;
        //Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

        //lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        //int[] page = new int[7];
        //page[0] = GridView1.PageIndex - 2;
        //page[1] = GridView1.PageIndex - 1;
        //page[2] = GridView1.PageIndex;
        //page[3] = GridView1.PageIndex + 1;
        //page[4] = GridView1.PageIndex + 2;
        //page[5] = GridView1.PageIndex + 3;
        //page[6] = GridView1.PageIndex + 4;

        //for (int i = 0; i < 7; i++)
        //{
        //    if (i != 3)
        //    {
        //        if (page[i] < 1 || page[i] > GridView1.PageCount)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Visible = false;
        //        }
        //        else
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Text = Convert.ToString(page[i]);
        //            lnkbtn.CommandName = "PageNo";
        //            lnkbtn.CommandArgument = lnkbtn.Text;
        //        }
        //    }
        //}
        //if (GridView1.PageIndex == 0)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
        //    lnkbtn.Visible = false;
        //}
        //if (GridView1.PageIndex == GridView1.PageCount - 1)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //    lnkbtn.Visible = false;
        //}
        //Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        //if (dv.ToTable().Rows.Count > 0)
        //{
        //    //Response.Write(dv.ToTable().Rows.Count);
        //    //Response.End();
        //    int iTotalRecords = dv.ToTable().Rows.Count;
        //    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //    if (iEndRecord > iTotalRecords)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    if (iStartsRecods == 0)
        //    {
        //        iStartsRecods = 1;
        //    }
        //    if (iEndRecord == 0)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        //}
        //else
        //{
        //    ltrPage.Text = "";
        //}
        //BindScript();
    }
    //protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    //{

    //    //if (e.Row.RowType == DataControlRowType.Pager)
    //    //{
    //    //    GridViewRow gvr = e.Row;
    //    //    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
    //    //    lb.Command += new CommandEventHandler(lb_Command);
    //    //    lb = (LinkButton)gvr.Cells[0].FindControl("p1");
    //    //    lb.Command += new CommandEventHandler(lb_Command);
    //    //    lb = (LinkButton)gvr.Cells[0].FindControl("p2");
    //    //    lb.Command += new CommandEventHandler(lb_Command);
    //    //    lb = (LinkButton)gvr.Cells[0].FindControl("p4");
    //    //    lb.Command += new CommandEventHandler(lb_Command);
    //    //    lb = (LinkButton)gvr.Cells[0].FindControl("p5");
    //    //    lb.Command += new CommandEventHandler(lb_Command);
    //    //    lb = (LinkButton)gvr.Cells[0].FindControl("p6");
    //    //    lb.Command += new CommandEventHandler(lb_Command);
    //    //}
    //}
    protected void ddlStreetCity_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        streetaddress();
        string[] cityarr = ddlStreetCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            ddlStreetCity.Text = cityarr[0].Trim();
            txtStreetState.Text = cityarr[1].Trim();
            txtStreetPostCode.Text = cityarr[2].Trim();
        }
        chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalCity.Text = ddlStreetCity.Text;
        //    txtPostalState.Text = txtStreetState.Text;
        //    txtPostalPostCode.Text = txtStreetPostCode.Text;
        //}

        //checkExistsAddress();
        PanSearch.Visible = false;
        BindScript();
        chkSameasAbove.Focus();

    }
    // dropDownList.Focus();

    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {

        PanAddUpdate.Visible = true;
        streetaddress();
        ddlformbayunittype.Focus();
        chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    txtPostalformbayUnitNo.Text = txtformbayUnitNo.Text;
        //}
        //checkExistsAddress();

        BindScript();
    }
    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        txtformbayStreetNo.Focus();
        chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbayunittype.SelectedValue = ddlformbayunittype.SelectedValue;
        //}
        //checkExistsAddress();

        BindScript();
    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        txtformbaystreetname.Focus();
        chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    txtPostalformbayStreetNo.Text = txtformbayStreetNo.Text;
        //}
        BindScript();

    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);

        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    txtPostalformbaystreetname.Text = txtformbaystreetname.Text;
        //}
        BindScript();

        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);
        ddlformbaystreettype.Focus();
        // Response.End();

    }
    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}
        ddlStreetCity.Focus();
    }

    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtStreetAddress.Text = address;
        BindScript();
    }

    protected void txtPostalformbayUnitNo_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        funpostaladdress();
        BindScript();
        ddlPostalformbayunittype.Focus();
    }
    protected void ddlPostalformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        funpostaladdress();
        BindScript();
        txtPostalformbayStreetNo.Focus();
    }
    protected void txtPostalformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        funpostaladdress();
        BindScript();
        txtPostalformbaystreetname.Focus();
    }
    //protected void txtPostalformbaystreetname_TextChanged(object sender, EventArgs e)
    //{

    //}
    protected void ddlPostalformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        funpostaladdress();
        BindScript();
        ddlPostalCity.Focus();
    }

    public void funpostaladdress()
    {
        postaladdress = ddlPostalformbayunittype.SelectedValue + " " + txtPostalformbayUnitNo.Text + " " + txtPostalformbayStreetNo.Text + " " + txtPostalformbaystreetname.Text + " " + ddlPostalformbaystreettype.SelectedValue;
        // address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtPostalAddress.Text = postaladdress;
        BindScript();
    }


    protected void txtPostalformbaystreetname_TextChanged1(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>postaladdress();</script>");
        //Response.End();
        PanAddUpdate.Visible = true;
        funpostaladdress();
        BindScript();
        ddlPostalformbaystreettype.Focus();
    }

    //protected void rblResCom1_CheckedChanged(object sender, EventArgs e)
    //{
    //    Response.End();
    //    PanAddUpdate.Visible = true;
    //    source.Visible = true;
    //}
    //protected void rblResCom1_CheckedChanged(object sender, EventArgs e)
    //{
    //    Response.End();
    //    PanAddUpdate.Visible = true;
    //       source.Visible = true;
    //}
    protected void ddlCustSourceID_SelectedIndexChanged1(object sender, EventArgs e)
    {
        ddlSearchSubSource.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
        ddlSearchSubSource.Items.Add(lst);

        if (ddlSearchSource.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlSearchSource.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlSearchSubSource.DataSource = dt;
                ddlSearchSubSource.DataMember = "CustSourceSub";
                ddlSearchSubSource.DataTextField = "CustSourceSub";
                ddlSearchSubSource.DataValueField = "CustSourceSubID";
                ddlSearchSubSource.DataBind();
            }
        }
    }
    protected void ddlSearchSource_SelectedIndexChanged1(object sender, EventArgs e)
    {
        ddlSearchSubSource.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
        ddlSearchSubSource.Items.Add(lst);

        if (ddlSearchSource.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlSearchSource.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlSearchSubSource.DataSource = dt;
                ddlSearchSubSource.DataMember = "CustSourceSub";
                ddlSearchSubSource.DataTextField = "CustSourceSub";
                ddlSearchSubSource.DataValueField = "CustSourceSubID";
                ddlSearchSubSource.DataBind();
            }
        }
        BindScript();
    }
    protected void lblprint_Click(object sender, EventArgs e)
    {

    }
    //protected void txtContMobile_Leave(object sender, EventArgs e)
    //{
    //}
    //protected void txtContMobile_LostFocus(object sender, System.EventArgs e)
    //{
    //}
    protected void btnDupeAddress_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        HidePanels();
        rptaddress.DataSource = null;
        rptaddress.DataBind();
        txtformbayUnitNo.Text = string.Empty;
        ddlformbayunittype.Text = string.Empty;
        txtformbayStreetNo.Text = string.Empty;
        ddlformbaystreettype.Text = string.Empty;
        txtformbaystreetname.Text = string.Empty;
        ddlStreetCity.Text = string.Empty;
        txtStreetState.Text = string.Empty;
        txtStreetPostCode.Text = string.Empty;
        txtStreetAddress.Text = string.Empty;
        lnkAdd.Visible = true;
    }
    protected void btnNotDupeAddress_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        DupButton();
        txtformbayUnitNo.Focus();
        ddlformbayunittype.Focus();
        txtformbayStreetNo.Focus();
        ddlformbaystreettype.Focus();
        txtformbaystreetname.Focus();
        ddlStreetCity.Focus();
        BindScript();
    }
    protected void txtContFirst_TextChanged(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        txtCompany.Text = txtContFirst.Text.Trim() + " " + txtContLast.Text.Trim();
        txtContLast.Focus();
        DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text.Trim() + ' ' + txtContLast.Text.Trim());
        rptName.DataSource = dt;
        rptName.DataBind();
        checkExistsName();
        BindScript();
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {

        string id = string.Empty;
        bool sucess1 = false;
        bool sucess2 = false;
        if (hdndelete.Value.ToString() != string.Empty)
        {
            id = hdndelete.Value.ToString();

            DataTable dt = ClstblContacts.tblContacts_SelectByCustId(id);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SiteConfiguration.deleteimage(dt.Rows[i]["AL"].ToString(), "AL");
                    SiteConfiguration.DeletePDFFile("AL", dt.Rows[i]["AL"].ToString());
                }
            }


            DataTable dtP = ClstblProjects.tblProjects_SelectByUCustomerID(id);
            if (dtP.Rows.Count > 0)
            {
                for (int i = 0; i < dtP.Rows.Count; i++)
                {
                    SiteConfiguration.deleteimage(dtP.Rows[i]["SQ"].ToString(), "SQ");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["MP"].ToString(), "MP");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["EB"].ToString(), "EB");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["PD"].ToString(), "PD");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["ST"].ToString(), "ST");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["CE"].ToString(), "CE");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["PR"].ToString(), "PR");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["InvDoc"].ToString(), "InvDoc");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["InvDocDoor"].ToString(), "InvDocDoor");

                    SiteConfiguration.DeletePDFFile("SQ", dtP.Rows[i]["SQ"].ToString());
                    SiteConfiguration.DeletePDFFile("MP", dtP.Rows[i]["MP"].ToString());
                    SiteConfiguration.DeletePDFFile("EB", dtP.Rows[i]["EB"].ToString());
                    SiteConfiguration.DeletePDFFile("PD", dtP.Rows[i]["PD"].ToString());
                    SiteConfiguration.DeletePDFFile("ST", dtP.Rows[i]["ST"].ToString());
                    SiteConfiguration.DeletePDFFile("CE", dtP.Rows[i]["CE"].ToString());
                    SiteConfiguration.DeletePDFFile("PR", dtP.Rows[i]["PR"].ToString());
                    SiteConfiguration.DeletePDFFile("InvDoc", dtP.Rows[i]["InvDoc"].ToString());
                    SiteConfiguration.DeletePDFFile("InvDocDoor", dtP.Rows[i]["InvDocDoor"].ToString());
                }
            }

            DataTable dtIC = ClstblInvoiceCommon.tblInvoiceCommon_SelectByCustomerID(id);
            if (dtIC.Rows.Count > 0)
            {
                for (int i = 0; i < dtIC.Rows.Count; i++)
                {
                    SiteConfiguration.deleteimage(dtIC.Rows[i]["InvDoc"].ToString(), "commoninvdoc");
                    SiteConfiguration.DeletePDFFile("commoninvdoc", dtIC.Rows[i]["InvDoc"].ToString());
                }
            }

            //Response.Write(id);
            //Response.End();  

            sucess1 = ClstblCustomers.tblCustomers_Delete(id);
        }
        if (hdndelete1.Value.ToString() != string.Empty)
        {
            id = hdndelete1.Value.ToString();
            DataTable dt = ClstblCustomers.tbl_CustImage_SelectByCustImageID(id);

            sucess2 = ClstblCustomers.tbl_CustImage_DeleteByCustImageID(id);
            if (sucess2)
            {
                if (dt.Rows.Count > 0)
                {
                    SiteConfiguration.deleteimage(dt.Rows[0]["Imagename"].ToString(), foldername);
                }
            }
        }


        //Response.Write(sucess1);
        //Response.End();
        //--- do not chage this code start------
        if (sucess1 || sucess2)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "modal_danger", "$('#modal_danger').modal('hide');", true);
        GridView1.EditIndex = -1;
        BindGrid(0);

    }
    //protected override PageStatePersister PageStatePersister
    //{
    //    get
    //    {
    //        return new FileViewStateOptimizer(Page);
    //    }
    //}
}