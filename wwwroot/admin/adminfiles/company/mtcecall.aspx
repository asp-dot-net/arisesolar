<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="mtcecall.aspx.cs" Inherits="admin_adminfiles_company_mtcecall" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/InvoicePayments.ascx" TagName="InvoicePayments" TagPrefix="uc1" %>
<%@ Register Src="~/admin/adminfiles/invoice/invcommon.ascx" TagName="InvCommon" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();

        }

    </script>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }

        //$(function () {
        //    $("[id*=GridView1] td").bind("click", function () {
        //        var row = $(this).parent();
        //        $("[id*=GridView1] tr").each(function () {
        //            if ($(this)[0] != row[0]) {
        //                $("td", this).removeClass("selected_row");
        //            }
        //        });
        //        $("td", row).each(function () {
        //            if (!$(this).hasClass("selected_row")) {
        //                $(this).addClass("selected_row");
        //            } else {
        //                $(this).removeClass("selected_row");
        //            }
        //        });
        //    });
        //});
    </script>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(".myval").select2({
                width: "50%",
                formatResult: function (state) {
                    if (!state.id) return state.text;
                    if ($(state.element).data('active') == "0") {
                        return state.text + "<i class='fa fa-dot-circle-o'></i>";
                    } else {
                        return state.text;
                    }
                },
                formatSelection: function (state) {
                    if ($(state.element).data('active') == "0") {
                        return state.text + "<i class='fa fa-dot-circle-o'></i>";
                    } else {
                        return state.text;
                    }
                }
            });
        }

    </script>


    <script type="text/javascript">
        //$(document).ready(function () {
        //            doMyAction();
        //        });
        function doMyAction() {
            HighlightControlToValidate();

            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });

            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
        }




        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#b94a48");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }

        $("[id*=grdFollowUp] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=grdFollowUp] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
        //gridviewScroll();

    </script>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        function pageLoaded() {
            $(".myvalmtcecall").select2({
                //placeholder: "select",
                allowclear: true
            });

            //$('.i-checks').iCheck({
            //    checkboxClass: 'icheckbox_square-green',
            //    radioClass: 'iradio_square-green'
            //});
        }
    </script>





    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Maintenance
        
                </h5>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter Responsive-search printorder">
                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div class="inlineblock">
                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtProjectNumber" runat="server" placeholder="Project No" CssClass="form-control m-b"></asp:TextBox>

                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>

                                                        <div class="input-group col-sm-2 martop5">
                                                            <asp:TextBox ID="txtCustomer" runat="server" placeholder="Customer" CssClass="form-control m-b"></asp:TextBox>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtmobile" runat="server" placeholder="Mobile No" CssClass="form-control m-b"></asp:TextBox>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtstreet" runat="server" placeholder="StreetName" CssClass="form-control m-b"></asp:TextBox>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtCity" runat="server" placeholder="City" CssClass="form-control m-b"></asp:TextBox>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" id="divCustomer" runat="server">
                                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                <asp:ListItem Value="">State</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5" style="width: 109px">
                                                            <asp:TextBox ID="txtSearchPostCode" placeholder="Post Code" runat="server" CssClass="form-control m-b"></asp:TextBox>

                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5" id="div2" runat="server">
                                                            <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Installer</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>

                                                    <div class="inlineblock">
                                                        <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Complete Date</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5" id="div3" runat="server">
                                                            <asp:DropDownList ID="ddlcalldate" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="0">Call Date</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5" id="div4" runat="server">
                                                            <asp:DropDownList ID="ddlpropDate" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="0">Proposed Date</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" id="div5" runat="server">
                                                            <asp:DropDownList ID="ddlProjectMtceReasonID" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval ">
                                                                <asp:ListItem Value="">Select Call Reason</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5" id="div7" runat="server">
                                                            <asp:DropDownList ID="datasearch" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="0">Select Date</asp:ListItem>
                                                                <asp:ListItem Value="1">Call Date</asp:ListItem>
                                                                <asp:ListItem Value="2">Proposed Date</asp:ListItem>
                                                                <asp:ListItem Value="3">Complete Date</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%--     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5" id="div8" runat="server">
                                                            <asp:DropDownList ID="ddlemployee" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Employee</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                         <div class="input-group col-sm-2 martop5">
                                                            <asp:TextBox ID="txtemail" runat="server" placeholder="Email" CssClass="form-control m-b"></asp:TextBox>
                                                        </div>
                                                        <div class="input-group  martop5">

                                                            <label for="<%=chkWarranty.ClientID %>" class="btn btn-magenta btn-xs">
                                                                <asp:CheckBox ID="chkWarranty" runat="server" />
                                                                <span class="text">&nbsp;Warrenty</span>
                                                            </label>

                                                        </div>
                                                        <div class="input-group martop5">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group martop5">
                                                            <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div class="datashowbox">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="dataTables_length showdata ">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="pull-right martop7 marleft8">
                                            <div class="checkbox-info ">
                                                <!-- <a href="#" class="btn btn-darkorange btn-xs Excel"><i class="fa fa-check-square-o"></i> View All</a>-->

                                                <span valign="top" id="tdAll1" class="paddtop3td alignchkbox btnviewallorange" runat="server" align="right">
                                                    <label for="<%=chkViewAll.ClientID %>" class="btn btn-darkorange btn-xs">
                                                        <asp:CheckBox ID="chkViewAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkViewAll_OnCheckedChanged" />
                                                        <span class="text">&nbsp;View All</span>&nbsp;
                                                           
                                                    </label>
                                                    <asp:LinkButton ID="lbtnExport" runat="server" CausesValidation="false" class="btn btn-success btn-xs1 Excel" data-original-title="Excel Export" data-placement="left" data-toggle="tooltip" OnClick="lbtnExport_Click" title=""><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </span>

                                            </div>
                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%><%-- </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=" animate-panel padtopzero">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="finalgrid">
                                    <div class=" padleftzero padrightzero">
                                        <div class="">
                                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="">
                                                    <div id="PanGrid" runat="server">
                                                        <div class="table-responsive xscroll">
                                                            <asp:GridView ID="GridView1" DataKeyNames="ProjectMaintenanceID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                                OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1"
                                                                OnRowDataBound="GridView1_RowDataBound3" OnRowCommand="GridView1_RowCommand" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                                <Columns>

                                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                                        <ItemTemplate>

                                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectMaintenanceID") %>','tr<%# Eval("ProjectMaintenanceID") %>');">
                                                                                <img id='imgdiv<%# Eval("ProjectMaintenanceID") %>' src="../../../images/icon_plus.png" />
                                                                            </a>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                       <asp:TemplateField HeaderText="Agent" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="OpenDate"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                          
                                                                             <asp:Label Width="100px" ID="Label2" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Employee")%>'
                                                                                runat="server"><%#Eval("Employee")%></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Project No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Projectnumber"
                                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px">
                                                                        <ItemTemplate>

                                                                            <asp:Label Width="100px" ID="Label3" runat="server">
                                                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"
                                                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("Projectnumber")%></asp:HyperLink></asp:Label>
                                                                            <div style="text-align: right; display: initial">
                                                                                <asp:LinkButton ID="lbtnFollowUpNote" CommandName="addfollowupnote" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-azure btn-xs"
                                                                                    CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add FollowUp">
                                                              <i class="fa fa-calendar"></i> Followup</asp:LinkButton>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--  <asp:TemplateField HeaderText="Call Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text"
                                                                        ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="80px" ID="Label1" runat="server"><%#Eval("OpenDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Call Desc" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="250px" ID="Label2" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustomerInput")%>'><%#Eval("CustomerInput")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="OrgInstaller"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="150px" ID="lblProjectStatus" runat="server"><%#Eval("ProjectStatus")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                               
                                                                    
                                                                   
                                                                    <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="MeterElectrician"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="150px" ID="lblCustomer" runat="server"> <%#Eval("Customer")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%-- <asp:TemplateField HeaderText="Install Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="lblInstallAddress">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="150px" ID="lblInstallAddress" runat="server"><%#Eval("InstallAddress")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                     <asp:TemplateField HeaderText="Fix Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="CompletionDate">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="80px" ID="lblFollowUp" runat="server"><%#Eval("PropResolutionDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                   <asp:TemplateField HeaderText="MTC" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Installer"
                                                                        ItemStyle-HorizontalAlign="left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="80px" ID="lblMtceCost" runat="server"><%#Eval("MtceCost")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                <%--    <asp:TemplateField HeaderText="Orig Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="CompletionDate"
                                                                        ItemStyle-HorizontalAlign="left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="90px" ID="Label7" runat="server"><%#Eval("CompletionDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <%--<asp:TemplateField HeaderText="WorkDone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="WorkDone"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="300px" ID="lblWorkDone" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("WorkDone")%>'
                                                                                runat="server"><%#Eval("WorkDone")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                  <%--  <asp:TemplateField HeaderText="PostCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="InstallPostCode"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="60px" ID="Label5" runat="server"><%#Eval("InstallPostCode")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <%--<asp:TemplateField HeaderText="Proposed Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="PropResolutionDate"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="150px" ID="lblInstallCity" runat="server"><%#Eval("PropResolutionDate")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <%--<asp:TemplateField HeaderText="Tech Assign" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Installer"
                                                                        ItemStyle-HorizontalAlign="left">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="80px" ID="Installer" runat="server"><%#Eval("Installer")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <%--  <asp:TemplateField HeaderText="Discount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="MtceDiscount"
                                                                        ItemStyle-HorizontalAlign="Right">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="80px" ID="lblMtceDiscount" runat="server"><%#Eval("MtceDiscount","{0:0.00}")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Cust Price" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="MtceBalance"
                                                                        ItemStyle-HorizontalAlign="Right">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="80px" ID="Label6" runat="server"><%#Eval("MtceBalance","{0:0.00}")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ServiceCost" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ServiceCost"
                                                                        ItemStyle-HorizontalAlign="Right">
                                                                        <ItemTemplate>
                                                                            <asp:Label Width="80px" ID="lblServiceCost" runat="server"><%#Eval("ServiceCost","{0:0.00}")%></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID") %>' />
                                                                            <asp:LinkButton ID="lbtnInvAdd" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ProjectID")%>' CommandName="invoiceadd"
                                                                                data-toggle="tooltip" data-placement="top" data-original-title="Add Invoice" CssClass="btn btn-sky btn-xs" Visible="false"><i class="fa fa-plus-circle"></i> Add Invoice</asp:LinkButton>

                                                                            <asp:LinkButton ID="lbtnInvUpdate" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ProjectID")%>' CommandName="invoiceupdate"
                                                                                CssClass="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit Invoice"><i class="fa fa-edit"></i>Edit</asp:LinkButton>
                                                                            <asp:LinkButton ID="gvbtnDetail" runat="server" CommandName="detail" CausesValidation="false" CssClass="btn btn-danger btn-xs"
                                                                                CommandArgument='<%#Eval("ProjectMaintenanceID")+","+ Eval("ProjectID")%>' data-toggle="tooltip" data-placement="top" data-original-title="Detail">
                                                    <i class="fa fa-link"></i>Detail</asp:LinkButton>
                                                                            <asp:LinkButton ID="gvbtnaddpayment" runat="server" CommandName="AddPayment" CausesValidation="false" CssClass="btn btn-darkorange btn-xs"
                                                                                CommandArgument='<%#Eval("ProjectID")%>' data-toggle="tooltip" data-placement="top" data-original-title="Add Payment" Visible="false">
                                                    <i class="fa fa-plus-circle"></i>Add Payment</asp:LinkButton>


                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                                        <ItemTemplate>
                                                                            <tr id='tr<%# Eval("ProjectMaintenanceID") %>' style="display: none;" class="dataTable GridviewScrollItem">
                                                                                <td colspan="98%" class="details">
                                                                                    <div id='div<%# Eval("ProjectMaintenanceID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                                        <table style="width: 80px;" class="table table-bordered table-hover subtablecolor">
                                                                                           
                                                                                            <tr class="GridviewScrollItem">
                                                                                                 <td style="width: 80px;"><b>Project Number</b></td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label6" runat="server">
                                                                                                <%#Eval("ProjectNumber").ToString() == "" ? "-" : Eval("ProjectNumber")%></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <b>Employee</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label12" runat="server">
                                                                                                <%#Eval("Employee").ToString() == "" ? "-" : Eval("Employee")%></asp:Label>
                                                                                                </td>
                                                                                               <td style="width: 80px;">
                                                                                                    <b>Project Status</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label13" runat="server">
                                                                                                <%#Eval("ProjectStatus").ToString() == "" ? "-" : Eval("ProjectStatus")%></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <b>Customer Name</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label14" runat="server">
                                                                                                <%#Eval("Customer").ToString() == "" ? "-" : Eval("Customer")%></asp:Label>
                                                                                                </td>
                                                                                                
                                                                                            </tr>
                                                                                             <tr>
                                                                                                 <td style="width: 80px;"><b>FollowUp Date</b></td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label15" runat="server">
                                                                                                <%#Eval("FollowUp").ToString() == "" ? "-" : Eval("FollowUp","{0:dd MMM yyyy}")%></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 80px;"><b>Call Date</b></td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="lbldate" runat="server">
                                                                                                <%#Eval("OpenDate").ToString() == "" ? "-" : Eval("OpenDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <b>Call Description</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label8" runat="server">
                                                                                                <%#Eval("CustomerInput").ToString() == "" ? "-" : Eval("CustomerInput")%></asp:Label>
                                                                                                </td>
                                                                                                  <td style="width: 80px;">
                                                                                                    <b>Email</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label17" runat="server">
                                                                                                <%#Eval("ContEmail").ToString() == "" ? "-" : Eval("ContEmail")%></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                 <td style="width: 80px;">
                                                                                                    <b>Mobile</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label16" runat="server">
                                                                                                <%#Eval("ContMobile").ToString() == "" ? "-" : Eval("ContMobile")%></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <b>Contact</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label9" runat="server" ddata-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Contact")%>' CssClass="tooltipwidth"><%#Eval("Contact").ToString() == "" ? "-" : Eval("Contact")%></asp:Label>
                                                                                                </td>

                                                                                                <td style="width: 80px;">
                                                                                                    <b>Tech Assign</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Installer")%>' CssClass="tooltipwidth"><%#Eval("Installer").ToString() == "" ? "-" : Eval("Installer")%></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="width: 80px;">
                                                                                                    <b>Prop Date</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label2" runat="server">
                                                                                                <%#Eval("PropResolutionDate").ToString() == "" ? "-" : Eval("PropResolutionDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                                                </td>--%>
                                                                                                <td style="width: 80px;">
                                                                                                    <b>Payment status</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label25" runat="server">
                                                                                                <%#Eval("PaymentStatus")%></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                  
                                                                                                  <td style="width: 80px;">
                                                                                                    <b>Address</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label19" runat="server">
                                                                                                <%#Eval("InstallAddress").ToString() == "" ? "-" : Eval("InstallAddress")%></asp:Label>
                                                                                                </td>
                                                                                                 <td style="width: 80px;">
                                                                                                    <b>Installation Booking Date</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                     <asp:Label ID="Label20" runat="server">
                                                                                                <%#Eval("InstallBookingDate").ToString() == "" ? "-" : Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <b>Price</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                     <asp:Label ID="Label21" runat="server">
                                                                                                 <%#Eval("TotalQuotePrice").ToString() == "" ? "-" : Eval("TotalQuotePrice")%></asp:Label>
                                                                                                </td>
                                                                                                  
                                                                                                 <td style="width: 80px;">
                                                                                                    <b>System Details</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                    <asp:Label ID="Label18" runat="server">
                                                                                                <%#Eval("SystemDetails").ToString() == "" ? "-" : Eval("SystemDetails")%></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="display:none">
                                                                                                <td style="width: 80px;">
                                                                                                    <b>Payment Status</b>
                                                                                                </td>
                                                                                                <td style="width: 80px;">
                                                                                                     <asp:Label ID="Label24" runat="server">
                                                                                                 <%#Eval("InvoiceStatus").ToString() == "" ? "-" : Eval("InvoiceStatus")%></asp:Label>
                                                                                                </td>
                                                                                               
                                                                                            </tr>
                                                                                        </table>

                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <AlternatingRowStyle />

                                                                <PagerTemplate>
                                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                    <div class="pagination">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                    </div>
                                                                </PagerTemplate>
                                                                <PagerStyle CssClass="paginationGrid" />
                                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                            </asp:GridView>
                                                        </div>
                                                        <div class="paginationnew1" runat="server" id="divnopage">
                                                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <uc1:InvoicePayments ID="InvoicePayments1" runat="server" Visible="false" />

            <asp:HiddenField ID="hndProInvID" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divFollowUp"
                OkControlID="btnOK" TargetControlID="btnNULL">
            </cc1:ModalPopupExtender>
            <div runat="server" id="divFollowUp" style="display: none;" class="modal_popup">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                    Close
                                </button>
                            </div>
                            <h4 class="modal-title" id="myModalLabel">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                Add FollowUp</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline row">
                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <asp:Label ID="Label3" runat="server" class="col-sm-12 control-label">
                                                									<strong>Contact</strong></asp:Label>
                                            <div class="col-sm-12">

                                                <asp:DropDownList ID="ddlContact" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Eurosolar</asp:ListItem>
                                                    <asp:ListItem Value="2">Door to Door</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="reqerror"
                                                    ControlToValidate="ddlContact" Display="Dynamic" ValidationGroup="followup"></asp:RequiredFieldValidator>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <asp:Label ID="Label11" runat="server" class="col-sm-12 control-label">
                                                <strong>Select</strong></asp:Label>
                                            <div class="col-sm-12">

                                                <asp:DropDownList ID="ddlManager" runat="server" OnSelectedIndexChanged="ddlManager_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true"
                                                    CssClass="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Follow Up</asp:ListItem>
                                                    <asp:ListItem Value="2">Manager</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddlManager" Display="Dynamic" ValidationGroup="followup"></asp:RequiredFieldValidator>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group col-md-4" id="divNextDate" runat="server" visible="false">
                                        <div class="row">
                                            <asp:Label ID="Label23" runat="server" class="col-md-12 control-label">
                                                <strong>Next Followup Date</strong></asp:Label>
                                            <div>
                                                <div class=" col-sm-12">
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">

                                                        </asp:TextBox>


                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNextFollowupDate"
                                                            ValidationGroup="followup" ErrorMessage="" CssClass="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <div>
                                                <asp:Label ID="Label22" runat="server" class="col-sm-12 control-label">
                                                <strong>Description</strong></asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtDescription" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                        ValidationGroup="followup" ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group  col-md-12  marbtm15 center-text">
                                        <!--<span class="name">
                                                                            <label class="control-label">&nbsp;</label>
                                                                        </span>-->
                                        <span>
                                            <asp:Button ID="ibtnAdd" runat="server" Text="Add" OnClick="ibtnAdd_Click"
                                                ValidationGroup="followup" CssClass="btn btn-primary btnaddicon redreq addwhiteicon center-text" />
                                            <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                                CssClass="btn" Text=" OK " />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="seachfinal">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tablescrolldiv xscroll">
                                            <div id="divgrdFollowUp" runat="server" class="table-responsive xscroll">
                                                <asp:GridView ID="grdFollowUp" DataKeyNames="CustInfoID" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                                                    AutoGenerateColumns="false" CssClass="tooltip-demo text-center GridviewScrollItem table table-striped table-bordered table-hover">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("Contact") %>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Next FollowupDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("Description") %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                    <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                    <EmptyDataRowStyle Font-Bold="True" />
                                                    <RowStyle CssClass="GridviewScrollItem" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndCustomerID" runat="server" />



            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divFollowUp"
                OkControlID="btnOK" TargetControlID="btnNULL">
            </cc1:ModalPopupExtender>
            <div runat="server" id="div6" style="display: none;" class="modal_popup">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="Button1" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                    Close
                                </button>
                            </div>
                            <h4 class="modal-title" id="H1" style="text-align: left; padding-top: 4px">
                                <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                                Add Invoice</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline row">



                                    <div class="form-group  col-md-12  marbtm15 center-text">
                                        <span>
                                            <asp:Button ID="Button2" runat="server" Text="Add" OnClick="ibtnAdd_Click"
                                                ValidationGroup="followup" CssClass="btn btn-primary btnaddicon redreq addwhiteicon center-text" />
                                            <asp:Button ID="Button3" Style="display: none; visible: false;" runat="server"
                                                CssClass="btn" Text=" OK " />
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button4" Style="display: none;" runat="server" />

            <uc2:InvCommon ID="invcommon1" runat="server" Visible="false" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>


