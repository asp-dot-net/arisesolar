<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="quickform.aspx.cs" Inherits="admin_adminfiles_company_quickform" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                }
            </script>


            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage QuickForm</h5>

            </div>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="">
                        <div class="animate-panelmessesgarea padbtmzero">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                        <div class="searchfinal">
                            <div class="widget-body shadownone brdrgray">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="dataTables_filter">
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                            <div class="contacttoparea">
                                                <div class="form-inline">
                                                    <div class="padd10all">


                                                        <div class="form-group">

                                                            <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtProjectNumber"
                                                                WatermarkText="Project Num" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>
                                                        <div class="form-group" style="width: 180px">
                                                            <asp:DropDownList ID="ddlInstaller" Width="200px" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                                                <asp:ListItem Value="">Installer</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group date datetimepicker1">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <asp:TextBox ID="txtstartdate" runat="server" class="form-control" placeholder="Start Date"> </asp:TextBox>
                                                        </div>

                                                        <div class="input-group date datetimepicker1">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <asp:TextBox ID="txtenddate" runat="server" class="form-control" placeholder="End Date"> </asp:TextBox>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                                CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="datashowbox">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="row">
                                            <div class="dataTables_length showdata col-sm-6">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div id="tdExport" runat="server" class="pull-right">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                        CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="finalgrid">
                            <div class="table-responsive printArea">
                                <div id="PanGrid" runat="server">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowDeleting="GridView1_RowDeleting" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>

                                                <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <center>   
                                                    <label for='<%# ((GridViewRow)Container).FindControl("chkheader").ClientID %>' runat="server" id="lblchk123">
                                                        <asp:CheckBox ID="chkheader" runat="server" AutoPostBack="true" OnCheckedChanged="chkheader_CheckedChanged" />
                                                        <span class="text">&nbsp;</span>
                                                    </label>                                                 
                                                </center>
                                                    </HeaderTemplate>

                                                    <ItemTemplate>
                                                        <label for='<%# ((GridViewRow)Container).FindControl("chk1").ClientID %>' runat="server" id="lblchk4543">
                                                            <asp:CheckBox ID="chk1" runat="server" />
                                                            <span class="text">&nbsp;</span>
                                                        </label>



                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-Width="20px" HeaderText="Project#" SortExpression="ProjectNumber">
                                                    <ItemTemplate>
                                                        <%# Eval("ProjectNumber") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Installer" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="installername">
                                                    <ItemTemplate>
                                                        <%# Eval("installername") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Panel Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%# Eval("panelname") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Inverter Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%# Eval("invertername1") %><br />
                                                        <%# Eval("invertername2") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="150px"  HeaderText="Install Address" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%# Eval("InstallAddress") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="150px" HeaderText="Install BookingDate" ItemStyle-VerticalAlign="Top" ItemStyle-CssClass="tdspecialclass spicalheghtnew"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="InstallBookingDate">
                                                    <ItemTemplate>
                                                        <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
              <asp:AsyncPostBackTrigger ControlID="GridView1" />
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="btnSearch" />
           
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
