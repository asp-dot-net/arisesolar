﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class admin_adminfiles_company_customerimage : System.Web.UI.Page
{
    static DataView dv;
    string foldername = "customerimage";
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();

        if (!IsPostBack)
        {
            PanGrid.Visible = false;

            //ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            //ddlSelectRecords.DataBind();

            if (Request.QueryString["id"] != string.Empty)
            {
                BindGrid(0);
            }
        }
    }

    protected DataTable GetGridData()
    {
        string CustomerId = "";

        if (Request.QueryString["id"].ToString() != string.Empty)
        {
            CustomerId = Request.QueryString["id"].ToString();
        }


        DataTable dt = ClstblCustomers.tbl_CustImage_SelectByCustomerID(CustomerId);
        if (dt.Rows.Count == 0)
        { }

        //Response.Write(dt.Rows.Count);
        //Response.End();
        // HidePanels();
        //if (!string.IsNullOrEmpty(hdncgharid.Value))
        //{
        //    dt = ClstblCustType.tblCustTypeGetDataByAlpha(hdncgharid.Value);
        //}
        //if (!string.IsNullOrEmpty(txtSearch.Text) || !string.IsNullOrEmpty(ddlActive.SelectedValue))
        //{
        //    dt = ClstblCustType.tblCustTypeGetDataBySearch(txtSearch.Text, ddlActive.SelectedValue);
        //}
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        // PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;

        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
           // divnopage.Visible = false;
        }
        else
        {

            RepeaterImages.DataSource = dt;
            RepeaterImages.DataBind();
            PanNoRecord.Visible = false;
            //if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            //{
            //    if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
            //    {
            //        //========label Hide
            //        divnopage.Visible = false;
            //    }
            //    else
            //    {
            //        divnopage.Visible = true;
            //        int iTotalRecords = dv.ToTable().Rows.Count;
            //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            //        if (iEndRecord > iTotalRecords)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        if (iStartsRecods == 0)
            //        {
            //            iStartsRecods = 1;
            //        }
            //        if (iEndRecord == 0)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            //    }
            //}
            //else
            //{
            //    if (ddlSelectRecords.SelectedValue == "All")
            //    {
            //        divnopage.Visible = true;
            //        ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
            //    }
            //}
        }
    }


    //protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
    //    bool sucess1 = ClstblCustomers.tbl_CustImage_DeleteByCustImageID(id);
    //    //--- do not chage this code start------
    //    if (sucess1)
    //    {
    //        SetDelete();
    //        //InitAdd();
    //    }
    //    else
    //    {
    //        SetError();
    //    }
    //    //--- do not chage this code end------
    //    //--- do not chage this code start------
    //    GridView1.EditIndex = -1;
    //    BindGrid(0);
    //    BindGrid(1);
    //    //--- do not chage this code end------
    //}

    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GridView1.PageIndex = e.NewPageIndex;
    //    BindGrid(0);
    //}

    //protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    //{
    //    GridView1.EditIndex = e.NewEditIndex;
    //    BindGrid(0);
    //}

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        //{
        //    GridView1.AllowPaging = false;
        //    BindGrid(0);
        //}
        //else
        //{
        //    GridView1.AllowPaging = true;
        //    GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
        //    BindGrid(0);
        //}
    }
    //protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    //{
    //    GridView1.EditIndex = -1;
    //    BindGrid(0);
    //}
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        //PanSearch.Visible = true;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        // ModalPopupExtender2.Show();
        Reset();
        //PanSearch.Visible = false;
        //lnkAdd.Visible = false;
        //lnkBack.Visible = true;
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/company/company.aspx");
        //BindGrid(0);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        // ModalPopupExtender2.Show();
        InitAdd();
    }
    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAddUpdate.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        //PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();


        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        ////PanAddUpdate.Visible = false;
        ////lnkAdd.Visible = true;
        ////lnkBack.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;

        // ModalPopupExtender2.Show();

       // PanGridSearch.Visible = false;
    }
    public void InitUpdate()
    {
        lnkBack.Visible = true;
        // lnkAdd.Visible = false;
        //PanAddUpdate.Visible = true;
        // HidePanels();
        // ModalPopupExtender2.Show();

        //lnkAdd.Visible = false;
        //lnkBack.Visible = true;
        PanGridSearch.Visible = false;
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        // lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
        //PanAddUpdate.Visible = false;
        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        //PanSearch.Visible = true;
    }
    public void Reset()
    {
        // PanAddUpdate.Visible = true;
    }
    protected void A_Click(object sender, EventArgs e)
    {
        LinkButton button = (LinkButton)sender;
        string alpha = button.CommandArgument;
        //hdncgharid.Value = alpha;
        BindGrid(0);
    }
    protected void all_Click(object sender, EventArgs e)
    {
        //hdncgharid.Value = string.Empty;
        BindGrid(0);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
        BindGrid(0);
    }
    //protected void GridView1_DataBound(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridView1.BottomPagerRow;
    //    Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
    //    lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
    //    int[] page = new int[7];
    //    page[0] = GridView1.PageIndex - 2;
    //    page[1] = GridView1.PageIndex - 1;
    //    page[2] = GridView1.PageIndex;
    //    page[3] = GridView1.PageIndex + 1;
    //    page[4] = GridView1.PageIndex + 2;
    //    page[5] = GridView1.PageIndex + 3;
    //    page[6] = GridView1.PageIndex + 4;
    //    for (int i = 0; i < 7; i++)
    //    {
    //        if (i != 3)
    //        {
    //            if (page[i] < 1 || page[i] > GridView1.PageCount)
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Visible = false;
    //            }
    //            else
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Text = Convert.ToString(page[i]);
    //                lnkbtn.CommandName = "PageNo";
    //                lnkbtn.CommandArgument = lnkbtn.Text;

    //            }
    //        }
    //    }
    //    if (GridView1.PageIndex == 0)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
    //        lnkbtn.Visible = false;

    //    }
    //    if (GridView1.PageIndex == GridView1.PageCount - 1)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
    //        lnkbtn.Visible = false;

    //    }
    //    Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
    //    if (dv.ToTable().Rows.Count > 0)
    //    {
    //        int iTotalRecords = dv.ToTable().Rows.Count;
    //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //        if (iEndRecord > iTotalRecords)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        if (iStartsRecods == 0)
    //        {
    //            iStartsRecods = 1;
    //        }
    //        if (iEndRecord == 0)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //    }
    //    else
    //    {
    //        ltrPage.Text = "";
    //    }
    //}
    //void lb_Command(object sender, CommandEventArgs e)
    //{
    //    GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    //}
    //protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.Pager)
    //    {
    //        GridViewRow gvr = e.Row;
    //        LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p1");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p2");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p4");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p5");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p6");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //    }
    //}
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BindGrid(0);
    }

    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }


    //protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.Pager)
    //    {
    //        GridViewRow gvr = e.Row;
    //        LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p1");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p2");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p4");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p5");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p6");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //    }
    //}
    //protected void GridView1_DataBound1(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridView1.BottomPagerRow;
    //    Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
    //    lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
    //    int[] page = new int[7];
    //    page[0] = GridView1.PageIndex - 2;
    //    page[1] = GridView1.PageIndex - 1;
    //    page[2] = GridView1.PageIndex;
    //    page[3] = GridView1.PageIndex + 1;
    //    page[4] = GridView1.PageIndex + 2;
    //    page[5] = GridView1.PageIndex + 3;
    //    page[6] = GridView1.PageIndex + 4;
    //    for (int i = 0; i < 7; i++)
    //    {
    //        if (i != 3)
    //        {
    //            if (page[i] < 1 || page[i] > GridView1.PageCount)
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Visible = false;
    //            }
    //            else
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Text = Convert.ToString(page[i]);
    //                lnkbtn.CommandName = "PageNo";
    //                lnkbtn.CommandArgument = lnkbtn.Text;

    //            }
    //        }
    //    }
    //    if (GridView1.PageIndex == 0)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
    //        lnkbtn.Visible = false;

    //    }
    //    if (GridView1.PageIndex == GridView1.PageCount - 1)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
    //        lnkbtn.Visible = false;

    //    }
    //    Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
    //    if (dv.ToTable().Rows.Count > 0)
    //    {
    //        int iTotalRecords = dv.ToTable().Rows.Count;
    //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //        if (iEndRecord > iTotalRecords)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        if (iStartsRecods == 0)
    //        {
    //            iStartsRecods = 1;
    //        }
    //        if (iEndRecord == 0)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //    }
    //    else
    //    {
    //        ltrPage.Text = "";
    //    }
    //}
    protected void RepeaterImages_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            string id = e.CommandArgument.ToString();
            hdndelete.Value = id;
            ModalPopupExtenderDelete.Show();
            BindGrid(0);
            //Response.Write("yes");
        }
      


        //  string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //bool sucess1 = ClstblCustType.tblCustType_Delete(id);
        //--- do not chage this code start------
        //if (sucess1)
        //{
        //    SetDelete();
        //    //InitAdd();
        //}
        //else
        //{
        //    SetError();
        //}
        ////--- do not chage this code end------
        ////--- do not chage this code start------
        //GridView1.EditIndex = -1;
        //BindGrid(0);
        //BindGrid(1);
    }
   
    
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //Response.Write("yes");
        //Response.End();
        if (fu1.HasFile)
        {
            string fileName = fu1.FileName;
            System.Drawing.Image imgSize = System.Drawing.Image.FromStream(fu1.PostedFile.InputStream);
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                string dirpath = Request.PhysicalApplicationPath + "/userfiles/customerimage/";

                int suc = ClstblCustomers.tbl_CustImage_Insert(Request.QueryString["id"].ToString(), fileName);
                fileName = suc + "Image.jpg";
                fu1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles\\customerimage\\" + fileName);
                ClstblCustomers.tbl_CustImage_UpdateImage(suc.ToString(), fileName);

                if (Convert.ToString(suc) != "")
                {
                    SetAdd();
                }
                else
                {
                    SetError();
                }
            }
        }
        BindGrid(0);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
          string id = hdndelete.Value.ToString();
      
            //Response.Write("delete" + id);
            //Response.End();
            DataTable dt = ClstblCustomers.tbl_CustImage_SelectByCustImageID(id);

            bool sucess1 = ClstblCustomers.tbl_CustImage_DeleteByCustImageID(id);
            if (sucess1)
            {
                SetAdd();
                if (dt.Rows.Count > 0)
                {
                    SiteConfiguration.deleteimage(dt.Rows[0]["Imagename"].ToString(), foldername);
                }
            }
            else
            {
                SetError();
            }
      
        BindGrid(0);
    }
}