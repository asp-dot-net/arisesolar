<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="leadtracker.aspx.cs" Inherits="admin_adminfiles_company_leadtracker" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ui-autocomplete-loading {
            background: white url("../../../images/indicator.gif") right center no-repeat;
        }
    </style>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);
            //alert(divname + "--" + trname + "--" + 'img' + divname);
            // alert(div+"--"+img+"--"+tr);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>
    <script type="text/javascript">
        //$(document).ready(function () {
        //            doMyAction();
        //        });

        function doMyAction1() {

            HighlightControlToValidate();
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {

                callMultiCheckbox();
            });
        }
        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }

        function doMyAction() {
            HighlightControlToValidate();
            $('#<%=ibtnUpdateStatus.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnCancelLead.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnEditDetail.ClientID %>').click(function () {
                formValidate();
                var drpdiv = document.getElementsByClassName("drpValidate");
                var flag = 0;

                for (i = 0; i < drpdiv.length; i++) {
                    if ($(drpdiv[i]).find(".myval").val() == "") {
                        $(drpdiv[i]).addClass("errormassage");
                        flag = flag + 1;
                    }
                }
                if (flag > 0) {
                    return false;
                }

            });


            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            //gridviewScroll();
        }
        function ChkFun(source, args) {
            validateAddress();
            var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

            if (elem == '1') {
                args.IsValid = true;
            }
            else {

                args.IsValid = false;
            }
        }
        function CompanyAddress() {
            $("#<%=txtStreetAddressline.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "address.aspx",
                        dataType: "jsonp",
                        data: {
                            s: 'auto',
                            term: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    //                    log(ui.item ?
                    //					"Selected: " + ui.item.label :
                    //					"Nothing selected, input was " + this.value);
                },
                open: function () {
                    //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    // $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
        }
        function getParsedAddress() {
            // alert("hii");
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'address',
                    term: $("#<%=txtStreetAddressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data != null) {
                        document.getElementById("<%= txtStreetAddress.ClientID %>").value = data.StreetLine;
                        document.getElementById("<%= txtStreetPostCode.ClientID %>").value = data.Postcode;
                        document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                        document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                        document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                        document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                        document.getElementById("<%= ddlStreetCity.ClientID %>").value = data.Suburb;
                        document.getElementById("<%= txtStreetState.ClientID %>").value = data.State;
                        document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                        document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                    }
                    validateAddress();
                }
            });
        }

        function validateAddress() {
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'validate',
                    term: $("#<%=txtStreetAddressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data == true) {
                        document.getElementById("validaddressid").style.display = "block";
                        document.getElementById("invalidaddressid").style.display = "none";

                        document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                    }
                    else {
                        document.getElementById("validaddressid").style.display = "none";
                        document.getElementById("invalidaddressid").style.display = "block";
                        document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                    }
                }
            });
        }




        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#b94a48");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnAssidnLead" />
            <asp:PostBackTrigger ControlID="lbtnExport" />


        </Triggers>
    </asp:UpdatePanel>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Lead Tracker</h5>
        <div id="fdfs" class="pull-right" runat="server">
            <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
            </ol>
        </div>
    </div>



    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.

        function pageLoaded() {

        }

        function address() {
            //=============Street 
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

            $.ajax({
                type: "POST",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                        $("#chkaddval").val("1");
                    }
                    else {
                        $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                        $("#chkaddval").val("0");
                    }
                }
            });
            // select2 - container-active

        }




        function validatestreetAddress(source, args) {
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

            $.ajax({
                type: "POST",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //alert(data.d);
                    if (data.d == true) {
                        $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                        $("#chkaddval").val("1");
                    }
                    else {
                        $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                        $("#chkaddval").val("0");
                    }
                }
            });

            var mydataval = $("#chkaddval").val();
            if (mydataval == "1") {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }

    </script>
    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="animate-panel" style="padding-bottom: 0px!important;">
                <div class="messesgarea">
                </div>
            </div>

            <div class="searchfinal" onkeypress="DefaultEnter(event);">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="paneldefault" runat="server" DefaultButton="btnSearch">
                                <div class="row">

                                    <div class="inlineblock ">
                                        <div class="col-sm-12">
                                            <div class="input-group col-sm-1 martop5" id="divCustomer" runat="server">
                                                <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                    <asp:ListItem Value="">Type</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                <asp:DropDownList ID="ddlSearchStatus" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Status</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-1 martop5" id="div2" runat="server">
                                                <asp:DropDownList ID="ddlSearchSource" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSearchSource_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Source</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-1 martop5" id="div3" runat="server">
                                                <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group col-sm-1 martop5" id="tdTeam" runat="server">
                                                <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Team</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                         
                                     <div class="input-group col-sm-1 martop5" id="empcategory" runat="server"  visible="false"> 
                                        <asp:DropDownList ID="ddlEmpCategory" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select Emp Category"></asp:ListItem>
                                        </asp:DropDownList>

                                     
                                    </div>
                                  <div class="input-group col-sm-1 martop5" id="tdsalerep" runat="server">
                                                <asp:DropDownList ID="ddlSalesRepSearch" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 martop5">
                                                <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtContactSearch"
                                                    WatermarkText="Contact" />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                </cc1:AutoCompleteExtender>
                                            </div>
                                             <div class="input-group col-sm-1 martop5">
                                                <asp:TextBox ID="txtMpbile" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtMpbile"
                                                    WatermarkText="Mobile" />
                                            </div>
                                             <div class="input-group col-sm-1 martop5">
                                                <asp:TextBox ID="txtprojectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtprojectNumber"
                                                    WatermarkText="Project Number" />
                                            </div>
                                            <div class="input-group col-sm-1 martop5">
                                                <asp:TextBox ID="txtEmil" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtEmil"
                                                    WatermarkText="Email" />
                                            </div>

                                            <div class="input-group col-sm-1 martop5">
                                                <asp:TextBox ID="txtSearchStreet" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchStreet"
                                                    WatermarkText="Street" />
                                            </div>


                                            <div class="input-group col-sm-1 martop5">
                                                <asp:TextBox ID="txtsuburb" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtsuburb"
                                                    WatermarkText="Suburb" />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtsuburb" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                </cc1:AutoCompleteExtender>
                                            </div>

                                            <div class="input-group col-sm-1 martop5" id="div6" runat="server">
                                                <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                            <div class="input-group col-sm-1 martop5">
                                                <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchPostCode"
                                                    WatermarkText="Post Code" />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                </cc1:AutoCompleteExtender>
                                            </div>

                                            <div class="input-group col-sm-1 martop5"  id="div7" runat="server">
                                                <asp:DropDownList ID="ddlSelectDate" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">CustEntered</asp:ListItem>
                                                    <asp:ListItem Value="2">NextFollowUp</asp:ListItem>
                                                    <asp:ListItem Value="3">NewDate-Lead</asp:ListItem>
                                                    <asp:ListItem Value="4">NewDate-Project</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                            </div>
                                            <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                            </div>

                                            <div class="form-group spical multiselect martop5" style="width: 142px">
                                                <dl class="dropdown">
                                                    <dt>
                                                        <a href="#">
                                                            <span class="hida" id="spanselect">Select</span>
                                                            <p class="multiSel"></p>
                                                        </a>
                                                    </dt>
                                                    <dd id="ddproject" runat="server">
                                                        <div class="mutliSelect" id="mutliSelect">
                                                            <ul>
                                                                <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <li>
                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                            <%--  <span class="checkbox-info checkbox">--%>
                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                            <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                <span></span>
                                                                            </label>
                                                                            <%-- </span>--%>
                                                                            <label class="chkval">
                                                                                <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                            </label>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </div>

                                            <div class="input-group col-sm-2 martop5" style="width: 70px;" id="div4" runat="server">
                                                <asp:DropDownList ID="ddlRejectOrNot" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Reject</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group martop5">
                                                <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                            </div>
                                            <div class="input-group martop5">
                                                <asp:Button ID="btnSendMail" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="SendMail" OnClick="btnSendMail_Click" />
                                            </div>
                                            <div class="input-group martop5">
                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </asp:Panel>
                        </div>
                    </div>

                    <div class="datashowbox martop5">
                        <div class="row">
                            <div class="dataTables_length showdata col-sm-6">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="padtopzero">
                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div id="tdExport" class="pull-right btnexelicon" runat="server">
                                    <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                        CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                    <%-- </ol>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                }

                function pageLoaded() {
                    $('.loading-container').addClass('loading-inactive');
                    //  CompanyAddress(); roshni
                    // gridviewScroll();
                    //alert($(".search-select").attr("class"));
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });


                    $("[data-toggle=tooltip]").tooltip();
                    //gridviewScroll();
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });

                    $("[id*=grdFollowUp] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=grdFollowUp] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                }
            </script>
            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divFollowUp"
                OkControlID="btnOK" TargetControlID="btnNULL">
            </cc1:ModalPopupExtender>
            <div runat="server" id="divFollowUp" style="display: none;" class="modal_popup">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                    Close
                                </button>
                            </div>

                            <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup" />

                            <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                <b>Add FollowUp</b></h4>
                        </div>
                        <div class="modal-body paddnone" style="max-height: 500px;">
                            <div class="panel-body">
                                <div class="formainline row">
                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <asp:Label ID="Label3" runat="server" class="col-sm-12 control-label">
                                                									<strong>Contact</strong></asp:Label>
                                            <div class="col-sm-12">

                                                <asp:DropDownList ID="ddlContact" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Eurosolar</asp:ListItem>
                                                    <asp:ListItem Value="2">Door to Door</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="reqerror"
                                                    ControlToValidate="ddlContact" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <asp:Label ID="Label11" runat="server" class="col-sm-12 control-label">
                                                <strong>Select</strong></asp:Label>
                                            <div class="col-sm-12">

                                                <asp:DropDownList ID="ddlManager" runat="server" OnSelectedIndexChanged="ddlManager_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true"
                                                    CssClass="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Follow Up</asp:ListItem>
                                                    <asp:ListItem Value="2">Manager</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddlManager" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4" id="divNextDate" runat="server" visible="false">
                                        <div class="row">
                                            <asp:Label ID="Label23" runat="server" class="col-md-12 control-label">
                                                <strong>Next Followup Date</strong></asp:Label>
                                            <div>
                                                <div class=" col-sm-12">
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNextFollowupDate"
                                                            ValidationGroup="info" ErrorMessage="" CssClass="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-8" runat="server" id="divDescription">
                                        <div class="row">
                                            <div>
                                                <asp:Label ID="Label22" runat="server" class="col-sm-12 control-label">
                                                <strong>Description</strong></asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtDescription" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                        ValidationGroup="info" ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <asp:Label ID="Label19" runat="server" class="col-sm-12 control-label">
                                                <strong>Reject</strong></asp:Label>
                                            <div class="col-sm-12">

                                                <asp:DropDownList ID="ddlreject" runat="server" AppendDataBoundItems="true"
                                                    CssClass="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddlreject" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-group  col-md-12  marbtm15 center-text">
                                        <!--<span class="name">
                                                                            <label class="control-label">&nbsp;</label>
                                                                        </span>-->
                                        <span>
                                            <asp:Button ID="ibtnAdd" runat="server" Text="Add" OnClick="ibtnAdd_Click"
                                                ValidationGroup="info" CssClass="btn btn-primary btnaddicon redreq addwhiteicon center-text" />
                                            <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                                CssClass="btn" Text=" OK " />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="seachfinal">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tablescrolldiv xscroll" style="overflow: auto; margin-bottom: 10px;">
                                            <%--<div id="divgrdFollowUp" runat="server" class="table-responsive xscroll">--%>
                                            <div id="divgrdFollowUp" runat="server" class="content padtopzero marbtm50 finalgrid">
                                                <asp:GridView ID="grdFollowUp" DataKeyNames="CustInfoID" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                                                    AutoGenerateColumns="false" CssClass="tooltip-demo text-center GridviewScrollItem table table-striped table-bordered table-hover">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("Contact") %>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Next Followup Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("Description") %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Reject" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("Reject").ToString()=="1"?"Yes":Eval("Reject").ToString()=="0"?"No":"" %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                    <%-- <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />--%>
                                                    <EmptyDataRowStyle Font-Bold="True" />
                                                    <%--<RowStyle CssClass="GridviewScrollItem" />--%>
                                                </asp:GridView>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndCustomerID" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtenderStatus" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="divUpdateStatus"
                OkControlID="btnOKStatus" TargetControlID="btnNULLStatus">
            </cc1:ModalPopupExtender>
            <div id="divUpdateStatus" runat="server" style="display: none;" class="modal_popup">

                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton5" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                            Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H1">
                                <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                Project Cancel Reason</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">

                                    <div class="form-group marbtm5 row" id="trCancel" runat="server">
                                        <asp:Label ID="Label13" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Reason:</strong></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlProjectCancelID" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                ValidationGroup="status" ControlToValidate="ddlProjectCancelID" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group row">
                                        <asp:Label ID="Label15" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Decsription:</strong></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtprojectcanceldesc" TextMode="MultiLine" Columns="4" Rows="4" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                            <br />

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                ValidationGroup="status" ControlToValidate="txtprojectcanceldesc" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group marginleft">
                                        <asp:Label ID="Label16" runat="server" class="col-sm-4 control-label">
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:Button ID="ibtnUpdateStatus" runat="server" Text="Update" OnClick="ibtnUpdateStatus_Onclick"
                                                CssClass="btn btn-primary savewhiteicon" ValidationGroup="status" />
                                            <asp:Button ID="btnOKStatus" Style="display: none; visible: false;" runat="server"
                                                CssClass="btn" Text=" OK " />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndStatusCustomerID" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupCancelLead" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="btnCancelLead" DropShadow="false" PopupControlID="divCancelLead"
                OkControlID="btnOKLead" TargetControlID="btnNULLLead">
            </cc1:ModalPopupExtender>
            <div id="divCancelLead" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="btnCancelLead" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                            Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H2" style="padding-top: 4px">
                                <asp:Label ID="Label14" runat="server" Text=""></asp:Label>
                                <b>Lead Cancel Reason</h4>
                            </b>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">

                                    <div class="form-group marbtm5 row">
                                        <asp:Label ID="Label17" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Reason:</strong></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlContLeadCancelReason" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                ValidationGroup="cancellead" ControlToValidate="ddlContLeadCancelReason" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group row">
                                        <asp:Label ID="Label20" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Decsription:</strong></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtcanceldesc" TextMode="MultiLine" Columns="4" Rows="4" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                ValidationGroup="cancellead" ControlToValidate="txtcanceldesc" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group marginleft">
                                        <asp:Label ID="Label28" runat="server" class="col-sm-4 control-label">
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:Button ID="ibtnCancelLead" runat="server" Text="Update" OnClick="ibtnCancelLead_Click"
                                                CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="cancellead" />
                                            <asp:Button ID="btnOKLead" Style="display: none; visible: false;" runat="server"
                                                CssClass="btn" Text=" OK " />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULLLead" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndLeadCustID" runat="server" />

            <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                <ProgressTemplate>
                    <%--   <div class="loading-container">
                                               <div class="loader"></div>
                                                       </div>--%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="updateprogress1"
                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup" />



            <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLEdit">
            </cc1:ModalPopupExtender>

            <div class="modal_popup" id="divEdit" runat="server" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="btnCancelEdit" CausesValidation="false" OnClick="btnCancelEdit_Click" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                                                    Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H3" style="padding-top: 4px"><b>Update Customer Detail</b></h4>
                        </div>
                        <div class="modal-body paddnone" style="overflow-y: scroll; height: 500px;">
                            <div class="panel-body">
                                <div class="formainline formnew">

                                    <div class="alert alert-info" id="lbleror" runat="server" visible="false">
                                        <i class="icon-info-sign"></i><strong>&nbsp;Record with this address aleardy Exist</strong>
                                    </div>
                                    <div class="alert alert-info" id="lbleror1" runat="server" visible="false">
                                        <i class="icon-info-sign"></i><strong>&nbsp;Record with this Email or Mobile aleardy Exist</strong>
                                    </div>

                                    <div class="clear"></div>
                                    <div class="form-group spical">
                                        <span class="col-md-12">
                                            <label>
                                                Name:
                                            </label>
                                        </span>
                                        <span>
                                            <div class="onelindiv">
                                                <span class="mrdiv col-md-4">
                                                    <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                        placeholder="Salutation"></asp:TextBox>
                                                </span>
                                                <span class="fistname col-md-4">
                                                    <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                        placeholder="First Name"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                        ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                </span>
                                                <span class="lastname col-md-4">
                                                    <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                        placeholder="Last Name"></asp:TextBox>
                                                </span>
                                            </div>
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                    <div id="Div5" class="form-group spicaldivin" runat="server" visible="false">
                                        <span class="name col-md-12">
                                            <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                        </span><span class="col-md-12">
                                            <asp:HiddenField ID="hndaddress" runat="server" />
                                            <asp:TextBox ID="txtStreetAddressline" runat="server" MaxLength="50" CssClass="form-control" onblur="getParsedAddress();"></asp:TextBox>
                                            <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company"
                                                ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="comperror"
                                                ControlToValidate="txtStreetAddressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>
                                        </span>
                                        <asp:HiddenField ID="hndstreetno" runat="server" />
                                        <asp:HiddenField ID="hndstreetname" runat="server" />
                                        <asp:HiddenField ID="hndstreettype" runat="server" />
                                        <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                        <asp:HiddenField ID="hndunittype" runat="server" />
                                        <asp:HiddenField ID="hndunitno" runat="server" />
                                        <div id="validaddressid" style="display: none">
                                            <img src="../../../images/check.png" alt="check">Address is valid.
                                        </div>
                                        <div id="invalidaddressid" style="display: none">
                                            <img src="../../../images/x.png" alt="cross">
                                            Address is invalid.
                                        </div>

                                        <div class="clear"></div>
                                    </div>
                                    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                        <ContentTemplate>--%>


                                    <div class="form-group spicaldivin">
                                        <span class="name col-md-12">
                                            <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                        </span><span class="col-md-12">
                                            <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                ControlToValidate="txtStreetAddress" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>
                                        <div class="clear"></div>
                                    </div>



                                    <div class="form-group spicaldivin">
                                        <span class="col-md-6">
                                            <label class="control-label">Unit No<span class="symbol required"></span> </label>
                                            <asp:TextBox ID="txtformbayUnitNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayUnitNo_TextChanged"></asp:TextBox>
                                        </span>
                                        <span class="col-md-6">
                                            <label class="control-label">Unit Type<span class="symbol required"></span> </label>

                                            <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnTextChanged="ddlformbayunittype_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Unit Type</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>

                                        </span>
                                        <div class="clear"></div>
                                        
                                    </div>
                                    <div class="form-group spicaldivin">
                                        <span class="form-group col-md-4">
                                            <label class="control-label" style="width: 150px;">Street No</label>
                                            <asp:TextBox ID="txtformbayStreetNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>
                                        <span class="col-md-4">
                                            <div class="autocompletedropdown">
                                                <label class="control-label" style="width: 150px;">Street Name<span class="symbol required"></span> </label>
                                                <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>

                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                    ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                    ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass="comperror"
                                                    ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                <div id="Divvalidstreetname" style="display: none">
                                                    <img src="../../../images/check.png" alt="check">Address is valid.
                                                </div>
                                                <div id="DivInvalidstreetname" style="display: none">
                                                    <img src="../../../images/x.png" alt="cross">
                                                    Address is invalid.
                                                </div>
                                            </div>
                                        </span>
                                        <span class="col-md-4">
                                            <div class="marginbtm15" id="divsttype" runat="server">
                                                <asp:Label ID="Label18" runat="server">Street Type</asp:Label>
                                                <div id="Div8" class="drpValidate" runat="server">
                                                    <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" AutoPostBack="true" CssClass="myval" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                        <asp:ListItem Value="">Street Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="editdetail" InitialValue=""> </asp:RequiredFieldValidator>

                                                </div>
                                            </div>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                    <%--    </ContentTemplate>
                                                                    </asp:UpdatePanel>--%>

                                    <div class="form-group spicaldivin">
                                        <span class="col-md-4">

                                            <label class="control-label" style="width: 150px;">Street City<span class="symbol required"></span> </label>

                                            <asp:TextBox ID="ddlStreetCity" runat="server" CssClass="form-control" AutoPostBack="true"
                                                OnTextChanged="ddlStreetCity_TextChanged" MaxLength="50"></asp:TextBox>


                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass="comperror"
                                                ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>

                                        <span class="col-md-4">

                                            <label class="control-label" style="width: 150px;">Street State<span class="symbol required"></span> </label>

                                            <asp:TextBox ID="txtStreetState" runat="server" MaxLength="10" CssClass="form-control"
                                                Enabled="false"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                        </span>

                                        <span class="col-md-4">
                                            <label class="control-label" style="width: 180px;">Street Post Code<span class="symbol required"></span> </label>
                                            <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="10" CssClass="form-control"
                                                Enabled="false"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter a number"
                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                ValidationGroup="editdetail" ControlToValidate="txtStreetPostCode" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </span>
                                        <div class="clear"></div>
                                        </span>
                                    </div>
                                    <%-- <div class="form-group spicaldivin">--%>
                                    <div class="form-group spical">
                                        <span class="col-md-12">
                                            <label class="control-label">
                                                Email:<span class="symbol required"></span>
                                            </label>
                                        </span><span class="col-md-12">
                                            <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                ValidationGroup="editdetail" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="" CssClass="comperror"
                                                ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="form-group spical">
                                        <span class="col-md-6">
                                            <label>
                                                Mobile:
                                            </label>
                                            <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile"
                                                ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>



                                        </span>
                                        <span class="col-md-6">
                                            <span>
                                                <label>Phone</label>
                                            </span>
                                            <span>
                                                <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                    ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                    ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>--%>

                                            </span>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group spical" style="margin-top: 10px; width: 100%;">
                                        <span class="col-md-6">
                                            <label style="width: 100%;">Solar Type</label>
                                            <div class="radio radio-info radio-inline">
                                                <label for="<%=rblResCom1.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                    <span class="text">Res&nbsp;</span>
                                                </label>

                                                <label for="<%=rblResCom2.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                    <span class="text">Com&nbsp;</span>
                                                </label>

                                            </div>
                                        </span>
                                        <span class="col-md-6">
                                            <label style="width: 100%;">Area</label>

                                            <div class="radio radio-info radio-inline">
                                                <label for="<%=rblArea1.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblArea1" GroupName="ar" />
                                                    <span class="text">Metro&nbsp;</span>
                                                </label>

                                                <label for="<%=rblArea2.ClientID %>">
                                                    <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" />
                                                    <span class="text">Regional&nbsp;</span>
                                                </label>
                                            </div>
                                        </span>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                        <asp:Button ID="ibtnEditDetail" runat="server" Text="Update" OnClick="ibtnEditDetail_Click"
                                            CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="editdetail" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNULLEdit" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndEditCustID" runat="server" />

            <asp:HiddenField runat="server" ID="hdncountdata" />
        </asp:Panel>
        <asp:Panel ID="panel" runat="server" CssClass="marbtm15">
            <div class="widget-body shadownone brdrgray">
                <div class="panel-body padallzero">
                    <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage col-sm-12">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="paddingleftright10" style="font-size: medium">
                                            <b>Total:&nbsp;</b><asp:Literal ID="lblnooflead" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp; <b>Unhandled:&nbsp;</b><asp:Literal ID="litUnHandlCount" runat="server"></asp:Literal>
                                        </td>
                                        <td class="paddingleftright10" style="font-size: medium">
                                            <b>Projects:&nbsp;</b><asp:Literal ID="lbltotalpanel" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp; <b>Sales:&nbsp;</b><asp:Literal ID="lblSale" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp; <b>Cancel:&nbsp;</b><asp:Literal ID="litCancel" runat="server"></asp:Literal>
                                        </td>

                                    </tr>
                                    
                                    <tr>
                                   
                                        <%--<td class="paddingleftright10" style="font-size: medium">
                                         <b>Total:&nbsp;</b><asp:Literal ID="Literal1" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;   <b>U-Yes:&nbsp;</b><asp:Literal ID="litunyes" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp; <b>U-No:&nbsp;</b><asp:Literal ID="litunhadleno" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td class="paddingleftright10" style="font-size: medium">
                                            <b>P-Yes:&nbsp;</b><asp:Literal ID="litprjyes" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp; <b>P-No:&nbsp;</b><asp:Literal ID="litprjno" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp; <b>Others:&nbsp;</b><asp:Literal ID="Literal2" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                        </td>--%>
                                    
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div class="form-group pull-right">
                        <table id="tdStatus" runat="server" border="0" cellpadding="5" cellspacing="0" style="width:400px">
                            
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlSalesRep1" runat="server" CssClass="myval"
                                        AppendDataBoundItems="true">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnNeawAssignLead" runat="server" Text="Assign Emp" CssClass="btn btn-s-md btn-danger btn-rounded"
                                        OnClick="btnNewAssignLead_Click" />
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
            <div class="">
                <div id="PanGrid" runat="server" class="leadtracker finalgrid">
                    <div class="table-responsive xscroll">
                        <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                            OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging"
                            OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hndEmpID1" runat="server" Value='<%# Eval("EmployeeID") %>' />
                                         <asp:HiddenField ID="HndLeadStatus" runat="server" Value='<%# Eval("ContLeadStatus") %>' />
                                        <asp:HiddenField ID="HndTeam" runat="server" Value='<%# Eval("Team") %>' />
                                        <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("CustomerID") %>' />
                                        <asp:HiddenField ID="hdnContactID" runat="server" Value='<%# Eval("ContactID") %>' />
                                        <asp:HiddenField ID="HndLeadDate" runat="server" Value='<%# Eval("CustEntered") %>' />
                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                            <%--  <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />--%>
                                            <%--  <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                            <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" SortExpression="Customer" HeaderStyle-CssClass="FrozenCell" ItemStyle-Width="250px">
                                    <ItemTemplate>
                                        <span>
                                            <asp:Label ID="Label1" runat="server" Width="150px" CssClass="postionrelative" Style="display: initial">
                                                <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                <asp:HyperLink ID="hypcompdetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank" CssClass="name2"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%> </asp:HyperLink>
                                                <div class="contacticonedit spcialcontacticonedit btneditmain">
                                                    <asp:LinkButton ID="gvbtnUpdate" CommandName="EditDetail" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-info btn-xs"
                                                        CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Edit">
                                                             <i class="fa fa-edit"></i> Edit</asp:LinkButton>


                                                </div>
                                            </asp:Label></span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ContMobile"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="Label9" runat="server" Width="80px"><%#Eval("ContMobile")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SubSource" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ContLeadStatus"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="Label8" runat="server" Width="80px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustSourceSub")%>'><%#Eval("CustSourceSub")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ContLeadStatus"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Width="80px"><%#Eval("ContLeadStatus")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="StreetAddress"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'
                                            Width="120px"><%#Eval("StreetAddress")%> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Suburb" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" SortExpression="StreetCity">
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Width="80px"><%#Eval("StreetCity")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" SortExpression="StreetState">
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Width="50px"><%#Eval("StreetState")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Next Follow-Up" ItemStyle-VerticalAlign="Top" ItemStyle-CssClass="tdspecialclass spicalheghtnew"
                                    HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" SortExpression="NextFollowupDate" ItemStyle-Width="250px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNextFollowUpDate" runat="server" Width="150px" CssClass="gridmainspan" Style="display: initial">
                                            <span class="name" style="width: 55%; float: left;"><%#Eval("NextFollowupDate","{0:dd MMM yyyy}")%></span>
                                            <div class="contacticonedit">
                                                <asp:LinkButton ID="lbtnFollowUpNote" CommandName="addfollowupnote" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-azure btn-xs"
                                                    CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add FollowUp">
                                                              <i class="fa fa-calendar"></i>Followup</asp:LinkButton>
                                            </div>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top"
                                    HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFollowUpNote" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>'
                                            Width="150px"><%#Eval("Description")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="ProjectNumber" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ResponseMsg"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProjectNumber" runat="server" Width="80px"><%#Eval("ProjectNumber")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ResponseMsg" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ResponseMsg"
                                    ItemStyle-HorizontalAlign="Center" Visible="false">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lblResponseMsg" runat="server" Width="80px"><%#Eval("ResponseMsg")%></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Lead Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="CustEntered" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Width="80px"><%# DataBinder.Eval(Container.DataItem, "CustEntered", "{0:dd MMM yyyy}")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cancel Lead" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="CustomerID"
                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCL" runat="server" Width="80px">
                                            <asp:LinkButton ID="lbtnCancelLead" CommandName="leadcancel" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-danger btn-xs"
                                                CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Cancel Lead">
                                                               <i class="fa fa-times"></i> Cancel </asp:LinkButton></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cancel Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="CustomerID"
                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" Visible="false">
                                    <ItemTemplate>

                                        <asp:Label ID="LabelCP" runat="server" Width="100px">
                                            <asp:LinkButton ID="lbtnCancelProject" CommandName="projectcancel" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-danger btn-xs"
                                                CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Cancel Project"> <i class="fa fa-times">
                                                    </i> Cancel </asp:LinkButton></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Sales Rep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" SortExpression="AssignedTo">
                                    <ItemTemplate>
                                        <asp:Label ID="Label33" runat="server" Width="100px"><%#Eval("AssignedTo")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="App Fixed By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="AppFixBy">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label113" runat="server" Width="120px"><%#Eval("AppFixBy")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="App Fixed Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="AppFixDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label112" runat="server" Width="110px"><%# DataBinder.Eval(Container.DataItem, "AppFixDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                <asp:TemplateField Visible="false" HeaderText="New Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="NewDateC">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelNDC" runat="server" Width="100px">
                                                            <%#Eval("NewDateC","{0:dd MMM yyyy}")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false" HeaderText="New Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="NewDateP">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelNDP" runat="server" Width="100px">
                                                       <%--bansi  <%#Eval("NewDateP","{0:dd MMM yyyy}")%>--%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="EmployeeID"
                                    HeaderText="Assign To Sales Rep">
                                    <ItemTemplate>

                                        <%--  <asp:Label ID="Label18" runat="server">--%>
                                        <asp:HiddenField ID="hdnemployeeid" runat="server" Value='<%# Eval("EmployeeID") %>' />
                                        <asp:DropDownList ID="ddlSalesRep" runat="server" CssClass="myval"
                                            AppendDataBoundItems="true">
                                        </asp:DropDownList>
                                        <%-- </asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Lead Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" SortExpression="CEDate">
                                    <ItemTemplate>
                                        <asp:Label ID="Label56" runat="server" Width="100px"><%#Eval("CEDate")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" HeaderText=" " visible="false">
                                    <HeaderTemplate>
                                        <center>
                                            <span class="euroradio">  
                                                        </span>
                                                      </center>
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <label for='<%# ((GridViewRow)Container).FindControl("chkread").ClientID %>' runat="server" id="lblchk789">

                                            <asp:CheckBox ID="chkread" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                                 <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText=" ">
                                        <HeaderTemplate>
                                            <center>
                                            <%--<span class="euroradio">                                      --%>
                                                 <label for='<%# ((GridViewRow)Container).FindControl("chksalesheader").ClientID %>' runat="server" id="lblchk1">
                                                <asp:CheckBox ID="chksalesheader" runat="server" AutoPostBack="true" OnCheckedChanged="chksalesheader_CheckedChanged" />
                                               
                                                    <span class="text">&nbsp;</span>
                                                </label>
                                          <%--  </span>--%>
                                        </center>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Label ID="Label11" runat="server" Width="150px">
                                                    <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select Sales Rep</asp:ListItem>
                                                    </asp:DropDownList></asp:Label>--%>
                                            <asp:Label ID="Label22" runat="server" Width="30px">

                                                <label for='<%# ((GridViewRow)Container).FindControl("chksales").ClientID %>' runat="server" id="lblchk">
                                                    <asp:CheckBox ID="chksales" runat="server" />
                                                    <span class="text">&nbsp;</span>
                                                </label>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                    <ItemTemplate>
                                        <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                            <td colspan="98%" class="details">
                                                <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                        <tr class="GridviewScrollItem">
                                                            <td width="15%"><b>PCode</b>
                                                            </td>
                                                            <td width="35%">
                                                                <asp:Label ID="Label6" runat="server"><%#Eval("StreetPostCode")%></asp:Label>
                                                            </td>
                                                            <td width="15%">
                                                                <b>Phone</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Labelcustphone" runat="server" Width="80px"><%#Eval("CustPhone")%></asp:Label>
                                                            </td>
                                                            <%--<td width="12%"><b>Sales Rep</b>
                                                                          </td>
                                                                            <td  width="36%">
                                                                                <asp:Label ID="Label3" runat="server" Width="89px"><%#Eval("AssignedTo")%></asp:Label>
                                                                            </td>--%>
                                                        </tr>
                                                        <tr>
                                                            <td runat="server" id="tdsource"><b>Source</b>
                                                            </td>
                                                            <td runat="server" id="tdsource1">
                                                                <asp:Label ID="Label111" runat="server"><%#Eval("CustSource")%></asp:Label>
                                                            </td>
                                                            <td width="15%"><b>Sub Source</b>
                                                            </td>

                                                            <td width="35%">
                                                                <asp:Label ID="Label121" runat="server"><%#Eval("CustSourceSub")%></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Email</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFollowUpNote1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ContEmail")%>'><%#Eval("ContEmail")%></asp:Label>
                                                            </td>
                                                            <td><b>Notes</b>
                                                            </td>

                                                            <td>
                                                                <asp:Label ID="Label44" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="trCancelReason" runat="server" visible="false">
                                                            <td><b>Cancel Lead Reason</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label4" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ContLeadCancelReason")%>'><%#Eval("ContLeadCancelReason")%></asp:Label>
                                                            </td>
                                                            <td><b>Cancel Project Reason</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label10" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("cancelprojectreason")%>'><%#Eval("cancelprojectreason")%></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="trCancelDescription" runat="server" visible="false">
                                                            <td><b>Cancel Lead Description</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblcancelleaddesc" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ContLeadCancelReasondesc")%>'><%#Eval("ContLeadCancelReasondesc")%></asp:Label>
                                                            </td>
                                                            <td><b>Cancel Project description</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblcancelprojectdesc" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("cancelprojectreason")%>'><%#Eval("cancelprojectreason")%></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td runat="server" id="thtype"><b>Type</b>
                                                            </td>
                                                            <td runat="server" id="tdtype">
                                                                <asp:Label ID="Label101" runat="server"><%#Eval("CustType")%></asp:Label>
                                                            </td>
                                                            <%--/td>
                                                                            <td><b>Cancel Project</b>
                                                                          </td>
                                                                           <td>
                                                                           <asp:Label ID="LabelCP" runat="server" Width="100px">
                                                            <asp:LinkButton ID="lbtnCancelProject" CommandName="projectcancel" CommandArgument='<%#Eval("CustomerID")%>'
                                                                CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Cancel Project"><img src="../../../images/delete_project.png"/></asp:LinkButton></asp:Label>--%>
                                                        </tr>



                                                    </table>

                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                            <AlternatingRowStyle />
                            <RowStyle />
                            <PagerTemplate>
                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                <div class="pagination">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                </div>
                            </PagerTemplate>
                            <PagerStyle CssClass="paginationGrid" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                        </asp:GridView>
                    </div>
                    <div class="paginationnew1" runat="server" id="divnopage">
                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                            <tr>
                                <td>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>                    
                </div>
                </div>
        </asp:Panel>
    </div>




    <script type="text/javascript">

        function gridviewScroll() {
            $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 25,
                height: 6000,
                freezesize: 0
            });
        }

    </script>

    <div class="loaderPopUP">
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
            }

            function pageLoadedpro() {
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $("[data-toggle=tooltip]").tooltip();

            }
        </script>


        <script type="text/javascript">
            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        </script>
    </div>
    <div class="test1" style="display: flex; justify-content: flex-end;">
        <style>
            .test1 .select2-container .select2-selection--single .select2-selection__rendered{min-width:120px;}           
             .test1 .btn{margin-left:10px;}
        </style>
         <div style="margin: 0px 0px 50px 0px;  display: flex; justify-content: flex-end;  flex-direction: row-reverse;">
        <div class="form-group pull-right">
                        <table id="tblassign" runat="server" border="0" cellpadding="5" cellspacing="0"
                            style="margin-top: 20px;">
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnAssidnLead" runat="server" Text="Assign lead" CssClass="btn btn-s-md btn-danger btn-rounded"
                                        OnClick="btnAssidnLead_Click" />
                                </td>

                            </tr>


                        </table>
                    </div>
                    <%--<div class="form-group pull-right">
                        <table id="tdStatus" runat="server" border="0" cellpadding="5" cellspacing="0"
                            style="margin-top: 20px;">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlSalesRep1" runat="server" CssClass="myval"
                                        AppendDataBoundItems="true">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnNeawAssignLead" runat="server" Text="Assign Emp" CssClass="btn btn-s-md btn-danger btn-rounded"
                                        OnClick="btnNewAssignLead_Click" />
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                    </div>--%>
    </div>
    </div>
   
    


</asp:Content>