﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PdfiumViewer;
using System.Drawing.Imaging;
using Google.Apis.Sheets.v4;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4.Data;
using System.Web.Services;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Security.Cryptography;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Threading.Tasks;

public partial class admin_adminfiles_company_SalesInfo : System.Web.UI.Page
{
    protected DataTable rpttable;
    protected DataTable rptTablePickList;
    protected static string Siteurl;
    protected string address;
    static int MaxAttribute = 1;
    protected DataTable dtRptVeriation;
    decimal capacity;
    decimal rebate;
    decimal SystemCapacity;
    decimal stcno;
    static DataView dv;
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];

    static int MaxAttributePick = 1;
    static int MaxAttributePickList = 1;

    #region CreateJob Model
    public partial class CreateJob
    {
        [JsonProperty("VendorJobId")]
        public int VendorJobId { get; set; }

        [JsonProperty("Arisesolarid")]
        public int Arisesolarid { get; set; }

        [JsonProperty("BasicDetails")]
        public BasicDetails BasicDetails { get; set; }

        [JsonProperty("InstallerView")]
        public InstallerView InstallerView { get; set; }

        [JsonProperty("ElectricianView")]
        public ElectricianView ElectricianView { get; set; }

        [JsonProperty("DesignerView")]
        public DesignerView DesignerView { get; set; }

        [JsonProperty("JobInstallationDetails")]
        public JobInstallationDetails JobInstallationDetails { get; set; }

        [JsonProperty("JobOwnerDetails")]
        public JobOwnerDetails JobOwnerDetails { get; set; }

        [JsonProperty("JobSystemDetails")]
        public JobSystemDetails JobSystemDetails { get; set; }

        [JsonProperty("JobSTCDetails")]
        public JobStcDetails JobStcDetails { get; set; }

        [JsonProperty("panel")]
        public Panel[] Panel { get; set; }

        [JsonProperty("inverter")]
        public Inverter[] Inverter { get; set; }

        [JsonProperty("lstJobNotes")]
        public LstJobNote[] LstJobNotes { get; set; }

        [JsonProperty("lstCustomDetails")]
        public LstCustomDetail[] LstCustomDetails { get; set; }

        public string quickformGuid { get; set; }

    }

    public partial class BasicDetails
    {
        [JsonProperty("JobType")]
        public int JobType { get; set; }
        [JsonProperty("Jobstatus")]
        public int Jobstatus { get; set; }

        [JsonProperty("formid")]
        public int formid { get; set; }

        [JsonProperty("Createddate")]
        public DateTime Createddate { get; set; }

        [JsonProperty("RefNumber")]
        public string RefNumber { get; set; }

        [JsonProperty("JobStage")]
        public int JobStage { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("strInstallationDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime StrInstallationDate { get; set; }

        [JsonProperty("Priority")]
        public int Priority { get; set; }
    }

    public partial class InstallerView
    {
        [JsonProperty("Installerid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }

        [JsonProperty("IsAutoAddVisit")]
        public bool IsAutoAddVisit { get; set; }

        [JsonProperty("IsUpdate")]
        public bool IsUpdate { get; set; }
    }

    public partial class ElectricianView
    {
        [JsonProperty("Electricianid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }

        [JsonProperty("LicenseNumber")]
        public string LicenseNumber { get; set; }
    }

    public partial class DesignerView
    {
        [JsonProperty("Designerid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }

        [JsonProperty("IsUpdate")]
        public bool IsUpdate { get; set; }
    }

    public partial class Inverter
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Series")]
        public string Series { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }
        [JsonProperty("size")]
        public string size { get; set; }
        [JsonProperty("noofinverter")]
        public int noofinverter { get; set; }
    }

    public partial class JobInstallationDetails
    {
        [JsonProperty("UnitTypeID")]
        public string UnitTypeID { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public string StreetTypeId { get; set; }

        [JsonProperty("Streetaddress")]
        public string Streetaddress { get; set; }


        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("AddressID")]
        public int AddressId { get; set; }

        [JsonProperty("PostalAddressID")]
        public int PostalAddressId { get; set; }

        [JsonProperty("Inst_UnitNo")]
        public string Inst_UnitNo { get; set; }

        [JsonProperty("Inst_UnitType")]
        public string Inst_UnitType { get; set; }

        [JsonProperty("Inst_StreetNo")]
        public string Inst_StreetNo { get; set; }

        [JsonProperty("Inst_StreetName")]
        public string Inst_StreetName { get; set; }


        [JsonProperty("Inst_StreetType")]
        public string Inst_StreetType { get; set; }

        [JsonProperty("Inst_StreetAddress")]
        public string Inst_StreetAddress { get; set; }


        [JsonProperty("Inst_Suburb")]
        public string Inst_Suburb { get; set; }

        [JsonProperty("Inst_State")]
        public string Inst_State { get; set; }

        [JsonProperty("Inst_PostCode")]
        public string Inst_PostCode { get; set; }


        [JsonProperty("PropertyType")]
        public string PropertyType { get; set; }

        [JsonProperty("SingleMultipleStory")]
        public string SingleMultipleStory { get; set; }

        [JsonProperty("Rooftype")]
        public string Rooftype { get; set; }

        [JsonProperty("RegPlanNo")]
        public string RegPlanNo { get; set; }

        [JsonProperty("LotNo")]
        public string LotNo { get; set; }

        [JsonProperty("InstallingNewPanel")]
        public string InstallingNewPanel { get; set; }

        [JsonProperty("ExistingSystem")]
        public bool ExistingSystem { get; set; }

        [JsonProperty("ExistingSystemSize")]
        public string ExistingSystemSize { get; set; }

        [JsonProperty("NoOfPanels")]
        public int NoOfPanels { get; set; }

        [JsonProperty("SystemLocation")]
        public string SystemLocation { get; set; }

        [JsonProperty("AdditionalInstallationInformation")]
        public string AdditionalInstallationInformation { get; set; }

        [JsonProperty("DistributorID")]
        public int DistributorID { get; set; }

        [JsonProperty("ElectricityProviderID")]
        public int ElectricityProviderID { get; set; }

        [JsonProperty("NMI")]
        public string NMI { get; set; }

        [JsonProperty("MeterNumber")]
        public string MeterNumber { get; set; }

        [JsonProperty("PhaseProperty")]
        public string PhaseProperty { get; set; }

    }

    public partial class JobOwnerDetails
    {
        [JsonProperty("OwnerType")]
        public string OwnerType { get; set; }

        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }



        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("UnitTypeID")]
        public string UnitTypeID { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }


        [JsonProperty("StreetTypeID")]
        public string StreetTypeId { get; set; }

        [JsonProperty("StreetAddress")]
        public string StreetAddress { get; set; }


        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }


        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
    }

    public partial class JobStcDetails
    {
        [JsonProperty("TypeOfConnection")]
        public string TypeOfConnection { get; set; }

        [JsonProperty("SystemMountingType")]
        public string SystemMountingType { get; set; }

        [JsonProperty("DeemingPeriod")]
        public string DeemingPeriod { get; set; }

        [JsonProperty("MultipleSGUAddress")]
        public string MultipleSguAddress { get; set; }

        [JsonProperty("Location")]
        public string Location { get; set; }

        //[JsonProperty("panel")]
        //public JobStcDetailsPanelBrand panel { get; set; }

        //[JsonProperty("inverter")]
        //public JobStcDetailsPanelBrand inverter { get; set; }


    }

    public partial class JobStcDetailsPanelBrand
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }
    }

    public partial class JobSystemDetails
    {
        [JsonProperty("SystemModel")]
        public string SystemModel { get; set; }

        [JsonProperty("SystemSize")]
        public decimal SystemSize { get; set; }

        [JsonProperty("Systemtype")]
        public int Systemtype { get; set; }

        [JsonProperty("ConnectionType")]
        public int ConnectionType { get; set; }

        [JsonProperty("MountingType")]
        public int MountingType { get; set; }

        [JsonProperty("SerialNumbers")]
        public string SerialNumbers { get; set; }

        [JsonProperty("NoOfPanel")]
        public int NoOfPanel { get; set; }

        [JsonProperty("InstallationType")]
        public string InstallationType { get; set; }

    }

    public partial class LstCustomDetail
    {
        [JsonProperty("VendorJobCustomFieldId")]
        public long VendorJobCustomFieldId { get; set; }

        [JsonProperty("FieldValue")]
        public string FieldValue { get; set; }

        [JsonProperty("FieldName")]
        public string FieldName { get; set; }
    }

    public partial class LstJobNote
    {
        [JsonProperty("VendorJobNoteId")]
        public string VendorJobNoteId { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }
    }

    public partial class Panel
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }

        [JsonProperty("size")]
        public string size { get; set; }
        [JsonProperty("STC")]
        public string STC { get; set; }
    }
    #endregion
    //Google Sheet 
    //For Local
    //static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    //static readonly string ApplicationName = "Sync tblWebDownload";
    //static readonly string sheet = "Sheet1";
    //static readonly string SpreadsheetId = "1tNnJ0g2KWSIFoOx8BDmDnbLrnTZMODrlop-ab-RRN-I";
    //static SheetsService service;

    //For Live
    static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    static readonly string ApplicationName = "AriseSolar WebDownloadData";
    static readonly string sheet = "Sheet1";
    static readonly string SpreadsheetId = "19NxOhA3Z1srODefkdzVbb0NJr2bHwJ8roS7rrmk3bMM";
    static SheetsService service;
    //Gogle Sheet
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }

    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }

    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }

    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st1.siteurl;

            SttblCustomers st_cust = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
            ltcompname.Text = " - " + st_cust.Customer;

            HidePanels();

            BindDropDown();// Bind Common Details DropDown
            if (Roles.IsUserInRole("Administrator"))
            {
                ddlSalesRep.Enabled = true;
            }
            BindProductDropdown();

            BindCommonDetails();
            BindSiteDetails();
            BindFinanceDetails();
            BindQuoteTabDropDown();
            BindQuoteDetails();
            BindTitle();

            BindRefund();

            BindGrid(0);

            BindStockDropdown();

            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Installation Manager"))
            {
                TabStock.Visible = true;
                TabPanelInstallDone.Visible = true;
                BindRptPicklistStock();

                BindInstallDone();
            }
            else
            {
                TabStock.Visible = false;
                TabPanelInstallDone.Visible = false;
            }
        }
    }

    public void BindTitle()
    {
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        //ltproject.Text = st.ProjectNumber + " - " + st.Project;
        ltproject.Text = st.ProjectNumber + " - " + st.InstallAddress + ',' + st.InstallCity + ',' + st.InstallState + '-' + st.InstallPostCode;
    }

    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanAddressValid.Visible = false;
    }

    protected void ddlProjectTypeID_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddllinkprojectid.Items.Clear();
        ddllinkprojectid.Items.Add(item2);
        if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
        {
            //ddllinkprojectid.Visible = false;
            //divoldproject.Visible = false;
            //if (ddlProjectTypeID.SelectedValue == "8")
            //{
            //    ddllinkprojectid.Visible = true;
            //    ddllinkprojectid.DataSource = ClstblProjects.tblProjects_GetProjectStatusComplete(Request.QueryString["compid"]);
            //    ddllinkprojectid.DataValueField = "ProjectID";
            //    ddllinkprojectid.DataTextField = "ProjectNumber";
            //    ddllinkprojectid.DataMember = "ProjectNumber";
            //    ddllinkprojectid.DataBind();
            //    RequiredFieldValidator8.Visible = true;
            //}
            //else
            //{
            //    ddllinkprojectid.Visible = false;
            //    RequiredFieldValidator8.Visible = false;
            //}
        }
        if (ddlProjectTypeID.SelectedValue == "5" || ddlProjectTypeID.SelectedValue == "3")
        {
            RequiredFieldValidatorOldSystemDetails.Enabled = true;
        }
        else
        {
            RequiredFieldValidatorOldSystemDetails.Enabled = false;
        }

        ddlOldRemoveOldSystem.SelectedValue = "1";
        rqVelidateRemoveOldSystem();
        removeOldPanel();
    }

    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        txtformbayUnitNo.Focus();
    }

    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtInstallAddress.Text = address;
    }

    public void checkExistsAddress()
    {
        string StreetCity = txtInstallCity.Text;
        string StreetState = txtInstallState.Text;
        string StreetPostCode = txtInstallPostCode.Text;
        string StreetAddress = txtInstallAddress.Text;
        int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);
        if (dt1.Rows.Count > 0)
        {
            //lbleror.Visible = true;
        }
        else
        {
            //lbleror.Visible = false;
        }
    }

    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        ddlformbayunittype.Focus();
    }

    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        txtformbayStreetNo.Focus();
    }

    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        streetaddress();
        //checkExistsAddress();
        txtformbaystreetname.Focus();
        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);
    }

    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        ddlformbaystreettype.Focus();
    }

    protected void txtInstallCity_OnTextChanged(object sender, EventArgs e)
    {
        string[] cityarr = txtInstallCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            txtInstallCity.Text = cityarr[0].Trim();
            txtInstallState.Text = cityarr[1].Trim();
            txtInstallPostCode.Text = cityarr[2].Trim();
        }
        else
        {
            txtInstallState.Text = string.Empty;
            txtInstallPostCode.Text = string.Empty;
        }
        //checkExistsAddress();
    }

    #region Veriation Repeater Code
    protected void BindRepeaterVeriation()
    {
        if (dtRptVeriation == null)
        {
            dtRptVeriation = new DataTable();
            dtRptVeriation.Columns.Add("VeriationID", Type.GetType("System.String"));
            dtRptVeriation.Columns.Add("VaritionValue", Type.GetType("System.String"));
            dtRptVeriation.Columns.Add("type", Type.GetType("System.String"));
            dtRptVeriation.Columns.Add("Notes", Type.GetType("System.String"));
        }

        for (int i = dtRptVeriation.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = dtRptVeriation.NewRow();

            dr["VeriationID"] = "";
            dr["VaritionValue"] = "";
            dr["type"] = "";
            dr["Notes"] = "";
            dtRptVeriation.Rows.Add(dr);
        }

        rptVeriation.DataSource = dtRptVeriation;
        rptVeriation.DataBind();
        rptVeriation.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptVeriation.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            LinkButton lbremove1 = (LinkButton)item1.FindControl("btnRemove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptVeriation.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptVeriation.Items)
                {
                    LinkButton lbremove = (LinkButton)item2.FindControl("btnRemove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void BindAddedAttributeRepeater()
    {
        dtRptVeriation = new DataTable();
        dtRptVeriation.Columns.Add("VeriationID", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("VaritionValue", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("type", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("Notes", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptVeriation.Items)
        {
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            HiddenField hndVeriationID = (HiddenField)item.FindControl("hndVeriationID");
            HiddenField hndVaritionValue = (HiddenField)item.FindControl("hndVaritionValue");
            HiddenField hndNotes = (HiddenField)item.FindControl("hndNotes");

            DropDownList ddlVeriation = (DropDownList)item.FindControl("ddlVeriation");
            TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");
            TextBox txtNotes = (TextBox)item.FindControl("txtNotes");

            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = ddlVeriation.SelectedValue;
            dr["VaritionValue"] = txtVariationValue.Text;
            dr["Notes"] = txtNotes.Text;

            dr["type"] = hdntype.Value;
            dtRptVeriation.Rows.Add(dr);
        }
    }

    protected void rptVeriation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        HiddenField hdntype = (HiddenField)e.Item.FindControl("hdntype");
        HiddenField hndVeriationID = (HiddenField)e.Item.FindControl("hndVeriationID");
        HiddenField hndVaritionValue = (HiddenField)e.Item.FindControl("VaritionValue");

        DropDownList ddlVeriation = (DropDownList)e.Item.FindControl("ddlVeriation");
        TextBox txtVariationValue = (TextBox)e.Item.FindControl("txtVariationValue");

        BindItem(item);

        //if (mode == "Edit")
        //{
        //DataTable dt11 = ClstblStockItems.tblStockItems_Select();
        //ddlitem.DataSource = dt11;
        //ddlitem.DataTextField = "StockItem";
        //ddlitem.DataValueField = "StockCode";
        //ddlitem.DataBind();
        //ddlitem.SelectedValue = hdnstockcode.Value;

        //DataTable dtC = ClstblStockItems.tblStockItems_SelectByItemID_CompanyLocationID(ddlitem.SelectedValue, ddltransferfrom.SelectedValue, ddltransferto.SelectedValue);
        ////   Response.Write(ddlitem.SelectedValue + "==" + ddltransferfrom.SelectedValue + "==" + ddltransferto.SelectedValue);
        //ddlcategory.DataSource = dtC;
        //ddlcategory.DataTextField = "StockCategory";
        //ddlcategory.DataValueField = "StockCategoryID";
        //ddlcategory.DataBind();
        //try
        //{
        //    // Response.Write(dtC.Rows[0]["StockCategoryID"].ToString());
        //    ddlcategory.SelectedValue = dtC.Rows[0]["StockCategoryID"].ToString();
        //}
        //catch
        //{
        //}
        //BindItem(item);
        //}
        // Response.End();
        //ddlitem.SelectedValue = hdnstockcode.Value;

        LinkButton lbremove = (LinkButton)e.Item.FindControl("btnRemove");
        if (MaxAttribute == 1)
        {
            lbremove.Visible = false;
        }

    }

    public void BindItem(RepeaterItem item)
    {
        DropDownList ddlVeriation = (DropDownList)item.FindControl("ddlVeriation");
        HiddenField hndVeriationID = (HiddenField)item.FindControl("hndVeriationID");

        TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");
        HiddenField VaritionValue = (HiddenField)item.FindControl("hndVaritionValue");
        HiddenField hndNotes = (HiddenField)item.FindControl("hndNotes");

        if (VaritionValue.Value != "")
        {
            txtVariationValue.Text = VaritionValue.Value;
        }

        ddlVeriation.Items.Clear();
        ListItem item1 = new ListItem();
        item1.Value = "";
        item1.Text = "Select";
        ddlVeriation.Items.Add(item1);

        string bindedVariation = "";
        foreach (RepeaterItem rptItem in rptVeriation.Items)
        {
            DropDownList ddlV = (DropDownList)rptItem.FindControl("ddlVeriation");
            bindedVariation += "," + ddlV.SelectedValue;
        }

        DataTable dt = ClstblSalesinfo.tbl_Variation_GetData(bindedVariation);
        ddlVeriation.DataSource = dt;
        ddlVeriation.DataTextField = "Variation";
        ddlVeriation.DataValueField = "ID";
        ddlVeriation.DataBind();

        ddlVeriation.SelectedValue = hndVeriationID.Value;

        TextBox txtNotes = (TextBox)item.FindControl("txtNotes");
        notesHideShow(ddlVeriation.SelectedValue, txtNotes);

        txtNotes.Text = hndNotes.Value;
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((LinkButton)sender).ClientID);
        RepeaterItem item = rptVeriation.Items[index];
        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        hdntype.Value = "1";
        int y = 0;

        foreach (RepeaterItem item1 in rptVeriation.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            LinkButton lbremove1 = (LinkButton)item1.FindControl("btnRemove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptVeriation.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptVeriation.Items)
                {
                    LinkButton lbremove = (LinkButton)item2.FindControl("btnRemove");
                    lbremove.Visible = false;
                }
            }
        }

        BindVeriationSum();
    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*]{2})",
                        RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);

        return Convert.ToInt32(match.Value);
    }

    protected void btnAddVeriation_Click(object sender, EventArgs e)
    {
        //foreach (RepeaterItem item in rptVeriation.Items)
        //{
        //    HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        //    HiddenField hndVeriationID = (HiddenField)item.FindControl("hndVeriationID");
        //    HiddenField hndVaritionValue = (HiddenField)item.FindControl("hndVaritionValue");

        //    DropDownList ddlVeriation = (DropDownList)item.FindControl("ddlVeriation");
        //    TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");

        //    hndVeriationID.Value = ddlVeriation.SelectedValue;
        //    hndVaritionValue.Value = txtVariationValue.Text;
        //}
        AddmoreAttributeBox();
    }

    protected void AddmoreAttributeBox()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttributeRepeater();
        BindRepeaterVeriation();
    }

    protected void HideShowRemove()
    {
        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptVeriation.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            LinkButton lbremove1 = (LinkButton)item1.FindControl("btnRemove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptVeriation.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptVeriation.Items)
                {
                    LinkButton lbremove = (LinkButton)item2.FindControl("btnRemove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    #endregion

    #region Bind Dropdown
    public void BindDropDown()
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);
        ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(Request.QueryString["compid"]);
        ddlContact.DataValueField = "ContactID";
        ddlContact.DataTextField = "Contact";
        ddlContact.DataMember = "Contact";
        ddlContact.DataBind();
        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlProjectTypeID.Items.Clear();
        ddlProjectTypeID.Items.Add(item2);

        DataTable dtProjectType = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectTypeID.DataSource = dtProjectType;
        ddlProjectTypeID.DataValueField = "ProjectTypeID";
        ddlProjectTypeID.DataTextField = "ProjectType";
        ddlProjectTypeID.DataMember = "ProjectType";
        ddlProjectTypeID.DataBind();
        if (dtProjectType.Rows.Count > 0)
        {
            ddlProjectTypeID.SelectedValue = "2";
        }

        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();

        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();
        //DataTable dtProjectType = ClstblProjectType.tblProjectType_SelectType();
        //if (dtProjectType.Rows.Count > 0)
        //{
        //    ddlProjectTypeID.SelectedValue = dtProjectType.Rows[0]["ProjectTypeID"].ToString();
        //}
        DataTable dtContact = ClstblContacts.tblContacts_SelectTop(Request.QueryString["compid"]);
        if (dtContact.Rows.Count > 0)
        {
            ddlContact.SelectedValue = dtContact.Rows[0]["ContactID"].ToString();
        }

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
        }

        DataTable dtSalesRap = new DataTable();
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            if (SalesTeam != string.Empty)
            {
                dtSalesRap = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            dtSalesRap = ClstblEmployees.tblEmployees_SelectASC();
        }
        ddlSalesRep.DataSource = dtSalesRap;
        ddlSalesRep.DataValueField = "EmployeeID";
        ddlSalesRep.DataTextField = "fullname";
        ddlSalesRep.DataMember = "fullname";
        ddlSalesRep.DataBind();

        ddlRecBy.DataSource = dtSalesRap;
        ddlRecBy.DataValueField = "EmployeeID";
        ddlRecBy.DataTextField = "fullname";
        ddlRecBy.DataMember = "fullname";
        ddlRecBy.DataBind();

        ddlRecByInstall.DataSource = dtSalesRap;
        ddlRecByInstall.DataValueField = "EmployeeID";
        ddlRecByInstall.DataTextField = "fullname";
        ddlRecByInstall.DataMember = "fullname";
        ddlRecByInstall.DataBind();

        try
        {
            ddlSalesRep.SelectedValue = st.EmployeeID;
            ddlRecBy.SelectedValue = st.EmployeeID;
            ddlRecByInstall.SelectedValue = st.EmployeeID;
        }
        catch (Exception ex)
        {

        }

    }

    public void BindProductDropdown()
    {
        SttblProjects stProj = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProj.CustomerID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);

        DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(stProj.InstallState);
        string State = "0";

        if (dtState.Rows.Count > 0)
        {
            State = dtState.Rows[0]["CompanyLocationID"].ToString();
        }

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        #region ddlPanel
        DataTable dtPanel = null;
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            dtPanel = ClsProjectSale.tblStockItems_SelectPanel_SalesRep(State);
            //Response.Write(State);
            //Response.End();
            //if (st.EmployeeID == "42")
            //{
            //    dtPanel = ClsProjectSale.tblStockItems_SelectPanel(State);
            //}
            //else
            //{
            //    dtPanel = ClsProjectSale.tblStockItems_SelectPanel_SalesRep(State);
            //}
        }
        else
        {
            dtPanel = ClsProjectSale.tblStockItems_SelectPanel(State);
        }

        ddlPanels.DataSource = dtPanel;
        ddlPanels.DataValueField = "StockItemID";
        ddlPanels.DataMember = "StockItem";
        ddlPanels.DataTextField = "StockItem";

        ddlOldPanel.DataSource = dtPanel;
        ddlOldPanel.DataValueField = "StockItemID";
        ddlOldPanel.DataMember = "StockItem";
        ddlOldPanel.DataTextField = "StockItem";

        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            string PanelID = stProj.PanelBrandID;
            if (!String.IsNullOrEmpty(PanelID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(PanelID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(PanelID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem P1 = new ListItem(); // Example List
                        P1.Text = dt.Rows[0]["StockItem"].ToString();
                        P1.Value = PanelID;
                        ddlPanels.Items.Add(P1);
                        ddlOldPanel.Items.Add(P1);
                    }
                }
            }
        }
        if (!String.IsNullOrEmpty(stProj.PanelBrandID))
        {
            SttblStockItems stitem = ClstblStockItems.tblStockItems_SelectByStockItemID(stProj.PanelBrandID);
            if (stitem.Active.ToString() == "False")
            {
                ListItem P1 = new ListItem(); // Example List
                P1.Text = stitem.StockItem.ToString();
                P1.Value = stProj.PanelBrandID;
                ddlPanels.Items.Add(P1);
                ddlOldPanel.Items.Add(P1);
            }
        }
        ddlPanels.DataBind();
        ddlOldPanel.DataBind();
        #endregion

        #region ddlInverter
        DataTable dtInverter = null;
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
            //if (st.EmployeeID == "42")
            //{
            //    dtInverter = ClsProjectSale.tblStockItems_SelectInverter(State);
            //}
            //else
            //{
            //    dtInverter = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
            //}

        }
        else
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter(State);
        }

        ddlInverter.DataSource = dtInverter;
        ddlInverter.DataValueField = "StockItemID";
        ddlInverter.DataMember = "StockItem";
        ddlInverter.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            string Inverter1ID = stProj.InverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter1ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter1ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter1ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I1 = new ListItem(); // Example List
                        I1.Text = dt.Rows[0]["StockItem"].ToString();
                        I1.Value = Inverter1ID;
                        ddlInverter.Items.Add(I1);
                    }
                }
            }
        }
        if (!String.IsNullOrEmpty(stProj.InverterDetailsID))
        {
            SttblStockItems stitem = ClstblStockItems.tblStockItems_SelectByStockItemID(stProj.InverterDetailsID);
            if (stitem.Active != null)
            {
                if (stitem.Active.ToString() == "False")
                {
                    ListItem P1 = new ListItem(); // Example List
                    P1.Text = stitem.StockItem.ToString();
                    P1.Value = stitem.StockItemID;
                    ddlInverter.Items.Add(P1);
                }
            }
        }
        ddlInverter.DataBind();
        #endregion

        #region ddlInverter2
        DataTable dtInverter2 = null;
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            dtInverter2 = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
            //if (st.EmployeeID == "42")
            //{
            //    dtInverter2 = ClsProjectSale.tblStockItems_SelectInverter(State);
            //}
            //else
            //{
            //    dtInverter2 = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
            //}
        }
        else
        {
            dtInverter2 = ClsProjectSale.tblStockItems_SelectInverter(State);
        }

        ddlSecondInverter.DataSource = dtInverter;
        ddlSecondInverter.DataValueField = "StockItemID";
        ddlSecondInverter.DataMember = "StockItem";
        ddlSecondInverter.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            string Inverter2ID = stProj.SecondInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter2ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter1ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter2ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I1 = new ListItem(); // Example List
                        I1.Text = dt.Rows[0]["StockItem"].ToString();
                        I1.Value = Inverter2ID;
                        ddlSecondInverter.Items.Add(I1);
                    }
                }
            }
        }
        if (!String.IsNullOrEmpty(stProj.SecondInverterDetailsID))
        {
            SttblStockItems stitem = ClstblStockItems.tblStockItems_SelectByStockItemID(stProj.SecondInverterDetailsID);
            if (stitem.Active != null)
            {
                if (stitem.Active.ToString() == "False")
                {
                    ListItem P1 = new ListItem(); // Example List
                    P1.Text = stitem.StockItem.ToString();
                    P1.Value = stitem.StockItemID;
                    ddlSecondInverter.Items.Add(P1);
                }
            }
        }
        ddlSecondInverter.DataBind();
        #endregion

        #region ddlInverter3
        //DataTable dtInverter3 = null;
        //if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        //{
        //    if (st.EmployeeID == "42")
        //    {
        //        dtInverter3 = ClsProjectSale.tblStockItems_SelectInverter(State);
        //    }
        //    else
        //    {
        //        dtInverter3 = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
        //    }
        //}
        //else
        //{
        //    dtInverter3 = ClsProjectSale.tblStockItems_SelectInverter(State);
        //}
        DataTable dtInverter3 = ClstblStockItems.tblStockItems_SelectbyAsc("5");

        ddlThirdInverter.DataSource = dtInverter3;
        ddlThirdInverter.DataValueField = "StockItemID";
        ddlThirdInverter.DataMember = "StockItem";
        ddlThirdInverter.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            string Inverter3ID = stProj.ThirdInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter3ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter1ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter3ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I1 = new ListItem(); // Example List
                        I1.Text = dt.Rows[0]["StockItem"].ToString();
                        I1.Value = Inverter3ID;
                        ddlThirdInverter.Items.Add(I1);
                    }
                }
            }
        }
        if (!String.IsNullOrEmpty(stProj.ThirdInverterDetailsID))
        {
            SttblStockItems stitem = ClstblStockItems.tblStockItems_SelectByStockItemID(stProj.ThirdInverterDetailsID);
            if (stitem.Active != null)
            {
                if (stitem.Active.ToString() == "False")
                {
                    ListItem P1 = new ListItem(); // Example List
                    P1.Text = stitem.StockItem.ToString();
                    P1.Value = stitem.StockItemID;
                    ddlThirdInverter.Items.Add(P1);
                }
            }
        }
        ddlThirdInverter.DataBind();
        #endregion

        ddlHouseType.DataSource = ClstblHouseType.tblHouseType_SelectASC();
        ddlHouseType.DataValueField = "HouseTypeID";
        ddlHouseType.DataMember = "HouseType";
        ddlHouseType.DataTextField = "HouseType";
        ddlHouseType.DataBind();

        string EleDist = "";
        if (stProj.InstallState == "NSW")
        {
            EleDist = "select * from tblElecDistributor where NSW=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "SA")
        {
            EleDist = "select * from tblElecDistributor where SA=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "QLD")
        {
            EleDist = "select * from tblElecDistributor where QLD=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "VIC")
        {
            EleDist = "select * from tblElecDistributor where VIC=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "WA")
        {
            EleDist = "select * from tblElecDistributor where WA=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "ACT")
        {
            EleDist = "select * from tblElecDistributor where ACT=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "TAS")
        {
            EleDist = "select * from tblElecDistributor where TAS=1 order by ElecDistributor asc";
        }
        if (stProj.InstallState == "NT")
        {
            EleDist = "select * from tblElecDistributor where NT=1 order by ElecDistributor asc";
        }

        DataTable dtElecDist = ClsProjectSale.ap_form_element_execute(EleDist);
        if (dtElecDist.Rows.Count > 0)
        {
            ddlElecDist.DataSource = dtElecDist;
            ddlElecDist.DataValueField = "ElecDistributorID";
            ddlElecDist.DataMember = "ElecDistributor";
            ddlElecDist.DataTextField = "ElecDistributor";
            ddlElecDist.DataBind();
        }

        ddlRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        ddlRoofType.DataValueField = "RoofTypeID";
        ddlRoofType.DataMember = "RoofType";
        ddlRoofType.DataTextField = "RoofType";
        ddlRoofType.DataBind();

        ddlRoofAngle.DataSource = ClstblRoofAngles.tblRoofAngles_SelectASC();
        ddlRoofAngle.DataValueField = "RoofAngleID";
        ddlRoofAngle.DataMember = "RoofAngle";
        ddlRoofAngle.DataTextField = "RoofAngle";
        ddlRoofAngle.DataBind();

        string EleRet = "select * from tblElecRetailer where Active=1";

        DataTable dtElecRet = ClsProjectSale.ap_form_element_execute(EleRet);
        if (dtElecRet.Rows.Count > 0)
        {
            ddlElecRetailer.DataSource = dtElecRet;
            ddlElecRetailer.DataValueField = "ElecRetailerID";
            ddlElecRetailer.DataMember = "ElecRetailer";
            ddlElecRetailer.DataTextField = "ElecRetailer";
            ddlElecRetailer.DataBind();
        }

        ddlPaymentOption.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlPaymentOption.DataValueField = "FinanceWithID";
        ddlPaymentOption.DataMember = "FinanceWith";
        ddlPaymentOption.DataTextField = "FinanceWith";

        string PaymentOption = stProj.FinanceWithID;
        if (!String.IsNullOrEmpty(PaymentOption))
        {
            DataTable dt = ClstblFinanceWith.tblFinanceWith_SelectByFinanceWithID_New(PaymentOption);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Active"].ToString() == "False")
                {
                    ListItem P1 = new ListItem(); // Example List
                    P1.Text = dt.Rows[0]["FinanceWith"].ToString();
                    P1.Value = PaymentOption;
                    ddlPaymentOption.Items.Add(P1);
                }
            }
        }
        ddlPaymentOption.DataBind();

        ddlPaymentType.DataSource = ClstblPaymentType.tblPaymentType_SelectActive();
        ddlPaymentType.DataValueField = "PaymentTypeID";
        ddlPaymentType.DataMember = "PaymentType";
        ddlPaymentType.DataTextField = "PaymentType";

        string PaymentType = stProj.PaymentTypeID;
        if (!String.IsNullOrEmpty(PaymentType))
        {
            DataTable dt = ClstblPaymentType.tblPaymentType_SelectByPaymentTypeIDNew(PaymentType);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Active"].ToString() == "False")
                {
                    ListItem P1 = new ListItem(); // Example List
                    P1.Text = dt.Rows[0]["PaymentType"].ToString();
                    P1.Value = PaymentType;
                    ddlPaymentType.Items.Add(P1);
                }
            }
        }
        ddlPaymentType.DataBind();

        ddlDepositOption.DataSource = ClstblFinanceWith.tblFinanceWithDeposit_SelectActive();
        ddlDepositOption.DataValueField = "FinanceWithDepositID";
        ddlDepositOption.DataMember = "FinanceWithDeposit";
        ddlDepositOption.DataTextField = "FinanceWithDeposit";
        string DepositOption = st2.FinanceWithDepositID;
        if (!String.IsNullOrEmpty(DepositOption))
        {
            DataTable dt = ClstblFinanceWith.tblFinanceWithDeposit_SelectByFinanceWithID(DepositOption);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Active"].ToString() == "False")
                {
                    ListItem P1 = new ListItem(); // Example List
                    P1.Text = dt.Rows[0]["FinanceWithDeposit"].ToString();
                    P1.Value = DepositOption;
                    ddlDepositOption.Items.Add(P1);
                }
            }
        }
        ddlDepositOption.DataBind();

        ddlPromo1ID.DataSource = ClstblPromoType.tblPromoOffer_SelectASC();
        ddlPromo1ID.DataValueField = "PromoOfferID";
        ddlPromo1ID.DataMember = "PromoOffer";
        ddlPromo1ID.DataTextField = "PromoOffer";
        ddlPromo1ID.DataBind();

        ddlPromo2ID.DataSource = ClstblPromoType.tbl_pstatus_SelectASC();
        ddlPromo2ID.DataValueField = "statusid";
        ddlPromo2ID.DataMember = "pstatus";
        ddlPromo2ID.DataTextField = "pstatus";
        ddlPromo2ID.DataBind();

        DataTable dt1 = ClsProjectSale.tblFPTransType_getInv();
        ddlInvoicePayMethodID.DataSource = dt1;
        ddlInvoicePayMethodID.DataValueField = "FPTransTypeID";
        ddlInvoicePayMethodID.DataTextField = "FPTransTypeAB";
        ddlInvoicePayMethodID.DataMember = "FPTransTypeAB";
        ddlInvoicePayMethodID.DataBind();

        ddlInvoicePayMethodIDInstall.DataSource = dt1;
        ddlInvoicePayMethodIDInstall.DataValueField = "FPTransTypeID";
        ddlInvoicePayMethodIDInstall.DataTextField = "FPTransTypeAB";
        ddlInvoicePayMethodIDInstall.DataMember = "FPTransTypeAB";
        ddlInvoicePayMethodIDInstall.DataBind();

        ddlHouseStatus.DataSource = ClstblProjects.tbl_HouseStatys_Select();
        ddlHouseStatus.DataValueField = "Id";
        ddlHouseStatus.DataTextField = "Name";
        ddlHouseStatus.DataMember = "Name";
        ddlHouseStatus.DataBind();

        try
        {
            ddlHouseStatus.SelectedValue = stProj.HouseStayID;
        }
        catch (Exception ex)
        { }

        try
        {
            txtCompletionDate.Text = stProj.HouseStayDate;
        }
        catch { }

        try
        {
            if (stProj.ElecDistApplied != "")
            {
                txtDistApplied.Text = Convert.ToDateTime(stProj.ElecDistApplied).ToShortDateString();
            }
        }
        catch { }
        txtApprovalRef.Text = stProj.ElecDistApprovelRef;
    }
    #endregion

    #region Fill Existing Value in Control
    public void BindCommonDetails()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //Bind Booking Tab
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Installation Manager"))
        {
            TabBooking.Visible = true;
            CheckActive(stPro);
            if (divActive.Visible != true)
            {
                BindBookingDropdown();
                BindBooking(stPro);
            }

        }
        else
        {
            TabBooking.Visible = false;
        }

        txtInstallAddress.Text = stPro.unit_type + " " + stPro.unit_number + " " + stPro.street_number + " " + stPro.street_name + " " + stPro.street_type;

        txtformbayUnitNo.Text = stPro.unit_number;
        txtformbayStreetNo.Text = stPro.street_number;
        txtformbaystreetname.Text = stPro.street_name;
        txtInstallCity.Text = stPro.InstallCity;
        txtInstallState.Text = stPro.InstallState;
        txtInstallPostCode.Text = stPro.InstallPostCode;
        txtProjectNotes.Text = stPro.ProjectNotes;
        txtManualQuoteNumber.Text = stPro.ManualQuoteNumber;
        txtProjectOpened.Text = Convert.ToDateTime(stPro.ProjectOpened).ToShortDateString();

        txtInstallerNotes.Text = stPro.InstallerNotes;


        try
        {
            ddlformbayunittype.SelectedValue = stPro.unit_type;
            ddlformbaystreettype.SelectedValue = stPro.street_type;
            if (stPro.EmployeeID != "")
            {
                ddlSalesRep.SelectedValue = stPro.EmployeeID;
            }
        }
        catch (Exception ex)
        {
            //Error Log
        }

        try
        {
            ddlProjectTypeID.SelectedValue = stPro.ProjectTypeID;
        }
        catch (Exception ex)
        {
        }

        if (ddlProjectTypeID.SelectedValue == "5" || ddlProjectTypeID.SelectedValue == "3")
        {
            RequiredFieldValidatorOldSystemDetails.Enabled = true;
        }
        else
        {
            RequiredFieldValidatorOldSystemDetails.Enabled = false;
        }

        

        if (stPro.InstallState == "NSW")
        {
            if (stPro.DOB != "")
            {
                txtDoB.Text = Convert.ToDateTime(stPro.DOB).ToShortDateString();
            }
            DivDoB.Visible = true;
        }
        else
        {
            DivDoB.Visible = false;
        }

        if (!Roles.IsUserInRole("Administrator"))
        {
            if (stPro.ProjectStatusID != "2")
            {
                txtProjectNotes.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            if (stPro.ProjectStatusID == "8")
            {
                txtInstallerNotes.Enabled = false;
                txtOldSystemDetails.Enabled = false;
                txtProjectNotes.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("Sales Manager"))
        {
            if (stPro.ProjectStatusID == "8")
            {
                //txtInstallerNotes.Enabled = false;
                txtOldSystemDetails.Enabled = true;
                txtProjectNotes.Enabled = true;
            }
        }
    }

    public void BindSiteDetails()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects stmntc = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            if (stmntc.ProjectStatusID == "15")
            {
                if (string.IsNullOrEmpty(ddlPanels.SelectedValue))
                {
                    DataTable dt = ClstblProjects.tblProjects_Select_First_ByUCustomerID(Request.QueryString["compid"]);
                    if (dt.Rows.Count > 0)
                    {
                        string projectid = dt.Rows[0]["ProjectID"].ToString();
                        //divMtce.Visible = false;
                        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(projectid);

                        hndInstallBookingDate.Value = st.InstallBookingDate;

                        try
                        {
                            //Response.Write(st.PanelBrandID);
                            //Response.End();
                            ddlPanels.SelectedValue = st.PanelBrandID;
                        }
                        catch { }

                        //txtPanelBrand.Text = st.PanelBrand;
                        //Response.Write(st.PanelBrand);
                        //Response.End();
                        txtWatts.Text = st.PanelOutput == "" ? "0" : st.PanelOutput;

                        hndPanelBrand.Value = st.PanelBrand;

                        try
                        {
                            txtRebate.Text = SiteConfiguration.ChangeCurrency_Val((st.RECRebate).ToString());
                        }
                        catch { }
                        txtSTCMult.Text = st.STCMultiplier;
                        txtPanelModel.Text = st.PanelModel;
                        txtNoOfPanels.Text = st.NumberPanels;
                        txtSystemCapacity.Text = st.SystemCapKW;
                        txtInstallerNotes.Text = st.InstallerNotes;
                        txtOldSystemDetails.Text = st.OldSystemDetails;



                        if (st.InverterOutput != "")
                        {
                            txtInverterOutput.Text = Convert.ToDecimal(st.InverterOutput).ToString("F");
                        }
                        else
                        {
                            txtInverterOutput.Text = "0.00";
                        }


                        //try
                        //{            
                        ddlInverter.SelectedValue = st.InverterDetailsID;

                        if (st.InverterDetailsID != "")
                        {
                            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                            if (stInverter.StockSize != "")
                            {
                                hndInverterOutput1.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                            }
                        }


                        //}
                        //catch
                        //{
                        //}
                        //txtInverterBrand.Text = st.InverterBrand;
                        txtSeries.Text = st.InverterSeries;
                        try
                        {
                            ddlSecondInverter.SelectedValue = st.SecondInverterDetailsID;
                            if (st.SecondInverterDetailsID != "")
                            {
                                SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.SecondInverterDetailsID);
                                if (stInverter.StockSize != "")
                                {
                                    hndInverterOutput2.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                }
                            }

                            if (st.ThirdInverterDetailsID != "")
                            {
                                ddlThirdInverter.SelectedValue = st.ThirdInverterDetailsID;
                                if (st.ThirdInverterDetailsID != "")
                                {
                                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.ThirdInverterDetailsID);
                                    if (stInverter.StockSize != "")
                                    {
                                        hndInverterOutput3.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                    }
                                }
                            }

                        }
                        catch
                        {
                        }
                        txtSTC.Text = st.STCNumber;

                        DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                        if (dtSTCValue.Rows.Count > 0)
                        {
                            //try
                            //{
                            txtSTCInverter.Text = SiteConfiguration.ChangeCurrency_Val(dtSTCValue.Rows[0]["STCValue"].ToString());
                            //}
                            //catch { }
                        }

                        txtInverterModel.Text = st.InverterModel;

                        if (st.inverterqty != "")
                        {
                            txtNoOfInverter.Text = st.inverterqty;
                        }
                        else
                        {
                            txtNoOfInverter.Text = "0";
                        }
                        if (st.inverterqty2 != "")
                        {
                            txtNoOfInverter2.Text = st.inverterqty2;
                        }
                        else
                        {
                            txtNoOfInverter2.Text = "0";
                        }
                        if (st.inverterqty3 != "")
                        {
                            txtNoOfInverter3.Text = st.inverterqty3;
                        }
                        else
                        {
                            txtNoOfInverter3.Text = "0";
                        }

                        //Response.Write(st.inverterqty2);
                        //Response.End();




                        //txtInverterOutput.Text = st.InverterOutput;
                        try
                        {
                            ddlElecDist.SelectedValue = st.ElecDistributorID;
                        }
                        catch
                        {
                        }
                        //txtApprovalRef.Text = st.ElecDistApprovelRef;
                        //try
                        //{
                        ddlElecRetailer.SelectedValue = st.ElecRetailerID;
                        //}
                        //catch
                        //{
                        //}
                        txtRegPlanNo.Text = st.RegPlanNo;
                        //try
                        //{
                        ddlHouseType.SelectedValue = st.HouseTypeID;
                        //}
                        //catch
                        //{
                        //}
                        //try
                        //{
                        ddlRoofType.SelectedValue = st.RoofTypeID;


                        if (st.FlatPanels != null && st.FlatPanels != "")
                        {
                            txtFlatPanels.Text = st.FlatPanels;
                        }
                        else
                        {
                            txtFlatPanels.Text = "0";

                        }
                        if (st.PitchedPanels != null && st.PitchedPanels != "")
                        {
                            txtPitchedPanels.Text = st.PitchedPanels;
                        }
                        else
                        {
                            txtPitchedPanels.Text = "0";

                        }
                        //updatedate(st.RoofTypeID);

                        //}
                        //catch
                        //{ }
                        //try
                        //{
                        ddlRoofAngle.SelectedValue = st.RoofAngleID;
                        //}
                        //catch
                        //{ }
                        txtLotNum.Text = st.LotNumber;
                        txtNMINumber.Text = st.NMINumber;
                        txtPeakMeterNo.Text = st.MeterNumber1;
                        txtOffPeakMeters.Text = st.MeterNumber2;
                        txtMeterPhase.Text = st.MeterPhase;
                        try
                        {
                            ddlmeterupgrade.SelectedValue = st.meterupgrade;
                        }
                        catch
                        {
                        }


                        chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
                        chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);

                        if (st.InstallPostCode != "")
                        {
                            DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                            if (dtZoneCode.Rows.Count > 0)
                            {

                                string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                                DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                if (dtZoneRt.Rows.Count > 0)
                                {
                                    //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                                    //Response.End();
                                    txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                                }
                            }
                        }

                        if (Roles.IsUserInRole("Installation Manager"))
                        {
                            if (st.ProjectStatusID == "3")
                            {
                                if (st.ElecDistributorID == "12")
                                {
                                    //txtApprovalRef.Enabled = false;
                                    txtNMINumber.Enabled = false;
                                }
                                else if (st.ElecDistributorID == "13")
                                {
                                    //txtApprovalRef.Enabled = false;
                                    txtNMINumber.Enabled = false;
                                    txtLotNum.Enabled = false;
                                    txtRegPlanNo.Enabled = false;
                                }
                                else
                                {
                                    //txtApprovalRef.Enabled = false;
                                    txtNMINumber.Enabled = false;
                                }
                            }
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
                        {
                            //divMtce.Visible = false;
                            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

                            try
                            {
                                //Response.Write(st.PanelBrandID);
                                //Response.End();
                                ddlPanels.SelectedValue = st.PanelBrandID;
                            }
                            catch { }

                            //txtPanelBrand.Text = st.PanelBrand;
                            //Response.Write(st.PanelBrand);
                            //Response.End();

                            txtWatts.Text = st.PanelOutput == "" ? "0" : st.PanelOutput;
                            hndPanelBrand.Value = st.PanelBrand;
                            try
                            {
                                txtRebate.Text = SiteConfiguration.ChangeCurrency_Val((st.RECRebate).ToString());
                            }
                            catch { }
                            txtSTCMult.Text = st.STCMultiplier;
                            txtPanelModel.Text = st.PanelModel;
                            txtNoOfPanels.Text = st.NumberPanels;
                            txtSystemCapacity.Text = st.SystemCapKW;
                            txtInstallerNotes.Text = st.InstallerNotes;
                            txtOldSystemDetails.Text = st.OldSystemDetails;
                            //try
                            //{            
                            ddlInverter.SelectedValue = st.InverterDetailsID;
                            if (st.InverterDetailsID != "")
                            {
                                SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                                if (stInverter.StockSize != "")
                                {
                                    hndInverterOutput1.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                }
                            }

                            if (st.InverterOutput != "")
                            {
                                txtInverterOutput.Text = Convert.ToDecimal(st.InverterOutput).ToString("F");
                            }
                            else
                            {
                                txtInverterOutput.Text = "0.00";
                            }
                            //}
                            //catch
                            //{
                            //}
                            //txtInverterBrand.Text = st.InverterBrand;
                            txtSeries.Text = st.InverterSeries;
                            try
                            {
                                ddlSecondInverter.SelectedValue = st.SecondInverterDetailsID;
                                if (st.SecondInverterDetailsID != "")
                                {
                                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.SecondInverterDetailsID);
                                    if (stInverter.StockSize != "")
                                    {
                                        hndInverterOutput2.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                    }
                                }

                                if (st.ThirdInverterDetailsID != "")
                                {
                                    ddlThirdInverter.SelectedValue = st.ThirdInverterDetailsID;
                                    if (st.ThirdInverterDetailsID != "")
                                    {
                                        SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.ThirdInverterDetailsID);
                                        if (stInverter.StockSize != "")
                                        {
                                            hndInverterOutput3.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                        }
                                    }
                                }

                            }
                            catch
                            {
                            }
                            txtSTC.Text = st.STCNumber;

                            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                            if (dtSTCValue.Rows.Count > 0)
                            {
                                //try
                                //{
                                txtSTCInverter.Text = SiteConfiguration.ChangeCurrency_Val(dtSTCValue.Rows[0]["STCValue"].ToString());
                                //}
                                //catch { }
                            }


                            txtInverterModel.Text = st.InverterModel;
                            if (st.inverterqty != "")
                            {
                                txtNoOfInverter.Text = st.inverterqty;
                            }
                            else
                            {
                                txtNoOfInverter.Text = "0";
                            }
                            if (st.inverterqty2 != "")
                            {
                                txtNoOfInverter2.Text = st.inverterqty2;
                            }
                            else
                            {
                                txtNoOfInverter2.Text = "0";
                            }
                            if (st.inverterqty3 != "")
                            {
                                txtNoOfInverter3.Text = st.inverterqty3;
                            }
                            else
                            {
                                txtNoOfInverter3.Text = "0";
                            }


                            //txtInverterOutput.Text = st.InverterOutput;
                            try
                            {
                                ddlElecDist.SelectedValue = st.ElecDistributorID;
                            }
                            catch
                            {
                            }
                            //txtApprovalRef.Text = st.ElecDistApprovelRef;
                            //try
                            //{
                            ddlElecRetailer.SelectedValue = st.ElecRetailerID;
                            //}
                            //catch
                            //{
                            //}
                            txtRegPlanNo.Text = st.RegPlanNo;
                            //try
                            //{
                            ddlHouseType.SelectedValue = st.HouseTypeID;
                            //}
                            //catch
                            //{
                            //}
                            //try
                            //{
                            ddlRoofType.SelectedValue = st.RoofTypeID;
                            //}
                            //catch
                            //{ }
                            //try
                            //{
                            ddlRoofAngle.SelectedValue = st.RoofAngleID;
                            //}
                            //catch
                            //{ }
                            txtLotNum.Text = st.LotNumber;
                            txtNMINumber.Text = st.NMINumber;
                            txtPeakMeterNo.Text = st.MeterNumber1;
                            txtOffPeakMeters.Text = st.MeterNumber2;
                            txtMeterPhase.Text = st.MeterPhase;
                            try
                            {
                                ddlmeterupgrade.SelectedValue = st.meterupgrade;
                            }
                            catch
                            {
                            }

                            chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
                            chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);

                            if (st.InstallPostCode != "")
                            {
                                DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                                if (dtZoneCode.Rows.Count > 0)
                                {

                                    string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                    if (dtZoneRt.Rows.Count > 0)
                                    {
                                        //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                                        //Response.End();
                                        txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                                    }
                                }
                            }

                            if (Roles.IsUserInRole("Installation Manager"))
                            {
                                if (st.ProjectStatusID == "3")
                                {
                                    if (st.ElecDistributorID == "12")
                                    {
                                        //txtApprovalRef.Enabled = false;
                                        txtNMINumber.Enabled = false;
                                    }
                                    else if (st.ElecDistributorID == "13")
                                    {
                                        //txtApprovalRef.Enabled = false;
                                        txtNMINumber.Enabled = false;
                                        txtLotNum.Enabled = false;
                                        txtRegPlanNo.Enabled = false;
                                    }
                                    else
                                    {
                                        //txtApprovalRef.Enabled = false;
                                        txtNMINumber.Enabled = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
                    {
                        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

                        try
                        {
                            ddlPanels.SelectedValue = st.PanelBrandID;
                        }
                        catch { }

                        txtWatts.Text = st.PanelOutput == "" ? "0" : st.PanelOutput;
                        hndPanelBrand.Value = st.PanelBrand;
                        try
                        {
                            txtRebate.Text = SiteConfiguration.ChangeCurrency_Val((st.RECRebate).ToString());
                        }
                        catch { }
                        txtSTCMult.Text = st.STCMultiplier;
                        txtPanelModel.Text = st.PanelModel;
                        txtNoOfPanels.Text = st.NumberPanels;
                        txtSystemCapacity.Text = st.SystemCapKW;
                        txtInstallerNotes.Text = st.InstallerNotes;
                        txtOldSystemDetails.Text = st.OldSystemDetails;

                        if (st.InverterOutput != "")
                        {
                            txtInverterOutput.Text = Convert.ToDecimal(st.InverterOutput).ToString("F");
                        }
                        else
                        {
                            txtInverterOutput.Text = "0.00";
                        }
                        //try
                        //{            
                        ddlInverter.SelectedValue = st.InverterDetailsID;
                        if (st.InverterDetailsID != "")
                        {
                            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                            if (stInverter.StockSize != "")
                            {
                                hndInverterOutput1.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                            }
                        }
                        //}
                        //catch
                        //{
                        //}
                        //txtInverterBrand.Text = st.InverterBrand;
                        txtSeries.Text = st.InverterSeries;
                        try
                        {
                            ddlSecondInverter.SelectedValue = st.SecondInverterDetailsID;
                            if (st.SecondInverterDetailsID != "")
                            {
                                SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.SecondInverterDetailsID);
                                txtInverterModel2.Text = stInverter.StockModel;
                                txtSeries2.Text = stInverter.StockSeries;
                                if (stInverter.StockSize != "")
                                {
                                    hndInverterOutput2.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                }
                            }

                            if (st.ThirdInverterDetailsID != "")
                            {
                                ddlThirdInverter.SelectedValue = st.ThirdInverterDetailsID;
                                if (st.ThirdInverterDetailsID != "")
                                {
                                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.ThirdInverterDetailsID);
                                    txtModel3.Text = stInverter.StockModel;
                                    txtSeries3.Text = stInverter.StockSeries;
                                    if (stInverter.StockSize != "")
                                    {
                                        hndInverterOutput3.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                    }
                                }
                            }

                        }
                        catch
                        {
                        }
                        txtSTC.Text = st.STCNumber;

                        DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                        if (dtSTCValue.Rows.Count > 0)
                        {
                            //try
                            //{
                            txtSTCInverter.Text = SiteConfiguration.ChangeCurrency_Val(dtSTCValue.Rows[0]["STCValue"].ToString());
                            //}
                            //catch { }
                        }

                        txtInverterModel.Text = st.InverterModel;
                        txtNoOfInverter.Text = st.inverterqty;
                        //Response.Write(st.inverterqty2);
                        //Response.End();
                        txtNoOfInverter2.Text = st.inverterqty2;
                        txtNoOfInverter3.Text = st.inverterqty3;


                        //txtInverterOutput.Text = st.InverterOutput;
                        try
                        {
                            ddlElecDist.SelectedValue = st.ElecDistributorID;
                        }
                        catch
                        {
                        }
                        //txtApprovalRef.Text = st.ElecDistApprovelRef;
                        //try
                        //{
                        ddlElecRetailer.SelectedValue = st.ElecRetailerID;
                        //}
                        //catch
                        //{
                        //}
                        txtRegPlanNo.Text = st.RegPlanNo;
                        //try
                        //{
                        ddlHouseType.SelectedValue = st.HouseTypeID;
                        //}
                        //catch
                        //{
                        //}
                        //try
                        //{
                        ddlRoofType.SelectedValue = st.RoofTypeID;
                        if (st.FlatPanels != null && st.FlatPanels != "")
                        {
                            txtFlatPanels.Text = st.FlatPanels;
                        }
                        else
                        {
                            txtFlatPanels.Text = "0";

                        }
                        if (st.PitchedPanels != null && st.PitchedPanels != "")
                        {
                            txtPitchedPanels.Text = st.PitchedPanels;
                        }
                        else
                        {
                            txtPitchedPanels.Text = "0";

                        }

                        txtPitchedPanels.Text = st.PitchedPanels;
                        //updatedate(st.RoofTypeID);
                        //}
                        //catch
                        //{ }
                        //try
                        //{
                        ddlRoofAngle.SelectedValue = st.RoofAngleID;
                        //}
                        //catch
                        //{ }
                        txtLotNum.Text = st.LotNumber;
                        txtNMINumber.Text = st.NMINumber;
                        txtPeakMeterNo.Text = st.MeterNumber1;
                        txtOffPeakMeters.Text = st.MeterNumber2;
                        txtMeterPhase.Text = st.MeterPhase;
                        try
                        {
                            ddlmeterupgrade.SelectedValue = st.meterupgrade;
                        }
                        catch
                        {
                        }


                        chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
                        chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);

                        if (st.InstallPostCode != "")
                        {
                            DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                            if (dtZoneCode.Rows.Count > 0)
                            {

                                string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                                DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                if (dtZoneRt.Rows.Count > 0)
                                {
                                    //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                                    //Response.End();
                                    txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                                }
                            }
                        }

                        if (Roles.IsUserInRole("Installation Manager"))
                        {
                            if (st.ProjectStatusID == "3")
                            {
                                if (st.ElecDistributorID == "12")
                                {
                                    //txtApprovalRef.Enabled = false;
                                    txtNMINumber.Enabled = false;
                                }
                                else if (st.ElecDistributorID == "13")
                                {
                                    //txtApprovalRef.Enabled = false;
                                    txtNMINumber.Enabled = false;
                                    txtLotNum.Enabled = false;
                                    txtRegPlanNo.Enabled = false;
                                }
                                else
                                {
                                    //txtApprovalRef.Enabled = false;
                                    txtNMINumber.Enabled = false;
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
                {
                    //divMtce.Visible = false;
                    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

                    try
                    {
                        ddlPanels.SelectedValue = st.PanelBrandID;
                    }
                    catch { }

                    txtWatts.Text = st.PanelOutput == "" ? "0" : st.PanelOutput;
                    hndPanelBrand.Value = st.PanelBrand;
                    try
                    {
                        txtRebate.Text = SiteConfiguration.ChangeCurrency_Val((st.RECRebate).ToString());
                    }
                    catch { }
                    txtSTCMult.Text = st.STCMultiplier;
                    txtPanelModel.Text = st.PanelModel;
                    txtNoOfPanels.Text = st.NumberPanels;
                    txtSystemCapacity.Text = st.SystemCapKW;
                    txtInstallerNotes.Text = st.InstallerNotes;
                    txtOldSystemDetails.Text = st.OldSystemDetails;

                    if (st.InverterOutput != "")
                    {
                        txtInverterOutput.Text = Convert.ToDecimal(st.InverterOutput).ToString("F");
                    }
                    else
                    {
                        txtInverterOutput.Text = "0.00";
                    }
                    //try
                    //{            
                    ddlInverter.SelectedValue = st.InverterDetailsID;
                    if (st.InverterDetailsID != "")
                    {
                        SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                        if (stInverter.StockSize != "")
                        {
                            hndInverterOutput1.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                        }
                    }
                    //}
                    //catch
                    //{
                    //}
                    //txtInverterBrand.Text = st.InverterBrand;
                    txtSeries.Text = st.InverterSeries;
                    try
                    {
                        ddlSecondInverter.SelectedValue = st.SecondInverterDetailsID;
                        if (st.SecondInverterDetailsID != "")
                        {
                            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.SecondInverterDetailsID);
                            txtInverterModel2.Text = stInverter.StockModel;
                            txtSeries2.Text = stInverter.StockSeries;
                            if (stInverter.StockSize != "")
                            {
                                hndInverterOutput2.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                            }
                        }

                        if (st.ThirdInverterDetailsID != "")
                        {
                            ddlThirdInverter.SelectedValue = st.ThirdInverterDetailsID;
                            if (st.ThirdInverterDetailsID != "")
                            {
                                SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(st.ThirdInverterDetailsID);

                                txtModel3.Text = stInverter.StockModel;
                                txtSeries3.Text = stInverter.StockSeries;
                                if (stInverter.StockSize != "")
                                {
                                    hndInverterOutput3.Value = Convert.ToDecimal(stInverter.StockSize).ToString("F");
                                }
                            }
                        }

                    }
                    catch
                    {
                    }
                    txtSTC.Text = st.STCNumber;

                    DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                    if (dtSTCValue.Rows.Count > 0)
                    {
                        //try
                        //{
                        txtSTCInverter.Text = SiteConfiguration.ChangeCurrency_Val(dtSTCValue.Rows[0]["STCValue"].ToString());
                        //}
                        //catch { }
                    }

                    txtInverterModel.Text = st.InverterModel;
                    if (st.inverterqty != "")
                    {
                        txtNoOfInverter.Text = st.inverterqty;
                    }
                    else
                    {
                        txtNoOfInverter.Text = "0";
                    }
                    if (st.inverterqty2 != "")
                    {
                        txtNoOfInverter2.Text = st.inverterqty2;
                    }
                    else
                    {
                        txtNoOfInverter2.Text = "0";
                    }
                    if (st.inverterqty3 != "")
                    {
                        txtNoOfInverter3.Text = st.inverterqty3;
                    }
                    else
                    {
                        txtNoOfInverter3.Text = "0";
                    }

                    try
                    {
                        ddlElecDist.SelectedValue = st.ElecDistributorID;
                    }
                    catch
                    {
                    }

                    if (st.ElecRetailerID != null && st.ElecRetailerID != "")
                    {
                        try
                        {
                            ddlElecRetailer.SelectedValue = st.ElecRetailerID;
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    txtRegPlanNo.Text = st.RegPlanNo;

                    if (st.HouseTypeID != null && st.HouseTypeID != "")
                    {
                        try
                        {
                            ddlHouseType.SelectedValue = st.HouseTypeID;
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    try
                    {
                        ddlRoofType.SelectedValue = st.RoofTypeID;
                    }
                    catch (Exception ex)
                    {

                    }

                    ddlRoofAngle.SelectedValue = st.RoofAngleID;

                    txtLotNum.Text = st.LotNumber;
                    txtNMINumber.Text = st.NMINumber;
                    txtPeakMeterNo.Text = st.MeterNumber1;
                    txtOffPeakMeters.Text = st.MeterNumber2;
                    txtMeterPhase.Text = st.MeterPhase;
                    try
                    {
                        ddlmeterupgrade.SelectedValue = st.meterupgrade;
                    }
                    catch
                    {
                    }

                    chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
                    chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);

                    if (st.InstallPostCode != "")
                    {
                        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                        if (dtZoneCode.Rows.Count > 0)
                        {

                            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                            if (dtZoneRt.Rows.Count > 0)
                            {
                                //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                                //Response.End();
                                txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                            }
                        }
                    }

                    if (st.FlatPanels != null && st.FlatPanels != "")
                    {
                        txtFlatPanels.Text = st.FlatPanels;
                    }
                    else
                    {
                        txtFlatPanels.Text = "0";

                    }
                    if (st.PitchedPanels != null && st.PitchedPanels != "")
                    {
                        txtPitchedPanels.Text = st.PitchedPanels;
                    }
                    else
                    {
                        txtPitchedPanels.Text = "0";

                    }
                    if (Roles.IsUserInRole("Installation Manager"))
                    {
                        if (st.ProjectStatusID == "3")
                        {
                            if (st.ElecDistributorID == "12")
                            {
                                //txtApprovalRef.Enabled = false;
                                txtNMINumber.Enabled = false;
                            }
                            else if (st.ElecDistributorID == "13")
                            {
                                //txtApprovalRef.Enabled = false;
                                txtNMINumber.Enabled = false;
                                txtLotNum.Enabled = false;
                                txtRegPlanNo.Enabled = false;
                            }
                            else
                            {
                                //txtApprovalRef.Enabled = false;
                                txtNMINumber.Enabled = false;
                            }
                        }
                    }

                }

            }

            if (Roles.IsUserInRole("SalesRep"))
            {
                if (stmntc.ProjectStatusID != "2")
                {
                    Pan1.Enabled = false;
                }
            }

            try
            {
                if (stmntc.oldPanelBrandId != "")
                {
                    ddlOldPanel.SelectedValue = stmntc.oldPanelBrandId;
                }
                txtOldModel.Text = stmntc.oldPanelModel;
                txtOldWatts.Text = stmntc.oldPanelOutput;
                txtOldNumberOfPanels.Text = stmntc.oldNoOfPanels == "" ? "0" : stmntc.oldNoOfPanels;
                txtOldSystemCapacity.Text = stmntc.oldSystemCapKW;

                txtOldPanelManual.Text = stmntc.oldPanelBrandManual;
                txtOldModelManual.Text = stmntc.oldPanelModelManual;
                txtOldWattsManual.Text = stmntc.oldPanelOutputManual;
                txtOldNumberOfPanelsManual.Text = stmntc.oldNoOfPanelsManual;
                txtOldSystemCapacityManual.Text = stmntc.oldSystemCapKWManual;

                if (stmntc.OldRemoveOldSystem != "")
                {
                    ddlOldRemoveOldSystem.SelectedValue = stmntc.OldRemoveOldSystem;
                }

                rqVelidateRemoveOldSystem();

                
            }
            catch (Exception ex)
            {

            }

            ddlExportDetail.SelectedValue = stmntc.ExportDetail;
            txtKwExport.Text = stmntc.KwExport;
            txtKwNonExport.Text = stmntc.KwNonExport;
            ddlGridConnectedSystem.SelectedValue = stmntc.GridConnectedSystem;
            txtFeedInTariff.Text = stmntc.FeedInTariff;
            txtApproxExpectedPaybackPeriod.Text = stmntc.ApproxExpectedPaybackPeriod;
            ExportDetailsHS();

            removeOldPanel();
        }
    }

    public void BindFinanceDetails()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);

        //Bind Finance Details
        ddlPaymentOption.SelectedValue = st.FinanceWithID;
        ddlDepositOption.SelectedValue = st2.FinanceWithDepositID;
        ddlPaymentType.SelectedValue = st.PaymentTypeID;

        if (st.ApplicationDate != "")
        {
            txtApplicationDate.Text = Convert.ToDateTime(st.ApplicationDate).ToShortDateString();
        }
        txtPurchaseNo.Text = st.PurchaseNo;

        //Bind Others Details
        txtPromoText.Text = st2.PromoText;
        ddlPromo1ID.SelectedValue = st2.Promo1ID;
        ddlPromo2ID.SelectedValue = st2.Promo2ID;
        chkPromo3.Checked = Convert.ToBoolean(st2.Promo3);

        //Bind Price Details
        if (!string.IsNullOrEmpty(st.ServiceValue))
        {
            txtBasicSystemCost.Text = SiteConfiguration.ChangeCurrency_Val(st.ServiceValue.ToString());
        }
        if (!Roles.IsUserInRole("Administrator"))
        {
            if (st.ProjectStatusID == "3")
            {
                txtBasicSystemCost.Enabled = false;
            }
            else
            {
                txtBasicSystemCost.Enabled = true;
            }
        }

        if (st.TotalQuotePrice.ToString() != "")
        {
            txtTotalCost.Text = SiteConfiguration.ChangeCurrency_Val(st.TotalQuotePrice);
        }
        if (st.DepositRequired.ToString() != "")
        {
            txtDepositRequired.Text = SiteConfiguration.ChangeCurrency_Val(st.DepositRequired);
        }

        txtBaltoPay.Text = (Convert.ToDecimal(txtTotalCost.Text) - Convert.ToDecimal(txtDepositRequired.Text)).ToString("F");

        //Bind Veriation Repeater
        #region Create Datatable For Veriation Repeater
        dtRptVeriation = new DataTable();
        dtRptVeriation.Columns.Add("VeriationID", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("VaritionValue", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("type", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("Notes", Type.GetType("System.String"));

        if (st.VarHouseType != "0.0000")
        {
            string VarHouseType = SiteConfiguration.ChangeCurrency_Val(st.VarHouseType);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "1";
            dr["VaritionValue"] = VarHouseType;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarRoofType != "0.0000")
        {
            string VarRoofType = SiteConfiguration.ChangeCurrency_Val(st.VarRoofType);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "2";
            dr["VaritionValue"] = VarRoofType;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarRoofAngle != "0.0000")
        {
            string VarRoofAngle = SiteConfiguration.ChangeCurrency_Val(st.VarRoofAngle);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "3";
            dr["VaritionValue"] = VarRoofAngle;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarMeterInstallation != "0.0000")
        {
            string VarMeterInstallation = SiteConfiguration.ChangeCurrency_Val(st.VarMeterInstallation);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "4";
            dr["VaritionValue"] = VarMeterInstallation;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarMeterUG != "0.0000")
        {
            string VarMeterUG = SiteConfiguration.ChangeCurrency_Val(st.VarMeterUG);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "5";
            dr["VaritionValue"] = VarMeterUG;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarAsbestos != "0.0000")
        {
            string VarAsbestos = SiteConfiguration.ChangeCurrency_Val(st.VarAsbestos);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "6";
            dr["VaritionValue"] = VarAsbestos;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarSplitSystem != "0.0000")
        {
            string VarSplitSystem = SiteConfiguration.ChangeCurrency_Val(st.VarSplitSystem);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "7";
            dr["VaritionValue"] = VarSplitSystem;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarCherryPicker != "0.0000")
        {
            string VarCherryPicker = SiteConfiguration.ChangeCurrency_Val(st.VarCherryPicker);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "8";
            dr["VaritionValue"] = VarCherryPicker;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarTravelTime != "0.0000")
        {
            string VarTravelTime = SiteConfiguration.ChangeCurrency_Val(st.VarTravelTime);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "9";
            dr["VaritionValue"] = VarTravelTime;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.HotWaterMeter != "" && st.HotWaterMeter != "0.0000")
        {
            string HotWaterMeter = SiteConfiguration.ChangeCurrency_Val(st.HotWaterMeter);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "10";
            dr["VaritionValue"] = HotWaterMeter;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.SmartMeter != "" && st.SmartMeter != "0.0000")
        {
            string SmartMeter = SiteConfiguration.ChangeCurrency_Val(st.SmartMeter);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "11";
            dr["VaritionValue"] = SmartMeter;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.VarOther != "0.0000")
        {
            string VarOther = SiteConfiguration.ChangeCurrency_Val(st.VarOther);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "12";
            dr["VaritionValue"] = VarOther;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.SpecialDiscount != "0.0000")
        {
            string SpecialDiscount = SiteConfiguration.ChangeCurrency_Val(st.SpecialDiscount);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "13";
            dr["VaritionValue"] = SpecialDiscount;
            dr["Notes"] = st.varSpecialDiscountNotes;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.mgrrdisc != "" && st.mgrrdisc != "0.0000")
        {
            string mgrrdisc = SiteConfiguration.ChangeCurrency_Val(st.mgrrdisc);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "14";
            dr["VaritionValue"] = mgrrdisc;
            dr["Notes"] = st.varManagerDiscountNotes;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.collectioncharge != "" && st.collectioncharge != "0.0000")
        {
            string collectioncharge = SiteConfiguration.ChangeCurrency_Val(st.collectioncharge);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "15";
            dr["VaritionValue"] = collectioncharge;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.OldSystemRemovalCost != "" && st.OldSystemRemovalCost != "0.0000")
        {
            string OldSystemRemovalCost = SiteConfiguration.ChangeCurrency_Val(st.OldSystemRemovalCost);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "16";
            dr["VaritionValue"] = OldSystemRemovalCost;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.TiltkitBrackets != "" && st.TiltkitBrackets != "0.0000")
        {
            string TiltkitBrackets = SiteConfiguration.ChangeCurrency_Val(st.TiltkitBrackets);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "17";
            dr["VaritionValue"] = TiltkitBrackets;
            dr["Notes"] = string.Empty;

            dtRptVeriation.Rows.Add(dr);
        }
        if (st.varOtherBasicCost != "" && st.varOtherBasicCost != "0.0000" && st.varOtherBasicCost != "0.00")
        {
            string varOtherBasicCost = SiteConfiguration.ChangeCurrency_Val(st.varOtherBasicCost);
            DataRow dr = dtRptVeriation.NewRow();
            dr["VeriationID"] = "18";
            dr["VaritionValue"] = varOtherBasicCost;
            dr["Notes"] = st.varOtherBasicCostNotes;

            dtRptVeriation.Rows.Add(dr);
        }

        #endregion

        if (dtRptVeriation.Rows.Count > 0)
        {
            rptVeriation.DataSource = dtRptVeriation;
            rptVeriation.DataBind();
            MaxAttribute = dtRptVeriation.Rows.Count;
            HideShowRemove();
        }
        else
        {
            MaxAttribute = 1;
            BindRepeaterVeriation();
            BindAddedAttributeRepeater();
        }
        //END Veriation Repeater

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Administrator"))
        {
            DivPriceCategory.Visible = true;
            ddlPriseCategory.SelectedValue = st.PriceCat;
        }

        BindVeriationSum();

        if (Roles.IsUserInRole("SalesRep"))
        {
            if (st.ProjectStatusID != "2")
            {
                Pan1.Enabled = false;
                PanelFinance.Enabled = false;

                //PanelPrice.Enabled = false;
                txtBasicSystemCost.Enabled = false;
                txtDepositRequired.Enabled = false;
            }
        }

        if (!Roles.IsUserInRole("Administrator"))
        {
            if (st.ProjectStatusID != "2")
            {
                txtBasicSystemCost.Enabled = false;
            }
        }
    }
    #endregion

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string CustomerID = Request.QueryString["compid"];

        string ProjectTypeID = ddlProjectTypeID.SelectedValue;
        string ProjectOpened = txtProjectOpened.Text;
        string ManualQuoteNumber = txtManualQuoteNumber.Text;
        string EmployeeID = ddlSalesRep.SelectedValue;
        string ContactID = ddlContact.SelectedValue;
        string SalesRep = ddlSalesRep.SelectedValue;

        string OldProjectNumber = ddllinkprojectid.SelectedValue;
        string Project = txtInstallCity.Text + "-" + txtInstallAddress.Text;

        string formbayUnitNo = txtformbayUnitNo.Text;
        string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;
        string InstallCity = txtInstallCity.Text;
        string InstallState = txtInstallState.Text;
        string InstallPostCode = txtInstallPostCode.Text;
        string ProjectNotes = txtProjectNotes.Text;
        string InstallerNotes = txtInstallerNotes.Text;
        string oldsystemdetail = txtOldSystemDetails.Text;

        string InstallAddress = formbayunittype + " " + formbayUnitNo + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
        string project = InstallCity + '-' + InstallAddress;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        SttblProjects stproject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        bool Flag = false;
        if (ddlDepositOption.SelectedValue != "")
        {
            if (ddlPaymentOption.SelectedValue == "" || ddlPaymentOption.SelectedValue == "1")
            {
                Flag = false;
            }
            else
            {
                Flag = true;
            }
        }
        else
        {
            Flag = true;
        }

        if (Flag)
        {
            DataTable dtaddress = new DataTable();
            if (!Roles.IsUserInRole("Administrator"))
            {
                int existcustomer = ClstblCustomers.tblCustomers_ExitsByID_Address(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
                dtaddress = ClstblCustomers.tblCustomers_tblProjects_Exits_Address_new(CustomerID, InstallAddress, InstallCity, InstallState, InstallPostCode);
            }
            if (dtaddress.Rows.Count > 0)
            {
                ModalPopupExtenderAddress.Show();
                // DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
                //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
                rptaddress.DataSource = dtaddress;
                rptaddress.DataBind();
            }
            else
            {
                //    lblerror.Visible = false;
                if (ddlSalesRep.SelectedValue.ToString() != stproject.EmployeeID)
                {
                    ClstblCustomers.tblCustomers_Update_Assignemployee(CustomerID, ddlSalesRep.SelectedValue.ToString(), stEmp.EmployeeID, DateTime.Now.AddHours(14).ToString(), Convert.ToString(true));
                    ClstblProjects.tblProjects_UpdateEmployee(CustomerID, ddlSalesRep.SelectedValue.ToString());
                    ClstblContacts.tblContacts_UpdateEmployee(CustomerID, ddlSalesRep.SelectedValue.ToString());
                }
                //Response.Write(InstallCity);
                //Response.End();
                bool sucDetail = ClsProjectSale.tblProjects_UpdateDetails(ProjectID, ContactID, EmployeeID, SalesRep, ProjectTypeID, ProjectOpened, OldProjectNumber, ManualQuoteNumber, ProjectNotes, "", "", InstallerNotes, "", InstallAddress, InstallCity, InstallState, InstallPostCode, UpdatedBy, Project);
                bool sucDetail2 = ClsProjectSale.OldSystemDetails(ProjectID, oldsystemdetail);

                bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value, project);

                bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);

                bool succ = ClstblCustomers.tblCustomer_Update_Address(CustomerID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);

                if (txtInstallerNotes.Text != string.Empty)
                {
                    int exist2 = ClsProjectSale.tblInstallerNotes_Exists(ProjectID, EmployeeID, txtInstallerNotes.Text);
                    if (exist2 == 0)
                    {
                        int success3 = ClstblInstallerNotes.tblInstallerNotes_Insert(ProjectID, EmployeeID, txtInstallerNotes.Text);
                    }
                }
                if (ddlProjectTypeID.SelectedValue == "2")
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateFormsSolar(ProjectID);
                }
                if (ddlProjectTypeID.SelectedValue == "3")
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateFormsSolarUG(ProjectID);
                }

                /* --------------------- Update STC No. --------------------- */

                decimal stcno;
                //if (stproject.InstallPostCode != string.Empty)
                //{
                //    DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stproject.InstallPostCode);
                //    if (dtZoneCode.Rows.Count > 0)
                //    {
                //        string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                //        DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                //        if (dtZoneRt.Rows.Count > 0)
                //        {
                //            string ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();

                //            stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * 15);
                //            int myInt = (int)Math.Round(stcno);
                //            ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));
                //        }
                //    }
                //}

                if (stproject.InstallPostCode != string.Empty)
                {
                    DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stproject.InstallPostCode);
                    if (dtZoneCode.Rows.Count > 0)
                    {
                        string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                        DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                        if (dtZoneRt.Rows.Count > 0)
                        {
                            string ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();

                            string installbooking = stproject.InstallBookingDate;
                            if (installbooking == null || installbooking == "")
                            {
                                DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                                String year = currentdate.Year.ToString();
                                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                                stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * stcrate);
                                int myInt = (int)Math.Round(stcno);
                                ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));

                            }
                            else
                            {
                                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                                DateTime dt1 = Convert.ToDateTime(date);
                                String year = dt1.Year.ToString();
                                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                                stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * stcrate);
                                int myInt = (int)Math.Round(stcno);
                                ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));
                            }

                        }
                    }
                }

                /* --------------------update Site Details-------------------------------------- */

                string FlatPanels = txtFlatPanels.Text.Trim();
                string PitchedPanels = txtPitchedPanels.Text.Trim();
                if (ddlRoofType.SelectedValue == "9" || ddlRoofType.SelectedValue == "7" || ddlRoofType.SelectedValue == "5" || ddlRoofType.SelectedValue == "8" || ddlRoofType.SelectedValue == "12" || ddlRoofType.SelectedValue == "13")
                {
                    int Flatpanls = 0;
                    int pitchpanel = 0;
                    if (txtFlatPanels.Text != null && txtFlatPanels.Text != null && txtPitchedPanels.Text != null && txtPitchedPanels.Text != "")
                    {
                        if ((txtFlatPanels.Text == "" || txtFlatPanels.Text == "0") && (txtPitchedPanels.Text == "" || txtPitchedPanels.Text == "0"))
                        {
                            MsgError("Roof Type Panel Qty Must GreaterThen Zero");
                            return;
                        }
                        //if (txtPitchedPanels.Text == "" || txtPitchedPanels.Text == "0")
                        //{
                        //    MsgError("Panel Qty Must GreaterThen Zero in" + ddlRoofType.SelectedItem.Text.ToString());
                        //    return;
                        //}
                        //}

                    }
                }
                else
                {
                    FlatPanels = "0";
                    PitchedPanels = "0";
                }

                if (ddlSecondInverter.SelectedValue != null && ddlSecondInverter.SelectedValue != "")
                {
                    if (ddlSecondInverter.SelectedValue == "11186")
                    {
                        txtNoOfInverter2.Text = "0";
                    }
                    else
                    {
                        if (txtNoOfInverter2.Text == "")
                        {
                            MsgError("Quantity Must GreaterThen Zero..");
                            return;
                        }
                    }
                }

                if (ddlThirdInverter.SelectedValue != null && ddlThirdInverter.SelectedValue != "")
                {
                    if (ddlThirdInverter.SelectedValue == "11186")
                    {
                        txtNoOfInverter3.Text = "0";
                    }
                    else
                    {
                        if (txtNoOfInverter3.Text == "")
                        {
                            MsgError("Quantity Must GreaterThen Zero..");
                            return;
                        }
                    }
                }

                string PanelBrandID = ddlPanels.SelectedValue; //ID
                string PanelBrand = hndPanelBrand.Value;
                string PanelOutput = txtWatts.Text;
                string RECRebate = txtRebate.Text;
                string STCMultiplier = txtSTCMult.Text;
                string STCZoneRating = txtZoneRt.Text;
                string PanelModel = txtPanelModel.Text;
                string NumberPanels = txtNoOfPanels.Text;
                string SystemCapKW = txtSystemCapacity.Text;


                string PanelDetails = "";
                if (ddlPanels.SelectedValue != string.Empty)
                {
                    SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanels.SelectedValue);
                    if (ddlPanels.SelectedValue != "")
                    {
                        PanelDetails = txtNoOfPanels.Text + " X " + txtWatts.Text + " Watt " + stPanel.StockItem + " Panels. (" + txtPanelModel.Text + ")";
                    }
                }

                string InverterDetailsID = ddlInverter.SelectedValue; //Id
                string InverterBrand = hndInverterBrand1.Value;
                string InverterSeries = txtSeries.Text;
                string SecondInverterDetailsID = ddlSecondInverter.SelectedValue;
                string ThirdInverterDetailsID = ddlThirdInverter.SelectedValue;
                string STCNumber = txtSTC.Text;

                DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                //txtSTC.Text = "0";
                if (dtSTCValue.Rows.Count > 0)
                {
                    txtSTCInverter.Text = dtSTCValue.Rows[0]["STCValue"].ToString();
                }
                string InverterModel = txtInverterModel.Text;
                string inverterqty = txtNoOfInverter.Text;
                string inverterqty2 = txtNoOfInverter2.Text;
                string inverterqty3 = txtNoOfInverter3.Text;
                string InverterOutput = txtInverterOutput.Text;

                string InverterDetails = "";
                if (ddlInverter.SelectedValue != "")
                {
                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
                    InverterDetails = "One " + stInverter.StockItem + " KW Inverter ";
                }
                if (ddlSecondInverter.SelectedValue != "")
                {
                    SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlSecondInverter.SelectedValue);
                    InverterDetails = "One " + stInverter2.StockItem + " KW Inverter";
                }
                if (ddlInverter.SelectedValue != "" && ddlSecondInverter.SelectedValue != "")
                {
                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
                    SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlSecondInverter.SelectedValue);
                    InverterDetails = "One " + stInverter.StockItem + " KW Inverter. Plus " + stInverter2.StockItem + " KW Inverter.";
                }

                string SystemDetails = "";
                if (ddlPanels.SelectedValue != "")
                {
                    SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanels.SelectedValue);
                    SystemDetails = txtNoOfPanels.Text + " X " + stPanel.StockItem;
                }

                if (ddlInverter.SelectedValue != "")
                {
                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
                    SystemDetails += " + " + stInverter.StockItem;
                }

                if (ddlSecondInverter.SelectedValue != "")
                {
                    SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlSecondInverter.SelectedValue);
                    SystemDetails += " + " + stInverter2.StockItem;
                }

                string ElecDistributorID = ddlElecDist.SelectedValue;
                string ElecDistApprovelRef = txtApprovalRef.Text;
                //string ElecDistApprovelRef = "";
                string ElecRetailerID = ddlElecRetailer.SelectedValue;
                string RegPlanNo = txtRegPlanNo.Text;
                string HouseTypeID = ddlHouseType.SelectedValue;
                string RoofTypeID = ddlRoofType.SelectedValue;
                string RoofAngleID = ddlRoofAngle.SelectedValue;
                string LotNumber = txtLotNum.Text;
                string NMINumber = txtNMINumber.Text;
                string MeterNumber1 = txtPeakMeterNo.Text;
                string MeterNumber2 = txtOffPeakMeters.Text;
                string MeterPhase = txtMeterPhase.Text;
                string MeterEnoughSpace = Convert.ToString(chkEnoughMeterSpace.Checked);
                string OffPeak = Convert.ToString(chkIsSystemOffPeak.Checked);

                //string SPAInvoiceNumber = txtSPAInvoiceNumber.Text;
                //string SPAPaid = txtSPAPaid.Text.Trim();
                //string SPAPaidBy = ddlSPAPaidBy.SelectedValue;
                //string SPAPaidAmount = txtSPAPaidAmount.Text;
                //string SPAPaidMethod = ddlSPAPaidMethod.SelectedValue;

                string ElecDistApplied = txtDistApplied.Text.Trim();
                //string ElecDistApplyMethod = ddlElecDistApplyMethod.SelectedValue;
                //string ElecDistApplySentFrom = txtElecDistApplySentFrom.Text;

                //string ElecDistApplied = "";
                string ElecDistApplyMethod = "";
                string ElecDistApplySentFrom = "";

                //string SurveyCerti = rblSurveyCerti.SelectedValue;
                //string CertiApprove = rblCertiApprove.SelectedValue;
                //string ElecDistAppDate = txtElecDistAppDate.Text.Trim();
                //string ElecDistAppBy = ddlElecDistAppBy.SelectedValue;

                string SurveyCerti = "";
                string CertiApprove = "";
                string ElecDistAppDate = "";
                string ElecDistAppBy = "";

                //string VicAppref = txtVicAppRefNo.Text;
                //string VicDate = txtVicdate.Text;
                //string VicNote = txtVicNote.Text;
                //string VicRebate = ddlvicrebate.SelectedValue;

                DataTable dtZoneCode1 = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stproject.InstallPostCode);
                if (dtZoneCode1.Rows.Count > 0)
                {
                    string STCZoneID = dtZoneCode1.Rows[0]["STCZoneID"].ToString();
                    DataTable dtZoneRt1 = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                    txtZoneRt.Text = dtZoneRt1.Rows[0]["STCRating"].ToString();
                }
                //SttblEmployees stEmp1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);

                if (txtNoOfPanels.Text != stproject.NumberPanels)
                {
                    //bool sucSalePrice = ClsProjectSale.tblProjects_UpdateSalePrice(ProjectID);

                    //add by jigar chokshi
                    //bool sucSaleTotalQuote = ClsProjectSale.tblProjects_UpdateTotalQuotePrice(ProjectID, "0");
                    int sucpanelslog = ClsProjectSale.tblPanelsUser_log_insert(ProjectID, txtNoOfPanels.Text, userid);
                }
                //Response.Write(ddlInverter2.SelectedValue + "==" + SecondInverterDetailsID);
                //Response.End();
                if (SecondInverterDetailsID == "")
                {
                    SecondInverterDetailsID = "0";
                }
                if (ThirdInverterDetailsID == "")
                {
                    ThirdInverterDetailsID = "0";
                }
                bool successSale = ClsProjectSale.tblProjects_UpdateSale(ProjectID, PanelBrandID, PanelBrand, PanelOutput, RECRebate, STCMultiplier, STCZoneRating, PanelModel, NumberPanels, SystemCapKW, PanelDetails, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, txtSTCInverter.Text, InverterModel, InverterOutput, InverterDetails, SystemDetails, ElecDistributorID, ElecDistApprovelRef, ElecRetailerID, RegPlanNo, HouseTypeID, RoofTypeID, RoofAngleID, LotNumber, NMINumber, MeterNumber1, MeterNumber2, MeterPhase, MeterEnoughSpace, OffPeak, UpdatedBy, ElecDistApplied, ElecDistApplyMethod, ElecDistApplySentFrom, FlatPanels, PitchedPanels, SurveyCerti, ElecDistAppDate, ElecDistAppBy, CertiApprove, ThirdInverterDetailsID);
                // add by roshni
                bool successSale_meterupgrade = ClsProjectSale.tblProjects_UpdateSale_meterupgrade(ProjectID, ddlmeterupgrade.SelectedValue);//

                bool successSale_inverterqty = ClsProjectSale.tblProjects_UpdateSale_inverterqty(ProjectID, inverterqty, inverterqty2, inverterqty3);

                //bool success_VicRebate = ClsProjectSale.tblProjects_UpdateSale_VicRebate(ProjectID, VicAppref, VicDate, VicNote, VicRebate);

                //bool sucQuote2 = ClsProjectSale.tblProjects2_UpdateQuote(ProjectID, SPAInvoiceNumber, SPAPaid, SPAPaidBy, SPAPaidAmount, SPAPaidMethod);

                //ClsProjectSale.tblProjects_UpdateSalesType(ProjectID, salestype);

                int Tracksuccess = ClstblTrackPanelDetails.tblTrackPanelDetails_Insert(ProjectID, DateTime.Now.ToString(), userid, PanelBrandID, PanelBrand, RECRebate, NumberPanels, PanelOutput, STCMultiplier, SystemCapKW, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, InverterModel, InverterOutput);

                //if (ddlthroughtype.SelectedValue != string.Empty)
                //{
                //    if (ddlthroughtype.SelectedValue == "1")
                //    {
                //        bool scc_update = ClsProjectSale.tblProjects_Update_InverterReplacementDetail(ProjectID, ddlthroughtype.SelectedValue, txtoldserialno.Text, txtlogdement.Text, txtlogdementdate.Text, txttrackingno.Text, txtnewserialno.Text, txterrormessage.Text, txtfaultydate.Text, ddlpickthrough.SelectedValue, txtdeliveryreceived.Text, txtsentsupplier.Text, ddlsentthrough.SelectedValue, txtinvoicesent.Text, txtinvoiceno.Text, ddlinstallerpickup.SelectedValue);
                //    }
                //    if (ddlthroughtype.SelectedValue == "2")
                //    {
                //        bool scc_update = ClsProjectSale.tblProjects_Update_InverterReplacementDetail(ProjectID, ddlthroughtype.SelectedValue, htxtoldserialno.Text, txthlogdementno.Text, txthlogdementdate.Text, "", txthnewserialno.Text, txtherrormesage.Text, "", "", txtdeliveryrec.Text, txt_hsentsupplierdate.Text, ddlhsentthrough.SelectedValue, txt_incoicesent_date.Text, txt_invoiceno.Text, "");
                //        //ddlinstallerpickup,txth_faultyunit,
                //    }
                //}

                //if (stPro.ProjectTypeID == "8")
                //{
                //    ClsProjectSale.tblProjectMaintenance_UpdateDetail(ProjectID, txtOpenDate.Text.Trim(), ddlProjectMtceReasonID.SelectedValue, ddlProjectMtceReasonSubID.SelectedValue, ddlProjectMtceCallID.SelectedValue, ddlProjectMtceStatusID.SelectedValue, Convert.ToString(chkWarranty.Checked), txtCustomerInput.Text, txtFaultIdentified.Text, txtActionRequired.Text, txtWorkDone.Text);
                //}

                #region OldPanelDetails
                string oldPanelBrandId = ddlOldPanel.SelectedValue;
                string oldPanelBrand = hndOldPanelBrand.Value;
                string oldPanelModel = txtOldModel.Text;
                string oldPanelOutput = txtOldWatts.Text;
                string oldNoOfPanels = txtOldNumberOfPanels.Text;
                string oldSystemCapKW = txtOldSystemCapacity.Text;

                bool updateOldPanelDetails = ClstblSalesinfo.Update_tblProjects_OldPanelDetails(ProjectID, oldPanelBrandId, oldPanelBrand, oldPanelModel, oldPanelOutput, oldNoOfPanels, oldSystemCapKW);

                string oldPanelManual = txtOldPanelManual.Text;
                string oldPanelModelManual = txtOldModelManual.Text;
                string oldPanelOutputManual = txtOldWattsManual.Text;
                string oldNoOfPanelsManual = txtOldNumberOfPanelsManual.Text;
                string oldSystemCapKWManual = txtOldSystemCapacityManual.Text;

                bool updateOldPanelDetailsManual = ClstblSalesinfo.Update_tblProjects_OldPanelDetailsManual(ProjectID, oldPanelManual, oldPanelModelManual, oldPanelOutputManual, oldNoOfPanelsManual, oldSystemCapKWManual);
                
                string OldRemoveOldSystem = ddlOldRemoveOldSystem.SelectedValue;

                bool updateOldRemoveOldSystem = ClstblSalesinfo.Update_tblProjects_OldRemoveOldSystem(ProjectID, OldRemoveOldSystem);
                #endregion

                SttblProjects stmntc = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
                if (stmntc.ProjectStatusID != "15")
                {
                    DataTable dt = ClsProjectSale.tblProjects_CheckProjectActiveBefore(ProjectID);
                    if (dt.Rows.Count == 0)//It would check if the project had status 'Active' before.It would allow only if status of project was never 'Active'
                    {
                        activemethod();
                    }
                }
                /*-------------------------------------------------------------------------------*/

                #region Update FinanceDetails/Price Details/Others Details
                /*---------------------------Update Finance Details--------------------------*/
                string ServiceValue = txtBasicSystemCost.Text;
                string TotalQuotePrice = txtTotalCost.Text;
                string DepositRequired = txtDepositRequired.Text;
                string FinanceWithID = ddlPaymentOption.SelectedValue;
                string PaymentTypeID = ddlPaymentType.SelectedValue;

                string PromoText = txtPromoText.Text;
                string Promo1ID = ddlPromo1ID.SelectedValue;
                string Promo2ID = ddlPromo2ID.SelectedValue;
                string Promo3 = Convert.ToString(chkPromo3.Checked);

                string InvoiceGST = "0";
                string InvoiceExGST = "0";
                if (Convert.ToDecimal(txtTotalCost.Text) > 0)
                {
                    //InvoiceGST = InvoiceTotal / 11
                    //InvoiceExGST = InvoiceTotal * 10 / 11

                    decimal gst = Convert.ToDecimal(txtTotalCost.Text) / 11;
                    InvoiceGST = Convert.ToString(gst);
                    decimal exgst = (Convert.ToDecimal(txtTotalCost.Text) * 10) / 11;
                    InvoiceExGST = Convert.ToString(exgst);
                }

                bool sucPrice2 = ClsProjectSale.tblProjects2_UpdatePrice(ProjectID, PromoText, Promo1ID, Promo2ID, Promo3);
                bool sucEx = ClsProjectSale.tblProjects_UpdateEx(ProjectID, InvoiceGST, InvoiceExGST);

                //if (ddlPaymentOption.SelectedValue != "3" || ddlDepositOption.SelectedValue != "1")
                //{
                //    if (stproject.ProjectStatusID != "2")
                //    {
                //        ClstblProjects.tblProjects_UpdateInvoiceDoc(ProjectID, stproject.ProjectNumber);
                //        int success = ClstblInvoicePayments.tblInvoicePayments_Insert(ProjectID, "0", "0", "0", Convert.ToString(DateTime.Now.AddHours(14)), "11", "0", stEmp.EmployeeID, "", "", "", "False", "0", "", "", "", "");
                //    }
                //}
                //if (ddlDepositOption.SelectedValue != "")
                //{

                //}
                ClstblProjects.tblProjects2_UpdateFinanceWithDeposit(ProjectID, ddlDepositOption.SelectedValue);
                if (stproject.ProjectStatusID == "3")
                {
                    ClsProjectSale.tblProjects_UpdatePreviousTotalQuotePrice(ProjectID, TotalQuotePrice);
                    //ClsProjectSale.tblProjects_UpdatePreviousTotalQuotePrice(ProjectID, TotalQuotePrice);
                }

                string ApplicationDate = txtApplicationDate.Text.Trim();
                string PurchaseNo = txtPurchaseNo.Text;

                bool sucFinance = ClstblSalesinfo.tblProjects_UpdateFinanceNew(ProjectID, ApplicationDate, PurchaseNo);

                #region Save Veriation
                string VarHouseType = "0.0000";
                string VarRoofType = "0.0000";
                string VarRoofAngle = "0.0000";
                string VarMeterInstallation = "0.0000";
                string VarMeterUG = "0.0000";
                string VarAsbestos = "0.0000";
                string VarSplitSystem = "0.0000";
                string VarCherryPicker = "0.0000";
                string VarTravelTime = "0.0000";
                string VarHotWaterMeter = "0.0000";
                string VarSmartMeter = "0.0000";
                string VarOther = "0.0000";
                string VarSpecialDiscount = "0.0000";
                string VarManagerDiscount = "0.0000";
                string VarCollectionCharge = "0.0000";
                string oldsystemremovalcost = "0.0000";
                string TiltkitCost = "0.0000";
                string varOtherBasicCost = "0.0000";
                string varSpecialDiscountNotes = string.Empty;
                string varManagerDiscountNotes = string.Empty;
                string varOtherBasicCostNotes = string.Empty;
                foreach (RepeaterItem item in rptVeriation.Items)
                {
                    if (item.Visible != false)
                    {
                        DropDownList ddlVeriation = (DropDownList)item.FindControl("ddlVeriation");
                        TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");
                        TextBox txtDiscountNotes = (TextBox)item.FindControl("txtNotes");

                        if (ddlVeriation.SelectedItem.Text == "House Type")
                        {
                            VarHouseType = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Roof Type")
                        {
                            VarRoofType = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Roof Angle")
                        {
                            VarRoofAngle = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Meter Installation")
                        {
                            VarMeterInstallation = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Meter Upgrade")
                        {
                            VarMeterUG = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Asbestos")
                        {
                            VarAsbestos = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Split System")
                        {
                            VarSplitSystem = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Cherry Picker")
                        {
                            VarCherryPicker = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Travel Cost")
                        {
                            VarTravelTime = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Hot Water Meter")
                        {
                            VarHotWaterMeter = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Smart Meter")
                        {
                            VarSmartMeter = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Other")
                        {
                            VarOther = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Special Discount")
                        {
                            VarSpecialDiscount = txtVariationValue.Text;
                            varSpecialDiscountNotes = txtDiscountNotes.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Manager Discount")
                        {
                            VarManagerDiscount = txtVariationValue.Text;
                            varManagerDiscountNotes = txtDiscountNotes.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Collection Charge")
                        {
                            VarCollectionCharge = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Old System Removal Cost")
                        {
                            oldsystemremovalcost = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Tilt Kit Brackets")
                        {
                            TiltkitCost = txtVariationValue.Text;
                        }
                        else if (ddlVeriation.SelectedItem.Text == "Other BasicCost")
                        {
                            varOtherBasicCost = txtVariationValue.Text;
                            varOtherBasicCostNotes = txtDiscountNotes.Text;
                        }
                    }
                }

                updateVariationLog(stproject);

                bool sucPrice = ClstblSalesinfo.tblProjects_UpdatePriceVeriation(ProjectID, FinanceWithID, PaymentTypeID, TotalQuotePrice, DepositRequired, ServiceValue, VarHouseType, VarRoofType, VarRoofAngle, VarMeterInstallation, VarMeterUG, VarAsbestos, VarSplitSystem, VarCherryPicker, VarTravelTime, VarHotWaterMeter, VarSmartMeter, VarOther, VarSpecialDiscount);

                bool succollectioncharge = ClsProjectSale.tblProjects_Updatecollectioncharge(ProjectID, VarCollectionCharge);

                bool sucprice = ClsProjectSale.tblProjects_UpdatePriceNew(ProjectID, oldsystemremovalcost, TiltkitCost);

                bool mgrdisc = ClsProjectSale.tblprojects_Update_mgrrdiscdatetime_Amt(ProjectID, VarManagerDiscount, DateTime.Now.AddHours(14).ToString());

                bool Update_OtherBasicCost = ClstblSalesinfo.tblProjects_Update_varOtherBasicCost(ProjectID, varOtherBasicCost);

                bool UpdatePriceCat = ClstblSalesinfo.tblProjects_Update_PriceCat(ProjectID, ddlPriseCategory.SelectedValue);

                bool UpdateDiscountNotes = ClstblSalesinfo.tblProjects_Update_DiscountNotes(ProjectID, varSpecialDiscountNotes, varManagerDiscountNotes, varOtherBasicCostNotes);
                //Manager Discount Log Save


                #endregion
                /*-----------------------------------END--------------------------------------*/
                #endregion


                #region House And Application Details
                bool success_House = ClstblProjects.tblProjects_UpdateHouseStays(ProjectID, ddlHouseStatus.SelectedValue, txtCompletionDate.Text.Trim());
                #endregion

                #region Retailer Details
                string ExportDetail = ddlExportDetail.SelectedValue;
                string KwExport = txtKwExport.Text.Trim();
                string KwNonExport = txtKwNonExport.Text.Trim();
                string GridConnectedSystem = ddlGridConnectedSystem.SelectedValue;
                string FeedInTariff = txtFeedInTariff.Text.Trim();
                string ApproxExpectedPaybackPeriod = txtApproxExpectedPaybackPeriod.Text.Trim();

                bool success_RetailerDetails = ClstblProjects.tblProjects_UpdateRetailerDetails(ProjectID, ExportDetail, KwExport, KwNonExport, GridConnectedSystem, FeedInTariff, ApproxExpectedPaybackPeriod);
                #endregion


                //Update DoB for NSW Customer
                if (stproject.InstallState == "NSW")
                {
                    bool UpdateDob = ClstblSalesinfo.tblProjects_UpdateDOBbyProjectID(ProjectID, txtDoB.Text);
                }

                //--- do not chage this code Start------
                if (sucDetail)
                {
                    SetAdd1();
                    //PanSuccess.Visible = true;
                }
                else
                {
                    SetError1();
                    //PanError.Visible = true;
                }

            }
            BindCommonDetails();
            BindQuoteDetails();
            BindGrid(0);
            BindTitle();
        }
        else
        {
            MsgError("Please Select Finance Option");
        }

    }

    public void activemethod()//put by roshni
    {
        string ProjectID = Request.QueryString["proid"];

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (stPro.ElecDistributorID == "13")
        {
            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                //Response.Write(stPro.DocumentVerified);
                //Response.End();
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    //Response.Write(stPro.DocumentVerified );
                    //Response.End();
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {

                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    //else if (stPro.InstallState == "NSW")
                    //{
                    //    if (!string.IsNullOrEmpty(stPro.DOB))
                    //    {
                    //        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    //    }
                    //}
                    else
                    {

                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {

                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    //else if (stPro.InstallState == "NSW")
                    //{
                    //    if (!string.IsNullOrEmpty(stPro.DOB))
                    //    {
                    //        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    //    }
                    //}
                    else
                    {

                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.RegPlanNo == string.Empty || stPro.LotNumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
            }
        }
        else if (stPro.ElecDistributorID == "12")
        {

            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    //else if (stPro.InstallState == "NSW")
                    //{
                    //    if (!string.IsNullOrEmpty(stPro.DOB))
                    //    {
                    //        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    //    }
                    //}
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    //else if (stPro.InstallState == "NSW")
                    //{
                    //    if (!string.IsNullOrEmpty(stPro.DOB))
                    //    {
                    //        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    //    }
                    //}
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
            }
        }
        else
        {
            if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                //else if (stPro.InstallState == "NSW")
                //{
                //    if (!string.IsNullOrEmpty(stPro.DOB))
                //    {
                //        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                //    }
                //}
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            else if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.ActiveDate != string.Empty)
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                //else if (stPro.InstallState == "NSW")
                //{
                //    if (!string.IsNullOrEmpty(stPro.DOB))
                //    {
                //        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                //    }
                //}
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
        }

    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        //Reset();
        HidePanels();
        //PanSuccess.Visible = true;
        SetAdd1();
        //PanSearch.Visible = true;
    }

    public void MsgSendSuccess()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunMsgSendSuccess();", true);
    }

    public void MsgBrochureNotAvaialble()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunBrochuresDoesNotExist();", true);
    }

    public void MsgInfo(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyBluefun('{0}');", msg), true);
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(UpdatePanelGrid, this.GetType(), "MyAction", "checkopen();", true);
        ScriptManager.RegisterStartupScript(UpdatePanelGrid, this.GetType(), "MyAction", "doMyAction();", true);

        ScriptManager.RegisterStartupScript(UpdatePanelQuote, this.GetType(), "MyAction", "checkopen();", true);
        ScriptManager.RegisterStartupScript(UpdatePanelQuote, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btnYesAddress_Click(object sender, EventArgs e)
    {
        //string companyId = Request.QueryString["compid"];
        //Response.Redirect("/admin/adminfiles/company/company.aspx?companyid=" + companyId);
    }

    protected void ddlPanels_SelectedIndexChanged(object sender, EventArgs e)
    {
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st2.InstallPostCode);
        string ZoneRt = "0";
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();
        }

        if (ddlPanels.SelectedValue != string.Empty)
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanels.SelectedValue);
            //txtPanelBrand.Text = st.StockManufacturer;
            txtPanelModel.Text = st.StockModel;
            txtWatts.Text = st.StockSize == "" ? "0" : st.StockSize;
            hndPanelBrand.Value = st.StockManufacturer;

            if (!string.IsNullOrEmpty(st.StockSize) && !string.IsNullOrEmpty(txtNoOfPanels.Text))
            {
                txtSystemCapacity.Text = ((Convert.ToDecimal(st.StockSize) * Convert.ToDecimal(txtNoOfPanels.Text)) / 1000).ToString();
            }

            txtSTC.Text = "1";
            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                try
                {
                    txtSTC.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                }
                catch { }
            }
            capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * Convert.ToDecimal(txtWatts.Text)) / 1000;

            string installbooking = stPro.InstallBookingDate;
            if (installbooking == null || installbooking == "")
            {
                DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                String year = currentdate.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(ZoneRt) * stcrate);

                //-------------------------------------------------------------------
                //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //---------------------------------------------------------------------
            }
            else
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(ZoneRt) * stcrate);

            }

            int myInt = (int)Math.Floor(stcno);
            txtSTC.Text = Convert.ToString(myInt);
            rebate = myInt * Convert.ToDecimal(txtSTC.Text);
            txtRebate.Text = Convert.ToString(rebate);
        }
        else
        {
            //txtPanelBrand.Text = string.Empty;
            txtPanelModel.Text = string.Empty;
            txtWatts.Text = "0";
            txtNoOfPanels.Text = "0";
            txtSystemCapacity.Text = "0";
            txtSTC.Text = string.Empty;
        }
    }

    protected void txtNoOfPanels_TextChanged(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stPro.InstallPostCode);
        string ZoneRt = "0";
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();
        }

        if (txtNoOfPanels.Text != string.Empty)
        {
            capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * Convert.ToDecimal(txtWatts.Text)) / 1000;
            txtSystemCapacity.Text = Convert.ToString(capacity);

            string installbooking = stPro.InstallBookingDate;
            if (installbooking == null || installbooking == "")
            {
                DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                String year = currentdate.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(ZoneRt) * stcrate);

                //-------------------------------------------------------------------
                //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //---------------------------------------------------------------------
            }
            else
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((capacity) * Convert.ToDecimal(ZoneRt) * stcrate);
            }
            //stcno = ((capacity) * Convert.ToDecimal(ZoneRt) * 13);
            int myInt = (int)Math.Floor(stcno);
            txtSTC.Text = Convert.ToString(myInt);
            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            rebate = myInt * Convert.ToDecimal(txtSTCInverter.Text);
            txtRebate.Text = rebate.ToString("F");
        }
    }

    protected void ddlInverter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlInverter.SelectedValue != "")
        {
            BindInverterDetails(ddlInverter.SelectedValue, txtInverterModel, txtSeries, hndInverterOutput1);
        }
        else
        {
            txtInverterModel.Text = string.Empty;
            txtSeries.Text = string.Empty;
            hndInverterOutput1.Value = "0.00";
        }

        txtInverterOutput.Text = (Convert.ToDecimal(hndInverterOutput1.Value) + Convert.ToDecimal(hndInverterOutput2.Value) + Convert.ToDecimal(hndInverterOutput3.Value)).ToString("F");
    }

    protected void ddlSecondInverter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSecondInverter.SelectedValue != "")
        {
            BindInverterDetails(ddlSecondInverter.SelectedValue, txtInverterModel2, txtSeries2, hndInverterOutput2);
        }
        else
        {
            txtInverterModel2.Text = string.Empty;
            txtSeries2.Text = string.Empty;
            hndInverterOutput2.Value = "0.00";
        }

        txtInverterOutput.Text = (Convert.ToDecimal(hndInverterOutput1.Value) + Convert.ToDecimal(hndInverterOutput2.Value) + Convert.ToDecimal(hndInverterOutput3.Value)).ToString("F");

    }

    protected void ddlThirdInverter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlThirdInverter.SelectedValue != "")
        {
            BindInverterDetails(ddlSecondInverter.SelectedValue, txtModel3, txtSeries3, hndInverterOutput3);
        }
        else
        {
            txtModel3.Text = string.Empty;
            txtSeries3.Text = string.Empty;
            hndInverterOutput3.Value = "0.00";
        }

        txtInverterOutput.Text = (Convert.ToDecimal(hndInverterOutput1.Value) + Convert.ToDecimal(hndInverterOutput2.Value) + Convert.ToDecimal(hndInverterOutput3.Value)).ToString("F");
    }

    public void BindInverterDetails(string InverterID, TextBox Model, TextBox Series, HiddenField InverterOutput)
    {
        SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterID);
        if (ddlInverter.SelectedValue != string.Empty)
        {
            Model.Text = st.StockModel;
            Series.Text = st.StockSeries;
            //txtInverterOutput.Text = (Convert.ToDecimal(txtInverterOutput.Text) + Convert.ToDecimal(st.StockSize)).ToString("F");
            InverterOutput.Value = Convert.ToDecimal(st.StockSize).ToString("F");
        }
        else
        {
            Model.Text = "";
            Series.Text = "";
            InverterOutput.Value = "0.00";
        }
    }

    protected void lnkCalculate_Click(object sender, EventArgs e)
    {
        BindVeriationSum();
    }

    protected void txtVariationValue_TextChanged(object sender, EventArgs e)
    {
        BindVeriationSum();
        //string TotalCost = txtBasicSystemCost.Text;
        //foreach (RepeaterItem item in rptVeriation.Items)
        //{
        //    if (item.Visible != false)
        //    {
        //        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlVeriation");
        //        TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");

        //        if (ddlStockItem.SelectedItem.Text == "Special Discount" || ddlStockItem.SelectedItem.Text == "Manager Discount")
        //        {
        //            TotalCost = (Convert.ToDecimal(TotalCost) - Convert.ToDecimal(txtVariationValue.Text)).ToString("F");
        //        }
        //        else
        //        {
        //            TotalCost = (Convert.ToDecimal(TotalCost) + Convert.ToDecimal(txtVariationValue.Text)).ToString("F");
        //        }
        //    }
        //}

        //txtTotalCost.Text = TotalCost;
        //txtBaltoPay.Text = (Convert.ToDecimal(TotalCost) - Convert.ToDecimal(txtDepositRequired.Text)).ToString("F");
    }

    public void BindVeriationSum()
    {
        string TotalCost = txtBasicSystemCost.Text;
        foreach (RepeaterItem item in rptVeriation.Items)
        {
            if (item.Visible != false)
            {
                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlVeriation");
                TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");

                if (ddlStockItem.SelectedItem.Text == "Special Discount" || ddlStockItem.SelectedItem.Text == "Manager Discount")
                {
                    TotalCost = (Convert.ToDecimal(TotalCost) - Convert.ToDecimal(txtVariationValue.Text)).ToString("F");
                }
                else
                {
                    TotalCost = (Convert.ToDecimal(TotalCost) + Convert.ToDecimal(txtVariationValue.Text)).ToString("F");
                }
            }
        }
        txtTotalCost.Text = TotalCost;
        txtBaltoPay.Text = (Convert.ToDecimal(TotalCost) - Convert.ToDecimal(txtDepositRequired.Text)).ToString("F");
    }

    protected void txtBasicSystemCost_TextChanged(object sender, EventArgs e)
    {
        BindVeriationSum();
        //string TotalCost = txtBasicSystemCost.Text;
        //foreach (RepeaterItem item in rptVeriation.Items)
        //{
        //    if (item.Visible != false)
        //    {
        //        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlVeriation");
        //        TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");

        //        if (ddlStockItem.SelectedItem.Text == "Special Discount" || ddlStockItem.SelectedItem.Text == "Manager Discount")
        //        {
        //            TotalCost = (Convert.ToDecimal(TotalCost) - Convert.ToDecimal(txtVariationValue.Text)).ToString("F");
        //        }
        //        else
        //        {
        //            TotalCost = (Convert.ToDecimal(TotalCost) + Convert.ToDecimal(txtVariationValue.Text)).ToString("F");
        //        }
        //    }
        //}

        //txtTotalCost.Text = TotalCost;
        //txtBaltoPay.Text = (Convert.ToDecimal(TotalCost) - Convert.ToDecimal(txtDepositRequired.Text)).ToString("F");
    }

    protected void txtDepositRequired_TextChanged(object sender, EventArgs e)
    {
        txtBaltoPay.Text = (Convert.ToDecimal(txtTotalCost.Text) - Convert.ToDecimal(txtDepositRequired.Text)).ToString("F");
    }

    #region Quote Tab Code
    protected void BindQuoteTabDropDown()
    {
        DataTable dt = ClstblSalesinfo.tbl_UploadDocumentList_GetActiveDocument();
        ddlDocument.DataSource = dt;
        ddlDocument.DataTextField = "Document";
        ddlDocument.DataValueField = "ID";
        ddlDocument.DataBind();
        ddlDocument.SelectedValue = "1";
    }

    public void BindQuoteDetails()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);

        BindExportControl(ProjectID);
        BindFeedInTariff(ProjectID);
        BindQuote(ProjectID);
        BindDocument(ProjectID);
        BindPaymentDetails(ProjectID);

        if (!Roles.IsUserInRole("SalesRep"))
        {
            DivMainInvoiceDetails.Visible = true;
        }
        else
        {
            if (st.PdfFileNaame == "")
            {
                //DivMainInvoiceDetails.Visible = false;
                PanBtnQuote.Enabled = false;
                //PanQuote.Enabled = false;
            }
            else
            {
                DivMainInvoiceDetails.Visible = true;
                PanQuote.Enabled = true;

                if (st.PanelBrandID != "" && st.InverterDetailsID != "" && st.TotalQuotePrice != "" && st.TotalQuotePrice != "0.0000")
                {
                    PanBtnQuote.Enabled = true;
                }
                else
                {
                    PanBtnQuote.Enabled = false;
                }
            }
        }

        if (st.QuoteSent != "")
        {
            txtQuoteSent.Text = Convert.ToDateTime(st.QuoteSent).ToShortDateString();
        }

        if (st.QuoteAccepted != "")
        {
            txtQuoteAcceptedQuote.Text = Convert.ToDateTime(st.QuoteAccepted).ToShortDateString();
        }
        else
        {
            DataTable dt = ClstblSalesinfo.tbl_SignatureLog_GetSignedDateByProjectID(ProjectID);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Signed_Date"].ToString() != "")
                {
                    txtQuoteAcceptedQuote.Text = Convert.ToDateTime(dt.Rows[0]["Signed_Date"].ToString()).ToShortDateString();
                }
            }
        }

        chkSignedQuote.Checked = Convert.ToBoolean(st.SignedQuote);
        if (st.SQ != string.Empty)
        {
            divSQ.Visible = true;
            lblSQ.Visible = true;
            lblSQ.Text = st.SQ;
            //lblSQ.NavigateUrl = "~/userfiles/SQ/" + st.SQ;
            //lblSQ.NavigateUrl = pdfURL + "SQ/" + st.SQ;
            lblSQ.NavigateUrl = SiteConfiguration.GetDocumnetPath("SQ", st.SQ);
            //   RequiredFieldValidatorSQ.Visible = false;
        }
        else
        {
            divSQ.Visible = false;
            lblSQ.Visible = false;
        }

        #region Bind Invoice Details
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);

        string totalprice = st.TotalQuotePrice;
        decimal TotalPrice = 0;
        if (totalprice != string.Empty)
        {
            TotalPrice = Convert.ToDecimal(totalprice);
        }
        decimal totalpay = 0;
        if (dtCount.Rows.Count > 0)
        {
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                totalpay = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            }
        }
        if (TotalPrice == 0)
        {
            lblPaymentStatus.Text = "";
            lblPaymentStatusInstall.Text = "";
        }
        else
        {
            if (TotalPrice - totalpay > 0)
            {
                lblPaymentStatus.Text = "Owing";
                lblPaymentStatusInstall.Text = "Owing";
            }
            else
            {
                lblPaymentStatus.Text = "Fully Paid";
                lblPaymentStatusInstall.Text = "Fully Paid";
            }
        }

        if (st.TotalQuotePrice != "")
        {
            txtTotalCostInvoice.Text = SiteConfiguration.ChangeCurrency_Val(st.TotalQuotePrice);
            txtTotalCostInvoiceInstall.Text = SiteConfiguration.ChangeCurrency_Val(st.TotalQuotePrice);
        }

        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotalWithOutBakVerified"].ToString() != "")
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotalWithOutBakVerified"].ToString());
            balown = Convert.ToDecimal(st.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                txtPaidDate.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(paiddate));
                txtPaidDateInstall.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(paiddate));
            }
            catch { }
            try
            {
                txtBalOwing.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
                txtBalOwingInstall.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            txtPaidDate.Text = "0.00";
            txtPaidDateInstall.Text = "0.00";
            try
            {
                txtBalOwing.Text = SiteConfiguration.ChangeCurrency_Val(st.TotalQuotePrice);
                txtBalOwingInstall.Text = SiteConfiguration.ChangeCurrency_Val(st.TotalQuotePrice);
            }
            catch { }
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            ddlRecBy.Enabled = true;
            ddlRecByInstall.Enabled = true;
        }

        #endregion

        //Bind Active Date/ Deposite date/ Ready To active
        if (st.DepositReceived != "")
        {
            txtDepositReceived.Text = Convert.ToDateTime(st.DepositReceived).ToShortDateString();
        }

        lblLastActiveEmp.Text = st.lastActiveEmp;
        if (st.ActiveDate != "")
        {
            txtActiveDate.Text = Convert.ToDateTime(st.ActiveDate).ToShortDateString();
        }

        if (st.readyactive.ToString() == "1")
        {
            chkactive.Checked = true;
        }
        else
        {
            chkactive.Checked = false;
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            // ImageDepRec.Visible = true;
            txtDepositReceived.Enabled = true;
        }
        else
        {
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PreInstaller"))
            {
                if (st2.FinanceWithDepositID == "1")
                {
                    txtDepositReceived.Enabled = true;
                    //  ImageDepRec.Visible = true;

                    if (st.InvoiceNumber == string.Empty)
                    {
                        //   ImageDepRec.Visible = false;
                        txtDepositReceived.Enabled = false;
                    }
                    else
                    {
                        if (st.PdfFileNaame != "")
                        {
                            txtDepositReceived.Enabled = true;
                        }
                        else
                        {
                            txtDepositReceived.Enabled = false;
                        }

                        //   ImageDepRec.Visible = true;
                    }
                }
                else
                {
                    DataTable dtDR = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
                    if (dtDR.Rows.Count > 0)
                    {
                        //txtDepositReceived.Enabled = true;
                        if (st.PdfFileNaame != "")
                        {
                            txtDepositReceived.Enabled = true;
                        }
                        else
                        {
                            txtDepositReceived.Enabled = false;
                        }
                        //   ImageDepRec.Visible = true;
                    }
                    else
                    {
                        //   ImageDepRec.Visible = false;
                        txtDepositReceived.Enabled = false;
                    }
                }
            }
            else
            {
                //ImageDepRec.Visible = false;
                txtDepositReceived.Enabled = false;
            }

        }

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {
            if (st.ProjectStatusID == "5")
            {

                PanAdd.Enabled = false;
                PanelQuoteDetails.Enabled = false;

                PanInvoiceDetails.Enabled = false;
                //PanelActiveDate.Enabled = false;

                //PanMainQuote.Enabled = false;
                PanBtnQuote.Enabled = false;
                PanQuote.Enabled = false;
                PanelDocument.Enabled = false;
                //btnOpenInvoice.Enabled = false;
                //btnPrintReceipt.Enabled = false;

                txtDepositReceived.Enabled = false;
                txtActiveDate.Enabled = false;
                chkactive.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            if (st.ProjectStatusID == "3" || st.ProjectStatusID == "10" || st.ProjectStatusID == "11" || st.ProjectStatusID == "12" || st.ProjectStatusID == "13" || st.ProjectStatusID == "14" || st.ProjectStatusID == "19" || st.ProjectStatusID == "5")
            {
                PanAdd.Enabled = false;
                PanelQuoteDetails.Enabled = false;
                PanInvoiceDetails.Enabled = false;
                //PanelActiveDate.Enabled = false;
                //PanMainQuote.Enabled = false;
                PanBtnQuote.Enabled = false;
                PanQuote.Enabled = false;
                PanelDocument.Enabled = false;
                //btnOpenInvoice.Enabled = false;
                //btnPrintReceipt.Enabled = false;
                txtDepositReceived.Enabled = false;
                txtActiveDate.Enabled = false;
                chkactive.Enabled = false;
            }

            //if (st.ProjectStatusID == "8")
            //{
            //    Pan1.Enabled = false;
            //    PanelPrice.Enabled = false;
            //}
        }

        if (Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            if (st.ActiveDate != string.Empty)
            {
                txtActiveDate.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("SalesRep"))
        {
            PanMainQuote.Enabled = true;
            //PanelActiveDate.Enabled = false;

            txtDepositReceived.Enabled = false;
            txtActiveDate.Enabled = false;
            chkactive.Enabled = false;

            bool Flag = false;

            if (st.SignedQuote != "False" && st.DepositReceived != string.Empty && st.QuoteAccepted != string.Empty)
            {
                if (st.ElecDistributorID == "12")
                {
                    if (st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.meterupgrade != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            Flag = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                Flag = true;
                            }
                            else
                            {
                                Flag = false;
                            }
                        }
                    }
                    else
                    {
                        Flag = false;
                    }
                }
                else if (st.ElecDistributorID == "13")
                {
                    if (st.RegPlanNo != string.Empty && st.LotNumber != string.Empty && st.ElecDistApprovelRef != string.Empty && st.meterupgrade != string.Empty && st.NMINumber != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            Flag = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                Flag = true;
                            }
                            else
                            {
                                Flag = false;
                            }
                        }
                    }
                    else
                    {
                        Flag = false;
                    }
                }
                else
                {
                    if (st.ElecDistApprovelRef != string.Empty && st.meterupgrade != string.Empty && st.NMINumber != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            Flag = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                Flag = true;
                            }
                            else
                            {
                                Flag = false;
                            }
                        }
                    }
                    else
                    {
                        Flag = false;
                    }
                }
            }
            else
            {
                Flag = false;
            }

            if (Flag == true)
            {
                chkactive.Enabled = true;
            }
            else
            {
                chkactive.Enabled = false;
            }
            //if (st.InstallState == "VIC")
            //{
            //    if (!string.IsNullOrEmpty(st.VicAppReference))
            //    {
            //        chkactive.Enabled = true;
            //    }
            //    else
            //    {
            //        chkactive.Enabled = false;
            //    }
            //}
            //else
            //{
            //    VicAppRefference.Visible = false;
            //    lblVicAppRefference.Visible = false;
            //}
        }

        if (Roles.IsUserInRole("Sales Manager"))
        {
            PanInvoiceDetails.Enabled = true;
        }

        if (!Roles.IsUserInRole("Administrator"))
        {
            if (txtDepositReceived.Text == "")
            {
                txtActiveDate.Enabled = false;
            }
            else
            {
                txtActiveDate.Enabled = true;
            }
        }

        if (!Roles.IsUserInRole("Administrator"))
        {
            if (st.ProjectTypeID == "3")
            {
                if(st.OldRemoveOldSystem != "2")
                {
                    if (st.oldPanelBrandId == "" || st.oldNoOfPanels == "0")
                    {
                        if (st.oldPanelBrandManual == "" || (st.oldNoOfPanelsManual == "0" || st.oldNoOfPanelsManual == ""))
                        {
                            //txtDepositReceived.Enabled = false;

                            txtActiveDate.Enabled = false;
                        }
                    }
                }
                
            }
        }

        if (Roles.IsUserInRole("SalesRep"))
        {
            txtActiveDate.Enabled = false;
            chkactive.Enabled = false;
            txtDepositReceived.Enabled = false;
        }
    }

    public void BindQuote(string ProjectID)
    {
        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(ProjectID);
        if (dtQuote.Rows.Count > 0)
        {
            divQuotes.Visible = true;
            rptQuote.DataSource = dtQuote;
            rptQuote.DataBind();
            if (dtQuote.Rows.Count > 5)
            {
                divquo.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divquo.Attributes.Add("style", "");
            }

            divQuotesDoc.Visible = true;
            rptQuoteDoc.DataSource = dtQuote;
            rptQuoteDoc.DataBind();
            if (dtQuote.Rows.Count > 5)
            {
                divquoDoc.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divquoDoc.Attributes.Add("style", "");
            }
        }
        else
        {
            divQuotes.Visible = false;
            divQuotesDoc.Visible = false;
        }
    }

    public void BindDocument(string ProjectID)
    {
        DataTable dtDocument = ClstblSalesinfo.tblProjects_GetDocDetailsbyProjectID(ProjectID);
        if (dtDocument.Rows.Count > 0)
        {
            DivDocumentView.Visible = true;
            rptDocumemt.DataSource = dtDocument;
            rptDocumemt.DataBind();
            if (dtDocument.Rows.Count > 4)
            {
                divDocRpt.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divDocRpt.Attributes.Add("style", "");
            }

            DivDocumentViewDoc.Visible = true;
            rptDocumemtDoc.DataSource = dtDocument;
            rptDocumemtDoc.DataBind();
            if (dtDocument.Rows.Count > 4)
            {
                divDocRptDoc.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divDocRptDoc.Attributes.Add("style", "");
            }
        }
        else
        {
            DivDocumentView.Visible = false;
            DivDocumentViewDoc.Visible = false;
        }
    }

    public void BindPaymentDetails(string ProjectID)
    {
        TextBox2.Text = ProjectID;
        //DataTable dt_inv = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
        DataTable dt_inv = ClstblSalesinfo.tblInvoicePayments_SalesInfo_SelectByProjectID(ProjectID);

        if (dt_inv.Rows.Count > 0)
        {
            divPaymentDetails.Visible = true;
            divPaymentDetailsInstall.Visible = true;
            rptPaymentDetails.DataSource = dt_inv;
            rptPaymentDetails.DataBind();
            rptPaymentDetailsInstall.DataSource = dt_inv;
            rptPaymentDetailsInstall.DataBind();
            if (dt_inv.Rows.Count > 5)
            {
                divPayment.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
                divPaymentInstall.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divPayment.Attributes.Add("style", "");
                divPaymentInstall.Attributes.Add("style", "");
            }

            TextBox2.Text = dt_inv.Rows.Count.ToString();
            TextBox4.Text = dt_inv.Rows.Count.ToString();
        }
        else
        {
            divPaymentDetails.Visible = false;
            divPaymentDetailsInstall.Visible = false;
            TextBox2.Text = "0";
            TextBox4.Text = "0";
        }
    }

    protected void chkSignedQuote_CheckedChanged(object sender, EventArgs e)
    {
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (chkSignedQuote.Checked == true)
        {
            divSQ.Visible = true;
            //if (st.SQ == string.Empty)
            //{
            //    //RequiredFieldValidatorSQ.Visible = true;
            //}
            //else
            //{
            //    // RequiredFieldValidatorSQ.Visible = false;
            //}
        }
        else
        {
            divSQ.Visible = false;
            //RequiredFieldValidatorSQ.Visible = false;
        }
        BindScript();
    }
    #endregion

    protected void lnksendmessage_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        string ProjectNumber = st.ProjectNumber;
        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        if (dtQuote.Rows.Count > 0)
        {
            string Quotepdfno = dtQuote.Rows[0]["ProjectQuoteDoc"].ToString();

            int success = Clstbl_TokenData.tbl_TokenData_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, Quotepdfno, SignFlag);

            if (success > 0)
            {
                #region Send Sms
                SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);


                if (st2.ContMobile.Substring(0, 1) == "0")
                {
                    st2.ContMobile = st2.ContMobile.Substring(1);
                }
                Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenData_SelectById(success.ToString());
                st2.ContMobile = "+61" + st2.ContMobile;
                string mobileno = st2.ContMobile;      // "+919979156818";  //contactno;
                string OwnerName = st2.ContFirst + " " + st2.ContLast;
                string messagetext = "Hello " + OwnerName + ",\nPlease open this URL to upload your signature on DocNo: " + sttok.QDocNo + ":\n" + Siteurl + "Signature.aspx?proid=" + ProjectID + "&rt=" + Token + "\n\n Arise Solar.";

                if (mobileno != string.Empty && messagetext != string.Empty)
                {
                    string AccountSid = "AC991f3f103f6a5f608278c1e283f139a1";
                    string AuthToken = "be9c00d8443b360082ab02d3d78969b2";
                    var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);//919879111739  +61451831980
                    var message = twilio.SendMessage("+61418671214", mobileno, messagetext);
                    //var message = twilio.SendMessage("+61418671214", "+61402262170", messagetext);
                    //Response.Write(message.Sid);

                    if (message.RestException == null)
                    {

                        // status = "success";
                        MsgSendSuccess();
                    }
                    else
                    {
                        string Errormsg = message.RestException.Message;
                        // status = "error";
                        MsgError(Errormsg.ToString());
                        //SetError1();
                    }

                }

                #endregion
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }
    }

    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        string ProjectNumber = st.ProjectNumber;
        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        if (dtQuote.Rows.Count > 0)
        {
            string Quotepdfno = dtQuote.Rows[0]["ProjectQuoteDoc"].ToString();


            int success = Clstbl_TokenData.tbl_TokenData_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, Quotepdfno, SignFlag);

            if (success > 0)
            {
                #region SendMail

                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                String Subject = "Signature Request - " + ConfigurationManager.AppSettings["SiteName"].ToString();

                Server.Execute("~/mailtemplate/SignatureLinkMail.aspx?proid=" + ProjectID + "&rt=" + Token, txtWriter);

                Utilities.SendMail(from, stContact.ContEmail, Subject, txtWriter.ToString());

                //  Utilities.SendMail(from, "nilesh.rai@meghtechnologies.com", Subject, txtWriter.ToString());

                //string filepath = "http://localhost:57341//userfiles//quotedoctest//6558Quotation.pdf";
                //Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath);
                try
                {
                    MsgSendSuccess();
                }
                catch
                {
                }

                #endregion

            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }
    }

    protected void lnksendbrochures_Click(object sender, EventArgs e)
    {
        try
        {
            string ProjectID = Request.QueryString["proid"];

            #region SendMail
            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "AriseSolar Quote";

            Server.Execute("~/mailtemplate/BrochureAttachedMail.aspx?proid=" + ProjectID, txtWriter);

            string[] filepath = new string[10];
            string[] PDFName = new string[10];
            int FileCount = 0;
            int exist = 0;

            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

            if (!string.IsNullOrEmpty(st.PanelBrandID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.PanelBrandID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        exist++;
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);

                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.InverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        exist++;
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.SecondInverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.SecondInverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.ThirdInverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        //filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        filepath[FileCount] = SiteConfiguration.GetDocumnetPath("StockItemPDF", st1.StockItemID + "_" + st1.FileName);
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }

            int QuoteDocNo = ClstblProjects.tblProjectQuotes_SelectTOPDocNoByProjectID(ProjectID);
            if (QuoteDocNo > 0)
            {
                try
                {
                    exist++;
                    PDFName[FileCount] = st.ProjectNumber + "_Quotation.pdf";
                    //filepath[FileCount] = pdfURL + "quotedoc/" + QuoteDocNo + "Quotation.pdf";
                    filepath[FileCount] = SiteConfiguration.GetDocumnetPath("quotedoc", QuoteDocNo + "Quotation.pdf");
                    FileCount++;
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }

            }

            if (exist == 3)//It sends mail only when one panel,one inverter and quote all 3 pdfs are available.
            {
                if (!string.IsNullOrEmpty(st.SalesRep))
                {
                    SttblEmployees stemploy = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.SalesRep);
                    Utilities.SendMailWithMultipleAttachmentWithReplyList(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName, FileCount, stemploy.EmpEmail, stemploy.EmpFirst + " " + stemploy.EmpLast);
                }
                else
                {
                    Utilities.SendMailWithMultipleAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName, FileCount);
                }
                //Utilities.SendMailWithMultipleAttachmentWithReplyList(from, "kiran.sawant@meghtechnologies.com", Subject, txtWriter.ToString(), filepath, PDFName, FileCount, "kiran.sawant@meghtechnologies.com", "Kiran Sawant");
                MsgSendSuccess();
                //Console.WriteLine("Hello");
            }
            else
            {
                if (QuoteDocNo <= 0)
                {
                    MsgInfo("Please generate Quotation first.");
                }
                else
                {
                    MsgBrochureNotAvaialble();
                }
            }

            #endregion
        }
        catch (Exception ex)
        {
            MsgInfo("Mail is not send.");
        }
    }

    protected void rptQuote_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndProjectQuoteID = (HiddenField)e.Item.FindControl("hndProjectQuoteID");
            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc");
            LinkButton btnSendAMail = (LinkButton)e.Item.FindControl("btnSendAMail");
            Image Image2 = (Image)e.Item.FindControl("Image2");
            //Label lblsignchked = (Label)e.Item.FindControl("lblsignchked");
            //Label lblsignUnChked = (Label)e.Item.FindControl("lblsignUnChked");
            // System.Web.UI.WebControls.CheckBox chkSerialNo = (System.Web.UI.WebControls.CheckBox)e.Item.FindControl("chkSerialNo");


            SttblProjectQuotes st = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteID(hndProjectQuoteID.Value);

            int Signed = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, st.ProjectQuoteDoc);
            if (Signed == 1)
            {
                //lblsignchked.Visible = true;
                //lblsignUnChked.Visible = false;               
                btnSendAMail.Visible = true;
                btnSendAMail.PostBackUrl = "~/admin/adminfiles/company/company.aspx?m=pro&compid=" + Request.QueryString["compid"] + "&proid=" + Request.QueryString["proid"];
                Image2.Visible = true;

            }
            else
            {
                //lblsignchked.Visible = false;
                //lblsignUnChked.Visible = true;
                btnSendAMail.Visible = false;
                Image2.Visible = false;
            }

            //hypDoc.NavigateUrl = pdfURL + "quotedoc/" + st.QuoteDoc;
            hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("quotedoc", st.QuoteDoc);
        }
    }

    protected void rptQuote_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.CommandName.ToString() == "MailAttachedPDf")
        {
            string QuoteId = e.CommandArgument.ToString();

            #region SendMail

            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "Arise Solar Signed Quotation PDF";

            Server.Execute("~/mailtemplate/QuotationAttachedMail.aspx?proid=" + ProjectID, txtWriter);

            SttblProjectQuotes st = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteID(QuoteId);
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st2.ContactID);

            string PDFName = st2.ProjectNumber + "_" + st.QuoteDoc;

            //string filepath = pdfURL + "quotedoc/" + st.QuoteDoc;
            string filepath = SiteConfiguration.GetDocumnetPath("quotedoc", st.QuoteDoc);

            //For Live
            // Utilities.SendMailWithAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName);

            //For Local
            Utilities.SendMailWithAttachment(from, "suresh.parmar@meghtechnologies.com", Subject, txtWriter.ToString(), filepath, PDFName);
            MsgSendSuccess();
            try
            {
            }
            catch
            {
            }

            #endregion

        }
    }

    protected void btnCreateNewQoute_Click(object sender, EventArgs e)
    {
        string QuoteID = string.Empty;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string ProjectNumber = st.ProjectNumber;
        //DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectTop1(ProjectID);
        DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectByProjectID(ProjectID);

        DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);

        if (dtTop1.Rows.Count > 0)
        {
        }
        else
        {
            bool sucQoute = ClstblProjects.tblProjects_UpdateQuoteSent(ProjectID);
        }

        //BindProjectQuote();

        int success = ClstblProjects.tblProjectQuotes_Insert(ProjectID, ProjectNumber, EmployeeID);
        string QuoteDoc = "Quotation.pdf";

        if (Convert.ToString(success) != "")
        {
            QuoteID = Convert.ToString(success);
            QuoteDoc = Convert.ToString(success) + QuoteDoc;
            bool suc = ClstblProjects.tblProjectQuotes_UpdateProjectQuoteDoc(Convert.ToString(success), QuoteDoc);
        }

        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
        TextWriter txtWriter = new StringWriter() as TextWriter;

        if (st.ProjectTypeID == "8")
        {
            if (stCust.ResCom == "2")
            {
                Server.Execute("~/mailtemplate/mtcecommquote.aspx?id=" + ProjectID, txtWriter);
            }
            else
            {
                Server.Execute("~/mailtemplate/mtcequote.aspx?id=" + ProjectID, txtWriter);
            }
        }
        else
        {
            if (stCust.ResCom == "2")
            {
                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                {
                    Telerik_reports.generate_Quote(ProjectID, QuoteID);

                    SetAdd1();
                }
                else
                {
                    //  Telerik_reports.generate_Quote(ProjectID, QuoteID);//Remove Before Uploadd
                    //Telerik_reports.generate_Quote(ProjectID, QuoteID);
                    Telerik_reports.generate_QuoteCash(ProjectID, QuoteID);
                    SetAdd1();
                }
                //Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);
            }
            else
            {
                if (st2.FinanceWithDeposit != string.Empty && dt1.Rows[0]["FinanceWith"].ToString() != "Cash")
                {
                    Telerik_reports.generate_Quote(ProjectID, QuoteID);
                    SetAdd1();
                }
                else
                {
                    Telerik_reports.generate_Quote(ProjectID, QuoteID);//Remove Before Uploadd
                    SetAdd1();
                    //Telerik_reports.generate_QuoteCash(ProjectID, QuoteID);
                }
                // Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);//quatationnew
                //Server.Execute("~/mailtemplate/receipt.aspx?id=" + ProjectID, txtWriter);//quatationdesign
            }
        }

        //String htmlText = txtWriter.ToString();

        //HTMLExportToPDF(htmlText, ProjectID + "Quote.pdf");
        // SavePDF(htmlText, QuoteDoc);
        BindQuote(ProjectID);
        BindQuoteDetails();


    }

    protected void btnDocUpload_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string map = "";
        string SQ = "";
        string MP = "";
        string EB = "";
        string PD = "";
        string PR = "";
        string beat = "";

        if (ddlDocument.SelectedItem.Text == "NearMap Photo")
        {
            if (fuDocument.HasFile)
            {
                bool s1 = ClsProjectSale.tblProjects_UpdatenearmapPdf(ProjectID, "");
                bool sucma8 = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
                bool sucmp6565 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, "");
                bool sucmap28952 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");

                SiteConfiguration.DeletePDFFile("NearMap", st.PdfFileNaame);
                map = fuDocument.FileName;
                string path = Request.PhysicalApplicationPath + "\\userfiles\\NearMap\\";
                fuDocument.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\NearMap\\") + map);
                string PdfFilePath = Request.PhysicalApplicationPath + "\\userfiles\\NearMap\\" + map;
                if (File.Exists(PdfFilePath))
                {
                    int NearMapId = ClsProjectSale.tblNearMapImages_Insert(ProjectID, st.ProjectNumber, map, DateTime.Now.AddHours(14).ToString());
                    bool s12 = ClsProjectSale.tblProjects_UpdatenearmapPdf(ProjectID, map);
                    //SiteConfiguration.UploadPDFFile("NearMap", map);
                    using (var document = PdfDocument.Load(PdfFilePath))
                    {
                        var pageCount = document.PageCount;

                        for (int i = 0; i < pageCount; i++)
                        {
                            var dpi = 100;

                            using (var image = document.Render(i, dpi, dpi, PdfRenderFlags.CorrectFromDpi))
                            {
                                var encoder = ImageCodecInfo.GetImageEncoders()
                                    .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                                var encParams = new EncoderParameters(1);
                                encParams.Param[0] = new EncoderParameter(
                                    System.Drawing.Imaging.Encoder.Quality, 100L);
                                if (i == 0)
                                {
                                    string filname = "";
                                    if (st.nearmapdoc != null && st.nearmapdoc != "")
                                    {
                                        bool sucmap34 = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");

                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                        SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
                                    }
                                    else
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                    }

                                    image.Save(path + "/" + filname, encoder, encParams);
                                    bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, filname);
                                    bool sucmap2 = ClsProjectSale.tblNearMapImages_UpdateImage1(NearMapId.ToString(), filname);
                                    //SiteConfiguration.UploadPDFFile("NearMap", filname);
                                    //SiteConfiguration.deleteimage(filname, "NearMap");

                                }
                                if (i == 1)
                                {
                                    string filname = "";
                                    if (st.nearmapdoc1 != null && st.nearmapdoc1 != "")
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                        SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc1);
                                        bool sucmp1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, "");
                                    }
                                    else
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                    }
                                    image.Save(path + "/" + filname, encoder, encParams);
                                    bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New(ProjectID, filname);
                                    bool sucmap2 = ClsProjectSale.tblNearMapImages_UpdateImage2(NearMapId.ToString(), filname);
                                    //SiteConfiguration.UploadPDFFile("NearMap", filname);
                                    //SiteConfiguration.deleteimage(filname, "NearMap");
                                }
                                if (i == 2)
                                {
                                    string filname = "";
                                    if (st.nearmapdoc2 != null && st.nearmapdoc2 != "")
                                    {

                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                        SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc2);
                                        bool sucmp1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, "");

                                    }
                                    else
                                    {
                                        filname = NearMapId + "_" + ProjectID + "Nearmap" + i + ".jpg";
                                    }

                                    image.Save(path + "/" + filname, encoder, encParams);
                                    bool sucmap1 = ClsProjectSale.tblProjects_Updatenearmapdoc_New1(ProjectID, filname);
                                    bool sucmap2 = ClsProjectSale.tblNearMapImages_UpdateImage3(NearMapId.ToString(), filname);
                                    //SiteConfiguration.UploadPDFFile("NearMap", filname);
                                    //SiteConfiguration.deleteimage(filname, "NearMap");
                                }
                            }
                        }

                        //SiteConfiguration.deleteimage(map, "NearMap");
                    }
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        // Console.WriteLine(ex.Message);
                    }
                }
                bool succ_nearmap = ClsProjectSale.tblProjects_UpdateQuotenearmapcheck(ProjectID, "True");
                SetAdd1();
                //SiteConfiguration.deleteimage(map, "NearMap");
            }
        }
        else if (ddlDocument.SelectedItem.Text == "Meter Photos Saved")
        {
            if (fuDocument.HasFile)
            {
                try
                {
                    SiteConfiguration.DeletePDFFile("MP", st.MP);
                    MP = ProjectID + fuDocument.FileName;
                    fuDocument.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\MP\\") + MP);
                    bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, MP);
                    //SiteConfiguration.UploadPDFFile("MP", MP);
                    //SiteConfiguration.deleteimage(MP, "MP");

                    bool sucMeterBoxPhotosSaved = ClstblSalesinfo.tblProjects_Update_MeterBoxPhotosSaved(ProjectID, "True");
                    SetAdd1();
                }
                catch (Exception ex)
                {
                    SetError1();
                }

            }
        }
        else if (ddlDocument.SelectedItem.Text == "Electricity Bill Saved")
        {
            if (fuDocument.HasFile)
            {
                try
                {
                    SiteConfiguration.DeletePDFFile("EB", st.EB);
                    EB = ProjectID + fuDocument.FileName;
                    fuDocument.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\EB\\") + EB);
                    bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, EB);
                    //SiteConfiguration.UploadPDFFile("EB", EB);
                    //SiteConfiguration.deleteimage(EB, "EB");

                    bool sucElecBillSaved = ClstblSalesinfo.tblProjects_Update_ElecBillSaved(ProjectID, "True");
                    SetAdd1();
                }
                catch (Exception ex)
                {
                    SetError1();
                }
            }
        }
        else if (ddlDocument.SelectedItem.Text == "Prop Design Saved")
        {
            if (fuDocument.HasFile)
            {
                try
                {
                    SiteConfiguration.DeletePDFFile("PD", st.PD);
                    PD = ProjectID + fuDocument.FileName;
                    fuDocument.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PD\\") + PD);
                    bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, PD);
                    //SiteConfiguration.UploadPDFFile("PD", PD);
                    //SiteConfiguration.deleteimage(PD, "PD");

                    bool sucProposedDesignSaved = ClstblSalesinfo.tblProjects_Update_ProposedDesignSaved(ProjectID, "True");
                    SetAdd1();
                }
                catch (Exception ex)
                {
                    SetError1();
                }
            }
        }
        else if (ddlDocument.SelectedItem.Text == "Payment Receipt")
        {
            if (fuDocument.HasFile)
            {
                try
                {
                    SiteConfiguration.DeletePDFFile("PR", st.PR);
                    PR = ProjectID + fuDocument.FileName;
                    fuDocument.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PR\\") + PR);
                    bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, PR);
                    //SiteConfiguration.UploadPDFFile("PR", PR);
                    //SiteConfiguration.deleteimage(PR, "PR");

                    bool sucPaymentReceipt = ClstblSalesinfo.tblProjects_Update_PaymentReceipt(ProjectID, "True");
                    SetAdd1();
                }
                catch (Exception ex)
                {
                    SetError1();
                }
            }
        }
        else if (ddlDocument.SelectedItem.Text == "Beat Quote")
        {
            if (fuDocument.HasFile)
            {
                try
                {
                    SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
                    beat = ProjectID + fuDocument.FileName;
                    fuDocument.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\BeatQuote\\") + beat);
                    bool sucbeat = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, beat);
                    //SiteConfiguration.UploadPDFFile("BeatQuote", beat);
                    //SiteConfiguration.deleteimage(beat, "BeatQuote");

                    bool succ_beatquote = ClsProjectSale.tblProjects_Updatebeatquote(ProjectID, "True");
                    SetAdd1();
                }
                catch (Exception ex)
                {
                    SetError1();
                }
            }
        }


        BindQuoteDetails();
    }

    protected void rptDocumemt_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView dr = (DataRowView)e.Item.DataItem;
            string Document = dr["Document"].ToString();
            string Doc = dr["Doc"].ToString();

            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc1");

            if (Document == "NearMap Photo")
            {
                //hypDoc.NavigateUrl = pdfURL + "NearMap/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("NearMap", Doc);
            }
            else if (Document == "Meter Photos Saved")
            {
                //hypDoc.NavigateUrl = pdfURL + "MP/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("MP", Doc);
            }
            else if (Document == "Electricity Bill Saved")
            {
                //hypDoc.NavigateUrl = pdfURL + "EB/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("EB", Doc);
            }
            else if (Document == "Prop Design Saved")
            {
                //hypDoc.NavigateUrl = pdfURL + "PD/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("PD", Doc);
            }
            else if (Document == "Payment Receipt")
            {
                //hypDoc.NavigateUrl = pdfURL + "PR/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("PR", Doc);
            }
            else if (Document == "Beat Quote")
            {
                //hypDoc.NavigateUrl = pdfURL + "BeatQuote/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("BeatQuote", Doc);
            }
        }
    }

    protected void btnInvoiceSave_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        if (stPro.InvoiceNumber == "")
        {
            if (ProjectID != "")
            {
                bool suc = ClstblProjects.tblProjects_UpdateInvoiceDoc(ProjectID, stPro.ProjectNumber);
            }
        }

        decimal invoicepayGST = Convert.ToDecimal(txtInvoicePayTotal.Text) / 10;
        decimal invoicepayExGST = (Convert.ToDecimal(txtInvoicePayTotal.Text) / 10) * 10;
        string InvoicePayExGST = Convert.ToString(invoicepayExGST);
        string InvoicePayGST = Convert.ToString(invoicepayGST);
        string InvoicePayTotal = txtInvoicePayTotal.Text;
        string InvoicePayDate = txtInvoicePayDate.Text;
        string ReceiptNumber = txtReceiptNumber.Text;
        string InvoicePayMethodID = ddlInvoicePayMethodID.SelectedValue;
        string EmployeeID = ddlRecBy.SelectedValue;
        string CCSurcharge = txtCCSurcharge.Text;
        string Refund = "False";
        string RefundType = "0";
        string UpdatedBy = stEmp.EmployeeID;
        string InvoiceNotes = stPro.InvoiceNotes;

        if (hndMode.Value == "") //Insert Done
        {
            int success = ClstblInvoicePayments.tblInvoicePayments_Insert(ProjectID, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, InvoicePayDate, InvoicePayMethodID, CCSurcharge, EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);

            bool UpdateTrasactionCode = ClstblInvoicePayments.tblInvoicePayments_Update_TransactionCode(success.ToString(), txtTransactionCode.Text);

            bool UpdateNotes = ClstblInvoicePayments.tblInvoicePayments_Update_PaymentNote(success.ToString(), txtInovoiceNotes.Text);

            if (success > 0)
            {
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }
        else //Update
        {
            bool success = ClstblInvoicePayments.tblInvoicePayments_Update(hndInvoicePaymentID.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, InvoicePayDate, InvoicePayMethodID, CCSurcharge, EmployeeID, UpdatedBy, ReceiptNumber);

            bool UpdateTrasactionCode = ClstblInvoicePayments.tblInvoicePayments_Update_TransactionCode(hndInvoicePaymentID.Value, txtTransactionCode.Text);

            bool UpdateNotes = ClstblInvoicePayments.tblInvoicePayments_Update_PaymentNote(hndInvoicePaymentID.Value, txtInovoiceNotes.Text);

            if (success)
            {
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }

        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        if (dtCount.Rows.Count > 0)
        {
            if (stPro.TotalQuotePrice != string.Empty && dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                decimal BalanceOwing = balown / 10;
                string BalanceGST = Convert.ToString(BalanceOwing);
                string pInvoicePayTotal = dtCount.Rows[0]["InvoicePayTotal"].ToString();
                bool suc = ClstblInvoicePayments.tblProjects_UpdateInvoice(ProjectID, BalanceGST, InvoiceNotes, InvoicePayDate);

                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) <= 0)
                {
                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "0");
                }
                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) < Convert.ToDecimal(stPro.TotalQuotePrice))
                {
                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
                }
                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) >= Convert.ToDecimal(stPro.TotalQuotePrice))
                {
                    string EmpID = stEmp.EmployeeID;

                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
                    ClstblInvoicePayments.tblProjects_UpdateInvoiceFU(ProjectID);
                }
            }
        }

        status(stPro, ProjectID);

        /* ----------------- Send Mail ----------------- */

        StUtilities StUtilities = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        //SiteURL = st.siteurl;
        string from = StUtilities.from;
        string ProjectNumber = stPro.ProjectNumber;
        string PayDate = txtInvoicePayDate.Text;
        string PaidAmount = txtInvoicePayTotal.Text;
        string PaidBy = stEmp.EmpFirst + " " + stEmp.EmpLast;
        ReceiptNumber = txtReceiptNumber.Text;
        //TextBox txtInvoiceFU = this.Parent.FindControl("txtInvoiceFU") as TextBox;

        if (ddlInvoicePayMethodID.SelectedValue == "7")
        {
            TextWriter txtWriter = new StringWriter() as TextWriter;
            string subject = "Cash Transaction";

            Server.Execute("~/mailtemplate/cashmail.aspx?ProjectNumber=" + ProjectNumber + "&PayDate=" + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(PayDate)) + "&PaidAmount=" + PaidAmount + "&ReceiptNumber=" + ReceiptNumber + "&PaidBy=" + PaidBy, txtWriter);
            try
            {
                Utilities.SendMail(from, "peter.eurosolar@gmail.com", subject, txtWriter.ToString(), "cash@eurosolar.com.au");
            }
            catch
            { }
        }

        /* --------------- Send Mail End --------------- */

        hndMode.Value = "";
        BindQuoteDetails();
        Reset(stEmp.EmployeeID);
    }

    public void status(SttblProjects stPro, string ProjectID)
    {
        DataTable dt_Count = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        string totalprice = stPro.TotalQuotePrice;
        decimal TotalPrice = 0;
        if (totalprice != string.Empty)
        {
            TotalPrice = Convert.ToDecimal(totalprice);
        }
        decimal totalpay = 0;

        //if (dt_Count.Rows.Count > 0)
        //{
        //    if (dt_Count.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        //    {
        //        totalpay = Convert.ToDecimal(dt_Count.Rows[0]["InvoicePayTotal"].ToString());
        //        decimal balown = Convert.ToDecimal(totalprice) - Convert.ToDecimal(totalpay);
        //        if (txtBalanceOwing != null)
        //        {
        //            try
        //            {
        //                txtBalanceOwing.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
        //            }
        //            catch { }
        //        }
        //    }
        //}

        if (TotalPrice - totalpay > 0)
        {
            bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
        }
        else
        {
            bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
        }

    }

    protected void txtInvoicePayTotal_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtInvoicePayTotal.Text) > 0)
        {
            decimal invoicepayGST = Convert.ToDecimal(txtInvoicePayTotal.Text) / 10;
            try
            {
                txtInvoicePayGST.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(invoicepayGST));
            }
            catch { }
        }
        else
        {
            txtInvoicePayGST.Text = "0";
            txtCCSurcharge.Text = "0";
        }
    }

    protected void ddlInvoicePayMethodID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlInvoicePayMethodID.SelectedValue == "3")
        //{
        //    ModalPopupExtender2.Show();

        //}
        //if (ddlInvoicePayMethodID.SelectedValue == "0")
        //{
        //    RequiredFieldValidatorPayBy.Visible = true;
        //}
        //else
        //{
        //    RequiredFieldValidatorPayBy.Visible = false;
        //}

        if (ddlInvoicePayMethodID.SelectedValue == "7" || ddlInvoicePayMethodID.SelectedValue == "3" || ddlInvoicePayMethodID.SelectedValue == "2")
        {
            //txtReceiptNumber.Visible = true;
            DivReceiptNo.Visible = true;
        }
        else
        {
            //txtReceiptNumber.Visible = false;
            DivReceiptNo.Visible = false;
        }

        if (ddlInvoicePayMethodID.SelectedValue == "3")
        {
            decimal ccsurcharge = 0;
            if (txtInvoicePayTotal.Text != string.Empty)
            {
                ccsurcharge = ((Convert.ToDecimal(txtInvoicePayTotal.Text)) * Convert.ToDecimal(1)) / 100;
            }
            txtCCSurcharge.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(ccsurcharge));
            txtCCSurcharge.Enabled = false;
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("PostInstaller"))
            {
                txtCCSurcharge.Enabled = true;
            }
        }
        else
        {
            if (ddlInvoicePayMethodID.SelectedValue == "6")
            {
                txtCCSurcharge.Text = "0";
                txtCCSurcharge.Enabled = true;
            }
            else
            {
                txtCCSurcharge.Text = string.Empty;
                txtCCSurcharge.Enabled = false;
            }
        }
    }

    protected void rptPaymentDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string InvoicePaymentID = e.CommandArgument.ToString();
        if (e.CommandName.ToLower() == "delete")
        {
            ModalPopupExtenderDelete.Show();
            //SttblInvoicePayments st = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(InvoicePaymentID);
            hdndelete.Value = InvoicePaymentID;
        }

        if (e.CommandName.ToLower() == "edit")
        {
            hndMode.Value = "Update";
            hndInvoicePaymentID.Value = InvoicePaymentID;

            SttblInvoicePayments st = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(InvoicePaymentID);

            txtInvoicePayDate.Text = Convert.ToDateTime(st.InvoicePayDate).ToShortDateString();
            txtInvoicePayTotal.Text = Convert.ToDecimal(st.InvoicePayTotal).ToString("F");
            txtInvoicePayGST.Text = Convert.ToDecimal(st.InvoicePayGST).ToString("F");
            ddlInvoicePayMethodID.SelectedValue = st.InvoicePayMethodID;
            txtCCSurcharge.Text = st.CCSurcharge;
            ddlRecBy.SelectedValue = st.EmployeeID;
            txtInovoiceNotes.Text = st.PaymentNote;

            if (st.InvoicePayMethodID == "7" || st.InvoicePayMethodID == "3" || st.InvoicePayMethodID == "2")
            {
                txtReceiptNumber.Text = st.ReceiptNumber;

                //DivReceiptNo.Visible = true;
                //DivTransactionCode.Visible = false;

                DivReceiptNo.Attributes.Remove("style");
                DivTransactionCode.Attributes.Add("Display", "none");
            }
            else if (st.InvoicePayMethodID == "6")
            {
                //DivReceiptNo.Visible = false;
                DivReceiptNo.Attributes.Add("Display", "none");

                txtTransactionCode.Text = st.TransactionCode;
                //DivTransactionCode.Visible = true;
                DivTransactionCode.Attributes.Remove("style");
            }
            else
            {
                //txtReceiptNumber.Visible = false;
                //DivReceiptNo.Visible = false;
                //DivTransactionCode.Visible = false;
                DivReceiptNo.Attributes.Add("Display", "none");
                DivTransactionCode.Attributes.Add("Display", "none");
            }
        }
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string InvoicePaymentID = hdndelete.Value.ToString();

        if (!string.IsNullOrEmpty(InvoicePaymentID))
        {
            bool DeletePayment = ClstblSalesinfo.tblInvoicePayments_DeleteByInvoicePaymentID(InvoicePaymentID);
        }

        BindQuoteDetails();
    }

    public void Reset(string EmployeeID)
    {
        txtInvoicePayDate.Text = string.Empty;
        txtInvoicePayTotal.Text = string.Empty;
        txtInvoicePayGST.Text = "0.00";
        ddlInvoicePayMethodID.SelectedValue = "";
        txtCCSurcharge.Text = string.Empty;
        ddlRecBy.SelectedValue = EmployeeID;

        DivReceiptNo.Visible = false;
        txtReceiptNumber.Text = string.Empty;

        txtInvoicePayDateInstall.Text = string.Empty;
        txtInvoicePayTotalInstall.Text = string.Empty;
        txtInvoicePayGSTInstall.Text = "0.00";
        ddlInvoicePayMethodIDInstall.SelectedValue = "";
        txtCCSurchargeInstall.Text = string.Empty;
        ddlRecByInstall.SelectedValue = EmployeeID;

        DivReceiptNoInstall.Visible = false;
        txtReceiptNumberInstall.Text = string.Empty;
        txtInovoiceNotes.Text = string.Empty;
        txtInvoiceNotesInstall.Text = string.Empty;
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "status")
        {
            precancel.Visible = false;
            ddlprecancel.Visible = false;
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>alert('');</script>");
            hndStatusProjectID.Value = e.CommandArgument.ToString();
            ModalPopupExtenderStatus.Show();
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(e.CommandArgument.ToString());
            ListItem item4 = new ListItem();
            item4.Text = "Select";
            item4.Value = "";
            string stusid = st2.ProjectStatusID;
            hndProjectStatusId.Value = stusid;
            ddlProjectStatusID.Items.Clear();
            ddlProjectStatusID.Items.Add(item4);
            txtActiveDate1.Text = DateTime.Now.AddHours(14).ToShortDateString();
            //if (st2.ProjectStatusID!=null && st2.ProjectStatusID != "")
            //{

            //    if (st2.ProjectStatusID == "4")

            //    {
            //        Div2.Visible = true;
            //        txtActiveDate1.Visible = true;
            //    }
            //    else
            //    {
            //        Div2.Visible =false;
            //        txtActiveDate1.Visible = false;
            //    }
            //}
            if (st2.ProjectStatusID == "2")
            {
                if ((Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep")))
                {
                    //Response.Write("Hi1");
                    //Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusDSalesRep();  // 4,6
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
                {
                    //Response.Write("Hi2");
                    //Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusSalesRep();  // 2,4,6,7,9
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if ((Roles.IsUserInRole("Maintenance")))
                {
                    //Response.Write("Hi3");
                    //Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_Mtce();  //  2,9
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if ((Roles.IsUserInRole("Installation Manager")))
                {
                    //Response.Write("Hi4");
                    //Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_InstMgr();  //  6,9
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if (Roles.IsUserInRole("Administrator"))
                {
                    ddlProjectStatusID.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();  // all
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else
                {
                    //ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusSM();   // 2,3,4,6,7,9
                    //ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    //ddlProjectStatusID.DataTextField = "ProjectStatus";
                    //ddlProjectStatusID.DataMember = "ProjectStatus";
                    //ddlProjectStatusID.DataBind();
                }
            }
            else
            {
                if (Roles.IsUserInRole("Administrator"))
                {
                    ddlProjectStatusID.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();  // all
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            if (st2.ProjectStatusID == "3" || st2.ProjectStatusID == "8")
            {
                if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
                {
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusDSalesRep();  //  4,6
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            if (st2.ProjectStatusID == "4")
            {
                if ((Roles.IsUserInRole("Installation Manager")))
                {
                    //Response.Write("Hiiii");
                    //Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusInstMgr();  // 3,6,8
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            if (st2.ProjectStatusID == "6")
            {
                if ((Roles.IsUserInRole("Installation Manager")))
                {
                    //Response.Write("Hiiii");
                    //Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusInstallMgrCancel();  // 3,4,8,20
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            if (st2.ProjectStatusID == "20")
            {
                if ((Roles.IsUserInRole("Installation Manager")))
                {
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusInstallMgr();  //6
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            string CSalesTeamID = stEmpC.SalesTeamID;
            //if (Profile.eurosolar.projectid != string.Empty)
            //{
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.EmployeeID);
            string EmpType = stEmp.EmpType;
            string SalesTeamID = stEmp.SalesTeamID;
            if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
            {
                hndStatusProjectID.Value = e.CommandArgument.ToString();
                ModalPopupExtenderStatus.Show();
            }
            else
            {
            }
            //}
        }
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End
        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        //string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(id);
        //ltproject.Text = st.ProjectNumber + " -" + st.Project;
        //Profile.eurosolar.projectid = id;
        //ViewProjectDetail();
        //string companyId = Request.QueryString["compid"];
        //Response.Redirect("/admin/adminfiles/company/SalesInfo.aspx?compid=" + companyId + "&ProjectID=" + id);
    }

    protected DataTable GetGridData()
    {
        DataTable dt = ClstblSalesinfo.tblProjects_NewProjectTabSelectByUCustomerID(Request.QueryString["compid"]);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGridProject.Visible = true;
        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            //  PanNoRecord.Visible = true;
            PanGridProject.Visible = false;
        }
        else
        {
            //BindDropDown();
            PanGridProject.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            // PanNoRecord.Visible = false;
        }
    }

    protected void btnSaveActiveDate_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string CompanyId = Request.QueryString["compid"];

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string QuoteAccepted = txtQuoteAcceptedQuote.Text.Trim();
        string SignedQuote = Convert.ToString(chkSignedQuote.Checked);
        string DepositReceived = txtDepositReceived.Text.Trim();
        string ActiveDate = txtActiveDate.Text.Trim();

        //tblProjects_Updatereadyactive
        if (chkactive.Checked)
        {
            bool sucyactive = ClstblProjects.tblProjects_Updatereadyactive(ProjectID, chkactive.Checked.ToString());
        }
        //=====================================================================

        DataTable dt_inv1 = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
        {
            if (dt_inv1.Rows.Count > 0)
            {
                if (dt_inv1.Rows[0]["InvoicePayDate"].ToString() != string.Empty)
                {
                    string ExpiryDate = Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]).AddDays(-1).ToString();//System.DateTime.Now.AddDays(-1).ToString();

                    DateTime dtInvPay = Convert.ToDateTime(ExpiryDate);
                    //CompareValidatorDepRec.ValueToCompare = DateTime.Today.Date.ToString("dd/MM/yyyy"); //string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]));
                    if (txtDepositReceived.Text != null && txtDepositReceived.Text != "")
                    {
                        DateTime dtDeprec = Convert.ToDateTime(txtDepositReceived.Text);
                        if (dtDeprec < dtInvPay)
                        {
                            MsgError("Deposit Date Canot be less then First Dep. Date");
                            return;
                        }
                    }
                }
            }

        }
        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
        {
            if (txtActiveDate.Text != null && txtActiveDate.Text != "")
            {
                string dtToday = Convert.ToDateTime(DateTime.Now.AddHours(14)).ToShortDateString();
                string ActDt = Convert.ToDateTime(txtActiveDate.Text).ToShortDateString();
                if (Convert.ToDateTime(ActDt) < Convert.ToDateTime(dtToday))
                {
                    MsgError("Active date should be less then today date..");
                    return;
                }
            }
        }



        /* ----------------------- Deposite Rec Status ----------------------- */
        //Mtn-New Inquiry than cant change Status Updated change by Kiran on 01-10-2019
        SttblProjects stmntc = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (stmntc.ProjectStatusID != "15")
        {
            if (st.ActiveDate == string.Empty)
            {
                if (txtDepositReceived.Text != string.Empty)
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                    if (dtStatusDR.Rows.Count > 0)
                    {
                    }
                    else
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    }
                    //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                        ClstblCustomers.tblCustomers_UpdateCustType("3", Request.QueryString["compid"]);
                    }
                }
                else
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "2");
                    if (dtStatusDR.Rows.Count > 0)
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    }
                    //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                    {
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "2");
                    }
                }
            }
        }
        /* -------------------------------------------------------------- */

        if (chkSignedQuote.Checked == true)
        {
            //string SQ = "";
            //if (fuSQ.HasFile)
            //{
            //    SiteConfiguration.DeletePDFFile("SQ", st.SQ);
            //    SQ = ProjectID + fuSQ.FileName;
            //    fuSQ.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/SQ/") + SQ);
            //    bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
            //    SiteConfiguration.UploadPDFFile("SQ", SQ);
            //    SiteConfiguration.deleteimage(SQ, "SQ");
            //}
            //else
            //{
            //    bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, st.SQ);
            //}
        }
        else
        {
            SiteConfiguration.DeletePDFFile("SQ", st.SQ);
            bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, "");

            //RequiredFieldValidatorSQ.Visible = false;
        }

        string UpdatedBy = stEmp.EmployeeID;
        bool sucQuote = ClstblSalesinfo.tblProjects_UpdateData(ProjectID, QuoteAccepted, DepositReceived, UpdatedBy, ActiveDate);

        string Time = DateTime.Now.AddHours(15).ToLongTimeString();

        string DepositReceivedNew = DepositReceived;

        if (DepositReceivedNew != "")
        {
            DepositReceivedNew = DepositReceivedNew + ' ' + Time;
            //DepositReceivedNew = Convert.ToDateTime(DepositReceivedNew).AddHours(14).ToString();
        }
        bool sucUpdateDRNew = ClstblSalesinfo.tblProjects_Update_DepositReceivedNew(ProjectID, DepositReceivedNew);

        #region Email Active Status
        /* ----------------------- Active Status ----------------------- */
        TextWriter txtWriter = new StringWriter() as TextWriter;
        StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");

        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        //Response.Write(st.ContactID);
        //Response.End();
        string from = stU.from;
        string mailto = stCont.ContEmail;
        string subject = "Thank you for choosing Arisesolar";
        //==========for test==============
        ////Server.Execute("~/mailtemplete/activemail.aspx?Customer=" + st.Customer, txtWriter);
        ////try
        ////{

        ////  //  Response.Write(from + "======" + mailto + "======" + subject + "======" + txtWriter.ToString());
        ////    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());

        ////}
        ////catch
        ////{
        ////}
        // Response.End();
        //=============================
        if (stmntc.ProjectStatusID != "15")
        {
            if (st.InstallCompleted == string.Empty)
            {
                if (txtActiveDate.Text != string.Empty)
                {
                    if (st.SurveyCerti == "1")
                    {
                        if (st.CertiApprove == "1")
                        {
                            DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                            {
                                if (dtStatusDR.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                {
                                    if (st.ProjectStatusID != "11")
                                    {
                                        if (st.InstallState == "VIC")
                                        {
                                            if (!string.IsNullOrEmpty(st.VicAppReference))
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        else
                                        {

                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                    //try
                                    //{
                                    //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    //    //   Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                    //}
                                    //catch
                                    //{
                                    //}
                                }
                            }
                            else
                            {
                                if (st.DocumentVerified == "True")
                                {
                                    if (dtStatusDR.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                    }
                                    if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                    {
                                        if (st.ProjectStatusID != "11")
                                        {
                                            if (st.InstallState == "VIC")
                                            {
                                                if (!string.IsNullOrEmpty(st.VicAppReference))
                                                {
                                                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                                }
                                            }
                                            else
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                        //try
                                        //{
                                        //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                        //    //  Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                        //}
                                        //catch
                                        //{
                                        //}
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            if (dtStatusDR.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                            }
                            if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                            {
                                if (st.ProjectStatusID != "11")
                                {
                                    if (st.InstallState == "VIC")
                                    {
                                        if (!string.IsNullOrEmpty(st.VicAppReference))
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    else
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                //try
                                //{
                                //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                //    // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                //}
                                //catch
                                //{
                                //}
                            }
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                if (dtStatusDR.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                {
                                    if (st.ProjectStatusID == "2" || st.ProjectStatusID == "8")//Changes on 4/7/2020 By Nilesh Add st.ProjectStatusID != "12"
                                    {
                                        if (st.InstallState == "VIC")
                                        {
                                            if (!string.IsNullOrEmpty(st.VicAppReference))
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        //else(st.ProjectStatusID != "11" && st.ProjectStatusID != "12")//Changes on 4/7/2020 By Nilesh Add
                                        //{
                                        //    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        //}
                                        else
                                        {
                                            if (st.ProjectStatusID == "2" || st.ProjectStatusID == "8")//Changes on 4/7/2020 By Nilesh Add
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                    }
                                    //Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                    //try
                                    //{
                                    //    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    //    // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                    //}
                                    //catch (Exception ex)
                                    //{

                                    //}
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (txtDepositReceived.Text != string.Empty)
                    {
                        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                        if (dtStatusDR.Rows.Count > 0)
                        {
                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                        }
                        //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                        {
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                        }
                    }
                }
            }
        }
        #endregion

        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Rep"))
        {
            try
            {
                ClstblProjects.tblProjects_UpdateQuoteSentDate(ProjectID, txtQuoteSent.Text);
            }
            catch (Exception ex)
            {
            }
        }


        #region GoogleSheet Code
        //GoogleSheet Code By Kiran 07-09-2019
        SttblCustomers stcomp = ClstblCustomers.tblCustomers_SelectByCustomerID(CompanyId);
        if (txtDepositReceived.Text != string.Empty)
        {
            if (string.IsNullOrEmpty(st.DepositReceived))
            {
                if (!string.IsNullOrEmpty(stcomp.gclid))
                {
                    string str = stcomp.gclid;
                    //if (str.Contains("?gclid="))
                    //{
                    //    int start = str.IndexOf("?gclid=") + ("?gclid=").Length;
                    //    string value = str.Substring(start);
                    //bool updategcid = ClstblCustomers.tblCustomers_Updategclid(Convert.ToString(success), value);



                    //GoogleSheet Code Start
                    try
                    {
                        GoogleCredential credential;
                        //Reading Credentials File...
                        //For Local
                        //string filepath = HttpContext.Current.Server.MapPath("app_client_secret.json");

                        //For Live
                        string filepath = HttpContext.Current.Server.MapPath("AriseSolar-692588a0321d.json");
                        using (var stream = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                        {
                            credential = GoogleCredential.FromStream(stream)
                                .CreateScoped(Scopes);
                        }

                        //Creating Google Sheets API service...
                        service = new SheetsService(new BaseClientService.Initializer()
                        {
                            HttpClientInitializer = credential,
                            ApplicationName = ApplicationName,
                        });

                        List<object> list = null;
                        //var range = $"{sheet}!A:S";
                        string range = "A:F";
                        var valueRange = new ValueRange();
                        //DateTime dt = Convert.ToDateTime(txtDepositReceived.Text);

                        //DateTime dt = new DateTime();
                        string dt = "";
                        if (DepositReceivedNew != "")
                        {
                            dt = Convert.ToDateTime(DepositReceivedNew).ToString("MM/dd/yyyy hh:mm:ss");
                        }

                        list = new List<object>() { str, "Offline Sales FINAL", dt, st.TotalQuotePrice, "", "" };

                        valueRange.Values = new List<IList<object>> { list };
                        var appendRequest = service.Spreadsheets.Values.Append(valueRange, SpreadsheetId, range);
                        appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
                        var appendReponse = appendRequest.Execute();

                    }
                    catch (Exception ex)
                    {

                    }
                    //GoogleSheet Code End


                    // }
                }
            }
        }
        #endregion


        if (sucQuote)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }
        BindScript();
        activemethodQuote();
        BindQuoteDetails();
        BindGrid(0);
    }

    public void activemethodQuote()//put by roshni
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if ((chkSignedQuote.Checked.ToString() == false.ToString() || st.QuoteAccepted == string.Empty || st.ActiveDate == string.Empty) && st.DepositReceived != string.Empty)
        {
            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
        }
        if (st.ActiveDate == string.Empty && st.DepositReceived == string.Empty)
        {
            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "2");
        }
    }

    protected void btnOpenInvoice_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.ProjectTypeID == "8")
        {
            Telerik_reports.generate_mtceinvoice(ProjectID, "Download");
        }
        else
        {
            Telerik_reports.generate_Taxinvoice(ProjectID, "Download");
        }
        BindScript();
    }

    protected void btnPrintReceipt_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        if (ProjectID != string.Empty)
        {
            Telerik_reports.generate_receipt(ProjectID);
        }
        BindScript();
    }

    protected void btnCheckActive_Click(object sender, ImageClickEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.SignedQuote != "False")
        {
            lblSignedQuote.Text = "<span class='width60px'>Signed Quote</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblSignedQuote.Text = "<span class='width60px'>Signed Quote </span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.ElecDistributorID == string.Empty)
        {
            lblEleDest.Text = "Please select Elec Distributor.";
        }
        else
        {
            lblEleDest.Visible = false;
        }
        if (st.ElecDistributorID == "12")
        {
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref </span>:&nbsp; <i class='fa fa-close redclr'></i>";
            }
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.meterupgrade != string.Empty)
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblmeterupgrade.Visible = true;
            meterupgrade.Visible = true;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        else if (st.ElecDistributorID == "13")
        {
            if (st.RegPlanNo != string.Empty)
            {
                lblRegPlanNo.Text = "<span class='width60px'>Reg Plan No</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblRegPlanNo.Text = "<span class='width60px'>Reg Plan No</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.LotNumber != string.Empty)
            {
                lblLotNum.Text = "<span class='width60px'>LotNumber</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblLotNum.Text = "<span class='width60px'>LotNumber</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.meterupgrade != string.Empty)
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblRegPlanNo.Visible = true;
            RegPlanNo.Visible = true;
            lblLotNum.Visible = true;
            LotNum.Visible = true;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblmeterupgrade.Visible = true;
            meterupgrade.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        else
        {
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.meterupgrade != string.Empty)
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblRegPlanNo.Visible = false;
            RegPlanNo.Visible = false;
            lblLotNum.Visible = false;
            LotNum.Visible = false;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblmeterupgrade.Visible = true;
            meterupgrade.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        //else
        //{
        //    lblRegPlanNo.Visible = false;
        //    lblLotNum.Visible = false;
        //    lblApprovalRef.Visible = false;
        //    lblNMINumber.Visible = false;
        //}
        if (st.DepositReceived != string.Empty)
        {
            lblDepositeRec.Text = "<span class='width60px'>Deposit Rec</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblDepositeRec.Text = "<span class='width60px'>Deposit Rec</span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.QuoteAccepted != string.Empty)
        {
            lblQuoteAccepted.Text = "<span class='width60px'>Quote Accepted</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblQuoteAccepted.Text = "<span class='width60px'>Quote Accepted</span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.InstallState == "VIC")
        {
            VicAppRefference.Visible = true;
            lblVicAppRefference.Visible = true;
            if (!string.IsNullOrEmpty(st.VicAppReference))
            {
                lblVicAppRefference.Text = "<span class='width60px'>Vic Rebate App Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblVicAppRefference.Text = "<span class='width60px'>Vic Rebate App Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
        }
        else
        {
            VicAppRefference.Visible = false;
            lblVicAppRefference.Visible = false;
        }

        //if (st.SQ != "" && st.SignedQuote != "False" && st.DepositReceived != "" && st.RegPlanNo != "" && st.LotNumber != "" && st.ElecDistApprovelRef != "" && st.NMINumber != "")
        if (st.ProjectStatusID == "3" || st.ProjectStatusID == "5")
        {
            lblActive.Text = "Project is Active";
        }
        else
        {
            lblActive.Text = "Project is not Active";
        }

        //if (st.InstallState == "NSW")
        //{
        //    NSWDob.Visible = true;
        //    lblNSWDob.Visible = true;
        //    if (!string.IsNullOrEmpty(st.DOB))
        //    {
        //        lblNSWDob.Text = "<span class='width60px'>DOB</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        //    }
        //    else
        //    {
        //        lblNSWDob.Text = "<span class='width60px'>DOB</span>:&nbsp;<i class='fa fa-close redclr'></i>";
        //    }
        //}
        //else
        //{
        //    NSWDob.Visible = false;
        //    lblNSWDob.Visible = false;
        //}

        ModalPopupExtender2.Show();
    }

    #region Web Methods
    [WebMethod]
    public static string ItemData(string ID, string NumberPanels, string IBDate, string ZoneRt, string STCValue)
    {
        if (ID != "")
        {
            ClstblSalesinfo.StStockItemData st = ClstblSalesinfo.SalesInfo_tblStockItems_SelectByStockItemID(ID);

            string StockCapacity = CalculateSystemCap(st.StockSize, NumberPanels);
            string STCNo = GetSTC(IBDate, ZoneRt, StockCapacity).ToString();
            string Rebate = GetRebate(STCNo, STCValue);

            var Data = new StockData
            {
                StockManufacturer = st.StockManufacturer,
                StockModel = st.StockModel,
                StockWatts = st.StockSize,
                StockCapacity = StockCapacity,
                STC = STCNo,
                Rebate = Rebate
            };

            return JsonConvert.SerializeObject(Data);
        }
        else
        {
            var Data = new StockData
            {
                StockManufacturer = "",
                StockModel = "",
                StockWatts = "0",
                StockCapacity = "0.00",
                STC = "0",
                Rebate = "0.00"
            };

            return JsonConvert.SerializeObject(Data);
        }
    }

    [WebMethod]
    public static string NumberPanels(string NumberPanels, string StockSize, string IBDate, string ZoneRt, string STCValue)
    {
        //ClstblSalesinfo.StStockItemData st = ClstblSalesinfo.SalesInfo_tblStockItems_SelectByStockItemID(ID);

        string StockCapacity = CalculateSystemCap(StockSize, NumberPanels);
        string STCNo = GetSTC(IBDate, ZoneRt, StockCapacity).ToString();
        string Rebate = GetRebate(STCNo, STCValue);

        var Data = new StockData
        {
            StockCapacity = StockCapacity,
            STC = STCNo,
            Rebate = Rebate
        };

        return JsonConvert.SerializeObject(Data);
    }

    public static string CalculateSystemCap(string StockSize, string NumberPanels)
    {
        string Capacity = ((Convert.ToDecimal(StockSize) * Convert.ToDecimal(NumberPanels)) / 1000).ToString();

        return Capacity;
    }

    public static int GetSTC(string InstalledBookingDate, string ZoneRt, string StockCapacity)
    {
        decimal STCNo;
        if (InstalledBookingDate == null || InstalledBookingDate == "")
        {
            DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
            String year = currentdate.Year.ToString();
            DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
            Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
            STCNo = (Convert.ToDecimal(StockCapacity) * Convert.ToDecimal(ZoneRt) * stcrate);
        }
        else
        {
            string date = Convert.ToDateTime(InstalledBookingDate).ToShortDateString();
            DateTime dt1 = Convert.ToDateTime(date);
            String year = dt1.Year.ToString();
            DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
            Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
            STCNo = (Convert.ToDecimal(StockCapacity) * Convert.ToDecimal(ZoneRt) * stcrate);
        }

        int myInt = (int)Math.Floor(STCNo);

        return myInt;
    }

    public static string GetRebate(string STCNo, string STCValue)
    {
        decimal Rebate = Convert.ToDecimal(STCNo) * Convert.ToDecimal(STCValue);

        return Rebate.ToString("F");
    }

    [WebMethod]
    public static string InverterChange(string ID)
    {
        if (ID != "")
        {
            ClstblSalesinfo.StStockItemData st = ClstblSalesinfo.SalesInfo_tblStockItems_SelectByStockItemID(ID);

            var Data = new StockData
            {
                StockModel = st.StockModel,
                StockSeries = st.StockSeries,
                StockWatts = Convert.ToDecimal(st.StockSize).ToString("F"),
                NoInverter = "1",
                StockManufacturer = st.StockManufacturer
            };

            return JsonConvert.SerializeObject(Data);
        }
        else
        {
            var Data = new StockData
            {
                StockModel = "",
                StockSeries = "",
                StockWatts = "0.00",
                NoInverter = "0",
                StockManufacturer = ""
            };

            return JsonConvert.SerializeObject(Data);
        }
    }

    [WebMethod]
    public static string InvoicePayTotalChange(string value, string InvoicePayMethodID, string Count)
    {
        decimal GST = Convert.ToDecimal("0");
        string RtnData = "0";

        if (value != "" && Convert.ToDecimal(value) > 0)
        {
            GST = Convert.ToDecimal(value) / 11;

            try
            {
                RtnData = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(GST));
            }
            catch { }

            string CCSurcharge = "0";
            if (InvoicePayMethodID == "3")
            {
                decimal ccsurcharge = 0;
                if (value != string.Empty)
                {
                    if (Count != "0")
                    {
                        ccsurcharge = ((Convert.ToDecimal(value)) * Convert.ToDecimal(1)) / 100;
                    }
                }
                CCSurcharge = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(ccsurcharge));
            }

            var Data = new InvoiceData
            {
                InvoicePayGST = RtnData,
                CCSurcharge = CCSurcharge
            };

            return JsonConvert.SerializeObject(Data);
        }
        else
        {
            var Data = new InvoiceData
            {
                InvoicePayGST = "0",
                CCSurcharge = "0"
            };

            return JsonConvert.SerializeObject(Data);
        }
    }

    [WebMethod]
    public static string InvoicePayMethodIDChange(string ID, string InvoicePayTotal, string Count)
    {
        var Data = new InvoiceData();
        if (ID != "")
        {
            decimal ccsurcharge = 0;
            if (InvoicePayTotal != string.Empty)
            {
                if (Count != "0")
                {
                    ccsurcharge = ((Convert.ToDecimal(InvoicePayTotal)) * Convert.ToDecimal(1)) / 100;
                }
            }
            string CCSurcharge = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(ccsurcharge));

            string RolesEnable = "false";
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("PostInstaller"))
            {
                RolesEnable = "true";
            }

            Data = new InvoiceData
            {
                CCSurcharge = CCSurcharge,
                RolesEnable = RolesEnable
            };
        }
        return JsonConvert.SerializeObject(Data);
    }
    #endregion

    protected void btnSQUpload_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string CompanyId = Request.QueryString["compid"];

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string SignedQuote = Convert.ToString(chkSignedQuote.Checked);
        string UpdatedBy = stEmp.EmployeeID;

        //if (chkSignedQuote.Checked == true)
        //{
        string SQ = "";
        if (fuSQ.HasFile)
        {
            SiteConfiguration.DeletePDFFile("SQ", st.SQ);
            SQ = ProjectID + fuSQ.FileName;
            fuSQ.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/SQ/") + SQ);
            bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
            //SiteConfiguration.UploadPDFFile("SQ", SQ);
            //SiteConfiguration.deleteimage(SQ, "SQ");

            bool sucQuote = ClstblSalesinfo.tblProjects_UpdateData_SQ(ProjectID, SignedQuote, UpdatedBy);

            SetAdd1();
            BindQuoteDetails();
        }
        else
        {
            //bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, st.SQ);
            SetError1();
        }
        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("SQ", st.SQ);
        //    bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, "");

        //    //RequiredFieldValidatorSQ.Visible = false;
        //}



    }

    public void BindBooking(SttblProjects stPro)
    {
        txtBkInstallSite.Text = stPro.InstallAddress + ',' + stPro.InstallCity + ',' + stPro.InstallState + '-' + stPro.InstallPostCode;
        txtBKSysDetails.Text = stPro.SystemDetails;

        txtBKInstallerNotes.Text = stPro.InstallerNotes;
        txtBKProjectNotes.Text = stPro.ProjectNotes;
        txtElecDistApproved.Text = stPro.ElecDistApproved;
        txtInstallBookingDate.Text = stPro.InstallBookingDate != "" ? Convert.ToDateTime(stPro.InstallBookingDate).ToShortDateString() : "";
        txtBKMeterInstallerNotes.Text = stPro.MeterInstallerNotes;

        if (stPro.ElecDistOK != "")
        {
            chkElecDistOK.Checked = stPro.ElecDistOK == "False" ? false : true;
        }

        try
        {
            ddlInstaller.SelectedValue = stPro.Installer;
        }
        catch (Exception ex) { }
        try
        {
            ddlDesigner.SelectedValue = stPro.Designer;
        }
        catch (Exception ex) { }
        try
        {
            ddlElectrician.SelectedValue = stPro.Electrician;
        }
        catch (Exception ex) { }

        hndProjectID.Value = Request.QueryString["proid"].ToString();
        string Location = "";
        if (stPro.StockAllocationStore != string.Empty)
        {
            try
            {
                ddlStockAllocationStore.SelectedValue = stPro.StockAllocationStore;
                Location = stPro.StockAllocationStore;
            }
            catch (Exception ex) { }
        }
        else
        {
            if (stPro.InstallState != string.Empty)
            {
                DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(stPro.InstallState);
                if (dtState.Rows.Count > 0)
                {
                    ddlStockAllocationStore.SelectedValue = dtState.Rows[0]["CompanyLocationID"].ToString();
                    Location = dtState.Rows[0]["CompanyLocationID"].ToString();
                }
            }
        }

        if (stPro.InstallBookingDate != "")
        {
            DivAddPickList.Visible = true;
            DateTime TodayDate = DateTime.Now.Date.AddHours(14).AddDays(56);
            DateTime date = Convert.ToDateTime(stPro.InstallBookingDate);

            if (TodayDate <= date)
            {
                DivAddPickList.Visible = false;
            }
            else
            {
                DivAddPickList.Visible = true;
            }
        }
        else
        {
            DivAddPickList.Visible = false;
        }

        txtBKProjectNotes.Enabled = false;

        hndPickListMode.Value = "";
        btnUpdatePicklist.Visible = false;
        BindPanelInverter(stPro, Location);
        BindPickList();
        BindPickListRepeater(stPro);
        BindCount(stPro, Location);

    }

    public void BindBookingDropdown()
    {
        ddlStockAllocationStore.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlStockAllocationStore.DataValueField = "CompanyLocationID";
        ddlStockAllocationStore.DataTextField = "CompanyLocation";
        ddlStockAllocationStore.DataBind();

        if (txtInstallBookingDate.Text.Trim() != string.Empty)
        {
            //string date1 = txtInstallBookingDate.Text.Trim();
            //string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

            //ListItem item2 = new ListItem();
            //item2.Text = "Installer";
            //item2.Value = "";
            //ddlInstaller.Items.Clear();
            //ddlInstaller.Items.Add(item2);
            ////   Response.Write(date1+"="+ date2);
            //ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
            //ddlInstaller.DataValueField = "ContactID";
            //ddlInstaller.DataTextField = "Contact";
            //ddlInstaller.DataMember = "Contact";
            //ddlInstaller.DataBind();
        }
        else
        {
            DataTable dt = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataSource = dt;
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();

            ddlinstall2.DataSource = dt;
            ddlinstall2.DataValueField = "ContactID";
            ddlinstall2.DataTextField = "Contact";
            ddlinstall2.DataMember = "Contact";
            ddlinstall2.DataBind();
        }

        DataTable dtDesign = ClstblContacts.tblContacts_SelectDesigner();
        ddlDesigner.DataSource = dtDesign;
        ddlDesigner.DataValueField = "ContactID";
        ddlDesigner.DataTextField = "Contact";
        ddlDesigner.DataMember = "Contact";
        ddlDesigner.DataBind();

        ddldesign2.DataSource = dtDesign;
        ddldesign2.DataValueField = "ContactID";
        ddldesign2.DataTextField = "Contact";
        ddldesign2.DataMember = "Contact";
        ddldesign2.DataBind();

        DataTable dtElectrician = ClstblContacts.tblContacts_SelectElectrician();
        ddlElectrician.DataSource = dtElectrician;
        ddlElectrician.DataValueField = "ContactID";
        ddlElectrician.DataTextField = "Contact";
        ddlElectrician.DataMember = "Contact";
        ddlElectrician.DataBind();

        ddlec2.DataSource = dtElectrician;
        ddlec2.DataValueField = "ContactID";
        ddlec2.DataTextField = "Contact";
        ddlec2.DataMember = "Contact";
        ddlec2.DataBind();

        ddlPickListCategory.Items.Clear();
        ListItem list = new ListItem();
        list.Text = "Select";
        list.Value = "";
        ddlPickListCategory.Items.Add(list);
        ddlPickListCategory.DataSource = ClsQuickStock.tbl_PickListCategory_GetData();
        ddlPickListCategory.DataValueField = "CategoryID";
        ddlPickListCategory.DataTextField = "Category";
        ddlPickListCategory.DataBind();
    }

    public void BindPanelInverter(SttblProjects stPro, string Location)
    {
        if (stPro.ProjectStatusID != "9")
        {
            if (stPro.ProjectTypeID == "7")
            {
                if (stPro.InstallBookingDate != string.Empty)
                {
                    if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Installation Manager")))
                    {
                        btnRemoveInst.Visible = true;
                    }
                }
                else
                {
                    btnRemoveInst.Visible = false;
                }
                if (stPro.FinanceWithID == string.Empty || stPro.FinanceWithID == "1")
                {
                    DivDocNotVerified.Visible = false;
                }
                else
                {
                    if (stPro.DocumentVerified == "True")
                    {
                        DivDocNotVerified.Visible = false;
                    }
                    else
                    {
                        DivDocNotVerified.Visible = true;
                    }
                }
            }
            else if (stPro.ProjectStatusID == "2" || stPro.ProjectStatusID == "8" || stPro.ProjectStatusID == "4" || stPro.ProjectStatusID == "20" || stPro.ProjectStatusID == "6")
            {

            }
            else
            {
                if (stPro.InstallBookingDate != string.Empty)
                {
                    if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Installation Manager")))
                    {
                        btnRemoveInst.Visible = true;
                    }
                }
                else
                {
                    btnRemoveInst.Visible = false;
                }
                if (stPro.FinanceWithID == string.Empty || stPro.FinanceWithID == "1")
                {
                    DivDocNotVerified.Visible = false;
                }
                else
                {
                    if (stPro.DocumentVerified == "True")
                    {
                        DivDocNotVerified.Visible = false;
                    }
                    else
                    {
                        DivDocNotVerified.Visible = true;
                    }
                }
            }
        }
    }

    public void HidePicklistButton()
    {
        //if (Roles.IsUserInRole("Administrator"))
        //{
        //    btnSavePickList.Visible = true;
        //}
        //else
        //{
        if (divpanel.Visible == true || DivPanel1.Visible == true || divPanelQuantity.Visible == true || divinverter.Visible == true || DivInverter1.Visible == true || divInverterQuantity.Visible == true)
        {
            btnSavePickList.Visible = false;
        }
        else
        {
            btnSavePickList.Visible = true;
        }
        //}
    }

    public void HideContent(string Mode)
    {
        DivSiteDetailsBK.Visible = Convert.ToBoolean(Mode);
        DivBookingDetails.Visible = Convert.ToBoolean(Mode);
        DivInstallerDetailsBK.Visible = Convert.ToBoolean(Mode);
        DivPIDetails.Visible = Convert.ToBoolean(Mode);
        PanPickList.Visible = Convert.ToBoolean(Mode);
        DivBuuton.Visible = Convert.ToBoolean(Mode);
        DivPickList.Visible = Convert.ToBoolean(Mode);
    }

    public void CheckActive(SttblProjects stPro)
    {
        if (stPro.ProjectStatusID != "9")
        {
            if (stPro.ProjectTypeID == "7")
            {
                divActive.Visible = false;
                HideContent("true");
            }
            else if (stPro.ProjectStatusID == "2" || stPro.ProjectStatusID == "8" || stPro.ProjectStatusID == "4" || stPro.ProjectStatusID == "20" || stPro.ProjectStatusID == "6")
            {
                if (stPro.InstallBookingDate != "")
                {
                    divActive.Visible = false;
                    HideContent("true");
                }
                else
                {
                    divActive.Visible = true;
                    HideContent("false");
                }

            }
            else
            {
                divActive.Visible = false;
                HideContent("true");
            }
        }

        string ProjectID = "";
        // installbookingtracker.openModal = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        if (!Roles.IsUserInRole("SalesRep") && !Roles.IsUserInRole("Sales Manager"))
        {
            int PickListExist = ClsQuickStock.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");

            if(PickListExist > 0)
            {
                divActive.Visible = false;
                HideContent("true");
            }
        }
    }

    protected void ddlInstaller_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlInstaller.SelectedValue != "")
        {
            try
            {
                ddlDesigner.SelectedValue = ddlInstaller.SelectedValue;
                ddlElectrician.SelectedValue = ddlInstaller.SelectedValue;
            }
            catch (Exception ex) { };

        }
        else
        {
            ddlDesigner.SelectedValue = "";
            ddlElectrician.SelectedValue = "";
        }
    }

    [WebMethod]
    public static string BindPanInverter(string ID, string ProjectID)
    {
        // For Panels
        string PL = "";
        string Apl = "";
        string SMPL = "";
        string WhOrderQty = "";
        string PT = "";
        string divpanel = "false";
        string PO = "";
        string ETA = "";
        string M = "";

        // For Inverter
        string IL = "";
        string IApl = "";
        string ISMPL = "";
        string IW = "";
        string IT = "";
        string divinverter = "false";
        string IO = "";
        string IETA = "";
        string IM = "";


        var Data = new BookingData();
        if (ID != "" && ProjectID != "")
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ID);
            string nm = stloc.CompanyLocation;
            sttblprojectqtypanelinverter stpanelqty = ClstblProjects.tblProjects_SelectByProjectqty(stPro.ProjectNumber, nm);
            sttblprojectqtypanelinverter stinverterqty = ClstblProjects.tblProjects_SelectByProjectinverterqty(stPro.ProjectNumber);

            #region Panel Section DataBind
            //Panel Live Qty
            PL = stpanelqty.StockQuantitypanel != "" ? stpanelqty.StockQuantitypanel : "0";

            //ArisePanelCount
            if (stPro.PanelBrand != null && stPro.PanelBrandID != "")
            {
                DataTable dtArise = ClstblProjects.GetItemCountompanyWise(stPro.PanelBrandID, "1", nm);
                if (dtArise.Rows.Count > 0)
                {
                    if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                    {
                        Apl = dtArise.Rows[0]["Qty"].ToString();
                    }
                    else
                    {
                        Apl = "0";

                    }

                }
                else
                {
                    Apl = "0";
                }
            }
            else
            {
                Apl = "0";
            }

            //SolarMinerPanelCount
            if (stPro.PanelBrandID != null && stPro.PanelBrandID != "")
            {
                // SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.PanelBrandID);
                DataTable DtSolarMinor = ClstblProjects.GetItemCountompanyWise(stPro.PanelBrandID, "2", nm);
                //DataTable DtSolarMinor = ClstblProjects.GetItemCountompanyWise(stockItems.FixStockItemID, "2");
                if (DtSolarMinor.Rows.Count > 0)
                {
                    if (DtSolarMinor.Rows[0]["Qty"].ToString() != null && DtSolarMinor.Rows[0]["Qty"].ToString() != "")
                    {
                        SMPL = DtSolarMinor.Rows[0]["Qty"].ToString();
                    }
                    else
                    {
                        SMPL = "0";

                    }
                }
                else
                {
                    SMPL = "0";

                }
            }
            else
            {
                SMPL = "0";
            }

            //Panel WholeSale Qty
            int panelWhOrderQty = 0;
            if (!string.IsNullOrEmpty(stPro.PanelBrandID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(stPro.PanelBrandID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        panelWhOrderQty = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            WhOrderQty = panelWhOrderQty.ToString();

            //Panel T
            int Totapl = 0;
            int Totsmpl = 0;
            int Totwh = 0;
            int livepnl = 0;

            int TotalPnal = 0;
            if (Apl != null && Apl != "")
            {
                Totapl = Convert.ToInt32(Apl);
            }
            if (SMPL != null && SMPL != "")
            {
                Totsmpl = Convert.ToInt32(SMPL);
            }
            if (WhOrderQty != null && WhOrderQty != "")
            {
                Totwh = Convert.ToInt32(WhOrderQty);
            }
            if (PL != null && PL != "")
            {
                livepnl = Convert.ToInt32(PL);
            }
            try
            {
                int PnalCount = Totapl + Totsmpl + Totwh;
                // int livepnl = Convert.ToInt32();
                PT = Convert.ToString(livepnl - PnalCount);
                if (PT != null && PT != "")
                {
                    TotalPnal = Convert.ToInt32(PT);
                    if (stPro.ProjectStatusID != "8" && stPro.ProjectStatusID != "2")//planned and deprec
                    {
                        if (TotalPnal <= 0)
                        {
                            //divpanel.Visible = true;
                            divpanel = "true";
                        }
                        else
                        {
                            //divpanel.Visible = false;
                            divpanel = "false";
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            //Panel Order Qty --O
            int panelOrderQty = 0;
            if (!string.IsNullOrEmpty(stPro.PanelBrandID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(stPro.PanelBrandID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        panelOrderQty = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            PO = panelOrderQty.ToString();
            //if (lblPO.Text == "0")
            //{
            //    lblPO.Enabled = false;
            //}
            //else
            //{
            //    lblPO.Enabled = true;
            //}

            //Panel ETA
            int panelOrderQtyETA = 0;
            if (!string.IsNullOrEmpty(stPro.PanelBrandID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(stPro.PanelBrandID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        panelOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            ETA = panelOrderQtyETA.ToString();

            //Panel Min Qty   
            string panelminQtu = "0";
            if (!string.IsNullOrEmpty(stPro.PanelBrandID) || stPro.PanelBrandID != "")
            {
                // DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(st.PanelBrandID, st.StockAllocationStore);
                DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(stPro.PanelBrandID, nm);
                if (dtLive.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                    {
                        panelminQtu = dtLive.Rows[0]["MinQty"].ToString();
                    }
                }
            }
            M = panelminQtu;
            #endregion

            #region Inverter Section DataBind
            //Inverter Live Qty
            DataTable dtinverter = ClstblProjects.tblProjects_Selectinverterqty(stPro.ProjectNumber, nm);
            int InverterliveQty1 = 0;
            int InverterliveQty2 = 0;
            int InverterliveQty3 = 0;
            if (dtinverter.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity1"].ToString()))
                {
                    InverterliveQty1 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity1"].ToString());
                }
                if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity2"].ToString()))
                {
                    InverterliveQty2 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity2"].ToString());
                }
                if (!string.IsNullOrEmpty(dtinverter.Rows[0]["StockQuantity3"].ToString()))
                {
                    InverterliveQty3 = Convert.ToInt32(dtinverter.Rows[0]["StockQuantity3"].ToString());
                }
            }

            IL = Convert.ToString(InverterliveQty1 + InverterliveQty3 + InverterliveQty3);

            //AriseSolar InverterCount
            int inv1 = 0;
            int inv2 = 0;
            int inv3 = 0;
            if (stPro.InverterDetailsID != null && stPro.InverterDetailsID != "")
            {

                DataTable dtArise = ClstblProjects.GetItemCountompanyWise(stPro.InverterDetailsID, "1", nm);
                if (dtArise.Rows.Count > 0)
                {
                    if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                    {
                        inv1 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                    }
                }
            }
            if (stPro.SecondInverterDetailsID != null && stPro.SecondInverterDetailsID != "")
            {

                DataTable dtArise = ClstblProjects.GetItemCountompanyWise(stPro.SecondInverterDetailsID, "1", nm);
                if (dtArise.Rows.Count > 0)
                {
                    if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                    {
                        inv2 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                    }
                }
            }
            if (stPro.ThirdInverterDetailsID != null && stPro.ThirdInverterDetailsID != "")
            {

                DataTable dtArise = ClstblProjects.GetItemCountompanyWise(stPro.ThirdInverterDetailsID, "1", nm);
                if (dtArise.Rows.Count > 0)
                {
                    if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                    {
                        inv3 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                    }
                }
            }
            IApl = Convert.ToString(inv1 + inv2 + inv3);

            //SolarMinor InverterCount
            int invSm1 = 0;
            int invSm2 = 0;
            int invSm3 = 0;
            if (stPro.InverterDetailsID != null && stPro.InverterDetailsID != "")
            {
                // SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.InverterDetailsID);
                DataTable dtArise = ClstblProjects.GetItemCountompanyWise(stPro.InverterDetailsID, "2", nm);
                if (dtArise.Rows.Count > 0)
                {
                    if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                    {
                        invSm1 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                    }
                }
            }
            if (stPro.SecondInverterDetailsID != null && stPro.SecondInverterDetailsID != "")
            {
                //SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.SecondInverterDetailsID);
                DataTable dtArise = ClstblProjects.GetItemCountompanyWise(stPro.SecondInverterDetailsID, "2", nm);
                if (dtArise.Rows.Count > 0)
                {
                    if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                    {
                        invSm2 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                    }
                }
            }
            if (stPro.ThirdInverterDetailsID != null && stPro.ThirdInverterDetailsID != "")
            {
                //SttblStockItems stockItems = ClstblStockItems.tblStockItems_SelectByStockItemIDForSolarMinor(st.InverterDetailsID);
                DataTable dtArise = ClstblProjects.GetItemCountompanyWise(stPro.ThirdInverterDetailsID, "2", nm);
                if (dtArise.Rows.Count > 0)
                {
                    if (dtArise.Rows[0]["Qty"].ToString() != null && dtArise.Rows[0]["Qty"].ToString() != "")
                    {
                        invSm3 = Convert.ToInt32(dtArise.Rows[0]["Qty"].ToString());
                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                    }
                }
            }
            ISMPL = Convert.ToString(invSm1 + invSm2 + invSm3);

            //Inverter Wholesale
            int InvertOrderwhQty1 = 0;
            int InvertOrderwhQty2 = 0;
            int InvertOrderwhQty3 = 0;
            if (!string.IsNullOrEmpty(stPro.InverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(stPro.InverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertOrderwhQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(stPro.SecondInverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertOrderwhQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(stPro.ThirdInverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertOrderwhQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            IW = Convert.ToString(InvertOrderwhQty1 + InvertOrderwhQty2 + InvertOrderwhQty3);
            //if (lblIW.Text == "0")
            //{
            //    lblIW.Enabled = false;
            //}
            //else
            //{
            //    lblIW.Enabled = true;
            //}

            try
            {
                int ariseinv = 0;
                int sminv = 0;
                int whinv = 0;
                int liveinv = 0;
                int ttlinv = 0;
                if (IApl != null && IApl != "")
                {
                    ariseinv = Convert.ToInt32(IApl);
                }
                if (ISMPL != null && ISMPL != "")
                {
                    sminv = Convert.ToInt32(ISMPL);
                }
                if (IW != null && IW != "")
                {
                    whinv = Convert.ToInt32(IW);
                }
                if (IL != null && IL != "")
                {
                    liveinv = Convert.ToInt32(IL);
                }

                int InvCount = ariseinv + sminv + whinv;
                // int LIVEINVCount = ();
                IT = Convert.ToString(liveinv - InvCount);
                if (IT != null && IT != "")
                {
                    ttlinv = Convert.ToInt32(IT);
                    if (stPro.ProjectStatusID != "8" && stPro.ProjectStatusID != "2")//planned and deprec
                    {
                        if (ttlinv <= 0)
                        {
                            //divinverter.Visible = true;
                            divinverter = "true";
                        }
                        else
                        {
                            divinverter = "false";
                        }
                    }
                }

            }
            catch (Exception ex)
            { }

            //Invert Order Qty
            int InvertOrderQty1 = 0;
            int InvertOrderQty2 = 0;
            int InvertOrderQty3 = 0;
            if (!string.IsNullOrEmpty(stPro.InverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                // DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.InverterDetailsID, st.StockAllocationStore);
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(stPro.InverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertOrderQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(stPro.SecondInverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertOrderQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(stPro.ThirdInverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertOrderQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            IO = Convert.ToString(InvertOrderQty1 + InvertOrderQty2 + InvertOrderQty3);

            //Invertr ETA Order Qty
            int InvertETAOrderQty1 = 0;
            int InvertETAOrderQty2 = 0;
            int InvertETAOrderQty3 = 0;
            if (!string.IsNullOrEmpty(stPro.InverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                // DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty(st.InverterDetailsID, st.StockAllocationStore);
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(stPro.InverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertETAOrderQty1 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.SecondInverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(stPro.SecondInverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertETAOrderQty2 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.ThirdInverterDetailsID) && !string.IsNullOrEmpty(nm))
            {
                DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(stPro.ThirdInverterDetailsID, nm);
                if (dtorderQty.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                    {
                        InvertETAOrderQty3 = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                    }
                }
            }
            IETA = Convert.ToString(InvertETAOrderQty1 + InvertETAOrderQty2 + InvertETAOrderQty3);

            //Invert Min Qty
            int InvertminQtu1 = 0;
            int InvertminQtu2 = 0;
            int InvertminQtu3 = 0;
            if (!string.IsNullOrEmpty(stPro.InverterDetailsID) || stPro.InverterDetailsID != "")
            {
                DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(stPro.InverterDetailsID, nm);
                if (dtLive.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                    {
                        InvertminQtu1 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.SecondInverterDetailsID) || stPro.SecondInverterDetailsID != "")
            {
                DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(stPro.SecondInverterDetailsID, nm);
                if (dtLive.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                    {
                        InvertminQtu2 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(stPro.ThirdInverterDetailsID) || stPro.ThirdInverterDetailsID != "")
            {
                DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty(stPro.ThirdInverterDetailsID, nm);
                if (dtLive.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtLive.Rows[0]["MinQty"].ToString()))
                    {
                        InvertminQtu3 = Convert.ToInt32(dtLive.Rows[0]["MinQty"].ToString());
                    }
                }
            }

            IM = Convert.ToString(InvertminQtu1 + InvertminQtu2 + InvertminQtu3);

            #endregion

            Data = new BookingData
            {
                ID = ID,
                PL = PL,
                Apl = Apl,
                SMPL = SMPL,
                PW = WhOrderQty,
                PT = PT,
                divpanel = divpanel,
                PO = PO,
                ETA = ETA,
                M = M,

                IL = IL,
                IApl = IApl,
                ISMPL = ISMPL,
                IW = IW,
                IT = IT,
                divinverter = divinverter,
                IO = IO,
                IETA = IETA,
                IM = IM,
            };
        }
        return JsonConvert.SerializeObject(Data);
    }

    protected void btnBookingDataSave_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        // installbookingtracker.openModal = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        if (ProjectID != string.Empty)
        {
            //string InstallDays = txtInstallDays.Text;
            string ElecDistOK = Convert.ToString(chkElecDistOK.Checked);
            string ElecDistApproved = txtElecDistApproved.Text.Trim();
            string InstallBookingDate = txtInstallBookingDate.Text.Trim();

            string StockAllocationStore = ddlStockAllocationStore.SelectedValue;
            string Installer = ddlInstaller.SelectedValue;
            string Designer = ddlDesigner.SelectedValue;
            string Electrician = ddlElectrician.SelectedValue;
            string ProjectNotes = txtBKProjectNotes.Text;
            string InstallerNotes = txtBKInstallerNotes.Text;
            string MeterInstallerNotes = txtBKMeterInstallerNotes.Text;

            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (ddlInstaller.SelectedValue != null && ddlInstaller.SelectedValue != "")
            {
                if (txtInstallBookingDate.Text != null && txtInstallBookingDate.Text != "")
                {
                    DataTable dt = ClsProjectSale.GetInstallerData(ddlInstaller.SelectedValue, txtInstallBookingDate.Text);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count > 1)
                        {
                            string ProjectNumber = "";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                ProjectNumber += dt.Rows[i]["ProjectNumber"].ToString() + ",";
                            }

                            string Msg = "This Installer " + dt.Rows[0]["InstallerName"].ToString() + " Is Already Booked on " + txtInstallBookingDate.Text + " for Projectnumber " + ProjectNumber.TrimEnd(',');
                            MsgError(Msg);
                            return;
                        }
                    }
                }
            }

            if (txtInstallBookingDate.Text != null && txtInstallBookingDate.Text != "")
            {
                string InstallDate = Convert.ToDateTime(txtInstallBookingDate.Text).ToShortDateString();
                string EndDate = Convert.ToDateTime(DateTime.Now.AddHours(14)).AddDays(56).ToShortDateString();
                //bool s1 = ClsProjectSale.tblprojects_UpdateInstallEndDate(ProjectID, EndDate);
            }

            bool UpdateBooking = false;
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string UpdatedBy = stEmp.EmployeeID;

            UpdateBooking = ClstblSalesinfo.tblProjects_UpdateBookingData(ProjectID, ElecDistOK, Installer, Designer, Electrician, StockAllocationStore, InstallerNotes, ProjectNotes, MeterInstallerNotes, ElecDistApproved, InstallBookingDate, UpdatedBy);

            if (UpdateBooking)
            {
                bool UpdateUInstallBookDate = ClsQuickStock.tbl_PickListLog_Update_InstallBookedDateByProjectID(ProjectID, InstallBookingDate);

                //Project Status cant change if Project in "Mtn-New Inquiry"
                SttblProjects stmntc = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                if (stmntc.ProjectStatusID != "15")
                {
                    /* -------------------- Job Booked -------------------- */
                    if (txtInstallBookingDate.Text.Trim() != string.Empty)
                    {
                        if (stPro.ProjectStatusID == "3")
                        {
                            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "11");
                            if (dtStatus.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, UpdatedBy, stPro.NumberPanels);
                            }
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(stPro.ProjectStatusID) > 11 || stPro.ProjectStatusID == "10" || stPro.ProjectStatusID == "5")
                        {
                            RequiredFieldValidatorIBDate.Visible = true;
                        }
                        else
                        {
                            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                            if (dtStatus.Rows.Count > 0)
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, stPro.NumberPanels);
                            }
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    /* ---------------------------------------------------- */
                }
                SetAdd1();
            }
            else
            {
                SetError1();
            }

            BindCommonDetails();
            BindGrid(0);
        }
    }

    protected void btnRemoveInst_Click(object sender, EventArgs e)
    {
        lblMassage.Text = "Are You Sure You Went to Remove Installation With Picklist.?";
        lnkDeleteInstallation.Visible = true;
        lnkDeletePickList.Visible = false;
        ModalPopupExtenderDeleteNew.Show();

    }

    static string sha256(string randomString)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    async Task RunAsync(string Uname, string pwd)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
            SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
            SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
            string ProjectTypeName = "New";
            if (stProject.ProjectTypeID == "3")
            {
                ProjectTypeName = "Adding";
            }
            if (stProject.ProjectTypeID == "7")
            {
                ProjectTypeName = "Replacing";
            }
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
            SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
            SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
            DataTable dt = ClstblCustomers.tbl_formbayunittypeID_SelectByUnittype(stcustomer.unit_type);
            DataTable dt2 = ClstblCustomers.tbl_formbayStreettypeID_SelectByStreetCode(stcustomer.street_type);
            string installbooking = stProject.InstallBookingDate;
            Decimal stcrate = 0;
            if (!string.IsNullOrEmpty(installbooking))
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt3 = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                stcrate = Convert.ToDecimal(dt3.Rows[0]["STCRate"]);
            }
            string Unitid = "";
            String StreetId = "";
            if (dt.Rows.Count > 0)
            {
                Unitid = dt.Rows[0]["Id"].ToString();
            }
            if (dt2.Rows.Count > 0)
            {
                StreetId = dt2.Rows[0]["GreenBoatStreetType"].ToString();
            }

            string SystemMountingType = "Ground mounted or free standing";
            string typeofconnection = "Connected to an electricity grid with battery storage";
            if (stProject.InstallBase == "1")
            {
                SystemMountingType = "Building or structure";
            }
            if (Convert.ToBoolean(stProject.GridConnected))
            {
                typeofconnection = "Connected to an electricity grid without battery storage";
            }
            try
            {
                //JObject oJsonObject = new JObject();
                //oJsonObject.Add("Username", "arisesolar");
                //oJsonObject.Add("Password", "arisesolar1");

                JObject oJsonObject = new JObject();
                oJsonObject.Add("Username", Uname);
                oJsonObject.Add("Password", pwd);

                try
                {
                    var oTaskPostAsync = await client.PostAsync("https://api.greenbot.com.au/api/Account/Login", new StringContent(oJsonObject.ToString(), Encoding.UTF8, "application/json"));

                    bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
                    if (IsSuccess)
                    {
                        var tokenJson = await oTaskPostAsync.Content.ReadAsStringAsync();
                        RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(tokenJson);
                        string AccessToken = RootObject.TokenData.access_token;

                        litmessage.Text = AccessToken.ToString();
                        client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                        var panellist = new List<Panel>();
                        SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(stProject.PanelBrandID);
                        panellist.Add(new Panel { Brand = stPanel.StockManufacturer, Model = stPanel.StockModel, NoOfPanel = stProject.NumberPanels });

                        var inverterlist = new List<Inverter>();
                        SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(stProject.InverterDetailsID);
                        inverterlist.Add(new Inverter { Brand = stInverter.StockManufacturer, Model = stInverter.StockModel, Series = stInverter.StockSeries });

                        if (stProject.SecondInverterDetailsID != "" && stProject.SecondInverterDetailsID != "0")
                        {
                            SttblStockItems stSecondInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(stProject.SecondInverterDetailsID);

                            inverterlist.Add(new Inverter { Brand = stSecondInverter.StockManufacturer, Model = stSecondInverter.StockModel, Series = stSecondInverter.StockSeries });
                        }
                        //var testlist = new List<JobStcDetailsPanelBrand>();
                        //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                        //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                        try
                        {
                            var content = new CreateJob
                            {

                                VendorJobId = Convert.ToInt32(stProject.ProjectNumber),
                                BasicDetails = new BasicDetails
                                {
                                    JobType = 1,
                                    RefNumber = stProject.ProjectNumber,
                                    JobStage = 1,
                                    Title = stcustomer.Customer,
                                    StrInstallationDate = Convert.ToDateTime(stProject.InstallBookingDate),
                                    Priority = 1,
                                    Description = stProject.ProjectNotes

                                },
                                JobOwnerDetails = new JobOwnerDetails
                                {

                                    OwnerType = "Individual",
                                    //CompanyName = "Customer Name",
                                    FirstName = stCustContact.ContFirst,
                                    LastName = stCustContact.ContLast,
                                    Email = stCustContact.ContEmail,
                                    Phone = stCustContact.ContMobile,
                                    Mobile = stCustContact.ContMobile,
                                    AddressID = 1,
                                    UnitTypeID = Unitid,
                                    UnitNumber = stcustomer.unit_number,
                                    StreetName = stcustomer.street_name,
                                    StreetNumber = stcustomer.street_number,
                                    StreetTypeId = StreetId,
                                    Town = stcustomer.StreetCity,
                                    State = stcustomer.StreetState,
                                    PostCode = stcustomer.StreetPostCode,
                                    IsPostalAddress = false
                                },
                                JobInstallationDetails = new JobInstallationDetails
                                {
                                    AddressId = 1,
                                    UnitTypeID = Unitid,
                                    UnitNumber = stcustomer.unit_number,
                                    StreetName = stProject.street_name,
                                    StreetNumber = stProject.street_number,
                                    StreetTypeId = StreetId,
                                    Town = stProject.InstallCity,
                                    State = stProject.InstallState,
                                    PostCode = stProject.InstallPostCode,
                                    //isPostalAddress = false,
                                    DistributorID = Convert.ToInt32(stdist.GreenBoatDistributor),
                                    ElectricityProviderID = Convert.ToInt32(stelect.ElectricityProviderId),
                                    NMI = stProject.NMINumber,
                                    MeterNumber = stProject.MeterNumber1,
                                    PhaseProperty = stProject.MeterPhase,
                                    PropertyType = stcustomer.ResCom == "1" ? "Residential" : "Commercial",
                                    SingleMultipleStory = sthouse.HouseType,
                                    InstallingNewPanel = ProjectTypeName,
                                    //NoOfPanels = Convert.ToInt32(stProject.NumberPanels),
                                    ExistingSystem = false
                                },
                                JobSystemDetails = new JobSystemDetails
                                {
                                    SystemSize = Convert.ToDecimal(stProject.SystemCapKW),
                                    //InstallationType = "Other"
                                    //SystemModel = "TestModel",
                                    //NoOfPanel = 5
                                },
                                JobStcDetails = new JobStcDetails
                                {
                                    TypeOfConnection = typeofconnection,
                                    SystemMountingType = SystemMountingType,
                                    // DeemingPeriod = "13",
                                    DeemingPeriod = stcrate.ToString(),
                                    MultipleSguAddress = "No"
                                },
                                Panel = panellist.ToArray(),
                                Inverter = inverterlist.ToArray(),
                                InstallerView = new InstallerView
                                {
                                    CECAccreditationNumber = stinst.Accreditation,
                                    IsAutoAddVisit = true,
                                    IsUpdate = false,
                                    //Email = "richa@meghtechnologies.com",
                                    FirstName = stinst.ContFirst,
                                    LastName = stinst.ContLast,
                                    Phone = stinst.ContMobile,
                                    Mobile = stinst.ContMobile,
                                    IsPostalAddress = true,
                                    //ElectricalContractorsLicenseNumber = "9876543214",
                                    AddressID = 1,
                                    //UnitTypeID = 1,
                                    //UnitNumber = "1",
                                    StreetName = stcustomerInst.street_name,
                                    StreetNumber = stcustomerInst.street_number,
                                    StreetTypeId = 1,
                                    Town = stcustomerInst.StreetCity,
                                    State = stcustomerInst.StreetState,
                                    PostCode = stcustomerInst.StreetPostCode,

                                    SeDesignRoleId = 1,
                                },
                                DesignerView = new DesignerView
                                {
                                    CECAccreditationNumber = stinst.Accreditation,
                                    IsUpdate = false,
                                    //Email = "richa@meghtechnologies.com",
                                    FirstName = stinst.ContFirst,
                                    LastName = stinst.ContLast,
                                    Phone = stinst.ContMobile,
                                    Mobile = stinst.ContMobile,
                                    IsPostalAddress = true,
                                    //ElectricalContractorsLicenseNumber = "9876543214",
                                    AddressID = 1,
                                    //UnitTypeID = 1,
                                    //UnitNumber = "1",
                                    StreetName = stcustomerInst.street_name,
                                    StreetNumber = stcustomerInst.street_number,
                                    StreetTypeId = 1,
                                    Town = stcustomerInst.StreetCity,
                                    State = stcustomerInst.StreetState,
                                    PostCode = stcustomerInst.StreetPostCode,

                                    SeDesignRoleId = 1,
                                },
                                ElectricianView = new ElectricianView
                                {
                                    CECAccreditationNumber = stinst.Accreditation,
                                    LicenseNumber = stinst.ElecLicence,
                                    //Email = "richa@meghtechnologies.com",
                                    FirstName = stinst.ContFirst,
                                    LastName = stinst.ContLast,
                                    Phone = stinst.ContMobile,
                                    Mobile = stinst.ContMobile,
                                    IsPostalAddress = true,
                                    //ElectricalContractorsLicenseNumber = "9876543214",
                                    AddressID = 1,
                                    //UnitTypeID = 1,
                                    //UnitNumber = "1",
                                    StreetName = stcustomerInst.street_name,
                                    StreetNumber = stcustomerInst.street_number,
                                    StreetTypeId = 1,
                                    Town = stcustomerInst.StreetCity,
                                    State = stcustomerInst.StreetState,
                                    PostCode = stcustomerInst.StreetPostCode,

                                    SeDesignRoleId = 1,
                                }

                            };
                            var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                            // lbltesting.Text = temp.ToString();

                            if (stProject.GreenBotFlag == "False")
                            {
                                var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/CreateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                                bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                                var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                                RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);
                                //Response.Write(JsonString + "   " + IsSuccessJob.ToString() + " Message: " + RootObjectJob.Message + " statusCode: " + RootObjectJob.StatusCode + "Status:" + RootObjectJob.Status);
                                //Response.Write(Convert.ToDateTime(stProject.InstallBookingDate));
                                //Response.End();

                                if (IsSuccessJob == true)
                                {
                                    bool GreenBotUpdate = ClstblProjects.tblProjects_update_IsGreenBot("true", ProjectID);
                                }

                                litmessage.Text = RootObjectJob.Message;
                                PanSuccess.Visible = true;
                            }
                            else if (stProject.GreenBotFlag == "True")
                            {
                                var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/UpdateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                                bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                                var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                                RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);

                                litmessage.Text = RootObjectJob.Message;
                                SetAdd1();
                                PanSuccess.Visible = true;
                            }

                        }
                        catch (Exception e)
                        {
                            litmessage.Text = e.Message;
                            PanError.Visible = true;
                            //Console.WriteLine(e.Message);
                        }
                    }

                }
                catch (Exception ex)
                {
                    litmessage.Text = ex.Message;
                    PanError.Visible = true;
                }


            }
            catch (Exception e)
            {
                litmessage.Text = e.Message;
                //Console.WriteLine(e.Message);
            }
        }
    }

    public void FormbayRunAsync()
    {
        // lbltesting.Text = "hii2";
        try
        {
            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }

            if (ProjectID != string.Empty)
            {
                // lbltesting.Text = "hii3";
                SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                // SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
                // SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
                SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
                string ProjectTypeName = "1";
                if (stProject.ProjectTypeID == "2")
                {
                    ProjectTypeName = "1";
                }
                else if (stProject.ProjectTypeID == "3")
                {
                    ProjectTypeName = "2";
                }
                else if (stProject.ProjectTypeID == "7")
                {
                    ProjectTypeName = "3";
                }

                SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
                //SttblContacts stelect = ClstblContacts.tblContacts_SelectByContactID(stProject.Electrician);
                //SttblContacts stdesig = ClstblContacts.tblContacts_SelectByContactID(stProject.Designer);
                SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
                SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
                SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
                Sttbl_formbaystreettype stStreeType = ClstblStreetType.tbl_formbaystreettype_SelectByStreetCode(stProject.street_type);
                string installbooking = stProject.InstallBookingDate;
                Decimal stcrate = 0;
                if (!string.IsNullOrEmpty(installbooking))
                {
                    string date = Convert.ToDateTime(installbooking).ToShortDateString();
                    DateTime dt1 = Convert.ToDateTime(date);
                    String year = dt1.Year.ToString();
                    DataTable dt3 = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                    stcrate = Convert.ToDecimal(dt3.Rows[0]["STCRate"]);
                }
                string SystemMountingType = "Ground mounted or free standing";
                string typeofconnection = "Connected to an electricity grid with battery storage";
                if (stProject.InstallBase == "1")
                {
                    SystemMountingType = "Building or structure";
                }
                if (Convert.ToBoolean(stProject.GridConnected))
                {
                    typeofconnection = "Connected to an electricity grid without battery storage";
                }
                try
                {
                    //  lbltesting.Text = "hii4";
                    Dictionary<string, string> oJsonObject = new Dictionary<string, string>();
                    /// JObject oJsonObject = new JObject();
                    oJsonObject.Add("Username", "arisesolar");
                    oJsonObject.Add("Password", "Arise@123");
                    oJsonObject.Add("redirect_uri", "http://api.quickforms.com.au/");
                    oJsonObject.Add("client_id", "5D95C4B5-EE01-4D87-B1BC-B4C159CBEFDF");
                    // oJsonObject.Add("scope", "JobData");
                    oJsonObject.Add("scope", "CreateJob");
                    oJsonObject.Add("response_type", "code");

                    HttpClient client = new HttpClient();

                    var content1 = new FormUrlEncodedContent(oJsonObject);
                    var result = client.PostAsync("http://api.quickforms.com.au/auth/logon", content1).Result;
                    var tokenJson = result.Content.ReadAsStringAsync();
                    bool IsSuccess = result.IsSuccessStatusCode;
                    if (IsSuccess)
                    {
                        TokenObject RootObject = JsonConvert.DeserializeObject<TokenObject>(tokenJson.Result);
                        string AccessToken = RootObject.access_token;
                        string CustomerID = RootObject.CustomerUserID;
                        //lbltesting.Text = AccessToken;
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                        var panellist = new List<Panel>();
                        panellist.Add(new Panel { Brand = stProject.PanelBrand, Model = stProject.PanelModel, NoOfPanel = stProject.NumberPanels, STC = stProject.SystemCapKW, size = stProject.PanelOutput });
                        var inverterlist = new List<Inverter>();
                        inverterlist.Add(new Inverter { Brand = stProject.InverterBrand, Model = stProject.InverterModel, Series = stProject.InverterSeries, size = stProject.InverterOutput, noofinverter = Convert.ToInt32(stProject.inverterqty) });
                        var content = new CreateJob
                        {
                            Arisesolarid = Convert.ToInt32(ProjectID),
                            VendorJobId = Convert.ToInt32(stProject.ProjectNumber),
                            BasicDetails = new BasicDetails
                            {
                                JobType = Convert.ToInt32(stcustomer.ResCom),
                                RefNumber = stProject.ProjectNumber,
                                //JobStage = Convert.ToInt32(stcustomer.ResCom),
                                JobStage = Convert.ToInt32(ProjectTypeName),
                                Jobstatus = 1,
                                Createddate = DateTime.Now,
                                StrInstallationDate = Convert.ToDateTime(stProject.InstallBookingDate),
                                Description = stProject.ProjectNotes,
                            },
                            JobOwnerDetails = new JobOwnerDetails
                            {
                                //  OwnerType = "Individual",
                                CompanyName = stCustContact.ContFirst + " " + stCustContact.ContLast,
                                FirstName = stCustContact.ContFirst,
                                LastName = stCustContact.ContLast,
                                Phone = stCustContact.ContMobile,
                                Email = stCustContact.ContEmail,
                                Mobile = stCustContact.ContMobile,
                                // AddressID = 1,
                                UnitTypeID = stProject.unit_type,
                                UnitNumber = stProject.unit_number,
                                StreetName = stProject.street_name,
                                StreetNumber = stProject.street_number,
                                StreetTypeId = stStreeType.StreetCode,
                                StreetAddress = stProject.street_address,
                                Town = stcustomer.StreetCity,
                                State = stcustomer.StreetState,
                                PostCode = stcustomer.StreetPostCode,
                                IsPostalAddress = true,
                            },
                            JobInstallationDetails = new JobInstallationDetails
                            {
                                // AddressId = 1,
                                Inst_UnitNo = stProject.unit_number,
                                Inst_UnitType = stProject.unit_type,
                                Inst_StreetName = stProject.street_name,
                                Inst_StreetNo = stProject.street_number,
                                Inst_StreetType = stStreeType.StreetCode,
                                Inst_StreetAddress = stProject.street_address,
                                Inst_Suburb = stcustomer.StreetCity,
                                Inst_PostCode = stcustomer.StreetPostCode,
                                Inst_State = stcustomer.StreetState,

                                //isPostalAddress = false,
                                //  DistributorID = Convert.ToInt32(stdist.GreenBoatDistributor),
                                // ElectricityProviderID = Convert.ToInt32(stelect.ElectricityProviderId),
                                NMI = stProject.NMINumber,
                                // MeterNumber = stProject.MeterNumber1,
                                //  PhaseProperty = stProject.MeterPhase,
                                //PropertyType = stcustomer.ResCom == "1" ? "Residential" : "Commercial",
                                SingleMultipleStory = sthouse.HouseType,
                                Rooftype = stProject.RoofType,
                                RegPlanNo = stProject.RegPlanNo,
                                LotNo = stProject.LotNumber,

                                // InstallingNewPanel = ProjectTypeName,
                                //NoOfPanels = Convert.ToInt32(stProject.NumberPanels),
                                //  ExistingSystem = false 
                            },
                            JobSystemDetails = new JobSystemDetails
                            {
                                Systemtype = 1,
                                MountingType = 1,
                                ConnectionType = 1,
                                SystemSize = Convert.ToDecimal(stProject.SystemCapKW),
                                //InstallationType = "Other"
                                //SystemModel = "TestModel",
                                //NoOfPanel = 5
                            },
                            JobStcDetails = new JobStcDetails
                            {
                                TypeOfConnection = typeofconnection,
                                SystemMountingType = SystemMountingType,
                                //DeemingPeriod = "13",
                                DeemingPeriod = stcrate.ToString(),
                                MultipleSguAddress = "No"
                            },
                            Panel = panellist.ToArray(),
                            Inverter = inverterlist.ToArray(),
                            InstallerView = new InstallerView
                            {
                                Installerid = stinst.formbayid,
                                FirstName = stinst.ContFirst,
                                LastName = stinst.ContLast,
                                FullName = stProject.InstallerName,
                                //Customerid = 2,
                                Customerid = Convert.ToInt32(CustomerID),
                                CECAccreditationNumber = stinst.Accreditation,
                                SeDesignRoleId = 1,
                                IsCompleteUnit = stProject.ProjectTypeID == "3" ? "No" : "Yes",
                                IsMoreSGU = stProject.ProjectTypeID == "3" ? "No" : "Yes",
                            },
                            ElectricianView = new ElectricianView
                            {
                                FullName = stProject.ElectricianName
                            },
                            DesignerView = new DesignerView
                            {
                                FullName = stProject.DesignerName
                            },
                            quickformGuid = stProject.quickformGuid
                        };

                        var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                        //lbltesting.Text = temp;
                        if (string.IsNullOrEmpty(stProject.quickformGuid))
                        {
                            using (var client1 = new HttpClient())
                            {
                                client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/Createjob/jobData");

                                //HTTP POST
                                var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                                postTask.Wait();

                                var resultdata = postTask.Result;
                                // lbltesting.Text = resultdata.ToString();
                                if (resultdata.IsSuccessStatusCode)
                                {
                                    var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                                    RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                                    //   litmessage.Text = jsondata.ToString();
                                    PanError.Visible = true;
                                    litmessage.Text = JObresult.Message;
                                    if (!string.IsNullOrEmpty(JObresult.guidvalue))
                                    {
                                        bool succ_apicall = ClstblProjects.tblproject_update_quickformFlag(ProjectID, JObresult.guidvalue);
                                        bool check = ClstblProjects.tblProjects_update_IsQuickForm("True", Request.QueryString["proid"]);
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                        //   lbltesting.Text = JObresult.Message;
                                    }
                                    else
                                    {
                                        //   lbltesting.Text = JObresult.Message.ToString();
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (var client1 = new HttpClient())
                            {
                                client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/Updatejob/jobData");

                                //HTTP POST
                                var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                                postTask.Wait();

                                var resultdata = postTask.Result;
                                // lbltesting.Text = resultdata.ToString();
                                if (resultdata.IsSuccessStatusCode)
                                {
                                    var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                                    RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                                    //   litmessage.Text = jsondata.ToString();
                                    PanError.Visible = true;
                                    litmessage.Text = JObresult.Message;
                                    if (!string.IsNullOrEmpty(JObresult.code))
                                    {
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                    }
                                }
                            }
                        }
                    }
                    else
                    { }
                }
                catch (Exception e522)
                {
                    lbltesting.Text = e522.Message;
                    //Console.WriteLine(e.Message);
                }
            }

        }
        catch (Exception e)
        {
            lbltesting.Text = e.Message;
            //Console.WriteLine(e.Message);
        }

    }

    protected async void btnGo_Click(object sender, EventArgs e)
    {
        if (ddlbtn.SelectedValue == "1") //New Picklist
        {
            GenratePickList();
        }
        if (ddlbtn.SelectedValue == "2")//GreenBoat
        {
            await RunAsync("arisesolar", "arisesolar1");
        }
        if (ddlbtn.SelectedValue == "3")//QuickForm
        {
            FormbayRunAsync();
            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }

            if (ProjectID != string.Empty)
            {
                SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                string quickformguid = stProject.quickformGuid;
            }
        }
        if (ddlbtn.SelectedValue == "4")
        {
            SendToBridgeSelect();
        }
        if (ddlbtn.SelectedValue == "5") // new GreenBoat
        {
            await RunAsync("ariseregistry", "arise189");
        }
    }

    private void SendToBridgeSelect()
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        //else
        //{
        //    ProjectID = HiddenField2.Value;
        //}
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            // SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
            // SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
            SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
            SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
            //SttblContacts stelect = ClstblContacts.tblContacts_SelectByContactID(stProject.Electrician);
            //SttblContacts stdesig = ClstblContacts.tblContacts_SelectByContactID(stProject.Designer);
            SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
            SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
            Sttbl_formbaystreettype stStreeType = ClstblStreetType.tbl_formbaystreettype_SelectByStreetCode(stProject.street_type);
            SttblStockItems stitems = ClstblStockItems.tblStockItems_SelectByStockItemID(stProject.PanelBrandID);
            string InverterDetailsID = "0";
            if (!string.IsNullOrEmpty(stProject.InverterDetailsID))
            {
                InverterDetailsID = stProject.InverterDetailsID;
            }
            SttblStockItems Inverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
            string SecondInverterDetailsID = "0";
            if (!string.IsNullOrEmpty(stProject.SecondInverterDetailsID))
            {
                SecondInverterDetailsID = stProject.SecondInverterDetailsID;
            }
            SttblStockItems secondInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
            string ThirdInverterDetailsID = "0";
            if (!string.IsNullOrEmpty(stProject.ThirdInverterDetailsID))
            {
                ThirdInverterDetailsID = stProject.ThirdInverterDetailsID;
            }
            SttblStockItems thirdInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ThirdInverterDetailsID);
            string panel = GetPanels(stProject, stitems);
            //string inverter = GetInverters(stProject, Inverter, secondInverter, thirdInverter);
            string inverter = GetInvertersnew(stProject, Inverter, secondInverter, thirdInverter);
            string iiavalue = "";
            if (stProject.ProjectTypeID == "1")
            {
                iiavalue = "No";
            }
            else
            {
                iiavalue = "No";
            }
            string ngcsvalue = "";
            if (stProject.GridConnected == "1")
            {
                ngcsvalue = "Yes";
            }
            else
            {
                ngcsvalue = "No";
            }
            string pobxnvalue = "";
            if (stProject.unit_type == "8")
            {
                pobxnvalue = stProject.unit_number;
            }
            string smtvalue = "";
            if (stProject.InstallBase == "1")
            {
                smtvalue = "Building or structure";
            }
            if (stProject.InstallBase == "2")
            {
                smtvalue = "Ground mounted or free standing";
            }
            string srtvalue = "";
            if (stProject.HouseTypeID == "1")
            {
                srtvalue = "Single";
            }
            else
            {
                srtvalue = "Multi";
            }
            string ampmvalue = "";
            if (stProject.InstallAM1 == "True")
            {
                ampmvalue = "AM";
            }
            else if (stProject.InstallAM2 == "False")
            {
                ampmvalue = "PM";
            }
            string InstallDate = "";
            if (stProject.InstallBookingDate != null && stProject.InstallBookingDate != "")
            {
                InstallDate = Convert.ToDateTime(stProject.InstallBookingDate).ToShortDateString();
            }

            var obj = new ClsBridgeSelect
            {
                crmid = stProject.ProjectNumber,
                crms = "",
                installer = stinst.Accreditation,
                mfs = panel,
                ifs = inverter,
                fn = stCustContact.ContFirst,
                ln = stCustContact.ContLast,
                ifn = stCustContact.ContFirst,
                iln = stCustContact.ContLast,
                m = stCustContact.ContMobile,
                im = stCustContact.ContMobile,
                ipa = stcustomer.StreetAddress,
                istp = stcustomer.brgStreetName,
                ot = "Individual",
                anzss = "Yes",
                at = "Physical",
                ac = stProject.InstallerNotes,
                bb = "",
                bm = "",
                cd = "",
                cecss = "Yes",
                cgst = "No",//0 or 1
                ctieg = "Connected to an electricity grid without battery storage",
                e = stCustContact.ContEmail,
                esd = "Yes",
                fp = stProject.RECRebate,
                iia = iiavalue,
                ibpoa = "No",
                icfc = "No",
                id = InstallDate,
                //id = InstallDate.ToString("dd-mm-yyyy"),//NEW
                ie = stCustContact.ContEmail,
                imto = iiavalue,
                ist = stProject.InstallState,
                ipc = stProject.InstallPostCode,
                iph = stcustomer.CustPhone,
                ipra = stProject.street_name,
                isb = stProject.InstallCity,
                istn = stcustomer.street_number,
                lat = "",
                lng = "",
                mea = "Yes",
                ngcs = ngcsvalue,
                nmi = stProject.NMINumber,
                noep = "",
                norp = "",
                p = stcustomer.CustPhone,
                pa = stcustomer.street_number + " " + stcustomer.street_name,
                pc = stcustomer.StreetPostCode,
                pobxn = pobxnvalue,
                potyp = "",
                pra = stcustomer.street_name,
                pt = stcustomer.SolarType,
                rn = "Arise Solar Pty Ltd",
                sb = stProject.InstallCity,
                sia = "Yes",
                siad = "Yes",
                smt = smtvalue,
                srt = srtvalue,
                st = stcustomer.StreetState,
                stcdp = "Ten years",
                stn = stcustomer.street_number,
                stp = stcustomer.street_type,
                tos = "S.G.U. - Solar (Deemed)",
                tp = stProject.NumberPanels,
                sw = "",
                sz = stProject.SystemCapKW,
                wd = "",
                sl = 0,
                ampm = ampmvalue,
                jt = "1",
                mtrn = stProject.MeterNumber1,
                dstbn = stProject.ElecDistributor,
                ismd = "1",
                trn = "QUICKFORM TRADES PTY.LTD."
                // stcdp= "Eleven years"
                ,
                peivf = "1",
                erpf = "1",
                eicrf = "1"
            };

            var json = new JavaScriptSerializer().Serialize(obj);
            json = json.Replace(@"\""", @"""");
            json = json.Replace(@"""{", "{");
            json = json.Replace(@"}""", "}");

            string encodeddata = EncodeTo64(json);

            string salt = "";
            string URL = "";


            //URL key 6d44470c6f7a1923d979f9d9209e298d626efccb0c35aea760fa93cd5a299b8cret
            //SALT is 1717ef54f2070a4c9ba451b9a4d15ba0f2851c2f7edbcf31f86c3b1454b8b2c5

            //salt = "1717ef54f2070a4c9ba451b9a4d15ba0f2851c2f7edbcf31f86c3b1454b8b2c5"; // salt need to be added
            //URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/6d44470c6f7a1923d979f9d9209e298d626efccb0c35aea760fa93cd5a299b8cret/job/create-or-edit"; // url need to be added
            salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
            URL = "https://us-central1-bs-897a3.cloudfunctions.net/handleJob?other=7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret";

            string csum = sha256(encodeddata + salt);
            string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            //request.Method = "POST";
            //request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;
            //using (Stream webStream = request.GetRequestStream())
            //using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            //{
            //    requestWriter.Write(DATA);
            //}

            //try
            //{
            //    WebResponse webResponse = request.GetResponse();
            //    using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
            //    using (StreamReader responseReader = new StreamReader(webStream))
            //    {
            //        string response = responseReader.ReadToEnd();

            //        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + response + "')", true);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
            //}

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = DATA.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(DATA);
            }

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();

                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + response + "')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
            }
        }
    }

    static public string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    private string GetPanels(SttblProjects stPro, SttblStockItems stp)
    {
        string fpanels = "";
        List<string> modelfields = new List<string>();
        modelfields.Add("n");
        modelfields.Add("np");
        modelfields.Add("rs");
        modelfields.Add("w");
        dynamic mexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelfields)
        {
            if (field == "n")
            {
                ((IDictionary<String, Object>)mexo).Add(field, stp.StockModel);
            }
            if (field == "np")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(stPro.NumberPanels) ? 0 : Convert.ToInt32(stPro.NumberPanels));
            }
            if (field == "rs")
            {
                ((IDictionary<String, Object>)mexo).Add(field, stp.RespSup);
            }
            if (field == "w")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(stp.StockSize) ? 0 : Convert.ToInt32(stp.StockSize));
            }
        }
        List<string> modelField = new List<string>();
        // my 'columns'
        //modelField.Add(stp.StockModel);
        string Model = stp.StockModel;
        string Model1 = Regex.Replace(Model, "[^a-zA-Z0-9-]", "_");
        modelField.Add(Model1);
        dynamic mmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelField)
        {
            ((IDictionary<String, Object>)mmexo).Add(field, mexo);
        }

        List<string> pnlmodelField = new List<string>();
        // my 'columns'
        pnlmodelField.Add("ms");
        pnlmodelField.Add("n");
        dynamic pnlmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in pnlmodelField)
        {
            if (field == "ms")
            {
                ((IDictionary<String, Object>)pnlmexo).Add(field, mmexo);
            }
            if (field == "n")
            {
                ((IDictionary<String, Object>)pnlmexo).Add(field, stp.StockManufacturer);
            }
        }

        List<string> fields = new List<string>();
        // my 'columns'

        fields.Add(Regex.Replace(stp.StockManufacturer, "[^a-zA-Z0-9-]", "_")); //var nstr = str.replace(/[^A-Z0-9^-]+/ig, "_"); return nstr;//
                                                                                //fields.Add(Regex.Replace(stp.StockManufacturer, @"\s^[a-zA-Z0-9_]*$", "_"));

        dynamic exo = new System.Dynamic.ExpandoObject();
        foreach (string field in fields)
        {
            ((IDictionary<String, Object>)exo).Add(field, pnlmexo);
        }
        fpanels = Newtonsoft.Json.JsonConvert.SerializeObject(exo);
        return fpanels;
    }

    private string GetInvertersnew(SttblProjects stPro, SttblStockItems sti, SttblStockItems sti2, SttblStockItems sti3)
    {
        string fInverters = "";
        List<string> modelfields = new List<string>();
        modelfields.Add("n");
        modelfields.Add("np");
        modelfields.Add("sr");
        modelfields.Add("w");
        dynamic mexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelfields)
        {
            int InverterQty1 = 0;
            int InverterQty2 = 0;
            int InverterQty3 = 0;
            if (stPro.inverterqty != null && stPro.inverterqty != "")
            {
                InverterQty1 = Convert.ToInt32(stPro.inverterqty);
            }
            if (stPro.inverterqty2 != null && stPro.inverterqty2 != "")
            {
                InverterQty2 = Convert.ToInt32(stPro.inverterqty2);
            }
            if (stPro.inverterqty3 != null && stPro.inverterqty3 != "")
            {
                InverterQty3 = Convert.ToInt32(stPro.inverterqty3);
            }
            int Qty = InverterQty1 + InverterQty2 + InverterQty3;
            string qty1 = Convert.ToString(Qty);
            if (field == "n")
            {
                ((IDictionary<String, Object>)mexo).Add(field, sti.StockModel);
            }
            if (field == "np")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(qty1) ? 0 : Convert.ToInt32(qty1));
            }
            if (field == "sr")
            {
                ((IDictionary<String, Object>)mexo).Add(field, sti.StockSeries);
            }
            if (field == "w")
            {
                ((IDictionary<String, Object>)mexo).Add(field, string.IsNullOrEmpty(sti.StockSize) ? 0 : Convert.ToInt32(sti.StockSize));
            }
        }
        List<string> modelField = new List<string>();
        // my 'columns'
        string Model = sti.StockModel;
        string Model1 = Regex.Replace(Model, "[^a-zA-Z0-9-]", "_");
        modelField.Add(Model1);
        dynamic mmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in modelField)
        {
            ((IDictionary<String, Object>)mmexo).Add(field, mexo);
        }

        List<string> invfields = new List<string>();
        // my 'columns'
        invfields.Add("ms");
        invfields.Add("n");
        dynamic pnlmexo = new System.Dynamic.ExpandoObject();
        foreach (string field in invfields)
        {
            if (field == "ms")
            {

                ((IDictionary<String, Object>)pnlmexo).Add(field, mmexo);
            }
            if (field == "n")
            {
                ((IDictionary<String, Object>)pnlmexo).Add(field, sti.StockManufacturer);
            }
        }

        List<string> fields = new List<string>();
        // my 'columns'

        fields.Add(Regex.Replace(sti.StockManufacturer, "[^a-zA-Z0-9-]", "_")); //var nstr = str.replace(/[^A-Z0-9^-]+/ig, "_"); return nstr;//
                                                                                //fields.Add(Regex.Replace(stp.StockManufacturer, @"\s^[a-zA-Z0-9_]*$", "_"));

        dynamic exo = new System.Dynamic.ExpandoObject();
        foreach (string field in fields)
        {
            ((IDictionary<String, Object>)exo).Add(field, pnlmexo);
        }
        fInverters = Newtonsoft.Json.JsonConvert.SerializeObject(exo);
        return fInverters;
    }

    #region PickList Code
    public void GenratePickList()
    {
        string ProjectID = Request.QueryString["proid"];

        ddlinstall2.SelectedValue = "";
        ddldesign2.SelectedValue = "";
        ddlec2.SelectedValue = "";

        try
        {
            int PickListExist = ClsQuickStock.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
            if (PickListExist == 0)
            {
                lblModelHeader.Text = "";
                //bindrepeater();
                //bindDropdownForEditPopup();
                BindAddedAttribute();
                DataTable dt = new DataTable();
                dt = ClstblProjects.tblProjects_ItemDetailsWise(ProjectID);
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();
                dt1.Columns.AddRange(new DataColumn[4] { new DataColumn("StockItemID", typeof(int)),
                            new DataColumn("Qty",typeof(string)),
                             new DataColumn("StockCategoryID",typeof(int)),
                              new DataColumn("Type",typeof(int))});


                if (!string.IsNullOrEmpty(dt.Rows[0]["PanelBrandID"].ToString()) || (dt.Rows[0]["PanelBrandID"].ToString()) != "0")
                {
                    if (Convert.ToInt32(dt.Rows[0]["PanelBrandID"]) != 0)
                    {
                        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["PanelBrandID"]), dt.Rows[0]["Panelqty"], dt.Rows[0]["PanelCategoryID"], dt.Rows[0]["Type"]);
                    }

                }
                if (!string.IsNullOrEmpty(dt.Rows[0]["InverterDetailsId"].ToString()))
                {

                    if (Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]) != 0)
                    {
                        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]), dt.Rows[0]["inverterqty"], dt.Rows[0]["InverterCategoryId"], dt.Rows[0]["Type"]);
                    }

                }
                if (!string.IsNullOrEmpty(dt.Rows[0]["SecondInverterDetailsID"].ToString()))
                {
                    if (Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]) != 0)
                    {
                        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]), dt.Rows[0]["inverterqty2"], dt.Rows[0]["SecondInverterCategoryID"], dt.Rows[0]["Type"]);
                    }
                }

                if (!string.IsNullOrEmpty(dt.Rows[0]["ThirdInverterDetailsID"].ToString()))
                {
                    if (Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]) != 0)
                    {
                        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]), dt.Rows[0]["inverterqty3"], dt.Rows[0]["ThirdInverterCategoryID"], dt.Rows[0]["Type"]);
                    }

                }

                if (dt1.Rows.Count > 0)
                {
                    MaxAttributePick = dt1.Rows.Count;
                }
                else
                {
                    MaxAttributePick = 1;
                }

                // dr["Type"] = hdntype.Value;
                SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                try
                {
                    ddlinstall2.SelectedValue = st.Installer;
                }
                catch { }
                try
                {
                    ddldesign2.SelectedValue = st.Designer;
                }
                catch { }
                try
                {
                    ddlec2.SelectedValue = st.Electrician;
                }
                catch { }
                TextBox5.Text = txtInstallBookingDate.Text;
                rptattribute.DataSource = dt1;
                rptattribute.DataBind();

                ModalPopupExtender4.Show();
            }
            else
            {
                TextBox5.Text = txtInstallBookingDate.Text;
                lblModelHeader.Text = "Again";
                MaxAttributePick = 1;
                bindrepeater();
                ModalPopupExtender4.Show();
            }
        }
        catch (Exception ex)
        {
            SetError1();
        }
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        HiddenField hdnStockCategoryId = (HiddenField)item.FindControl("hndStockCategoryID");
        ddlStockItem.Items.Clear();

        if (ddlStockCategoryID.SelectedValue != string.Empty)
        {
            hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue;
            // ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
           
            DataTable dtStockItem = new DataTable();
            if (Roles.IsUserInRole("Administrator"))
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            }
            else
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category_BySalesTeg(hdnStockCategoryId.Value);
            }

            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "FixStockItemID";
            ddlStockItem.DataBind();
        }

        ModalPopupExtender4.Show();
    }

    protected void ddlStockItem_SelectedIndexChanged2(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        TextBox txtwallateQty = (TextBox)item.FindControl("txtwallateQty");

        string wallQty = "0";
        txtwallateQty.Text = "0";
        if (ddlinstall2.SelectedValue != string.Empty)
        {
            if (ddlStockItem.SelectedValue != string.Empty)
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlinstall2.SelectedValue);
                string StockItemid = ddlStockItem.SelectedValue.ToString();
                if (dtAcno.Rows.Count > 0)
                {
                    if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                    {
                        string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                        DataTable dt = ClstblProjects.spexistsPicklIstdata(StockItemid, Acno);

                        if (dt.Rows.Count > 0)
                        {
                            wallQty = dt.Rows[0]["Qty"].ToString();
                            txtwallateQty.Text = wallQty;
                        }
                    }
                }

            }
            else
            {
                MsgError("Please select Item");
            }
        }
        else
        {
            MsgError("Please select Installer");
        }

        ModalPopupExtender4.Show();
    }

    protected void litremove_Click(object sender, EventArgs e)
    {
        Div6Pnl.Visible = false;
        Div10inv.Visible = false;
        DivPanel1.Visible = false;
        DivInverter1.Visible = false;
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        hdntype.Value = "1";
        int y = 0;
        //int pnlerror = 0;
        //int inverror = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            HiddenField hndPickLIstItemId = (HiddenField)item1.FindControl("hndPickLIstItemId");

            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }

        ModalPopupExtender4.Show();
    }

    protected void rptattribute_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtQuantity");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hndStockCategoryID");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdbQty = (HiddenField)e.Item.FindControl("hndQty");
        HiddenField hndqty1 = (HiddenField)e.Item.FindControl("hndqty1");
        TextBox txtwallateQty = (TextBox)e.Item.FindControl("txtwallateQty");

        if (hdnStockCategoryId.Value != "")
        {
            DataTable dtStockItem = new DataTable();
            if (Roles.IsUserInRole("Administrator"))
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            }
            else
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category_BySalesTeg(hdnStockCategoryId.Value);
            }

            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "FixStockItemID";
            ddlStockItem.DataBind();

            ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
            ddlStockItem.SelectedValue = hdnStockItem.Value;

        }

        if (string.IsNullOrEmpty(hndqty1.Value))
        {
            txtOrderQuantity.Text = "0";
        }
        else
        {
            txtOrderQuantity.Text = hndqty1.Value;
        }

        if (e.Item.ItemIndex == 0)
        {
            /// btnaddnew.Attributes.Remove("disabled");
            //litremove.Attributes.Remove("disabled");
            litremove.Visible = false;
        }
        else
        {
            // chkdelete.Visible = true;
            litremove.Visible = true;
        }

        //Wallate Qty Installaer wise
        string Accreditation = "";
        string wallQty = "0";
        txtwallateQty.Text = "0";
        if (ddlinstall2.SelectedValue != string.Empty)
        {
            SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(ddlinstall2.SelectedValue);
            Accreditation = st.Accreditation;
            if (!string.IsNullOrEmpty(Accreditation))
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlinstall2.SelectedValue);
                if (ddlStockItem.SelectedValue != string.Empty)
                {
                    //string StockItemid = ddlStockCategoryID.SelectedValue.ToString();
                    //string Installer = ddlInstaller1.SelectedValue.ToString();
                    if (dtAcno.Rows.Count > 0)
                    {
                        if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                        {
                            string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                            DataTable dt = ClstblProjects.spexistsPicklIstdata(ddlStockItem.SelectedValue, Acno);

                            if (dt.Rows.Count > 0)
                            {
                                wallQty = dt.Rows[0]["Qty"].ToString();
                                txtwallateQty.Text = wallQty;
                            }
                        }
                    }
                }
                else
                {
                    // MsgError("Please select Item");
                }
            }
        }
        else
        {
            // MsgError("Please select Installer");
        }

    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        ModalPopupExtender4.Show();
    }

    protected void AddmoreAttribute()
    {
        MaxAttributePick = MaxAttributePick + 1;
        BindAddedAttribute();
        bindrepeater();
    }

    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("Qty", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));
        rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
            //TextBox txtSysDet = (TextBox)item.FindControl("txtSysDetails");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

            HiddenField hdnlistitemId = (HiddenField)item.FindControl("hndPckListItemId");
            DataRow dr = rpttable.NewRow();
            dr["type"] = hdntype.Value;

            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["Qty"] = txtQuantity.Text;
            if (hdnlistitemId != null)
            {
                if (hdnlistitemId.Value != null && hdnlistitemId.Value != "")
                {
                    dr["PicklstItemId"] = hdnlistitemId.Value;
                }
            }
            rpttable.Rows.Add(dr);
        }
    }

    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("Qty", Type.GetType("System.String"));
            // rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));
            //rpttable.Columns.Add("SysDetails", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttributePick; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["Qty"] = "";
            //  dr["StockOrderItemID"] = "";
            dr["type"] = "";
            dr["PicklstItemId"] = "";
            // dr["SysDetails"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void ddlinstall2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlinstall2.SelectedValue != "")
        {
            try
            {
                ddldesign2.SelectedValue = ddlinstall2.SelectedValue;
                ddlec2.SelectedValue = ddlinstall2.SelectedValue;
            }
            catch (Exception ex) { };

        }
        else
        {
            ddldesign2.SelectedValue = "";
            ddlec2.SelectedValue = "";
        }

        ModalPopupExtender4.Show();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        divminStockModel4.Visible = false;
        DivPanel1.Visible = false;
        DivInverter1.Visible = false;
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Compid = Request.QueryString["compid"];

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        int FirstInv = 0;
        int Secondinv = 0;
        int thirdinv = 0;
        int TotalInv = 0;
        int pnlerror = 0;
        int inverror = 0;
        if (Roles.IsUserInRole("Adistrator"))
        {
            int PickListExist = ClsQuickStock.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
            int success = 0;
            if (PickListExist == 0)
            {
                // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

                success = ClsQuickStock.tblPickListLog_Insert(ProjectID, txtResonePLA.Text, txtNotesAgain.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                if (success > 0)
                {
                    bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                    //Update IsTopone by PickList Id
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                    ClsQuickStock.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                    try
                    {
                        bool s1 = ClsQuickStock.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                        bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                    }
                    catch (Exception ex) { }
                    //Telerik_reports.generate_PickList(ProjectID);
                    // picklistdiv.Visible = false;

                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                }
                bool P = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                bool I = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), "0");

                foreach (RepeaterItem item in rptattribute.Items)
                {
                    HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                    if (hdntype.Value != "1")
                    {
                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                        int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                        if (ddlStockCategoryID.SelectedValue == "2")
                        {
                            if (st.inverterqty != null && st.inverterqty != "")
                            {
                                FirstInv = Convert.ToInt32(st.inverterqty);
                            }
                            if (st.inverterqty2 != null && st.inverterqty2 != "")
                            {
                                Secondinv = Convert.ToInt32(st.inverterqty2);
                            }
                            if (st.inverterqty3 != null && st.inverterqty3 != "")
                            {
                                thirdinv = Convert.ToInt32(st.inverterqty3);
                            }
                            TotalInv = FirstInv + Secondinv + thirdinv;
                            bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                        }
                        else if (ddlStockCategoryID.SelectedValue == "1")
                        {
                            if (st.NumberPanels != null && st.NumberPanels != "")
                            {
                                bool s244 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                            }
                        }
                        //string stockitem = ddlStockItem.SelectedItem.Text;  

                    }
                }
                SetAdd1();
                BindPickList();
                ModalPopupExtender4.Hide();
                //BindProjectPreInst(ProjectID);
            }
            else //Again Picklist...
            {
                success = ClsQuickStock.tblPickListLog_Insert(ProjectID, txtResonePLA.Text, txtNotesAgain.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);

                if (success > 0)
                {
                    bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                    //Update IsTopone by PickList Id
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                    ClsQuickStock.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                    try
                    {
                        bool s1 = ClsQuickStock.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                        bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                    }
                    catch (Exception ex) { }
                    //Telerik_reports.generate_PickList(ProjectID);
                    // picklistdiv.Visible = false;

                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                }
                bool P = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                bool I = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), "0");

                foreach (RepeaterItem item in rptattribute.Items)
                {
                    HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                    if (hdntype.Value != "1")
                    {

                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                        int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                        if (ddlStockCategoryID.SelectedValue == "2")
                        {
                            if (st.inverterqty != null && st.inverterqty != "")
                            {
                                FirstInv = Convert.ToInt32(st.inverterqty);
                            }
                            if (st.inverterqty2 != null && st.inverterqty2 != "")
                            {
                                Secondinv = Convert.ToInt32(st.inverterqty2);
                            }
                            if (st.inverterqty3 != null && st.inverterqty3 != "")
                            {
                                thirdinv = Convert.ToInt32(st.inverterqty3);
                            }
                            TotalInv = FirstInv + Secondinv + thirdinv;
                            bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                        }
                        else if (ddlStockCategoryID.SelectedValue == "1")
                        {
                            if (st.NumberPanels != null && st.NumberPanels != "")
                            {
                                bool s244 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                            }
                            else
                            {
                                bool s244 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                            }

                        }
                        //string stockitem = ddlStockItem.SelectedItem.Text;  

                    }
                }
                SetAdd1();
                BindPickList();
                ModalPopupExtender4.Hide();
            }
        }
        else
        {
            foreach (RepeaterItem item in rptattribute.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    //int MinStock = 0;

                    // int pnllivestock = 0;
                    int LiveStock = 0;
                    int pnlwholesale = 0;
                    int pnlapl = 0;
                    int pnlsmpl = 0;
                    int pnlminstock = 0;
                    int pnlavailqty = 0;

                    int InvLiveStock = 0;
                    int Invwholesale = 0;
                    int Invapl = 0;
                    int Invsmpl = 0;
                    int Invminstock = 0;
                    int Invavailqty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                    // DataTable dtLive = ClstblProjects.tblStockItemsLocation_SelectLiveQty_Qty_Pickist(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue);
                    int pnlttl = 0;
                    int invttl = 0;

                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                        {
                            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                            {
                                DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                                if (dtpaneldata.Rows.Count > 0)
                                {
                                    if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                                    {
                                        LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                                    }
                                    if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                                    {
                                        pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                                    }
                                }
                                //ArisePanelPicklistCount
                                DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                                if (DtApl.Rows.Count > 0)
                                {
                                    if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //SolarminerPanelPicklistCount
                                DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                                if (DtSmpl.Rows.Count > 0)
                                {
                                    if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //PanelWholeSale
                                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                                if (dtorderQty.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                    {
                                        pnlwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                            pnlavailqty = LiveStock - pnlttl;
                            if ((pnlavailqty <= pnlminstock))
                            {
                                pnlerror++;
                            }
                        }
                        //if (lblPt.Text != null && lblPt.Text != "")
                        //{
                        //    pnlttl = Convert.ToInt32(lblPt.Text);
                        //}
                        //if (pnlttl > 0)
                        //{
                        //    if (dtLive.Rows[0]["StockQuantity"].ToString() != null && dtLive.Rows[0]["StockQuantity"].ToString() != "")
                        //    {
                        //        LiveStock = Convert.ToInt32(dtLive.Rows[0]["StockQuantity"].ToString());
                        //    }
                        //}
                        //if ((pnlttl <= MinStock))
                        //{
                        //    pnlerror++;
                        //}
                    }
                    else if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        if (ddlStockItem.SelectedValue != null && ddlStockItem.SelectedValue != "")
                        {
                            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
                            {
                                DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                                if (DtInvData.Rows.Count > 0)
                                {
                                    if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                                    {
                                        InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                                    }
                                    if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                                    {
                                        Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                                    }
                                }
                                //AriseINVERTERPicklistCount
                                DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                                if (DtApl.Rows.Count > 0)
                                {
                                    if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //SolarminerINVERTERPicklistCount
                                DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                                if (DtSmpl.Rows.Count > 0)
                                {
                                    if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                    {
                                        Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                        //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                    }
                                }
                                //InverterWholeSale
                                DataTable dtorderQty = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                                if (dtorderQty.Rows.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                    {
                                        Invwholesale = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            invttl = Invapl + Invsmpl + Invwholesale;
                            Invavailqty = InvLiveStock - invttl;
                            if ((Invavailqty <= Invminstock))
                            {
                                inverror++;
                            }
                        }

                    }
                }
            }
            // int success = 0;
            if (pnlerror == 0 && inverror == 0)
            {
                int PickListExist = ClsQuickStock.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");

                if (PickListExist == 0)
                {
                    // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

                    int success = ClsQuickStock.tblPickListLog_Insert(ProjectID, txtResonePLA.Text, txtNotesAgain.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                    bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
                    if (success > 0)
                    {
                        ClsQuickStock.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                        //Update IsTopone by PickList Id
                        ClsQuickStock.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                        //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                        ClsQuickStock.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                        try
                        {
                            bool s1 = ClsQuickStock.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                            bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                        }
                        catch (Exception ex) { }
                        //Telerik_reports.generate_PickList(ProjectID);
                        // picklistdiv.Visible = false;
                        bool P = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                        bool I = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                        foreach (RepeaterItem item in rptattribute.Items)
                        {
                            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                            if (hdntype.Value != "1")
                            {

                                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                                // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                                int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                                //string stockitem = ddlStockItem.SelectedItem.Text;                          
                                if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                                {
                                    if (ddlStockCategoryID.SelectedValue == "1")
                                    {
                                        if (st.NumberPanels != null && st.NumberPanels != "")
                                        {
                                            bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                        }
                                        else
                                        {
                                            bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                        }
                                    }
                                    if (ddlStockCategoryID.SelectedValue == "2")
                                    {
                                        if (st.inverterqty != null && st.inverterqty != "")
                                        {
                                            FirstInv = Convert.ToInt32(st.inverterqty);
                                        }
                                        if (st.inverterqty2 != null && st.inverterqty2 != "")
                                        {
                                            Secondinv = Convert.ToInt32(st.inverterqty2);
                                        }
                                        if (st.inverterqty3 != null && st.inverterqty3 != "")
                                        {
                                            thirdinv = Convert.ToInt32(st.inverterqty3);
                                        }
                                        TotalInv = FirstInv + Secondinv + thirdinv;
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                    }
                                }
                            }
                        }
                        ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        //BindProjectPreInst(ProjectID);
                        SetAdd1();
                        //ModalPopupExtender4.Hide();
                        BindPickList();
                    }
                }
                else //Add PickList Again
                {
                    int success = ClsQuickStock.tblPickListLog_Insert(ProjectID, txtResonePLA.Text, txtNotesAgain.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                    bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
                    if (success > 0)
                    {
                        ClsQuickStock.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                        //Update IsTopone by PickList Id
                        ClsQuickStock.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                        //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                        ClsQuickStock.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                        try
                        {
                            bool s1 = ClsQuickStock.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                            bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                        }
                        catch (Exception ex) { }
                        //Telerik_reports.generate_PickList(ProjectID);
                        // picklistdiv.Visible = false;
                        bool P = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                        bool I = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                        foreach (RepeaterItem item in rptattribute.Items)
                        {
                            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                            if (hdntype.Value != "1")
                            {

                                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                                // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                                int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                                //string stockitem = ddlStockItem.SelectedItem.Text;                          
                                if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                                {
                                    if (ddlStockCategoryID.SelectedValue == "1")
                                    {
                                        if (st.NumberPanels != null && st.NumberPanels != "")
                                        {
                                            bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                        }
                                        else
                                        {
                                            bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                        }
                                    }
                                    if (ddlStockCategoryID.SelectedValue == "2")
                                    {
                                        if (st.inverterqty != null && st.inverterqty != "")
                                        {
                                            FirstInv = Convert.ToInt32(st.inverterqty);
                                        }
                                        if (st.inverterqty2 != null && st.inverterqty2 != "")
                                        {
                                            Secondinv = Convert.ToInt32(st.inverterqty2);
                                        }
                                        if (st.inverterqty3 != null && st.inverterqty3 != "")
                                        {
                                            thirdinv = Convert.ToInt32(st.inverterqty3);
                                        }
                                        TotalInv = FirstInv + Secondinv + thirdinv;
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                    }
                                }
                            }
                        }
                        ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        //BindProjectPreInst(ProjectID);
                        SetAdd1();
                        //ModalPopupExtender4.Hide();
                        BindPickList();
                    }
                }

            }

            if (pnlerror > 0)
            {
                Div6Pnl.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Show();
            }
            if (inverror > 0)
            {
                Div10inv.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Show();
            }

        }
    }

    protected void lnkDraft_Click(object sender, EventArgs e)
    {
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Compid = Request.QueryString["compid"];
        string ProjectID = "";

        int success = 0;
        int FirstInv = 0;
        int Secondinv = 0;
        int thirdinv = 0;
        int TotalInv = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        {

            SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
            nm = stloc.CompanyLocation;
        }
        else
        {
            if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
            {
                SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
                nm = stloc.CompanyLocation;
            }
        }
        int PickListExist = ClsQuickStock.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");
        if (PickListExist == 0)
        {
            // SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            int pnl = 0;
            int inv = 0;
            int LiveStock = 0;
            int pnlwholesale = 0;
            int pnlapl = 0;
            int pnlsmpl = 0;
            int pnlminstock = 0;
            int pnlavailqty = 0;

            int InvLiveStock = 0;
            int Invwholesale = 0;
            int Invapl = 0;
            int Invsmpl = 0;
            int Invminstock = 0;
            int Invavailqty = 0;

            int pnlttl = 0;
            int invttl = 0;

            int pnlerror = 0;
            int inverror = 0;

            foreach (RepeaterItem item in rptattribute.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    int pnlTotalOrderQtyETA = 0;
                    int pnlDraftPckQty = 0;
                    int pnlRemQty = 0;
                    int pnlOrderQty = 0;


                    int invTotalOrderQtyETA = 0;
                    int invDraftPckQty = 0;
                    int invRemQty = 0;
                    int invOrderQty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");

                    //
                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            #region ETA
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    if (dtorderQty.Rows[0]["OrderQuantity"].ToString() != null && dtorderQty.Rows[0]["OrderQuantity"].ToString() != "")
                                    {
                                        pnlTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());

                                    }
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (dtDraftQty.Rows[0]["orderQty"].ToString() != null && dtDraftQty.Rows[0]["orderQty"].ToString() != "")
                                {
                                    pnlDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                pnlOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }

                            pnlRemQty = pnlTotalOrderQtyETA - pnlDraftPckQty;
                            if (pnlOrderQty > pnlRemQty)
                            {
                                pnl++;
                            }
                            #endregion ETA
                            #region TotalCount
                            DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (dtpaneldata.Rows.Count > 0)
                            {
                                if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                                {
                                    pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }

                            //ArisePanelPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerPanelPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //PanelWholeSale
                            DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    pnlwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                            pnlavailqty = LiveStock - pnlttl;
                            if ((pnlavailqty <= pnlminstock))
                            {
                                pnlerror++;
                            }

                            #endregion
                        }
                    }
                    if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            #region ETA
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    invTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtDraftQty.Rows[0]["orderQty"].ToString()))
                                {
                                    invDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                invOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (invOrderQty > invRemQty)
                            {
                                inv++;
                            }
                            #endregion ETA
                            #region Total
                            DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (DtInvData.Rows.Count > 0)
                            {
                                if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                                {
                                    Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }
                            //AriseINVERTERPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerINVERTERPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //InverterWholeSale
                            DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    Invwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            invttl = Invapl + Invsmpl + Invwholesale;
                            Invavailqty = InvLiveStock - invttl;
                            if ((Invavailqty <= Invminstock))
                            {
                                inverror++;
                            }

                            #endregion Total
                        }
                    }

                }
            }

            if ((pnl == 0 || pnlerror == 0) && (inv == 0 || inverror == 0))
            {
                success = ClsQuickStock.tblPickListLog_Insert(ProjectID, txtResonePLA.Text, txtNotesAgain.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(success.ToString(), "1");//1)Draft (2) PickLIst
                if (success > 0)
                {
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                    //Update IsTopone by PickList Id
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                    ClsQuickStock.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                    try
                    {
                        bool s1 = ClsQuickStock.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                        bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                    }
                    catch (Exception ex) { }
                    //Telerik_reports.generate_PickList(ProjectID);
                    // picklistdiv.Visible = false;
                    bool P = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                    bool I = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                    foreach (RepeaterItem item in rptattribute.Items)
                    {
                        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                        if (hdntype.Value != "1")
                        {

                            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                            // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                            int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                            //string stockitem = ddlStockItem.SelectedItem.Text;                          
                            if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                            {
                                if (ddlStockCategoryID.SelectedValue == "1")
                                {
                                    if (st.NumberPanels != null && st.NumberPanels != "")
                                    {
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                    }
                                    else
                                    {
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                    }
                                }
                                if (ddlStockCategoryID.SelectedValue == "2")
                                {
                                    if (st.inverterqty != null && st.inverterqty != "")
                                    {
                                        FirstInv = Convert.ToInt32(st.inverterqty);
                                    }
                                    if (st.inverterqty2 != null && st.inverterqty2 != "")
                                    {
                                        Secondinv = Convert.ToInt32(st.inverterqty2);
                                    }
                                    if (st.inverterqty3 != null && st.inverterqty3 != "")
                                    {
                                        thirdinv = Convert.ToInt32(st.inverterqty3);
                                    }
                                    TotalInv = FirstInv + Secondinv + thirdinv;
                                    bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                }
                            }
                        }
                    }
                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                    SetAdd1();
                    BindPickList();
                    ModalPopupExtender6.Hide();
                    ModalPopupExtender4.Hide();
                }
                //}

            }
            if (pnl > 0)
            {
                Div6Pnl.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Hide();
            }
            if (inv > 0)
            {
                Div10inv.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Hide();
            }

        }
        else
        {
            int pnl = 0;
            int inv = 0;
            int LiveStock = 0;
            int pnlwholesale = 0;
            int pnlapl = 0;
            int pnlsmpl = 0;
            int pnlminstock = 0;
            int pnlavailqty = 0;

            int InvLiveStock = 0;
            int Invwholesale = 0;
            int Invapl = 0;
            int Invsmpl = 0;
            int Invminstock = 0;
            int Invavailqty = 0;

            int pnlttl = 0;
            int invttl = 0;

            int pnlerror = 0;
            int inverror = 0;

            foreach (RepeaterItem item in rptattribute.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    int pnlTotalOrderQtyETA = 0;
                    int pnlDraftPckQty = 0;
                    int pnlRemQty = 0;
                    int pnlOrderQty = 0;


                    int invTotalOrderQtyETA = 0;
                    int invDraftPckQty = 0;
                    int invRemQty = 0;
                    int invOrderQty = 0;
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");

                    //
                    if (ddlStockCategoryID.SelectedValue == "1")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            #region ETA
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    if (dtorderQty.Rows[0]["OrderQuantity"].ToString() != null && dtorderQty.Rows[0]["OrderQuantity"].ToString() != "")
                                    {
                                        pnlTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());

                                    }
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (dtDraftQty.Rows[0]["orderQty"].ToString() != null && dtDraftQty.Rows[0]["orderQty"].ToString() != "")
                                {
                                    pnlDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                pnlOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }

                            pnlRemQty = pnlTotalOrderQtyETA - pnlDraftPckQty;
                            if (pnlOrderQty > pnlRemQty)
                            {
                                pnl++;
                            }
                            #endregion ETA
                            #region TotalCount
                            DataTable dtpaneldata = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (dtpaneldata.Rows.Count > 0)
                            {
                                if (dtpaneldata.Rows[0]["StockQuantity"].ToString() != null && dtpaneldata.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    LiveStock = Convert.ToInt32(dtpaneldata.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (dtpaneldata.Rows[0]["MinQty"].ToString() != null && dtpaneldata.Rows[0]["MinQty"].ToString() != "")
                                {
                                    pnlminstock = Convert.ToInt32(dtpaneldata.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }

                            //ArisePanelPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerPanelPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    pnlsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //PanelWholeSale
                            DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    pnlwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            pnlttl = pnlapl + pnlsmpl + pnlwholesale;
                            pnlavailqty = LiveStock - pnlttl;
                            if ((pnlavailqty <= pnlminstock))
                            {
                                pnlerror++;
                            }

                            #endregion
                        }
                    }
                    if (ddlStockCategoryID.SelectedValue == "2")
                    {
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        if (!string.IsNullOrEmpty(ddlStockItem.SelectedValue) && !string.IsNullOrEmpty(nm))
                        {
                            #region ETA
                            DataTable dtorderQty = ClstblProjects.tblProjects_SelectOrderQty_ETA(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    invTotalOrderQtyETA = Convert.ToInt32(dtorderQty.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            DataTable dtDraftQty = ClstblProjects.GetDraftPicklistCount(ddlStockItem.SelectedValue, nm);
                            if (dtDraftQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtDraftQty.Rows[0]["orderQty"].ToString()))
                                {
                                    invDraftPckQty = Convert.ToInt32(dtDraftQty.Rows[0]["orderQty"].ToString());
                                }
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (txtOrderQuantity.Text != null && txtOrderQuantity.Text != "")
                            {
                                invOrderQty = Convert.ToInt32(txtOrderQuantity.Text);
                            }
                            invRemQty = invTotalOrderQtyETA - invDraftPckQty;
                            if (invOrderQty > invRemQty)
                            {
                                inv++;
                            }
                            #endregion ETA
                            #region Total
                            DataTable DtInvData = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStockAllocationStore.SelectedValue, ddlStockItem.SelectedValue);
                            if (DtInvData.Rows.Count > 0)
                            {
                                if (DtInvData.Rows[0]["StockQuantity"].ToString() != null && DtInvData.Rows[0]["StockQuantity"].ToString() != "")
                                {
                                    InvLiveStock = Convert.ToInt32(DtInvData.Rows[0]["StockQuantity"].ToString());///LiveQty

                                }
                                if (DtInvData.Rows[0]["MinQty"].ToString() != null && DtInvData.Rows[0]["MinQty"].ToString() != "")
                                {
                                    Invminstock = Convert.ToInt32(DtInvData.Rows[0]["MinQty"].ToString());//MinQty

                                }
                            }
                            //AriseINVERTERPicklistCount
                            DataTable DtApl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "1", nm);
                            if (DtApl.Rows.Count > 0)
                            {
                                if (DtApl.Rows[0]["Qty"].ToString() != null && DtApl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invapl = Convert.ToInt32(DtApl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //SolarminerINVERTERPicklistCount
                            DataTable DtSmpl = ClstblProjects.GetItemCountompanyWise(ddlStockItem.SelectedValue, "2", nm);
                            if (DtSmpl.Rows.Count > 0)
                            {
                                if (DtSmpl.Rows[0]["Qty"].ToString() != null && DtSmpl.Rows[0]["Qty"].ToString() != "")
                                {
                                    Invsmpl = Convert.ToInt32(DtSmpl.Rows[0]["Qty"].ToString());
                                    //lblApl.Text = dtArise.Rows[0]["Qty"].ToString();
                                }
                            }
                            //InverterWholeSale
                            DataTable dtorderQty1 = ClstblProjects.tblProjects_SelectWholesaleQty(ddlStockItem.SelectedValue, nm);
                            if (dtorderQty.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(dtorderQty1.Rows[0]["OrderQuantity"].ToString()))
                                {
                                    Invwholesale = Convert.ToInt32(dtorderQty1.Rows[0]["OrderQuantity"].ToString());
                                }
                            }
                            invttl = Invapl + Invsmpl + Invwholesale;
                            Invavailqty = InvLiveStock - invttl;
                            if ((Invavailqty <= Invminstock))
                            {
                                inverror++;
                            }

                            #endregion Total
                        }
                    }

                }
            }

            if ((pnl == 0 || pnlerror == 0) && (inv == 0 || inverror == 0))
            {
                success = ClsQuickStock.tblPickListLog_Insert(ProjectID, txtResonePLA.Text, txtNotesAgain.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
                bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(success.ToString(), "1");//1)Draft (2) PickLIst
                if (success > 0)
                {
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                    //Update IsTopone by PickList Id
                    ClsQuickStock.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                    //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                    ClsQuickStock.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                    try
                    {
                        bool s1 = ClsQuickStock.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);
                        bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                    }
                    catch (Exception ex) { }
                    //Telerik_reports.generate_PickList(ProjectID);
                    // picklistdiv.Visible = false;
                    bool P = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                    bool I = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), "0");
                    foreach (RepeaterItem item in rptattribute.Items)
                    {
                        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                        if (hdntype.Value != "1")
                        {

                            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                            // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                            int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                            //string stockitem = ddlStockItem.SelectedItem.Text;                          
                            if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                            {
                                if (ddlStockCategoryID.SelectedValue == "1")
                                {
                                    if (st.NumberPanels != null && st.NumberPanels != "")
                                    {
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                    }
                                    else
                                    {
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                    }
                                }
                                if (ddlStockCategoryID.SelectedValue == "2")
                                {
                                    if (st.inverterqty != null && st.inverterqty != "")
                                    {
                                        FirstInv = Convert.ToInt32(st.inverterqty);
                                    }
                                    if (st.inverterqty2 != null && st.inverterqty2 != "")
                                    {
                                        Secondinv = Convert.ToInt32(st.inverterqty2);
                                    }
                                    if (st.inverterqty3 != null && st.inverterqty3 != "")
                                    {
                                        thirdinv = Convert.ToInt32(st.inverterqty3);
                                    }
                                    TotalInv = FirstInv + Secondinv + thirdinv;
                                    bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                }
                            }
                        }
                    }
                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                    SetAdd1();
                    BindPickList();
                    ModalPopupExtender6.Hide();
                    ModalPopupExtender4.Hide();
                }
                //}

            }
            if (pnl > 0)
            {
                Div6Pnl.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Hide();
            }
            if (inv > 0)
            {
                Div10inv.Visible = true;
                ModalPopupExtender4.Show();
                ModalPopupExtender6.Hide();
            }
        }
    }
    #endregion

    #region Manage New PickList Code
    protected void BindPickListRepeater(SttblProjects stPro)
    {
        string ProjectID = Request.QueryString["proid"];
        int PickListExist = ClsQuickStock.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");

        txtPicklistAgainNotes.Text = string.Empty;
        txtPickListAgainReson.Text = string.Empty;

        if (PickListExist != 0)
        {
            MaxAttributePickList = 1;
            BindRptPickList();
        }
        else
        {
            DataTable dtStock = new DataTable();
            dtStock.Columns.AddRange(new DataColumn[5] { new DataColumn("StockItemID", typeof(String)),
                            new DataColumn("Qty",typeof(int)),
                             new DataColumn("StockCategoryID",typeof(int)),
                              new DataColumn("Type",typeof(int)), new DataColumn("PicklistitemId", typeof(int))});


            if (!string.IsNullOrEmpty(stPro.PanelBrandID))
            {
                if (Convert.ToInt32(stPro.PanelBrandID) != 0)
                {
                    string StockItemIDPanel = ClsQuickStock.tblStockItems_GetFixItemIDByStockItemID("1", stPro.PanelBrandID);
                    dtStock.Rows.Add(StockItemIDPanel, stPro.NumberPanels, 1, 0);
                }

            }
            if (!string.IsNullOrEmpty(stPro.InverterDetailsID))
            {
                if (Convert.ToInt32(stPro.InverterDetailsID) != 0)
                {
                    string StockItemIDInverter = ClsQuickStock.tblStockItems_GetFixItemIDByStockItemID("1", stPro.InverterDetailsID);
                    dtStock.Rows.Add(StockItemIDInverter, stPro.inverterqty, 2, 0);
                }
            }
            if (!string.IsNullOrEmpty(stPro.SecondInverterDetailsID))
            {
                if (Convert.ToInt32(stPro.SecondInverterDetailsID) != 0)
                {
                    string StockItemIDInverter = ClsQuickStock.tblStockItems_GetFixItemIDByStockItemID("1", stPro.SecondInverterDetailsID);
                    dtStock.Rows.Add(StockItemIDInverter, stPro.inverterqty2, 2, 0);
                }
            }
            if (!string.IsNullOrEmpty(stPro.ThirdInverterDetailsID))
            {
                if (Convert.ToInt32(stPro.ThirdInverterDetailsID) != 0)
                {
                    string StockItemIDInverter = ClsQuickStock.tblStockItems_GetFixItemIDByStockItemID("1", stPro.ThirdInverterDetailsID);
                    dtStock.Rows.Add(StockItemIDInverter, stPro.inverterqty3, 2, 0);
                }
            }

            // Bind Other Details For First Picklist
            int MQty = 0;
            //if (dtStock.Rows.Count > 0)
            //{
            //    MQty = !string.IsNullOrEmpty(dtStock.Compute("SUM(Qty)", "StockCategoryID=1").ToString()) ? Convert.ToInt32(dtStock.Compute("SUM(Qty)", "StockCategoryID=1")) / 2 : 0;
            //}
            dtStock.Rows.Add(14173, MQty, 3, 0);

            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.InverterDetailsID);
            int EQty = 0;
            string ItemID = "";
            if (st.StockSize != "")
            {
                if (Convert.ToDecimal(st.StockSize) < 5)
                {
                    ItemID = "13688";
                    //EQty = 1;
                }
                else if (Convert.ToDecimal(st.StockSize) >= 5 || stPro.InstallState != "VIC")
                {
                    ItemID = "11188";
                    //EQty = 1;
                }
                else if (Convert.ToDecimal(st.StockSize) >= 5 || stPro.InstallState == "VIC")
                {
                    ItemID = "15742";
                    //EQty = 1;
                }
            }
            dtStock.Rows.Add(ItemID, EQty, 4, 0);

            int OQty = 0;
            //if (dtStock.Rows.Count > 0)
            //{
            //    //OQty = Convert.ToInt32(dtStock.Compute("SUM(Qty)", "StockCategoryID=1")) / 6;
            //    OQty = !string.IsNullOrEmpty(dtStock.Compute("SUM(Qty)", "StockCategoryID=1").ToString()) ? Convert.ToInt32(dtStock.Compute("SUM(Qty)", "StockCategoryID=1")) / 6 : 0;
            //}
            if (stPro.RoofTypeID == "5" || stPro.RoofTypeID == "6") // Tile
            {
                //dtStock.Rows.Add("15815", OQty, 5, 0);
                dtStock.Rows.Add("15924", OQty, 3, 0);
            }
            else if (stPro.RoofTypeID == "7" || stPro.RoofTypeID == "8" || stPro.RoofTypeID == "9" || stPro.RoofTypeID == "10" || stPro.RoofTypeID == "12" || stPro.RoofTypeID == "13") // Tin
            {
               // dtStock.Rows.Add("15814", OQty, 5, 0);
                dtStock.Rows.Add("16026", OQty, 3, 0);
            }
            else //Tilt
            {
                if (stPro.RoofTypeID != "")
                {
                   // dtStock.Rows.Add("15816", OQty, 5, 0);
                    dtStock.Rows.Add("16027", OQty, 3, 0);
                }
            }

            if (dtStock.Rows.Count > 0)
            {
                MaxAttributePickList = dtStock.Rows.Count;
                rptPickList.DataSource = dtStock;
                rptPickList.DataBind();
            }
            else
            {
                MaxAttributePickList = 1;
            }

        }

    }

    public void BindPickList()
    {
        string ProjectID = Request.QueryString["proid"].ToString();

        DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");
        if (dtPickList.Rows.Count > 0)
        {
            PanPickList.Visible = true;
            rptPickListDetails.DataSource = dtPickList;
            rptPickListDetails.DataBind();

            if (Roles.IsUserInRole("Administrator"))
            {
                lnkDownloadPicklist.Visible = true;
            }
            else
            {
                if (dtPickList.Rows[0]["IsDeduct"].ToString() == "False")
                {
                    lnkDownloadPicklist.Visible = true;
                }
                else
                {
                    lnkDownloadPicklist.Visible = false;
                }
            }
        }
        else
        {
            PanPickList.Visible = false;
            lnkDownloadPicklist.Visible = false;
        }

        if (dtPickList.Rows.Count > 4)
        {
            //$('#<%=divpl.ClientID %>').attr("style", "overflow-y: scroll; height: " + table_height + "px !important;");

            divpl.Attributes.Add("style", "overflow-y: scroll; height: " + 300 + "px !important;");
        }
        else
        {
            divpl.Attributes.Add("style", "");
        }
    }

    protected void BindRptPickList()
    {
        if (rptTablePickList == null)
        {
            rptTablePickList = new DataTable();
            rptTablePickList.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("StockItemID", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("Qty", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("type", Type.GetType("System.String"));
            rptTablePickList.Columns.Add("PicklistitemId", Type.GetType("System.String"));
        }

        for (int i = rptTablePickList.Rows.Count; i < MaxAttributePickList; i++)
        {
            DataRow dr = rptTablePickList.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["Qty"] = "0";
            dr["type"] = "";
            dr["PicklistitemId"] = "";
            //dr["PicklstItemId"] = "";
            rptTablePickList.Rows.Add(dr);
        }

        rptPickList.DataSource = rptTablePickList;
        rptPickList.DataBind();

        //============Do not Delete Start========================================
        int y = 0;
        foreach (RepeaterItem item1 in rptPickList.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            LinkButton lbremove1 = (LinkButton)item1.FindControl("litremovePicklistItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptPickList.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptPickList.Items)
                {
                    LinkButton lbremove = (LinkButton)item2.FindControl("litremovePicklistItem");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End==========================================
    }

    protected void AddmoreAttributePickList()
    {
        MaxAttributePickList = MaxAttributePickList + 1;
        BindAddedAttributePicklist();
        BindRptPickList();
    }

    protected void BindAddedAttributePicklist()
    {
        rptTablePickList = new DataTable();
        rptTablePickList.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("StockItemID", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("Qty", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("type", Type.GetType("System.String"));
        rptTablePickList.Columns.Add("PicklistitemId", Type.GetType("System.String"));
        //rpttable.Columns.Add("PicklstItemId", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptPickList.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
            //TextBox txtSysDet = (TextBox)item.FindControl("txtSysDetails");
            //HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            HiddenField PicklistitemId = (HiddenField)item.FindControl("PicklistitemId");

            //HiddenField hdnlistitemId = (HiddenField)item.FindControl("hndPckListItemId");
            DataRow dr = rptTablePickList.NewRow();
            dr["type"] = hdntype.Value;

            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["Qty"] = txtQuantity.Text;
            dr["PicklistitemId"] = PicklistitemId.Value;
            //if (hdnlistitemId != null)
            //{
            //    if (hdnlistitemId.Value != null && hdnlistitemId.Value != "")
            //    {
            //        dr["PicklstItemId"] = hdnlistitemId.Value;
            //    }
            //}
            rptTablePickList.Rows.Add(dr);
        }
    }

    protected void lnkAddItem_Click(object sender, EventArgs e)
    {
        AddmoreAttributePickList();
    }

    protected void litremovePicklistItem_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((LinkButton)sender).ClientID);

        RepeaterItem item = rptPickList.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        hdntype.Value = "1";
        int y = 0;
        //int pnlerror = 0;
        //int inverror = 0;
        foreach (RepeaterItem item1 in rptPickList.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            //HiddenField hndPickLIstItemId = (HiddenField)item1.FindControl("hndPickLIstItemId");

            LinkButton lbremove1 = (LinkButton)item1.FindControl("litremovePicklistItem");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptPickList.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptPickList.Items)
                {
                    LinkButton lbremove = (LinkButton)item2.FindControl("litremovePicklistItem");
                    lbremove.Visible = false;
                }
            }


        }

        BindCountByRpt();
    }

    protected void rptPickList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        LinkButton litremove = (LinkButton)item.FindControl("litremovePicklistItem");

        //litremove.Visible = e.Item.ItemIndex == 0 ? false : true;

        //Bind DropDown
        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hndStockCategoryID");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        if (hdnStockCategoryId.Value != "")
        {
            DataTable dtStockItem = new DataTable();
            if (Roles.IsUserInRole("Administrator"))
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            }
            else
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category_BySalesTeg(hdnStockCategoryId.Value);
            }

            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "FixStockItemID";
            ddlStockItem.DataBind();

            try
            {
                ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
                ddlStockItem.SelectedValue = hdnStockItem.Value;
            }
            catch (Exception ex)
            {

            }
        }

        //Wallate Qty Installaer wise
        TextBox txtWallateQty = (TextBox)e.Item.FindControl("txtWallateQty");
        string Accreditation = "";
        string wallQty = "0";
        txtWallateQty.Text = "0";
        if (ddlInstaller.SelectedValue != string.Empty)
        {
            SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(ddlInstaller.SelectedValue);
            Accreditation = st.Accreditation;
            if (!string.IsNullOrEmpty(Accreditation))
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlInstaller.SelectedValue);
                if (ddlStockItem.SelectedValue != string.Empty)
                {
                    if (dtAcno.Rows.Count > 0)
                    {
                        if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                        {
                            string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                            DataTable dt = ClstblProjects.spexistsPicklIstdata(ddlStockItem.SelectedValue, Acno);

                            if (dt.Rows.Count > 0)
                            {
                                wallQty = dt.Rows[0]["Qty"].ToString();
                                txtWallateQty.Text = wallQty;
                            }
                        }
                    }
                }
                else
                {
                    // MsgError("Please select Item");
                }
            }
        }
    }

    protected void ddlStockCategoryID_SelectedIndexChanged1(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptPickList.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        HiddenField hdnStockCategoryId = (HiddenField)item.FindControl("hndStockCategoryID");

        ddlStockItem.Items.Clear();
        ListItem listItem = new ListItem();
        listItem.Value = "";
        listItem.Text = "Select";
        ddlStockItem.Items.Add(listItem);

        if (ddlStockCategoryID.SelectedValue != string.Empty)
        {
            hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue;

            DataTable dtStockItem = new DataTable();
            if(Roles.IsUserInRole("Administrator"))
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            }
            else
            {
                dtStockItem = ClstblStockItems.tblStockItems_Select_Category_BySalesTeg(hdnStockCategoryId.Value);
            }

            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "FixStockItemID";
            ddlStockItem.DataBind();

        }
    }

    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptPickList.Items[index];

        //Wallate Qty Installaer wise
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        TextBox txtWallateQty = (TextBox)item.FindControl("txtWallateQty");
        TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");

        string StockItemID = ddlStockItem.SelectedValue;
        if (txtInstallBookingDate.Text != "")
        {
            SttblStockItems sttbl = ClstblStockItems.tblStockItems_SelectByFixStockItemID(StockItemID);

            string ExpiryDate = sttbl.ExpiryDate;

            if (ExpiryDate != "")
            {
                if (Convert.ToDateTime(txtInstallBookingDate.Text) > Convert.ToDateTime(ExpiryDate))
                {
                    MsgError("Stock Expiry date Greater then Install Booking Date");
                    ddlStockItem.Focus();
                    ddlStockItem.SelectedValue = "";
                }
            }
        }

        string Accreditation = "";
        string wallQty = "0";
        txtWallateQty.Text = "0";
        if (ddlInstaller.SelectedValue != string.Empty)
        {
            SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(ddlInstaller.SelectedValue);
            Accreditation = st.Accreditation;
            if (!string.IsNullOrEmpty(Accreditation))
            {
                DataTable dtAcno = ClstblContacts.tblContacts_SelectbyInstaller(ddlInstaller.SelectedValue);
                if (ddlStockItem.SelectedValue != string.Empty)
                {
                    if (dtAcno.Rows.Count > 0)
                    {
                        if (dtAcno.Rows[0]["Accreditation"].ToString() != null && dtAcno.Rows[0]["Accreditation"].ToString() != "")
                        {
                            string Acno = dtAcno.Rows[0]["Accreditation"].ToString();
                            DataTable dt = ClstblProjects.spexistsPicklIstdata(ddlStockItem.SelectedValue, Acno);

                            if (dt.Rows.Count > 0)
                            {
                                wallQty = dt.Rows[0]["Qty"].ToString();
                                txtWallateQty.Text = wallQty;
                            }
                        }
                    }
                }
                else
                {
                    MsgError("Please select Item");
                }
            }

            if (ddlStockItem.SelectedValue != string.Empty)
            {
                if (ddlStockCategoryID.SelectedValue == "1")
                {
                    BindPanel(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue, txtQuantity.Text);
                }
                else if (ddlStockCategoryID.SelectedValue == "2")
                {
                    BindInverter(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue, txtQuantity.Text);
                }
            }
        }
        else
        {
            //ddlStockItem.SelectedValue = "";
            MsgError("Please select Installer");
        }

        BindOtherQty();
        CheckMsg();

    }

    protected void btnSavePickList_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        string Compid = Request.QueryString["compid"];
        string ProjectID = Request.QueryString["proid"];

        //string nm = "";
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //if (st.StockAllocationStore != null && st.StockAllocationStore != "")
        //{

        //    SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(st.StockAllocationStore);
        //    nm = stloc.CompanyLocation;
        //}
        //else
        //{
        //    if (ddlStockAllocationStore.SelectedValue != null && ddlStockAllocationStore.SelectedValue != "")
        //    {
        //        SttblCompanyLocations stloc = ClstblCompanyLocations.tblCompanyLocations_SelectByCompanyLocationID(ddlStockAllocationStore.SelectedValue);
        //        nm = stloc.CompanyLocation;
        //    }
        //}

        string PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string InstallBookingDate = txtInstallBookingDate.Text;
        string Installer = ddlInstaller.SelectedValue;
        string InstallerName = ddlInstaller.SelectedItem.Text;
        string Designer = ddlDesigner.SelectedValue;
        string Electrician = ddlElectrician.SelectedValue;
        string PickListCategory = ddlPickListCategory.SelectedValue;
        string stockWith = ddlStockWith.SelectedValue; // 1) With Installer 2) With Customer

        int PickListExist = ClsQuickStock.tblPickListLog_ExistsByProjectID_CompanyId(ProjectID, "1");

        if (ddlInstaller.SelectedValue != "")
        {
            int success = ClsQuickStock.tblPickListLog_Insert(ProjectID, txtPickListAgainReson.Text, txtPicklistAgainNotes.Text, PickListDate, InstallBookingDate, Installer, InstallerName, Designer, Electrician, UpdatedBy);
            bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(success.ToString(), "2");//1)Draft (2) PickLIst
            bool suc2 = ClsQuickStock.tbl_PickListLog_UpdatePickListCategory(success.ToString(), PickListCategory);
            bool sucStockWith = ClsQuickStock.tbl_PickListLog_Update_stockWith(success.ToString(), stockWith);

            if (success > 0)
            {
                ClsQuickStock.tbl_PickListLog_Update_IsToponeByProjectId(ProjectID, "0");
                //Update IsTopone by PickList Id
                ClsQuickStock.tbl_PickListLog_Update_IsToponeByPickListId(success.ToString(), "1");

                //Update Companyid like 1 for "Arise Solar" 2 for "Solar Miner" 3 for "Achivers"
                ClsQuickStock.tbl_PickListLog_upateCompanyid(Convert.ToString(success), "1", st.ProjectNumber);
                try
                {
                    //bool s1 = ClsQuickStock.tbl_PickListLog_updateInstallerAndEcDetails(Convert.ToString(success), ddlinstall2.SelectedValue, ddlinstall2.SelectedItem.Text, ddldesign2.SelectedValue, ddlec2.SelectedValue);

                    bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(Convert.ToString(success), ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
                }
                catch (Exception ex) { }

                //Update Panel Installed & Inverter Installed
                bool P = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                bool I = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), "0");

                foreach (RepeaterItem item in rptPickList.Items)
                {
                    HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                    if (hdntype.Value != "1")
                    {
                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                        //int StockItemID = ClsQuickStock.tblStockItems_GetFixItemIDByStockItemID("1", ddlStockItem.SelectedValue);

                        int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                        //string stockitem = ddlStockItem.SelectedItem.Text;   

                        if (PickListExist == 0)
                        {
                            if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                            {
                                if (ddlStockCategoryID.SelectedValue == "1")
                                {
                                    if (st.NumberPanels != null && st.NumberPanels != "")
                                    {
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), st.NumberPanels);
                                    }
                                    else
                                    {
                                        bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(success.ToString(), "0");
                                    }
                                }
                                if (ddlStockCategoryID.SelectedValue == "2")
                                {
                                    int FirstInv = 0;
                                    int Secondinv = 0;
                                    int thirdinv = 0;

                                    if (st.inverterqty != null && st.inverterqty != "")
                                    {
                                        FirstInv = Convert.ToInt32(st.inverterqty);
                                    }
                                    if (st.inverterqty2 != null && st.inverterqty2 != "")
                                    {
                                        Secondinv = Convert.ToInt32(st.inverterqty2);
                                    }
                                    if (st.inverterqty3 != null && st.inverterqty3 != "")
                                    {
                                        thirdinv = Convert.ToInt32(st.inverterqty3);
                                    }
                                    int TotalInv = FirstInv + Secondinv + thirdinv;
                                    bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(success.ToString(), TotalInv.ToString());
                                }
                            }
                        }

                    }
                }
                SetAdd1();
                BindPickList();
                ClearRpt();
                BindBooking(st);
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            MsgError("Please Select Intaller");
        }
    }

    protected void ClearRpt()
    {
        txtPickListAgainReson.Text = string.Empty;
        txtPicklistAgainNotes.Text = string.Empty;
        ddlPickListCategory.SelectedValue = "";
        ddlStockWith.SelectedValue = "";
        MaxAttributePickList = 1;
        BindRptPickList();
    }

    protected void rptPickListDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.CommandName.ToString() == "Edit")
        {
            string PickListID = e.CommandArgument.ToString();
            hndPickListID.Value = PickListID;

            int tblMaintanHistoryLogExist = ClsQuickStock.tblMaintainHistory_ExistsBySectionID(PickListID);
            if (tblMaintanHistoryLogExist == 0)
            {
                btnSavePickList.Visible = false;
                btnUpdatePicklist.Visible = true;

                DataTable dt = ClsQuickStock.tbl_PicklistItemDetail_GetDataByPickID(PickListID);

                if (dt.Rows.Count > 0)
                {
                    txtPickListAgainReson.Text = dt.Rows[0]["Reasons"].ToString();
                    txtPicklistAgainNotes.Text = dt.Rows[0]["Note"].ToString();
                    if (dt.Rows[0]["PickListCategory"].ToString() != "")
                    {
                        ddlPickListCategory.SelectedValue = dt.Rows[0]["PickListCategory"].ToString();
                    }
                    if (dt.Rows[0]["stockWith"].ToString() != "")
                    {
                        ddlStockWith.SelectedValue = dt.Rows[0]["stockWith"].ToString();
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["StockCategoryID"].ToString() == "1")
                        {
                            BindPanel(dt.Rows[i]["StockItemId"].ToString(), ddlStockAllocationStore.SelectedValue, dt.Rows[i]["Qty"].ToString());
                        }
                        else if (dt.Rows[i]["StockCategoryID"].ToString() == "2")
                        {
                            BindInverter(dt.Rows[i]["StockItemId"].ToString(), ddlStockAllocationStore.SelectedValue, dt.Rows[i]["Qty"].ToString());
                        }
                    }

                    MaxAttributePickList = dt.Rows.Count;
                    rptPickList.DataSource = dt;
                    rptPickList.DataBind();
                }
            }
            else
            {
                MsgInfo("Stock Already Scanned, Contact WareHouse..");
            }

            hndPickListMode.Value = "Edit";
        }

        if (e.CommandName.ToString() == "Delete")
        {
            string PickListID = e.CommandArgument.ToString();
            int tblMaintanHistoryLogExist = ClsQuickStock.tblMaintainHistory_ExistsBySectionID(PickListID);

            if (tblMaintanHistoryLogExist == 0) //Write Delete Code
            {
                lblMassage.Text = "Are You Sure Delete This Picklist?";
                hndDelete.Value = PickListID;
                lnkDeleteInstallation.Visible = false;
                lnkDeletePickList.Visible = true;
                ModalPopupExtenderDeleteNew.Show();

            }
            else
            {
                lblMassage.Text = "Stock Already Scanned, Contact WareHouse..";
                lnkDeletePickList.Visible = false;
                lnkDeleteInstallation.Visible = false;
                ModalPopupExtenderDeleteNew.Show();
            }
        }
    }

    protected void btnUpdatePicklist_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        string Compid = Request.QueryString["compid"];
        string ProjectID = Request.QueryString["proid"];

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string PicklistID = hndPickListID.Value;
        string Reason = txtPickListAgainReson.Text;
        string Notes = txtPicklistAgainNotes.Text;
        string InstallBookingDate = txtInstallBookingDate.Text;
        string Installer = ddlInstaller.SelectedValue;
        string InstallerName = ddlInstaller.SelectedItem.Text;
        string Designer = ddlDesigner.SelectedValue;
        string Electrician = ddlElectrician.SelectedValue;
        string PickListCategory = ddlPickListCategory.SelectedValue;
        string stockWith = ddlStockWith.SelectedValue; // 1) With Installer 2) With Customer

        if (PicklistID != "")
        {
            bool UpdatePicklistData = ClsQuickStock.tblPickListLog_Update(PicklistID, Reason, Notes, InstallBookingDate, Installer, InstallerName, Designer, Electrician, UpdatedBy);

            bool loc = ClsQuickStock.tbl_PickListLog_Update_locationId_CompanyWise(PicklistID, ddlStockAllocationStore.SelectedValue, "1", st.Project, st.RoofType, st.RoofTypeID);
            bool suc1 = ClsQuickStock.tbl_picklistlog_updatePicklistType(PicklistID, "2");//1)Draft (2) PickLIst

            bool suc2 = ClsQuickStock.tbl_PickListLog_UpdatePickListCategory(PicklistID, PickListCategory);
            bool sucStockWith = ClsQuickStock.tbl_PickListLog_Update_stockWith(PicklistID, stockWith);

            ClsQuickStock.tbl_PicklistItemsDetail_Delete(PicklistID);

            foreach (RepeaterItem item in rptPickList.Items)
            {
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                if (hdntype.Value != "1")
                {
                    DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                    DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                    TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");

                    int suc = ClsQuickStock.tbl_PicklistItemDetail_Insert(PicklistID, ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                    //string stockitem = ddlStockItem.SelectedItem.Text;                          
                    if (ddlStockCategoryID.SelectedValue != null && ddlStockCategoryID.SelectedValue != "")
                    {
                        if (ddlStockCategoryID.SelectedValue == "1")
                        {
                            if (st.NumberPanels != null && st.NumberPanels != "")
                            {
                                bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(PicklistID, st.NumberPanels);
                            }
                            else
                            {
                                bool s2 = ClsQuickStock.tbl_picklistlog_UpdateIPanel(PicklistID, "0");
                            }
                        }
                        if (ddlStockCategoryID.SelectedValue == "2")
                        {
                            int FirstInv = 0;
                            int Secondinv = 0;
                            int thirdinv = 0;

                            if (st.inverterqty != null && st.inverterqty != "")
                            {
                                FirstInv = Convert.ToInt32(st.inverterqty);
                            }
                            if (st.inverterqty2 != null && st.inverterqty2 != "")
                            {
                                Secondinv = Convert.ToInt32(st.inverterqty2);
                            }
                            if (st.inverterqty3 != null && st.inverterqty3 != "")
                            {
                                thirdinv = Convert.ToInt32(st.inverterqty3);
                            }
                            int TotalInv = FirstInv + Secondinv + thirdinv;
                            bool s2 = ClsQuickStock.tbl_picklistlog_UpdateInverter(PicklistID, TotalInv.ToString());
                        }
                    }
                }
            }

            SetAdd1();
            BindPickList();
            ClearRpt();
            btnSavePickList.Visible = true;
            btnUpdatePicklist.Visible = false;
            hndPickListMode.Value = "";
        }

    }

    protected void lnkDeletePickList_Click1(object sender, EventArgs e)
    {
        string PicklistID = hndDelete.Value;

        if (PicklistID != "")
        {
            try
            {
                ClsQuickStock.tbl_PickListLog_DeleteByID(PicklistID);
                ClsQuickStock.tbl_PicklistItemsDetail_Delete(PicklistID);
                SetAdd1();
                BindPickList();

                string ProjectID = Request.QueryString["proid"];
                SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                BindPickListRepeater(stPro);
                BindBooking(stPro);
            }
            catch (Exception ex)
            {
                SetError1();
            }
        }
        else
        {
            SetError1();
        }
    }

    protected void rptPickListDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        LinkButton lbtnDelete = (LinkButton)e.Item.FindControl("lbtnDelete");

        if (Roles.IsUserInRole("Administrator"))
        {
            lbtnDelete.Visible = true;
        }
    }

    protected void lnkDownloadPicklist_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }

        //try
        //{
        //    if (ProjectID != "")
        //    {
        //        //Telerik_reports.generate_PickList(ProjectID);

        //    }
        //}
        //catch (Exception ex)
        //{
        //    MsgError(ex.Message);
        //}
        Telerik_reports.Generate_NewPickList(ProjectID);
    }

    protected void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((TextBox)sender).ClientID);
        RepeaterItem item = rptPickList.Items[index];

        //Wallate Qty Installaer wise
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");

        BindOtherQty();
        CheckMsg();

    }

    public void BindOtherQty()
    {
        int MQty = 0;
        int EQty = 0;
        foreach (RepeaterItem rptItem in rptPickList.Items)
        {
            DropDownList ddlStkCatID = (DropDownList)rptItem.FindControl("ddlStockCategoryID");
            TextBox txtQty = (TextBox)rptItem.FindControl("txtQuantity");
            DropDownList ddlStkItemID = (DropDownList)rptItem.FindControl("ddlStockItem");

            if (ddlStkCatID.SelectedValue == "1")
            {
                MQty += Convert.ToInt32(txtQty.Text != "" ? txtQty.Text : "0");
                //MQty = !string.IsNullOrEmpty(dtStock.Compute("SUM(Qty)", "StockCategoryID=1").ToString()) ? Convert.ToInt32(dtStock.Compute("SUM(Qty)", "StockCategoryID=1")) / 2 : 0;
            }
            // if (ddlStkCatID.SelectedValue == "2")
            //{
            //SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlStkItemID.SelectedValue);
            //string ItemID = "0";
            //if (st.StockSize != "")
            //{
            //    if (Convert.ToDecimal(st.StockSize) < 5)
            //    {
            //        ItemID = "13688";
            //        EQty = 1;
            //    }
            //    else if (Convert.ToDecimal(st.StockSize) >= 5 || ddlStockAllocationStore.SelectedValue != "2")
            //    {
            //        ItemID = "11188";
            //        EQty = 1;
            //    }
            //    else if (Convert.ToDecimal(st.StockSize) >= 5 || ddlStockAllocationStore.SelectedValue != "2")
            //    {
            //        ItemID = "15742";
            //        EQty = 1;
            //    }
            //    //dtStock.Rows.Add(ItemID, EQty, 4, 0);
            //}
            //}
        }

        foreach (RepeaterItem rptItem in rptPickList.Items)
        {
            DropDownList ddlStkCatID1 = (DropDownList)rptItem.FindControl("ddlStockCategoryID");
            TextBox txtQty1 = (TextBox)rptItem.FindControl("txtQuantity");

            //if (ddlStkCatID1.SelectedValue == "3")
            //{
            //    int qty = txtQty1.Text != "" ? Convert.ToInt32(txtQty1.Text) : 0;
            //    txtQty1.Text = (MQty != 0 ? (MQty / 2) : qty).ToString();
            //}
            //else if (ddlStkCatID1.SelectedValue == "4")
            //{
            //    txtQty1.Text = (EQty != 0 ? (EQty / 5) : 0).ToString();
            //}
            //else if (ddlStkCatID1.SelectedValue == "5")
            //{
            //    txtQty1.Text = (MQty != 0 ? (MQty / 6) : 0).ToString();
            //}
        }
    }

    #endregion

    protected void lnkDeleteInstallation_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (ProjectID != "")
        {

            bool Success = ClstblSalesinfo.tblProjects_Update_RemoveIntallation(ProjectID, "", "", "", "", "3");

            DataTable dt = ClsQuickStock.tbl_PicklistLog_GetDeleteFlagByProjectID(ProjectID);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ExistFlag"].ToString() == "0")
                    {
                        string PicklistID = dt.Rows[i]["ID"].ToString();
                        ClsQuickStock.tbl_PickListLog_DeleteByID(PicklistID);
                        ClsQuickStock.tbl_PicklistItemsDetail_Delete(PicklistID);
                    }
                }
            }

            if (Success)
            {
                BindCommonDetails();
                BindGrid(0);
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }
        {
            MsgError("ProjectID is Null");
        }
    }

    #region refund
    protected void BindRefund()
    {
        string ProjectID = Request.QueryString["proid"];
        BindRefundDetails(ProjectID);
        BindRefundDropDown();
    }

    protected void BindRefundDropDown()
    {
        ddlOption.DataSource = ClstblRefundOptions.tblRefundOptions_SelectActice();
        ddlOption.DataValueField = "OptionID";
        ddlOption.DataMember = "OptionName";
        ddlOption.DataTextField = "OptionName";
        ddlOption.DataBind();
    }

    protected void btnrefundSave_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string UserID = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string Option = ddlOption.SelectedValue;
        string Amount = txtAmount.Text;
        string BankName = txtbankname.Text;
        string AccName = txtAccName.Text;
        string BSBNo = txtBSBNo.Text;
        string AccNumber = txtAccNo.Text;
        string Notes = txtNotes.Text;

        if (hndRefundMode.Value != "Update") // Insert New 
        {
            if (txtAmount.Text != string.Empty || ddlOption.SelectedValue != "")
            {
                int success = ClstblProjectRefund.tblProjectRefund_Insert(ProjectID, Option, Amount.Trim(), UserID, Notes, AccName, BSBNo, AccNumber, BankName);
                SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                ClstblProjectRefund.tblProjectRefund_UpdateProjectStatusID(success.ToString(), st2.ProjectStatusID);

                if (Convert.ToString(success) != string.Empty)
                {
                    BindRefundDetails(ProjectID);
                    ClearRefund();
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
            }
        }
        else // Update
        {
            string RefundID = hndRefundID.Value;

            if (RefundID != "")
            {
                bool success = ClstblProjectRefund.tblProjectRefund_UpdateBy_RefundId(RefundID, Option, AccName, Amount, BSBNo, AccNumber, Notes, BankName);
                if (success)
                {
                    BindRefundDetails(ProjectID);

                    SetUpdate();
                }
                else
                {
                    SetError1();
                }
            }
            hndRefundMode.Value = "";
            ClearRefund();
        }
    }

    public void BindRefundDetails(string ProjectID)
    {
        //DataTable dt_inv = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
        DataTable dt = ClstblProjectRefund.tblProjectRefund_SelectByPID(ProjectID);

        if (dt.Rows.Count > 0)
        {
            DivGridRefund.Visible = true;
            rptRefund.DataSource = dt;
            rptRefund.DataBind();
            if (dt.Rows.Count > 5)
            {
                DivGridRedundData.Attributes.Add("style", "overflow-x: scroll; overflow-y: scroll; height: 200px !important;");
            }
            else
            {
                DivGridRedundData.Attributes.Add("style", "overflow-y: scroll");
            }

        }
        else
        {
            DivGridRefund.Visible = false;
        }
    }

    public void ClearRefund()
    {
        ddlOption.SelectedValue = "";
        txtAmount.Text = string.Empty;
        txtbankname.Text = string.Empty;
        txtAccName.Text = string.Empty;
        txtBSBNo.Text = string.Empty;
        txtAccNo.Text = string.Empty;
        txtNotes.Text = string.Empty;
    }

    protected void rptRefund_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            string RefundID = e.CommandArgument.ToString();
            hndRefundID.Value = RefundID;
            hndRefundMode.Value = "Update";

            SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
            ddlOption.SelectedValue = st.OptionID;
            txtAmount.Text = st.Amount;
            txtbankname.Text = st.BankName;
            txtAccName.Text = st.AccName;
            txtBSBNo.Text = st.BSBNo;
            txtAccNo.Text = st.AccNo;
            txtNotes.Text = st.Notes;
        }

        if (e.CommandName == "Delete")
        {
            string RefundID = e.CommandArgument.ToString();

            hndDeleteRefundID.Value = RefundID;
            ModalPopupExtenderRefundDelete.Show();
        }

        if (e.CommandName == "Print")
        {
            string ProjectID = Request.QueryString["proid"];
            string RefundID = e.CommandArgument.ToString();

            Telerik_reports.generate_refundform(ProjectID, RefundID);
        }
    }

    protected void lnkRefundDelete_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];

        string RefundID = hndDeleteRefundID.Value;

        if (RefundID != "")
        {
            bool sucess1 = ClstblProjectRefund.tblProjectRefund_Delete(RefundID);

            if (sucess1)
            {
                SetAdd1();
                BindRefundDetails(ProjectID);
            }
            else
            {
                SetError1();
            }


        }
    }
    #endregion

    #region Check Cout Code
    public void BindCount(SttblProjects stPro, string Location)
    {
        string ProjectID = Request.QueryString["proid"].ToString();
        DataTable dtPickList = ClsQuickStock.tbl_PickListLog_SelectByPId_CompnayID(ProjectID, "1");

        if (dtPickList.Rows.Count > 0)
        {
            ClearQtyCount();
        }
        else
        {
            string StockItemIDPanel = "";
            if (stPro.PanelBrandID != "")
            {
                StockItemIDPanel = ClsQuickStock.tblStockItems_GetFixItemIDByStockItemID("1", stPro.PanelBrandID);
            }
            string StockItemIDInverter = "";
            if (stPro.InverterDetailsID != "")
            {
                StockItemIDInverter = ClsQuickStock.tblStockItems_GetFixItemIDByStockItemID("1", stPro.InverterDetailsID);
            }

            BindPanel(StockItemIDPanel.ToString(), ddlStockAllocationStore.SelectedValue, stPro.NumberPanels);
            BindInverter(StockItemIDInverter.ToString(), ddlStockAllocationStore.SelectedValue, stPro.inverterqty);
        }

        CheckMsg();
    }

    protected void ClearQtyCount()
    {
        ClearQtyPanel();
        ClearQtyInverter();
    }

    public void ClearQtyPanel()
    {
        lblPl.Text = "0";
        lblApl.Text = "0";
        lblSMPL.Text = "0";
        lblPW.Text = "0";
        lblPt.Text = "0";
        lblPO.Text = "0";
        lnketap.Text = "0";
        lblPm.Text = "0";
    }

    public void ClearQtyInverter()
    {
        lblIl.Text = "0";
        lblAriseInv.Text = "0";
        lblSmInv.Text = "0";
        lblIW.Text = "0";
        lblIt.Text = "0";
        lblIo.Text = "0";
        lnketaI.Text = "0";
        lblIm.Text = "0";
    }

    public void BindPanel(string StockItemID, string LocationID, string OrderQuantity)
    {
        //For Panels
        if (LocationID != "")
        {
            ClsQuickStock.SttblStockQuantity stStockOrderPanel = ClsQuickStock.Sp_GetPanelCount_ByStockItemLocationID(StockItemID, LocationID);

            hndPanelID.Value = StockItemID;
            hndPanelLocation.Value = LocationID;
            lblPl.Text = stStockOrderPanel.StockLiveQty == null ? "0" : stStockOrderPanel.StockLiveQty;
            lblApl.Text = stStockOrderPanel.ArisePickListQty == null ? "0" : stStockOrderPanel.ArisePickListQty;
            lblSMPL.Text = stStockOrderPanel.SMPickListQty == null ? "0" : stStockOrderPanel.SMPickListQty;
            lblPW.Text = stStockOrderPanel.WholesaleQty == null ? "0" : stStockOrderPanel.WholesaleQty;
            lblPt.Text = stStockOrderPanel.Total == null ? "0" : stStockOrderPanel.Total;
            lblPO.Text = stStockOrderPanel.OrderQty == null ? "0" : stStockOrderPanel.OrderQty;
            lnketap.Text = stStockOrderPanel.ETAQty == null ? "0" : stStockOrderPanel.ETAQty;
            lblPm.Text = stStockOrderPanel.MinQty == null ? "0" : stStockOrderPanel.MinQty;
        }
        else
        {
            ClearQtyCount();
        }
    }

    public void BindInverter(string StockItemID, string LocationID, string OrderQuantity)
    {
        //For Inverter
        if (LocationID != "")
        {
            ClsQuickStock.SttblStockQuantity stStockOrderInverter = ClsQuickStock.Sp_GetPanelCount_ByStockItemLocationID(StockItemID, LocationID);

            hndInverterID.Value = StockItemID;
            hndInverterLocation.Value = LocationID;
            lblIl.Text = stStockOrderInverter.StockLiveQty == null ? "0" : stStockOrderInverter.StockLiveQty;
            lblAriseInv.Text = stStockOrderInverter.ArisePickListQty == null ? "0" : stStockOrderInverter.ArisePickListQty;
            lblSmInv.Text = stStockOrderInverter.SMPickListQty == null ? "0" : stStockOrderInverter.SMPickListQty;
            lblIW.Text = stStockOrderInverter.WholesaleQty == null ? "0" : stStockOrderInverter.WholesaleQty;
            lblIt.Text = stStockOrderInverter.Total == null ? "0" : stStockOrderInverter.Total;
            lblIo.Text = stStockOrderInverter.OrderQty == null ? "0" : stStockOrderInverter.OrderQty;
            lnketaI.Text = stStockOrderInverter.ETAQty == null ? "0" : stStockOrderInverter.ETAQty;
            lblIm.Text = stStockOrderInverter.MinQty == null ? "0" : stStockOrderInverter.MinQty;
        }
        else
        {
            ClearQtyCount();
        }
    }

    public void BindCountByRpt()
    {
        foreach (RepeaterItem item in rptPickList.Items)
        {
            //For Count
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");

            if (ddlStockItem.SelectedValue != string.Empty)
            {
                //ClearQtyCount();
                if (ddlStockCategoryID.SelectedValue == "1")
                {
                    if (item.Visible == true)
                    {
                        BindPanel(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue, txtQuantity.Text);
                    }
                    else
                    {
                        ClearQtyPanel();
                    }
                }
                else if (ddlStockCategoryID.SelectedValue == "2")
                {
                    if (item.Visible == true)
                    {
                        BindInverter(ddlStockItem.SelectedValue, ddlStockAllocationStore.SelectedValue, txtQuantity.Text);
                    }
                    else
                    {
                        ClearQtyInverter();
                    }
                }
            }
        }

        CheckMsg();
    }

    protected void CheckMsg()
    {
        if (Convert.ToInt32(lblPt.Text) < 0)
        {
            divpanel.Visible = true;
        }
        else
        {
            divpanel.Visible = false;
        }

        if (Convert.ToInt32(lblPt.Text) < Convert.ToInt32(lblPm.Text))
        {
            DivPanel1.Visible = true;
        }
        else
        {
            DivPanel1.Visible = false;
        }

        if (Convert.ToInt32(lblIt.Text) < 0)
        {
            divinverter.Visible = true;
        }
        else
        {
            divinverter.Visible = false;
        }

        if (Convert.ToInt32(lblIt.Text) < Convert.ToInt32(lblIm.Text))
        {
            DivInverter1.Visible = true;
        }
        else
        {
            DivInverter1.Visible = false;
        }

        string PanelQty = "0";
        string InverterQty = "0";
        int StckItemCategoryCount = 0;
        foreach (RepeaterItem item in rptPickList.Items)
        {
            //For Count
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");

            if (ddlStockItem.SelectedValue != string.Empty)
            {
                //ClearQtyCount();
                if (ddlStockCategoryID.SelectedValue == "1")
                {
                    if (item.Visible == true)
                    {
                        PanelQty = txtQuantity.Text;
                        StckItemCategoryCount++;
                    }
                }
                else if (ddlStockCategoryID.SelectedValue == "2")
                {
                    if (item.Visible == true)
                    {
                        InverterQty = txtQuantity.Text;
                        StckItemCategoryCount++;
                    }
                }
            }
        }

        if (Convert.ToInt32(lblPt.Text) < Convert.ToInt32(PanelQty))
        {
            divPanelQuantity.Visible = true;
        }
        else
        {
            divPanelQuantity.Visible = false;
        }

        if (Convert.ToInt32(lblIt.Text) < Convert.ToInt32(InverterQty))
        {
            divInverterQuantity.Visible = true;
        }
        else
        {
            divInverterQuantity.Visible = false;
        }

        if(hndPickListMode.Value == "Edit")
        {
            HidePicklistButtons(StckItemCategoryCount, btnUpdatePicklist);
        }
        else
        {
            HidePicklistButtons(StckItemCategoryCount, btnSavePickList);
        }
        

        //Hode Show PickList Button
        //if (Roles.IsUserInRole("Administrator"))
        //{
        //    btnSavePickList.Visible = true;
        //}
        //else
        //{
        //    if (StckItemCategoryCount == 0)
        //    {
        //        btnSavePickList.Visible = true;
        //    }
        //    else
        //    {
        //        if (divpanel.Visible == true || DivPanel1.Visible == true || divinverter.Visible == true || DivInverter1.Visible == true || divPanelQuantity.Visible == true || divInverterQuantity.Visible == true)
        //        {
        //            btnSavePickList.Visible = false;
        //        }
        //        else
        //        {
        //            if (lblPl.Text == "0" && lblApl.Text == "0" && lblSMPL.Text == "0" && lblPW.Text == "0" && lblPt.Text == "0" && lblPO.Text == "0" && lnketap.Text == "0" && lblPm.Text == "0" && lblIl.Text == "0" && lblAriseInv.Text == "0" && lblSmInv.Text == "0" && lblIW.Text == "0" && lblIt.Text == "0" && lblIo.Text == "0" && lnketaI.Text == "0" && lblIm.Text == "0")
        //            {
        //                btnSavePickList.Visible = false;
        //            }
        //            else
        //            {
        //                btnSavePickList.Visible = true;
        //            }
        //        }
        //    }
        //}
    }

    public void HidePicklistButtons(int StckItemCategoryCount, LinkButton btn)
    {
        if (Roles.IsUserInRole("Administrator"))
        {
            btn.Visible = true;
        }
        else
        {
            if (StckItemCategoryCount == 0)
            {
                btn.Visible = true;
            }
            else
            {
                if (divpanel.Visible == true || DivPanel1.Visible == true || divinverter.Visible == true || DivInverter1.Visible == true || divPanelQuantity.Visible == true || divInverterQuantity.Visible == true)
                {
                    btn.Visible = false;
                }
                else
                {
                    if (lblPl.Text == "0" && lblApl.Text == "0" && lblSMPL.Text == "0" && lblPW.Text == "0" && lblPt.Text == "0" && lblPO.Text == "0" && lnketap.Text == "0" && lblPm.Text == "0" && lblIl.Text == "0" && lblAriseInv.Text == "0" && lblSmInv.Text == "0" && lblIW.Text == "0" && lblIt.Text == "0" && lblIo.Text == "0" && lnketaI.Text == "0" && lblIm.Text == "0")
                    {
                        btn.Visible = false;
                    }
                    else
                    {
                        btn.Visible = true;
                    }
                }
            }
        }
    }
    #endregion

    #region Quantity Button Click
    protected void lblApl_Click(object sender, EventArgs e)
    {
        string StockItemID = hndPanelID.Value;
        string LocationID = hndPanelLocation.Value;

        if (StockItemID != "" && LocationID != "")
        {
            DataTable dt = ClstblProjects.tbl_PickListLog_SelectBy_CompnayID("1", StockItemID, LocationID);
            if (dt != null && dt.Rows.Count > 0)
            {
                rptAPL.DataSource = dt;
                rptAPL.DataBind();
                if (dt.Rows.Count > 5)
                {
                    div15.Attributes.Add("style", "overflow-x: scroll; height: 400px !important; max-width:90%; width:100%; margin-left:auto!important; margin-right:auto!important;");
                }
                else
                {
                    div15.Attributes.Add("style", "");
                }
            }
            else
            {
                SetNoRecords();
            }

            ModalPopupExtenderAPL.Show();
        }
    }
    #endregion

    #region Update Status
    protected void ddlProjectStatusID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //PanAddUpdate.Visible = false;
        ModalPopupExtenderStatus.Show();
        Div2.Visible = false;
        if (ddlProjectStatusID.SelectedValue != string.Empty)
        {
            if (ddlProjectStatusID.SelectedValue == "4")
            {
                trHold.Visible = true;
                ListItem item7 = new ListItem();
                item7.Text = "Select";
                item7.Value = "";
                ddlProjectOnHoldID.Items.Clear();
                ddlProjectOnHoldID.Items.Add(item7);
                ddlProjectOnHoldID.DataSource = ClstblProjectOnHold.tblProjectOnHold_Select();
                ddlProjectOnHoldID.DataValueField = "ProjectOnHoldID";
                ddlProjectOnHoldID.DataTextField = "ProjectOnHold";
                ddlProjectOnHoldID.DataMember = "ProjectOnHold";
                ddlProjectOnHoldID.DataBind();
            }
            else
            {
                trHold.Visible = false;
            }
            if (ddlProjectStatusID.SelectedValue == "6")
            {
                trCancel.Visible = true;
                ListItem item8 = new ListItem();
                item8.Text = "Select";
                item8.Value = "";
                ddlProjectCancelID.Items.Clear();
                ddlProjectCancelID.Items.Add(item8);
                ddlProjectCancelID.DataSource = ClstblProjectCancel.tblProjectCancel_SelectASC();
                ddlProjectCancelID.DataValueField = "ProjectCancelID";
                ddlProjectCancelID.DataTextField = "ProjectCancel";
                ddlProjectCancelID.DataMember = "ProjectCancel";
                ddlProjectCancelID.DataBind();
            }
            else
            {
                trCancel.Visible = false;
            }
            if (ddlProjectStatusID.SelectedValue == "6" || ddlProjectStatusID.SelectedValue == "4")
            {
                trComment.Visible = true;
                Div2.Visible = true;
            }
            else
            {
                Div2.Visible = false;
                trComment.Visible = false;
                trHold.Visible = false;
                trCancel.Visible = false;
            }
        }
        else
        {
            trComment.Visible = false;
            trHold.Visible = false;
            trCancel.Visible = false;
        }
        if (ddlProjectStatusID.SelectedValue == "3")
        {
            if (hndProjectStatusId.Value == "4")
            {
                Div2.Visible = true;
            }
            else
            {
                Div2.Visible = false;
            }
        }
        if (ddlProjectStatusID.SelectedValue == "20")
        {
            ddlcancelReason.Visible = true;
            precancel.Visible = true;
            Div2.Visible = true;
        }
        else
        {
            ddlcancelReason.Visible = false;
            Div2.Visible = false;
            precancel.Visible = false;
        }
    }

    protected void ibtnUpdateStatus_Onclick(object sender, EventArgs e)
    {
        string ProjectID = hndStatusProjectID.Value;
        string ProjectStatusID = ddlProjectStatusID.SelectedValue;
        string ProjectCancelID = ddlProjectCancelID.SelectedValue;
        string ProjectOnHoldID = ddlProjectOnHoldID.SelectedValue;
        string StatusComment = txtComment.Text;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmpID = stEmp.EmployeeID;
        bool suc = ClstblProjects.tblProjects_UpdateProjectStatus(ProjectID, ProjectStatusID, ProjectCancelID, ProjectOnHoldID, StatusComment);
        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert(ProjectStatusID, ProjectID, EmpID, st.NumberPanels);
        if (st.ProjectStatusID == "6" || st.ProjectStatusID == "4" || st.ProjectStatusID == "20" || st.ProjectStatusID == "3")
        {
            if ((ProjectStatusID == "6" || ProjectStatusID == "4" || ProjectStatusID == "20") && st.ProjectStatusID != "3")
            {

            }
            else
            {
                if (!string.IsNullOrEmpty(st.ActiveDate))
                {
                    if (ProjectStatusID == "3")//Active
                    {
                        int ActieDate = ClstblProjects.tblProjectStatusUpdateData_Insert(ProjectID, st.ProjectNumber, st.CustomerID, st.ContactID, "", "", "", "", "", "", "", "", "", EmpID);
                        if (ActieDate > 0)
                        {
                            bool s = ClstblProjects.tblprojects_UpdateActiveOldDate(ActieDate.ToString(), st.ActiveDate);
                        }
                        if (ddlProjectStatusID.SelectedValue == "3")
                        {
                            bool s89 = ClstblProjects.tblprojects_UpdateActiveDate(ProjectID, txtActiveDate1.Text);
                        }
                    }

                    if (ProjectStatusID == "6")//Cancel
                    {
                        //SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                        ClstblContacts.tblContacts_UpdateContLeadStatus("4", st.ContactID);

                        if (!string.IsNullOrEmpty(st.ActiveDate))
                        {
                            string CancelDate = DateTime.Now.AddHours(14).ToString();
                            int cancel = ClstblProjects.tblProjectStatusUpdateData_Insert(ProjectID, st.ProjectNumber, st.CustomerID, st.ContactID, "", "", "", ProjectCancelID, StatusComment, "", CancelDate, "", "", EmpID);
                            //  bool s = ClstblProjects.tblprojects_UpdateActiveOldDate(cancel.ToString(), st.ActiveDate);
                            //
                            if (cancel > 0)
                            {

                                bool s = ClstblProjects.tblprojects_UpdateActiveOldDate(cancel.ToString(), st.ActiveDate);

                                ///}
                            }
                        }

                    }
                    if (ProjectStatusID == "4")//OnHold
                    {

                        if (!string.IsNullOrEmpty(st.ActiveDate))
                        {
                            string HoldDate = DateTime.Now.AddHours(14).ToString();
                            int Hold = ClstblProjects.tblProjectStatusUpdateData_Insert(ProjectID, st.ProjectNumber, st.CustomerID, st.ContactID, ProjectOnHoldID, StatusComment, HoldDate, "", "", "", "", "", "", EmpID);

                            if (Hold > 0)
                            {

                                // bool s52 = ClstblProjects.tblprojects_UpdateActiveDate(ProjectID, txtActiveDate1.Text);
                                bool s = ClstblProjects.tblprojects_UpdateActiveOldDate(Hold.ToString(), st.ActiveDate);

                                //}
                            }
                        }
                    }
                    if (ProjectStatusID == "20")//PreCancel
                    {
                        string PreCancelDate = DateTime.Now.AddHours(14).ToString();
                        string PreCancelReason = txtPreCancelReason.Text;

                        if (!string.IsNullOrEmpty(st.ActiveDate))
                        {
                            //bool s = ClstblProjects.tblprojects_UpdateActiveDate(ProjectID, txtActiveDate1.Text);
                            int PreCancel = ClstblProjects.tblProjectStatusUpdateData_Insert(ProjectID, st.ProjectNumber, st.CustomerID, st.ContactID, "", "", "", "", "", ddlprecancel.SelectedValue, "", PreCancelReason, PreCancelDate, EmpID);

                            if (PreCancel > 0)
                            {

                                bool s8921 = ClstblProjects.tblprojects_UpdateActiveOldDate(PreCancel.ToString(), st.ActiveDate);
                            }
                        }
                    }
                }
            }

        }
        if (ProjectStatusID != null && ProjectStatusID != "")
        {


        }

        //if (txtActiveDate1.Text != null && txtActiveDate1.Text != "")
        //{
        //    bool s = ClstblProjects.tblprojects_UpdateActiveDate(ProjectID, txtActiveDate1.Text);
        //}
        if (suc)
        {
            ModalPopupExtenderStatus.Hide();
            txtComment.Text = string.Empty;
            ddlProjectStatusID.SelectedValue = "";
            ddlProjectOnHoldID.SelectedValue = "";
            ddlProjectCancelID.SelectedValue = "";
            trCancel.Visible = false;
            trComment.Visible = false;
            trHold.Visible = false;
        }

        BindGrid(0);
        BindCommonDetails();
        //divprojecttab.Visible = false;
        //PanAddUpdate.Visible = false;
        //lnkAdd.Visible = true;
    }
    #endregion

    #region Stock
    public void BindStockDropdown()
    {
        DataTable dt = ClstblContacts.tblContacts_GetInstallers();
        ddlInstallerPick.DataSource = dt;
        ddlInstallerPick.DataValueField = "ContactID";
        ddlInstallerPick.DataTextField = "Contact";
        ddlInstallerPick.DataBind();

        ddlInstallerPickUp.DataSource = dt;
        ddlInstallerPickUp.DataValueField = "ContactID";
        ddlInstallerPickUp.DataTextField = "Contact";
        ddlInstallerPickUp.DataBind();
    }

    public void BindRptPicklistStock()
    {
        string ProjectID = Request.QueryString["proid"];
        DataTable dt1 = ClsQuickStock.tbl_StockPickUpReturnDetail_SelectByProjectID(ProjectID);
        if (dt1.Rows.Count > 0)
        {
            string pickupreturndate = dt1.Rows[0]["PickUpReturnDate"].ToString();
            if (string.IsNullOrEmpty(pickupreturndate))
            {
                txtPickupReturnDate.Text = string.Empty;
            }
            else
            {
                txtPickupReturnDate.Text = Convert.ToDateTime(pickupreturndate).ToShortDateString();
            }
            txtPickupReturnNote.Text = dt1.Rows[0]["PickUpReturnNote"].ToString();
            if (!string.IsNullOrEmpty(dt1.Rows[0]["InstallerID"].ToString()))
            {
                ddlInstallerPick.SelectedValue = dt1.Rows[0]["InstallerID"].ToString();
            }
            //else
            //{
            //    BindDropDown();
            //}
            //ddlInstaller2.SelectedValue = dt1.Rows[0]["InstallerID"].ToString();
        }

        DataTable dtPickList = ClsQuickStock.tblPickUpChangeReason_SelectByPId(ProjectID);
        if (dtPickList.Rows.Count > 0)
        {
            //txtPickupNote.Text = dtPickList.Rows[0]["Notes"].ToString();
            txtPickupNote.Text = dtPickList.Rows[0]["PickUpNote"].ToString();

            if (string.IsNullOrEmpty(dtPickList.Rows[0]["PickUpDate"].ToString()))
            {
                txtpickupdate.Text = string.Empty;
            }
            else
            {
                txtpickupdate.Text = Convert.ToDateTime(dtPickList.Rows[0]["PickUpDate"]).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(dtPickList.Rows[0]["PInstallerID"].ToString()))
            {
                ddlInstallerPickUp.SelectedValue = dtPickList.Rows[0]["PInstallerID"].ToString();
            }

            divChangePickUp.Visible = true;
            rptChangePickUp.DataSource = dtPickList;
            rptChangePickUp.DataBind();
        }
        else
        {
            divChangePickUp.Visible = false;
        }

        if (dtPickList.Rows.Count > 5)
        {
            //$('#<%=divpl.ClientID %>').attr("style", "overflow-y: scroll; height: " + table_height + "px !important;");
            divreasontochange.Attributes.Add("style", "overflow-y: scroll; height: " + 150 + "px !important;");
        }
        else
        {
            divreasontochange.Attributes.Add("style", "");
        }
    }

    protected void btnStock_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);

        if (!string.IsNullOrEmpty(txtpickupdate.Text))
        {
            string PickUpdate = "";
            string CInstallerID = "";
            DataTable dtPickList = ClsQuickStock.tblPickUpChangeReason_SelectByPId(ProjectID);
            if (dtPickList.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dtPickList.Rows[0]["PickUpDate"].ToString()))
                {
                    PickUpdate = dtPickList.Rows[0]["PickUpDate"].ToString();
                }

                if (!string.IsNullOrEmpty(dtPickList.Rows[0]["PInstallerID"].ToString()))
                {
                    CInstallerID = dtPickList.Rows[0]["PInstallerID"].ToString();
                }
            }

            string EnteredPickUpdate = txtpickupdate.Text;
            EnteredPickUpdate = EnteredPickUpdate != "" ? Convert.ToDateTime(EnteredPickUpdate).ToString() : "";

            string EnteredCInstallerID = ddlInstallerPickUp.SelectedValue;
            string PickUpNote = txtPickupNote.Text;
            string ProjectNumber = st.ProjectNumber;
            string reasonid = "";
            string reason = "";
            string reasonnote = "";
            string UpdateDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string UpdatedBy = stEmp.EmployeeID;

            if ((EnteredPickUpdate != PickUpdate) || (EnteredCInstallerID != CInstallerID))
            {
                int sucTrack = ClsQuickStock.tblPickUpChangeReason_Insert(ProjectID, ProjectNumber, reasonid, reason, reasonnote, EnteredPickUpdate, EnteredCInstallerID, PickUpNote, UpdatedBy, UpdateDate);

                if (sucTrack == Convert.ToInt32(ProjectID))
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "12");

                    SetAdd1();
                    BindRptPicklistStock();
                    BindGrid(0);
                }
                else
                {
                    SetError1();
                }
            }
        }
        else
        {
            MsgError("Please Select PickUpDate");
        }

    }

    #endregion

    #region Install Done
    public void BindInstallDone()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);

        if (st2.ProjectIncomplete != "")
        {
            chkProjectIncomplete.Checked = Convert.ToBoolean(st2.ProjectIncomplete);
            txtIncomplateReason.Text = st2.ProjectInCompReason;
        }

        if (chkProjectIncomplete.Checked == true)
        {
            RequiredFieldValidator13.Visible = true;
        }
        else
        {
            RequiredFieldValidator13.Visible = false;
        }

        if (st.InstallCompleted != "")
        {
            txtInstallCompleted.Text = Convert.ToDateTime(st.InstallCompleted).ToShortDateString();
        }

        txtMeterAppliedRef.Text = st.MeterAppliedRef;
        txtInspectorName.Text = st2.InspectorName;
        try
        {
            if (st2.InspectionDate != "")
            {
                txtInspectionDate.Text = Convert.ToDateTime(st2.InspectionDate).ToShortDateString();
            }
        }
        catch (Exception ex)
        {
        }

        try
        {
            ddlcomcerti.SelectedValue = st.ComplainceCertificate;
        }
        catch (Exception ex) { }
    }

    protected void chkProjectIncomplete_CheckedChanged(object sender, EventArgs e)
    {
        if (chkProjectIncomplete.Checked == true)
        {
            RequiredFieldValidator13.Visible = true;
        }
        else
        {
            RequiredFieldValidator13.Visible = false;
        }
    }

    protected void btnSaveInstall_Click1(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stProj = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        try
        {
            if (stProj.ProjectStatusID == "12" || stProj.ProjectStatusID == "25" || stProj.ProjectStatusID == "10")
            {
                string ProjectIncomplete = Convert.ToString(chkProjectIncomplete.Checked);
                bool suc2 = ClsProjectSale.tblProjects2_UpdatePostInst(ProjectID, ProjectIncomplete, txtIncomplateReason.Text);
                if (chkProjectIncomplete.Checked == true)
                {
                    ProjectIncomplete = Convert.ToString(chkProjectIncomplete.Checked);

                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "25");
                    //bool suc2 = ClsProjectSale.tblProjects2_UpdatePostInst(ProjectID, ProjectIncomplete, txtIncomplateReason.Text);
                }
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "12");
                }
            }

            if (stProj.ProjectStatusID == "12" || stProj.ProjectStatusID == "25" || stProj.ProjectStatusID == "10")
            {
                bool Update = ClstblSalesinfo.tblProjects_Update_InstallCompleted(ProjectID, txtInstallCompleted.Text);
                bool s2 = ClstblProjects.tbl_PickListLog_Update_InstallCompleteDate(ProjectID, "1", txtInstallCompleted.Text);
                if (txtInstallCompleted.Text != "")
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "10");
                }
            }

            string MeterAppliedRef = txtMeterAppliedRef.Text;
            int complaincecertificate = Convert.ToInt32(ddlcomcerti.SelectedValue);

            //if (stProj.ProjectStatusID == "5" || stProj.ProjectStatusID == "10" || stProj.ProjectStatusID == "13" || stProj.ProjectStatusID == "14" || stProj.ProjectStatusID == "12" || stProj.ProjectStatusID == "11")
            //{
            //}

            ClstblSalesinfo.tblProjects_Update_MeterDetail(ProjectID, MeterAppliedRef, complaincecertificate.ToString());
            ClsProjectSale.tblProjects2_UpdatePostInstInspection(ProjectID, txtInspectorName.Text, txtInspectionDate.Text);

            BindInstallDone();
            BindGrid(0);
            SetAdd1();
        }
        catch (Exception ex)
        {
            SetError1();
        }
    }

    protected void rptPaymentDetailsInstall_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string InvoicePaymentID = e.CommandArgument.ToString();
        if (e.CommandName.ToLower() == "delete")
        {
            ModalPopupExtenderDelete.Show();
            //SttblInvoicePayments st = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(InvoicePaymentID);
            hdndelete.Value = InvoicePaymentID;
        }

        if (e.CommandName.ToLower() == "edit")
        {
            hndModeInstall.Value = "Update";
            hndInvoicePaymentIDInstall.Value = InvoicePaymentID;

            SttblInvoicePayments st = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(InvoicePaymentID);

            txtInvoicePayDateInstall.Text = Convert.ToDateTime(st.InvoicePayDate).ToShortDateString();
            txtInvoicePayTotalInstall.Text = Convert.ToDecimal(st.InvoicePayTotal).ToString("F");
            txtInvoicePayGSTInstall.Text = Convert.ToDecimal(st.InvoicePayGST).ToString("F");
            ddlInvoicePayMethodIDInstall.SelectedValue = st.InvoicePayMethodID;
            txtCCSurchargeInstall.Text = st.CCSurcharge;
            ddlRecByInstall.SelectedValue = st.EmployeeID;
            txtInvoiceNotesInstall.Text = st.PaymentNote;

            if (st.InvoicePayMethodID == "7" || st.InvoicePayMethodID == "3" || st.InvoicePayMethodID == "2")
            {
                txtReceiptNumberInstall.Text = st.ReceiptNumber;

                //DivReceiptNo.Visible = true;
                //DivTransactionCode.Visible = false;

                DivReceiptNoInstall.Attributes.Remove("style");
                DivTransactionCodeInstall.Attributes.Add("Display", "none");
            }
            else if (st.InvoicePayMethodID == "6")
            {
                //DivReceiptNo.Visible = false;
                DivReceiptNoInstall.Attributes.Add("Display", "none");

                txtTransactionCodeInstall.Text = st.TransactionCode;
                //DivTransactionCode.Visible = true;
                DivTransactionCodeInstall.Attributes.Remove("style");
            }
            else
            {
                //txtReceiptNumber.Visible = false;
                //DivReceiptNo.Visible = false;
                //DivTransactionCode.Visible = false;
                DivReceiptNoInstall.Attributes.Add("Display", "none");
                DivTransactionCodeInstall.Attributes.Add("Display", "none");
            }
        }
    }

    #endregion

    protected void btnInvoiceSaveInstall_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        if (stPro.InvoiceNumber == "")
        {
            if (ProjectID != "")
            {
                bool suc = ClstblProjects.tblProjects_UpdateInvoiceDoc(ProjectID, stPro.ProjectNumber);
            }
        }

        decimal invoicepayGST = Convert.ToDecimal(txtInvoicePayTotalInstall.Text) / 10;
        decimal invoicepayExGST = (Convert.ToDecimal(txtInvoicePayTotalInstall.Text) / 10) * 10;
        string InvoicePayExGST = Convert.ToString(invoicepayExGST);
        string InvoicePayGST = Convert.ToString(invoicepayGST);
        string InvoicePayTotal = txtInvoicePayTotalInstall.Text;
        string InvoicePayDate = txtInvoicePayDateInstall.Text;
        string ReceiptNumber = txtReceiptNumberInstall.Text;
        string InvoicePayMethodID = ddlInvoicePayMethodIDInstall.SelectedValue;
        string EmployeeID = ddlRecByInstall.SelectedValue;
        string CCSurcharge = txtCCSurchargeInstall.Text;
        string Refund = "False";
        string RefundType = "0";
        string UpdatedBy = stEmp.EmployeeID;
        string InvoiceNotes = stPro.InvoiceNotes;

        if (hndModeInstall.Value == "") //Insert Done
        {
            int success = ClstblInvoicePayments.tblInvoicePayments_Insert(ProjectID, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, InvoicePayDate, InvoicePayMethodID, CCSurcharge, EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);

            bool UpdateTrasactionCode = ClstblInvoicePayments.tblInvoicePayments_Update_TransactionCode(success.ToString(), txtTransactionCodeInstall.Text);

            bool UpdateNotes = ClstblInvoicePayments.tblInvoicePayments_Update_PaymentNote(success.ToString(), txtInvoiceNotesInstall.Text);

            if (success > 0)
            {
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }
        else //Update
        {
            bool success = ClstblInvoicePayments.tblInvoicePayments_Update(hndInvoicePaymentIDInstall.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, InvoicePayDate, InvoicePayMethodID, CCSurcharge, EmployeeID, UpdatedBy, ReceiptNumber);

            bool UpdateTrasactionCode = ClstblInvoicePayments.tblInvoicePayments_Update_TransactionCode(hndInvoicePaymentIDInstall.Value, txtTransactionCodeInstall.Text);

            bool UpdateNotes = ClstblInvoicePayments.tblInvoicePayments_Update_PaymentNote(hndInvoicePaymentIDInstall.Value, txtInvoiceNotesInstall.Text);

            if (success)
            {
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }

        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        if (dtCount.Rows.Count > 0)
        {
            if (stPro.TotalQuotePrice != string.Empty && dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                decimal BalanceOwing = balown / 10;
                string BalanceGST = Convert.ToString(BalanceOwing);
                string pInvoicePayTotal = dtCount.Rows[0]["InvoicePayTotal"].ToString();
                bool suc = ClstblInvoicePayments.tblProjects_UpdateInvoice(ProjectID, BalanceGST, InvoiceNotes, InvoicePayDate);

                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) <= 0)
                {
                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "0");
                }
                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) < Convert.ToDecimal(stPro.TotalQuotePrice))
                {
                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
                }
                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) >= Convert.ToDecimal(stPro.TotalQuotePrice))
                {
                    string EmpID = stEmp.EmployeeID;

                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
                    ClstblInvoicePayments.tblProjects_UpdateInvoiceFU(ProjectID);
                }
            }
        }

        status(stPro, ProjectID);

        /* ----------------- Send Mail ----------------- */

        StUtilities StUtilities = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        //SiteURL = st.siteurl;
        string from = StUtilities.from;
        string ProjectNumber = stPro.ProjectNumber;
        string PayDate = txtInvoicePayDateInstall.Text;
        string PaidAmount = txtInvoicePayTotalInstall.Text;
        string PaidBy = stEmp.EmpFirst + " " + stEmp.EmpLast;
        ReceiptNumber = txtReceiptNumberInstall.Text;
        //TextBox txtInvoiceFU = this.Parent.FindControl("txtInvoiceFU") as TextBox;

        if (ddlInvoicePayMethodIDInstall.SelectedValue == "7")
        {
            TextWriter txtWriter = new StringWriter() as TextWriter;
            string subject = "Cash Transaction";

            Server.Execute("~/mailtemplate/cashmail.aspx?ProjectNumber=" + ProjectNumber + "&PayDate=" + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(PayDate)) + "&PaidAmount=" + PaidAmount + "&ReceiptNumber=" + ReceiptNumber + "&PaidBy=" + PaidBy, txtWriter);
            try
            {
                Utilities.SendMail(from, "peter.eurosolar@gmail.com", subject, txtWriter.ToString(), "cash@eurosolar.com.au");
            }
            catch
            { }
        }

        /* --------------- Send Mail End --------------- */

        hndModeInstall.Value = "";
        BindQuoteDetails();
        Reset(stEmp.EmployeeID);
    }

    protected void lnkSendInvoice_Click(object sender, EventArgs e)
    {
        try
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

            #region SendMail
            if (!String.IsNullOrEmpty(st.InvoiceSent))
            {
                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                String Subject = "Arise Solar Tax Invoice";

                Server.Execute("~/mailtemplate/TaxInvoiceAttach.aspx?proid=" + ProjectID, txtWriter);

                //if (st.ProjectTypeID == "8")
                //{
                //    Telerik_reports.generate_mtceinvoice(ProjectID, "Save");
                //}
                //else
                //{
                Telerik_reports.generate_Taxinvoice(ProjectID, "Save");
                //}
                string FileName = st.ProjectNumber + "_TaxInvoice.pdf";

                string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SavePDF2\\", FileName);

                Utilities.SendMailWithAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), fullPath, FileName);

                //Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), fullPath, FileName);

                SiteConfiguration.deleteimage(FileName, "SavePDF2");

                MsgSendSuccess();
            }
            else
            {
                MsgError("Please create invoice first.");
            }
            #endregion
        }
        catch (Exception exc)
        {
            SetError1();
        }
    }

    protected void ddlStockAllocationStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCountByRpt();
    }

    #region Documnet Tab
    protected void rptQuoteDoc_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndProjectQuoteID = (HiddenField)e.Item.FindControl("hndProjectQuoteID");
            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc");
            LinkButton btnSendAMail = (LinkButton)e.Item.FindControl("btnSendAMail");
            Image Image2 = (Image)e.Item.FindControl("Image2");
            //Label lblsignchked = (Label)e.Item.FindControl("lblsignchked");
            //Label lblsignUnChked = (Label)e.Item.FindControl("lblsignUnChked");
            // System.Web.UI.WebControls.CheckBox chkSerialNo = (System.Web.UI.WebControls.CheckBox)e.Item.FindControl("chkSerialNo");

            SttblProjectQuotes st = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteID(hndProjectQuoteID.Value);

            int Signed = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, st.ProjectQuoteDoc);
            if (Signed == 1)
            {
                //lblsignchked.Visible = true;
                //lblsignUnChked.Visible = false;               
                //btnSendAMail.Visible = true;
                //btnSendAMail.PostBackUrl = "~/admin/adminfiles/company/company.aspx?m=pro&compid=" + Request.QueryString["compid"] + "&proid=" + Request.QueryString["proid"];
                Image2.Visible = true;

            }
            else
            {
                //lblsignchked.Visible = false;
                //lblsignUnChked.Visible = true;
                //btnSendAMail.Visible = false;
                Image2.Visible = false;
            }

            //hypDoc.NavigateUrl = pdfURL + "quotedoc/" + st.QuoteDoc;
            hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("quotedoc", st.QuoteDoc);
        }
    }

    protected void rptDocumemt_ItemDataBoundDoc(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView dr = (DataRowView)e.Item.DataItem;
            string Document = dr["Document"].ToString();
            string Doc = dr["Doc"].ToString();

            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc1");

            if (Document == "NearMap Photo")
            {
                //hypDoc.NavigateUrl = pdfURL + "NearMap/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("NearMap", Doc);
            }
            else if (Document == "Meter Photos Saved")
            {
                //hypDoc.NavigateUrl = pdfURL + "MP/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("MP", Doc);
            }
            else if (Document == "Electricity Bill Saved")
            {
                //hypDoc.NavigateUrl = pdfURL + "EB/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("EB", Doc);
            }
            else if (Document == "Prop Design Saved")
            {
                //hypDoc.NavigateUrl = pdfURL + "PD/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("PD", Doc);
            }
            else if (Document == "Payment Receipt")
            {
                //hypDoc.NavigateUrl = pdfURL + "PR/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("PR", Doc);
            }
            else if (Document == "Beat Quote")
            {
                //hypDoc.NavigateUrl = pdfURL + "BeatQuote/" + Doc;
                hypDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("BeatQuote", Doc);
            }
        }
    }
    #endregion

    #region Updaet VariationLog
    public void updateVariationLog(SttblProjects stPro)
    {
        #region Create Datatable For Veriation
        dtRptVeriation = new DataTable();
        dtRptVeriation.Columns.Add("VeriationID", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("Veriation", Type.GetType("System.String"));
        dtRptVeriation.Columns.Add("VaritionValue", Type.GetType("System.String"));

        DataRow dr1 = dtRptVeriation.NewRow();
        dr1["VeriationID"] = "1";
        dr1["Veriation"] = "House Type";
        dr1["VaritionValue"] = string.IsNullOrEmpty(stPro.VarHouseType) ? "0" : stPro.VarHouseType;
        dtRptVeriation.Rows.Add(dr1);

        DataRow dr2 = dtRptVeriation.NewRow();
        dr2["VeriationID"] = "2";
        dr2["Veriation"] = "Roof Type";
        dr2["VaritionValue"] = string.IsNullOrEmpty(stPro.VarRoofType) ? "0" : stPro.VarRoofType;
        dtRptVeriation.Rows.Add(dr2);

        DataRow dr3 = dtRptVeriation.NewRow();
        dr3["VeriationID"] = "3";
        dr3["Veriation"] = "Roof Angle";
        dr3["VaritionValue"] = string.IsNullOrEmpty(stPro.VarRoofAngle) ? "0" : stPro.VarRoofAngle;
        dtRptVeriation.Rows.Add(dr3);

        DataRow dr4 = dtRptVeriation.NewRow();
        dr4["VeriationID"] = "4";
        dr4["Veriation"] = "Meter Installation";
        dr4["VaritionValue"] = string.IsNullOrEmpty(stPro.VarMeterInstallation) ? "0" : stPro.VarMeterInstallation;
        dtRptVeriation.Rows.Add(dr4);

        DataRow dr5 = dtRptVeriation.NewRow();
        dr5["VeriationID"] = "5";
        dr5["Veriation"] = "Meter Upgrade";
        dr5["VaritionValue"] = string.IsNullOrEmpty(stPro.VarMeterUG) ? "0" : stPro.VarMeterUG;
        dtRptVeriation.Rows.Add(dr5);

        DataRow dr6 = dtRptVeriation.NewRow();
        dr6["VeriationID"] = "6";
        dr6["Veriation"] = "Asbestos";
        dr6["VaritionValue"] = string.IsNullOrEmpty(stPro.VarAsbestos) ? "0" : stPro.VarAsbestos;
        dtRptVeriation.Rows.Add(dr6);

        DataRow dr7 = dtRptVeriation.NewRow();
        dr7["VeriationID"] = "7";
        dr7["Veriation"] = "Split System";
        dr7["VaritionValue"] = string.IsNullOrEmpty(stPro.VarSplitSystem) ? "0" : stPro.VarSplitSystem;
        dtRptVeriation.Rows.Add(dr7);

        DataRow dr8 = dtRptVeriation.NewRow();
        dr8["VeriationID"] = "8";
        dr8["Veriation"] = "Cherry Picker";
        dr8["VaritionValue"] = string.IsNullOrEmpty(stPro.VarCherryPicker) ? "0" : stPro.VarCherryPicker;
        dtRptVeriation.Rows.Add(dr8);

        DataRow dr9 = dtRptVeriation.NewRow();
        dr9["VeriationID"] = "9";
        dr9["Veriation"] = "Travel Cost";
        dr9["VaritionValue"] = string.IsNullOrEmpty(stPro.VarTravelTime) ? "0" : stPro.VarTravelTime;
        dtRptVeriation.Rows.Add(dr9);

        DataRow dr10 = dtRptVeriation.NewRow();
        dr10["VeriationID"] = "10";
        dr10["Veriation"] = "Hot Water Meter";
        dr10["VaritionValue"] = string.IsNullOrEmpty(stPro.HotWaterMeter) ? "0" : stPro.HotWaterMeter;
        dtRptVeriation.Rows.Add(dr10);

        DataRow dr11 = dtRptVeriation.NewRow();
        dr11["VeriationID"] = "11";
        dr11["Veriation"] = "Smart Meter";
        dr11["VaritionValue"] = string.IsNullOrEmpty(stPro.SmartMeter) ? "0" : stPro.SmartMeter;
        dtRptVeriation.Rows.Add(dr11);

        DataRow dr12 = dtRptVeriation.NewRow();
        dr12["VeriationID"] = "12";
        dr12["Veriation"] = "Other";
        dr12["VaritionValue"] = string.IsNullOrEmpty(stPro.VarOther) ? "0" : stPro.VarOther;
        dtRptVeriation.Rows.Add(dr12);

        DataRow dr13 = dtRptVeriation.NewRow();
        dr13["VeriationID"] = "13";
        dr13["Veriation"] = "Special Discount";
        dr13["VaritionValue"] = string.IsNullOrEmpty(stPro.SpecialDiscount) ? "0" : stPro.SpecialDiscount;
        dtRptVeriation.Rows.Add(dr13);

        DataRow dr14 = dtRptVeriation.NewRow();
        dr14["VeriationID"] = "14";
        dr14["Veriation"] = "Manager Discount";
        dr14["VaritionValue"] = string.IsNullOrEmpty(stPro.mgrrdisc) ? "0" : stPro.mgrrdisc;
        dtRptVeriation.Rows.Add(dr14);

        DataRow dr15 = dtRptVeriation.NewRow();
        dr15["VeriationID"] = "15";
        dr15["Veriation"] = "Collection Charge";
        dr15["VaritionValue"] = string.IsNullOrEmpty(stPro.collectioncharge) ? "0" : stPro.collectioncharge;
        dtRptVeriation.Rows.Add(dr15);

        DataRow dr16 = dtRptVeriation.NewRow();
        dr16["VeriationID"] = "16";
        dr16["Veriation"] = "Old System Removal Cost";
        dr16["VaritionValue"] = string.IsNullOrEmpty(stPro.OldSystemRemovalCost) ? "0" : stPro.OldSystemRemovalCost;
        dtRptVeriation.Rows.Add(dr16);

        DataRow dr17 = dtRptVeriation.NewRow();
        dr17["VeriationID"] = "17";
        dr17["Veriation"] = "Tilt Kit Brackets";
        dr17["VaritionValue"] = string.IsNullOrEmpty(stPro.TiltkitBrackets) ? "0" : stPro.TiltkitBrackets;
        dtRptVeriation.Rows.Add(dr17);

        DataRow dr18 = dtRptVeriation.NewRow();
        dr18["VeriationID"] = "18";
        dr18["Veriation"] = "Other BasicCost";
        dr18["VaritionValue"] = string.IsNullOrEmpty(stPro.varOtherBasicCost) ? "0" : stPro.varOtherBasicCost;
        dtRptVeriation.Rows.Add(dr18);

        #endregion

        string projectNo = stPro.ProjectNumber;
        string createdOn = DateTime.Now.AddHours(15).ToString();
        string createdBy = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        for (int i = 0; i < dtRptVeriation.Rows.Count; i++)
        {
            string varitionId = dtRptVeriation.Rows[i]["VeriationID"].ToString();
            string varition = dtRptVeriation.Rows[i]["Veriation"].ToString();
            string varitionValue = dtRptVeriation.Rows[i]["VaritionValue"].ToString();

            if (Convert.ToDecimal(varitionValue).ToString("F") != "0.00")
            {
                string newValue = getVariationNewValue(varition);
                string isDeleted = "";
                if (Convert.ToDecimal(newValue).ToString() != "0.00")
                {
                    isDeleted = "False";
                }
                else
                {
                    isDeleted = "True";
                }

                if (Convert.ToDecimal(varitionValue).ToString("F") != Convert.ToDecimal(newValue).ToString("F"))
                {
                    int InsertLog = ClstblSalesinfo.tbl_VariationLog_Insert(projectNo, varitionId, varitionValue, newValue, createdOn, createdBy, isDeleted);
                }
            }
            else
            {
                string newValue = getVariationNewValue(varition);
                if (Convert.ToDecimal(newValue).ToString() != "0.00")
                {
                    int InsertLog = ClstblSalesinfo.tbl_VariationLog_Insert(projectNo, varitionId, varitionValue, newValue, createdOn, createdBy, "False");
                }
            }
        }
    }

    public string getVariationNewValue(string variation)
    {
        string newValue = "0.00";
        foreach (RepeaterItem item in rptVeriation.Items)
        {
            if (item.Visible != false)
            {
                DropDownList ddlVeriation = (DropDownList)item.FindControl("ddlVeriation");
                TextBox txtVariationValue = (TextBox)item.FindControl("txtVariationValue");

                if (ddlVeriation.SelectedItem.Text.Trim() == variation.Trim())
                {
                    newValue = txtVariationValue.Text;
                    break;
                }
            }
        }
        return newValue;
    }
    #endregion

    protected void ddlVeriation_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlVeriation = sender as DropDownList;
        int repeaterItemIndex = ((RepeaterItem)ddlVeriation.NamingContainer).ItemIndex;

        TextBox txtNotes = rptVeriation.Items[repeaterItemIndex].FindControl("txtNotes") as TextBox;

        notesHideShow(ddlVeriation.SelectedValue, txtNotes);
    }

    public static void notesHideShow(string variationId, TextBox txtNotes)
    {
        if (variationId == "13") // Special Discount
        {
            txtNotes.Visible = true;
        }
        else if (variationId == "14") // Manager Discount
        {
            txtNotes.Visible = true;
        }
        else if (variationId == "18") // Other BasicCost
        {
            txtNotes.Visible = true;
        }
        else
        {
            txtNotes.Visible = false;
        }
    }

    public void rqVelidateRemoveOldSystem()
    {
        //if (!string.IsNullOrEmpty(ddlOldPanel.SelectedValue) || !string.IsNullOrEmpty(txtOldPanelManual.Text))
        //{
        //    RqOldRemoveOldSystem.Enabled = false;
        //}
        //else
        //{
        //    RqOldRemoveOldSystem.Enabled = true;
        //}

        if (ddlProjectTypeID.SelectedValue == "3")
        {
            RqOldPanelManual.Enabled = true;
            RqOldPanel.Enabled = true;

            if (!string.IsNullOrEmpty(ddlOldPanel.SelectedValue))
            {
                RqOldPanelManual.Enabled = false;
            }
            else
            {
                RqOldPanelManual.Enabled = true;
            }

            if (!string.IsNullOrEmpty(txtOldPanelManual.Text))
            {
                RqOldPanel.Enabled = false;
            }
            else
            {
                RqOldPanel.Enabled = true;
            }

            RqtxtOldNumberOfPanels.Enabled = true;
            RqtxtOldNumberOfPanelsManual.Enabled = true;
            if (!string.IsNullOrEmpty(txtOldNumberOfPanels.Text) && txtOldNumberOfPanels.Text != "0")
            {
                RqtxtOldNumberOfPanelsManual.Enabled = false;
            }
            else
            {
                RqtxtOldNumberOfPanelsManual.Enabled = true;
            }

            if (!string.IsNullOrEmpty(txtOldNumberOfPanelsManual.Text))
            {
                RqtxtOldNumberOfPanels.Enabled = false;
            }
            else
            {
                RqtxtOldNumberOfPanels.Enabled = true;
            }
        }
        else
        {
            RqOldPanelManual.Enabled = false;
            RqOldPanel.Enabled = false;

            RqtxtOldNumberOfPanels.Enabled = false;
            RqtxtOldNumberOfPanelsManual.Enabled = false;
        }
    }

    public void removeOldPanel()
    {
        if(ddlProjectTypeID.SelectedValue == "3")
        {
            divProjectOpened.Attributes.Remove("class");
            divProjectOpened.Attributes.Add("class", "col-md-1");

            divManualQuoteNo.Attributes.Remove("class");
            divManualQuoteNo.Attributes.Add("class", "col-md-1");

            divRemoveOldSystem.Visible = true;
        }
        else
        {
            divProjectOpened.Attributes.Remove("class");
            divProjectOpened.Attributes.Add("class", "col-md-2");

            divManualQuoteNo.Attributes.Remove("class");
            divManualQuoteNo.Attributes.Add("class", "col-md-2");

            divRemoveOldSystem.Visible = false;
        }

        divOldPanelDetailsShow();
    }

    protected void divOldPanelDetailsShow()
    {
        if (ddlOldRemoveOldSystem.SelectedValue == "1")
        {
            divOldPanelDetails.Visible = true;
        }
        else
        {
            divOldPanelDetails.Visible = false;
        }
    }

    protected void ddlOldRemoveOldSystem_SelectedIndexChanged(object sender, EventArgs e)
    {

        divOldPanelDetailsShow();
    }

    protected void ddlExportDetail_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExportDetailsHS();
    }

    protected void ExportDetailsHS()
    {
        if (ddlExportDetail.SelectedValue == "Partial Export")
        {
            divKWExport.Visible = true;
            divKWNonExport.Visible = true;
        }
        else
        {
            divKWExport.Visible = false;
            divKWNonExport.Visible = false;
        }
    }

    protected void btnRetailerStatement_Click(object sender, EventArgs e)
    {
        //string ProjectID = Request.QueryString["proid"];
        //Telerik_reports.generate_RetailerStatement(ProjectID);
    }

    protected void btnCreateExportControlForm_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;
        string CreatedOn = DateTime.Now.AddHours(14).ToString();

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        
        SttblContacts stcont = ClstblContacts.tblContacts_SelectByCustomerID(st.CustomerID);

        string Address = st.InstallAddress + ',' + st.InstallCity + ',' + st.InstallState + '-' + st.InstallPostCode;
        int DocId = ClstblProjects.tblProjectAcknowledgement_Insert("1", ProjectID, st.Customer, Address, stcont.ContMobile, stcont.ContEmail, EmployeeID, CreatedOn);

        if (Convert.ToString(DocId) != "")
        {
            //
            //BindQuote(ProjectID);
            //BindQuoteDetails();
            BindExportControl(ProjectID);
        }
    }

    public void BindExportControl(string ProjectID)
    {
        DataTable dtExportCntrl = ClstblProjects.tblProjectAcknowledgement_SelectByProjectID(ProjectID, "1");
        if (dtExportCntrl.Rows.Count > 0)
        {
            DivExportCont.Visible = true;
            rptExportControl.DataSource = dtExportCntrl;
            rptExportControl.DataBind();
            if (dtExportCntrl.Rows.Count > 5)
            {
                divExpCont.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divExpCont.Attributes.Add("style", "");
            }
        }
        else
        {
            DivExportCont.Visible = false;
        }
    }

    public void BindFeedInTariff(string ProjectID)
    {
        DataTable dt = ClstblProjects.tblProjectAcknowledgement_SelectByProjectID(ProjectID, "2");
        if (dt.Rows.Count > 0)
        {
            DivFeed.Visible = true;
            rptFeedInTariff.DataSource = dt;
            rptFeedInTariff.DataBind();
            if (dt.Rows.Count > 5)
            {
                DivFT.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                DivFT.Attributes.Add("style", "");
            }
        }
        else
        {
            DivFeed.Visible = false;
        }
    }

    protected void btnFeedinTariff_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;
        string CreatedOn = DateTime.Now.AddHours(14).ToString();

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        SttblContacts stcont = ClstblContacts.tblContacts_SelectByCustomerID(st.CustomerID);

        string Address = st.InstallAddress + ',' + st.InstallCity + ',' + st.InstallState + '-' + st.InstallPostCode;
        int DocId = ClstblProjects.tblProjectAcknowledgement_Insert("2", ProjectID, st.Customer, Address, stcont.ContMobile, stcont.ContEmail, EmployeeID, CreatedOn);

        if (Convert.ToString(DocId) != "")
        {
            //
            //BindQuote(ProjectID);
            //BindQuoteDetails();

            BindFeedInTariff(ProjectID);
        }
    }

    protected void rptExportControl_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.CommandName.ToString() == "DownloadDoc")
        {
            string DocId = e.CommandArgument.ToString();

            Telerik_reports.generate_ExportControl(DocId, ProjectID);
        }
    }

    protected void rptFeedInTariff_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.CommandName.ToString() == "DownloadDoc")
        {
            string DocId = e.CommandArgument.ToString();

            Telerik_reports.generate_FeedInTariff(DocId, ProjectID);
        }
    }

    protected void lbtnExportControlSMS_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];
        
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        string ProjectNumber = st.ProjectNumber;
        
        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        //DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        DataTable dt = ClstblProjects.tblProjectAcknowledgement_SelectByProjectID(ProjectID, "1");
        if (dt.Rows.Count > 0)
        {
            string docNo = dt.Rows[0]["id"].ToString();

            int success = Clstbl_TokenData.tbl_TokenDataAcno_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, docNo, SignFlag);

            if (success > 0)
            {
                #region Send Sms
                SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

                if (st2.ContMobile.Substring(0, 1) == "0")
                {
                    st2.ContMobile = st2.ContMobile.Substring(1);
                }
                //Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenData_SelectById(success.ToString());
                st2.ContMobile = "+61" + st2.ContMobile;
                string mobileno = st2.ContMobile;
                //string mobileno = "+919924860723";
                string OwnerName = st2.ContFirst + " " + st2.ContLast;
                string messagetext = "Hello " + OwnerName + ",\nPlease open this URL to upload your signature on DocNo: " + docNo + ".\n" + Siteurl + "ExportControlSign.aspx?proid=" + ProjectID + "&rt=" + Token + "\n\n Arise Solar.";

                if (mobileno != string.Empty && messagetext != string.Empty)
                {
                    string AccountSid = "AC991f3f103f6a5f608278c1e283f139a1";
                    string AuthToken = "be9c00d8443b360082ab02d3d78969b2";
                    var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);//919879111739  +61451831980
                    var message = twilio.SendMessage("+61418671214", mobileno, messagetext);
                    //var message = twilio.SendMessage("+61418671214", "+61402262170", messagetext);
                    //Response.Write(message.Sid);

                    if (message.RestException == null)
                    {

                        // status = "success";
                        MsgSendSuccess();
                    }
                    else
                    {
                        string Errormsg = message.RestException.Message;
                        // status = "error";
                        MsgError(Errormsg.ToString());
                        //SetError1();
                    }

                }

                #endregion
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }
    }

    protected void lbtnExportControlEmail_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string ProjectNumber = st.ProjectNumber;

        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        //DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        DataTable dt = ClstblProjects.tblProjectAcknowledgement_SelectByProjectID(ProjectID, "1");
        if (dt.Rows.Count > 0)
        {
            string docNo = dt.Rows[0]["id"].ToString();

            int success = Clstbl_TokenData.tbl_TokenDataAcno_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, docNo, SignFlag);

            if (success > 0)
            {
                #region SendMail
                SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                String Subject = "Export Control Signature Request - " + ConfigurationManager.AppSettings["SiteName"].ToString();

                Server.Execute("~/mailtemplate/AcnoSignLinkMail.aspx?p=EC&proid=" + ProjectID + "&rt=" + Token, txtWriter);

                Utilities.SendMail(from, st2.ContEmail, Subject, txtWriter.ToString());

                //Utilities.SendMail(from, "suresh@localchamp.com.au", Subject, txtWriter.ToString());

                //string filepath = "http://localhost:57341//userfiles//quotedoctest//6558Quotation.pdf";
                //Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath);
                try
                {
                    MsgSendSuccess();
                }
                catch
                {
                }

                #endregion
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }
    }

    protected void lbtnFeedInTariffSMS_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        string ProjectNumber = st.ProjectNumber;

        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        //DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        DataTable dt = ClstblProjects.tblProjectAcknowledgement_SelectByProjectID(ProjectID, "2");
        if (dt.Rows.Count > 0)
        {
            string docNo = dt.Rows[0]["id"].ToString();

            int success = Clstbl_TokenData.tbl_TokenDataAcno_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, docNo, SignFlag);

            if (success > 0)
            {
                #region Send Sms
                SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

                if (st2.ContMobile.Substring(0, 1) == "0")
                {
                    st2.ContMobile = st2.ContMobile.Substring(1);
                }
                //Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenData_SelectById(success.ToString());
                st2.ContMobile = "+61" + st2.ContMobile;
                string mobileno = st2.ContMobile;
                //string mobileno = "+919924860723";
                string OwnerName = st2.ContFirst + " " + st2.ContLast;
                string messagetext = "Hello " + OwnerName + ",\nPlease open this URL to upload your signature on DocNo: " + docNo + ".\n" + Siteurl + "FeedInTariffSign.aspx?proid=" + ProjectID + "&rt=" + Token + "\n\n Arise Solar.";

                if (mobileno != string.Empty && messagetext != string.Empty)
                {
                    string AccountSid = "AC991f3f103f6a5f608278c1e283f139a1";
                    string AuthToken = "be9c00d8443b360082ab02d3d78969b2";
                    var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);//919879111739  +61451831980
                    var message = twilio.SendMessage("+61418671214", mobileno, messagetext);
                    //var message = twilio.SendMessage("+61418671214", "+61402262170", messagetext);
                    //Response.Write(message.Sid);

                    if (message.RestException == null)
                    {

                        // status = "success";
                        MsgSendSuccess();
                    }
                    else
                    {
                        string Errormsg = message.RestException.Message;
                        // status = "error";
                        MsgError(Errormsg.ToString());
                        //SetError1();
                    }

                }

                #endregion
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }
    }

    protected void lbtnFeedInTariffEmail_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string ProjectNumber = st.ProjectNumber;

        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        //DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        DataTable dt = ClstblProjects.tblProjectAcknowledgement_SelectByProjectID(ProjectID, "2");
        if (dt.Rows.Count > 0)
        {
            string docNo = dt.Rows[0]["id"].ToString();

            int success = Clstbl_TokenData.tbl_TokenDataAcno_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, docNo, SignFlag);

            if (success > 0)
            {
                #region SendMail
                SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                String Subject = "Feed In Tariff Acknowledgement Signature Request - " + ConfigurationManager.AppSettings["SiteName"].ToString();

                Server.Execute("~/mailtemplate/AcnoSignLinkMail.aspx?p=FIT&proid=" + ProjectID + "&rt=" + Token, txtWriter);

                Utilities.SendMail(from, st2.ContEmail, Subject, txtWriter.ToString());

                //Utilities.SendMail(from, "suresh@localchamp.com.au", Subject, txtWriter.ToString());

                //string filepath = "http://localhost:57341//userfiles//quotedoctest//6558Quotation.pdf";
                //Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath);
                try
                {
                    MsgSendSuccess();
                }
                catch
                {
                }

                #endregion
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }
    }
}


public class StockData
{
    public string StockModel { get; set; }
    public string StockWatts { get; set; }
    public string StockCapacity { get; set; }
    public string STC { get; set; }
    public string Rebate { get; set; }
    public string StockManufacturer { get; set; }
    public string StockSeries { get; set; }
    public string NoInverter { get; set; }
}

public class InvoiceData
{
    public string InvoicePayGST { get; set; }
    public string CCSurcharge { get; set; }
    public string RolesEnable { get; set; }
}

public class BookingData
{
    public string ID { get; set; }
    public string PL { get; set; }
    public string Apl { get; set; }
    public string SMPL { get; set; }
    public string PW { get; set; }
    public string PT { get; set; }
    public string divpanel { get; set; }
    public string PO { get; set; }
    public string ETA { get; set; }
    public string M { get; set; }

    public string IL { get; set; }
    public string IApl { get; set; }
    public string ISMPL { get; set; }
    public string IW { get; set; }
    public string IT { get; set; }
    public string divinverter { get; set; }
    public string IO { get; set; }
    public string IETA { get; set; }
    public string IM { get; set; }
}