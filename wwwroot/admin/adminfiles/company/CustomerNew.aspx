﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" Async="true"
    MaintainScrollPositionOnPostback="true" Culture="en-GB" UICulture="en-GB"
    CodeFile="CustomerNew.aspx.cs" Inherits="admin_adminfiles_company_CustomerNew" 
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/controls/companysummary.ascx" TagName="companysummary" TagPrefix="uc1" %>
<%@ Register Src="../../../includes/controls/info.ascx" TagName="info" TagPrefix="uc4" %>
<%@ Register Src="../../../includes/controls/NewProject.ascx" TagName="newproject" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="chkaddval" value="0" />
    <input type="hidden" id="chkaddval1" value="0" />
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endrequesthandler);
        function endrequesthandler(sender, args) {
            $('input[type=file]').bootstrapFileInput();
        }
    </script>
    <script src="../../assets/js/jquery.min.js"></script>
    <style>
        input[type=checkbox]:checked,
        input[type=radio]:checked,
        input[type=checkbox]:focus,
        input[type=radio]:focus {
            outline: none !important;
        }

            input[type=checkbox]:checked ~ .text:before,
            input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
                display: inline-block;
                content: '\f00c';
                background-color: #f5f8fc;
                -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                border-color: #333333;
            }

        input[type=checkbox]:hover ~ .text :before,
        input[type=radio]:hover ~ .text :before, input[type=radio]:hover ~ label:before {
            border-color: #737373;
        }

        input[type=checkbox]:active ~ .text :before,
        input[type=radio]:active ~ .text :before, input[type=radio]:active ~ label :before {
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
        }


        input[type=checkbox] ~ .text,
        input[type=radio] ~ .text, input[type=radio] ~ label {
            position: relative;
            z-index: 11;
            display: inline-block;
            margin: 0;
            line-height: 20px;
            min-height: 18px;
            min-width: 18px;
            font-weight: normal;
        }

            input[type=checkbox] ~ .text:before,
            input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                font-family: fontAwesome;
                font-weight: bold;
                font-size: 13px;
                color: #333333;
                content: "\a0";
                background-color: #fafafa;
                border: 1px solid #c8c8c8;
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                border-radius: 0;
                display: inline-block;
                text-align: center;
                vertical-align: middle;
                height: 18px;
                line-height: 16px;
                min-width: 18px;
                margin-right: 5px;
                margin-bottom: 2px;
                -webkit-transition: all 0.3s ease;
                -moz-transition: all 0.3s ease;
                -o-transition: all 0.3s ease;
                transition: all 0.3s ease;
            }

            input[type=checkbox] ~ .text:hover:before,
            input[type=radio] ~ .text:hover:before, input[type=radio] ~ label:hover:before {
                border-color: #737373;
            }

            input[type=checkbox] ~ .text:active:before,
            input[type=radio] ~ .text:active:before, input[type=radio] ~ label:active:before {
                -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
            }

            input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                border-radius: 100%;
                font-size: 10px;
                font-family: FontAwesome;
                line-height: 17px;
                height: 19px;
                min-width: 19px;
            }

        input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
            content: "\f111";
        }
    </style>
 
    <script type="text/javascript">
        $(document).ready(function () {

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //gridviewScroll();
        });

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }


    </script>


    <script>
        var focusedElementId = "";
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        function BeginRequestHandler(sender, args) {

            var fe = document.activeElement;
            if (fe != null) {
                focusedElementId = fe.id;
            } else {
                focusedElementId = "";
            }
        }

        prm.add_pageLoaded(pageLoaded);
        prm.add_beginRequest(BeginRequestHandler);

        function pageLoaded() {
            

            $("[data-toggle=tooltip]").tooltip();

            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

        }


    </script>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
                //  divexpandcollapse("", "");
            }
        }
    </script>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        function pageLoaded() {
            $(".myvalcomp").select2({
                //placeholder: "select",
                allowclear: true
            });
        }
    </script>
   
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        
          

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Customer
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
            </div>

            <div class="page-body padtopzero">
               
                <%--<asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div id="divleft" runat="server">
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server"><i class="icon-ok-sign"></i>&nbsp;Transaction Successful! </div>
                                <div class="alert alert-danger" id="PanError" runat="server">
                                    <i class="icon-remove-sign"></i>&nbsp;
                <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server"><i class="icon-remove-sign"></i>&nbsp;Record with this name already exists. </div>
                               
                                <div class="alert alert-info" id="PAnAddress" runat="server"><i class="icon-info-sign"></i>&nbsp;Record with this Address already exists. </div>
                                <div class="alert alert-info" id="divTeamTime" runat="server"><i class="icon-info-sign"></i>&nbsp;Appointment Time over. </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>--%>
  
                <div class="row">
                    <div>
                        <div class="hpanel">
                            <asp:Literal ID="lblCount" runat="server"></asp:Literal>
                            <div class="bodymianbg">
                                <div>
                                    <div class="col-md-12" id="divright" runat="server" visible="false">
                                        <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                            AutoPostBack="true">
                                            <cc1:TabPanel ID="TabSummary" runat="server" HeaderText="Summary">
                                                <ContentTemplate>
                                                    <div class="table-responsive">
                                                        <uc1:companysummary ID="companysummary1" runat="server" />
                                                    </div>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabInfo" runat="server" HeaderText="Follow Ups">
                                                <ContentTemplate>
                                                    <uc4:info ID="info1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                           
                                            <cc1:TabPanel ID="TabNewProject" runat="server" HeaderText="NewProject">
                                                <ContentTemplate>
                                                     <uc6:newproject ID="newProject1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                        </cc1:TabContainer>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

  
            <asp:HiddenField runat="server" ID="hdncountdata" />


            <div class="loaderPopUP">
                <script>
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_pageLoaded(pageLoadedpro);
                    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(beginrequesthandler);
                    // raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(endrequesthandler);

                    function beginrequesthandler(sender, args) {
                        //shows the modal popup - the update progress
                        $('.loading-container').css('display', 'block');

                    }
                    function endrequesthandler(sender, args) {
                        //hide the modal popup - the update progress
                    }

                    function pageLoadedpro() {

                        $('.loading-container').css('display', 'none');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $("[data-toggle=tooltip]").tooltip();
                        $('.datetimepicker1').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                    }
                </script>

            </div>      
                     </ContentTemplate>

        
    </asp:UpdatePanel>
</asp:Content>
