<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="admin_adminfiles_dashboard" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/includes/dashboard/leadcount.ascx" TagPrefix="uc3" TagName="leadcount" %>
<%@ Register Src="~/includes/dashboard/weeklystatus.ascx" TagPrefix="uc4" TagName="weeklystatus" %>
<%@ Register Src="~/includes/dashboard/teamstatusdeprec.ascx" TagPrefix="uc2" TagName="teamstatusdeprec" %>
<%@ Register Src="~/includes/dashboard/imdeprec.ascx" TagPrefix="uc1" TagName="imdeprec" %>
<%--<%@ Register Src="~/includes/dashboard/paymentstatus.ascx" TagPrefix="uc1" TagName="paymentstatus" %>
<%@ Register Src="~/includes/dashboard/projectstatus.ascx" TagPrefix="uc1" TagName="projectstatus" %>
<%@ Register Src="~/includes/dashboard/weatherreport.ascx" TagPrefix="uc1" TagName="weatherreport" %>
<%@ Register Src="~/includes/dashboard/task.ascx" TagPrefix="uc1" TagName="task" %>
<%@ Register Src="~/includes/dashboard/calender.ascx" TagPrefix="uc1" TagName="calender" %>
<%@ Register Src="~/includes/dashboard/instchartpre.ascx" TagPrefix="uc1" TagName="instchartpre" %>
<%@ Register Src="~/includes/dashboard/statewisepre.ascx" TagPrefix="uc1" TagName="statewisepre" %>
<%@ Register Src="~/includes/dashboard/receivable.ascx" TagPrefix="uc1" TagName="receivable" %>
<%@ Register Src="~/includes/dashboard/accountant.ascx" TagPrefix="uc1" TagName="accountant" %>
<%@ Register Src="~/includes/dashboard/purchasemanager.ascx" TagPrefix="uc1" TagName="purchasemanager" %>
<%@ Register Src="~/includes/dashboard/warehouse.ascx" TagPrefix="uc1" TagName="warehouse" %>
<%@ Register Src="~/includes/dashboard/stc.ascx" TagPrefix="uc1" TagName="stc" %>
<%@ Register Src="~/includes/dashboard/verification.ascx" TagPrefix="uc1" TagName="verification" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../js/jquery.min.js"></script>
     <script src="../js/jquery.easy-pie-chart.js"></script>
      <script src="../js/jquery.flot.min.js"></script>
     <script src="../js/jquery.flot.tooltip.min.js"></script>
  <script src="../js/jquery.flot.spline.js"></script>
<script src="../js/jquery.flot.pie.min.js"></script>

   
     <script src="../js/charts.js"></script>
    <%--, <i class="fa fa-map-marker fa-lg text-primary"></i>New York City--%>
     <%--<section class="row m-b-md ">
        <div class="col-sm-12 ">
        <div class="small-header transition animated fadeIn">
        	<div class="hpanel">
            <div class="panel-body">
            	<h2 class="font-light m-b-xs">Dashboard</h2> <br/>
                <small>Welcome To,
                <asp:Label ID="lblusername" runat="server"></asp:Label>
                    </small>
             Date<asp:Label ID="lbldate" runat="server"></asp:Label>
                  Time<asp:Label ID="lbltime" runat="server"></asp:Label>
            </div>
            </div>
        </div>
        </div>
    </section>--%>
<%--    Welcome  Neil Clark
Login Detail: Date: 19 July 2016 Time: 2:27 Pm--%>

    <div class="col-lg-12 text-center m-t-md">
                <h2>
                    Welcome   <asp:Label ID="lblusername" runat="server"></asp:Label>
                </h2>
        
                <p>
                     <strong>Login Detail:</strong>
                     <strong>Date:</strong><asp:Label ID="lbldate" runat="server"></asp:Label>
                    <strong>Time:</strong><asp:Label ID="lbltime" runat="server"></asp:Label>
                   
                </p>
            </div>

     <section>

        <%-- <div id="divemployee" runat="server">
                        <div class="databox-cell cell-3 text-align-right padding-10">
                            <div class="pull-right">
                                <div class="btn-group">
                                    <asp:DropDownList ID="ddlFollowUpEmployees" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                        <asp:ListItem Value="">Employee</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--<span class="databox-text white">13 DECEMBER</span>
                        </div>

                        <asp:Button runat="server" ID="btnFollowupgo" OnClick="btnFollowupgo_Click" CssClass="btn btn-primary" Text="Go" />
                    </div>--%>

     <div class="col-md-12">
         <uc3:leadcount runat="server" ID="leadcount1" visible="false" />
         <uc4:weeklystatus runat="server" ID="weeklystatus1" visible="false"/>
         </div>
    </section>

    <%--  <asp:UpdatePanel runat="server" ID="UpdateLead">
        <ContentTemplate>
            <section id="divTop10" runat="server" visible="false">
            <div class="col-md-12">
                <div class="col-md-6" id="PanGridLast10FollowUps" runat="server">
                    <div class="panlenew">
                        <section class="panel">
                            <%--<div class="panel-heading">
                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-sm btn-rounded btn-default dropdown-toggle" data-toggle="dropdown"><span class="dropdown-label">Week</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-select">
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Month</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Week</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Day</a></li>
                                </ul>
                            </div>
                            <a class="btn btn-default btn-icon btn-rounded btn-sm marginleft3" href="#">Go</a> <span class="badge bg-warning marginleft3">10</span>
                        </div>
                        <a href="#">Last 10 FollowUps</a>
                    </div>--%>
                          <%--  <div class="panel-heading">
                                <div class="pull-left">
                                    <h4>Todays FollowUps</h4>
                                </div>
                                <div class="pull-right" id="divemployee" runat="server" visible="false">

                                    <div class="btn-group">
                                        <asp:DropDownList ID="ddlFollowUpEmployees" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                            <asp:ListItem Value="">Employee</asp:ListItem>
                                        </asp:DropDownList>--%>
                                        <%--<asp:DropDownList CssClass="myval btn btn-sm btn-rounded" runat="server" ID="ddl">
                            <asp:ListItem>Month</asp:ListItem>
                            <asp:ListItem>Week</asp:ListItem>
                            <asp:ListItem>Day</asp:ListItem>
                        </asp:DropDownList>--%>
                                 <%--   </div>
                                    <%--<a class="btn btn-default btn-icon btn-rounded btn-sm marginleft3" href="#">Go</a>--%>
                                  <%--  <asp:Button runat="server" ID="btnFollowupgo" OnClick="btnFollowupgo_Click" CssClass="btn btn-primary" Text="Go" />--%>
                                    <%--<span class="badge bg-warning marginleft3">10</span>--%>
                             <%--   </div>
                               <a href="#">&nbsp;</a>
                            </div>
                            <div class="panel-body">
                                <%-- <h4><a href="#">Todays FollowUps</a></h4>--%>
                <%--                <div class="marginnone">
                                    <div class="tableblack">
                                        <div class="table-responsive" runat="server" id="divfollow">
                                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                            </div>
                                            <asp:GridView ID="GridViewLastFollowUp" DataKeyNames="CustomerID" runat="server" AllowPaging="true" PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" AllowSorting="true" 
                                                CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" OnPageIndexChanging="GridViewLastFollowUp_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                                        <ItemTemplate>
                                                            <%#Eval("NextFollowupDate","{0:dd MMM yyyy}")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="120px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkFullname" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=f&compid="+Eval("CustomerID")+"&contid="+ Eval("ContactID") %>'><%#Eval("Contact")%></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="170px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%#Eval("Description")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>--%>
                <%-- <div class="col-md-6" id="PanGridNext10FollowUps" runat="server" visible="false">
            <div class="panlenew">
                <section class="panel">
                    <div class="panel-heading">
                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-sm btn-rounded btn-default dropdown-toggle" data-toggle="dropdown"><span class="dropdown-label">Week</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-select">
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Month</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Week</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Day</a></li>
                                </ul>
                            </div>
                            <a class="btn btn-default btn-icon btn-rounded btn-sm marginleft3" href="#">Go</a> <span class="badge bg-warning marginleft3">10</span>
                        </div>
                        <a href="#">Reminder</a>
                    </div>
                    <div class="panel-body">
                        <h4><a href="#">Reminder</a></h4>
                        <div class="table-responsive">
                            <asp:GridView ID="GridViewNextFollowUp" DataKeyNames="CustomerID" runat="server"
                                AllowPaging="true" PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false"
                                AllowSorting="true" class="table table-bordered table-hover" OnPageIndexChanging="GridViewNextFollowUp_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                        <ItemTemplate>
                                            <%#Eval("NextFollowupDate","{0:dd MMM yyyy}")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkFullname" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=c&compid="+Eval("CustomerID")+"&contid="+ Eval("ContactID") %>'><%#Eval("Contact")%></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle Width="170px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%#Eval("Description")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </section>
            </div>
        </div>--%>
              <%--  <div class="col-md-6" id="PanGridNewLead" runat="server">
                    <div class="panlenew">
                        <section class="panel">--%>
                            <%--<div class="panel-heading">
                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-sm btn-rounded btn-default dropdown-toggle" data-toggle="dropdown"><span class="dropdown-label">Week</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-select">
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Month</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Week</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Day</a></li>
                                </ul>
                            </div>
                            <a class="btn btn-default btn-icon btn-rounded btn-sm marginleft3" href="#">Go</a> <span class="badge bg-warning marginleft3">10</span>
                        </div>
                        <a href="#">New Lead</a>
                    </div>--%>
                           <%-- <div class="panel-heading">
                                <div class="pull-left">
                                    <h4>New Lead</h4>
                                </div>
                                <div class="pull-right" id="divemployee1" runat="server" visible="false">
                                    <div class="btn-group">
                                        <asp:DropDownList ID="ddlnewLeadEmployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                            <asp:ListItem Value="">Employee</asp:ListItem>
                                        </asp:DropDownList>--%>
                                        <%--<asp:DropDownList CssClass="myval btn btn-sm btn-rounded" runat="server" ID="ddl">
                            <asp:ListItem>Month</asp:ListItem>
                            <asp:ListItem>Week</asp:ListItem>
                            <asp:ListItem>Day</asp:ListItem>
                        </asp:DropDownList>--%>
                                    <%--</div>--%>
                                    <%--<a class="btn btn-default btn-icon btn-rounded btn-sm marginleft3" href="#">Go</a>--%>
                                    <%--<asp:Button runat="server" ID="btnLead" OnClick="btnLead_Click" CssClass="btn btn-primary" Text="Go" />--%>
                                    <%--<span class="badge bg-warning marginleft3">10</span>--%>
                              <%--  </div>
                                <a href="#">&nbsp;</a>
                            </div>
                   
                            <div class="panel-body">
                                
                                <div class="marginnone">
                                    <div class="tableblack">
                                        <div class="table-responsive" runat="server" id="divlead">
                                            <div class="alert alert-info" id="Div1" runat="server" visible="false">
                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                            </div>
                                            <asp:GridView ID="GridViewNewLead" DataKeyNames="CustomerID" runat="server" AllowPaging="true"
                                                PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" AllowSorting="true"
                                                CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" OnPageIndexChanging="GridViewNewLead_PageIndexChanging" OnRowCreated="GridViewNewLead_RowCreated" OnDataBound="GridViewNewLead_DataBound">
                                                <Columns>--%>
                                                    <%--<asp:TemplateField HeaderText="Company #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerID") %>' />
                                                    <%#Eval("CompanyNumber")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="110px" />
                                            </asp:TemplateField>--%>
                                                   <%-- <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                            <asp:HyperLink ID="lnkCustomer" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%>&nbsp;<%#Eval("CompanyNumber")%></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Assigned To" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <%#Eval("AssignedTo")%>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <%#Eval("Location")%>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                        <ItemTemplate>
                                                            <%#Eval("Streetaddress")%>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <%#Eval("CustPhone")%>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <%#Eval("ContMobile")%>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle BackColor="#EFF3FB" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </PagerTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                </div>
            </section>--%>
   
                    
    <section  visible="false" id="divTeamStatus" runat="server">
      
        <div class="col-md-12">
        <uc2:teamstatusdeprec runat="server" id="teamstatusdeprec1" />
        <uc1:imdeprec runat="server" id="imdeprec1" />
        </div>
    </section>
   <%-- <section class=" bg-light marbtm20">
     <div class="col-md-12">
        <uc1:paymentstatus ID="paymentstatus1" runat="server" Visible="false"  />
        <uc1:projectstatus ID="projectstatus1" runat="server" Visible="false"/>
        </div>
    </section>
    <section class="row" runat="server" visible="false" id="divWeatherReport">
        <uc1:weatherreport ID="weatherreport1" runat="server" />
    </section>
    <section class="row">
    <div class="col-md-12">
    <div class="col-md-12">
        <uc1:task ID="task1" runat="server" Visible="false"/>
        <uc1:calender runat="server" ID="calender" Visible="false"/>
        <%--<uc8:imdeprec ID="imdeprec1" runat="server" Visible="false" />--%>
       <%-- </div>
        </div>
    </section>
    <section class="row" id="divPreInstaller" runat="server" visible="false">
        <uc1:instchartpre ID="instchartpre1" runat="server" />
        <uc1:statewisepre ID="statewisepre1" runat="server" />
    </section>--%>
 <%--   <section class="row" id="divPostInstaller" runat="server" visible="false">
    <div class="col-md-12">
    <div class="col-md-12">
        <uc1:receivable ID="receivable1" runat="server"/>
        </div>
        </div>
    </section>--%>

   <%-- <section class="row" id="divAccountant" runat="server" visible="false">
        <uc1:accountant ID="accountant1" runat="server" />
    </section>--%>
    <%--<section class="row" id="divPurchaseManager" runat="server" visible="false">
        <uc1:purchasemanager ID="purchasemanager1" runat="server" />
    </section>--%>
    <%--<section class="row" id="divWarehouseManager" runat="server" visible="false">
        <uc1:warehouse runat="server" id="warehouse1" />
    </section>--%>
    <%--<section class="row" id="divSTC" runat="server" visible="false">
        <uc1:stc runat="server" id="stc1" />
    </section>--%>
   <%-- <section class="row" id="divVerification" runat="server" visible="false">
        <uc1:verification runat="server" id="verification1" />
    </section>--%>

     <script>
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_pageLoaded(pageLoaded);
         //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
         prm.add_beginRequest(beginrequesthandler);
         // raised after an asynchronous postback is finished and control has been returned to the browser.
         prm.add_endRequest(endrequesthandler);
         function beginrequesthandler(sender, args) {
             //shows the modal popup - the update progress
             $('.loading-container').css('display', 'block');
         }
         function endrequesthandler(sender, args) {
             //hide the modal popup - the update progress
             $('.loading-container').css('display', 'none');
             if (args.get_error() != undefined) {
                 args.set_errorhandled(true);
             }
         }
         function pageLoaded() {
             $('.loading-container').css('display', 'none');
             $(".myval").select2({
                 //placeholder: "select",
                 allowclear: true
             });
             $(".myval1").select2({
                 minimumResultsForSearch: -1
             });

             $('.datetimepicker1').datetimepicker({
                 format: 'DD/MM/YYYY'
             });

         }
                        </script>
                              
        </contenttemplate>
      
    </asp:updatepanel>
    <style type="text/css">
        .selected_row
        {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridViewLastFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewLastFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=GridViewNextFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewNextFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=GridViewNewLead] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewNewLead] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

        });
        function doMyAction() {
            if ($.fn.select2 !== 'undefined') {
                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });
                $(".myval1").select2({
                    minimumResultsForSearch: -1
                });
            }
        }
    </script>
</asp:Content>
