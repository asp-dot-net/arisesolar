using System;
using System.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stockorder : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    protected string mode = "";
    protected string SiteURL;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        HidePanels();
        if (!IsPostBack)
        {

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindGrid(0);
            BindLocation();
            BindVendor();

            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }

        }

        ModeAddUpdate();
    }
    public void BindLocation()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlStockLocation.DataSource = dt;
        ddlStockLocation.DataTextField = "location";
        ddlStockLocation.DataValueField = "CompanyLocationID";
        ddlStockLocation.DataBind();

        rptstocklocation.DataSource = dt;
        rptstocklocation.DataBind();
    }

    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectVender();
        ddlVendor.DataSource = dt;
        ddlVendor.DataTextField = "Customer";
        ddlVendor.DataValueField = "CustomerID";
        ddlVendor.DataBind();

        ddlSearchVendor.DataSource = dt;
        ddlSearchVendor.DataTextField = "Customer";
        ddlSearchVendor.DataValueField = "CustomerID";
        ddlSearchVendor.DataBind();

        ddlloc.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlloc.DataTextField = "location";
        ddlloc.DataValueField = "CompanyLocationID";
        ddlloc.DataMember = "CompanyLocationID";
        ddlloc.DataBind();

    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();
        dt = ClstblStockOrders.tblStockOrders_SelectBySearch(ddlShow.SelectedValue, ddlDue.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), ddlloc.SelectedValue, "", "");

        int iTotalRecords = dt.Rows.Count;
        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(txtVendorInvoiceNo.Text);
        if (existaddress != 1)
        {
            string StockLocation = ddlStockLocation.SelectedValue.ToString();
            // string StockCategoryID = ddlStockCategoryID.SelectedValue.ToString();

            string DateOrdered = DateTime.Now.AddHours(14).ToString();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            string Notes = txtNotes.Text;
            string BOLReceived = txtBOLReceived.Text;
            string vendor = ddlVendor.SelectedValue.ToString();
            string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
            string ManualOrderNumber = txtManualOrderNumber.Text;
            string ExpectedDelivery = txtExpectedDelivery.Text;

            var vals = "";
            var vals1 = "";

            if (ddlStockLocation.SelectedValue != string.Empty)
            {
                vals = StockLocation_Text.Split(':')[0];
                vals1 = StockLocation_Text.Split(':')[1];
            }
            string state = Convert.ToString(vals);
            //string CompanyLocation = Convert.ToString(vals1).Substring(1);

            int success = ClstblStockOrders.tblStockOrders_Insert(DateOrdered, st.EmployeeID, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
            ClstblStockOrders.tblStockOrders_Update_OrderNumber(success.ToString(), success.ToString());
            ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(success.ToString(), txtVendorInvoiceNo.Text);

            foreach (RepeaterItem item in rptattribute.Items)
            {
                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");

                string StockItem = ddlStockItem.SelectedValue.ToString();
                string OrderQuantity = txtOrderQuantity.Text;
                string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
                if (!chkdelete.Checked)
                {
                    if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                    {
                        int success1 = ClstblStockOrders.tblStockOrderItems_Insert(Convert.ToString(success), StockItem, OrderQuantity, StockOrderItem, state);
                    }
                }
            }

            //--- do not chage this code start------
            if (success > 0)
            {
                SetAdd();
            }
            else
            {
                SetError();
            }
        }
        else
        {
            PAnAddress.Visible = true;
            //BindGrid(0);
            //lblexistame.Visible = true;
            //lblexistame.Text = "Record with this Address already exists ";
        }
        BindGrid(0);
        // Reset();
        mode = "Add";
        //--- do not chage this code end------

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        int exist = ClstblStockOrders.tblStockOrders_ExistsByIdVendorInvoiceNo(id1, txtVendorInvoiceNo.Text);
        if (exist == 0)
        {
            string Notes = txtNotes.Text;
            string BOLReceived = txtBOLReceived.Text;
            string vendor = ddlVendor.SelectedValue.ToString();
            string StockLocation = ddlStockLocation.SelectedValue.ToString();
            string StockLocation_Text = ddlStockLocation.SelectedItem.ToString();
            var vals = StockLocation_Text.Split(':')[0];
            var vals1 = StockLocation_Text.Split(':')[1];
            string state = Convert.ToString(vals);
            string ManualOrderNumber = txtManualOrderNumber.Text;
            string ExpectedDelivery = txtExpectedDelivery.Text;

            bool success = ClstblStockOrders.tblStockOrders_Update(id1, Notes, StockLocation, vendor, BOLReceived, ManualOrderNumber, ExpectedDelivery);
            ClstblStockOrders.tblStockOrders_Update_VendorInvoiceNo(id1, txtVendorInvoiceNo.Text);

            //foreach (RepeaterItem item in rptviewdata.Items)
            //{
            //    HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            //    CheckBox chkdelete1 = (CheckBox)item.FindControl("chkdelete1");
            //    if (chkdelete1.Checked)
            //    {
            //        ClstblStockOrders.tblStockOrderItems_Delete(hdnStockOrderItemID.Value);
            //    }
            //}
            ClstblStockOrders.tblStockOrderItems_whole_Delete(id1);
            foreach (RepeaterItem item in rptattribute.Items)
            {
                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
                HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
                string StockItem = ddlStockItem.SelectedValue.ToString();
                string OrderQuantity = txtOrderQuantity.Text;
                string StockOrderItem = ddlStockCategoryID.SelectedItem.ToString() + ": " + ddlStockItem.SelectedItem.ToString();

                CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");

                if (!chkdelete.Checked)
                {

                    if (ddlStockItem.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "" && txtOrderQuantity.Text != "")
                    {
                        int success1 = ClstblStockOrders.tblStockOrderItems_Insert(id1, StockItem, OrderQuantity, StockOrderItem, state);
                    }
                }

                if (chkdelete.Checked)
                {
                    ClstblStockOrders.tblStockOrderItems_Delete(hdnStockOrderItemID.Value);
                }
            }
            //--- do not chage this code Start------
            if (success)
            {
                SetUpdate();
            }
            else
            {
                SetError();
            }
            Reset();
        }
        else
        {
            //InitUpdate();
            PAnAddress.Visible = true;
        }
        BindScript();
        BindGrid(0);
        Response.Redirect(Request.Url.PathAndQuery);
        //--- do not chage this code end------
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);



        //--- do not chage this code end------
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanGrid.Visible = false;
        PanGridSearch.Visible = false;
        mode = "Edit";

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(id);
        ddlStockLocation.SelectedValue = st.CompanyLocationID;
        try
        {
            ddlVendor.SelectedValue = st.CustomerID;
        }
        catch { }
        txtBOLReceived.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.BOLReceived)); //st.BOLReceived;
        txtNotes.Text = st.Notes;
        txtManualOrderNumber.Text = st.ManualOrderNumber;
        txtExpectedDelivery.Text = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st.ExpectedDelivery));//Convert.ToDateTime(st.ExpectedDelivery).ToShortDateString();
        txtVendorInvoiceNo.Text = st.VendorInvoiceNo;

        DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(id);


        if (dt.Rows.Count > 0)
        {
            rptattribute.DataSource = dt;
            rptattribute.DataBind();
            MaxAttribute = dt.Rows.Count;

        }
        ModeAddUpdate();
        //MaxAttribute = 1;
        //bindrepeater();
        //BindAddedAttribute();
        //--- do not chage this code start------
        //lnkBack.Visible = true;
        InitUpdate();
        //--- do not chage this code end------
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName.ToString() == "Delivered")
        {
            //Response.Write("cbv");
            //Response.End();
            ModalPopupdeliver.Show();
            // Button lnkBtn = (Button)e.CommandSource;    // the button
            //GridViewRow myRow = (GridViewRow)lnkBtn.Parent.Parent;  // the row
            //GridView myGrid = (GridView)sender; // the gridview
            //GridViewRow gvr = (GridViewRow)lnkBtn.NamingContainer;
            string ID = e.CommandArgument.ToString();
            hndid.Value = ID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
            //hdnStockOrderID
            //int id = (int)GridView1.DataKeys[gvr.RowIndex].Value;
            //string ID = myGrid.DataKeys[myRow.RowIndex].Value.ToString();

            //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(ID, stEmployeeid.EmployeeID, "1");
            //SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(ID);
            //if (success)
            //{
            //    DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(ID);
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow row in dt.Rows)
            //        {
            //            SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
            //            ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
            //            ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
            //        }
            //    }
            //    SetDelete();
            //}
            //else
            //{
            //    SetError();
            //}
            //BindGrid(0);

        }

        if (e.CommandName == "Revert")
        {

            string ID = e.CommandArgument.ToString();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(ID, stEmployeeid.EmployeeID, "0");
            ClstblStockOrders.tblStockOrders_Update_ActualDelivery(ID, "");

            SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(ID);

            DataTable dtQty = ClstblStockOrders.tblStockOrderItems_SelectQty(ID);
            string OrderQuantity = "";
            string StockOrderID = "";
            string StockItemID = "";
            if (dtQty.Rows.Count > 0)
            {
                OrderQuantity = dtQty.Rows[0]["OrderQuantity"].ToString();
                StockOrderID = dtQty.Rows[0]["StockOrderID"].ToString();
                StockItemID = dtQty.Rows[0]["StockItemID"].ToString();
            }
            if (success)
            {
                ClstblStockOrders.tblStockSerialNo_Delete(ID);

                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(ID);
                if (dt.Rows.Count > 0)
                {


                    foreach (DataRow row in dt.Rows)
                    {
                        //  Response.Write(row["OrderQuantity"].ToString() + "==" + row["StockItemID"].ToString());
                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                        //Response.Write(StOldQty.StockQuantity);
                        //Response.End();
                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, (0 - Convert.ToInt32(row["OrderQuantity"].ToString())).ToString(), userid, "0");
                        ClstblStockItemsLocation.tblStockItemsLocation_Update_Revert(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                    }
                    //Response.End();
                }
                SetDelete();
            }
            else
            {
                SetError();
            }
            BindGrid(0);
        }

        if (e.CommandName == "print")
        {
            string StockOrderID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/reports/order.aspx?id=" + StockOrderID);
        }

        if (e.CommandName.ToLower() == "detail")
        {
            ModalPopupExtenderDetail.Show();
            string StockOrderID = e.CommandArgument.ToString();
            SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(StockOrderID);

            if (st.CompanyLocation != string.Empty)
            {
                lblStockLocation.Text = st.CompanyLocation;
            }
            else
            {
                lblStockLocation.Text = "-";
            }
            if (st.Vendor != string.Empty)
            {
                lblVendor.Text = st.Vendor;
            }
            else
            {
                lblVendor.Text = "-";
            }
            if (st.BOLReceived != string.Empty)
            {
                lblBOLReceived.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.BOLReceived));
            }
            else
            {
                lblBOLReceived.Text = "-";
            }
            if (st.ManualOrderNumber != string.Empty)
            {
                lblManualOrderNo.Text = st.ManualOrderNumber;
            }
            else
            {
                lblManualOrderNo.Text = "-";
            }
            if (st.ExpectedDelivery != string.Empty)
            {
                lblExpectedDelevery.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ExpectedDelivery));
            }
            else
            {
                lblExpectedDelevery.Text = "-";
            }
            DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(StockOrderID);
            if (dt.Rows.Count > 0)
            {
                rptOrder.DataSource = dt;
                rptOrder.DataBind();
                trOrderItem.Visible = true;
            }
            else
            {
                trOrderItem.Visible = false;
            }
        }
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);

        }
        BindScript();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindScript();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        MaxAttribute = 1;
        bindrepeater();
        BindAddedAttribute();
        BindScript();
        PanGridSearch.Visible = false;
        PanGrid.Visible = false;
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Reset();
        InitAdd();
        mode = "Add";
        if (mode == "Add")
        {
            MaxAttribute = 1;
            bindrepeater();
            BindAddedAttribute();
        }
        PanGrid.Visible = false;
        //  PanGridSearch.Visible = false;
        BindScript();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void RecordMisMatch()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunRecordMismatch();", true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        PanGrid.Visible = true;
        PanGridSearch.Visible = true;

        PanAddUpdate.Visible = true;
        rptattribute.DataSource = null;
        rptattribute.DataBind();
        //rptviewdata.DataSource = null;
        //rptattribute.DataBind();
        ddlStockLocation.ClearSelection();
        ddlVendor.ClearSelection();
        txtBOLReceived.Text = "";
        txtNotes.Text = "";
        txtManualOrderNumber.Text = string.Empty;
        txtExpectedDelivery.Text = string.Empty;
        //rptviewdata.Visible = false;

        ddlstockcategory.ClearSelection();
        //txtstockitem.Text = string.Empty;
        //txtbrand.Text = string.Empty;
        //txtmodel.Text = string.Empty;
        //txtseries.Text = string.Empty;
        //txtminstock.Text = string.Empty;
        //chksalestag.Checked = false;
        //chkisactive.Checked = false;
        //txtdescription.Text = string.Empty;
        //txtStockSize.Text = string.Empty;
        txtVendorInvoiceNo.Text = string.Empty;
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            txtqty.Text = "0";
        }
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtSysDetails");
            txtOrderQuantity.Text = "";
            ddlStockItem.SelectedValue = "";
            ddlStockCategoryID.SelectedValue = "";
        }

    }

    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    public void ModeAddUpdate()
    {
        if (mode == "Add")
        {
            // //lblAddUpdate.Text = "Add ";
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnReset.Visible = true;
            btnCancel.Visible = true;
        }
        if (mode == "Edit")
        {
            // //lblAddUpdate.Text = "Update ";
            btnAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = false;
        }
    }

    protected void ddlStockLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

            ddlStockItem.Items.Clear();

            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            if (ddlStockLocation.SelectedValue != "" && ddlStockCategoryID.SelectedValue != "")
            {

                DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
                ddlStockItem.DataSource = dtStockItem;
                ddlStockItem.DataTextField = "StockItem";
                ddlStockItem.DataValueField = "StockItemID";
                ddlStockItem.DataBind();
            }
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(
        //             PanGridSearch,
        //             this.GetType(),
        //             "MyAction",
        //             "doMyAction();",
        //             true);

        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

        if (ddlStockLocation.SelectedValue != "")
        {
            ddlStockItem.Items.Clear();
            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlStockItem.Items.Add(item1);

            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(ddlStockCategoryID.SelectedValue.ToString(), ddlStockLocation.SelectedValue.ToString());
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();
        }

        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;

        ModeAddUpdate();
        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        //ModeAddUpdate();
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        //  BindScript();
    }

    protected void btnAddUpdateRow_Click(object sender, EventArgs e)
    {
        InitUpdate();
        AddmoreAttribute();
    }

    protected void rptattribute_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtOrderQuantity");

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategory = (HiddenField)e.Item.FindControl("hdnStockCategory");

        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_ByCategory_Location(hdnStockCategory.Value, ddlStockLocation.SelectedValue.ToString());
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();
        HiddenField hdnOrderQuantity = (HiddenField)e.Item.FindControl("hdnOrderQuantity");
        ddlStockCategoryID.SelectedValue = hdnStockCategory.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;
        txtOrderQuantity.Text = hdnOrderQuantity.Value;
        //RequiredFieldValidator RequiredFieldValidator11 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator11");
        //RequiredFieldValidator RequiredFieldValidator1 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator1");
        //RequiredFieldValidator RequiredFieldValidator19 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator19");

        if (e.Item.ItemIndex == 0)
        {
            //btnDelete.Visible = false;
            chkdelete.Visible = false;
            litremove.Visible = false;
            //RequiredFieldValidator11.Visible = true;
            // RequiredFieldValidator1.Visible = true;
            //   RequiredFieldValidator19.Visible = true;
        }
        else
        {
            //btnDelete.Visible = true;
            chkdelete.Visible = true;
            litremove.Visible = true;

            //RequiredFieldValidator11.Visible = false;
            //RequiredFieldValidator1.Visible = false;
            //  RequiredFieldValidator19.Visible = false;
        }
        // HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");

        //Response.Write(hdnStockCategory.Value);
        //Response.End();

    }

    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
        rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));
        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtOrderQuantity");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            DataRow dr = rpttable.NewRow();
            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["OrderQuantity"] = txtOrderQuantity.Text;
            dr["StockOrderItemID"] = hdnStockOrderItemID.Value;
            dr["type"] = hdntype.Value;

            rpttable.Rows.Add(dr);
        }
    }

    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("OrderQuantity", Type.GetType("System.String"));
            rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["OrderQuantity"] = "";
            dr["StockOrderItemID"] = "";
            dr["type"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }

    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    ddlShow.SelectedValue = "";
    //    ddlDue.SelectedValue = "";
    //    ddlSearchVendor.SelectedValue = "";
    //    ddlDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtSearchOrderNo.Text = string.Empty;

    //    BindGrid(0);
    //}
    //protected void btnNewVendor_Click(object sender, ImageClickEventArgs e)
    //{
    //    PanAddUpdate.Visible = true;
    //    ModalPopupExtenderVendor.Show();
    //    txtCompany.Text = string.Empty;
    //    txtContFirst.Text = string.Empty;
    //    txtContLast.Text = string.Empty;
    //    BindScript();
    //}
    protected void ibtnaddvendor_click(object sender, EventArgs e)
    {

        if (txtCompany.Text != string.Empty)
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string employeeid = st.EmployeeID;

            int success = ClstblCustomers.tblCustomers_Insert("", employeeid, "false", "13", "3", "", "", "", "1", employeeid, "", "", "", "", "", txtCompany.Text, "", "", "", "", "", "", "", "", "", "", "australia", "", "", "", "", "", "", "", "", "", "", "false", "", "", "", "", "", "", "", "");
            int succontacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), "", txtContFirst.Text.Trim(), txtContLast.Text.Trim(), "", "", employeeid, employeeid);
            int succustinfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "customer entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(succontacts), employeeid, "1");

            if (Convert.ToString(success) != string.Empty)
            {
                ModalPopupExtenderVendor.Hide();
                BindVendor();
                ddlVendor.SelectedValue = Convert.ToString(success);
                SetAdd1();
            }
            else
            {
                SetError1();

            }
        }
    }
    protected void btnNewStock_Click(object sender, EventArgs e)
    {
        ddlstockcategory.ClearSelection();
        txtstockitem.Text = string.Empty;
        txtbrand.Text = string.Empty;
        txtmodel.Text = string.Empty;
        txtseries.Text = string.Empty;
        txtminstock.Text = string.Empty;
        chksalestag.Checked = false;
        chkisactive.Checked = false;
        txtdescription.Text = string.Empty;
        chkDashboard.Checked = false;

        PanAddUpdate.Visible = true;
        ModalPopupExtenderStock.Show();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlstockcategory.Items.Clear();
        ddlstockcategory.Items.Add(item8);

        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlstockcategory.DataSource = dtStockCategory;
        ddlstockcategory.DataTextField = "StockCategory";
        ddlstockcategory.DataValueField = "StockCategoryID";
        ddlstockcategory.DataBind();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        BindScript();
    }
    protected void ibtnAddStock_Click(object sender, EventArgs e)
    {
        InitAdd();
        string stockcategory = ddlstockcategory.SelectedValue;
        string stockitem = txtstockitem.Text;
        string brand = txtbrand.Text;
        string model = txtmodel.Text;
        string series = txtseries.Text;
        string minstock = txtminstock.Text;
        string salestag = chksalestag.Checked.ToString();
        string isactive = chkisactive.Checked.ToString();
        string description = txtdescription.Text;
        string IsDashboard = chkDashboard.Checked.ToString();

        int success = ClstblStockItems.tblStockItems_Insert(stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard);
        ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            HiddenField hyplocationid = (HiddenField)item.FindControl("hyplocationid");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            if (txtqty.Text != "" || hyplocationid.Value != "")
            {
                ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), hyplocationid.Value, txtqty.Text.Trim());
            }
        }
        if (success > 0)
        {
            ModalPopupExtenderStock.Hide();
            SetAdd1();
        }
        else
        {
            ModalPopupExtenderStock.Show();
            SetError1();
        }
        BindScript();
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }


    protected void btndeliver_Click(object sender, EventArgs e)
    {

        string receive = txtdatereceived.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
        //if (receive != string.Empty)
        //{
        //    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
        //}

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
        //Response.Write(success);
        //Response.End();
        //if (success)
        //{
        //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
        //if (dt.Rows.Count > 0)
        //{
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
        //        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
        //        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
        //    }
        //}
        //SetDelete();

        int success1 = 0;
        //Response.Expires = 400;
        //Server.ScriptTimeout = 1200;

        if (FileUpload1.HasFile)
        {
            //SetAdd1();
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
            string connectionString = "";

            //if (FileUpload1.FileName.EndsWith(".csv"))
            //{
            //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
            //}
            // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

            //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";


            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (FileUpload1.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
            }


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    // string employeeid = string.Empty;
                    //string flag = "true";
                    // SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);

                    DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum(hndid.Value);
                    DataRow dr2 = dt3.Rows[0];
                    string qty = dr2["Qty"].ToString();

                    DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                    DataRow dr1 = dt2.Rows[0];
                    string stockid = dr1["StockItemID"].ToString();
                    int count = 0;
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        //Response.Write(dr.Depth);
                        //Response.End();
                        int blank = 0;

                        if (dr.HasRows)
                        {


                            while (dr.Read())
                            {

                                string SerialNo = "";
                                string Pallet = "";
                                SerialNo = dr["Serial No"].ToString();
                                Pallet = dr["Pallet"].ToString();
                                if (SerialNo == "" && Pallet == "")
                                {
                                    blank = 1;
                                    break;
                                }
                                if (blank != 1)
                                {
                                    count++;
                                }
                            }
                            //Response.Write();
                        }
                    }
                    //Response.Write(count);
                    //Response.End();
                    if (count == Convert.ToInt32(qty))
                    {
                        using (DbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {
                                //SetAdd1();

                                //if (flag == "false")
                                //{
                                //    PanEmpty.Visible = true;
                                //}
                                //else
                                //{


                                while (dr.Read())
                                {

                                    int blank2 = 0;
                                    string SerialNo = "";
                                    string Pallet = "";

                                    SerialNo = dr["Serial No"].ToString();
                                    Pallet = dr["Pallet"].ToString();
                                    if (SerialNo == "" && Pallet == "")
                                    {
                                        blank2 = 1;
                                    }
                                    if (blank2 != 1)
                                    {
                                        if ((SerialNo != string.Empty) || (Pallet != string.Empty))
                                        {
                                            //try
                                            //{

                                            //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                            // SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                            //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                            //Response.End();
                                            // success1 = ClstblStockOrders.tblStockSerialNo_Insert(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet);
                                            string StockOrderID = hndid.Value;
                                            success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID);
                                            //}
                                            //catch { }
                                        }
                                        if (success1 > 0)
                                        {
                                            SetAdd1();
                                        }
                                        else
                                        {
                                            SetError1();
                                        }
                                    }
                                    //}
                                }
                            }
                        }
                        bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
                        if (success)
                        {
                            if (receive != string.Empty)
                            {
                                ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                            }

                            DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow row in dt.Rows)
                                {
                                    SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                                    ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                                    ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                                }
                            }
                        }
                        else
                        {
                            SetError();
                        }
                    }
                    else
                    {
                        RecordMisMatch();
                    }

                }
            }
        }
        else
        {
            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
            if (success)
            {
                if (receive != string.Empty)
                {
                    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                }

                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                    }
                }
                SetAdd1();
            }
            else
            {
                SetError();
            }

        }

        //}
        //else
        //{
        //    SetError();
        //}
        txtdatereceived.Text = string.Empty;

        // txtserialno.Text = string.Empty;


        BindGrid(0);
    }

    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnNewVendor_Click1(object sender, EventArgs e)
    {

        ModalPopupExtenderVendor.Show();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        PanGridSearch.Visible = false;
        txtCompany.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        BindScript();
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlShow.SelectedValue = "";
        ddlDue.SelectedValue = "";
        ddlSearchVendor.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchOrderNo.Text = string.Empty;
        ddlloc.SelectedValue = "";
        BindGrid(0);
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void btnDelivered_Click(object sender, ImageClickEventArgs e)
    {

    }


    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblStockOrders.tblStockOrders_Update_Cancelled(id, Convert.ToString(true));
        ClstblStockOrders.tblStockOrderItems_DeleteStockOrderID(id);
        ClstblStockOrders.tblStockOrders_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
        }

        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;


        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        //Me.MyDataSet.Clear();
        //Me.MyDataSet.EnforceConstraints = False;
       
        DataTable dt = ClstblStockOrders.tblStockOrders_SelectBySearchExcel(ddlShow.SelectedValue, ddlDue.SelectedValue, ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlDate.SelectedValue, txtSearchOrderNo.Text.Trim(), ddlloc.SelectedValue, "", "");
        Response.Clear();
        //dt.Constraints = false;
        int count = dt.Rows.Count;
        try
        {
            Export oExport = new Export();
            string FileName = "Stock Order" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 1,17,29,23,15,33,25,34,30,28 };
            string[] arrHeader = { "Order Number","Invoice Number","Ordered Date"
                    ,"Stock For","Manual Order No","Items Ordered","Vendor","Total Order Qty","Delivery","Delivered By"
                };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
}