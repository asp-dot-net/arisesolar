﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class admin_adminfiles_stock_ViewSerialHistory : System.Web.UI.Page
{
    static DataView dv;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string Location;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid(0);
        }
    }

    protected DataTable GetGridData()
    {
        //String SerailNo = Request.QueryString["SerailNo"].ToString();
        ////String active = ddlActive.SelectedValue;
        //DataTable dt = ClstblMaintainHistory.tblMaintainHistory_GetData_BySerialNo(SerailNo);
        ////DataTable dt = ClstblStockItems.tblStockItemsGetDataBySearch(txtSearchStockItem.Text, txtSearchManufacturer.Text, ddlcategorysearch.SelectedValue.ToString(), ddllocationsearch.SelectedValue.ToString(), ddlSearchState.SelectedValue.ToString(), ddlActive.SelectedValue.ToString(), ddlSalesTag.SelectedValue.ToString(), txtExpiryDate.Text);

        //return dt;
        return null;
    }
    public void BindGrid(int deleteFlag)
    {
        //DataTable dt = ClstblStockItems.tblStockSerialNo_Getdata_ByStockItemID(StockItemID.ToString());
        //if (dt.Rows.Count > 0)
        //{
        //    GridView1.DataSource = dt;
        //    GridView1.DataBind();
        //}

        //PanAddUpdate.Visible = false;
        //PanGridSearch.Visible = true;
        //Panel3.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                // SetDelete();
            }
            else
            {
                //  SetNoRecords();
                PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {      
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BindGrid(0);
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
   
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        int StockItemID = int.Parse(Request.QueryString["StockItemID"].ToString());
        string StockLocationId = Request.QueryString["CompanyLocationId"].ToString();
        Response.Redirect("~/admin/adminfiles/stock/Stockdetail.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + StockLocationId);
    }
}