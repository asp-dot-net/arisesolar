using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_wholesalededuct : System.Web.UI.Page
{
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static DataView dv5;
    static DataView dv3;
    static DataView dv2;
    static int countPanelNo;
    static int countSerialNo;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //ddlSelectRecords4.DataSource = SiteConfiguration.GetArray().Split(',');
            //ddlSelectRecords4.DataBind();

            DataTable dt = ClstblContacts.tblCustType_SelectWholesaleVendor();
            ddlSearchVendor.DataSource = dt;
            ddlSearchVendor.DataTextField = "Customer";
            ddlSearchVendor.DataValueField = "CustomerID";
            ddlSearchVendor.DataBind();

            ddlvendor3.DataSource = dt;
            ddlvendor3.DataTextField = "Customer";
            ddlvendor3.DataValueField = "CustomerID";
            ddlvendor3.DataBind();

            ddlvendor4.DataSource = dt;
            ddlvendor4.DataTextField = "Customer";
            ddlvendor4.DataValueField = "CustomerID";
            ddlvendor4.DataBind();

            BindGrid5(0);

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller") || (Roles.IsUserInRole("PostInstaller"))))
            {
                GridView5.Columns[GridView5.Columns.Count - 1].Visible = true;
                //GridView2.Columns[GridView2.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView5.Columns[GridView5.Columns.Count - 1].Visible = false;
                //GridView2.Columns[GridView2.Columns.Count - 1].Visible = false;
            }

            BindDropDown();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                //tdExport.Visible = true;
            }
            else
            {
                // tdExport.Visible = false;
            }
        }
    }

    public void BindDropDown()
    {

        ddllocationsearch3.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch3.DataTextField = "location";
        ddllocationsearch3.DataValueField = "CompanyLocationID";
        ddllocationsearch3.DataMember = "CompanyLocationID";
        ddllocationsearch3.DataBind();

        ddlloc.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlloc.DataTextField = "location";
        ddlloc.DataValueField = "CompanyLocationID";
        ddlloc.DataMember = "CompanyLocationID";
        ddlloc.DataBind();

        ddllocationsearch4.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch4.DataTextField = "location";
        ddllocationsearch4.DataValueField = "CompanyLocationID";
        ddllocationsearch4.DataMember = "CompanyLocationID";
        ddllocationsearch4.DataBind();

        //ddlSearchState2.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        //ddlSearchState2.DataMember = "State";
        //ddlSearchState2.DataTextField = "State";
        //ddlSearchState2.DataValueField = "State";
        //ddlSearchState2.DataBind();

        //ddllocationsearch2.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddllocationsearch2.DataTextField = "location";
        //ddllocationsearch2.DataValueField = "CompanyLocationID";
        //ddllocationsearch2.DataMember = "CompanyLocationID";
        //ddllocationsearch2.DataBind();

    }

    //protected DataTable GetGridData2()
    //{
    //    DataTable dt2 = ClstblProjects.tblProjects_SelectWarehouse("1", txtSearch2.Text, txtProjectNumber2.Text, txtSerachCity2.Text, ddlSearchState2.SelectedValue, txtStartDate2.Text, txtEndDate2.Text, ddlSearchDate2.SelectedValue, ddllocationsearch2.SelectedValue);
    //    return dt2;
    //}
    protected DataTable GetGridData3()
    {

        //Response.Write("1" + "==" + txtSearch3.Text + "==" + txtProjectNumber3.Text + "==" + txtSerachCity3.Text + "==" + ddlSearchState3.SelectedValue + "==" + txtStartDate3.Text + "==" + txtEndDate3.Text + "==" + ddlSearchDate3.SelectedValue + "==" + ddllocationsearch3.SelectedValue + "==" + selectedItem);
        //Response.End();
        //DataTable dt3 = ClstblProjects.tblProjects_SelectWarehouseAllocated("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, selectedItem);
        DataTable dt3 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectWarehouseAllocated("1", ddlvendor3.SelectedValue, txtStartDate3.Text.Trim(), txtEndDate3.Text.Trim(), ddlSearchDate3.SelectedValue, txtinvoiceno3.Text.Trim(), ddllocationsearch3.SelectedValue);
        int iTotalRecords = dt3.Rows.Count;
        int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt3;
    }
    protected DataTable GetGridData2()
    {
        DataTable dt2 = new DataTable();
        //dt4 = ClstblProjects.tblProjects_StockRevert("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, ddlRevert.SelectedValue);
        dt2 = Clstbl_WholesaleOrders.tblWholesaleOrderItems_StockRevert("1", ddlvendor4.SelectedValue, txtStartDate4.Text.Trim(), txtEndDate4.Text.Trim(), ddlSearchDate4.SelectedValue, txtinvoiceno4.Text.Trim(), ddllocationsearch4.SelectedValue, ddlRevert.SelectedValue);

        int iTotalRecords = dt2.Rows.Count;
        int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt2;
    }
    protected DataTable GetGridData5()
    {
        DataTable dt1 = Clstbl_WholesaleOrders.tbl_WholesaleDeduct_SelectBySearch(ddlSearchVendor.SelectedValue, txtStartDate2.Text.Trim(), txtEndDate2.Text.Trim(), ddldate2.SelectedValue, txtinvoiceno.Text.Trim(), ddlloc.SelectedValue, "", histchkbox.Checked.ToString());
        //Response.Write(dt1.Rows[0]["WholesaleOrderId"].ToString());
        //Response.End();
        int iTotalRecords = dt1.Rows.Count;
        int iEndRecord = GridView5.PageSize * (GridView5.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView5.PageSize;

        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt1;
    }





    public void BindGrid5(int deleteFlag)
    {
        DataTable dt5 = new DataTable();
        dt5 = GetGridData5();
        dv5 = new DataView(dt5);
        if (dt5.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord4.Visible = true;
            PanGrid5.Visible = false;
            // divnopage4.Visible = false;
        }
        else
        {
            PanGrid5.Visible = true;
            GridView5.DataSource = dt5;
            GridView5.DataBind();
            PanNoRecord5.Visible = false;

            if (ddlSelectRecords.SelectedValue != string.Empty)
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt5.Rows.Count)
                {
                    //========label Hide
                    divnopage5.Visible = false;
                }
                else
                {
                    if (GridView5.Rows.Count >= 10)
                    {
                        divnopage5.Visible = false;
                    }
                    else
                    {
                        divnopage5.Visible = true;
                    }
                    int iTotalRecords = dv5.ToTable().Rows.Count;
                    int iEndRecord = GridView5.PageSize * (GridView5.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView5.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
        }


        DataTable dt3 = new DataTable();
        dt3 = GetGridData3();
        dv3 = new DataView(dt3);
        if (dt3.Rows.Count == 0)
        {

            // SetNoRecords();
            //Div15.Visible = true;

            //PanGrid3.Visible = false;
            Div6.Visible = false;
            //divnopage3.Visible = false;
        }
        else
        {
            Div6.Visible = true;
            //PanGrid3.Visible = true;
            GridView3.DataSource = dt3;
            GridView3.DataBind();
            Div15.Visible = false;

            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt3.Rows.Count)
            {
                //========label Hide
                divnopage3.Visible = false;
            }
            else
            {

                if (GridView3.Rows.Count >= 10)
                {
                    divnopage3.Visible = false;
                }
                else
                {
                    divnopage3.Visible = true;
                }
                int iTotalRecords = dv3.ToTable().Rows.Count;
                int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
        }

        DataTable dt2 = new DataTable();
        dt2 = GetGridData2();
        dv2 = new DataView(dt2);

        if (dt2.Rows.Count == 0)
        {

            //SetNoRecords();
            //here setnorecords is removed as all three tabs are included 
            //Div15.Visible = true;
            Div10.Visible = false;
            //PanGrid4.Visible = false;
            //divnopage3.Visible = false;
        }
        else
        {
            Div10.Visible = true;
            // PanGrid4.Visible = true;
            GridView2.DataSource = dt2;
            GridView2.DataBind();
            Div15.Visible = false;

            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt2.Rows.Count)
            {
                //========label Hide
                divnopage2.Visible = false;
            }
            else
            {

                //  divnopage3.Visible = true;
                if (GridView2.Rows.Count >= 10)

                {
                    divnopage2.Visible = false;
                }
                else
                {

                    divnopage2.Visible = true;
                }
                int iTotalRecords = dv2.ToTable().Rows.Count;
                int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
        }

    }


    protected void GridView5_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView5.PageIndex = e.NewPageIndex;
        BindGrid5(0);
    }


    protected void GridView5_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "deduct")
        {

            string WholesaleOrderID = e.CommandArgument.ToString();
            hdnWholesaleOrderID2.Value = e.CommandArgument.ToString();
            //Response.Write(ProjectID);
            //Response.End();
            Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);

            Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);


            //Response.Write(st.IsDeduct);
            //Response.End();
            if (st.IsDeduct == "0")
            {
                //DataTable dtHistory = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(ProjectID);
                //if (dtHistory.Rows.Count > 0)
                //{

                ModalPopupExtenderDeduct.Show();
                //Response.Write(ModalPopupExtenderDeduct);
                //Response.End();
                lblCustomer.Text = st.vendor;
                lblSystemDetails.Text = st1.WholesaleOrderItemlist;

                //Response.Write(st1.WholesaleOrderItemlist);
                //Response.End();
                //lblProject.Text = st.Project;
                //lblPanelDetails.Text = st.;
                // lblInverterDetails.Text = st.InverterDetails;
                lblInstallCity.Text = st.CompanyLocation;
                lblInstallState.Text = st1.WholesaleLocation;
                // lblRoofType.Text = st.RoofType;

                txtpallet.Text = string.Empty;
                //txtpallet.Value = string.Empty;
                SerialNoList.Visible = false;
                divActive.Visible = false;
                litremove.Visible = false;

                //txtProjectNo.Text = string.Empty;
                txtInvoiceNo2.Text = st.InvoiceNo;
                SerialNoList2.Visible = false;
                litremove2.Visible = false;
                //savebtndiv.Enabled = false;
                ibtnAdd.Disabled = true;

                ListItem item6 = new ListItem();
                item6.Text = "Select";
                item6.Value = "";
                ddlStore.Items.Clear();
                ddlStore.Items.Add(item6);

                DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
                ddlStore.DataSource = dt;
                ddlStore.DataValueField = "CompanyLocationID";
                ddlStore.DataMember = "CompanyLocation";
                ddlStore.DataTextField = "CompanyLocation";
                ddlStore.DataBind();

                if (st.CompanyLocationID != string.Empty)
                {
                    ddlStore.SelectedValue = st.CompanyLocationID;
                }

                // DataTable r = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Selectby_IdandLocation(WholesaleOrderID, ddlStore.SelectedValue, st1.StockItemID);
                //here the main procedure has '&' instead of 'and'
                DataTable r = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWSOrderId_CompLocID(WholesaleOrderID, ddlStore.SelectedValue);

                //Response.Write(WholesaleOrderID + "++" + ddlStore.SelectedValue + "++" + st1.StockItemID);
                //Response.End();
                //Response.Write(r.Rows.Count);
                //Response.End();
                Repeater2.DataSource = r;
                Repeater2.DataBind();

                if ((Roles.IsUserInRole("Administrator")))
                {
                    foreach (RepeaterItem item in Repeater2.Items)
                    {
                        TextBox rtxtQty = (TextBox)item.FindControl("rtxtQty");
                        rtxtQty.ReadOnly = false;
                    }
                }
                hndWholesaleID.Value = e.CommandArgument.ToString();
                Operationmode = "Add";
                MaxAttribute = 1;
                bindrepeater();

                Validation();

                BindAddedAttribute();
                //}
            }
            else
            {
                hndWholesaleID.Value = e.CommandArgument.ToString();
                ModalPopupExtenderDR.Show();

                lblCustomer2.Text = st.vendor;
                lblSystemDetails2.Text = st1.WholesaleOrderItemlist;
                lblInstallCity2.Text = st.CompanyLocation;
                lblInstallState2.Text = st1.WholesaleLocation;

                DataTable dt = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_SelectStock(WholesaleOrderID, st.InvoiceNo);
                if (dt.Rows.Count > 0)
                {
                    rptDeductRev.DataSource = dt;
                    rptDeductRev.DataBind();
                }
            }
        }

        if (e.CommandName == "move")
        {
            //    string WholesaleOrderID = e.CommandArgument.ToString();
            //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(WholesaleOrderID);
            //    ModalPopupExtenderMove.Show();

            //    ListItem itemP = new ListItem();
            //    itemP.Text = "Select";
            //    itemP.Value = "";
            //    ddlProjectMove.Items.Clear();
            //    ddlProjectMove.Items.Add(itemP);

            //    DataTable dt = ClstblProjects.tblProjects_SelectMove(stPro.PanelBrandID, stPro.InverterDetailsID);
            //    if (dt.Rows.Count > 0)
            //    {
            //        //    ddlProjectMove.Items.Add(itemP);
            //        ddlProjectMove.DataSource = dt;
            //        ddlProjectMove.DataValueField = "ProjectID";
            //        ddlProjectMove.DataMember = "Project";
            //        ddlProjectMove.DataTextField = "Project";
            //        ddlProjectMove.DataBind();
            //    }

            //    ListItem itemI = new ListItem();
            //    itemI.Text = "Select";
            //    itemI.Value = "";
            //    ddlInstallerMove.Items.Clear();
            //    ddlInstallerMove.Items.Add(itemI);

            //    ddlInstallerMove.DataSource = ClstblContacts.tblContacts_SelectInverter();
            //    ddlInstallerMove.DataValueField = "ContactID";
            //    ddlInstallerMove.DataMember = "Contact";
            //    ddlInstallerMove.DataTextField = "Contact";
            //    ddlInstallerMove.DataBind();

            //    try
            //    {
            //        ddlInstallerMove.SelectedValue = stPro.Installer;
            //    }
            //    catch { }
            //    try
            //    {
            //        txtInstallerDateMove.Text = Convert.ToDateTime(stPro.InstallBookingDate).ToShortDateString();
            //    }
            //    catch { }
            //    hndWholesaleID.Value = WholesaleOrderID;
        }

        BindGrid5(0);
    }

    protected void btnSearch_Click5(object sender, EventArgs e)
    {

        //Response.Write(ddlloc.SelectedValue);
        //Response.End();
        BindGrid5(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView5.AllowPaging = false;
            BindGrid5(0);
        }
        else
        {
            GridView5.AllowPaging = true;
            GridView5.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid5(0);
        }
    }


    /* -------------------------
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        BindGrid(0);
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string ProjectID = GridView2.DataKeys[e.RowIndex].Value.ToString();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(ProjectID);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string StockItemID = dt.Rows[i]["StockItemID"].ToString();
            string StockSize = dt.Rows[i]["Stock"].ToString();
            string StockLocation = dt.Rows[i]["CompanyLocationID"].ToString();

            decimal size = Math.Round(Convert.ToDecimal(StockSize));
            int myStock = (Convert.ToInt32(size)) * (-1);

            SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockLocation);
            ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockLocation, stOldQty.StockQuantity, Convert.ToString(myStock), userid, ProjectID);
            bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Convert.ToString(myStock), StockLocation);
        }
        ClstblProjects.tblProjects_UpdateRevert(ProjectID, stEmp.EmployeeID, "0");
        ClstblExtraStock.tblExtraStock_DeleteByProjectID(ProjectID);

        GridView2.EditIndex = -1;
        BindGrid(0);
    }
    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {
            string[] strids = e.CommandArgument.ToString().Split('|');
            string projectid = strids[0];
            string companyid = strids[1];
            Profile.eurosolar.companyid = companyid;
            Profile.eurosolar.projectid = projectid;
            Profile.eurosolar.contactid = "";
            if (e.CommandName == "viewproject")
            {
                Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
            }
        }
    }
    protected void btnSearch2_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void btnClearAll2_Click(object sender, ImageClickEventArgs e)
    {
        ddllocationsearch2.SelectedValue = "";
        ddlSearchDate2.SelectedValue = "";
        ddlSearchState2.SelectedValue = "";
        txtSerachCity2.Text = string.Empty;
        txtSearch2.Text = string.Empty;
        txtProjectNumber2.Text = string.Empty;
        txtStartDate2.Text = string.Empty;
        txtEndDate2.Text = string.Empty;

        BindGrid(0);
    }
    ----------------------*/

    protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView3.PageIndex = e.NewPageIndex;
        BindGrid5(0);
    }
    protected void btnSearch3_Click(object sender, EventArgs e)
    {
        BindGrid5(0);
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }
    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockQuantity", Type.GetType("System.String"));
        rpttable.Columns.Add("ExtraStock", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptDeduct.Items)
        {
            HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("rddlStockItem");
            Label lblQty = (Label)item.FindControl("rlblQty");
            TextBox txtExtraStock = (TextBox)item.FindControl("rtxtExtraStock");

            DataRow dr = rpttable.NewRow();
            dr["StockItemID"] = hndStockItemID.Value;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["StockQuantity"] = lblQty.Text;
            dr["ExtraStock"] = txtExtraStock.Text;

            rpttable.Rows.Add(dr);
        }
    }
    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockItemID", Type.GetType("System.Int32"));
            rpttable.Columns.Add("StockQuantity", Type.GetType("System.Int32"));
            rpttable.Columns.Add("ExtraStock", Type.GetType("System.Int32"));
        }
        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();
            dr["StockItemID"] = i;
            dr["StockQuantity"] = DBNull.Value;
            dr["ExtraStock"] = DBNull.Value;
            try
            {
                rpttable.Rows.Add(dr);
            }
            catch { }
        }
        rptDeduct.DataSource = rpttable;
        rptDeduct.DataBind();
    }
    protected void rptDeduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (hndWholesaleID.Value != string.Empty)
            {
                SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(hndWholesaleID.Value);
                DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("rddlStockItem");
                HiddenField hndStockItemID = (HiddenField)e.Item.FindControl("hndStockItemID");

                ListItem item6 = new ListItem();
                item6.Text = "Select";
                item6.Value = "";
                ddlStockItem.Items.Clear();
                ddlStockItem.Items.Add(item6);

                if (st.RoofTypeID != string.Empty)
                {
                    ddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElectMount(ddlStore.SelectedValue);
                    ddlStockItem.DataTextField = "StockItem";
                    ddlStockItem.DataMember = "StockItem";
                    ddlStockItem.DataValueField = "StockItemID";
                    ddlStockItem.DataBind();
                }
                else
                {
                    ddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElect(ddlStore.SelectedValue);
                    ddlStockItem.DataTextField = "StockItem";
                    ddlStockItem.DataMember = "StockItem";
                    ddlStockItem.DataValueField = "StockItemID";
                    ddlStockItem.DataBind();
                }
                ddlStockItem.SelectedValue = hndStockItemID.Value;
            }
        }
    }
    protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderDeduct.Show();
        string WholesaleOrderID = hndWholesaleID.Value;
        ///string WholesaleOrderItemId = hndWholesaleItemID.Value;
        //Response.Write(ProjectID);
        //Response.End();         

        //string ProjectID = hndProjectID.Value;
        if (hndWholesaleID.Value != string.Empty)
        {
            Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
            Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);
            // DataTable dt1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Selectby_IdandLocation(WholesaleOrderID, ddlStore.SelectedValue, st1.StockItemID);
            DataTable dt1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWSOrderId_CompLocID(WholesaleOrderID, ddlStore.SelectedValue);
            if (ddlStore.SelectedValue != string.Empty)
            {
                if (st1.WholesaleOrderItemlist != string.Empty)

                    if (dt1.Rows.Count > 0)
                    {
                        Repeater2.DataSource = dt1;
                        Repeater2.DataBind();
                        //lblStock.Text = dt1.Rows[0]["IteminStock"].ToString();
                    }
            }

            foreach (RepeaterItem item in rptDeduct.Items)
            {
                DropDownList rddlStockItem = (DropDownList)item.FindControl("rddlStockItem");
                Label rlblQty = (Label)item.FindControl("rlblQty");

                ListItem item6 = new ListItem();
                item6.Text = "Select";
                item6.Value = "";
                rddlStockItem.Items.Clear();
                rddlStockItem.Items.Add(item6);
                //st.Roofid
                if (st.StockItemID != string.Empty)
                {
                    rddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElectMount(ddlStore.SelectedValue);
                    rddlStockItem.DataTextField = "StockItem";
                    rddlStockItem.DataMember = "StockItem";
                    rddlStockItem.DataValueField = "StockItemID";
                    rddlStockItem.DataBind();
                }
                else
                {
                    rddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElect(ddlStore.SelectedValue);
                    rddlStockItem.DataTextField = "StockItem";
                    rddlStockItem.DataMember = "StockItem";
                    rddlStockItem.DataValueField = "StockItemID";
                    rddlStockItem.DataBind();
                }

                DataTable dt = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, rddlStockItem.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    rlblQty.Text = dt.Rows[0]["StockQuantity"].ToString();
                }
                else
                {
                    rlblQty.Text = "0";
                }
            }
        }

        int TotalPanelCount = 0;


        if (!string.IsNullOrEmpty(txtpallet.Text) && ddlStore.SelectedValue != "")
        {
            DataTable dt = new DataTable();
            String PalletNo = txtpallet.Text;
            string storelocation = ddlStore.SelectedItem.Value;
            for (int item = 0; item < Repeater2.Items.Count; item++)
            {
                HiddenField hdnStockCategoryID = Repeater2.Items[item].FindControl("hdnStockCategoryID") as HiddenField;
                TextBox rtxtQty = Repeater2.Items[item].FindControl("rtxtQty") as TextBox;
                HiddenField hdnStockItemID = Repeater2.Items[item].FindControl("hdnStockItemID") as HiddenField;

                if (hdnStockCategoryID.Value == "1")//Sorting panels from order
                {
                    if (!string.IsNullOrEmpty(rtxtQty.Text))
                    {
                        TotalPanelCount = TotalPanelCount + Convert.ToInt32(rtxtQty.Text);
                    }
                    string StockitemID = hdnStockItemID.Value;
                    string[] Pallets = PalletNo.Split(',');
                    for (int j = 0; j < Pallets.Length; j++)
                    {
                        Pallets[j] = Pallets[j].Trim();
                        DataTable dt3 = ClstblStockOrders.tblStockSerialNo_SearchByPallet(StockitemID, storelocation, Pallets[j]);
                        dt.Merge(dt3);
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                Repeater3.DataSource = dt;
                Repeater3.DataBind();
                countPanelNo = dt.Rows.Count;
                if (dt.Rows.Count == TotalPanelCount)
                {
                    // savebtndiv.Enabled = true;
                    ibtnAdd.Disabled = false;
                }
                else
                {
                    // savebtndiv.Enabled = false;
                    ibtnAdd.Disabled = true;
                }
                SerialNoList.Visible = true;
                divActive.Visible = false;
                divActive.Visible = false;
                litremove.Visible = true;
            }
            else
            {
                SerialNoList.Visible = false;
                divActive.Visible = true;
                divActive.Visible = true;
                litremove.Visible = false;
                // savebtndiv.Enabled = false;
                ibtnAdd.Disabled = true;
            }

        }
        else
        {

            SerialNoList.Visible = false;
            divActive.Visible = false;
            litremove.Visible = false;
            // savebtndiv.Enabled = false;
            ibtnAdd.Disabled = true;
        }

        if ((Roles.IsUserInRole("Administrator")))
        {
            foreach (RepeaterItem item in Repeater2.Items)
            {
                TextBox rtxtQty = (TextBox)item.FindControl("rtxtQty");
                rtxtQty.ReadOnly = false;
            }
        }
    }
    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderDeduct.Show();
        AddmoreAttribute();
    }
    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderDeduct.Show();
        foreach (RepeaterItem item in rptDeduct.Items)
        {
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            Label lblQty = (Label)item.FindControl("lblQty");

            if (ddlStockItem.SelectedValue != string.Empty)
            {
                DataTable dt = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, ddlStockItem.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    lblQty.Text = dt.Rows[0]["StockQuantity"].ToString();
                }
            }
        }
    }

    protected void ibtnAdd_Click(object sender, EventArgs e)
    {


        // bool sucStock = false;
        bool sucStock1 = false;
        string WholesaleOrderID = hndWholesaleID.Value;
        ddlStore.Enabled = true;
        string StockAllocationStore1 = ddlStore.SelectedValue;
        string userid1 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp1 = ClstblEmployees.tblEmployees_SelectByUserId(userid1);
        // string ProjectID = "";       
        Sttbl_WholesaleOrders stwhole2 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(hdnWholesaleOrderID2.Value);

        foreach (RepeaterItem item in Repeater2.Items)
        {
            HiddenField hdnWholesaleOrderItemID = (HiddenField)item.FindControl("hdnWholesaleOrderItemID");
            Label rlblStockItem = (Label)item.FindControl("rlblStockItem");
            Label rlblStock = (Label)item.FindControl("rlblStock");
            TextBox rtxtQty = (TextBox)item.FindControl("rtxtQty");
            TextBox rtxt_ExtraStock = (TextBox)item.FindControl("rtxt_ExtraStock");

            //Response.Write(hdnWholesaleOrderItemID.Value);
            //Response.End();
            Sttbl_WholesaleOrderItems stwhole = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderItemID(hdnWholesaleOrderItemID.Value);

            string id = stwhole.StockItemID;
            //Response.Write(id);
            //Response.End();

            sucStock1 = ClsProjectSale.tblStockItems_UpdateStock(stwhole.StockItemID, stwhole.OrderQuantity, StockAllocationStore1);
            //deduct stock in tblStockItems by declaring variable @stock and then set StockQuantity = (@Stock - @StockSize)
            SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(stwhole.StockItemID, StockAllocationStore1);

            Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", stwhole.StockItemID, StockAllocationStore1, StOldQty.StockQuantity, (0 - Convert.ToInt32(rtxtQty.Text)).ToString(), userid1, WholesaleOrderID, hdnWholesaleOrderItemID.Value, stwhole2.InvoiceNo);
            //history to get what and who deducted the stock,and (0-Quantity) is to get what amount is deducted. 
            //Response.Write("5" + "--" + stwhole.StockItemID + "--" + StockAllocationStore1 + "--" + StOldQty.StockQuantity + "--" + (0 - Convert.ToInt32(rtxtQty.Text)) + "--" + userid1 + "--" + WholesaleOrderID);
            //Response.End();
            string ExtraStock = "0";
            if (rtxtQty.Text != string.Empty)
            {
                //Response.Write(rtxtQty.Text+"--"+ stwhole.OrderQuantity);
                //Response.End();
                ExtraStock = (Convert.ToInt32(rtxtQty.Text) - Convert.ToInt32(stwhole.OrderQuantity)).ToString();
            }
            if (Convert.ToInt32(ExtraStock) > 0)
            {
                //Response.Write(hdnWholesaleOrderItemID.Value+"--"+ stwhole.WholesaleOrderID + "--" + stwhole2.InvoiceNo + "--" + stwhole.StockItemID + "--" + ExtraStock + "--" + StockAllocationStore1);
                //Response.End();
                ClstblExtraWholesaleStock.tblExtraWholesaleStock_Insert(hdnWholesaleOrderItemID.Value, stwhole.WholesaleOrderID, stwhole2.InvoiceNo, stwhole.StockItemID, ExtraStock, StockAllocationStore1);

            }
            if (rtxt_ExtraStock.Text != string.Empty)
            {
                DataTable dtEXStock = ClstblExtraWholesaleStock.tblExtraWholesaleStock_SelectByInstallerID(stwhole2.InvoiceNo, stwhole.StockItemID, ddlStore.SelectedValue);
                int Stock = (Convert.ToInt32(dtEXStock.Rows[0]["ExtraStock"].ToString()) - Convert.ToInt32(rtxt_ExtraStock.Text));
                ClstblExtraWholesaleStock.tblExtraWholesaleStock_Update(hdnWholesaleOrderItemID.Value, stwhole2.InvoiceNo, stwhole.StockItemID, Convert.ToString(Stock), ddlStore.SelectedValue);
                //Response.Write("swwww");
                //Response.End();
            }

            foreach (RepeaterItem item2 in rptDeduct.Items)
            {
                TextBox rtxtExtraStock = (TextBox)item2.FindControl("rtxtExtraStock");
                DropDownList rddlStockItem = (DropDownList)item2.FindControl("rddlStockItem");

                if (rddlStockItem.SelectedValue != string.Empty)
                {
                    SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(rddlStockItem.SelectedValue, StockAllocationStore1);
                    Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", rddlStockItem.SelectedValue, StockAllocationStore1, stOldQty.StockQuantity, (0 - Convert.ToInt32(rtxtExtraStock.Text)).ToString(), userid1, stwhole.WholesaleOrderID, hdnWholesaleOrderItemID.Value, stwhole2.InvoiceNo);
                    string Stock = rtxtExtraStock.Text;
                    string StockItemID = rddlStockItem.SelectedValue;
                    bool suc = ClsProjectSale.tblStockItems_UpdateStock(StockItemID, Stock, StockAllocationStore1);
                }
            }

        }


        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        String CurrentDateTime = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        if (SerialNoList.Visible == true)
        {
            foreach (RepeaterItem ri in Repeater3.Items)
            {
                Label lblpallet = (Label)ri.FindControl("lblPallet");
                Label lblSerialNo = (Label)ri.FindControl("lblSerialNo");
                if (ri.Visible == true)
                {
                    Clstbl_WholesaleOrders.tblWholesaleStockDeductSerialNo_Insert("", "", lblSerialNo.Text, lblpallet.Text, stwhole2.InvoiceNo, hdnWholesaleOrderID2.Value, stEmp.EmployeeID, CurrentDateTime, "");
                }
            }
        }
        if (SerialNoList2.Visible == true)
        {
            foreach (RepeaterItem ri in Repeater4.Items)
            {
                Label lblSerialNo2 = (Label)ri.FindControl("lblSerialNo2");
                if (ri.Visible == true)
                {
                    Clstbl_WholesaleOrders.tblWholesaleStockDeductSerialNo_Insert("", "", lblSerialNo2.Text, "", stwhole2.InvoiceNo, hdnWholesaleOrderID2.Value, stEmp.EmployeeID, CurrentDateTime, "");
                }
            }
        }


        //this foreach is used to to deduct extra stock and quantity added manually below the table
        /* ---------------------- Stock Assigned ---------------------- */
        //if (st.ProjectStatusID == "11")
        //{
        //    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("12", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "12");
        //}
        //project status updated from job booked to stock assigned
        /* ------------------------------------------------------------ */

        Clstbl_WholesaleOrders.tblWholesaleOrders_UpdateDeduct(WholesaleOrderID, stEmp1.EmployeeID, "1", StockAllocationStore1, DateTime.Now.AddHours(14).ToString());
        //update is_deduct flag,deduct date and deducted_by in tblWholesaleOrders
        //stockallocationlocation is updated in tblProjects
        //deduct date is updated in tblProjects



        ModalPopupExtenderDeduct.Hide();
        BindGrid5(0);

    }

    protected void btnDeduct_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = hndWholesaleID.Value;
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);

        string StockAllocationStore = st.CompanyLocationID;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        foreach (RepeaterItem item in rptDeductRev.Items)
        {
            TextBox txtDeductRevert = (TextBox)item.FindControl("txtDeductRevert");
            HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");

            string Stock = txtDeductRevert.Text.Trim();
            string StockItemID = hndStockItemID.Value;

            if (txtDeductRevert.Text.Trim() != string.Empty)
            {
                SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(hndStockItemID.Value, StockAllocationStore);
                Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, (0 - Convert.ToInt32(txtDeductRevert.Text)).ToString(), userid, WholesaleOrderID, st1.WholesaleOrderItemID, st.InvoiceNo);
                bool suc = ClsProjectSale.tblStockItems_UpdateStock(StockItemID, Stock, StockAllocationStore);
                //It would simply deduct stock qty again and the record of deduction is stored in tblWholesaleStockItemInventoryHistory and after that Qty entered is deduct from tblStockItems 
            }
        }
        Clstbl_WholesaleOrders.tblWholesaleOrders_UpdateDeductDate(WholesaleOrderID, DateTime.Now.AddHours(14).ToString());
        //Nextly,deductdate is updated in tblWholesaleOrders for the particular selected WholesaleOrderID.
    }

    protected void btnRevert_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = hndWholesaleID.Value;
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);
        string StockAllocationStore = st.CompanyLocationID;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        foreach (RepeaterItem item in rptDeductRev.Items)
        {
            TextBox txtDeductRevert = (TextBox)item.FindControl("txtDeductRevert");
            HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");

            string Stock = txtDeductRevert.Text.Trim();
            string StockItemID = hndStockItemID.Value;

            if (txtDeductRevert.Text.Trim() != string.Empty)
            {
                SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockAllocationStore);
                int suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, Stock, userid, WholesaleOrderID, st1.WholesaleOrderItemID, st.InvoiceNo);
                Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Stock, StockAllocationStore);
                //It would simply deduct stock qty again and the record of deduction is stored in tblWholesaleStockItemInventoryHistory and for that tblWholesaleStockItemInventoryHistory id revert flag and revert date is updated
                //Nextly,in tblStockItems the stock is moved back for that system id and location.
                //Here first tblWholesaleStockItemInventoryHistory_Insert called to created separate record and that particular record's revert data is updated +stock qty is +ve which states that it was revert operation.
                //Here,both deduct and revert record is stored separatly.
            }
        }
        // ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID,"");
        //  ClstblProjects.tblProjects_UpdateStockDeductBy(ProjectID, "");
        ModalPopupExtender3.Hide();
        BindGrid5(0);
    }
    protected void btnRevertAll_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = hndWholesaleID.Value;
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        DataTable dt = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_SelectStock(WholesaleOrderID, st.InvoiceNo);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string StockItemID = dt.Rows[i]["StockItemID"].ToString();
            string StockSize = dt.Rows[i]["Stock"].ToString();
            string StockLocation = st.CompanyLocationID;

            decimal size = Math.Round(Convert.ToDecimal(StockSize));
            int myStock = (Convert.ToInt32(size)) * (-1);
            //Here,-1 is multipled as value which is fetched from from tblWholesaleStockItemInventoryHistory is negative
            //We need to make the value positive because we add the qty back in tblStockItems

            SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockLocation);
            int suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", StockItemID, StockLocation, stOldQty.StockQuantity, Convert.ToString(myStock), userid, WholesaleOrderID, st1.WholesaleOrderItemID, st.InvoiceNo);
            //Response.Write(suc1);
            //Response.End();
            Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "2", DateTime.Now.AddHours(14).ToString());
            bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Convert.ToString(myStock), StockLocation);
        }
        Clstbl_WholesaleOrders.tblWholesaleOrders_UpdateRevert(WholesaleOrderID, "", "0");
        //Clstbl_WholesaleOrders.tblWholesaleOrders_UpdateRevert(WholesaleOrderID, stEmp.EmployeeID, "0");
        //Deduct flag is changed to zero and deductdate is changed to null for WholesaleOrderID
        ClstblExtraWholesaleStock.tblExtraWholesaleStock_DeleteByWholeOrderID(WholesaleOrderID);
        //delete record by WholesaleOrderID in tblExtraWholesaleStock

        /* -------------------- Update Project Status -------------------- */
        //if (st.InstallBookingDate != string.Empty)
        //{
        //    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
        //    //Project status 11 is for status 'job booked'.
        //}
        //else
        //{
        //    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //Project status 3 is for status 'Active'.
        // }
        /* -------------------------------------------------------------- */
        //we change the status since the status was changed to 'Stock Assigned' after deduction,but after reverting all it has to be 
        //changed back to 'Job Booked'(When install booking date is assigned) and 'Active'

        ModalPopupExtender3.Hide();
        BindGrid5(0);
    }
    protected void btnMove_Click(object sender, EventArgs e)
    {
        //string WholesaleOrderID = hndWholesaleID.Value;
        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        //if (ddlProjectMove.SelectedValue != string.Empty)
        //{
        //    ClstblStockItems.tblStockItemInventoryHistory_UpdateMove(ProjectID, ddlProjectMove.SelectedValue, userid);
        //    ClstblProjects.tblProjects_UpdateDeduct(ddlProjectMove.SelectedValue, stEmp.EmployeeID, "1");
        //    ClstblProjects.tblProjects_UpdateDeduct(ProjectID, stEmp.EmployeeID, "0");
        //    ClstblProjects.tblProjects_UpdateMove(ddlProjectMove.SelectedValue, ddlInstallerMove.SelectedValue, txtInstallerDateMove.Text.Trim());
        //    ClstblProjects.tblProjects_UpdateMove(ProjectID, "", "");
        //}
        //BindGrid5(0);
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView3.DataSource = sortedView;
        GridView3.DataBind();
    }

    protected void GridView5_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView5.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView5.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView5.PageIndex - 2;
        page[1] = GridView5.PageIndex - 1;
        page[2] = GridView5.PageIndex;
        page[3] = GridView5.PageIndex + 1;
        page[4] = GridView5.PageIndex + 2;
        page[5] = GridView5.PageIndex + 3;
        page[6] = GridView5.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView5.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView5.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView5.PageIndex == GridView5.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv5.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv5.ToTable().Rows.Count;
            int iEndRecord = GridView5.PageSize * (GridView5.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView5.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView5.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid5(0);

    }
    protected void GridView5_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView3_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView3.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView3.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView3.PageIndex - 2;
        page[1] = GridView3.PageIndex - 1;
        page[2] = GridView3.PageIndex;
        page[3] = GridView3.PageIndex + 1;
        page[4] = GridView3.PageIndex + 2;
        page[5] = GridView3.PageIndex + 3;
        page[6] = GridView3.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView3.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView3.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView3.PageIndex == GridView3.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage3 = (Label)gvrow.Cells[0].FindControl("ltrPage3");
        if (dv3.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv3.ToTable().Rows.Count;
            int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage3.Text = "";
        }
    }
    void lb3_Command(object sender, CommandEventArgs e)
    {
        GridView3.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid5(0);
    }
    protected void GridView3_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb3_Command);
        }
    }

    //protected void ddlSelectRecords4_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (Convert.ToString(ddlSelectRecords4.SelectedValue) == "All")
    //    {
    //        GridView2.AllowPaging = false;
    //        BindGrid5(0);
    //    }


    //     else if(Convert.ToString(ddlSelectRecords4.SelectedValue) == "")
    //    {
    //        GridView2.AllowPaging = false;
    //    }
    //    else{
    //        GridView2.AllowPaging = true;
    //        GridView2.PageSize = Convert.ToInt32(ddlSelectRecords4.SelectedValue);
    //        BindGrid5(0);
    //    }
    //}
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid5(0);
    }
    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt2 = new DataTable();
        dt2 = GetGridData2();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt2);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView2.DataSource = sortedView;
        GridView2.DataBind();
    }
    protected void GridView2_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView2.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView2.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView2.PageIndex - 2;
        page[1] = GridView2.PageIndex - 1;
        page[2] = GridView2.PageIndex;
        page[3] = GridView2.PageIndex + 1;
        page[4] = GridView2.PageIndex + 2;
        page[5] = GridView2.PageIndex + 3;
        page[6] = GridView2.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView2.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView2.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView2.PageIndex == GridView2.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage2 = (Label)gvrow.Cells[0].FindControl("ltrPage2");
        if (dv2.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv2.ToTable().Rows.Count;
            int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage2.Text = "";
        }
    }
    protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command4);
        }
    }
    void lb_Command4(object sender, CommandEventArgs e)
    {
        GridView2.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid5(0);
    }
    protected void btnSearch4_Click(object sender, EventArgs e)
    {
        BindGrid5(0);
    }
    protected void btnClearAll4_Click(object sender, EventArgs e)
    {
        txtinvoiceno4.Text = string.Empty;
        ddllocationsearch4.SelectedValue = "";
        ddlSearchDate4.SelectedValue = "";
        ddlvendor4.SelectedValue = "0";
        txtStartDate4.Text = string.Empty;
        txtEndDate4.Text = string.Empty;
        BindGrid5(0);
    }
    protected void lnkReceived_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnWholesaleOrderID = (HiddenField)item1.FindControl("hdnWholesaleOrderID");

        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(hdnWholesaleOrderID.Value);
        if (dt.Rows.Count > 0)
        {
            hdnstocktransferid.Value = hdnWholesaleOrderID.Value;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
            divdetail.Visible = true;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }

    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        hdnstocktransferid.Value = "";
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid5(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;

        divright.Visible = true;
    }
    //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        Label lblAllocatePanels = (Label)e.Row.FindControl("lblAllocatePanels");
    //        HiddenField hdnProjectID = (HiddenField)e.Row.FindControl("hdnProjectID");
    //        SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
    //        if (stpro.StockDeductDate != string.Empty)
    //        {
    //            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(hdnProjectID.Value);
    //            if (dt.Rows.Count > 0)
    //            {
    //                decimal deduct = 0;
    //                foreach (DataRow dr in dt.Rows)
    //                {
    //                    deduct += Convert.ToDecimal(dr["Deduct"].ToString());
    //                }
    //                lblAllocatePanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0}", deduct);
    //            }
    //            //lblAllocatePanels.Text = stpro.NumberPanels;
    //        }

    //    }
    //}
    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndWholesaleID");
            //Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID.Value);
            //Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID.Value);
            Literal ltdeductpanels = (Literal)e.Row.FindControl("ltdeductpanels");
            Literal ltallocatedpanels = (Literal)e.Row.FindControl("ltallocatedpanels");
            Label ltremainingpanels = (Label)e.Row.FindControl("ltremainingpanels");
            Label lblrevert = (Label)e.Row.FindControl("lblrevert");
            //Button btnRevertStock = (Button)e.Row.FindControl("btnRevertStock");
            HiddenField hdnStockItem = (HiddenField)e.Row.FindControl("hdnStockItem");
            LinkButton lbtnDeduct = (LinkButton)e.Row.FindControl("lbtnDeduct");

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hndProjectID.Value);
            ltallocatedpanels.Text = stpro.NumberPanels;
            DataTable dt = Clstbl_WholesaleOrders.tblWholesaleItemInventoryHistory_SelectStock(hndProjectID.Value);
            if (dt.Rows.Count > 0)
            {
                decimal deduct = 0;
                decimal revert = 0;
                DataTable dt1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_WholesaleOrderID(hndProjectID.Value, hdnStockItem.Value);
                if (dt1.Rows.Count > 0)
                {
                    deduct = Convert.ToDecimal(dt1.Rows[0]["Stock"].ToString());
                }
                //Response.Write(hndProjectID.Value+"=="+ hdnStockItem.Value);
                //Response.End();
                DataTable dt2 = Clstbl_WholesaleOrders.tblWholesaleItemInventoryHistory_revert(hndProjectID.Value, hdnStockItem.Value);
                if (dt2.Rows.Count > 0)
                {
                    revert = Convert.ToDecimal(dt2.Rows[0]["Stock"].ToString());
                }
                // lblrevert.Text = dt1.Rows[0]["stock1"].ToString();///tblStockItemInventoryHistory_revert
                // Response.Write(dt1.Rows[0]["RevertFlag"].ToString());
                foreach (DataRow dr in dt1.Rows)
                {
                    // Response.Write(dr["RevertFlag"].ToString());
                    //if (dr["RevertFlag"].ToString() != true.ToString())
                    //{
                    //    revert = Convert.ToDecimal(dr["stock1"].ToString());
                    //}
                    //if (dt.Rows[0]["StockItemID"].ToString() == hdnStockItem.Value)
                    if (dr["StockItemID"].ToString() == hdnStockItem.Value)
                    {
                        //  deduct += Convert.ToDecimal(dr["Stock"].ToString());
                    }
                }

                //Response.Write(hndProjectID.Value + "=" + hdnStockItem.Value + "=" + deduct);
                deduct = System.Math.Abs(deduct);
                revert = System.Math.Abs(revert);
                //if (hndProjectID.Value == "-331651726")
                //{
                //    Response.Write("-->" + dt.Rows[0]["StockItem"].ToString() + "=" + hdnStockItem.Value + "=" + deduct + "<br/>");
                //}
                //Response.End();

                //Response.End();
                //if (dt.Rows[0]["deduct"].ToString() != string.Empty)
                //{
                //    deduct = Convert.ToDecimal(dt.Rows[0]["deduct"].ToString());
                //}


                ltdeductpanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0}", deduct);
                lblrevert.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0}", revert);
                if (Convert.ToInt32(ltdeductpanels.Text) == 0)
                {
                    lbtnDeduct.Visible = false;
                    ltdeductpanels.Visible = false;
                }
                decimal noofpanels = 0;
                if (stpro.NumberPanels != string.Empty)
                {
                    noofpanels = Convert.ToDecimal(stpro.NumberPanels);
                }
                if (deduct >= 0)
                {
                    //deduct = deduct;
                }
                // decimal remain = (Convert.ToDecimal(ltallocatedpanels.Text) - Convert.ToDecimal(ltdeductpanels.Text));
                decimal remain = noofpanels - deduct + revert;
                ltremainingpanels.Text = remain.ToString();
                if (remain < 0)
                {
                    ltremainingpanels.ForeColor = System.Drawing.Color.Red;
                }

                //if (remain < 0)
                //{
                //    ltremainingpanels.ForeColor = System.Drawing.Color.Red;
                //    btnRevertStock.Visible = true;
                //}
                //else
                //{
                //    if (remain == 0)
                //    {
                //        btnRevertStock.Visible = false;
                //    }
                //    else
                //    {
                //        btnRevertStock.Visible = false;
                //    }
                //}
            }
        }
    }
    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // Response.Write(e.CommandName);
        //Response.End();
        if (e.CommandName == "RevertStock")
        {

            decimal remain = 0;
            int deduct1 = 0;
            string ID = e.CommandArgument.ToString(); //projectID

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(ID);
            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ID);
            if (dt.Rows.Count > 0)
            {

                decimal deduct = 0;
                //foreach (DataRow dr in dt.Rows)
                //{
                //    deduct += Convert.ToDecimal(dr["Deduct"].ToString());
                //}
                if (dt.Rows[0]["deduct"].ToString() != string.Empty)
                {
                    deduct = Convert.ToDecimal(dt.Rows[0]["deduct"].ToString());
                }
                //ltdeductpanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0}", deduct);


                remain = (Convert.ToInt32(deduct) - Convert.ToInt32(stpro.NumberPanels));
                deduct1 = Convert.ToInt32(stpro.NumberPanels) - Convert.ToInt32(remain);

                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString());

                int stocksucc = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString(), StOldQty.StockQuantity, remain.ToString(), userid, dt.Rows[0]["ProjectID"].ToString());
                ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(stocksucc.ToString(), "1", DateTime.Now.AddHours(14).ToString());


                SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString());
                string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) + (Convert.ToInt32(remain))).ToString();


                ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString(), qty1);



            }


            //bool s =ClstblProjects.tblProjects_UpdateNumberPanels(ID, deduct1.ToString());

        }
        if (e.CommandName == "deduct")
        {

            string ProjectID = e.CommandArgument.ToString();
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            hndWholesaleID.Value = e.CommandArgument.ToString();
            ModalPopupExtender3.Show();

            lblCustomer6.Text = st.Customer;
            lblProject6.Text = st.Project;
            lblPanelDetails6.Text = st.PanelDetails;
            lblInverterDetails6.Text = st.InverterDetails;
            lblInstallCity6.Text = st.InstallCity;
            lblInstallState6.Text = st.InstallState;
            lblRoofType6.Text = st.RoofType;

            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
            if (dt.Rows.Count > 0)
            {
                rptDeductRev6.DataSource = dt;
                rptDeductRev6.DataBind();
            }
            BindScript();
        }
        BindGrid5(0);
    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        //DataTable dt = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, chkHistoric.Checked.ToString());
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleDeduct_SelectBySearch(ddlSearchVendor.SelectedValue, txtStartDate2.Text.Trim(), txtEndDate2.Text.Trim(), ddldate2.SelectedValue, txtinvoiceno.Text.Trim(), ddlloc.SelectedValue, "", histchkbox.Checked.ToString());
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockDeduct" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 11, 14, 301, 299, 52, 300 };
            string[] arrHeader = { "ProjectNumber", "Project", "InstallBookingDate", "Installer", "Details", "Store Name" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        // DataTable dt = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, chkHistoric.Checked.ToString());
        string selectedItem = "";
        DataTable dt3 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectWarehouseAllocated("1", ddlvendor3.SelectedValue, txtStartDate3.Text.Trim(), txtEndDate3.Text.Trim(), ddlSearchDate3.SelectedValue, txtinvoiceno3.Text.Trim(), ddllocationsearch3.SelectedValue);
        // DataTable dt3 = ClstblProjects.tblProjects_SelectWarehouseAllocated("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, selectedItem);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockAllocated" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 304, 11, 302, 303, 299, 53, 52, 300 };
            string[] arrHeader = { "DeductDate", "P.No.", "P.Status", "InstallDate", "Installer", "N.Panels", "Details", "Store Name" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt3, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void lbtnExport3_Click(object sender, EventArgs e)
    {
        //DataTable dt5 = ClstblProjects.tblProjects_StockRevert("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, ddlRevert.SelectedValue);
        // Response.Clear();
        //Response.Write(dt5.Rows.Count);
        //Response.End();
        try
        {
            Export oExport = new Export();
            string FileName = "StockRevert" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 2, 3, 6, 5 };
            string[] arrHeader = { "Project No.", "Project", "InstallBookingDate", "Details" };
            //only change file extension to .xls for excel file
            // oExport.ExportDetails(dt5, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lblexport7_Click(object sender, EventArgs e)
    {

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtinvoiceno.Text = string.Empty;
        ddlloc.SelectedValue = "";
        ddlSearchVendor.SelectedValue = "0";
        // ddlSearchVendor.SelectedValue = "";
        // ddldate2.SelectedValue = "";
        txtStartDate2.Text = string.Empty;
        txtEndDate2.Text = string.Empty;
        BindGrid5(0);
    }
    protected void btnClear3_Click1(object sender, EventArgs e)
    {
        txtStartDate3.Text = string.Empty;
        txtEndDate3.Text = string.Empty;
        txtinvoiceno3.Text = string.Empty;
        ddllocationsearch3.SelectedValue = "";
        ddlvendor3.SelectedValue = "0";
        BindGrid5(0);
    }


    protected void GridView5_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData5();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView5.DataSource = sortedView;
        GridView5.DataBind();
    }

    protected void btnSearchPallet_Click(object sender, EventArgs e)
    {
        //Hide Project Number search repeater
        //txtProjectNo.Text = string.Empty;
        DataTable dt = new DataTable();
        SerialNoList2.Visible = false;
        litremove2.Visible = false;
        int TotalPanelCount = 0;


        ModalPopupExtenderDeduct.Show();
        String WholesaleOrderID = hdnWholesaleOrderID2.Value;
        DataTable dt2 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(WholesaleOrderID);
        string storelocation = ddlStore.SelectedItem.Value;
        string PalletNo = txtpallet.Text;

        for (int item = 0; item < Repeater2.Items.Count; item++)
        {
            HiddenField hdnStockCategoryID = Repeater2.Items[item].FindControl("hdnStockCategoryID") as HiddenField;
            TextBox rtxtQty = Repeater2.Items[item].FindControl("rtxtQty") as TextBox;
            HiddenField hdnStockItemID = Repeater2.Items[item].FindControl("hdnStockItemID") as HiddenField;

            if (hdnStockCategoryID.Value == "1")//Sorting panels from order
            {
                if (!string.IsNullOrEmpty(rtxtQty.Text))
                {
                    TotalPanelCount = TotalPanelCount + Convert.ToInt32(rtxtQty.Text);
                }
                string StockitemID = hdnStockItemID.Value;
                string[] Pallets = PalletNo.Split(',');
                for (int j = 0; j < Pallets.Length; j++)
                {
                    Pallets[j] = Pallets[j].Trim();
                    DataTable dt3 = ClstblStockOrders.tblStockSerialNo_SearchByPallet(StockitemID, storelocation, Pallets[j]);
                    dt.Merge(dt3);
                }
            }
        }

        if (dt.Rows.Count > 0)
        {
            countPanelNo = dt.Rows.Count;
            Repeater3.DataSource = dt;
            Repeater3.DataBind();
            SerialNoList.Visible = true;
            divActive.Visible = false;
            litremove.Visible = true;
            if (TotalPanelCount == dt.Rows.Count)
            {
                // savebtndiv.Enabled = true;
                ibtnAdd.Disabled = false;
            }
            else
            {
                //  savebtndiv.Enabled = false;
                ibtnAdd.Disabled = true;
            }
        }
        else
        {
            SerialNoList.Visible = false;
            Repeater3.DataSource = "";
            Repeater3.DataBind();
            divActive.Visible = true;
        }
    }

    protected void litremove_Click(object sender, EventArgs e)
    {

        int TotalPanelCount = 0;

        for (int item = 0; item < Repeater2.Items.Count; item++)
        {
            HiddenField hdnStockCategoryID = Repeater2.Items[item].FindControl("hdnStockCategoryID") as HiddenField;
            TextBox rtxtQty = Repeater2.Items[item].FindControl("rtxtQty") as TextBox;
            HiddenField hdnStockItemID = Repeater2.Items[item].FindControl("hdnStockItemID") as HiddenField;

            if (hdnStockCategoryID.Value == "1")//Sorting panels from order
            {
                if (!string.IsNullOrEmpty(rtxtQty.Text))
                {
                    TotalPanelCount = TotalPanelCount + Convert.ToInt32(rtxtQty.Text);
                }
            }
        }

        int panelQty = TotalPanelCount;
        ModalPopupExtenderDeduct.Show();
        int srNo = 0;
        foreach (RepeaterItem ri in Repeater3.Items)
        {
            Label lblSrNo = ri.FindControl("lblSrNo") as Label;
            CheckBox chk = ri.FindControl("chkSerialNo") as CheckBox;
            if (ri.Visible == true)
            {
                srNo++;
                lblSrNo.Text = srNo.ToString();
                if (chk.Checked)
                {
                    countPanelNo--;
                    ri.Visible = false;
                    srNo--;
                }
            }
        }

        if (countPanelNo == 0)
        {
            SerialNoList.Visible = false;
            litremove.Visible = false;
            // savebtndiv.Enabled = false;
            ibtnAdd.Disabled = true;


        }
        if (panelQty == countPanelNo)
        {
            //Response.Write("hello");
            //Response.End();
            //  savebtndiv.Enabled = true;
            ibtnAdd.Disabled = false;
        }
        else
        {
            // savebtndiv.Enabled = false;
            ibtnAdd.Disabled = true;
        }
    }


    protected void litremove2_Click(object sender, EventArgs e)
    {
        // int countPanelNo = Repeater2.Items.Count;

        int TotalPanelCount = 0;

        for (int item = 0; item < Repeater2.Items.Count; item++)
        {
            HiddenField hdnStockCategoryID = Repeater2.Items[item].FindControl("hdnStockCategoryID") as HiddenField;
            TextBox rtxtQty = Repeater2.Items[item].FindControl("rtxtQty") as TextBox;
            HiddenField hdnStockItemID = Repeater2.Items[item].FindControl("hdnStockItemID") as HiddenField;

            if (hdnStockCategoryID.Value == "1")//Sorting panels from order
            {
                if (!string.IsNullOrEmpty(rtxtQty.Text))
                {
                    TotalPanelCount = TotalPanelCount + Convert.ToInt32(rtxtQty.Text);
                }
            }
        }

        int panelQty = TotalPanelCount;
        ModalPopupExtenderDeduct.Show();
        int srNo = 0;
        foreach (RepeaterItem ri in Repeater4.Items)
        {

            Label lblSrNo = ri.FindControl("lblSrNo2") as Label;
            CheckBox chk = ri.FindControl("chkSerialNo2") as CheckBox;
            if (ri.Visible == true)
            {
                srNo++;
                lblSrNo.Text = srNo.ToString();
                if (chk.Checked)
                {
                    countSerialNo--;
                    ri.Visible = false;
                    srNo--;
                }
            }
        }

        if (countSerialNo == 0)
        {
            SerialNoList2.Visible = false;
            litremove2.Visible = false;
            ibtnAdd.Disabled = true;
        }
        if (panelQty == countSerialNo)
        {
            //Response.Write("hello");
            //Response.End();
            ibtnAdd.Disabled = false;
        }
        else
        {
            ibtnAdd.Disabled = true;
        }
    }

    protected void btnSearchInvoiceNo2_Click(object sender, EventArgs e)
    {
        //hide pallet search repeater
        txtpallet.Text = string.Empty;
        SerialNoList.Visible = false;
        litremove.Visible = false;
        int TotalPanelCount = 0;

        ModalPopupExtenderDeduct.Show();
        String WholesaleOrderID = hdnWholesaleOrderID2.Value;
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        DataTable dt = ClstblStockOrders.tblStockSerialNoByInvNo_SearchByInvNo(st.InvoiceNo);

        for (int item = 0; item < Repeater2.Items.Count; item++)
        {
            HiddenField hdnStockCategoryID = Repeater2.Items[item].FindControl("hdnStockCategoryID") as HiddenField;
            TextBox rtxtQty = Repeater2.Items[item].FindControl("rtxtQty") as TextBox;
            HiddenField hdnStockItemID = Repeater2.Items[item].FindControl("hdnStockItemID") as HiddenField;

            if (hdnStockCategoryID.Value == "1")//Sorting panels from order
            {
                if (!string.IsNullOrEmpty(rtxtQty.Text))
                {
                    TotalPanelCount = TotalPanelCount + Convert.ToInt32(rtxtQty.Text);
                }
            }
        }

        if (dt.Rows.Count > 0)
        {
            countSerialNo = dt.Rows.Count;
            Repeater4.DataSource = dt;
            Repeater4.DataBind();
            SerialNoList2.Visible = true;
            divActive.Visible = false;
            litremove2.Visible = true;
            if (TotalPanelCount == dt.Rows.Count)
            {
                ibtnAdd.Disabled = false;
            }
            else
            {
                ibtnAdd.Disabled = true;
            }
        }
        else
        {
            divActive.Visible = true;
        }
    }

    protected void btnaddpanel_Click(object sender, EventArgs e)
    {
        if (PanelFileUpload.HasFile)
        {

            string RandomNumber = ClsAdminSiteConfiguration.RandomNumber();
            PanelFileUpload.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\WholesaleDeductPanel\\" + RandomNumber + "_" + PanelFileUpload.FileName);
            string connectionString = "";

            if (PanelFileUpload.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\WholesaleDeductPanel\\" + RandomNumber + "_" + PanelFileUpload.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (PanelFileUpload.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\WholesaleDeductPanel\\" + RandomNumber + "_" + PanelFileUpload.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            }


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                string InvoiceNo = "";
                                string Serial_No = "";

                                Serial_No = dr["SerialNo"].ToString();
                                InvoiceNo = dr["InvoiceNo"].ToString();

                                if (!string.IsNullOrEmpty(Serial_No) && !string.IsNullOrEmpty(InvoiceNo))
                                {
                                    //count++;
                                    int success = ClstblStockOrders.tblStockSerialNoByInvNo_Insert(InvoiceNo, Serial_No);
                                    if (success > 0)
                                    {
                                        SetAdd1();
                                    }
                                    else
                                    {
                                        SetError1();
                                    }
                                }
                                else
                                {
                                    SetError1();
                                }

                            }
                        }
                    }
                }
            }
        }
    }

    protected void rtxtQty_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderDeduct.Show();
        Validation();


    }

    public void Validation()
    {
        int TotalPanelCount = 0;

        for (int item = 0; item < Repeater2.Items.Count; item++)
        {
            HiddenField hdnStockCategoryID = Repeater2.Items[item].FindControl("hdnStockCategoryID") as HiddenField;
            TextBox rtxtQty = Repeater2.Items[item].FindControl("rtxtQty") as TextBox;
            HiddenField hdnStockItemID = Repeater2.Items[item].FindControl("hdnStockItemID") as HiddenField;
            Label lblerrmsg = Repeater2.Items[item].FindControl("lblerrmsg") as Label;
            Label rlblStock = Repeater2.Items[item].FindControl("rlblStock") as Label;

            if (!string.IsNullOrEmpty(rtxtQty.Text))
            {
                //if (Convert.ToInt32(rtxtQty.Text) != 0)
                //{
                    if (Convert.ToInt32(rtxtQty.Text) <= Convert.ToInt32(rlblStock.Text))
                    {
                        if (hdnStockCategoryID.Value == "1")//Sorting panels from order
                        {
                            //if (!string.IsNullOrEmpty(rtxtQty.Text))
                            //{
                            TotalPanelCount = TotalPanelCount + Convert.ToInt32(rtxtQty.Text);

                            //}
                        }
                        lblerrmsg.Text = "";
                        rtxtQty.Style.Add("border-color", "#B5B5B5");
                    }
                    else
                    {
                        lblerrmsg.Text = "Enter valid Qty.";
                        TotalPanelCount = -10000;
                        rtxtQty.Style.Add("border-color", "#FF5F5F");
                    }
                //}
                //else
                //{
                //    lblerrmsg.Text = "Qty can't be 0";
                //    TotalPanelCount = -10000;
                //    rtxtQty.Style.Add("border-color", "#FF5F5F");
                //}
            }
            else
            {
                lblerrmsg.Text = "Qty can't be Empty";
                TotalPanelCount = -10000;
                rtxtQty.Style.Add("border-color", "#FF5F5F");
            }
        }

        int ChangedQty = TotalPanelCount;
        //SerialNoList.Visible = false;
        //litremove.Visible = false;
        //savebtndiv.Enabled = false;
        ibtnAdd.Disabled = true;

        int count2 = 0;
        int count3 = 0;
        foreach (RepeaterItem item in Repeater3.Items)
        {
            if (item.Visible == true)
            {
                count2++;
            }
        }
        foreach (RepeaterItem item in Repeater4.Items)
        {
            if (item.Visible == true)
            {
                count3++;
            }
        }

        if (count2 == ChangedQty || count3 == ChangedQty)
        {
            // savebtndiv.Enabled = true;
            ibtnAdd.Disabled = false;
        }
        else
        {
            //  savebtndiv.Enabled = false;
            ibtnAdd.Disabled = true;
        }
    }
}