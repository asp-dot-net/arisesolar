using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_WholesaleIn : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv2;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSelectRecords2.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords2.DataBind();

            DataTable dt = ClstblContacts.tblCustType_SelectVender();


            BindGrid(0);
            BindGrid2(0);


            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller") || (Roles.IsUserInRole("PostInstaller"))))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                GridView2.Columns[GridView2.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                GridView2.Columns[GridView2.Columns.Count - 1].Visible = false;
            }

            BindDropDown();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                //tdExport.Visible = true;
            }
            else
            {
                // tdExport.Visible = false;
            }
        }
    }

    public void BindDropDown()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectWholesaleVendor();
        ddlvendor.DataSource = dt;
        ddlvendor.DataTextField = "Customer";
        ddlvendor.DataValueField = "CustomerID";
        ddlvendor.DataBind();

        ddlvendor2.DataSource = dt;
        ddlvendor2.DataTextField = "Customer";
        ddlvendor2.DataValueField = "CustomerID";
        ddlvendor2.DataBind();

        ddllocationsearch.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataMember = "CompanyLocationID";
        ddllocationsearch.DataBind();

        ddllocationsearch2.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch2.DataTextField = "location";
        ddllocationsearch2.DataValueField = "CompanyLocationID";
        ddllocationsearch2.DataMember = "CompanyLocationID";
        ddllocationsearch2.DataBind();

    }
    protected DataTable GetGridData1()
    {
        //DataTable dt1 = ClstblProjects.tblProjects_SelectWarehouseAllocated("1", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, "");
        DataTable dt1 = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectWarehouseAllocated("1", ddlvendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlSearchDate.SelectedValue, txtinvoiceno.Text.Trim(), ddllocationsearch.SelectedValue);
        return dt1;
    }

    protected DataTable GetGridData2()
    {
        DataTable dt2 = new DataTable();
        dt2 = Clstbl_WholesaleOrders.tblWholesaleOrderItems_StockRevert("1", ddlvendor2.SelectedValue, txtStartDate2.Text.Trim(), txtEndDate2.Text.Trim(), ddlSearchDate2.SelectedValue, txtinvoiceno2.Text.Trim(), ddllocationsearch2.SelectedValue, ddlRevert.SelectedValue);
        return dt2;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt1 = new DataTable();
        dt1 = GetGridData1();
        dv = new DataView(dt1);

        if (dt1.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt1;
            GridView1.DataBind();
            PanGrid.Visible = true;
            PanNoRecord.Visible = false;
            divnopage.Visible = true;
            if (dt1.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt1.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt1.Rows.Count + " entries";
                }
            }
        }
    }

    public void BindGrid2(int deleteFlag)
    {
        DataTable dt2 = new DataTable();
        dt2 = GetGridData2();
        dv2 = new DataView(dt2);

        if (dt2.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid2.Visible = false;
            divnopage2.Visible = false;
        }
        else
        {
            GridView2.DataSource = dt2;
            GridView2.DataBind();
            PanGrid2.Visible = true;
            PanNoRecord.Visible = false;
            divnopage2.Visible = true;
            if (dt2.Rows.Count > 0 && ddlSelectRecords2.SelectedValue != string.Empty && ddlSelectRecords2.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords2.SelectedValue) < dt2.Rows.Count)
                {
                    //========label Hide
                    divnopage2.Visible = false;
                }
                else
                {
                    divnopage2.Visible = true;
                    int iTotalRecords = dv2.ToTable().Rows.Count;
                    int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords2.SelectedValue == "All")
                {
                    divnopage2.Visible = true;
                    ltrPage2.Text = "Showing " + dt2.Rows.Count + " entries";
                }
            }
        }
    }


    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void ddlSelectRecords2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords2.SelectedValue) == "All")
        {
            GridView2.AllowPaging = false;
            BindGrid2(0);
        }
        else if (Convert.ToString(ddlSelectRecords2.SelectedValue) == "")
        {
            GridView2.AllowPaging = false;
        }
        else
        {
            GridView2.AllowPaging = true;
            GridView2.PageSize = Convert.ToInt32(ddlSelectRecords2.SelectedValue);
            BindGrid2(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid2(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "revert")
        {
            divMatch.Visible = false;
            string WholesaleOrderID = e.CommandArgument.ToString();
            hdnWholesaleOrderID.Value = e.CommandArgument.ToString();
            //Response.Write(ProjectID);
            //Response.End();
            Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
            Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);
            if (st.IsDeduct == "1")
            {
                ModalPopupExtenderDR.Show();
                rptDeductRev.DataSource = null;
                rptDeductRev.DataBind();
                rptDeductRevSerial.DataSource = null;
                rptDeductRevSerial.DataBind();

                lblCustomer2.Text = st.vendor;
                lblSystemDetails2.Text = st1.WholesaleOrderItemlist;
                lblInstallCity2.Text = st.CompanyLocation;
                lblInstallState2.Text = st1.WholesaleLocation;

                DataTable dt = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_SelectStock(WholesaleOrderID, st.InvoiceNo);
                if (dt.Rows.Count > 0)
                {
                    rptDeductRev.DataSource = dt;
                    rptDeductRev.DataBind();
                }

                DataTable dt1 = Clstbl_WholesaleOrders.tblDeductStockDeductSerialNo_SelectBy_InvNoWoid_RFlag(st.InvoiceNo, WholesaleOrderID, "0");
                if (dt1.Rows.Count > 0)
                {
                    rptDeductRevSerial.DataSource = dt1;
                    rptDeductRevSerial.DataBind();
                }

                foreach (RepeaterItem item in rptDeductRevSerial.Items)//panel column disappears when field value is null
                {
                    Panel tdhd2 = (Panel)item.FindControl("tdhd2");
                    Label lblPallet = (Label)item.FindControl("lblPallet");
                    if (string.IsNullOrEmpty(lblPallet.Text))
                    {
                        tdhd1.Visible = false;
                        tdhd2.Visible = false;
                    }
                }

                int TotalPanelCount = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    String StockCategoryID = dt.Rows[i]["StockCategoryID"].ToString();
                    String QTY = dt.Rows[i]["Deduct"].ToString();
                    Decimal QTY2 = Math.Floor(Convert.ToDecimal(QTY));
                    if (StockCategoryID == "1")
                    {
                        TotalPanelCount = TotalPanelCount + Convert.ToInt32(QTY2);
                    }
                }

                int OriginalPanelCount = 0;
                DataTable dt2 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(WholesaleOrderID);
                for(int i = 0; i < dt2.Rows.Count; i++)
                {
                    string StockCategoryID = dt2.Rows[i]["StockCategoryID"].ToString();
                    int OrderQuantity = Convert.ToInt32(dt2.Rows[i]["OrderQuantity"]);
                    if(StockCategoryID=="1")
                    {
                        OriginalPanelCount = OriginalPanelCount + OrderQuantity;
                    }
                }

                if(TotalPanelCount== OriginalPanelCount)
                {
                    btnRevertAll.Disabled = false;
                }
                else
                {
                    btnRevertAll.Disabled = true;
                }

            }
        }

        BindGrid(0);
    }

    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BindGrid2(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void btnSearch2_Click(object sender, EventArgs e)
    {
        BindGrid2(0);
    }

    protected void btnRevert_Click(object sender, EventArgs e)
    {
        string WholesaleOrderID = hdnWholesaleOrderID.Value;
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);
        string StockAllocationStore = st.CompanyLocationID;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        String Currentdate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        int suc1 = 0;
        int count = 0;
        divMatch.Visible = false;

        foreach (RepeaterItem item in rptDeductRevSerial.Items)//here we are counting the no of checked row so that we can compare it later.
        {
            CheckBox chkSerialNo = (CheckBox)item.FindControl("chkSerialNo");
            if (chkSerialNo.Checked == true)
            {
                count++;
            }
        }

        int TotalPanelCount = 0;
        foreach (RepeaterItem item in rptDeductRev.Items)//counting total panels quantity
        {
            HiddenField hndStockCatergoryID = (HiddenField)item.FindControl("hndStockCatergoryID");
            TextBox txtDeductRevert = (TextBox)item.FindControl("txtDeductRevert");
            String x = txtDeductRevert.Text;
            if (hndStockCatergoryID.Value == "1")
            {
                if (!string.IsNullOrEmpty(txtDeductRevert.Text))
                {
                    TotalPanelCount = TotalPanelCount + Convert.ToInt32(txtDeductRevert.Text);
                }
            }
        }

        if (count == TotalPanelCount)
        {
            foreach (RepeaterItem item in rptDeductRev.Items)
            {
                TextBox txtDeductRevert = (TextBox)item.FindControl("txtDeductRevert");
                HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");

                string Stock = txtDeductRevert.Text.Trim();
                string StockItemID = hndStockItemID.Value;

                if (txtDeductRevert.Text.Trim() != string.Empty)
                {
                    SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockAllocationStore);
                    suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, Stock, userid, WholesaleOrderID, st1.WholesaleOrderItemID, st.InvoiceNo);
                    Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Stock, StockAllocationStore);
                    //It would simply deduct stock qty again and the record of deduction is stored in tblWholesaleStockItemInventoryHistory and for that tblWholesaleStockItemInventoryHistory id revert flag and revert date is updated
                    //Nextly,in tblStockItems the stock is moved back for that system id and location.
                    //Here first tblWholesaleStockItemInventoryHistory_Insert called to created separate record and that particular record's revert data is updated +stock qty is +ve which states that it was revert operation.
                    //Here,both deduct and revert record is stored separatly.
                }
            }
            // ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID,"");
            //  ClstblProjects.tblProjects_UpdateStockDeductBy(ProjectID, "");

            foreach (RepeaterItem item2 in rptDeductRevSerial.Items)
            {
                CheckBox chkSerialNo = (CheckBox)item2.FindControl("chkSerialNo");
                if (chkSerialNo.Checked == true)
                {
                    String Pallet = "";
                    Label lblPallet = (Label)item2.FindControl("lblPallet");
                    Label lblSerialNo = (Label)item2.FindControl("lblSerialNo");
                    if (!string.IsNullOrEmpty(lblPallet.Text))
                    {
                        Pallet = lblPallet.Text;
                    }
                    Clstbl_WholesaleOrders.tblWholesaleStockDeductSerialNo_UpdateRevert(lblSerialNo.Text, Pallet, WholesaleOrderID, Currentdate, stEmp.EmployeeID);
                }
            }

            ModalPopupExtenderDR.Hide();
            BindGrid(0);

        }
        else
        {
            ModalPopupExtenderDR.Show();
            divMatch.Visible = true; //div mismatch  
        }
    }
    protected void btnRevertAll_Click(object sender, EventArgs e)

    {
        string WholesaleOrderID = hdnWholesaleOrderID.Value;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);

        //DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
        DataTable dt = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_SelectStock(WholesaleOrderID, st.InvoiceNo);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string StockItemID = dt.Rows[i]["StockItemID"].ToString();
            string StockSize = dt.Rows[i]["Stock"].ToString();
            string StockLocation = st.CompanyLocationID;

            SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(StockItemID);
            String ItemType = st2.StockCategory;

            decimal size = Math.Round(Convert.ToDecimal(StockSize));
            int myStock = (Convert.ToInt32(size)) * (-1);
            //Here,-1 is multipled as value which is fetched from from tblWholesaleStockItemInventoryHistory is negative
            //We need to make the value positive because we add the qty back in tblStockItems

            SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockLocation);
            int suc1 = Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_Insert("5", StockItemID, StockLocation, stOldQty.StockQuantity, Convert.ToString(myStock), userid, WholesaleOrderID, st1.WholesaleOrderItemID, st.InvoiceNo);
            //Response.Write(suc1);
            //Response.End();
            Clstbl_WholesaleOrders.tblWholesaleStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "2", DateTime.Now.AddHours(14).ToString());
            bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Convert.ToString(myStock), StockLocation);
        }
        Clstbl_WholesaleOrders.tblWholesaleOrders_UpdateRevert(WholesaleOrderID, "", "0");
        //Clstbl_WholesaleOrders.tblWholesaleOrders_UpdateRevert(WholesaleOrderID, stEmp.EmployeeID, "0");
        //Deduct flag is changed to zero and deductdate is changed to null for WholesaleOrderID
        ClstblExtraWholesaleStock.tblExtraWholesaleStock_DeleteByWholeOrderID(WholesaleOrderID);
        //delete record by WholesaleOrderID in tblExtraWholesaleStock

        /* -------------------- Update Project Status -------------------- */
        //if (st.InstallBookingDate != string.Empty)
        //{
        //    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
        //    //Project status 11 is for status 'job booked'.
        //}
        //else
        //{
        //    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //Project status 3 is for status 'Active'.
        // }
        /* -------------------------------------------------------------- */
        //we change the status since the status was changed to 'Stock Assigned' after deduction,but after reverting all it has to be 
        //changed back to 'Job Booked'(When install booking date is assigned) and 'Active'

        String Currentdate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));

        foreach (RepeaterItem item2 in rptDeductRevSerial.Items)
        {
            String Pallet = "";
            CheckBox chkSerialNo = (CheckBox)item2.FindControl("chkSerialNo");
            Label lblPallet = (Label)item2.FindControl("lblPallet");
            Label lblSerialNo = (Label)item2.FindControl("lblSerialNo");
            if (!string.IsNullOrEmpty(lblPallet.Text))
            {
                Pallet = lblPallet.Text;
            }
            Clstbl_WholesaleOrders.tblWholesaleStockDeductSerialNo_UpdateRevert(lblSerialNo.Text, Pallet, WholesaleOrderID, Currentdate, stEmp.EmployeeID);
        }

        ModalPopupExtenderDR.Hide();
        BindGrid(0);

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData2();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView2.DataSource = sortedView;
        GridView2.DataBind();
    }


    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void GridView2_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView2.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView2.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView2.PageIndex - 2;
        page[1] = GridView2.PageIndex - 1;
        page[2] = GridView2.PageIndex;
        page[3] = GridView2.PageIndex + 1;
        page[4] = GridView2.PageIndex + 2;
        page[5] = GridView2.PageIndex + 3;
        page[6] = GridView2.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView2.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView2.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView2.PageIndex == GridView2.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage2 = (Label)gvrow.Cells[0].FindControl("ltrPage2");
        if (dv2.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv2.ToTable().Rows.Count;
            int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage2.Text = "";
        }
    }


    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }

    void lb_Command2(object sender, CommandEventArgs e)
    {
        GridView2.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid2(0);

    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command2);
        }
    }

    protected void lnkReceived_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");

        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(hdnstockid.Value);
        if (dt.Rows.Count > 0)
        {
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
            divdetail.Visible = true;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }

    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;

        divright.Visible = true;
    }
    //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        Label lblAllocatePanels = (Label)e.Row.FindControl("lblAllocatePanels");
    //        HiddenField hdnProjectID = (HiddenField)e.Row.FindControl("hdnProjectID");
    //        SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
    //        if (stpro.StockDeductDate != string.Empty)
    //        {
    //            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(hdnProjectID.Value);
    //            if (dt.Rows.Count > 0)
    //            {
    //                decimal deduct = 0;
    //                foreach (DataRow dr in dt.Rows)
    //                {
    //                    deduct += Convert.ToDecimal(dr["Deduct"].ToString());
    //                }
    //                lblAllocatePanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0}", deduct);
    //            }
    //            //lblAllocatePanels.Text = stpro.NumberPanels;
    //        }

    //    }
    //}
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    //protected void lbtnExport1_Click(object sender, EventArgs e)
    //{
    //    DataTable dt = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, chkHistoric.Checked.ToString());
    //    Response.Clear();
    //    try
    //    {
    //        Export oExport = new Export();
    //        string FileName = "StockDeduct" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
    //        int[] ColList = { 11, 14, 301, 299, 52, 300 };
    //        string[] arrHeader = { "ProjectNumber", "Project", "InstallBookingDate", "Installer", "Details", "Store Name" };
    //        //only change file extension to .xls for excel file
    //        oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
    //    }
    //    catch (Exception Ex)
    //    {
    //        //   lblError.Text = Ex.Message;
    //    }
    //}

    protected void lblexport7_Click(object sender, EventArgs e)
    {

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtinvoiceno.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddllocationsearch.SelectedValue = "";
        ddlSearchDate.SelectedValue = "1";
        BindGrid(0);
    }


    protected void btnClearAll2_Click1(object sender, EventArgs e)
    {
        txtinvoiceno2.Text = string.Empty;
        ddllocationsearch2.SelectedValue = "";
        ddlSearchDate2.SelectedValue = "";
        ddlvendor2.SelectedValue = "0";
        txtStartDate2.Text = string.Empty;
        txtEndDate2.Text = string.Empty;
        BindGrid2(0);
    }

    //protected void rptDeductRevSerial_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{

    //    //rptDeductRevSerial.Columns[0].Visible = false;
    //    Panel tdhd2 = (Panel)e.Item.FindControl("tdhd2");
    //    string WholesaleOrderID = hdnWholesaleOrderID.Value;
    //    Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
    //    Sttbl_WholesaleOrderItems st1 = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_SelectByWholesaleOrderID(WholesaleOrderID);
    //    DataTable dt1 = Clstbl_WholesaleOrders.tblDeductStockDeductSerialNo_SelectBy_InvNoWoid_RFlag(st.InvoiceNo, WholesaleOrderID, "0");
    //    if (dt1.Rows.Count > 0)
    //    {
    //        if (string.IsNullOrEmpty(dt1.Rows[0]["Pallet"].ToString()))
    //        {
    //            tdhd1.Visible = false;
    //            tdhd2.Visible = false;
    //            //tdhd2.Visible=
    //        }
    //        //rptDeductRevSerial.DataSource = dt1;
    //        //rptDeductRevSerial.DataBind();
    //    }
    //}
}