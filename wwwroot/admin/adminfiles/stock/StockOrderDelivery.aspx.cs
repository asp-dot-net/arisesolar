﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_StockOrderDelivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string StockOrderID = Request.QueryString["StockOrderID"].ToString();
            Binddata(StockOrderID);
        }
    }

    public void Binddata(string StockOrderID)
    {
        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(StockOrderID);

        if (st.CompanyLocation != string.Empty)
        {
            lblStockLocation.Text = st.CompanyLocation;
        }
        else
        {
            lblStockLocation.Text = "-";
        }
        if (st.Vendor != string.Empty)
        {
            lblVendor.Text = st.Vendor;
        }
        else
        {
            lblVendor.Text = "-";
        }
        if (st.BOLReceived != string.Empty)
        {
            lblBOLReceived.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.BOLReceived));
        }
        else
        {
            lblBOLReceived.Text = "-";
        }
        if (st.ManualOrderNumber != string.Empty)
        {
            lblManualOrderNo.Text = st.ManualOrderNumber;
        }
        else
        {
            lblManualOrderNo.Text = "-";
        }
        if (st.ExpectedDelivery != string.Empty)
        {
            lblExpectedDelevery.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ExpectedDelivery));
        }
        else
        {
            lblExpectedDelevery.Text = "-";
        }

        DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(StockOrderID);
        if (dt.Rows.Count > 0)
        {
            rptOrder.DataSource = dt;
            rptOrder.DataBind();
            trOrderItem.Visible = true;
        }
        else
        {
            trOrderItem.Visible = false;
        }
    }

    protected void rptOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        int CategoryID;
        int OrderQuantity;
        HiddenField hfStockOrderID = (HiddenField)e.Item.FindControl("hfStockOrderID");
        HiddenField StockCategoryID = (HiddenField)e.Item.FindControl("hfStockCategoryID");
        HiddenField hfStockItemID = (HiddenField)e.Item.FindControl("hfStockItemID");
        LinkButton btnDelivery = (LinkButton)e.Item.FindControl("btnDelivery");
        ImageButton checkimag = (ImageButton)e.Item.FindControl("checkimag");
        Label lblOrderQuantity = (Label)e.Item.FindControl("lblOrderQuantity");
        DataTable dtGetData = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(Convert.ToString(hfStockOrderID.Value));
        //1 For Module 2 For Inveerter
        if (dtGetData.Rows.Count > 0)
        {
            foreach (DataRow dr in dtGetData.Rows)
            {
                int DeliveryOrderStatus = Convert.ToInt32(dr["DeliveryOrderStatus"].ToString());
                if (DeliveryOrderStatus == 1)
                {
                    // divdelivery.Visible = true;
                    if (Convert.ToInt32(StockCategoryID.Value) == 1)
                    {

                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "1", Convert.ToString(hfStockItemID.Value));
                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 1)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                    btnDelivery.Visible = true;
                                    checkimag.Visible = false;
                                }
                            }
                        }

                    }
                    else if (Convert.ToInt32(StockCategoryID.Value) == 2)
                    {
                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "2", Convert.ToString(hfStockItemID.Value));

                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 2)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                    btnDelivery.Visible = true;
                                    checkimag.Visible = false;
                                }
                            }
                        }
                    }
                }
                else if (DeliveryOrderStatus == 0)
                {
                    // divdelivery.Visible = false;
                    // btnDelivery.Visible = false;
                    if (Convert.ToInt32(StockCategoryID.Value) == 1)
                    {

                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "1", Convert.ToString(hfStockItemID.Value));
                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 1)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                    btnDelivery.Enabled = false;
                                    checkimag.Visible = false;
                                }
                            }
                        }

                    }
                    else if (Convert.ToInt32(StockCategoryID.Value) == 2)
                    {
                        DataTable dt = ClstblStockOrders.tblStockSerialNoCount_ByStockOrderID(Convert.ToString(hfStockOrderID.Value), "2", Convert.ToString(hfStockItemID.Value));

                        foreach (DataRow row in dtGetData.Rows)
                        {
                            //get the values here
                            CategoryID = Convert.ToInt32(row["StockCategoryID"].ToString());
                            OrderQuantity = Convert.ToInt32(row["OrderQuantity"].ToString());
                            if (CategoryID == 2)
                            {
                                if (OrderQuantity == dt.Rows.Count)
                                {
                                    btnDelivery.Visible = false;
                                    checkimag.Visible = true;
                                }
                                else
                                {
                                   btnDelivery.Enabled = false;
                                    checkimag.Visible = false;
                                }
                            }
                        }
                    }

                }
                else
                {
                     btnDelivery.Visible = false;
                }
            }
        }
    }

    protected void rptOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delivered")
        {
            ModalPopupdeliver.Show();
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            string StockOrderID = commandArgs[0];
            string StockOrderItemID = commandArgs[1];
            hndStockOrderItemID.Value = StockOrderItemID;


            hndid.Value = StockOrderID;
            txtdatereceived.Text = DateTime.Now.AddHours(14).ToString();
        }
    }

    protected void btndeliver_Click(object sender, EventArgs e)
    {
        string receive = txtdatereceived.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Section = "";
        string Message = "";
        DateTime Currendate = DateTime.Now;
        //bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
        //if (receive != string.Empty)
        //{
        //    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
        //}

        SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);
        //Response.Write(success);
        //Response.End();
        //if (success)
        //{
        //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
        //if (dt.Rows.Count > 0)
        //{
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
        //        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
        //        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
        //    }
        //}
        //SetDelete();

        int success1 = 0;
        //Response.Expires = 400;
        //Server.ScriptTimeout = 1200;

        if (FileUpload1.HasFile)
        {
            //SetAdd1();
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Delivery\\" + FileUpload1.FileName);
            string connectionString = "";

            //if (FileUpload1.FileName.EndsWith(".csv"))
            //{
            //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
            //}
            // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

            //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";


            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            //else if (FileUpload1.FileName.EndsWith(".xlsx"))
            //{
            //    connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Delivery\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            //}


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    // string employeeid = string.Empty;
                    //string flag = "true";
                    // SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(hndid.Value);

                    //DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum(hndid.Value);
                    DataTable dt3 = ClstblStockOrders.tblStockOrderItems_QtySum_ByStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr2 = dt3.Rows[0];
                    string qty = dr2["Qty"].ToString();

                    //DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                    DataTable dt2 = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                    DataRow dr1 = dt2.Rows[0];
                    string stockid = dr1["StockItemID"].ToString();
                    int count = 0;
                    int exist = 0;
                    var ExistSerialNo = new List<string>();
                    List<string> NovalueList = new List<string>();
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        //Response.Write(dr.Depth);
                        //Response.End();
                        int blank = 0;
                        int flagcolNames = 0;

                        if (dr.HasRows)
                        {
                            var columns = new List<string>();
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                columns.Add(dr.GetName(i));
                                if (!string.IsNullOrEmpty(dr.GetName(i)))
                                {
                                    string attributevalue0 = "Serial No";
                                    string attributevalue1 = "Pallet";
                                    int j = 0;
                                    string columnname = dr.GetName(i).ToString();
                                    if (columnname.StartsWith(attributevalue0) == true || columnname.StartsWith(attributevalue1) == true)
                                    {
                                        flagcolNames = 0;
                                    }
                                    else
                                    {
                                        flagcolNames = 1;
                                        break;
                                    }
                                }
                            }

                            if (flagcolNames == 0)
                            {
                                while (dr.Read())
                                {
                                    string SerialNo = "";
                                    string Pallet = "";
                                    SerialNo = dr["Serial No"].ToString();
                                    Pallet = dr["Pallet"].ToString();

                                    if (SerialNo == "" && Pallet == "")
                                    {
                                        blank = 1;
                                        break;
                                    }
                                    if (blank != 1)
                                    {
                                        exist = ClstblStockOrders.tblStockSerialNo_Exist(SerialNo);
                                        if (exist == 0)
                                        {
                                            count++;
                                        }
                                        else
                                        {
                                            sttblStockSerialNo stitem = ClstblStockOrders.tblStockSerialNo_Getdata_BySerialNo(SerialNo);
                                            //blank = 1;
                                            ExistSerialNo.Add(stitem.SerialNo.ToString());
                                            NovalueList.Add("exist");
                                            //  break;
                                        }

                                    }
                                }
                            }
                            else
                            {
                                ExcelFileHeader();
                            }
                            //Response.Write();
                        }
                    }
                    //Response.Write(count);
                    //Response.End();
                    if ((NovalueList != null) && (!NovalueList.Any()))
                    {
                        PanSerialNo.Visible = false;
                        if ((count == Convert.ToInt32(qty)) && (!NovalueList.Any()))
                        {

                            using (DbDataReader dr = command.ExecuteReader())
                            {

                                if (dr.HasRows)
                                {
                                    //SetAdd1();

                                    //if (flag == "false")
                                    //{
                                    //    PanEmpty.Visible = true;
                                    //}
                                    //else
                                    //{


                                    while (dr.Read())
                                    {

                                        int blank2 = 0;
                                        string SerialNo = "";
                                        string Pallet = "";

                                        SerialNo = dr["Serial No"].ToString();
                                        Pallet = dr["Pallet"].ToString();
                                        if (SerialNo == "" && Pallet == "")
                                        {
                                            blank2 = 1;
                                        }
                                        if (blank2 != 1)
                                        {
                                            if ((SerialNo != string.Empty) || (Pallet != string.Empty))
                                            {
                                                //try
                                                //{

                                                //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                                // SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                                //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                                //Response.End();
                                                // success1 = ClstblStockOrders.tblStockSerialNo_Insert(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet);

                                                string StockOrderID = hndid.Value;
                                                Section = "Stock Order";
                                                Message = "Stock In for Order Number:" + StockOrderID;

                                                //success1 = ClstblStockOrders.tblStockSerialNo_InsertWithOrderID(stockid, st.CompanyLocationID.ToString(), SerialNo, Pallet, StockOrderID);
                                                //ClstblMaintainHistory.tblMaintainHistory_InsertWithOrderID(userid, StockOrderID, SerialNo, Section, Message, Currendate);
                                                //}
                                                //catch { }
                                            }
                                            if (success1 > 0)
                                            {
                                                SetAdd1();
                                                string StockOrderID = Request.QueryString["StockOrderID"].ToString();
                                                Binddata(StockOrderID);

                                            }
                                            else
                                            {
                                                SetError1();
                                            }
                                        }
                                        //}
                                    }
                                }
                            }
                            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
                            if (success)
                            {
                                if (receive != string.Empty)
                                {
                                    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                                }

                                //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                                    }
                                }
                            }
                            else
                            {
                                SetError();
                            }


                        }
                        else
                        {
                            //if (exist == 0)
                            //{
                            RecordMisMatch();
                            // }
                            //if (exist == 1)
                            //{
                            //    SerialNoExist();
                            //}
                        }
                    }
                    else
                    {
                        PanSerialNo.Visible = true;
                        int sizeOfList = ExistSerialNo.Count;
                        List<string> NovalueListmsg = new List<string>();
                        for (int i = 0; i < sizeOfList; i++)
                        {
                            string Something =  Convert.ToString(ExistSerialNo[i]) + ",";
                            string error = Something.TrimEnd(',');
                            NovalueListmsg.Add(Something);


                            //Response.Write(Convert.ToString(orderList[i]) + "<br \\");
                        }
                        ltrerror.Text = "Duplicate Serial No:<br/>" + string.Join("<br/>", NovalueListmsg).TrimEnd(',') ;
                    }

                }
            }
        }
        else
        {
            bool success = ClstblStockOrders.tblStockOrders_Update_Delivered(hndid.Value, stEmployeeid.EmployeeID, "1");
            if (success)
            {
                if (receive != string.Empty)
                {
                    ClstblStockOrders.tblStockOrders_Update_ActualDelivery(hndid.Value, receive);
                }

                //DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(hndid.Value);
                DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(hndid.Value, hndStockOrderItemID.Value);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(row["StockItemID"].ToString(), st.CompanyLocationID);
                        ClstblStockItems.tblStockItemInventoryHistory_Insert("4", row["StockItemID"].ToString(), st.CompanyLocationID, StOldQty.StockQuantity, row["OrderQuantity"].ToString(), userid, "0");
                        ClstblStockItemsLocation.tblStockItemsLocation_Update_StockItemID_CompanyLocationID(row["StockItemID"].ToString(), st.CompanyLocationID.ToString(), row["OrderQuantity"].ToString());
                    }
                }
                SetAdd1();
            }
            else
            {
                SetError();
            }

        }

        //}
        //else
        //{
        //    SetError();
        //}
        txtdatereceived.Text = string.Empty;
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void RecordMisMatch()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunRecordMismatch();", true);
    }

    public void SerialNoExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSerialNoExist();", true);
    }

    public void ExcelFileHeader()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunExcelFileHeader();", true);
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }
    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
       // lnkAdd.Visible = true;

        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
       // PanGrid.Visible = true;
        PanGridSearch.Visible = true;
                  

    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/stockorder.aspx");
    }
}