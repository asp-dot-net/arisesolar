<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="Wholesale.aspx.cs" Inherits="admin_adminfiles_stock_Wholesale" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }
            </script>

            <script>
                function printContent() {
                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();
                }

            </script>
            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {


                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {


                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $("[data-toggle=tooltip]").tooltip();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $(document).ready(function () {

                        $('#<%=ibtnAddVendor.ClientID %>').click(function () {
                            formValidate();
                        });
                        $('#<%=ibtnAddStock.ClientID %>').click(function () {
                            formValidate();
                        });
                    });

                }
            </script>
            <div class="page-body headertopbox printorder">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Wholesale
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
                </div>
            </div>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                }
            </script>
            <script>


                function doMyAction() {

                    $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();
                    });
                }

            </script>

            <script>
                function ComfirmDelete(event, ctl) {
                    event.preventDefault();
                    var defaultAction = $(ctl).prop("href");

                    swal({
                        title: "Are you sure you want to delete this Record?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },

                        function (isConfirm) {
                            if (isConfirm) {
                                // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                eval(defaultAction);

                                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                return true;
                            } else {
                                // swal("Cancelled", "Your imaginary file is safe :)", "error");
                                return false;
                            }
                        });
                }
            </script>
            <div class="finaladdupdate printorder">
                <div id="PanAddUpdate" runat="server" visible="true">

                    <div class="panel-body animate-panel padtopzero stockorder">
                        <div class="well with-header  addform">
                            <div class="header bordered-blue">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                Wholesale Order
                            </div>

                            <div class="form-horizontal">
                                <div class="form-group col-md-3">
                                    <asp:Label ID="Label1" runat="server" class="col-sm-12">
                                               Invoice No</asp:Label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtVendorInvoiceNo" runat="server" MaxLength="200" CssClass="form-control modaltextbox" AutoPostBack="true"
                                             OnTextChanged="txtVendorInvoiceNo_TextChanged"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass=""
                                            ControlToValidate="txtVendorInvoiceNo" Display="Dynamic" ValidateRequestMode="Enabled"></asp:RequiredFieldValidator>

                                    </div>
                                </div>

                                <div class="form-group col-md-3 col-sm-3">
                                    <asp:Label ID="lblLastName" runat="server" class="col-sm-12">
                                             Customer Name</asp:Label>
                                    <div class="col-sm-12">
                                        <div class=" selectboxman">
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlVendor" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                                    ControlToValidate="ddlVendor" InitialValue="" ErrorMessage=""></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="usericonbox">
                                            <asp:LinkButton ID="btnNewVendor" runat="server" CausesValidation="false" OnClick="btnNewVendor_Click1"
                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Customer Name" CssClass="btn btn-into btn-xs">
                                                            <i class="fa fa-user"></i>
                                            </asp:LinkButton>
                                            <%-- <asp:ImageButton ID="btnNewVendor" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add Vendor"
                                                    OnClick="btnNewVendor_Click" ImageUrl="~/images/addvendor.png" />--%>

                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                    ControlToValidate="ddlVendor" Display="Dynamic" ValidationGroup="addorder"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <asp:Label ID="lblFirstName" runat="server" class="col-sm-12">
                                              Reference No</asp:Label>
                                    <div class="col-sm-12">
                                        <div class="selectboxman">
                                            <asp:TextBox ID="txtManualOrderNumber" runat="server" MaxLength="200" CssClass="form-control modaltextbox"
                                                 AutoPostBack="true" OnTextChanged="ReferenceNo_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="dynamic"
                                                ControlToValidate="txtManualOrderNumber" InitialValue="" ErrorMessage=""></asp:RequiredFieldValidator>
                                        </div>
                                        <%--  <div class="usericonbox">
                                            <asp:LinkButton ID="btnNewStock" runat="server" CausesValidation="false" OnClick="btnNewStock_Click"
                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Stock" CssClass="btn btn-default btn-xs" Visible="false">
                                                            <i class="fa fa-plus-square"></i>
                                            </asp:LinkButton>
                                        </div>--%>
                                        <%-- <asp:ImageButton ID="btnNewStock" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add Stock"
                                                    OnClick="btnNewStock_Click" ImageUrl="~/images/addstock.png" />--%>
                                    </div>
                                </div>




                                <div class="form-group col-md-3">
                                    <asp:Label ID="Label3" runat="server" class="col-sm-12">
                                                 Stock Location</asp:Label>
                                    <div class="col-sm-12">
                                        <div class="drpValidate">
                                            <asp:DropDownList ID="ddlStockLocation" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                ValidationGroup="Add" ControlToValidate="ddlStockLocation" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="graybgarea">

                                        <asp:Repeater ID="rptattribute" runat="server" OnItemDataBound="rptattribute_OnItemDataBound">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Category
                                                                </label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockCategory" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged"
                                                                        AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                        ValidationGroup="Add" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Item</label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Quantity</label>
                                                            </span><span style="width: 60%">
                                                                <asp:HiddenField ID="hdnOrderQuantity" runat="server" Value='<%#Eval("OrderQuantity") %>' />
                                                                <asp:TextBox ID="txtOrderQuantity" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                                    ErrorMessage="Enter valid digit" ControlToValidate="txtOrderQuantity" ValidationGroup="addorder"></asp:RegularExpressionValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="comperror"
                                                                    ControlToValidate="txtOrderQuantity" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group ">
                                                            <div class="padtop15">
                                                                <span class="name">
                                                                    <asp:HiddenField ID="hdnWholesaleOrderItemID" runat="server" Value='<%# Eval("WholesaleOrderItemID") %>' />
                                                                    <asp:CheckBox ID="chkdelete" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <br />
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:Button ID="litremove" runat="server" OnClick="litremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" /><br />

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                     
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                   
                                    </div>
                                </div>
                                                           <div class="col-md-9">
                                    <div class="form-group" style="text-align: right;">
                                        <asp:Button ID="btnaddnew" runat="server" Text="Add" OnClick="btnAddRow_Click" CssClass="btn btn-info redreq"
                                            CausesValidation="false" /><br />

                                    </div>
                         

                                </div>
                                <%--     <div class="col-md-12">
                                                   
                                                </div>--%>
         
                         <div class="col-md-12">
                                    <div class="graybgarea">
                                              <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Job Type
                                                                </label>
                                                            </span><span>
                                                                
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlJobType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                                                          <asp:ListItem Value="1">Cash</asp:ListItem>
                                                                          <asp:ListItem Value="2">STC</asp:ListItem>
                                                                    </asp:DropDownList>                                                               
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>

                                                  <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Invoice Amount</label>
                                                            </span><span >                                                              
                                                                <asp:TextBox ID="txtinvoiceamt" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <asp:RegularExpressionValidator runat="server" ID="regexp" ControlToValidate="txtinvoiceamt"  ValidationExpression="^[0-9]*.?[0-9]+$"
                                                                 ErrorMessage="Amount should be in digit." ForeColor="Red"></asp:RegularExpressionValidator> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    PVDNumber</label>
                                                            </span><span >                                                              
                                                                <asp:TextBox ID="txtpvdno" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                  
                                        <div class="form-group col-md-3">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                   Status
                                                                </label>
                                                            </span><span>
                                                                
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlstatus" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                                                          <asp:ListItem Value="1">Pending</asp:ListItem>
                                                                          <asp:ListItem Value="2">JobBooked</asp:ListItem>
                                                                         <asp:ListItem Value="3">Cancel</asp:ListItem>
                                                                         <asp:ListItem Value="4">PVD Applied</asp:ListItem>
                                                                    </asp:DropDownList>                                                               
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                        </div>
                                  </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label26" runat="server" class="disblock">
                                              BOL Received</asp:Label>
                                                <div class="input-group date datetimepicker1 col-sm-12">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtBOLReceived" runat="server" class="form-control" placeholder="Bol Received Date">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group col-md-3">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label27" runat="server" class="disblock">
                                                   Expected Delevery</asp:Label>
                                                <div class="input-group date datetimepicker1 col-sm-12">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtExpectedDelivery" runat="server" class="form-control" placeholder="Expected Delevery Date">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" Display="dynamic"
                                                        ControlToValidate="txtExpectedDelivery" InitialValue="" ErrorMessage=""></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-6">
                                            <div class="col-md-12">
                                                <span class="name disblock">
                                                    <label id="Label4" runat="server" class="disblock">
                                                        Notes</label>
                                                </span>
                                                <div>
                                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Height="100px" Width="100%"
                                                        CssClass="form-control modaltextbox">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <asp:Button CssClass="btn btn-default purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" CausesValidation="true" />
                                        <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CausesValidation="false"
                                            Text="Save" Visible="false" />
                                        <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-body padtopzero printorder">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Vendor Invoice Number already exists.</strong>
                            </div>
                        </div>


                        <div class="searchfinal">
                            <div class="widget-body shadownone brdrgray">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="dataTables_filter Responsive-search">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="input-group col-sm-1">
                                                            <asp:TextBox ID="txtSearchOrderNo" runat="server" placeholder="Invoice No" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchOrderNo"
                                                                WatermarkText="Invoice No" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtSearchOrderNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>
                                                        <div class="input-group col-sm-1">
                                                            <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                <asp:ListItem Value="">Customer Name</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-2">
                                                            <asp:TextBox ID="txtreferenceno" runat="server" placeholder="Reference No" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtreferenceno"
                                                                WatermarkText="Reference No" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtreferenceno" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>


                                                        <div class="input-group col-sm-1">
                                                            <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                <asp:ListItem Value="">Date</asp:ListItem>
                                                                <asp:ListItem Value="1">DateOrdered</asp:ListItem>
                                                                <asp:ListItem Value="2">BOLReceived</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic"
                                                            ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                                            ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                        <div class="input-group col-sm-1">
                                                            <asp:DropDownList ID="ddlShow" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" Visible="false">
                                                                <asp:ListItem Value="">Show</asp:ListItem>
                                                                <asp:ListItem Value="False">Not Received</asp:ListItem>
                                                                <asp:ListItem Value="True">Received</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1">
                                                            <asp:DropDownList ID="ddlDue" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" Visible="false">
                                                                <asp:ListItem Value="">Due</asp:ListItem>
                                                                <asp:ListItem Value="0">Due Today</asp:ListItem>
                                                                <asp:ListItem Value="1">Due Tomorrow</asp:ListItem>
                                                                <asp:ListItem Value="2">OverDue</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="datashowbox">
                                    <div class="row">
                                        <div class="dataTables_length showdata col-sm-3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>

                </asp:Panel>



                <div class="finalgrid ">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview printorder"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                        OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" >
                                        <Columns>
                                            <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="tdspecialclass"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="OrderNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Width="100px" CssClass="gridmainspan">
                                                        <asp:HiddenField ID="hdnStockOrderID" runat="server" Value='<%# Eval("WholesaleOrderID") %>' />
                                                        <asp:HyperLink ID="hlDetails1" runat="server" NavigateUrl='<%# "~/admin/adminfiles/reports/Wholesaleorder.aspx?id="+ Eval("WholesaleOrderID").ToString()%>'>
                                                                    <%#Eval("OrderNumber")%>
                                                        </asp:HyperLink>

                                                        <div class="contacticonedit">
                                                            <asp:LinkButton ID="hypDetail" runat="server" CausesValidation="false" CommandName="detail" CommandArgument='<%# Eval("WholesaleOrderID") %>'
                                                                data-placement="top" data-original-title="Detail" data-toggle="tooltip" CssClass="btn btn-primary btn-xs">
                                                                        <%-- <img src="../../../images/icon_detail.png" width="15" height="16" />--%>
                                                                          <i class="fa fa-link"></i>Detail
                                                            </asp:LinkButton>
                                                        </div>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--      <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="80px">
                                                <%#Eval("InvoiceNo")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Invoice No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="InvoiceNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Width="80px">
                                                   <%#Eval("InvoiceNo")%>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Invoice Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="DateOrdered">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Width="100px">
                                                <%# DataBinder.Eval(Container.DataItem, "DateOrdered", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Stock For" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="80px">
                                                <%#Eval("CompanyLocation")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reference No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="ReferenceNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Width="80px">
                                                <%#Eval("ReferenceNo")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Items Ordered" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="WholesaleOrderItem">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("WholesaleOrderItem")%>' data-toggle="tooltip"
                                                        Width="260px"><%#Eval("WholesaleOrderItem")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                SortExpression="Vendor">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label7" runat="server" data-placement="top" data-original-title='<%#Eval("Vendor")%>' data-toggle="tooltip"
                                                        Width="170px"><%#Eval("Vendor")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text"
                                                SortExpression="Qty">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label8" runat="server" Width="50px"><%#Eval("Qty")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="ContPhone">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label9" runat="server" Width="90px">
                                                <%#Eval("ContPhone")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Expected Delivery" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" SortExpression="ExpectedDelivery">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label10" runat="server" Width="130px">
                                                <%# DataBinder.Eval(Container.DataItem, "ExpectedDelivery", "{0:dd MMM yyyy}")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="Delivery" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center-text" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="ActualDelivery">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Width="70px">
                                                <%# DataBinder.Eval(Container.DataItem, "ActualDelivery", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="Received By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="ReceivedName">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label121" runat="server" Width="100px"> 
                                                <%#Eval("ReceivedName")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                <ItemTemplate>
                                                    <%--  <img runat="server" id="imgr" src="../../images/icon_delivered.png" visible=' <%# Eval("Delivered").ToString() == "False" ? false : true%>' />--%>
                                                    <%--<asp:LinkButton ID="btnDelivered" runat="server" Text="Delivered" CausesValidation="false" CssClass="btn btn-maroon btn-xs" Visible='<%# Eval("Delivered").ToString() == "False" ? true : false%>'
                                                        data-toggle="tooltip" data-placement="top" title="Delivery" CommandName="Delivered" OnClientClick="yes" CommandArgument='<%#Eval("StockOrderID")%>'> <i class="fa fa-truck"></i> Delivery</asp:LinkButton>--%>
                                                    <asp:LinkButton ID="btnRevert" runat="server" Text="Revert" CausesValidation="false" CssClass="btn btn-maroon btn-xs" Visible='<%# Eval("Delivered").ToString() == "False" ? false : true%>'
                                                        data-toggle="tooltip" data-placement="top" title="Revert" CommandName="Revert" CommandArgument='<%#Eval("WholesaleOrderID")%>' OnClientClick="return ComfirmRevert(event,this)"> <i class="fa fa-retweet"></i> Revert</asp:LinkButton>


                                                    <%--<asp:Image runat="server"  ImageUrl="~/images/revert_deactive.png" />--%>
                                                    <%--	<span class="btn btn-default btn-xs" ID="imgko" Visible='<%# Eval("Delivered").ToString() == "False" ? true : false%>'  data-toggle="tooltip" data-placement="top" title="Deactive Revert" runat="server"><i class="fa fa-retweet"></i> Revert</span>--%>
                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" CssClass="btn btn-info btn-xs">
                                                            <i class="fa fa-edit"></i>Edit
                                                    </asp:LinkButton>

                                                    <!--DELETE Modal Templates-->

                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("WholesaleOrderID")%>' Visible='<%# Eval("Delivered").ToString() == "False" ? true : false%>'>
                        <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>
                                                    <%-- <asp:ImageButton ID="gvbtnUpdate" runat="server" Visible='<%# Eval("Delivered").ToString() == "True" ? false : true%>'
                                                                    CommandName="Select" ImageUrl="../../../images/icon_edit.png" CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />--%>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="verticaaline" />
                                            </asp:TemplateField>



                                        </Columns>
                                        <AlternatingRowStyle />

                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing printorder" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid printorder" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1 printorder" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>



                            <!--End Danger Modal Templates-->
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <asp:HiddenField ID="hndWholesaleOrderID" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="btnCancelDetail">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
                <div class="modal-dialog" style="width: 60%">
                    <div class="modal-content">
                        <div class="color-line "></div>
                        <div class="modal-header printorder">
                            <div style="float: right">
                                <asp:Button ID="btnCancelDetail" Width="65px" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" Text="Close" />

                           <%--     <asp:LinkButton ID="gvbtnPrint" runat="server" CssClass="btn btn-success btn-xs" OnClientClick="javascript:printContent();" CausesValidation="false">
                                              <i class="fa fa-print"></i> Print
                                </asp:LinkButton>--%>

                                     <asp:LinkButton ID="gvbtnPrint" runat="server" CssClass="btn btn-success btn-xs" OnClick="gvbtnPrint_Click" CausesValidation="false">
                                              <i class="fa fa-print"></i> Print
                                </asp:LinkButton>
                            </div>

                            <h4 class="modal-title" id="myModalLabel">Wholesale Order Detail</h4>

                        </div>
                        <h4 id="myModalLabel1" style="display: none" class="row-title"><i class="typcn typcn-th-small"></i>Wholesale Order Detail</h4>
                        <div class="modal-body paddnone" style="width: auto">
                            <div class="panel-body" id="detailsid" runat="server">
                                <div class="formainline" style="width: auto">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered table-striped">
                                                <tr>
                                                    <td width="200px" valign="top">Location
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStockLocation" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Customer
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVendor" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">BOL Received
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBOLReceived" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Reference No
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblManualOrderNo" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Expected Delevery
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblExpectedDelevery" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Notes
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trOrderItem" runat="server">
                                                    <td colspan="2">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered table-striped">
                                                            <tr class="graybgarea">
                                                                <td colspan="3">
                                                                    <h4>Wholesale Order Item Detail</h4>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Stock Category</b></td>
                                                                <td><b>Quantity</b></td>
                                                                <td><b>Stock Item</b></td>
                                                            </tr>
                                                            <asp:Repeater ID="rptOrder" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%#Eval("WholesaleOrderItem") %></td>
                                                                        <td><%#Eval("OrderQuantity") %></td>
                                                                        <td><%#Eval("StockItem") %></td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupdeliver" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divdeliver" TargetControlID="btnNull1"
                CancelControlID="btnCancel1">
            </cc1:ModalPopupExtender>
            <div id="divdeliver" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="hndid" runat="server" />
                <div class="modal-dialog" style="width: 300px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancel1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H1">Delivery Date</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">

                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label25" runat="server" class="control-label ">
                                                <strong>Delivery Date</strong></asp:Label>
                                        </span>
                                        <div class="input-group date datetimepicker1 col-sm-12">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtdatereceived" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label22" runat="server" class="control-label ">
                                                <strong>Serial No.</strong></asp:Label>
                                        </span>
                                        <span class="">
                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block; padding: 0; margin-top: 5px; margin-bottom: 5px;" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                                                ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                            <%--<div class="col-sm-12" style="padding:0; margin-top:5px;">                            
                                            <asp:TextBox ID="txtserialno" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>--%>
                                        </span>
                                    </div>



                                    <div class="form-group marginleft col-sm-12" style="text-align: center; margin-bottom: 0;">
                                        <asp:Button ID="btndeliver" runat="server" Text="Save" OnClick="btndeliver_Click"
                                            CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="adddate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderStock" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divStock" TargetControlID="btnNULLStock"
                CancelControlID="btnCancelStock">
            </cc1:ModalPopupExtender>
            <div id="divStock" class="modal_popup" runat="server" style="display: none">
                <div class="modal-dialog" style="width: 800px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancelStock" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H3">Add Stock Item</h4>
                        </div>

                        <div class="modal-body paddnone">
                            <div class="panel-body" style="min-height: 500px; overflow: auto;">
                                <div class="form-horizontal">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label12" runat="server" class="col-sm-3 control-label">
                                                <strong>Stock&nbsp;Category</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                </asp:DropDownList>

                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                    ControlToValidate="ddlstockcategory" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label13" runat="server" class="col-sm-3 control-label">
                                                <strong>Stock&nbsp;Item</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtstockitem" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label16" runat="server" class="col-sm-3 control-label">
                                                <strong>Brand</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtbrand" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label2" runat="server" class="col-sm-3 control-label">
                                                <strong> Stock&nbsp;Model</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtmodel" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label17" runat="server" class="col-sm-3 control-label">
                                                <strong> Stock&nbsp;Series</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtseries" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label18" runat="server" class="col-sm-3 control-label">
                                                <strong>Stock&nbsp;Size</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                    ControlToValidate="txtStockSize" Display="Dynamic" ErrorMessage="Enter valid digit"
                                                    ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtStockSize" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label5" runat="server" class="col-sm-3 control-label">
                                                <strong>Min.&nbsp;Stock</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                    ErrorMessage="Enter valid digit" ControlToValidate="txtminstock" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtminstock" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group checkareanew" id="sales" runat="server" visible="false">
                                            <div class="col-md-4 rightalign">
                                                <asp:Label ID="Label19" runat="server" class=" control-label">
                                                <strong>Sales&nbsp;Tag</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6 checkbox-info checkbox">
                                                <asp:CheckBox ID="chksalestag" runat="server" />
                                                <label for="<%=chksalestag.ClientID %>">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="clear"></div>
                                        </div>

                                        <div class="form-group checkareanew">
                                            <div class="col-md-4 rightalign ">
                                                <asp:Label ID="Label11" runat="server" class=" control-label">
                                                <strong>Is Active?</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="<%=chkisactive.ClientID %>">
                                                    <asp:CheckBox ID="chkisactive" runat="server" />
                                                    <span class="text">&nbsp;</span>
                                                </label>

                                            </div>
                                            <div class="clear"></div>
                                        </div>


                                        <div class="form-group checkareanew">
                                            <div class="col-md-4 rightalign">
                                                <asp:Label ID="Label20" runat="server" class=" control-label">
                                                <strong>Is&nbsp;Dashboard?</strong></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="<%=chkDashboard.ClientID %>">
                                                    <asp:CheckBox ID="chkDashboard" runat="server" />
                                                    <span class="text">&nbsp;</span>
                                                </label>



                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <span class="name form-group">

                                                    <label class="control-label">
                                                        <strong>Stock&nbsp;Location </strong>
                                                    </label>

                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Stock&nbsp;Quantity</strong>
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        <strong>Sales&nbsp;Tag</strong>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                            <ItemTemplate>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <span>
                                                                        <asp:HiddenField ID="hyplocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                        <asp:HiddenField ID="hndlocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                        <asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <span>
                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                            ControlToValidate="txtqty" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3 right-text checkbox-info checkbox ">
                                                                    <div class="thkmartop">
                                                                        <label for="<%# Container.FindControl("chksalestagrep").ClientID %>">
                                                                            <asp:CheckBox ID="chksalestagrep" runat="server" />
                                                                            <span class="text">&nbsp;</span>
                                                                        </label>



                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                        <div id="Div4" class="form-group" runat="server" visible="false">
                                            <span class="name">
                                                <label class="control-label">
                                                    Description</label>
                                            </span><span>
                                                <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                    Width="100%" Style="resize: none;">
                                                </asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <%--<div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Is&nbsp;Dashboard?</label>
                                                            </span><span>
                                                                <asp:CheckBox ID="chkDashboard" runat="server" />

                                                                <label for="<%=chkDashboard.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>--%>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group ">
                                        <div class="col-sm-8 col-sm-offset-4">
                                            <asp:Button ID="ibtnAddStock" runat="server" Text="Add" OnClick="ibtnAddStock_Click"
                                                CssClass="btn btn-primary savewhiteicon" ValidationGroup="stock" />

                                        </div>
                                    </div>
                                </div>
                                <%--   <div class="formainline">

                                    <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label12" runat="server" class="col-sm-6 control-label"> Stock&nbsp;Category</asp:Label>

                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlstockcategory" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label13" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Item</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtstockitem" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label16" runat="server" class="col-sm-2 control-label">
                                                                            Brand</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtbrand" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label17" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Model</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtmodel" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label18" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Series</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtseries" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label19" runat="server" class="col-sm-2 control-label">
                                                                            Stock&nbsp;Size</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^[0-9]*$" ValidationGroup="stock"
                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtStockSize" Display="Dynamic"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtStockSize" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label20" runat="server" class="col-sm-2 control-label">
                                                                             Min.&nbsp;Stock</asp:Label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="comperror" ValidationGroup="stock"
                                                            ControlToValidate="txtminstock" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^[0-9]*$"
                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtminstock" ValidationGroup="stock"></asp:RegularExpressionValidator>

                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-4 rightalign">

                                                        <label for="<%=chksalestag.ClientID %>">
                                                            <asp:CheckBox ID="chksalestag" runat="server" />
                                                            <span class="text">Sales&nbsp;Tag</span>
                                                        </label>


                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-4 rightalign">

                                                        <label for="<%=chkisactive.ClientID %>">
                                                            <asp:CheckBox ID="chkisactive" runat="server" />
                                                            <span class="text">Is&nbsp;Active?</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Stock&nbsp;Location
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                &nbsp;&nbsp;   Stock&nbsp;Quantity
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                                            <ItemTemplate>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <span>
                                                                                        <asp:HiddenField ID="hyplocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                                        <asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <span>
                                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass=""
                                                                                            ControlToValidate="txtqty" Display="Dynamic" ValidationGroup="stock"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtqty" ValidationGroup="stock" Display="Dynamic"></asp:RegularExpressionValidator>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>



                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label class="control-label">
                                                            Description</label>

                                                        <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                            Width="100%" Style="resize: none;">
                                                        </asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="form-group row checkareanew">
                                                    <div class="col-sm-12">

                                                        <label for="<%=chkDashboard.ClientID %>">
                                                            <asp:CheckBox ID="chkDashboard" runat="server" />
                                                            <span class="text">Is&nbsp;Dashboard?</span>
                                                        </label>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="row" style="text-align: center">
                                            <div class="form-group marginleft">
                                                <asp:Button ID="ibtnAddStock" runat="server" Text="Add" OnClick="ibtnAddStock_Click"
                                                    CssClass="btn btn-primary savewhiteicon" ValidationGroup="stock" />
                                            </div>
                                        </div>
                                    </div>

                                </div>--%>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="Button1" Style="display: none;" runat="server" />


                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                        DropShadow="false" PopupControlID="divVendor" TargetControlID="btnNullVndr"
                        CancelControlID="btnCancelVndr">
                    </cc1:ModalPopupExtender>
                    <div id="div1" runat="server" style="display: none;" class="modal_popup">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">
                                    <div style="float: right">
                                        <button id="Button2" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                                    </div>
                                    <h4 class="modal-title" id="H2">Add New Vendor</h4>
                                </div>
                                <div class="modal-body paddnone">
                                    <div class="panel-body">
                                        <div class="formainline">

                                            <div class="form-group col-md-12">
                                                <asp:Label ID="Label14" runat="server" class="col-sm-4 control-label">
                                                <strong>Customer</strong></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="TextBox1" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required."
                                                ControlToValidate="txtCompany" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                                    --%>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:Label ID="Label15" runat="server" class="col-sm-4 control-label">
                                                <strong>Name:</strong></asp:Label>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="TextBox2" runat="server" placeholder="First Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="" CssClass=""
                                                                ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="TextBox3" runat="server" placeholder="Last Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group center-text col-sm-12">
                                                <asp:Button ID="Button3" runat="server" Text="Add" OnClick="ibtnaddvendor_click"
                                                    CssClass="btn btn-primary savewhiteicon" ValidationGroup="vendor" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="Button4" Style="display: none;" runat="server" />
                    <!--Danger Modal Templates-->
                    <asp:Button ID="Button5" Style="display: none;" runat="server" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                        PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
                    </cc1:ModalPopupExtender>
                    <div id="Div2" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                        <div class="modal-dialog " style="margin-top: -300px">
                            <div class=" modal-content ">
                                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                                <div class="modal-header text-center">
                                    <i class="glyphicon glyphicon-fire"></i>
                                </div>


                                <div class="modal-title">Delete</div>
                                <label id="Label21" runat="server"></label>
                                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                                <div class="modal-footer " style="text-align: center">
                                    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton6" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>

                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <!--End Danger Modal Templates-->
                </div>
            </div>
            <asp:Button ID="btnNULLStock" Style="display: none;" runat="server" />


            <cc1:ModalPopupExtender ID="ModalPopupExtenderVendor" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divVendor" TargetControlID="btnNullVndr"
                CancelControlID="btnCancelVndr">
            </cc1:ModalPopupExtender>
            <div id="divVendor" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancelVndr" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H2">Add New Vendor</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="Label9" runat="server" class="col-sm-4 control-label">
                                                <strong>Customer</strong></asp:Label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required."
                                                ControlToValidate="txtCompany" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                            --%>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="Label10" runat="server" class="col-sm-4 control-label">
                                                <strong>Name:</strong></asp:Label>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContFirst" runat="server" placeholder="First Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContLast" runat="server" placeholder="Last Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group center-text col-sm-12">
                                        <asp:Button ID="ibtnAddVendor" runat="server" Text="Add" OnClick="ibtnaddvendor_click"
                                            CssClass="btn btn-primary savewhiteicon" ValidationGroup="vendor" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNullVndr" Style="display: none;" runat="server" />
            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Delete</div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />
            <asp:PostBackTrigger ControlID="btndeliver" />
            <asp:PostBackTrigger ControlID="gvbtnPrint" />


        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
