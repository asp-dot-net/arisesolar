<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="stocktransfer.aspx.cs" Inherits="admin_adminfiles_stock_stocktransfer"
    EnableEventValidation="false" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h4 id="myModalLabel1" style="display: none" class="row-title"><i class="typcn typcn-th-small"></i>Stock Order Detail</h4>
            <div class="page-body headertopbox printorder">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Transfer
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
                <div id="hbreadcrumb" class="pull-right ">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="lnkAdd" runat="server" Visible="false" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" Visible="false" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
                </div>
            </div>

            <script>


                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {

                    $(".myvalstocktransfer").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                }
            </script>
            <script>


                function doMyAction() {

                    $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();
                    });
                }

            </script>
            <script>
                function printContent() {
                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();
                }

            </script>

            <script>
                function ComfirmDelete(event, ctl) {
                    event.preventDefault();
                    var defaultAction = $(ctl).prop("href");

                    swal({
                        title: "Are you sure you want to delete this Record?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },

                      function (isConfirm) {
                          if (isConfirm) {
                              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              eval(defaultAction);

                              //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              return true;
                          } else {
                              // swal("Cancelled", "Your imaginary file is safe :)", "error");
                              return false;
                          }
                      });
                }
            </script>

            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {


                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {


                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $("[data-toggle=tooltip]").tooltip();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                }
            </script>
            <div class="supreme-container printorder">
                <div class="finaladdupdate">
                    <div id="PanAddUpdate" runat="server" visible="false">
                        <div class="panel-body animate-panel padtopzero">
                            <%--<div>
                <div class="row">
                    <div class="">--%>
                            <%-- <a href="../../css/print.css"></a>
                         <a href="../../../css/style.css"></a>--%>
                            <%-- <div class="col-lg-12">--%>
                            <%-- <div class="hpanel marbtmzero">--%>
                            <%-- <div class="panel-heading">--%>


                            <div class="well with-header  addform">
                                <div class="header bordered-blue">
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Stock Transfer
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">

                                                <asp:Label ID="Label4" runat="server" class="col-sm-12 ">
                                                <strong>Transfered&nbsp;Date</strong></asp:Label>

                                                <div class="col-sm-10">
                                                    <div class="amtvaluebox" style="width: 257px;">
                                                        <asp:Literal ID="ltTransfered" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" class="col-sm-12 ">
                                                <strong>  Transfer&nbsp;From</strong></asp:Label>
                                                <div class="col-sm-10">
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddltransferfrom" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="true" OnSelectedIndexChanged="ddltransferfrom_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddltransferfrom" Display="Dynamic"></asp:RequiredFieldValidator>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <b>
                                                    <asp:Label ID="lblLastName" runat="server" class="col-sm-12" Text="Entered By"></asp:Label></b>
                                                <%--  <asp:Label ID="lblLastName" runat="server" class="col-sm-12">
                                                <strong><asp:Label ID="lblname" runat="server"></asp:Label></strong></asp:Label>--%>
                                                <div class="col-sm-10">
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlemployee" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlemployee" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" class="col-sm-12">
                                                <strong> Transfer&nbsp;To</strong></asp:Label>
                                                <div class="col-sm-10">
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddltransferto" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddltransferto" Display="Dynamic"></asp:RequiredFieldValidator>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4 col-sm-4">

                                            <div class="form-group">
                                                <span class="name col-sm-12">
                                                    <label>
                                                        <strong>Tracking&nbsp;No.</strong>

                                                    </label>
                                                </span><span class="col-sm-10" style="max-width: 288px;">
                                                    <span class="">
                                                        <asp:TextBox ID="txttracking" runat="server" MaxLength="200" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txttracking_TextChanged"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" ControlToValidate="txttracking"
                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </span>
                                                </span>
                                            </div>
                                            <%--<div class="form-group">
                                                <asp:Label ID="Label4" runat="server" class="col-sm-12">
                                                <strong></strong></asp:Label>
                                                <div class="col-sm-12">
                                                  
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="Label23" runat="server" class="col-sm-12">
                                                <strong>Date&nbsp;Received</strong></asp:Label>
                                                <div class="col-sm-12">
                                                    <div style="max-width: 256px;">
                                                        <div class="input-group date datetimepicker1 col-sm-12">

                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtdatereceived" runat="server" class="form-control" placeholder="Date Received">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <asp:Label ID="Label5" runat="server" class="col-sm-12">
                                                <strong>Notes</strong></asp:Label>
                                                <div class="col-sm-12" style="max-width: 1233px">
                                                    <asp:TextBox ID="txtnote" runat="server" MaxLength="200" class="form-control modaltextbox" Width="100%"
                                                        Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtnote"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group col-md-7">
                                                <table width="100%" class="table tablebrd table-bordered table-striped gridcss spcaldate">
                                                    <thead>
                                                        <tr class="gridheader">
                                                            <th style="width: 120px!important;">Category</th>
                                                            <th>Item</th>
                                                            <th style="width: 100px!important;">Qty</th>
                                                            <th style="width: 100px!important;">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <asp:Repeater ID="rptstock" runat="server" OnItemDataBound="rptstock_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type")%>' />
                                                                    <asp:HiddenField ID="hndid" runat="server" Value='<%#Eval("StockTransferItemsID") %>' />
                                                                    <asp:HiddenField ID="hndcategory" runat="server" Value='<%#Eval("StockCategoryID") %>' />
                                                                    <div class="drpValidate">
                                                                        <asp:DropDownList ID="ddlcategory" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged"
                                                                            AppendDataBoundItems="true">
                                                                            <asp:ListItem Value="">Select Category</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                                            ControlToValidate="ddlcategory" Display="None" ValidationGroup="transfer"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <asp:HiddenField ID="hdnstockcode" runat="server" Value='<%#Eval("StockCode") %>' />
                                                                    <div class="drpValidate">
                                                                        <asp:DropDownList ID="ddlitem" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                            AppendDataBoundItems="true" Style="width: 60%!important;">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage=""
                                                                            ControlToValidate="ddlitem" Display="None" ValidationGroup="transfer"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <asp:HiddenField ID="hdnQty" runat="server" Value='<%#Eval("TransferQty") %>' />
                                                                    <asp:TextBox ID="txtqty" runat="server" CssClass="form-control modaltextbox" MaxLength="7"></asp:TextBox>

                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                                        ErrorMessage="Enter valid digit" ControlToValidate="txtqty"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                                        ControlToValidate="txtqty" Display="None" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="lbremove" runat="server" OnClientClick="return confirm('Are you sure you want to delete This Record?  ');" CausesValidation="false" data-placement="top" OnClick="lbremove_Click"
                                                                        data-toggle="tooltip" data-original-title="Delete" ImageUrl="~/admin/images/icon_delet.png"></asp:ImageButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <tr>
                                                        <td colspan="8" align="right">
                                                            <asp:Button ID="btnAddRow" runat="server" Text="Add" OnClick="btnAddBoxRow_Click" CssClass="btn btn-primary redreq btnaddicon"
                                                                CausesValidation="false" /></td>
                                                    </tr>
                                                </table>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-7 col-sm-offset-2">
                                                <asp:Button CssClass="btn btn-default purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                    Text="Add" />
                                                <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" CausesValidation="true" OnClick="btnUpdate_Click"
                                                    Text="Save" Visible="false" />
                                                <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                    CausesValidation="false" Text="Reset" />
                                                <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%-- </div>--%>
                            <%--</div>--%>
                            <%--</div>--%>
                            <%-- </div>
                </div>--%>
                        </div>
                    </div>
                </div>
                <div class="page-body padtopzero printorder">
                    <asp:Panel runat="server" ID="PanGridSearch">
                        <div class=" animate-panel" style="padding-bottom: 0px!important;">
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                        Text="Transaction Failed."></asp:Label></strong>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                                </div>
                                <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                                <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this Tracking Number already exists.</strong>
                                </div>
                            </div>
                            <div class="finalheader">
                                <div class="animate-panel">
                                    <div class="searchfinal">
                                        <div class="widget-body shadownone brdrgray">
                                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="dataTables_filter Responsive-search">
                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="inlineblock martop5">
                                                                    <div>

                                                                        <div class="input-group col-sm-1" style="width: 200px;">
                                                                            <asp:DropDownList ID="ddlby" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                                                <asp:ListItem Value="">Select By</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" style="width: 175px;">
                                                                            <asp:DropDownList ID="ddlsearchtransferfrom" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                                                <asp:ListItem Value="">Location From</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" style="width: 175px;">
                                                                            <asp:DropDownList ID="ddlsearchtransferto" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                                                <asp:ListItem Value="">Location To</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group col-sm-1">
                                                                            <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                                                <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                                <asp:ListItem Value="1">Received Date</asp:ListItem>
                                                                                <asp:ListItem Value="2">Transfer Date</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker1  col-sm-2">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="padding-top: 5px">
                                                            <div class="col-md-12">
                                                                <div class="input-group col-sm-1">
                                                                    <asp:DropDownList ID="ddlReceived" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalstocktransfer">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                        <asp:ListItem Value="0">Not Received</asp:ListItem>
                                                                        <asp:ListItem Value="1">Received</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group">
                                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                </div>
                                            </div>
                                            <div class="datashowbox">
                                                <div class="row">
                                                    <div class="dataTables_length showdata col-sm-8">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>


                    <div class="finalgrid">
                        <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                            <div class="xscroll">
                                <div id="PanGrid" runat="server">
                                    <div>
                                        <div class="table-responsive">
                                            <asp:GridView ID="GridView1" DataKeyNames="StockTransferID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" OnRowDataBound="GridView1_RowDataBound" AllowSorting="false" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Transfer Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="TransferDate" HeaderStyle-CssClass="center-text">
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem, "TransferDate", "{0:dd MMM yyyy}") %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="150px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tsfr No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="StockTransferNumber" HeaderStyle-CssClass="center-text">
                                                        <ItemTemplate>

                                                            <%#Eval("StockTransferNumber")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Transfer From" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="LocationFromName">
                                                        <ItemTemplate>
                                                            <%#Eval("LocationFromName")%>
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="brdnoneleft" Width="130px" />
                                                        <HeaderStyle CssClass="brdnoneleft" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Transfer To" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="110px" SortExpression="LocationToName">
                                                        <ItemTemplate>
                                                            <%#Eval("LocationToName")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sent By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="TransferByName">
                                                        <ItemTemplate>
                                                            <%#Eval("TransferByName")%>
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="brdnoneleft" />
                                                        <HeaderStyle CssClass="brdnoneleft" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tracking No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px" SortExpression="TrackingNumber" HeaderStyle-CssClass="center-text">
                                                        <ItemTemplate>
                                                            <%#Eval("TrackingNumber")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Received" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" SortExpression="ReceivedDate" HeaderStyle-CssClass="center-text">
                                                        <ItemTemplate>

                                                            <asp:Literal runat="server" ID="ltrecdt" Text='<%# DataBinder.Eval(Container.DataItem, "ReceivedDate", "{0:dd MMM yyyy}")%>' Visible='<%#Eval("Received").ToString()=="True"?true:false %>'></asp:Literal>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Received By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" SortExpression="ReceivedDate">
                                                        <ItemTemplate>

                                                            <asp:Literal runat="server" ID="ltrecdt1" Text='<%# Eval( "TransferByName1")%>' Visible='<%#Eval("Received").ToString()=="True"?true:false %>'></asp:Literal>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Item" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hyptransferfrom" runat="server" NavigateUrl='<%# "~/admin/adminfiles/stock/additemqty.aspx?StockTransferID="+Eval("StockTransferID") %>'
                                                                data-toggle="tooltip" data-placement="top" data-original-title="New Item">
                                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/addinvoice.png" />
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <asp:HiddenField Value='<%#Eval("StockTransferID") %>' runat="server" ID="hdnstockid" />
                                                            <%--Visible='<%#Eval("Received").ToString()=="False"?true:false %>'--%>
                                                            <asp:LinkButton ID="lnkReceived" CausesValidation="false" CssClass="btn btn-yellow btn-xs" Visible="false"  runat="server" OnClick="lnkReceived_Click" data-toggle="tooltip" data-placement="top" data-original-title="Received">
                                                         <i class=" fa fa-tasks"></i>  Received     </asp:LinkButton>
                                                           <%-- <asp:LinkButton ID="lnkrevert" CausesValidation="false" Visible='<%#Eval("Received").ToString()=="True"?true:false %>' CssClass="btn btn-maroon btn-xs wdth100" runat="server" OnClick="lnkrevert_Click" data-toggle="tooltip" data-placement="top" data-original-title="Revert">
                                                       <i class="fa fa-retweet"></i> Revert      
                                                            </asp:LinkButton>--%>
                                                            <%-- <asp:Image ID="Image31" runat="server" ImageUrl="~/images/recieved.png" Height="25px" />--%>

                                                            <asp:LinkButton ID="lbtnTsfrNo" runat="server" CommandArgument='<%# Eval("StockTransferNumber") %>' CommandName="print"
                                                                data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn btn-primary btn-xs">
                                                                <i class="fa fa-link"></i> Detail
                                                               <%-- <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icon_detail.png" />--%>
                                                            </asp:LinkButton>


                                                            <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" Visible='<%#Eval("Received").ToString()=="False"?true:false%>'
                                                                CssClass="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i>Edit</asp:LinkButton>

                                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("StockTransferID")%>' CssClass="btn btn-danger btn-xs" CausesValidation="false"><i class="fa fa-trash"></i>Delete
                                                            </asp:LinkButton>



                                                            <asp:LinkButton ID="gvbtnPrint" runat="server" CssClass="btn btn-success btn-xs" CommandName="printpage" CommandArgument='<%#Eval("StockTransferID")%>' OnClick="gvbtnPrint_Click" CausesValidation="false" data-original-title="Print" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-print"></i> Print
                                                            </asp:LinkButton>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>




                                                </Columns>
                                                <AlternatingRowStyle />
                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <div class="pagination">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                        <div class="paginationnew1" runat="server" id="divnopage">
                                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>
                    </div>
                </div>

            </div>




        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="GridView1" />

        </Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnNULLData3" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderTracker" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="divEmailCheck" DropShadow="false" CancelControlID="ibtnCancelActive" OkControlID="btnOKMobile" TargetControlID="btnNULLData3">
    </cc1:ModalPopupExtender>
    <div id="divEmailCheck" runat="server" style="display: none" class="modal_popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div style="float: right">
                        <asp:LinkButton ID="ibtnCancelActive" CausesValidation="false" Visible="false"
                            runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="ibtnCancelActive_Click">
                   Close
                        </asp:LinkButton>
                    </div>
                    <h4 class="modal-title center-text" id="H2">Project Status</h4>
                </div>
                <%--     <asp:Button ID="ibtnCancel" runat="server" OnClick="ibtnCancel_Click1" />close--%>
                <%--  <button id="ibtnCancel" runat="server" onclick="" type="button" class="close" data-dismiss="modal" causesvalidation="false"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span> </button>--%>

                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        <h3 class="noline"><b>There is a Stock in the database already who has this tracking number.
                                        </b></h3>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <asp:Button ID="btnDupeMobile" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeMobile_Onclick"
                                            Text="Dupe" CausesValidation="false" />
                                        <asp:Button ID="btnDupeNotMobile" runat="server" OnClick="btnNotDupeMobile_Onclick"
                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                        <asp:Button ID="btnOKMobile" Style="display: none; visible: false;" runat="server"
                                            CssClass="btn" Text=" OK " />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="tablescrolldiv">
                            <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                <asp:GridView ID="GridView2" DataKeyNames="StockTransferID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Transfer Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="TransferDate">
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container.DataItem, "TransferDate", "{0:dd MMM yyyy}") %>
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tsfr No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="StockTransferNumber">
                                            <ItemTemplate>
                                                <%#Eval("StockTransferNumber")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="80px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transfer From" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="LocationFromName">
                                            <ItemTemplate>
                                                <%#Eval("LocationFromName")%>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="brdnoneleft" Width="150px" />
                                            <HeaderStyle CssClass="brdnoneleft" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transfer To" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px" SortExpression="LocationToName">
                                            <ItemTemplate>
                                                <%#Eval("LocationToName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <asp:Button ID="Button2" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="btnNo" DropShadow="false" PopupControlID="divstockdetail" TargetControlID="Button2">
    </cc1:ModalPopupExtender>
    <div id="divstockdetail" runat="server" style="display: none;" class="modal_popup">
        <div class="modal-dialog" style="max-width: 300px;">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div style="float: right">
                        <asp:LinkButton ID="LinkButton1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="Button3_Click">
                        Close
                        </asp:LinkButton>
                    </div>
                    <h4 class="modal-title" id="H1" runat="server">
                        <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                        Stock Transfer Details</h4>

                </div>
                <div class="modal-body paddnone" runat="server" id="divdetail" visible="false">
                    <div class="panel-body">
                        <div class="formainline">
                            <div class="">
                                <div class="" style="background: none!important;">
                                    <div>
                                        <div class="col-md-12">
                                            <div class="form-group wd100">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                        <thead>
                                                            <tr>
                                                                <td style="width: 70%">Stock Item
                                                                </td>
                                                                <td style="width: 20%" class="center-text">Transfer Qty
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <asp:Repeater ID="rptstockdetail" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 70%;">
                                                                        <%#Eval("StockItem")%>
                                                                    </td>
                                                                    <td style="width: 20%" class="center-text">
                                                                        <%#Eval("TransferQty")%>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group marginleft text-center col-md-12">
                                            <asp:Button class="btn btn-primary addwhiteicon" ID="btnYes" runat="server" OnClick="btnYes_Click" Text="Yes" Visible="false" />
                                            <asp:Button class="btn btn-primary addwhiteicon" ID="btnReturn" runat="server" OnClick="btnReturn_Click" Text="Return" Visible="false" />
                                            <asp:Button class="btn btn-primary addwhiteicon" ID="btntansite" runat="server" OnClick="btntansite_Click" Text="Transite" Visible="false" />
                                            <asp:Button class="btn btn-primary addwhiteicon" ID="btnNo" runat="server" OnClick="btnNo_Click" Text="No" CausesValidation="false" Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body paddnone" runat="server" id="divdetailmsg" visible="false">
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="formainline">
                            <div class="panel panel-default">
                                <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>

                                                    <div class="messesgarea">
                                                        <div class="alert alert-info" id="Div1" runat="server">
                                                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Danger Modal Templates-->
    <asp:Button ID="btndelete" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="modal_danger" DropShadow="false" CancelControlID="Button4" OkControlID="btnOKMobile" TargetControlID="btndelete">
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

        <div class="modal-dialog " style="margin-top: -300px">
            <div class=" modal-content ">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>


                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server"></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align: center">
                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                    <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                </div>
            </div>
        </div>

    </div>

    <asp:HiddenField ID="hdndelete" runat="server" />
    <!--End Danger Modal Templates-->
    <asp:HiddenField runat="server" ID="hdnstocktransferid" />


    <cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
        CancelControlID="btnCancelDetail">
    </cc1:ModalPopupExtender>
    <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line printorder "></div>
                <div class="modal-header printorder">
                    <div style="float: right">

                        <asp:LinkButton ID="LinkButton5" runat="server" type="button" CssClass="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                        Close
                        </asp:LinkButton>


                        <asp:LinkButton ID="gvbtnPrint" runat="server" CssClass="btn btn-success btn-xs" OnClientClick="javascript:printContent();" CausesValidation="false">
                                              <i class="fa fa-print"></i> Print
                        </asp:LinkButton>
                    </div>

                    <h4 class="modal-title" id="myModalLabel">Stock Order Detail</h4>

                </div>

                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="bodymianbg">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-12">

                                        <table class="table table-bordered table-striped printarea">
                                            <tr>
                                                <th align="left" width="100">From&nbsp;Location:&nbsp;</th>
                                                <td align="left">
                                                    <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
                                                <th align="left" width="100">To&nbsp;Location:&nbsp;</th>
                                                <td align="left">
                                                    <asp:Label ID="lblTo" runat="server"></asp:Label></td>
                                            </tr>

                                            <tr>
                                                <th align="left" width="100">Transfer Date: </th>
                                                <td align="left">
                                                    <asp:Label ID="lblTransferDate" runat="server"></asp:Label></td>
                                                <th align="left" width="100">Transfer By: </th>
                                                <td align="left">
                                                    <asp:Label ID="lblTransferBy" runat="server"></asp:Label></td>
                                            </tr>

                                        </table>
                                        <br />

                                        <div class="qty marbmt25">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="stockitem">
                                                <tr>
                                                    <th width="5%" align="center">Qty</th>
                                                    <th width="20%" align="left">Code</th>
                                                    <th align="left">Stock Item</th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="lblQty" runat="server"><%#Eval("TransferQty") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCode" runat="server"><%#Eval("StockCode") %></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                        <br />

                                        <div>
                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="printarea">
                                                <tr>
                                                    <th>Total Qty :
                                                    </th>
                                                    <td colspan="3">
                                                        <asp:Label ID="lbltotalqty" runat="server"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th width="20%">Tracking No :
                                                    </th>
                                                    <td width="40%">
                                                        <asp:Label ID="lblTrackingNo" runat="server"></asp:Label></td>
                                                    <th width="20%">Received Date :</th>
                                                    <td>
                                                        <asp:Label ID="lblReceivedDate" runat="server"></asp:Label></td>

                                                </tr>
                                                <tr>
                                                    <th colspan="2">Notes :
                                                    </th>


                                                    <th>Received By :</th>
                                                    <td>
                                                        <asp:Label ID="lblReceivedBy" runat="server"></asp:Label></td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="lblNotes" runat="server"></asp:Label></td>

                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
</asp:Content>
