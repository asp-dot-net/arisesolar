<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="gridconnectiontracker.aspx.cs" Inherits="admin_adminfiles_master_gridconnectiontracker" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
    <style type="text/css">
        .selected_row {
            background-color: #A1DCF2 !important;
        }
    </style>

    <script type="text/javascript">


        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>GridConnection Tracker</h5>
            </div>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                prm.add_endRequest(endrequesthandler);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.

                function pageLoaded() {


                }

                function endrequesthandler(sender, args) {
                    callMultiCheckbox();
                }
            </script>


            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');

                    $("[data-toggle=tooltip]").tooltip();
                    if (args.get_error() != undefined) {
                        args.set_errorhandled(true);
                    }
                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });


                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });

                    callMultiCheckbox();
                }

                function pageLoaded() {

                    //alert($(".search-select").attr("class"));
                    $('.loading-container').addClass('loading-inactive');
                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                    $("[data-toggle=tooltip]").tooltip();
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }
                    //gridviewScroll();

                    callMultiCheckbox();
                }
            </script>


            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch" DefaultButton="btnSearch">
                    <div class="animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>




                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel runat="server" ID="PanelCreateEdit" DefaultButton="btnSearch">
                                        <div class="row">
                                            <div class="inlineblock">
                                                <div class="col-sm-12">

                                                    <div class="input-group col-sm-1">
                                                        <asp:TextBox ID="txtproject" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtproject"
                                                            WatermarkText="Project No" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtproject" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                            ControlToValidate="txtproject" Display="Dynamic" ErrorMessage="Please enter a number"
                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                    </div>

                                                    <div class="form-group spical multiselect" style="width: 160px">
                                                        <dl class="dropdown">
                                                            <dt>
                                                                <a href="#">
                                                                    <span class="hida" id="spanselect">Select</span>
                                                                    <p class="multiSel"></p>
                                                                </a>
                                                            </dt>
                                                            <dd id="ddproject" runat="server">
                                                                <div class="mutliSelect" id="mutliSelect">
                                                                    <ul>
                                                                        <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                    <%--  <span class="checkbox-info checkbox">--%>
                                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                        <span></span>
                                                                                    </label>
                                                                                    <%-- </span>--%>
                                                                                    <label class="chkval">
                                                                                        <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                    </label>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                    <div class="form-group" style="width: 100px">


                                                        <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddlEmailFlag" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Email Flag</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0" >No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="form-group gridconnectiontracker">
                                                        <asp:DropDownList ID="ddlmeterreno" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="0">MeterApplyFerNo</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2" Selected="True">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                           <asp:DropDownList ID="ddlcomcerti" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" Visible="false">
                                                            <asp:ListItem Value="0">Compliance Certificate</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="form-group" style="width: 100px">
                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select Installer</asp:ListItem>
                                                           
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="form-group gridconnectiontracker">
                                                        <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" >
                                                            <asp:ListItem Value="">Select Date</asp:ListItem>
                                                            <asp:ListItem Value="1">Install Date</asp:ListItem>
                                                            <asp:ListItem Value="2">Install CompleteDate</asp:ListItem>
                                                        </asp:DropDownList>
                                                        </div>
                                                    <div class="input-group date datetimepicker1 col-sm-1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>


                                                    <div class="input-group date datetimepicker1 col-sm-1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>

                                                    
                                                    
                                                 
                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnClearAll" runat="server"
                                                            CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <%--                    <div class="col-sm-3">
                                            <div class="form-group gridconnectiontracker">
                                                        <asp:DropDownList ID="ddlcomcerti" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="0">Compliance Certificate</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                        </div>--%>

                            <div class="datashowbox">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="dataTables_length showdata">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="padtopzero">
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td >
                                                             
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-9 printorder">
                                            <div id="tdExport" class="pull-right btnexelicon" runat="server">
 <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </span>
                 
                   <%-- <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>--%>
                                                <%--  </ol>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </asp:Panel>
                <div class="finalgrid">

                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer tablegridselect">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="table-responsive printArea">
                                    <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand"
                                        OnDataBound="GridView1_DataBound" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%--   <asp:Label ID="lblProjectNumber" runat="server" Width="80px">--%>
                                                    <%--    <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />--%>
                                                    <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'> <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                    <%--   </asp:Label>--%>
                                                    <asp:HiddenField Value='<%#Eval("ProjectID") %>' runat="server" ID="hdnProjectID" />
                                                    <asp:HiddenField Value='<%#Eval("CustomerID") %>' runat="server" ID="hdncust" />
                                                    <asp:LinkButton ID="lnkjob" runat="server" OnClick="lnkjob_Click" data-toggle="tooltip" data-placement="top" data-original-title="Meter Application" CssClass="btn btn-info btn-xs"><i class="fa fa-edit"></i> Meter</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Project Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                <ItemTemplate>
                                                    <%#Eval("ProjectType")%>
                                                    <%-- <%#Eval("CreateDate","{0: dd MMM yyyy}")%>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                <ItemTemplate>
                                                    <%#Eval("InstallBookingDate","{0: dd MMM yyyy}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Address")%>'
                                                        Width="200px"><%#Eval("Address")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallPostCode" runat="server" Width="50px"><%#Eval("InstallPostCode")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblState" runat="server" Width="50px"><%#Eval("InstallState")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Installer Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInst" runat="server" Width="50px"><%#Eval("InstallerName")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="InstallCompleted Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%#Eval("InstallCompleted","{0: dd MMM yyyy}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProjectStatus" runat="server" Width="120px"><%#Eval("ProjectStatus")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Elec.Retailer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%#Eval("ElecRetailer")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="NMI Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%#Eval("NMINumber")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Approval Ref" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%#Eval("ElecDistApprovelRef")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SystemCapacity" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                <ItemTemplate>
                                                    <%#Eval("SystemCapKW")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MeterAppliedRef" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%#Eval("MeterAppliedRef")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="SendEmail" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="SendEmail" CommandArgument='<%#Eval("ProjectID") %>' CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="SendEmail">
                                                            SendEmail
                                                </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                             <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                           
                                                            <asp:LinkButton ID="gvbtnVerify" runat="server" CssClass="btn btn-danger btn-mini" CommandName="Send Email"
                                                                CommandArgument='<%# Eval("ProjectID") %>' Visible='<%# Eval("EmailFlag").ToString() == "0" ? true : false %>'
                                                                CausesValidation="false"  data-original-title="Not Sent" data-toggle="tooltip" data-placement="top">                                                                          
                                              <i class="btn-label fa fa-close"></i> Email
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="gvbtnVerify1" runat="server" CssClass="btn btn-success btn-mini"
                                                                CommandArgument='<%# Eval("ProjectID") %>' Visible='<%# Eval("EmailFlag").ToString() == "1" ? true : false %>'
                                                                CausesValidation="false" Enabled="false" data-original-title="Mail Sent"  data-toggle="tooltip" data-placement="top">                                                                          
                                              <i class="btn-label fa fa-check"></i> Email
                                                            </asp:LinkButton>
                                                           <%-- <asp:HiddenField ID="hdnIsApproved" runat="server" Value=' <%# Eval("Isaaproved").ToString()%>' />--%>

                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="verticaaline" />
                                                    </asp:TemplateField>
                                        </Columns>
                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>





            </div>



            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="divUpdateStatus"
                OkControlID="btnOKStatus" TargetControlID="btnNULLStatus">
            </cc1:ModalPopupExtender>
            <div id="divUpdateStatus" runat="server" style="display: none;" class="modal_popup">

                <div class="modal-dialog " style="margin-left: -300px">
                    <div class="modal-content  modal-lg" style="width: 900px">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                           Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="H2">
                                <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                Meter Application Details</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">



                                    <div class="row">
                                        <div>

                                            <div class="form-group col-md-3 wdth211">
                                                <span class="name">
                                                    <asp:Label ID="Label1" runat="server" class="control-label">
                                                        Time</asp:Label></span>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtMeterAppliedTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="MaskedEditExtenderST" runat="server" TargetControlID="txtMeterAppliedTime" Mask="99:99:99"
                                                        MessageValidatorTip="true" MaskType="Time">
                                                    </cc1:MaskedEditExtender>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group col-md-3 wdth211">
                                            <span class="name">
                                                <asp:Label ID="Label8" runat="server" class="control-label">
                                                                Meter Apply Ref
                                                </asp:Label>
                                            </span><span>
                                                <asp:TextBox ID="txtMeterAppliedRef" runat="server" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3 wdth211">
                                            <span class="name">
                                                <asp:Label ID="Label9" runat="server" class="control-label">
                                                                Inspector Name
                                                </asp:Label>
                                            </span><span>
                                                <asp:TextBox ID="txtInspectorName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3 wdth211">
                                            <span class="name">
                                                <asp:Label ID="Label5" runat="server" class="control-label">
                                                     Inspection Date</asp:Label></span>
                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtInspectionDate" runat="server" class="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row" id="divAddUpdate" runat="server">
                                        <div class="col-md-12 text-center">
                                            <asp:LinkButton class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                ValidationGroup="postinst" Text="Save" CausesValidation="false" />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />






            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="divAddComment" TargetControlID="btnNULL" CancelControlID="ibtnCancelComment">
            </cc1:ModalPopupExtender>
            <div id="divAddComment" runat="server">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelComment" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal"
                                    OnClick="ibtnCancelComment_Onclick">Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="myModalLabel">Update Refund</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="form-group">
                                        <label>Pay Method&nbsp;</label>
                                        <asp:DropDownList ID="ddlPayMethod" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPayMethod"
                                            ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Paid Date&nbsp;</label>
                                        <span class="dateimg">
                                            <asp:TextBox ID="txtPaidDate" runat="server" Width="100px" CssClass="form-control"></asp:TextBox>
                                            <asp:ImageButton ID="Image20" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                            <cc1:CalendarExtender ID="CalendarExtender20" runat="server" PopupButtonID="Image20"
                                                TargetControlID="txtPaidDate" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                            <asp:RegularExpressionValidator ValidationGroup="AddNotes" ControlToValidate="txtPaidDate" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPaidDate"
                                                ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </span>
                                    </div>
                                    <div class="form-group modaldesc">
                                        <label>Remarks&nbsp;</label>
                                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" Width="300px" Height="60px"></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="reqvaltxtNotesName" runat="server" ControlToValidate="txtRemarks"
                                            ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">&nbsp;</label>
                                        </span>
                                        <asp:Button ID="ibtnAddComment" runat="server" Text="Update" OnClick="ibtnAddComment_Onclick"
                                            ValidationGroup="AddNotes" CssClass="btn btn-primary savewhiteicon" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndMetereProjectId" runat="server" />
            <asp:HiddenField ID="hndRefundID" runat="server" />
            <asp:HiddenField ID="hdnProID" runat="server" />
            <!--<script type="text/javascript">
                $(document).ready(function () {
                    doMyAction();
                });
                function doMyAction() {
                    $(".myvallteamtracker").select2({
                        placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                }
            </script>-->
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.

                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });

                }
            </script>



            <script type="text/javascript">

                $(".dropdown dt a").on('click', function () {
                    $(".dropdown dd ul").slideToggle('fast');

                });

                $(".dropdown dd ul li a").on('click', function () {
                    $(".dropdown dd ul").hide();
                });
                $(document).bind('click', function (e) {
                    var $clicked = $(e.target);
                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                });


                $(document).ready(function () {


                    HighlightControlToValidate();
                    $('#<%=ibtnAddComment.ClientID %>').click(function () {
                        formValidate();
                    });
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });
                });

                function callMultiCheckbox() {
                    var title = "";
                    $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }


                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
                function HighlightControlToValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            $('#' + Page_Validators[i].controltovalidate).blur(function () {
                                var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                                if (validatorctrl != null && !validatorctrl.isvalid) {
                                    $(this).css("border-color", "#FF5F5F");
                                }
                                else {
                                    $(this).css("border-color", "#B5B5B5");
                                }
                            });
                        }
                    }
                }
                function getValidatorUsingControl(controltovalidate) {
                    var length = Page_Validators.length;
                    for (var j = 0; j < length; j++) {
                        if (Page_Validators[j].controltovalidate == controltovalidate) {
                            return Page_Validators[j];
                        }
                    }
                    return null;
                }
            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                    //gridviewScroll();
                });
                $("#nav").on("click", "a", function () {
                    $('#content').animate({ opacity: 0 }, 500, function () {
                        gridviewScroll();
                        $('#content').delay(250).animate({ opacity: 1 }, 500);
                    });
                });
                function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
                }
            </script>



        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="LinkButton5" />

            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
<asp:Button ID="Button6" Style="display: none;" runat="server" />

    <asp:Button ID="btnverify" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderverify" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="modal_verify" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btnverify">
    </cc1:ModalPopupExtender>
    <div id="modal_verify" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
        <asp:HiddenField ID="hdnprojectID1" runat="server" />
        <div class="modal-dialog " style="margin-top: -300px">
            <div class=" modal-content">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header">
                    <h5 class="modal-title fullWidth">Send Email
                              <%--  <div style="float: right">
                                        <asp:LinkButton ID="lnkcancel" runat="server" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></asp:LinkButton>
                                </div>--%>
                    </h5>
                </div>
                <div class="modal-body ">Are You Sure You want to Send Email?</div>
                <div class="modal-footer " style="text-align: center">
                    <asp:Button ID="lnkSendEmail" runat="server" OnClick="lnkSendEmail_Click" class="btn btn-danger POPupLoader" CausesValidation="false" Text="Ok" />
                    <asp:Button ID="Button7" runat="server" class="btn btn-danger" data-dismiss="modal" Text="Cancel" />
                </div>
            </div>
        </div>

    </div>
</asp:Content>
