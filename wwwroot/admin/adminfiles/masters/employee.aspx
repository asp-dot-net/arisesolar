<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="employee.aspx.cs" Inherits="admin_adminfiles_master_employee" EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <script type="text/javascript">

                function SomeValueChanged(obj) {
                    alert(obj.value);
                }
                function myjs() {
                    //alert("sdfdfdsg");
                    var x = "11:11:11";
                    var reason = document.getElementById("<%=txtStartTime.ClientID %>").value;
                    alert(reason);
                    if (reason == "") {
                        document.getElementById("<%=txtStartTime.ClientID %>").value = " ";
                        alert("ewew");
                    }
                }

            </script>
            <script>

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $(".myval").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $(".myvalemployee").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $('.redreq').click(function () {
                        formValidate();
                    });

                }
            </script>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Employee</h5>
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
                </div>
            </div>


            <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="well with-header with-footer addform">
                            <div class="header bordered-blue">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                Employee
                            </div>

                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label">
                                                First</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpFirst" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" ControlToValidate="txtEmpFirst"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label">
                                                Last</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpLast" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" ControlToValidate="txtEmpLast"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" class="col-sm-2 control-label">
                                                Title</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpTitle" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <%--     <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" ControlToValidate="txtEmpTitle"
                                                            Display="Dynamic" ></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label">
                                                Initials</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpInitials" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" ControlToValidate="txtEmpInitials"
                                                            Display="Dynamic" ></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" class="col-sm-2 control-label">
                                                Email</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpEmail" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" ControlToValidate="txtEmpEmail"
                                                            Display="Dynamic" ></asp:RequiredFieldValidator>--%>
                                        <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtEmpEmail"
                                            Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" class="col-sm-2 control-label">
                                                Mobile</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpMobile" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>


                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmpMobile"
                                            Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" ControlToValidate="txtEmpMobile"
                                                            Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" class="col-sm-2 control-label">
                                                Phone</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" ControlToValidate="txtEmpPhone"
                                                            Display="Dynamic" ></asp:RequiredFieldValidator>--%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                            ControlToValidate="txtEmpPhone" Display="Dynamic" ErrorMessage="Number Only"
                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" class="col-sm-2 control-label">
                                                PhoneExtNo</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpExtNo" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ControlToValidate="txtEmpExtNo" Display="Dynamic" ErrorMessage="Number Only"
                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" class="col-sm-2 control-label">
                                                Nic&nbsp;Name&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpNicName" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" ControlToValidate="txtEmpNicName"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" class="col-sm-2 control-label">
                                                Role</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:ListBox ID="lstrole" runat="server" SelectionMode="Multiple" Width="200px" CssClass="myvalemployee"></asp:ListBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" ControlToValidate="lstrole"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" class="col-sm-2 control-label">
                                                SalesTeam</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:ListBox ID="ddlSalesTeamID" runat="server" SelectionMode="Multiple" Width="200px" CssClass="myvalemployee"></asp:ListBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label12" runat="server" class="col-sm-2 control-label">
                                                Team&nbsp;OutDoor</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkLTeamOutDoor.ClientID %>" style="width: 70px;">
                                            <asp:CheckBox ID="chkLTeamOutDoor" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" class="col-sm-2 control-label">
                                                Team&nbsp;Closer</asp:Label>
                                    <div class="col-sm-6">
                                        <label for="<%=chkLTeamCloser.ClientID %>" style="width: 70px;">
                                            <asp:CheckBox ID="chkLTeamCloser" runat="server"></asp:CheckBox>
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <asp:Label ID="Label14" runat="server" class="col-sm-2 control-label">
                                                Emp&nbsp;Type&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlEmpType" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="1">Arise Solar</asp:ListItem>
                                            <asp:ListItem Value="2">Door to Door</asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlEmpType" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <asp:Label ID="Label15" runat="server" class="col-sm-2 control-label">
                                                
Emp Status</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlEmployeeStatusID" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlEmployeeStatusID" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                
                                <div class="form-group ">
                                    <asp:Label ID="Label34" runat="server" class="col-sm-2 control-label">
                                                
Emp Category</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlEmpCategory" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select Category"></asp:ListItem>
                                        </asp:DropDownList>

                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlEmployeeStatusID" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label16" runat="server" class="col-sm-2 control-label">
                                                UserName</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtuname" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtuname"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group" id="password" runat="server" visible="false">
                                    <asp:Label ID="Label17" runat="server" class="col-sm-2 control-label">
                                                Password</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Password has to be 5 characters !"
                                            ControlToValidate="txtpassword" ValidationExpression="^.{5,}$" Display="Dynamic" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" ControlToValidate="txtpassword"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group" id="confpassword" runat="server" visible="false">
                                    <asp:Label ID="Label18" runat="server" class="col-sm-2 control-label">
                                                Confirm&nbsp;Password&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtcpassword" runat="server" TextMode="Password" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:CompareValidator ID="compvalconfirmPassword" runat="server" ControlToCompare="txtpassword"
                                            ControlToValidate="txtcpassword" ErrorMessage="Password MisMatch" SetFocusOnError="True"
                                            Display="Dynamic"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" ControlToValidate="txtcpassword"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <asp:Label ID="Label19" runat="server" class="col-sm-2 control-label">
                                                Location</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlLocation" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label20" runat="server" class="col-sm-2 control-label">
                                                Start&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <%--  <input type="text" runat="server" id="txtStartTime" data-mask="99:99:99" class="form-control" placeholder="00:00:00">--%>
                                        <asp:TextBox ID="txtStartTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtenderST" runat="server" TargetControlID="txtStartTime" Mask="99:99:99"
                                            MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlToValidate="txtStartTime" ControlExtender="MaskedEditExtenderST"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label21" runat="server" class="col-sm-2 control-label">
                                                End&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEndTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>

                                        <cc1:MaskedEditExtender ID="MaskedEditExtenderET" runat="server" TargetControlID="txtEndTime" Mask="99:99:99" MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlToValidate="txtEndTime" ControlExtender="MaskedEditExtenderET"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label22" runat="server" class="col-sm-2 control-label">
                                                Break&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtBreakTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtBreakTime" Mask="99:99:99"
                                            MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlToValidate="txtBreakTime" ControlExtender="MaskedEditExtenderST"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label23" runat="server" class="col-sm-2  control-label">
                                                Date&nbsp;Hired</asp:Label>
                                    <div class="col-sm-6">
                                        <div class="input-group date datetimepicker1 col-sm-6" style="width: 255px">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtHireDate" runat="server" class="form-controlpagel">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label24" runat="server" class="col-sm-2 control-label">
                                                Include&nbsp;in&nbsp;Lists</asp:Label>
                                    <div class="col-sm-6">
                                        <label for="<%=chkInclude.ClientID %>">
                                            <asp:CheckBox ID="chkInclude" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label25" runat="server" class="col-sm-2 control-label">
                                                Active&nbsp;Employee</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkActiveEmp.ClientID %>">
                                            <asp:CheckBox ID="chkActiveEmp" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label26" runat="server" class="col-sm-2 control-label">
                                                Show Excel</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkshowexcel.ClientID %>">
                                            <asp:CheckBox ID="chkshowexcel" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label27" runat="server" class="col-sm-2 control-label">
                                                Info</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpInfo" runat="server" TextMode="MultiLine" Rows="2" Width="500px" class="form-control modaltextbox"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group" id="divtaxfile" runat="server" visible="false">
                                    <asp:Label ID="Label28" runat="server" class="col-sm-2 control-label">
                                                TaxFileNumber</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtTaxFileNumber" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group" id="divempabn" runat="server" visible="false">
                                    <asp:Label ID="Label29" runat="server" class="col-sm-2 control-label">
                                                EmpABN</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpABN" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group" id="divempaccontant" runat="server" visible="false">
                                    <asp:Label ID="Label30" runat="server" class="col-sm-2 control-label">
                                                EmpAccountName</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpAccountName" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group" id="divroster" runat="server" visible="false">
                                    <asp:Label ID="Label31" runat="server" class="col-sm-2 control-label">
                                                On&nbsp;Roster&nbsp;List</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkOnRoster" runat="server" />
                                        <label for="<%=chkOnRoster.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" id="divpayown" runat="server" visible="false">
                                    <asp:Label ID="Label32" runat="server" class="col-sm-2 control-label">
                                                Company&nbsp;Pays&nbsp;Super</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkPaysOwnSuper" runat="server" />
                                        <label for="<%=chkPaysOwnSuper.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group" id="divgstyown" runat="server" visible="false">
                                    <asp:Label ID="Label33" runat="server" class="col-sm-2 control-label">
                                                GST&nbsp;Payment</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkGSTPayment" runat="server"></asp:CheckBox>
                                        <label for="<%=chkGSTPayment.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <asp:Button CssClass="btn redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" />
                                        <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                            Text="Save" Visible="false" />
                                        <asp:Button CssClass="btn btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">

                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-success" id="PanSuccess" runat="server">
                            <i class="icon-ok-sign"></i>&nbsp;Transaction Successful!
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server">
                            <i class="icon-remove-sign"></i>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                            <i class="icon-remove-sign"></i>&nbsp;Record with this name already exists.
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <i class="icon-info-sign"></i>&nbsp;There are no items to show in this view
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">


                                    <table border="0" cellspacing="0" width="100%" style="text-align: right;" cellpadding="0">
                                        <tr>
                                            <td class="left-text dataTables_length showdata padtopzero">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <div class="input-group col-sm-2">
                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Employee Name" CssClass="form-control m-b"></asp:TextBox>
                                                </div>
                                                <div class="input-group col-sm-2">
                                                    <asp:TextBox ID="txtusername" runat="server" placeholder="UserName" CssClass="form-control m-b"></asp:TextBox>

                                                </div>
                                                <div class="input-group col-sm-1">
                                                    <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalemployee">
                                                        <asp:ListItem Value="">Teams</asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2">
                                                    <asp:DropDownList ID="ddlsearchrole" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalemployee">
                                                        <asp:ListItem Value="">Roles</asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="finalgrid">
                            <div class="table-responsive printArea">
                                <div id="PanGrid" runat="server">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GridView1" DataKeyNames="EmployeeID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                            OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnRowCreated="GridView1_RowCreated" OnDataBound="GridView1_DataBound"
                                            AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Employee" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%#Eval("fullname")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Job Title" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%#Eval("JobTitle")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%#Eval("UserName")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Password" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%#Eval("Password")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%# Eval("employeestatusname")%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Roles" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Repeater ID="rptrole" runat="server">
                                                            <ItemTemplate>
                                                                <asp:Label ID="hypLeadDetail" runat="server" data-toggle="tooltip" data-placement="top" title="" data-original-title='<%# Eval("RoleName")%>'>
                                                            <i ></i> <%# Eval("RoleName")%> 
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <SeparatorTemplate>, </SeparatorTemplate>
                                                        </asp:Repeater>

                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sales Team" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hndEmployeeID" Value='<%#Eval("EmployeeID")%>' />
                                                        <asp:Repeater ID="rptTeam" runat="server">


                                                            <ItemTemplate>
                                                                <asp:Label ID="hypLeadDetail" runat="server" data-toggle="tooltip" data-placement="top" title="" data-original-title='<%# Eval("SalesTeam")%> '>
                                                            <i ></i> <%# Eval("SalesTeam")%>  
                                                                </asp:Label>
                                                            </ItemTemplate>

                                                            <%-- <SeparatorTemplate >, </SeparatorTemplate>--%>
                                                        </asp:Repeater>

                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hypLeadDetail" runat="server" data-toggle="tooltip" CssClass="btn btn-primary btn-xs" data-placement="top" title="" data-original-title="Detail"
                                                            NavigateUrl='<%#"~/admin/adminfiles/masters/empdetails.aspx?id=" + Eval("EmployeeId")%>'>
                                                            <i class="fa fa-link"></i> Detail
                                                        </asp:HyperLink>
                                                        <asp:LinkButton ID="lbtnReset" runat="server" CssClass="btn btn-blue btn-xs" CommandName="Reset" data-original-title="Reset Password" data-toggle="tooltip" data-placement="top"
                                                            OnClientClick="return confirm('Are you sure you want to change password?');"
                                                            CommandArgument='<%#Eval("EmployeeID")%>' CausesValidation="false">
                                                    <i class="fa fa-refresh"></i> Reset
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" CssClass="btn btn-info btn-xs" CommandName="Select" CausesValidation="false"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="lbtnLock" runat="server" CommandName="lock" CssClass="btn btn-darkorange btn-xs" CommandArgument='<%#Eval("userid")%>' data-original-title="Lock" data-toggle="tooltip" data-placement="top"
                                                            OnClientClick="return confirm('Are you sure you want to Lock this User?');" Visible='<%# Eval("IsLockedOut").ToString()=="False"?true:false %>'
                                                            CausesValidation="false"><i class="fa fa-lock"></i> Lock....
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnUnLock" runat="server" CommandName="unlock" class="btn  btn-darkorange btn-xs btn-teal tooltips" CommandArgument='<%#Eval("userid")%>'
                                                            OnClientClick="return confirm('Are you sure you want to UnLock this User?');" Visible='<%# Eval("IsLockedOut").ToString()=="True"?true:false %>'
                                                            data-original-title="UnLock" data-toggle="tooltip" data-placement="top" CausesValidation="false"><i class="fa fa-unlock"></i> Unlock
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                </asp:TemplateField>





                                            </Columns>

                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>

                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>

                                    <div class="paginationnew1 xscroll" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpdate" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
