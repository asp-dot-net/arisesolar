﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_master_gridconnectiontracker : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
          
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropDown();
            BindGrid(0);
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Verification")))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            //if (Convert.ToBoolean(st_emp.showexcel) == true)
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}

            if ((Roles.IsUserInRole("Administrator")))
            {
                //tdTeam.Visible = true;
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                //tdsalerep.Visible = false;
            }

        }
    }

    public void BindDropDown()
    {
        //ddlOptionID.DataSource = ClstblRefundOptions.tblRefundOptions_SelectActice();
        //ddlOptionID.DataMember = "OptionName";
        //ddlOptionID.DataTextField = "OptionName";
        //ddlOptionID.DataValueField = "OptionID";
        //ddlOptionID.DataBind();
        ListItem item2 = new ListItem();
        item2.Text = "Installer";
        item2.Value = "";
        ddlInstaller.Items.Clear();
        ddlInstaller.Items.Add(item2);

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();
        //ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        //ddlTeam.DataMember = "SalesTeam";
        //ddlTeam.DataTextField = "SalesTeam";
        //ddlTeam.DataValueField = "SalesTeamID";
        //ddlTeam.DataBind();


        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
        }

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            if (SalesTeam != string.Empty)
            {
                //ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            // ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        //ddlSalesRepSearch.DataMember = "fullname";
        //ddlSalesRepSearch.DataTextField = "fullname";
        //ddlSalesRepSearch.DataValueField = "EmployeeID";
        //ddlSalesRepSearch.DataBind();
    }

    protected DataTable GetGridData()
    {
        //string historic = false.ToString();
        //if (chkhist.Checked.ToString() == "False")
        //{
        //    historic = "";//Convert.ToString(chkNoPVD.Checked)
        //}
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
       // int ComplianceCertificate = Convert.ToInt32(ddlcomcerti.SelectedValue);
        DataTable dt = new DataTable();
        dt = ClstblProjectRefund.tblProjectgridconnection_Search(ddlmeterreno.SelectedValue, txtproject.Text, txtStartDate.Text, txtEndDate.Text, st.EmployeeID, ddlSearchState.SelectedValue, selectedItem, ddlcomcerti.SelectedValue,ddlEmailFlag.SelectedValue,DropDownList1.SelectedValue,ddlInstaller.SelectedValue);
       // Response.Write(dt.Rows[0]["MeterAppliedRef"]);
       // Response.End();
        // ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
            //tblassign.Visible = false;
        }
        else
        {
            //Response.Write(dt.Rows.Count);
            //Response.End();
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            //  Label ltrPage = (Label)GridView1.FindControl("ltrPage");

            //  Label label1 = (Label)GridView1.Rows[e.NewEditIndex].FindControl("label1");
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    divnopage.Visible = false;
                    //========label Hide
                    //  tblassign.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    //  tblassign.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    //Response.Write(ltrPage);
                    //Response.End();
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //    tblassign.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "refund")
        {
            string RefundID = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();

            ListItem item6 = new ListItem();
            item6.Text = "Select";
            item6.Value = "";
            ddlPayMethod.Items.Clear();
            ddlPayMethod.Items.Add(item6);

            ddlPayMethod.DataSource = ClsProjectSale.tblInvoicePayMethod_Select();
            ddlPayMethod.DataValueField = "InvoicePayMethodID";
            ddlPayMethod.DataTextField = "InvoicePayMethodABB";
            ddlPayMethod.DataMember = "InvoicePayMethodABB";
            ddlPayMethod.DataBind();

            hndRefundID.Value = RefundID;
            SttblProjectRefund stref = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
            hdnProID.Value = stref.ProjectID;
        }
        if (e.CommandName.ToLower() == "revert")
        {
            string RefundID = e.CommandArgument.ToString();
            string AccUserID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;
            string Refund = "False";
            string RefundType = "0";
            string ReceiptNumber = "";
            hndRefundID.Value = RefundID;

            SttblProjectRefund stref = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
            hdnProID.Value = stref.ProjectID;

            ClstblProjectRefund.tblProjectRefund_Update(RefundID, "", "", AccUserID, "");
            bool suc = ClstblProjectRefund.tblProjectRefund_UpdateStatus(RefundID, false.ToString());

            decimal amount = Convert.ToDecimal(stref.Amount);
            decimal invoicepayGST = Convert.ToDecimal(amount) / 11;
            decimal invoicepayExGST = (Convert.ToDecimal(amount) / 11) * 10;
            string InvoicePayExGST = Convert.ToString(invoicepayExGST);
            string InvoicePayGST = Convert.ToString(invoicepayGST);
            string PaymentMode = ddlPayMethod.SelectedValue;
            string InvoicePayTotal = Convert.ToDecimal(amount * 1).ToString();
            int success = ClstblInvoicePayments.tblInvoicePayments_Insert(hdnProID.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, DateTime.Now.AddHours(14).ToString(), PaymentMode, "", EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);
            //int success = ClstblInvoicePayments.tblInvoicePayments_Insert(hdnProID.Value, "", "", "", DateTime.Now.AddHours(14).ToString(), "", "", EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);

            bool s = ClsProjectSale.tblProjects_UpdateProjectStatusID(stref.ProjectID, stref.ProjectStatusID);

        }
        if(e.CommandName.ToString()== "Send Email")
        {
            string ProjectID= e.CommandArgument.ToString();
            hdnProID.Value = ProjectID;
            ModalPopupExtenderverify.Show();
        }
        BindGrid(0);
    }

    protected void ibtnAddComment_Onclick(object sender, EventArgs e)
    {
        string RefundID = hndRefundID.Value;
        string AccUserID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string PaymentMode = ddlPayMethod.SelectedValue;
        string PaidDate = txtPaidDate.Text;
        string Remarks = txtRemarks.Text;
        SttblProjectRefund stref = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
        decimal amount = Convert.ToDecimal(stref.Amount);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;
        string InvoicePayTotal = Convert.ToDecimal(amount * -1).ToString();
        string Refund = "False";
        string RefundType = "0";
        string ReceiptNumber = "";

        bool suc = ClstblProjectRefund.tblProjectRefund_Update(RefundID, PaymentMode, PaidDate, AccUserID, Remarks);

        decimal invoicepayGST = Convert.ToDecimal(amount) / 11;
        decimal invoicepayExGST = (Convert.ToDecimal(amount) / 11) * 10;
        string InvoicePayExGST = Convert.ToString(invoicepayExGST);
        string InvoicePayGST = Convert.ToString(invoicepayGST);


        if (stref.OptionID == "4")
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(hdnProID.Value, "6");
            int success = ClstblInvoicePayments.tblInvoicePayments_Insert(hdnProID.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, DateTime.Now.AddHours(14).ToString(), PaymentMode, "", EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);
        }
        if (suc)
        {
            ModalPopupExtender2.Hide();
            ddlPayMethod.SelectedValue = "";
            txtPaidDate.Text = string.Empty;
            txtRemarks.Text = string.Empty;
        }
        BindGrid(0);
    }
    protected void ibtnCancelComment_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
    }
    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtproject.Text = string.Empty;
        // ddlOptionID.SelectedValue = "";
        txtEndDate.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        ddlmeterreno.SelectedValue = "0";
        ddlSelectRecords.SelectedValue = "25";
        ddlcomcerti.SelectedValue="0";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

       BindGrid(0);
    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        //=======DataTable dt = ClstblProjectRefund.tblProjectRefund_Search("False", txtproject.Text, ddlOptionID.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlTeam.SelectedValue, ddlSalesRepSearch.SelectedValue, userid, selectedItem);

        DataTable dt = GetGridData();
        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                    .Select(x => x.ColumnName)
                                    .ToArray();
            Export oExport = new Export();
            string FileName = "GridConnection" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = {1,2,3,7,8,9,17,4,12,15,6,14,13,10};
            string[] arrHeader = {"ProjectNumber","ProjectType","InstallDate","Adddress","PostCode","State","Installer","InstallComplted","ProjectStatus","Retailer","NMI","ApprovalRef.","SystemCap","MeterAppliedRef"  };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        //Response.Write(lblcurrentpage);
        //Response.End();
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            //Response.Write("1" +iStartsRecods+ iEndRecord+iTotalRecords);
            //Response.End();
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            //Response.Write(ltrPage.Text);
            //Response.End();
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);


        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal ltProjectStatus = (Literal)e.Row.FindControl("ltProjectStatus");
            HiddenField hdnProjectID = (HiddenField)e.Row.FindControl("hdnProjectID");
            //Response.Write(hdnProjectID.Value);
            //Response.End();
            try
            {
                SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);//783297
                if (st.ProjectStatusID != string.Empty)
                {
                    SttblProjectStatus ststatus = ClstblProjectStatus.tblProjectStatus_SelectByProjectStatusID(st.ProjectStatusID);
                    ltProjectStatus.Text = ststatus.ProjectStatus;
                    //Response.Write(ltProjectStatus.Text);
                    //Response.End();
                }
            }
            catch
            {
            }
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");


        if (hdnID.Value == "2")
        {
            e.Item.Visible = false;
        }

    }

    protected void lnkjob_Click(object sender, EventArgs e)
    {

        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnProjectID = (HiddenField)item1.FindControl("hdnProjectID");
        HiddenField hdncust = (HiddenField)item1.FindControl("hdncust");
        hndMetereProjectId.Value = hdnProjectID.Value;
        ModalPopupExtender1.Show();
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(hndMetereProjectId.Value);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(hndMetereProjectId.Value);
        if (st.MeterAppliedTime != "")
        {
           
            txtMeterAppliedTime.Text = st.MeterAppliedTime;
        }
        txtMeterAppliedRef.Text = st.MeterAppliedRef;
        txtInspectorName.Text = st2.InspectorName;

        if (st.MeterAppliedTime != "")
        {
            if (st2.InspectionDate != null && st2.InspectionDate != "")
            {
                DateTime dt1 = Convert.ToDateTime(st2.InspectionDate.ToString());
                txtInspectionDate.Text = dt1.ToString("dd/MM/yyyy");
            }
        }
        ModalPopupExtender1.Show();



    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        bool success = ClstblProjects.tblProjects_UpdatePostInstMeterDetail(hndMetereProjectId.Value, txtMeterAppliedTime.Text, txtMeterAppliedRef.Text);
        bool success1 = ClsProjectSale.tblProjects2_UpdatePostInstInspection(hndMetereProjectId.Value, txtInspectorName.Text, txtInspectionDate.Text);
        if (success && success1)
        {
            ModalPopupExtender1.Hide();
            BindGrid(0);
        }
    }
    protected void lnkSendEmail_Click(object sender, EventArgs e)
    { 

       if(hdnProID.Value!=null && hdnProID.Value!="")
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(hdnProID.Value);
            SttblContacts stc = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
            if(stc.ContEmail!=null && stc.ContEmail!="")
            {
               
                int id = ClsProjectSale.tbl_GridSentEmailLog_InsertData(hdnProID.Value, st.ProjectNumber, st.ContactID, stc.ContEmail, DateTime.Now.AddHours(14).ToShortDateString(), EmployeeID);
                bool prj = ClsProjectSale.tblprojects_updateemailflag(hdnProID.Value, "1");
                if(id>0)
                {
                    SetAdd1();
                }
                else
                {
                    SetError1();
                }
            }
            
        }
        BindGrid(0);
    }
}