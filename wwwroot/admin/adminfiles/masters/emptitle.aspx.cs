﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class admin_adminfiles_master_emptitle : System.Web.UI.Page
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        HidePanels();
        if (!IsPostBack)
        {
            BindGrid(0);

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
        }

    }
    protected DataTable GetGridData()
    {
        DataTable dt = ClstblEmpTitle.tblEmpTitle_Select();

        if (dt.Rows.Count == 0)
        {
            //PanSearch.Visible = false;
        }
        //if (!string.IsNullOrEmpty(hdncgharid.Value))
        //{
        //    dt = ClstblStockCategory.tblStockCategoryGetDataByAlpha(hdncgharid.Value);
        //}
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {

            dt = ClstblEmpTitle.tblEmpTitleGetDataBySearch(txtSearch.Text);
        }
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        //PanSearch.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string EmpTitle = txtEmpTitle.Text;
        string Active = chkActive.Checked.ToString();

        int exist = ClstblEmpTitle.EmpTitleExists(EmpTitle);
        if (exist == 0)
        {
            int success = ClstblEmpTitle.tblEmpTitle_Insert(EmpTitle, Active);
            //--- do not chage this code start------
            if (Convert.ToString(success) != "")
            {
                SetAdd();
            }
            else
            {
                SetError();
            }
            BindGrid(0);
            //GridView1.Rows[0].BackColor = System.Drawing.Color.FromName("#ffffcc");
            Reset();
        }
        else
        {
            InitAdd();
            SetExist();
            //PanAlreadExists.Visible = true;
        }

        //--- do not chage this code end------
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string EmpTitle = txtEmpTitle.Text;
        string Active = chkActive.Checked.ToString();

        int exist = ClstblEmpTitle.EmpTitleExistsWithID(EmpTitle, id1);

        if (exist == 0)
        {
            bool success = ClstblEmpTitle.tblEmpTitle_Update(id1, EmpTitle, Active);
            //--- do not chage this code Start------
            if (success)
            {
                //InitAdd();
                SetUpdate();
            }
            else
            {
                //InitAdd();
                SetError();
            }
            BindGrid(0);
        }
        else
        {
            InitUpdate();
            SetExist();
            //PanAlreadExists.Visible = true;
        }
        //--- do not chage this code end------
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
        
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblEmpTitle st = ClstblEmpTitle.tblEmpTitle_SelectByEmpTitleID(id);
        txtEmpTitle.Text = st.EmpTitle;
        chkActive.Checked = Convert.ToBoolean(st.Active);

        //--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        lblAddUpdate.Text = "";

    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        //Response.Write("ss");
        //Response.End();
        Reset();
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        InitAdd();
    }
    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        ModalPopupExtender2.Hide();
        Reset();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        ModalPopupExtender2.Show();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        ModalPopupExtender2.Show();
        HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        //PanAddUpdate.Visible = true;
        txtEmpTitle.Text = string.Empty;
        chkActive.Checked = false;
    }
    protected void A_Click(object sender, EventArgs e)
    {
        LinkButton button = (LinkButton)sender;
        string alpha = button.CommandArgument;

        BindGrid(0);
    }
    protected void all_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblEmpTitle.tblEmpTitle_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
            //InitAdd();
        }
        else
        {
            SetError();
        }
    
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        //--- do not chage this code end------

    }
}