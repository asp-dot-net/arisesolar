﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_masters_paywaytracker : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindGrid(0);

            ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_SelectASC();
            ddlSalesRepSearch.DataMember = "fullname";
            ddlSalesRepSearch.DataTextField = "fullname";
            ddlSalesRepSearch.DataValueField = "EmployeeID";
            ddlSalesRepSearch.DataBind();
        }
        
    }

    protected DataTable GetGridData()
    {
        DataTable dt3 = new DataTable();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
      
        string userid1 = "";
        if (Roles.IsUserInRole("Administrator"))
        {
            userid = "";
            userid1 = userid;
        }
        else
        {
            userid1 = userid;
        }

        if (Roles.IsUserInRole("Sales Manager"))
        {
            userid = "";
        }
        dt3 = ClstblPayWay.tblPayway_Search(txtprojectno.Text, txtreceiptno.Text, ddlSalesRepSearch.SelectedValue, txtStartDate.Text, txtEndDate.Text, userid1, userid);
         //Response.Write(dt3.Rows.Count);
         //Response.End();
         return dt3;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
           
            PanGrid.Visible = false;
            divnopage.Visible = false;
          
        }
        else
        {
           
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    divnopage.Visible = false;

                }
                else
                {
                    divnopage.Visible = true;

                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }

                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;

                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }

    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtprojectno.Text = string.Empty;
        txtreceiptno.Text = string.Empty;
        ddlSalesRepSearch.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {

    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
}