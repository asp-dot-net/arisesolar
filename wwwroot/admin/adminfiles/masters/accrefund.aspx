<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="accrefund.aspx.cs" Inherits="admin_adminfiles_master_accrefund" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
    <style type="text/css">
        .selected_row {
            background-color: #A1DCF2 !important;
        }

        .modal-body .select2-container {
            z-index: 99 !important;
        }
    </style>
    <script src="../../assets/js/jquery.min.js"></script>

    <script>


        function doMyAction() {

            $('#<%=ibtnAddComment.ClientID %>').click(function () {
                formValidate();
            });


            $("[data-toggle=tooltip]").tooltip();

            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //$(".ddlval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$(".ddlval1").select2({
            //    minimumResultsForSearch: -1
            //});
        }
    </script>



    <script type="text/javascript">

        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }

        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');


        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress


            //if (args.get_error() != undefined) {
            //    args.set_errorhandled(true);
            //}

            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });


            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

            //callMultiCheckbox();
        }

        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            $('#<%=ibtnAddComment.ClientID %>').click(function (e) {

                formValidate();
                //DropDown Validation

                var drpdiv = document.getElementsByClassName("drpValidate");
                var flag = 0;

                for (i = 0; i < drpdiv.length; i++) {


                    if ($(drpdiv[i]).find(".myval").val() == "") {

                        $(drpdiv[i]).addClass("errormassage");

                        flag = flag + 1;
                    }

                }

                if (flag > 0) {
                    e.preventDefault();
                }
                //End DropDown Validation
            });
        }
    </script>



    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Refund</h5>
        <div class="pull-right">
        </div>
    </div>
    <div class="page-body padtopzero padbtmzero">
        <div class="messesgarea">
            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
            </div>
        </div>
        <div class="searchfinal">
            <div class="widget-body shadownone brdrgray">
                <div class="dataTables_wrapper dt-bootstrap no-footer">
                    <div class="dataTables_filter">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                            <div class="contacttoparea ">
                                <div class="form-inline">
                                    <div class="inlineblock">
                                        <div class="input-group col-sm-1 martop5" style="width: 100px">
                                            <asp:TextBox ID="txtproject" runat="server" CssClass="form-control"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtproject"
                                                WatermarkText="Project No" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtproject" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                        </div>
                                        <div class="input-group col-sm-1 martop5" id="tdTeam" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Team</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="input-group spical col-sm-1 martop5" style="width: 140px" id="tdsalerep" runat="server">
                                            <asp:DropDownList ID="ddlSalesRepSearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group spical multiselect martop5" style="width: 160px">
                                            <dl class="dropdown">
                                                <dt>
                                                    <a href="#">
                                                        <span class="hida" id="spanselect">Select</span>
                                                        <p class="multiSel"></p>
                                                    </a>
                                                </dt>
                                                <dd id="ddproject" runat="server">
                                                    <div class="mutliSelect" id="mutliSelect">
                                                        <ul>
                                                            <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                        <%--  <span class="checkbox-info checkbox">--%>
                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                            <span></span>
                                                                        </label>
                                                                        <%-- </span>--%>
                                                                        <label class="chkval">
                                                                            <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                        </label>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>

                                        <div class="form-group spical martop5" style="width: 150px">
                                            <asp:DropDownList ID="ddlOptionID" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="">Option</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control m-b"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtStartDate"
                                                WatermarkText="Start Date" />
                                            <cc1:MaskedEditExtender ID="mskeditCheckIn" runat="server" Mask="99/99/9999" MessageValidatorTip="true"
                                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="date"
                                                TargetControlID="txtStartDate" CultureName="en-GB">
                                            </cc1:MaskedEditExtender>
                                        </div>



                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control m-b"></asp:TextBox>
                                            <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                    ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                            <asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>

                                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                MaskType="date" TargetControlID="txtEndDate" CultureName="en-GB">
                                            </cc1:MaskedEditExtender>
                                        </div>
                                        <div class="form-group martop5" runat="server" id="tdhistoric">

                                            <label for="<%=chkhist.ClientID %>" class="btn btn-magenta">
                                                <asp:CheckBox ID="chkhist" runat="server" />
                                                <span class="text">Historic</span>
                                            </label>
                                        </div>
                                        <div class="form-group martop5">
                                            <span>

                                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" class="btn btn-info"
                                                    CausesValidation="false" OnClick="btnSearch_Click"> <i class="fa fa-search"></i>Search</asp:LinkButton>
                                            </span>
                                        </div>
                                        <div class="form-group martop5">
                                            <asp:LinkButton ID="btnClearAll" runat="server" Text="Search" ValidationGroup="search" class="btn btn-primary"
                                                CausesValidation="false" OnClick="btnClearAll_Click"> <i class="fa fa-refresh"></i>Clear</asp:LinkButton>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="datashowbox">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="row">
                            <div class="dataTables_length showdata col-sm-6">
                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                    aria-controls="DataTables_Table_0" class="myval">
                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div id="tdExport" runat="server" class="pull-right" style="padding-top:8px">
                                    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                        CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body padtopzero">
        <div class="contactbottomarea finalgrid">
            <div class="marginnone">
                <div class="tableblack">
                    <div class="table-responsive noPagination" id="PanGrid" runat="server">
                        <asp:GridView ID="GridView1" DataKeyNames="RefundID" runat="server" ShowFooter="false"
                            OnPageIndexChanging="GridView1_PageIndexChanging" AllowSorting="true" AllowPaging="true" AutoGenerateColumns="false"
                            OnDataBound="GridView1_DataBound" OnRowCreated="GridView1_RowCreated" CssClass="table table-bordered table-striped"
                            OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound">



                            <Columns>
                                <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ProjectNumber"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%#Eval("Project")%>&nbsp;-&nbsp;<%#Eval("ProjectNumber")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="CreateDate"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <%#Eval("CreateDate","{0: dd MMM yyyy}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80px" SortExpression="Amount">
                                    <ItemTemplate>
                                        <%#Eval("Amount","{0:0.00}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Acc Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("AccName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="BSB No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="BSBNo"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <%#Eval("BSBNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Acc No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="AccNo"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <%#Eval("AccNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="UserName"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                    <ItemTemplate>
                                        <%#Eval("UserName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Option" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="OptionName"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                    <ItemTemplate>
                                        <%#Eval("OptionName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pay Method" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="left" ItemStyle-Width="150px" SortExpression="InvoicePayMethod">
                                    <ItemTemplate>
                                        <%#Eval("InvoicePayMethod")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Paid Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="PaidDate"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <%#Eval("PaidDate","{0: dd MMM yyyy}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Previous Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px" SortExpression="PreProjectStatus">
                                    <ItemTemplate>
                                        <%#Eval("PreProjectStatus")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px" SortExpression="ProjectID">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnProjectID" Value='<%#Eval("ProjectID")%>' />
                                        <asp:Literal runat="server" ID="ltProjectStatus"></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Rep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Employee"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <%#Eval("Employee")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refund" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="Status"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:LinkButton ID="lbtnRefund" CommandName="Refund" Visible='<%#Eval("Status").ToString()=="False"?true:false %>' CausesValidation="false"
                                            CommandArgument='<%#Eval("RefundID") %>' runat="server" data-toggle="tooltip" data-placement="top" title="Refund" CssClass="btn btn-info btn-xs">
                                                           <i class="fa fa-dollar"></i> Refund
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="lbtnRevert" CommandName="RevertRefund" CssClass="btn btn-maroon btn-xs"
                                            CommandArgument='<%#Eval("RefundID") %>' Visible='<%#Eval("Status").ToString()=="False"?false:true %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Revert Refund">
                                                          <i class="fa fa-retweet"></i>Revert  </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                <div class="pagination">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                </div>
                                </div>
                            </PagerTemplate>
                            <PagerStyle CssClass="paginationGrid" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                        </asp:GridView>
                    </div>
                    <div class="paginationnew1" runat="server" id="divnopage">
                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                            <tr>
                                <td>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLEdit">
    </cc1:ModalPopupExtender>

    <div class="modal_popup" id="divEdit" runat="server" style="display: none;">
        <div class="modal-dialog" style="width: 250px;">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div style="float: right">
                        <asp:LinkButton ID="btnCancelEdit" CausesValidation="false" OnClick="btnCancelEdit_Click" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                                                    Close
                        </asp:LinkButton>
                    </div>
                    <h4 class="modal-title" id="H3">Update Refund</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline formnew">

                            <div class="clear"></div>

                            <div class="form-group spicaldivin">


                                <div class="formainline">
                                    <div class="form-group" style="width: 187px;">
                                        <label>Pay Method&nbsp;</label>
                                        <div class="drpValidate">
                                            <asp:DropDownList ID="ddlPayMethod" runat="server" Style="z-index: 99;" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPayMethod"
                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group" id="divElecDistApproved" runat="server">
                                        <span class="name">
                                            <asp:Label ID="Label23" runat="server" class="control-label">
                                           Paid Date</asp:Label></span>
                                        <div class="input-group date datetimepicker1">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtPaidDate" runat="server" class="form-control" Width="150px">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPaidDate"
                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>



                                    <div class="form-group modaldesc" style="width: 187px;">
                                        <label>Remarks&nbsp;</label>
                                        <div>
                                            <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" Width="187px" Height="60px"></asp:TextBox>
                                            <br />
                                            <asp:RequiredFieldValidator ID="reqvaltxtNotesName" runat="server" ControlToValidate="txtRemarks"
                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                        <asp:Button ID="ibtnAddComment" runat="server" Text="Update" OnClick="ibtnAddComment_Onclick"
                                            ValidationGroup="AddNotes" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Button ID="btnNULLEdit" Style="display: none;" runat="server" />




    <%--updatepanel--%>
    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });



        $(document).ready(function () {

            $('#<%=ibtnAddComment.ClientID %>').click(function () {
                formValidate();
            });
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

        }


    </script>
    <script type="text/javascript">
                $(document).ready(function () {
                    gridviewScroll();
                });
                $("#nav").on("click", "a", function () {
                    $('#content').animate({ opacity: 0 }, 500, function () {
                        gridviewScroll();
                        $('#content').delay(250).animate({ opacity: 1 }, 500);
                    });
                });
                function gridviewScroll() {
                    $('#<%=GridView1.ClientID%>').gridviewScroll({
                        width: $("#content").width() - 40,
                        height: 6000,
                        freezesize: 0
                    });
                }
    </script>

    <asp:Button ID="btndelete" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

        <div class="modal-dialog " style="margin-top: -300px">
            <div class=" modal-content ">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>


                <div class="modal-title">Revert</div>
                <label id="ghh" runat="server"></label>
                <div class="modal-body ">Are You Sure Revert the Refund?</div>
                <div class="modal-footer " style="text-align: center">
                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                    <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                </div>
            </div>
        </div>

    </div>

    <asp:HiddenField ID="hdndelete" runat="server" />

    <asp:HiddenField ID="hndRefundID" runat="server" />
    <asp:HiddenField ID="hdnProID" runat="server" />
</asp:Content>
