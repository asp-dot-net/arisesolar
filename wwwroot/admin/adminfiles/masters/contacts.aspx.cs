using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Twilio;

public partial class admin_adminfiles_master_contacts : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string address;
    protected static string openModal = "false";

    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());
    protected void Page_Load(object sender, EventArgs e)
    {
        if (openModal == "true")
        {
            ModalPopupExtenderEdit.Show();
        }
        lbleror.Visible = false;
        lbleror1.Visible = false;


        if (!IsPostBack)
        {
            ModalPopupExtenderEdit.Hide();

            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            HidePanels();

            RequiredFieldValidator17.Visible = false;
            RequiredFieldValidator18.Visible = false;
            RequiredFieldValidator19.Visible = false;
            if (Roles.IsUserInRole("DSalesRep"))
            {
                //tdAll.Visible = false;
                tdAll2.Visible = false;
                tdSalesRep.Visible = false;
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                tdAll2.Visible = false;
                ddlTeam.Enabled = false;
                ddlsearchsalerap.Enabled = false;
                ddlSearchSource.Enabled = false;
                ddlSearchSubSource.Enabled = false;

                RequiredFieldValidator17.Visible = false;
                RequiredFieldValidator18.Visible = false;
                RequiredFieldValidator19.Visible = false;
            }
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                lbtnExport.Visible = true;
            }
            else
            {
                lbtnExport.Visible = false;
            }

            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            txtpromosent.Text = DateTime.Now.AddHours(14).ToShortDateString();
            txtpromosentsms.Text = DateTime.Now.AddHours(14).ToShortDateString();
            BindSearchDropdon();
            //BindGrid(0);

            if (Roles.IsUserInRole("SalesRep"))
            {
                ddlSearchRec.Enabled = false;
            }

        }
    }

    public void BindSearchDropdon()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            string SalesTeam = "";
            // Response.Write(st.EmployeeID);

            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                //Response.Write(SalesTeam);
                //Response.End();
                ddlsearchsalerap.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlsearchsalerap.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }

        ddlsearchsalerap.DataMember = "fullname";
        ddlsearchsalerap.DataTextField = "fullname";
        ddlsearchsalerap.DataValueField = "EmployeeID";
        ddlsearchsalerap.DataBind();

        //if (chkViewAll.Checked)
        //{
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {

            if ((ddlSearchType.SelectedValue == "") && (ddlSearchRec.SelectedValue == "") && (txtSearchFirst.Text == string.Empty) && (txtSearchClient.Text == string.Empty)
                && (txtSearchFirst.Text == string.Empty) && (txtSearchLast.Text == string.Empty) && (txtSearchMobile.Text == string.Empty) && (txtProjectNo.Text == string.Empty) && (txtSearchPhone.Text == string.Empty) &&
                (txtSearchStreet.Text == string.Empty) && (txtSerachCity.Text == string.Empty) && (txtSearchPostCode.Text == string.Empty) && (txtSearchEmail.Text == string.Empty) && (txtStartDate.Text == string.Empty) && (txtEndDate.Text == string.Empty))
            {
                ddlsearchsalerap.SelectedValue = st.EmployeeID;
            }
            else
            {
                ddlsearchsalerap.SelectedValue = "";
            }
            //  ddlsearchsalerap.SelectedValue = "";
        }
        //}
        //else
        //{

        //    if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        //    {
        //        ddlsearchsalerap.SelectedValue = st.EmployeeID;

        //    }
        //}

        ddlSearchType.DataSource = ClstblCustType.tblCustType_SelectActive();
        ddlSearchType.DataMember = "CustType";
        ddlSearchType.DataTextField = "CustType";
        ddlSearchType.DataValueField = "CustTypeID";
        ddlSearchType.DataBind();

        ddlSearchSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlSearchSource.DataMember = "CustSource";
        ddlSearchSource.DataTextField = "CustSource";
        ddlSearchSource.DataValueField = "CustSourceID";
        ddlSearchSource.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        //ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        //ddlSearchState.DataMember = "State";
        //ddlSearchState.DataTextField = "State";
        //ddlSearchState.DataValueField = "State";
        //ddlSearchState.DataBind();

        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlNews.DataSource = ClstblNewsLetter.tblNewsLetter_SelectASC();
        ddlNews.DataMember = "NewsTitle";
        ddlNews.DataTextField = "NewsTitle";
        ddlNews.DataValueField = "NewsID";
        ddlNews.DataBind();


        //ddlpromooffer.DataSource = ClstblPromoOffer.tblPromoOffer_Select();
        //ddlpromooffer.DataMember = "PromoOffer";
        //ddlpromooffer.DataTextField = "PromoOffer";
        //ddlpromooffer.DataValueField = "PromoOfferID";
        //ddlpromooffer.DataBind();
    }
    public void BindDataCount()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string userid1 = userid;
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        string Employeeid1 = Employeeid;

        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("PostInstaller")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("CompanyManager")))
        {
            userid1 = "";
            Employeeid1 = "";
        }
        else
        {
            if (chkViewAll.Checked)
            {
                if (!Roles.IsUserInRole("DSalesRep"))
                {
                    userid1 = "";
                    Employeeid1 = "";
                }
            }
        }
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //if (chkViewAll.Checked)
        //{
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {

            if ((ddlSearchType.SelectedValue == "") && (ddlSearchRec.SelectedValue == "") && (selectedItem == "") && (txtSearchFirst.Text == string.Empty) && (txtSearchClient.Text == string.Empty)
                && (txtSearchFirst.Text == string.Empty) && (txtSearchLast.Text == string.Empty) && (txtSearchMobile.Text == string.Empty) && (txtProjectNo.Text == string.Empty) && (txtSearchPhone.Text == string.Empty) &&
                (txtSearchStreet.Text == string.Empty) && (txtSerachCity.Text == string.Empty) && (txtSearchPostCode.Text == string.Empty) && (txtSearchEmail.Text == string.Empty) && (txtStartDate.Text == string.Empty) && (txtEndDate.Text == string.Empty))
            {
                ddlsearchsalerap.SelectedValue = st.EmployeeID;
            }
            else
            {
                ddlsearchsalerap.SelectedValue = "";
            }
            //  ddlsearchsalerap.SelectedValue = "";
        }
        //}
        //else
        //{

        //    if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        //    {
        //        ddlsearchsalerap.SelectedValue = st.EmployeeID;

        //    }
        //}
        string Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "", status = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";
        //Response.Write(Count);
        //Response.End();


        if (Roles.IsUserInRole("PreInstaller"))
        {
            status = "(CNT.ContactID in (select ContactID from tblProjects where ProjectStatusID !='2')) and";
        }
        else
        {
            status = "";

        }
        //string data = "select  count(CNT.ContactID) as countdata  from tblContacts CNT join tblCustomers C on C.CustomerID=CNT.CustomerID join tblEmployees E on E.EmployeeID=CNT.EmployeeID left join tblEmployeeTeam ET on ET.EmployeeID=CNT.EmployeeID join tblCustType CT on CT.CustTypeID=C.CustTypeID where ((upper(CNT.ContMobile) like '%'+'" + txtSearchMobile.Text.Trim() + "'+'%') or '" + txtSearchMobile.Text.Trim() + "'='')and ((upper(CNT.ContPhone) like '%'+'" + txtSearchPhone.Text.Trim() + "'+'%') or '" + txtSearchPhone.Text.Trim() + "'='') and ((upper(CNT.ContFirst) like '%'+'" + txtSearchFirst.Text.Trim() + "'+'%') or '" + txtSearchFirst.Text.Trim() + "'='')  and ((upper(CNT.ContLast) like '%'+'" + txtSearchLast.Text.Trim() + "'+'%') or '" + txtSearchLast.Text.Trim() + "'='')  and ((upper(CNT.ContEmail) like '%'+'" + txtSearchEmail.Text.Trim() + "'+'%') or '" + txtSearchEmail.Text.Trim() + "'='') and (E.EmployeeID='" + ddlsearchsalerap.SelectedValue + "' or '" + ddlsearchsalerap.SelectedValue + "'=0) and ((C.CustTypeID='" + ddlSearchType.SelectedValue + "') or '" + ddlSearchType.SelectedValue + "'=0)  and ((ResCom='" + ddlSearchRec.SelectedValue + "') or '" + ddlSearchRec.SelectedValue + "'=0) and ((C.CustSourceID='" + ddlSearchSource.SelectedValue + "') or '" + ddlSearchSource.SelectedValue + "'=0) and ((upper(C.Customer) like '%'+'" + txtSearchClient.Text.Trim() + "'+'%') or '" + txtSearchClient.Text.Trim() + "'='') and ((C.CompanyNumber= '" + txtSearchCompanyNo.Text.Trim() + "') or '" + txtSearchCompanyNo.Text.Trim() + "'=0) and ((upper(C.StreetPostCode) like '%'+'" + txtSearchPostCode.Text.Trim() + "'+'%') or '" + txtSearchPostCode.Text.Trim() + "'='') and ((upper(C.StreetCity) like '%'+'" + txtSerachCity.Text.Trim() + "'+'%') or '" + txtSerachCity.Text.Trim() + "'='') and ((upper(C.StreetAddress) like '%'+'" + txtSearchStreet.Text.Trim() + "'+'%') or '" + txtSearchStreet.Text.Trim() + "'='') and ((upper(C.StreetState) like '%'+'" + "" + "'+'%') or '" + "" + "'='') and ((C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "') or '" + ddlSearchSubSource.SelectedValue + "'=0) and ((CNT.ContactEntered + 2 >= '" + txtStartDate.Text + "') or '" + txtStartDate.Text + "'='') and ((CNT.ContactEntered + 2 <= '" + txtEndDate.Text + "') or '" + txtEndDate.Text + "'='') and ((convert(varchar(100),E.EmployeeID)='" + Employeeid1 + "' or '" + Employeeid1 + "'='')) and ((convert(varchar(100),E.SalesTeamID)='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'=0)) " + data1;

        //---O:17/7/18 string data = "select  count(CNT.ContactID) as countdata  from tblContacts CNT join tblCustomers C on C.CustomerID=CNT.CustomerID join tblEmployees E on E.EmployeeID=CNT.EmployeeID left join tblEmployeeTeam ET on ET.EmployeeID=CNT.EmployeeID join tblCustType CT on CT.CustTypeID=C.CustTypeID where ((upper(CNT.ContMobile) like '%'+'" + txtSearchMobile.Text.Trim() + "'+'%') or '" + txtSearchMobile.Text.Trim() + "'='')and ((upper(CNT.ContPhone) like '%'+'" + txtSearchPhone.Text.Trim() + "'+'%') or '" + txtSearchPhone.Text.Trim() + "'='') and ((upper(CNT.ContFirst) like '%'+'" + txtSearchFirst.Text.Trim() + "'+'%') or '" + txtSearchFirst.Text.Trim() + "'='')  and ((upper(CNT.ContLast) like '%'+'" + txtSearchLast.Text.Trim() + "'+'%') or '" + txtSearchLast.Text.Trim() + "'='')  and ((upper(CNT.ContEmail) like '%'+'" + txtSearchEmail.Text.Trim() + "'+'%') or '" + txtSearchEmail.Text.Trim() + "'='') and (E.EmployeeID='" + ddlsearchsalerap.SelectedValue + "' or '" + ddlsearchsalerap.SelectedValue + "'=0) and ((C.CustTypeID='" + ddlSearchType.SelectedValue + "') or '" + ddlSearchType.SelectedValue + "'=0)  and ((ResCom='" + ddlSearchRec.SelectedValue + "') or '" + ddlSearchRec.SelectedValue + "'=0) and ((C.CustSourceID='" + ddlSearchSource.SelectedValue + "') or '" + ddlSearchSource.SelectedValue + "'=0) and ((upper(C.Customer) like '%'+'" + txtSearchClient.Text.Trim() + "'+'%') or '" + txtSearchClient.Text.Trim() + "'='') and ((C.CompanyNumber= '" + txtSearchCompanyNo.Text.Trim() + "') or '" + txtSearchCompanyNo.Text.Trim() + "'=0) and ((upper(C.StreetPostCode) like '%'+'" + txtSearchPostCode.Text.Trim() + "'+'%') or '" + txtSearchPostCode.Text.Trim() + "'='') and ((upper(C.StreetCity) like '%'+'" + txtSerachCity.Text.Trim() + "'+'%') or '" + txtSerachCity.Text.Trim() + "'='') and ((upper(C.StreetAddress) like '%'+'" + txtSearchStreet.Text.Trim() + "'+'%') or '" + txtSearchStreet.Text.Trim() + "'='') and ((upper(C.StreetState) like '%'+'" + "" + "'+'%') or '" + "" + "'='') and ((C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "') or '" + ddlSearchSubSource.SelectedValue + "'=0) and ((CNT.ContactEntered + 2 >= '" + txtStartDate.Text + "') or '" + txtStartDate.Text + "'='') and ((CNT.ContactEntered + 2 <= '" + txtEndDate.Text + "') or '" + txtEndDate.Text + "'='') and ((convert(varchar(100),E.EmployeeID)='" + Employeeid1 + "' or '" + Employeeid1 + "'='')) and ((CNT.ContactID in (select ContactID from tblProjects where ProjectNumber='" + txtProjectNo.Text.Trim() + "')) or '" + txtProjectNo.Text.Trim() + "'= '') and ((convert(varchar(100),E.SalesTeamID)='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'=0)) " + data1;
        //-----------------------------------------deep

        string data = "select  count(CNT.ContactID) as countdata  from tblContacts CNT join tblCustomers C on C.CustomerID=CNT.CustomerID join tblEmployees E on E.EmployeeID=CNT.EmployeeID left join tblEmployeeTeam ET on ET.EmployeeID=CNT.EmployeeID join tblCustType CT on CT.CustTypeID=C.CustTypeID where" + status + "((upper(CNT.ContMobile) like '%'+'" + txtSearchMobile.Text.Trim() + "'+'%') or '" + txtSearchMobile.Text.Trim() + "'='')and ((upper(CNT.ContPhone) like '%'+'" + txtSearchPhone.Text.Trim() + "'+'%') or '" + txtSearchPhone.Text.Trim() + "'='') and ((upper(CNT.ContFirst) like '%'+'" + txtSearchFirst.Text.Trim() + "'+'%') or '" + txtSearchFirst.Text.Trim() + "'='')  and ((upper(CNT.ContLast) like '%'+'" + txtSearchLast.Text.Trim() + "'+'%') or '" + txtSearchLast.Text.Trim() + "'='')  and ((upper(CNT.ContEmail) like '%'+'" + txtSearchEmail.Text.Trim() + "'+'%') or '" + txtSearchEmail.Text.Trim() + "'='') and (E.EmployeeID='" + ddlsearchsalerap.SelectedValue + "' or '" + ddlsearchsalerap.SelectedValue + "'=0) and ((C.CustTypeID='" + ddlSearchType.SelectedValue + "') or '" + ddlSearchType.SelectedValue + "'=0)  and ((ResCom='" + ddlSearchRec.SelectedValue + "') or '" + ddlSearchRec.SelectedValue + "'=0) and ((C.CustSourceID='" + ddlSearchSource.SelectedValue + "') or '" + ddlSearchSource.SelectedValue + "'=0) and ((upper(C.Customer) like '%'+'" + txtSearchClient.Text.Trim() + "'+'%') or '" + txtSearchClient.Text.Trim() + "'='') and ((C.CompanyNumber= '" + txtSearchCompanyNo.Text.Trim() + "') or '" + txtSearchCompanyNo.Text.Trim() + "'=0) and ((upper(C.StreetPostCode) like '%'+'" + txtSearchPostCode.Text.Trim() + "'+'%') or '" + txtSearchPostCode.Text.Trim() + "'='') and ((upper(C.StreetCity) like '%'+'" + txtSerachCity.Text.Trim() + "'+'%') or '" + txtSerachCity.Text.Trim() + "'='') and ((upper(C.StreetAddress) like '%'+'" + txtSearchStreet.Text.Trim() + "'+'%') or '" + txtSearchStreet.Text.Trim() + "'='') and ((upper(C.StreetState) like '%'+'" + "" + "'+'%') or '" + "" + "'='') and ((C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "') or '" + ddlSearchSubSource.SelectedValue + "'=0) and ((CNT.ContactEntered + 2 >= '" + txtStartDate.Text + "') or '" + txtStartDate.Text + "'='') and ((CNT.ContactEntered + 2 <= '" + txtEndDate.Text + "') or '" + txtEndDate.Text + "'='') and ((convert(varchar(100),E.EmployeeID)='" + Employeeid1 + "' or '" + Employeeid1 + "'='')) and ((CNT.ContactID in (select ContactID from tblProjects where ProjectNumber='" + txtProjectNo.Text.Trim() + "')) or '" + txtProjectNo.Text.Trim() + "'= '') and ((convert(varchar(100),E.SalesTeamID)='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'=0)) " + data1;

        //-----------------------------------------------

        //join tblProjects PT on CT.ContactID=PT.ContactID  
        //and (CNT.ContactID in (select ContactID from tblProjects where (ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'=''))) 

        //and (isnull(ET.SalesTeamID,0)= case when '"+ddlTeam.SelectedValue+"' is null then isnull(ET.SalesTeamID,0) else '"+ddlTeam.SelectedValue+"' end) 

        DataTable dt = ClstblCustomers.query_execute(data);
        if (dt.Rows.Count > 0)
        {

            countdata = Convert.ToInt32(dt.Rows[0]["countdata"]);
            //Response.Write(countdata);
            //Response.End();
            hdncountdata.Value = countdata.ToString();
        }
        #region MyRegionForPagination

        if (custompagesize != 0 && ddlSelectRecords.SelectedValue != "All")
        {

            //Response.End();
            lastpageindex = Convert.ToInt32(hdncountdata.Value) / Convert.ToInt32(ddlSelectRecords.SelectedValue);

            if (Convert.ToInt32(hdncountdata.Value) % Convert.ToInt32(ddlSelectRecords.SelectedValue) > 0)
            {
                lastpageindex = lastpageindex + 1;
            }

            int pageindexcustom1 = Convert.ToInt32(hdncountdata.Value) / Convert.ToInt32(ddlSelectRecords.SelectedValue);

            if (pageindexcustom1 > 0)
            {
                if (Convert.ToInt32(hdncountdata.Value) % Convert.ToInt32(ddlSelectRecords.SelectedValue) > 0)
                {
                    pageindexcustom1 = pageindexcustom1 + 1;
                }

                if (pageindexcustom1 >= custompageIndex)
                {

                }
                else
                {
                    custompageIndex = pageindexcustom1;
                }

                int PageIndex = 3;
                int StartPageIndex = custompageIndex - PageIndex;//10-3=7
                int EndPageIndex = custompageIndex + PageIndex;//10+3=13

                if (StartPageIndex < 1)
                {
                    EndPageIndex = EndPageIndex - StartPageIndex;
                }
                if (EndPageIndex > lastpageindex)
                {
                    StartPageIndex = (StartPageIndex - (EndPageIndex - lastpageindex)) + 1;
                }
                DataTable dt_page = new DataTable();
                dt_page.Columns.Add("ID");

                for (int i = 1; i <= lastpageindex; i++)
                {
                    if ((StartPageIndex < i && EndPageIndex > i))
                    {
                        DataRow dr = dt_page.NewRow();
                        dr["ID"] = i;
                        dt_page.Rows.Add(dr);

                    }
                }

                rptpage.DataSource = dt_page;
                rptpage.DataBind();

                if (custompageIndex == 1)
                {
                    lnkfirst.Visible = false;
                    lnkprevious.Visible = false;
                    lnknext.Visible = true;
                    lnklast.Visible = true;
                }
                if (custompageIndex > 1)
                {
                    lnkfirst.Visible = true;
                    lnkprevious.Visible = true;
                }
                if (custompageIndex == EndPageIndex)
                {
                    lnknext.Visible = false;
                    lnklast.Visible = false;
                }
                if (lastpageindex == custompageIndex)
                {
                    lnknext.Visible = false;
                    lnklast.Visible = false;
                }
                if (custompageIndex > 1 && custompageIndex < lastpageindex)
                {
                    lnkfirst.Visible = true;
                    lnkprevious.Visible = true;
                    lnknext.Visible = true;
                    lnklast.Visible = true;
                }
            }
            else
            {
                rptpage.DataSource = null;
                rptpage.DataBind();
                lnkfirst.Visible = false;
                lnkprevious.Visible = false;
                lnknext.Visible = false;
                lnklast.Visible = false;
            }
        }
        else
        {
            rptpage.DataSource = null;
            rptpage.DataBind();
            lnkfirst.Visible = false;
            lnkprevious.Visible = false;
            lnknext.Visible = false;
            lnklast.Visible = false;
        }
        #endregion
    }
    protected DataTable GetGridData()
    {
        BindDataCount();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string userid1 = userid;
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        string Employeeid1 = Employeeid;
        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("PostInstaller")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("CompanyManager")))
        {
            userid1 = "";
            Employeeid1 = "";
        }
        else
        {
            if (chkViewAll.Checked)
            {
                if (!Roles.IsUserInRole("DSalesRep"))
                {
                    userid1 = "";
                    Employeeid1 = "";
                }
            }
        }
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //if (chkViewAll.Checked)
        //{
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {

            if ((ddlSearchType.SelectedValue == "") && (ddlSearchRec.SelectedValue == "") && (selectedItem == "") && (txtSearchFirst.Text == string.Empty) && (txtSearchClient.Text == string.Empty)
                && (txtSearchFirst.Text == string.Empty) && (txtSearchLast.Text == string.Empty) && (txtSearchMobile.Text == string.Empty) && (txtProjectNo.Text == string.Empty) && (txtSearchPhone.Text == string.Empty) &&
                (txtSearchStreet.Text == string.Empty) && (txtSerachCity.Text == string.Empty) && (txtSearchPostCode.Text == string.Empty) && (txtSearchEmail.Text == string.Empty) && (txtStartDate.Text == string.Empty) && (txtEndDate.Text == string.Empty))
            {
                custompageIndex = 1;//it is used because when we clear the filter the records are less than 25 and if we are are 3 page index and do clear then it gives offset error.
                ddlsearchsalerap.SelectedValue = st.EmployeeID;
            }
            else
            {
                ddlsearchsalerap.SelectedValue = "";
            }

            //  ddlsearchsalerap.SelectedValue = "";
        }
        //}
        //else
        //{

        //    if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        //    {
        //        ddlsearchsalerap.SelectedValue = st.EmployeeID;

        //    }
        //}
        if (ddlSelectRecords.SelectedValue != "All")
        {
            startindex = (custompageIndex - 1) * Convert.ToInt32(ddlSelectRecords.SelectedValue) + 1;
        }
        else
        {
            startindex = 1;
        }


        string datagrid = "", Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "", data2 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";

        String status = "";
        if (Roles.IsUserInRole("PreInstaller"))
        {
            status = "(CNT.ContactID in (select ContactID from tblProjects where ProjectStatusID !='2')) and";
        }
        else
        {
            status = "";

        }

        if (ddlSelectRecords.SelectedValue != "All")
        {
            if (ddlSelectRecords.SelectedValue == "50" || ddlSelectRecords.SelectedValue == "100")
            {
                datagrid = "select  case when CNT.ContLast is null then CNT.ContFirst else CNT.ContFirst + ' ' +CNT.ContLast end as fullname,CNT.ContactID,CNT.CustomerID,CNT.EmployeeID,C.Customer as Customer,C.StreetAddress as Street,C.StreetCity as Suburb, C.StreetPostCode as PCd,C.StreetState as [State], [dbo].[funGetProject](Cnt.ContactID) as projectstatus,CT.CustType as leadtype,(select 1) as empflag,(select case when E.EmpLast is null then E.EmpFirst else E.EmpFirst+' '+E.EmpLast end) as salesrep,(select top 1 ProjectQuoteDate from tblProjectQuotes where ProjectID in  (select ProjectID from tblProjects where ContactID=CNT.ContactID) order by ProjectQuoteDate desc)as QuoteDate, (select Top 1 NextFollowupDate from tblCustInfo where CustomerID=CNT.CustomerID order by CustInfoID desc) as NextFollowUpDate, (select Top 1 Description from tblCustInfo where CustomerID=CNT.CustomerID order by CustInfoID desc) as Description    ,CNT.SendSMS, (select top 1 contactid from tblcontacts where customerid=C.CustomerID ) as FirstContact  from tblContacts CNT join tblCustomers C on C.CustomerID=CNT.CustomerID join tblEmployees E on E.EmployeeID=CNT.EmployeeID left join tblEmployeeTeam ET on ET.EmployeeID=CNT.EmployeeID join tblCustType CT on CT.CustTypeID=C.CustTypeID where" + status + " ((upper(CNT.ContMobile) like '%'+'" + txtSearchMobile.Text.Trim() + "'+'%') or '" + txtSearchMobile.Text.Trim() + "'='')and ((upper(CNT.ContPhone) like '%'+'" + txtSearchPhone.Text.Trim() + "'+'%') or '" + txtSearchPhone.Text.Trim() + "'='') and ((upper(CNT.ContFirst) like '%'+'" + txtSearchFirst.Text.Trim() + "'+'%') or '" + txtSearchFirst.Text.Trim() + "'='')  and ((upper(CNT.ContLast) like '%'+'" + txtSearchLast.Text.Trim() + "'+'%') or '" + txtSearchLast.Text.Trim() + "'='')  and ((upper(CNT.ContEmail) like '%'+'" + txtSearchEmail.Text.Trim() + "'+'%') or '" + txtSearchEmail.Text.Trim() + "'='') and (E.EmployeeID='" + ddlsearchsalerap.SelectedValue + "' or '" + ddlsearchsalerap.SelectedValue + "'=0)   and ((CNT.ContactEntered + 2 >= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='') and ((CNT.ContactEntered + 2 <= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='')  and ((C.CustTypeID='" + ddlSearchType.SelectedValue + "') or '" + ddlSearchType.SelectedValue + "'=0)  and ((ResCom='" + ddlSearchRec.SelectedValue + "') or '" + ddlSearchRec.SelectedValue + "'=0) and ((C.CustSourceID='" + ddlSearchSource.SelectedValue + "') or '" + ddlSearchSource.SelectedValue + "'=0) and ((C.CompanyNumber= '" + txtSearchCompanyNo.Text.Trim() + "') or '" + txtSearchCompanyNo.Text.Trim() + "'=0) and ((upper(C.StreetPostCode) like '%'+'" + txtSearchPostCode.Text.Trim() + "'+'%') or '" + txtSearchPostCode.Text.Trim() + "'='') and ((upper(C.StreetCity) like '%'+'" + txtSerachCity.Text.Trim() + "'+'%') or '" + txtSerachCity.Text.Trim() + "'='') and ((upper(C.StreetAddress) like '%'+'" + txtSearchStreet.Text.Trim() + "'+'%') or '" + txtSearchStreet.Text.Trim() + "'='') and ((upper(C.StreetState) like '%'+'" + "" + "'+'%') or '" + "" + "'='') and ((C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "') or '" + ddlSearchSubSource.SelectedValue + "'=0) and ((convert(varchar(100),E.EmployeeID)='" + Employeeid1 + "' or '" + Employeeid1 + "'='')) and ((CNT.ContactID in (select ContactID from tblProjects where ProjectNumber='" + txtProjectNo.Text.Trim() + "')) or '" + txtProjectNo.Text.Trim() + "'= '') and  ((convert(varchar(100),E.SalesTeamID)='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'=0)) " + data1 + " order by CNT.ContactEntered desc OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + ddlSelectRecords.SelectedValue + ">0 then(" + ddlSelectRecords.SelectedValue + " ) else (select count(CustomerID) from tblcontacts ) end) ROWS ONLY ;";
                //datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) " + empid + "and (C.isformbayadd='" + ddladdressverification.SelectedValue + "' or '" + ddladdressverification.SelectedValue + "'='') and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= C.CustEntered or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=C.CustEntered  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + ddlSelectRecords.SelectedValue.ToString() + ">0 then(" + ddlSelectRecords.SelectedValue.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";
            }
            else
            {
                datagrid = "select  case when CNT.ContLast is null then CNT.ContFirst else CNT.ContFirst + ' ' +CNT.ContLast end as fullname,CNT.ContactID,CNT.CustomerID,CNT.EmployeeID,C.Customer as Customer,C.StreetAddress as Street,C.StreetCity as Suburb, C.StreetPostCode as PCd,C.StreetState as [State], [dbo].[funGetProject](Cnt.ContactID) as projectstatus,CT.CustType as leadtype,(select 1) as empflag,(select case when E.EmpLast is null then E.EmpFirst else E.EmpFirst+' '+E.EmpLast end) as salesrep,(select top 1 ProjectQuoteDate from tblProjectQuotes where ProjectID in  (select ProjectID from tblProjects where ContactID=CNT.ContactID) order by ProjectQuoteDate desc)as QuoteDate, (select Top 1 NextFollowupDate from tblCustInfo where CustomerID=CNT.CustomerID order by CustInfoID desc) as NextFollowUpDate, (select Top 1 Description from tblCustInfo where CustomerID=CNT.CustomerID order by CustInfoID desc) as Description    ,CNT.SendSMS, (select top 1 contactid from tblcontacts where customerid=C.CustomerID ) as FirstContact  from tblContacts CNT join tblCustomers C on C.CustomerID=CNT.CustomerID join tblEmployees E on E.EmployeeID=CNT.EmployeeID left join tblEmployeeTeam ET on ET.EmployeeID=CNT.EmployeeID join tblCustType CT on CT.CustTypeID=C.CustTypeID where " + status + "((upper(CNT.ContMobile) like '%'+'" + txtSearchMobile.Text.Trim() + "'+'%') or '" + txtSearchMobile.Text.Trim() + "'='')and ((upper(CNT.ContPhone) like '%'+'" + txtSearchPhone.Text.Trim() + "'+'%') or '" + txtSearchPhone.Text.Trim() + "'='') and ((upper(CNT.ContFirst) like '%'+'" + txtSearchFirst.Text.Trim() + "'+'%') or '" + txtSearchFirst.Text.Trim() + "'='')  and ((upper(CNT.ContLast) like '%'+'" + txtSearchLast.Text.Trim() + "'+'%') or '" + txtSearchLast.Text.Trim() + "'='')  and ((upper(CNT.ContEmail) like '%'+'" + txtSearchEmail.Text.Trim() + "'+'%') or '" + txtSearchEmail.Text.Trim() + "'='') and (E.EmployeeID='" + ddlsearchsalerap.SelectedValue + "' or '" + ddlsearchsalerap.SelectedValue + "'=0)   and ((CNT.ContactEntered + 2 >= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='') and ((CNT.ContactEntered + 2 <= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='')  and ((C.CustTypeID='" + ddlSearchType.SelectedValue + "') or '" + ddlSearchType.SelectedValue + "'=0)  and ((ResCom='" + ddlSearchRec.SelectedValue + "') or '" + ddlSearchRec.SelectedValue + "'=0) and ((C.CustSourceID='" + ddlSearchSource.SelectedValue + "') or '" + ddlSearchSource.SelectedValue + "'=0) and ((C.CompanyNumber= '" + txtSearchCompanyNo.Text.Trim() + "') or '" + txtSearchCompanyNo.Text.Trim() + "'=0) and ((upper(C.StreetPostCode) like '%'+'" + txtSearchPostCode.Text.Trim() + "'+'%') or '" + txtSearchPostCode.Text.Trim() + "'='') and ((upper(C.StreetCity) like '%'+'" + txtSerachCity.Text.Trim() + "'+'%') or '" + txtSerachCity.Text.Trim() + "'='') and ((upper(C.StreetAddress) like '%'+'" + txtSearchStreet.Text.Trim() + "'+'%') or '" + txtSearchStreet.Text.Trim() + "'='') and ((upper(C.StreetState) like '%'+'" + "" + "'+'%') or '" + "" + "'='') and ((C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "') or '" + ddlSearchSubSource.SelectedValue + "'=0) and ((convert(varchar(100),E.EmployeeID)='" + Employeeid1 + "' or '" + Employeeid1 + "'='')) and ((CNT.ContactID in (select ContactID from tblProjects where ProjectNumber='" + txtProjectNo.Text.Trim() + "')) or '" + txtProjectNo.Text.Trim() + "'= '') and  ((convert(varchar(100),E.SalesTeamID)='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'=0)) " + data1 + " order by CNT.ContactEntered desc OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblcontacts ) end) ROWS ONLY ;";
            }
        }
        else
        {
            datagrid = "select  case when CNT.ContLast is null then CNT.ContFirst else CNT.ContFirst + ' ' +CNT.ContLast end as fullname,CNT.ContactID,CNT.CustomerID,CNT.EmployeeID,C.Customer as Customer,C.StreetAddress as Street,C.StreetCity as Suburb, C.StreetPostCode as PCd,C.StreetState as [State], [dbo].[funGetProject](Cnt.ContactID) as projectstatus,CT.CustType as leadtype,(select 1) as empflag,(select case when E.EmpLast is null then E.EmpFirst else E.EmpFirst+' '+E.EmpLast end) as salesrep,(select top 1 ProjectQuoteDate from tblProjectQuotes where ProjectID in  (select ProjectID from tblProjects where ContactID=CNT.ContactID) order by ProjectQuoteDate desc)as QuoteDate, (select Top 1 NextFollowupDate from tblCustInfo where CustomerID=CNT.CustomerID order by CustInfoID desc) as NextFollowUpDate, (select Top 1 Description from tblCustInfo where CustomerID=CNT.CustomerID order by CustInfoID desc) as Description    ,CNT.SendSMS, (select top 1 contactid from tblcontacts where customerid=C.CustomerID ) as FirstContact  from tblContacts CNT join tblCustomers C on C.CustomerID=CNT.CustomerID join tblEmployees E on E.EmployeeID=CNT.EmployeeID left join tblEmployeeTeam ET on ET.EmployeeID=CNT.EmployeeID join tblCustType CT on CT.CustTypeID=C.CustTypeID where " + status + "((upper(CNT.ContMobile) like '%'+'" + txtSearchMobile.Text.Trim() + "'+'%') or '" + txtSearchMobile.Text.Trim() + "'='')and ((upper(CNT.ContPhone) like '%'+'" + txtSearchPhone.Text.Trim() + "'+'%') or '" + txtSearchPhone.Text.Trim() + "'='') and ((upper(CNT.ContFirst) like '%'+'" + txtSearchFirst.Text.Trim() + "'+'%') or '" + txtSearchFirst.Text.Trim() + "'='')  and ((upper(CNT.ContLast) like '%'+'" + txtSearchLast.Text.Trim() + "'+'%') or '" + txtSearchLast.Text.Trim() + "'='')  and ((upper(CNT.ContEmail) like '%'+'" + txtSearchEmail.Text.Trim() + "'+'%') or '" + txtSearchEmail.Text.Trim() + "'='') and (E.EmployeeID='" + ddlsearchsalerap.SelectedValue + "' or '" + ddlsearchsalerap.SelectedValue + "'=0)   and ((CNT.ContactEntered + 2 >= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='') and ((CNT.ContactEntered + 2 <= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='')  and ((C.CustTypeID='" + ddlSearchType.SelectedValue + "') or '" + ddlSearchType.SelectedValue + "'=0)  and ((ResCom='" + ddlSearchRec.SelectedValue + "') or '" + ddlSearchRec.SelectedValue + "'=0) and ((C.CustSourceID='" + ddlSearchSource.SelectedValue + "') or '" + ddlSearchSource.SelectedValue + "'=0) and ((C.CompanyNumber= '" + txtSearchCompanyNo.Text.Trim() + "') or '" + txtSearchCompanyNo.Text.Trim() + "'=0) and ((upper(C.StreetPostCode) like '%'+'" + txtSearchPostCode.Text.Trim() + "'+'%') or '" + txtSearchPostCode.Text.Trim() + "'='') and ((upper(C.StreetCity) like '%'+'" + txtSerachCity.Text.Trim() + "'+'%') or '" + txtSerachCity.Text.Trim() + "'='') and ((upper(C.StreetAddress) like '%'+'" + txtSearchStreet.Text.Trim() + "'+'%') or '" + txtSearchStreet.Text.Trim() + "'='') and ((upper(C.StreetState) like '%'+'" + "" + "'+'%') or '" + "" + "'='') and ((C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "') or '" + ddlSearchSubSource.SelectedValue + "'=0) and ((convert(varchar(100),E.EmployeeID)='" + Employeeid1 + "' or '" + Employeeid1 + "'='')) and ((CNT.ContactID in (select ContactID from tblProjects where ProjectNumber='" + txtProjectNo.Text.Trim() + "')) or '" + txtProjectNo.Text.Trim() + "'= '') and  ((convert(varchar(100),E.SalesTeamID)='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'=0)) " + data1 + " order by CNT.ContactEntered desc OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT (select count(CustomerID) from tblcontacts ) ROWS ONLY ;";
            //datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and(C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or '" + ddlSearchSource.SelectedValue + "'=0) and(C.ResCom='" + ddlSearchRec.SelectedValue + "' or '" + ddlSearchRec.SelectedValue + "'=0) and (C.Area='" + ddlSearchArea.SelectedValue + "' or '" + ddlSearchArea.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or '" + ddlSearchSubSource.SelectedValue + "'=0) " + empid + "and (C.isformbayadd='" + ddladdressverification.SelectedValue + "' or '" + ddladdressverification.SelectedValue + "'='') and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0) and (C.StreetState like '%'+'" + ddlSearchState.Text + "'+'%'  or  '" + ddlSearchState.Text + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%'  or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%' or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= C.CustEntered or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=C.CustEntered  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustEntered DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT (select count(CustomerID) from tblCustomers ) ROWS ONLY ;";
        }


        //(CNT.ContactID in (select ContactID from tblProjects where (ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='')))and (CNT.ContactID in (select ContactID from tblProjects where ProjectNumber like '" + txtProjectNo.Text + "' or '" + txtProjectNo.Text + "'='')) 
        //datagrid = "select  case when CNT.ContLast is null then CNT.ContFirst else CNT.ContFirst + ' ' +CNT.ContLast end as fullname,CNT.ContactID,CNT.CustomerID,CNT.EmployeeID,C.Customer as Customer,C.StreetAddress as Street,C.StreetCity as Suburb, C.StreetPostCode as PCd,C.StreetState as [State], [dbo].[funGetProject](Cnt.ContactID) as projectstatus,CT.CustType as leadtype,(select 1) as empflag,(select case when E.EmpLast is null then E.EmpFirst else E.EmpFirst+' '+E.EmpLast end) as salesrep,(select top 1 ProjectQuoteDate from tblProjectQuotes where ProjectID in  (select ProjectID from tblProjects where ContactID=CNT.ContactID) order by ProjectQuoteDate desc)as QuoteDate, (select max(NextFollowupDate) from tblCustInfo where CustomerID=CNT.CustomerID) as NextFollowUpDate  ,CNT.SendSMS, (select top 1 contactid from tblcontacts where customerid=C.CustomerID ) as FirstContact  from tblContacts CNT join tblCustomers C on C.CustomerID=CNT.CustomerID join tblEmployees E on E.EmployeeID=CNT.EmployeeID left join tblEmployeeTeam ET on ET.EmployeeID=CNT.EmployeeID join tblCustType CT on CT.CustTypeID=C.CustTypeID where ((upper(CNT.ContMobile) like '%'+'" + txtSearchMobile.Text.Trim() + "'+'%') or '" + txtSearchMobile.Text.Trim() + "'='')and ((upper(CNT.ContPhone) like '%'+'" + txtSearchPhone.Text.Trim() + "'+'%') or '" + txtSearchPhone.Text.Trim() + "'='') and ((upper(CNT.ContFirst) like '%'+'" + txtSearchFirst.Text.Trim() + "'+'%') or '" + txtSearchFirst.Text.Trim() + "'='')  and ((upper(CNT.ContLast) like '%'+'" + txtSearchLast.Text.Trim() + "'+'%') or '" + txtSearchLast.Text.Trim() + "'='')  and ((upper(CNT.ContEmail) like '%'+'" + txtSearchEmail.Text.Trim() + "'+'%') or '" + txtSearchEmail.Text.Trim() + "'='') and (E.EmployeeID='" + ddlsearchsalerap.SelectedValue + "' or '" + ddlsearchsalerap.SelectedValue + "'=0)   and ((CNT.ContactEntered + 2 >= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='') and ((CNT.ContactEntered + 2 <= '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "') or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='')  and ((C.CustTypeID='" + ddlSearchType.SelectedValue + "') or '" + ddlSearchType.SelectedValue + "'=0)  and ((ResCom='" + ddlSearchRec.SelectedValue + "') or '" + ddlSearchRec.SelectedValue + "'=0) and ((C.CustSourceID='" + ddlSearchSource.SelectedValue + "') or '" + ddlSearchSource.SelectedValue + "'=0) and ((upper(C.Customer) like '%'+'" + txtSearchClient.Text.Trim() + "'+'%') or '" + txtSearchClient.Text.Trim() + "'='') and ((C.CompanyNumber= '" + txtSearchCompanyNo.Text.Trim() + "') or '" + txtSearchCompanyNo.Text.Trim() + "'=0) and ((upper(C.StreetPostCode) like '%'+'" + txtSearchPostCode.Text.Trim() + "'+'%') or '" + txtSearchPostCode.Text.Trim() + "'='') and ((upper(C.StreetCity) like '%'+'" + txtSerachCity.Text.Trim() + "'+'%') or '" + txtSerachCity.Text.Trim() + "'='') and ((upper(C.StreetAddress) like '%'+'" + txtSearchStreet.Text.Trim() + "'+'%') or '" + txtSearchStreet.Text.Trim() + "'='') and ((upper(C.StreetState) like '%'+'" + "" + "'+'%') or '" + "" + "'='') and ((C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "') or '" + ddlSearchSubSource.SelectedValue + "'=0) and ((convert(varchar(100),E.EmployeeID)='" + Employeeid1 + "' or '" + Employeeid1 + "'='')) and ((CNT.ContactID in (select ContactID from tblProjects where ProjectNumber='" + txtProjectNo.Text.Trim() + "')) or '" + txtProjectNo.Text.Trim() + "'= '') and  ((convert(varchar(100),E.SalesTeamID)='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'=0)) " + data1 + " order by CNT.ContactEntered desc OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblcontacts ) end) ROWS ONLY ;";
        // Response.Write( txtStartDate.Text+ "=="+ txtEndDate.Text + "==");


        DataTable dt = new DataTable();
        // dt.Clear();
        dt = ClstblCustomers.query_execute(datagrid);

        //Response.Write(dt.Rows.Count);
        //Response.End();

        return dt;

    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        //Response.Write(hdncountdata.Value);
        //Response.End();
        if (dt.Rows.Count == 0)
        {
            HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            divBulkemail.Visible = false;
            divbulksms.Visible = false;


        }
        else
        {
            if ((Roles.IsUserInRole("Administrator")))
            {
                //divpromosms.Visible = true;
                GridView1.Columns[0].Visible = true;
            }
            //else
            //{
            //    //divpromosms.Visible = false;
            //    GridView1.Columns[0].Visible = false;
            //}
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            PanNoRecord.Visible = false;

            //    divnopage.Visible = true;
            //divBulkemail.Visible = true;
            //divbulksms.Visible = true;
            //Response.Write(dt.Rows.Count);

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    foreach (RepeaterItem item in rptpage.Items)
                    {
                        LinkButton senderbtn = (LinkButton)item.FindControl("lnkpagebtn");
                        HiddenField postIdHidden = (HiddenField)item.FindControl("psotId");
                        LinkButton lnkpagebtn = (LinkButton)item.FindControl("lnkpagebtn");
                        if (Convert.ToInt32(postIdHidden.Value) == custompageIndex)
                        {
                            senderbtn.CssClass = "Linkbutton activepage";
                        }
                    }
                    divnopage.Visible = true;

                    int iTotalRecords = Convert.ToInt32(hdncountdata.Value);
                    int iEndRecord = GridView1.PageSize * (custompageIndex);
                    //Response.Write(iEndRecord);
                    //Response.End();

                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {

                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }


                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

                }
            }
            else
            {

                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewnotes" || e.CommandName == "viewpromo")
        {

            string[] strids = e.CommandArgument.ToString().Split('|');
            string companyid = strids[0];
            string contactid = strids[1];

            //Profile.eurosolar.companyid = companyid;
            //Profile.eurosolar.contactid = contactid;
            //Profile.eurosolar.projectid = "";

            if (e.CommandName == "viewnotes")
            {
                Response.Redirect("~/admin/adminfiles/company/company.aspx?m=n");
            }
            if (e.CommandName == "viewpromo")
            {
                Response.Redirect("~/admin/adminfiles/company/company.aspx?m=p");
            }
        }
        if (e.CommandName.ToLower() == "editdetail")
        {

            ModalPopupExtenderEdit.Show();

            hndEditCustID.Value = e.CommandArgument.ToString();
            DataTable dtCont = ClstblContacts.tblContacts_SelectTop(hndEditCustID.Value);
            if (dtCont.Rows.Count > 0)
            {
                txtContMr.Text = dtCont.Rows[0]["ContMr"].ToString();
                txtContFirst.Text = dtCont.Rows[0]["ContFirst"].ToString();
                txtContLast.Text = dtCont.Rows[0]["ContLast"].ToString();
                txtContEmail.Text = dtCont.Rows[0]["ContEmail"].ToString();
                txtContMobile.Text = dtCont.Rows[0]["ContMobile"].ToString();

                SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(hndEditCustID.Value);
                txtCustPhone.Text = stCust.CustPhone;
                rblResCom1.Checked = false;
                rblResCom2.Checked = false;
                rblArea1.Checked = false;
                rblArea2.Checked = false;

                ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
                ddlformbayunittype.DataMember = "UnitType";
                ddlformbayunittype.DataTextField = "UnitType";
                ddlformbayunittype.DataValueField = "UnitType";
                ddlformbayunittype.DataBind();

                ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
                ddlformbaystreettype.DataMember = "StreetType";
                ddlformbaystreettype.DataTextField = "StreetType";
                ddlformbaystreettype.DataValueField = "StreetCode";
                ddlformbaystreettype.DataBind();

                if (stCust.ResCom == "1")
                {
                    rblResCom1.Checked = true;
                }
                if (stCust.ResCom == "2")
                {
                    rblResCom2.Checked = true;
                }
                if (stCust.Area != string.Empty)
                {
                    if (stCust.Area == "1")
                    {
                        rblArea1.Checked = true;
                    }
                    if (stCust.Area == "2")
                    {
                        rblArea2.Checked = true;
                    }
                }

                try
                {
                    txtStreetAddressline.Text = stCust.StreetAddress + ", " + stCust.StreetCity + " " + stCust.StreetState + " " + stCust.StreetPostCode;
                }
                catch { }

                txtformbayUnitNo.Text = stCust.unit_number;

                ddlformbayunittype.SelectedValue = stCust.unit_type;
                txtformbayStreetNo.Text = stCust.street_number;
                txtformbaystreetname.Text = stCust.street_name;
                try
                {
                    ddlformbaystreettype.SelectedValue = stCust.street_type;
                }
                catch { }
                txtStreetAddress.Text = stCust.StreetAddress;
                ddlStreetCity.Text = stCust.StreetCity;
                txtStreetState.Text = stCust.StreetState;
                txtStreetPostCode.Text = stCust.StreetPostCode;

            }
            BindScript();
            BindGrid(0);
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    //protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
    //    bool sucess1 = ClstblContacts.tblContacts_Delete(id);
    //    //--- do not chage this code start------
    //    if (sucess1)
    //    {
    //        SetDelete();
    //    }
    //    else
    //    {
    //        SetError();
    //    }
    //    GridView1.EditIndex = -1;
    //    BindGrid(0);
    //    BindGrid(1);
    //    //--- do not chage this code end------
    //}
    //protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{
    //    string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
    //    SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(id);

    //    InitUpdate();
    //}

    protected void chkViewAll_OnCheckedChanged(object sender, EventArgs e)
    {
        search();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            //custompagesize = 0;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            // custompagesize = Convert.ToInt32(ddlSelectRecords.SelectedValue.ToString());
        }
        BindGrid(0);
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("company.aspx");
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/company/leadtracker.aspx");
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        InitAdd();
    }

    public void search()
    {
        if (Roles.IsUserInRole("Administrator"))
        {
            BindGrid(0);
            BindScript();
        }
        else
        {
            if(txtSearchMobile.Text.Trim() != "" || txtSearchEmail.Text.Trim() != "" || txtProjectNo.Text.Trim() != "")
            {
                BindGrid(0);
                BindScript();
            }
            else if (txtSearchFirst.Text.Trim() != "" || txtSearchLast.Text.Trim() != "")
            {
                if (txtSerachCity.Text.Trim() != "")
                {
                    BindGrid(0);
                    BindScript();
                }
                else
                {
                    SetFocus(txtSerachCity);
                    MsgError("Please Enter City");
                }
            }
            else
            {
                if (txtSerachCity.Text.Trim() == "")
                {
                    MsgError("Please Enter Contact Details");
                }
                else
                {
                    BindGrid(0);
                    BindScript();
                }
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        search();
    }

    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }



    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
    }
    public void InitUpdate()
    {
        HidePanels();
    }
    private void HidePanels()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;

        divMail.Visible = false;
        divBulkemail.Visible = false;
        divbulksms.Visible = false;
        ModalPopupExtender1.Hide();
        ModalPopupExtender2.Hide();
    }
    public void Reset()
    {
        //ddlSearchType.SelectedValue = "0";
        //ddlSearchRec.SelectedValue = "0";
        //ddlSearchSource.SelectedValue = "0";
        //ddlSearchSubSource.SelectedValue = "0";
        //lstSearchStatus.DataSource = null;
        //lstSearchStatus.DataBind();
        //ddlsearchsalerap.SelectedValue = "0";
        //ddlTeam.SelectedValue = "0";
        //txtSearchClient.Text = string.Empty;
        //txtSearchFirst.Text = string.Empty;
        //txtSearchLast.Text = string.Empty;
        //txtSearchMobile.Text = string.Empty;
        //txtSearchPhone.Text = string.Empty;
        //txtSearchStreet.Text = string.Empty;
        //txtSerachCity.Text = string.Empty;
        //txtSearchPostCode.Text = string.Empty;
        //txtSearchCompanyNo.Text = string.Empty;
        //txtSearchEmail.Text = string.Empty;
        //txtStartDate.Text = string.Empty;
        //txtEndDate.Text = string.Empty;

    }
    protected void btnaddqualificationtitle_Onclick(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CreatedBy = st.EmployeeID;

        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        int success = 0;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            CheckBox chktag = (CheckBox)gridRow.FindControl("chktag");
            HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");

            if (chktag.Checked)
            {
                //if (chksaveaspromo.Checked)
                //{
                success = ClstblPromo.tblPromo_Insert(hndid.Value, txtpromoname.Text, "3", txtpromosent.Text, "0", ddlNews.SelectedValue, CreatedBy);
                //}
            }
        }
        resetbulkemail();
        //bindpromoemail();
    }
    protected void btnqualificationcancel_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        resetbulkemail();
        //bindpromoemail();
    }
    //protected void chksaveaspromo_OnCheckedChanged(object sender, EventArgs e)
    //{
    //    ModalPopupExtender2.Show();
    //    bindpromoemail();
    //}

    //public void bindpromoemail()
    //{
    //    if (chksaveaspromo.Checked.ToString() == "True")
    //    {
    //        txtpromoname.Text = string.Empty;
    //        txtpromosent.Text = DateTime.Now.AddHours(14).ToShortDateString();
    //        trpromoname.Visible = true;
    //        trpromosent.Visible = true;
    //    }
    //    else
    //    {
    //        txtpromoname.Text = string.Empty;
    //        txtpromosent.Text = DateTime.Now.AddHours(14).ToShortDateString();
    //        trpromoname.Visible = false;
    //        trpromosent.Visible = false;
    //    }
    //}

    public void resetbulkemail()
    {
        txtpromoname.Text = string.Empty;
        txtpromosent.Text = DateTime.Now.AddHours(14).ToShortDateString();
        //chksaveaspromo.Checked = false;
    }
    protected void btnbulksmsadd_Onclick(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CreatedBy = st.EmployeeID;

        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        int success = 0;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            CheckBox chktagsms = (CheckBox)gridRow.FindControl("chktagsms");
            HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");

            if (chktagsms.Checked)
            {
                if (chkpromosms.Checked)
                {
                    success = ClstblPromo.tblPromo_Insert(hndid.Value, txtpromonamesms.Text, "2", txtpromosentsms.Text, "0", "", CreatedBy);
                }
            }
        }
        resetbulksms();
        bindpromosms();
    }
    public void resetbulksms()
    {
        txtpromonamesms.Text = string.Empty;
        chkpromosms.Checked = false;
        txtpromosentsms.Text = DateTime.Now.AddHours(14).ToShortDateString();
    }
    protected void btnbulksmscancel_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
        resetbulksms();
        bindpromosms();
    }
    protected void chkpromosms_OnCheckedChanged(object sender, EventArgs e)
    {
        ModalPopupExtender1.Show();
        bindpromosms();
    }
    public void bindpromosms()
    {
        if (chkpromosms.Checked.ToString() == "True")
        {
            trpromonamesms.Visible = true;
            trpromodatesms.Visible = true;
            txtpromonamesms.Text = string.Empty;
            txtpromosentsms.Text = DateTime.Now.AddHours(14).ToShortDateString();
        }
        else
        {
            trpromonamesms.Visible = false;
            trpromodatesms.Visible = false;
            txtpromonamesms.Text = string.Empty;
            txtpromosentsms.Text = DateTime.Now.AddHours(14).ToShortDateString();
        }
    }

    protected void btnpromotracker_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("promotracker.aspx");
    }

    protected void chkemailheadertag_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkemailheadertag = (CheckBox)GridView1.HeaderRow.FindControl("chkemailheadertag");

        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktag = (CheckBox)row.FindControl("chktag");

                if (chkemailheadertag.Checked == true)
                {
                    chktag.Checked = true;
                    lbshowtagemail.Visible = true;
                }
                else
                {
                    chktag.Checked = false;
                    lbshowtagemail.Visible = false;
                }
            }
        }
    }
    protected void chktag_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkemailheadertag = (CheckBox)GridView1.HeaderRow.FindControl("chkemailheadertag");

        lbshowtagemail.Visible = false;
        foreach (GridViewRow row1 in GridView1.Rows)
        {
            if (row1.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktag = (CheckBox)row1.FindControl("chktag");
                if (chktag.Checked == true)
                {
                    chkemailheadertag.Checked = true;
                    lbshowtagemail.Visible = true;
                }
            }
        }
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktag = (CheckBox)row.FindControl("chktag");
                if (chktag.Checked == false)
                {
                    chkemailheadertag.Checked = false;
                }
            }
        }
    }
    protected void lbshowtagemail_OnClick(object sender, EventArgs e)
    {
        btnclear.Visible = false;
        foreach (GridViewRow row in GridView1.Rows)
        {
            row.Visible = true;
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktag = (CheckBox)row.FindControl("chktag");
                if (chktag.Checked == false || chktag.Visible == false)
                {
                    row.Visible = false;
                }
            }
        }
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chktag = (CheckBox)row.FindControl("chktag");
            if (chktag.Checked == true && chktag.Visible == true)
            {
                btnclear.Visible = true;
            }
        }
    }

    protected void chksmsheadertag_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chksmsheadertag = (CheckBox)GridView1.HeaderRow.FindControl("chksmsheadertag");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktagsms = (CheckBox)row.FindControl("chktagsms");

                if (chksmsheadertag.Checked == true)
                {
                    chktagsms.Checked = true;
                    lblshowtagsms.Visible = true;
                }
                else
                {
                    chktagsms.Checked = false;
                    lblshowtagsms.Visible = false;
                }
            }
        }
    }

    protected void chktagsms_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chksmsheadertag = (CheckBox)GridView1.HeaderRow.FindControl("chksmsheadertag");
        lblshowtagsms.Visible = false;
        foreach (GridViewRow row1 in GridView1.Rows)
        {
            if (row1.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktagsms = (CheckBox)row1.FindControl("chktagsms");
                if (chktagsms.Checked == true)
                {
                    chksmsheadertag.Checked = true;
                    lblshowtagsms.Visible = true;
                }
            }
        }
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktagsms = (CheckBox)row.FindControl("chktagsms");
                if (chktagsms.Checked == false)
                {
                    chksmsheadertag.Checked = false;
                }
            }
        }
    }

    protected void lblshowtagsms_OnClick(object sender, EventArgs e)
    {
        btnclear.Visible = false;
        foreach (GridViewRow row in GridView1.Rows)
        {
            row.Visible = true;
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktagsms = (CheckBox)row.FindControl("chktagsms");
                if (chktagsms.Checked == false || chktagsms.Visible == false)
                {
                    row.Visible = false;
                }
            }
        }
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chktagsms = (CheckBox)row.FindControl("chktagsms");
            if (chktagsms.Checked == true && chktagsms.Visible == true)
            {
                btnclear.Visible = true;
            }
        }
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        btnclear.Visible = false;
        lblshowtagsms.Visible = false;
        lbshowtagemail.Visible = false;
        BindGrid(0);
    }

    //protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        HyperLink lnkFullname = (HyperLink)e.Row.FindControl("lnkFullname");
    //        HyperLink lnkCustomer = (HyperLink)e.Row.FindControl("lnkCustomer");
    //        HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");

    //        SttblEmployees stSalesRep = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
    //        string SalesTeamRep = "";
    //        DataTable dt_empsalerep = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(hndEmployeeID.Value);
    //        if (dt_empsalerep.Rows.Count > 0)
    //        {
    //            foreach (DataRow dr in dt_empsalerep.Rows)
    //            {
    //                SalesTeamRep += dr["SalesTeamID"].ToString() + ",";
    //            }
    //            SalesTeamRep = SalesTeamRep.Substring(0, SalesTeamRep.Length - 1);
    //        }

    //        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
    //        string EmployeeID = stEmp.EmployeeID;
    //        ImageButton gvbtnUpdate = (ImageButton)e.Row.FindControl("gvbtnUpdate");

    //        if (Roles.IsUserInRole("Sales Manager"))
    //        {
    //            //string SalesTeamID = stEmp.SalesTeamID;
    //            string SalesTeam = "";
    //            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
    //            if (dt_empsale.Rows.Count > 0)
    //            {
    //                foreach (DataRow dr in dt_empsale.Rows)
    //                {
    //                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
    //                }
    //                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
    //            }
    //            string EmpType = stEmp.EmpType;
    //            //if (stSalesRep.EmpType == EmpType && stSalesRep.SalesTeamID == SalesTeamID.ToString())
    //            if (stSalesRep.EmpType == EmpType && SalesTeamRep == SalesTeam)
    //            {
    //                lnkFullname.Enabled = true;
    //                lnkCustomer.Enabled = true;
    //                gvbtnUpdate.Visible = true;
    //            }
    //            else
    //            {
    //                lnkFullname.Enabled = false;
    //                lnkCustomer.Enabled = false;
    //                gvbtnUpdate.Visible = false;
    //            }
    //        }
    //        if (Roles.IsUserInRole("SalesRep"))
    //        {
    //            if (EmployeeID == hndEmployeeID.Value)
    //            {
    //                lnkFullname.Enabled = true;
    //                lnkCustomer.Enabled = true;
    //            }
    //            else
    //            {
    //                lnkFullname.Enabled = false;
    //                lnkCustomer.Enabled = false;
    //            }

    //            HiddenField hndContactID = (HiddenField)e.Row.FindControl("hndContactID");
    //            SttblContacts stSRep = ClstblContacts.tblContacts_SelectByContactID(hndContactID.Value);

    //            if (stEmp.EmployeeID == stSRep.EmployeeID)
    //            {
    //                gvbtnUpdate.Visible = true;
    //            }
    //            else
    //            {
    //                gvbtnUpdate.Visible = false;
    //            }
    //        }
    //    }
    //}
    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    ddlSearchType.SelectedValue = "";
    //    txtSearchMobile.Text = string.Empty;
    //    txtSearchPhone.Text = string.Empty;
    //    ddlSearchRec.SelectedValue = "";
    //    ddlSearchSource.SelectedValue = "";
    //    txtSearchClient.Text = string.Empty;
    //    txtSearchCompanyNo.Text = string.Empty;
    //    txtSearchFirst.Text = string.Empty;
    //    txtSearchLast.Text = string.Empty;
    //    txtSearchPostCode.Text = string.Empty;
    //    //ddlSearchState.SelectedValue = "";
    //    txtSerachCity.Text = string.Empty;
    //    txtSearchEmail.Text = string.Empty;
    //    ddlTeam.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtSearchStreet.Text = string.Empty;
    //    // chkViewAll.Checked = false;
    //    ddlSearchSubSource.SelectedValue = "";

    //    string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //    SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
    //    if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
    //    {
    //        ddlsearchsalerap.SelectedValue = st.EmployeeID;
    //    }

    //    foreach (RepeaterItem item in lstSearchStatus.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        chkselect.Checked = false;
    //    }

    //    //BindGrid(0);
    //    if (GridView1.Rows.Count > 0)
    //    {
    //        GridView1.DataSource = null;
    //        GridView1.DataBind();

    //    }
    //    PanGrid.Visible = false;
    //    divBulkemail.Visible = false;
    //    divbulksms.Visible = false;
    //    //divnopage.Visible = false;
    //    //divpromosms.Visible = false;
    //}
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string userid1 = userid;
        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("PostInstaller")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("CompanyManager")))
        {
            userid1 = "";
        }
        else
        {
            if (chkViewAll.Checked)
            {
                if (!Roles.IsUserInRole("DSalesRep"))
                {
                    userid1 = "";
                }
            }
        }
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        DataTable dt = ClstblContacts.tblcontactsGetDataBySearch(ddlSearchType.SelectedValue, txtSearchMobile.Text.Trim(), txtSearchPhone.Text.Trim(), ddlSearchRec.SelectedValue, ddlSearchSource.SelectedValue, txtSearchClient.Text.Trim(), txtSearchCompanyNo.Text, txtSearchFirst.Text, txtSearchLast.Text, txtSearchPostCode.Text, "", txtSerachCity.Text, txtSearchEmail.Text, ddlsearchsalerap.SelectedValue, ddlTeam.SelectedValue, userid, userid1, txtStartDate.Text, txtEndDate.Text, selectedItem, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, txtProjectNo.Text);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Contacts" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 0, 4, 12, 5, 6, 7, 8, 10, 9 };
            string[] arrHeader = { "Contact", "Company", "Employee", "Street", "Suburb", "P/Cd", "State", "Lead Type", "Project Status" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void txtSearchStreet_TextChanged(object sender, EventArgs e)
    {
        if (txtSearchStreet.Text != string.Empty)
        {
            // RequiredFieldValidatorCode.Visible = true;
            //  RequiredFieldValidatorCity.Visible = true;
        }
        else
        {
            //  RequiredFieldValidatorCode.Visible = false;
            //  RequiredFieldValidatorCity.Visible = false;
        }
        BindScript();
    }
    protected void txtSearchPostCode_TextChanged(object sender, EventArgs e)
    {
        if (txtSearchPostCode.Text.Trim() != string.Empty)
        {
            //  RequiredFieldValidatorStreet.Visible = true;
            //  RequiredFieldValidatorCity.Visible = true;
        }
        BindScript();
    }
    protected void txtSerachCity_TextChanged(object sender, EventArgs e)
    {
        if (txtSerachCity.Text != string.Empty)
        {
            //  RequiredFieldValidatorCode.Visible = true;
            //  RequiredFieldValidatorStreet.Visible = true;
        }
    }
    protected void ddlSearchSource_SelectedIndexChanged(object sender, EventArgs e)
    {

        ddlSearchSubSource.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
        ddlSearchSubSource.Items.Add(lst);

        if (ddlSearchSource.SelectedValue != "")
        {

            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlSearchSource.SelectedValue);
            if (dt.Rows.Count > 0)
            {

                ddlSearchSubSource.DataSource = dt;
                ddlSearchSubSource.DataMember = "CustSourceSub";
                ddlSearchSubSource.DataTextField = "CustSourceSub";
                ddlSearchSubSource.DataValueField = "CustSourceSubID";
                ddlSearchSubSource.DataBind();
            }
        }
        //  ScriptManager.RegisterStartupScript(updatepanelgrid.GetType(), "MyAction", "doMyAction();", true);
    }


    protected void ibtnEditDetail_Click(object sender, EventArgs e)
    {
        BindScript();
        ModalPopupExtenderEdit.Show();
        string ResCom = "0";
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }
        string Area = "0";
        if (rblArea1.Checked == true)
        {
            Area = "1";
        }
        if (rblArea2.Checked == true)
        {
            Area = "2";
        }

        string formbayUnitNo = txtformbayUnitNo.Text.Trim();
        string formbayunittype = ddlformbayunittype.SelectedValue.Trim();
        string formbayStreetNo = txtformbayStreetNo.Text.Trim();
        string formbaystreetname = txtformbaystreetname.Text.Trim();
        string formbaystreettype = ddlformbaystreettype.SelectedValue.Trim();
        string StreetCity = ddlStreetCity.Text.Trim();
        string StreetState = txtStreetState.Text.Trim();
        string StreetPostCode = txtStreetPostCode.Text.Trim();
        string StreetAddress = formbayunittype.Trim() + " " + formbayUnitNo.Trim() + " " + formbayStreetNo.Trim() + " " + formbaystreetname.Trim() + " " + formbaystreettype.Trim();
        //txtStreetAddress.Text;

        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(hndEditCustID.Value);
        string id = hndEditCustID.Value;
        if (dtCont.Rows.Count > 0)
        {
            //int existaddress = ClstblCustomers.tblCustomers_Exits_Address(formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, StreetCity, StreetState, StreetPostCode);
            DataTable dtaddress = ClstblCustomers.tblCustomers_Exits_Select_StreetAddressByID(id, StreetAddress, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim());
            DataTable dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID(id, txtContMobile.Text.Trim(), txtContEmail.Text);
            if (dtaddress.Rows.Count == 0)
            {
                if (dtMobileEmailExist.Rows.Count == 0)
                {
                    //Response.Write(existaddress);
                    // Response.End();
                    lbleror.Visible = false;
                    lbleror1.Visible = false;
                    string ContactID = dtCont.Rows[0]["ContactID"].ToString();
                    bool succ = ClstblCustomers.tblCustomer_Update_Address(hndEditCustID.Value, ddlformbayunittype.SelectedValue.Trim(), txtformbayUnitNo.Text.Trim(), txtformbayStreetNo.Text.Trim(), txtformbaystreetname.Text.Trim(), ddlformbaystreettype.SelectedValue.Trim(), hndstreetsuffix.Value);
                    bool suc = ClstblContacts.tblContacts_UpdateCustDetail(ContactID, txtContMr.Text, txtContFirst.Text, txtContLast.Text, txtContMobile.Text.Trim(), txtContEmail.Text);
                    ClstblCustomers.tblCustomers_UpdateAddress(hndEditCustID.Value, StreetAddress, ddlStreetCity.Text, txtStreetState.Text, txtStreetPostCode.Text, txtContFirst.Text + " " + txtContLast.Text);
                    if (suc)
                    {
                        bool Suc1 = ClstblCustomers.tblCustomers_UpdateInContact(hndEditCustID.Value, ResCom, txtCustPhone.Text, Area);
                        ModalPopupExtenderEdit.Hide();
                        openModal = "false";
                        BindGrid(0);
                    }
                }
                else
                {
                    lbleror1.Visible = true;
                    openModal = "true";
                    ModalPopupExtenderEdit.Show();
                }
            }
            else
            {

                lbleror.Visible = true;
                openModal = "true";
                ModalPopupExtenderEdit.Show();
            }
        }
    }
    protected void ddlStreetCity_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        if (ddlStreetCity.Text != string.Empty)
        {
            DataTable dtStreet = ClstblCustomers.tblPostCodes_SelectBy_PostCodeID(ddlStreetCity.Text);
            if (dtStreet.Rows.Count > 0)
            {
                txtStreetState.Text = dtStreet.Rows[0]["State"].ToString();
                txtStreetPostCode.Text = dtStreet.Rows[0]["PostCode"].ToString();
            }
            streetaddress();
            string[] cityarr = ddlStreetCity.Text.Split('|');
            if (cityarr.Length > 1)
            {
                ddlStreetCity.Text = cityarr[0].Trim();
                txtStreetState.Text = cityarr[1].Trim();
                txtStreetPostCode.Text = cityarr[2].Trim();
            }
            txtContEmail.Focus();
            //checkExistsAddress();
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        //GridViewRow gvrow = GridView1.BottomPagerRow;
        //Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        //lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        //int[] page = new int[7];
        //page[0] = GridView1.PageIndex - 2;
        //page[1] = GridView1.PageIndex - 1;
        //page[2] = GridView1.PageIndex;
        //page[3] = GridView1.PageIndex + 1;
        //page[4] = GridView1.PageIndex + 2;
        //page[5] = GridView1.PageIndex + 3;
        //page[6] = GridView1.PageIndex + 4;
        //for (int i = 0; i < 7; i++)
        //{
        //    if (i != 3)
        //    {
        //        if (page[i] < 1 || page[i] > GridView1.PageCount)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Visible = false;
        //        }
        //        else
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Text = Convert.ToString(page[i]);
        //            lnkbtn.CommandName = "PageNo";
        //            lnkbtn.CommandArgument = lnkbtn.Text;

        //        }
        //    }
        //}
        //if (GridView1.PageIndex == 0)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
        //    lnkbtn.Visible = false;

        //}
        //if (GridView1.PageIndex == GridView1.PageCount - 1)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //    lnkbtn.Visible = false;

        //}
        //Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        //if (dv.ToTable().Rows.Count > 0)
        //{
        //    int iTotalRecords = dv.ToTable().Rows.Count;
        //    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //    if (iEndRecord > iTotalRecords)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    if (iStartsRecods == 0)
        //    {
        //        iStartsRecods = 1;
        //    }
        //    if (iEndRecord == 0)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        //}
        //else
        //{
        //    ltrPage.Text = "";
        //}
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.Pager)
        //{
        //    GridViewRow gvr = e.Row;
        //    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p1");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p2");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p4");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p5");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p6");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //}
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    //protected void btnpromooffer_Click(object sender, EventArgs e)
    //{
    //    string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //    SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
    //    string CreatedBy = st.EmployeeID;

    //    int rowsCount = GridView1.Rows.Count;
    //    GridViewRow gridRow;
    //    int success = 0;
    //    for (int i = 0; i < rowsCount; i++)
    //    {
    //        string id;
    //        gridRow = GridView1.Rows[i];
    //        id = GridView1.DataKeys[i].Value.ToString();

    //        CheckBox chktagsms = (CheckBox)gridRow.FindControl("chktagsms");
    //        HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");
    //        if (ddlpromooffer.SelectedValue != string.Empty)
    //        {
    //            if (chktagsms.Checked)
    //            {
    //                if (hndid.Value != string.Empty)
    //                {
    //                    SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(hndid.Value);
    //                    SttblPromoOffer stPromooffer = ClstblPromoOffer.tblPromoOffer_SelectByPromoOfferID(ddlpromooffer.SelectedValue);
    //                    if (stContact.ContMobile != string.Empty && stPromooffer.Description !=string.Empty)
    //                    {
    //                        string AccountSid = "AC991f3f103f6a5f608278c1e283f139a1";
    //                        string AuthToken = "be9c00d8443b360082ab02d3d78969b2";
    //                        var twilio = new TwilioRestClient(AccountSid, AuthToken);//919879111739
    //                        //var message = twilio.SendMessage("+61428258614", "+61451831980", "Hello!!");
    //                        var message = twilio.SendMessage("+61428258614", stContact.ContMobile, stPromooffer.Description);
    //                        //Response.Write(message.Sid);
    //                        if (message.RestException != null)
    //                        {
    //                            var error = message.RestException.Message;
    //                            //Response.Write(error);
    //                            // handle the error ...
    //                        }
    //                        else
    //                        {
    //                            ClstblPromo.tblPromo_UpdateIsArchive(stContact.ContMobile, true.ToString());
    //                            success = ClstblPromo.tblPromo_Insert(hndid.Value, ddlpromooffer.SelectedItem.Text, "2", DateTime.Now.ToString(), "0", "", CreatedBy);
    //                            ClstblPromo.tblPromo_UpdatePromoOfferID(success.ToString(), ddlpromooffer.SelectedValue.ToString());
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }

    //}
    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    lnkpagebtn.CssClass = "active";
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        lastpageindex = Convert.ToInt32(hdncountdata.Value) / Convert.ToInt32(ddlSelectRecords.SelectedValue);



        if (Convert.ToInt32(hdncountdata.Value) % Convert.ToInt32(ddlSelectRecords.SelectedValue) > 0)
        {
            lastpageindex = lastpageindex + 1;
        }
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    [WebMethod]
    public static bool Getstreetname(string streetname)
    {
        string data = "select StreetName from tbl_formbaystreetname where StreetName='" + streetname + "'";
        DataTable dt = ClstblCustomers.query_execute(data);
        string desc = string.Empty;
        if (dt.Rows.Count > 0)
        {
            desc = dt.Rows[0]["StreetName"].ToString();
            return true;
        }
        else
        {
            return false;
        }
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchType.SelectedValue = "";
        txtSearchMobile.Text = string.Empty;
        txtSearchPhone.Text = string.Empty;
        ddlSearchRec.SelectedValue = "";
        ddlSearchSource.SelectedValue = "";
        txtSearchClient.Text = string.Empty;
        txtSearchCompanyNo.Text = string.Empty;
        txtSearchFirst.Text = string.Empty;
        txtSearchLast.Text = string.Empty;
        txtSearchPostCode.Text = string.Empty;
        //ddlSearchState.SelectedValue = "";
        txtSerachCity.Text = string.Empty;
        txtSearchEmail.Text = string.Empty;
        ddlTeam.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchStreet.Text = string.Empty;
        txtProjectNo.Text = string.Empty;
        // chkViewAll.Checked = false;
        ddlSearchSubSource.SelectedValue = "";

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {
            ddlsearchsalerap.SelectedValue = st.EmployeeID;
        }

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
        BindDataCount();
        //if (GridView1.Rows.Count > 0)
        //{
        //    GridView1.DataSource = null;
        //    GridView1.DataBind();

        //}
        //PanGrid.Visible = true;
        divBulkemail.Visible = false;
        divbulksms.Visible = false;
        //divnopage.Visible = false;
        //divpromosms.Visible = false;
    }

    protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink lnkFullname = (HyperLink)e.Row.FindControl("lnkFullname");
            HyperLink lnkCustomer = (HyperLink)e.Row.FindControl("lnkCustomer");
            HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");
            Label lblfullname = (Label)e.Row.FindControl("lblFullname");
            Label lblcustomer = (Label)e.Row.FindControl("lblCust");

            SttblEmployees stSalesRep = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
            string SalesTeamRep = "";
            DataTable dt_empsalerep = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(hndEmployeeID.Value);
            if (dt_empsalerep.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsalerep.Rows)
                {
                    SalesTeamRep += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeamRep = SalesTeamRep.Substring(0, SalesTeamRep.Length - 1);
            }

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;
            LinkButton gvbtnUpdate = (LinkButton)e.Row.FindControl("gvbtnUpdate");

            if (Roles.IsUserInRole("Sales Manager"))
            {
                //string SalesTeamID = stEmp.SalesTeamID;
                string SalesTeam = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }
                string EmpType = stEmp.EmpType;
                //if (stSalesRep.EmpType == EmpType && stSalesRep.SalesTeamID == SalesTeamID.ToString())
                if (stSalesRep.EmpType == EmpType && SalesTeamRep == SalesTeam)
                {
                    lnkFullname.Enabled = true;
                    lnkCustomer.Enabled = true;
                    gvbtnUpdate.Visible = true;
                }
                else
                {
                    lnkFullname.Visible = false;
                    lblfullname.Visible = true;
                    lnkCustomer.Visible = false;
                    lblcustomer.Visible = true;
                    gvbtnUpdate.Visible = false;
                }
            }
            if (Roles.IsUserInRole("Installation Manager"))
            {

                SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                if (st1.EmployeeID != hndEmployeeID.Value)
                {
                    lblfullname.Visible = true;
                    lnkFullname.Visible = false;
                    lblcustomer.Visible = true;
                    lnkCustomer.Visible = false;
                    gvbtnUpdate.Visible = false;
                }
                //else
                //{
                //    gvbtnDelete.Visible = false;
                //    gvbtnUpdate.Visible = false;
                //}

            }
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Lead Manager"))
            {
                if (EmployeeID == hndEmployeeID.Value)
                {
                    lnkFullname.Enabled = true;
                    lnkCustomer.Enabled = true;
                }
                else
                {
                    lnkFullname.Enabled = false;
                    lnkCustomer.Enabled = false;
                }

                HiddenField hndContactID = (HiddenField)e.Row.FindControl("hndContactID");
                SttblContacts stSRep = ClstblContacts.tblContacts_SelectByContactID(hndContactID.Value);

                if (stEmp.EmployeeID == stSRep.EmployeeID)
                {
                    gvbtnUpdate.Visible = true;
                }
                else
                {
                    gvbtnUpdate.Visible = false;
                }
            }

            if (Roles.IsUserInRole("PostInstaller"))
            {
                gvbtnUpdate.Visible = false;
            }

            if (Roles.IsUserInRole("Administrator"))
            {
                lnkFullname.Enabled = true;
            }
            else
            {
                lnkFullname.Enabled = false;
            }
        }
    }
    protected void GridView1_RowDeleting1(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        bool sucess1 = ClstblContacts.tblContacts_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
    }
    protected void GridView1_SelectedIndexChanging1(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(id);

        InitUpdate();
    }


    //    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    //    {
    //        updatestreetaddress();
    //    }
    //    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    //    {

    //            updatestreetaddress();
    //            ModalPopupExtenderEdit.Show();
    //    }
    //    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    //    {
    //        updatestreetaddress();
    //    }
    //    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    //    {
    //        updatestreetaddress();
    //    }
    //    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    //    {
    //        updatestreetaddress();
    //    }
    //    public void updatestreetaddress()
    //    {
    //        ModalPopupExtenderEdit.Show();
    //        txtStreetAddress.Text = ddlformbayunittype.SelectedValue.ToString() + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue.ToString(); ;
    //    }

    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {

        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();     
        BindScript();
        ddlformbayunittype.Focus();
    }


    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();     
        BindScript();
        txtformbayStreetNo.Focus();

    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        txtformbaystreetname.Focus();
    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);

        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();

        BindScript();
        ddlformbaystreettype.Focus();


        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);


    }
    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        ddlStreetCity.Focus();
    }

    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtStreetAddress.Text = address;
        lbleror.Visible = false;
        lbleror1.Visible = false;
        BindScript();

    }

    public void checkExistsAddress()
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string StreetAddress = txtStreetAddress.Text;
        int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);

        if (dt1.Rows.Count > 0)
        {
            lbleror.Visible = true;
        }
        else
        {
            lbleror.Visible = false;
        }
    }
    protected void btnCancelEdit_Click(object sender, EventArgs e)
    {
        lbleror.Visible = false;
        lbleror1.Visible = false;
        openModal = "false";
    }

}