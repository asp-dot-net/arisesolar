<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="contacts.aspx.cs" Inherits="admin_adminfiles_master_contacts" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <style>
        .groupalign {
            vertical-align: top !important;
        }
        .dnone{ display:none }
    </style>
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>

    <script language="javascript" type="text/javascript">
        // defult enter
        function DefaultEnter(evnt) {

            var btn = $get('#<%=btnSearch.ClientID %>');
            alert(btn.text);
            var x1 = btn.id;
            if (evnt.keyCode == Sys.UI.Key.enter) {
                //__doPostBack('" + btn.id + "', '');
                location = document.getElementById('<%=btnSearch.ClientID %>').href;
                return false;
            }
            return true;
        }
    </script>

    <script>
        function doMyAction() {
            //$(".ddlval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$(".ddlval1").select2({
            //    minimumResultsForSearch: -1
            //});
            $('#<%=ibtnEditDetail.ClientID %>').click(function () {

                formValidate();
            });


            $("[data-toggle=tooltip]").tooltip();

            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //$(".ddlval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$(".ddlval1").select2({
            //    minimumResultsForSearch: -1
            //});
        }

        function address() {

            //=============Street 
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

            $.ajax({
                type: "POST",
                url: "contacts.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                        $("#chkaddval").val("1");
                    }
                    else {
                        $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                        $("#chkaddval").val("0");
                    }
                }
            });
            // select2 - container-active

        }

    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
        function validatestreetAddress(source, args) {
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
            $.ajax({
                type: "POST",
                //action:"continue.aspx",
                url: "contacts.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        document.getElementById("Divvalidstreetname").style.display = "block";
                        document.getElementById("DivInvalidstreetname").style.display = "none";
                        $("#chkaddval").val("1");
                    }
                    else {
                        document.getElementById("Divvalidstreetname").style.display = "none";
                        document.getElementById("DivInvalidstreetname").style.display = "block";
                        $("#chkaddval").val("0");
                    }
                }
            });
            var mydataval = $("#chkaddval").val();
            if (mydataval == "1") {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }


    </script>


    <script type="text/javascript">
        $(document).ready(function () {

            $("[data-toggle=tooltip]").tooltip();
            doMyAction();
            $('#<%=btnSearch.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=ibtnEditDetail.ClientID %>').click(function () {
                //alert('2');
                formValidate();
            });
        });

        function doMyAction() {
            $('#<%=ibtnEditDetail.ClientID %>').click(function (e) {

                formValidate();
                //DropDown Validation

                var drpdiv = document.getElementsByClassName("drpValidate");
                var flag = 0;

                for (i = 0; i < drpdiv.length; i++) {


                    if ($(drpdiv[i]).find(".myval").val() == "") {

                        $(drpdiv[i]).addClass("errormassage");

                        flag = flag + 1;
                    }

                }

                if (flag > 0) {
                    e.preventDefault();
                }
                //End DropDown Validation
            });



            $('#<%= btnSearch.ClientID %>').click(function () {
                formValidate();
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        }
        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }

        function ChkFun(source, args) {
            validateAddress();
            var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

            if (elem == '1') {
                args.IsValid = true;
            }
            else {

                args.IsValid = false;
            }
        }


    </script>
    <asp:Panel runat="server" ID="PanelCreateEdit" DefaultButton="btnSearch">


        <div class="page-body headertopbox">
            <h5 class="row-title"><i class="typcn typcn-th-small"></i>Contact</h5>
            <div id="hbreadcrumb" class="pull-right">
                <div id="fdbd" class="pull-right" runat="server">
                    <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                    <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>

                    <%-- </ol>--%>
                </div>
            </div>
        </div>




        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);
            function beginrequesthandler(sender, args) {

            }
            function endrequesthandler(sender, args) {

            }
            function pageLoaded() {
                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });

                $("[data-toggle=tooltip]").tooltip();
            }

            function validatestreetAddress(source, args) {
                var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

                        $.ajax({
                            type: "POST",
                            url: "contacts.aspx/Getstreetname",
                            data: "{'streetname':'" + streetname + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                //alert(data.d);
                                if (data.d == true) {
                                    $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                    $("#chkaddval").val("1");
                                }
                                else {
                                    $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                    $("#chkaddval").val("0");
                                }
                            }
                        });

                        var mydataval = $("#chkaddval").val();
                        if (mydataval == "1") {
                            args.IsValid = true;
                        }
                        else {
                            args.IsValid = false;
                        }
                    }
        </script>

        <script>
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_pageLoaded(pageLoaded);
                    function pageLoaded() {
                        $(".myvalcontact").select2({
                            //placeholder: "select",
                            allowclear: true
                        });


                    }


        </script>
        <script>
                    var focusedElementId = "";

                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_pageLoaded(pageLoadedpro);
                    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(beginrequesthandler);
                    // raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(endrequesthandler);

                    function beginrequesthandler(sender, args) {
                        //shows the modal popup - the update progress
                        $('.loading-container').css('display', 'block');

                    }
                    function endrequesthandler(sender, args) {
                        //hide the modal popup - the update progress
                    }

                    function pageLoadedpro() {

                        $('#<%=ibtnEditDetail.ClientID %>').click(function (e) {

                            formValidate();
                            //DropDown Validation

                            var drpdiv = document.getElementsByClassName("drpValidate");
                            var flag = 0;

                            for (i = 0; i < drpdiv.length; i++) {


                                if ($(drpdiv[i]).find(".myval").val() == "") {

                                    $(drpdiv[i]).addClass("errormassage");

                                    flag = flag + 1;
                                }

                            }

                            if (flag > 0) {
                                e.preventDefault();
                            }
                            //End DropDown Validation
                        });


                        $('.loading-container').css('display', 'none');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();

                    }
        </script>
        <asp:UpdatePanel ID="updatepanelgrid" runat="server">
            <ContentTemplate>
            

    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="animate-panel" style="padding-bottom: 0px!important;">
                <div class="messesgarea">
                    <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                    </div>
                    <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                    </div>
                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>


                <div class="searchfinal" onkeypress="DefaultEnter(event);">
                    <div class="widget-body shadownone brdrgray">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="dataTables_filter">
                                <div class="row">
                                    <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                        <tr>
                                            <td>

                                                <div class="inlineblock ">
                                                    <div class="col-sm-12">
                                                        <div class="input-group col-sm-1" id="type" runat="server">
                                                            <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px" Enabled='<%# Roles.IsUserInRole("SalesRep") ? false : true %>'>
                                                                <asp:ListItem Value="" Text="Type"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group  col-sm-1" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlSearchRec" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" >
                                                                <asp:ListItem Value="">Rec/Com</asp:ListItem>
                                                                <asp:ListItem Value="1">Residential</asp:ListItem>
                                                                <asp:ListItem Value="2">Commercial</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-2" id="div2" runat="server">
                                                            <asp:DropDownList ID="ddlSearchSource" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="true" OnSelectedIndexChanged="ddlSearchSource_SelectedIndexChanged">
                                                                <asp:ListItem Value="">Source</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-2" id="div3" runat="server">
                                                            <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="form-group groupalign spical multiselect input-group" style="width: 213px;">
                                                            <dl class="dropdown">
                                                                <dt>
                                                                    <a href="#">
                                                                        <span class="hida" id="spanselect">Project Status</span>
                                                                        <p class="multiSel"></p>
                                                                    </a>
                                                                </dt>
                                                                <dd id="ddproject" runat="server">
                                                                    <div class="mutliSelect" id="mutliSelect">
                                                                        <ul>
                                                                            <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li>
                                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                                        <label for='<%# Container.FindControl("chkselect").ClientID %>' runat="server" id="lblrmo">
                                                                                            <span></span>
                                                                                        </label>
                                                                                        <label class="chkval">
                                                                                            <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal></label>
                                                                                    </li>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </div>
                                                                </dd>
                                                            </dl>
                                                        </div>

                                                        <div class="input-group col-sm-2" id="tdSalesRep" runat="server" style="width: 191px;">
                                                            <asp:DropDownList ID="ddlsearchsalerap" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>


                                                    </div>
                                                </div>


                                                <div class="inlineblock ">
                                                    <div class="col-sm-12">
                                                        <div class="input-group col-sm-1" id="div5" runat="server">
                                                            <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Team</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group groupalign col-sm-2">
                                                            <asp:TextBox ID="txtSearchCompanyNo" runat="server" ValidationGroup="searchcontact" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtSearchCompanyNo"
                                                                WatermarkText="Company Number" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearchCompanyNo" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="searchcontact"
                                                                ControlToValidate="txtSearchCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>
                                                        <div class="input-group autocompletedropdown col-sm-2" style="display:none">
                                                            <asp:TextBox ID="txtSearchClient" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchClient"
                                                                WatermarkText="Company" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server" UseContextKey="true" TargetControlID="txtSearchClient" ServicePath="~/Search.asmx"
                                                                ServiceMethod="GetCompanyList" EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>

                                                        <div class="input-group groupalign col-sm-1">
                                                            <asp:TextBox ID="txtSearchFirst" runat="server" ValidationGroup="searchcontact" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSearchFirst"
                                                                WatermarkText="Contact First" />
                                                            <cc1:FilteredTextBoxExtender TargetControlID="txtSearchFirst" runat="server" FilterMode="InvalidChars"
                                                                FilterType="Custom" InvalidChars=" "></cc1:FilteredTextBoxExtender>
                                                        </div>
                                                        <div class="input-group groupalign col-sm-1">
                                                            <asp:TextBox ID="txtSearchLast" runat="server" ValidationGroup="searchcontact" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtSearchLast"
                                                                WatermarkText="Contact Last" />
                                                            <cc1:FilteredTextBoxExtender TargetControlID="txtSearchLast" runat="server" FilterMode="InvalidChars"
                                                                FilterType="Custom" InvalidChars=" "></cc1:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="input-group groupalign col-sm-1" style="width: 155px">
                                                            <asp:TextBox ID="txtSearchMobile" runat="server" ValidationGroup="searchcontact" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtSearchMobile"
                                                                WatermarkText="Mobile" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtSearchMobile">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                        <div class="input-group groupalign col-sm-1" style="width: 113px">
                                                            <asp:TextBox ID="txtProjectNo" CssClass="form-control" runat="server" placeholder="Project No." />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtProjectNo">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>



                                                    </div>
                                                </div>

                                                <div class="inlineblock ">
                                                    <div class="col-sm-12">

                                                        <div class="input-group groupalign col-sm-1" style="width: 155px">
                                                            <asp:TextBox ID="txtSearchPhone" runat="server" ValidationGroup="searchcontact" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtSearchPhone" WatermarkText="Phone" />
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtSearchPhone">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>



                                                        <div class="input-group groupalign col-sm-1">
                                                            <asp:TextBox ID="txtSearchStreet" runat="server" ValidationGroup="searchcontact" OnTextChanged="txtSearchStreet_TextChanged" placeholder="Street" CssClass="form-control"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" Display="dynamic"
                                                                ControlToValidate="txtSearchStreet" ErrorMessage=""></asp:RequiredFieldValidator>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidatorStreet" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="txtSearchStreet" Display="Dynamic" Visible="false" ValidationGroup="searchcontact"></asp:RequiredFieldValidator>--%>
                                                        </div>

                                                        <div class="input-group groupalign col-sm-1 autocompletedropdown">

                                                            <asp:TextBox ID="txtSerachCity" runat="server" ValidationGroup="searchcontact" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtSerachCity"
                                                                WatermarkText="City" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" Display="dynamic"
                                                                ControlToValidate="txtSerachCity" ErrorMessage=""></asp:RequiredFieldValidator>

                                                            <%--     <asp:RequiredFieldValidator ID="RequiredFieldValidatorCity" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="txtSerachCity" Display="Dynamic" ValidationGroup="searchcontact" Visible="false"></asp:RequiredFieldValidator>--%>
                                                        </div>

                                                        <div class="input-group groupalign col-sm-1 autocompletedropdown">
                                                            <asp:TextBox ID="txtSearchPostCode" runat="server" ValidationGroup="searchcontact"
                                                                CssClass="form-control "></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                                                                TargetControlID="txtSearchPostCode" WatermarkText="Post Code" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" Display="dynamic"
                                                                ControlToValidate="txtSearchPostCode" ErrorMessage=""></asp:RequiredFieldValidator>


                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidatorCode" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                            ControlToValidate="txtSearchPostCode" Display="Dynamic" Visible="false" ValidationGroup="searchcontact"></asp:RequiredFieldValidator>--%>
                                                        </div>



                                                        <div class="input-group groupalign col-sm-1" style="width: 228px">
                                                            <asp:TextBox ID="txtSearchEmail" runat="server" ValidationGroup="searchcontact" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchEmail"
                                                                WatermarkText="Email" />
                                                        </div>


                                                        <div class="input-group date datetimepicker1 col-sm-1" style="display:none">
                                                            <span class="input-group-addon martop15">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="From" runat="server" class="form-control"></asp:TextBox>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                    ControlToValidate="txtStartDate" ErrorMessage=""></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-1" style="display:none">
                                                            <span class="input-group-addon martop15">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="To" runat="server" class="form-control"></asp:TextBox>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                                                    ControlToValidate="txtEndDate" ErrorMessage=""></asp:RequiredFieldValidator>--%>
                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                    Display="Dynamic" ValidationGroup=""></asp:CompareValidator>--%>
                                                        </div>

                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn redreq btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="inlineblock ">
                                                    <div class="col-sm-12">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                            <div class="datashowbox">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="row">
                                        <div class="dataTables_length showdata col-sm-8">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="padtopzero">
                                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>

                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="pull-right martop7 marleft8">
                                                <div class="checkbox-info ">
                                                    <!-- <a href="#" class="btn btn-darkorange btn-xs Excel"><i class="fa fa-check-square-o"></i> View All</a>-->

                                                    <span valign="top" id="tdAll2" class="paddtop3td alignchkbox btnviewallorange" runat="server" align="right">
                                                        <label for="<%=chkViewAll.ClientID %>" class="btn btn-darkorange btn-xs">
                                                            <asp:CheckBox ID="chkViewAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkViewAll_OnCheckedChanged" />
                                                            <span class="text">&nbsp;View All</span>&nbsp;
                                                           
                                                        </label>
                                                        <asp:LinkButton ID="lbtnExport" runat="server" CausesValidation="false" class="btn btn-success btn-xs Excel" data-original-title="Excel Export" data-placement="left" data-toggle="tooltip" OnClick="lbtnExport_Click" title=""><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                    </span>

                                                </div>
                                                <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%><%-- </ol>--%>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="finalgrid">
                    <div class="animate-panel">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hpanel marbtmzero">
                                    <%--<div class="panel-heading">
                            <div class="panel-tools">
                            <br />
                        </div>--%>
                                    <div>

                                        <script>
                                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                                            prm.add_pageLoaded(pageLoaded);
                                            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                                            prm.add_beginRequest(beginrequesthandler);
                                            // raised after an asynchronous postback is finished and control has been returned to the browser.
                                            prm.add_endRequest(endrequesthandler);
                                            function beginrequesthandler(sender, args) {

                                            }
                                            function endrequesthandler(sender, args) {
                                                //hide the modal popup - the update progress


                                                //if (args.get_error() != undefined) {
                                                //    args.set_errorhandled(true);
                                                //}

                                                $(".dropdown dt a").on('click', function () {
                                                    $(".dropdown dd ul").slideToggle('fast');
                                                });

                                                $(".dropdown dd ul li a").on('click', function () {
                                                    $(".dropdown dd ul").hide();
                                                });


                                                $(document).bind('click', function (e) {
                                                    var $clicked = $(e.target);
                                                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                                                });

                                                //callMultiCheckbox();
                                            }

                                            function pageLoaded() {
                                                $('.datetimepicker1').datetimepicker({
                                                    format: 'DD/MM/YYYY'
                                                });


                                            }



                                        </script>
                                        <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
                                            CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLEdit">
                                        </cc1:ModalPopupExtender>

                                        <div class="modal_popup" id="divEdit" runat="server" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="color-line"></div>
                                                    <div class="modal-header">
                                                        <div style="float: right">
                                                            <asp:Button ID="btnCancelEdit" Width="65px" CausesValidation="false" data-dismiss="modal" OnClick="btnCancelEdit_Click" runat="server" CssClass="btn btn-danger btncancelicon" Text="Close" />


                                                        </div>
                                                        <h4 class="modal-title" id="H3">Update Customer Detail</h4>
                                                    </div>
                                                    <div class="modal-body paddnone" style="overflow-y: scroll; height: 500px;">
                                                        <div class="panel-body">
                                                            <div class="formainline formnew">

                                                                <div class="alert alert-info" id="lbleror" runat="server" visible="false">
                                                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this address aleardy Exist</strong>
                                                                </div>
                                                                <div class="alert alert-info" id="lbleror1" runat="server" visible="false">
                                                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this Email or Mobile aleardy Exist</strong>
                                                                </div>

                                                                <div class="clear"></div>
                                                                <div class="form-group spical">
                                                                    <span class="col-md-12">
                                                                        <label>
                                                                            Name:
                                                                        </label>
                                                                    </span>
                                                                    <span>
                                                                        <div class="onelindiv">
                                                                            <span class="mrdiv col-md-4">
                                                                                <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                                    placeholder="Salutation"></asp:TextBox>
                                                                            </span>
                                                                            <span class="fistname col-md-4">
                                                                                <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                                    placeholder="First Name"></asp:TextBox>

                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                                                    ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                            </span>
                                                                            <span class="lastname col-md-4">
                                                                                <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                                    placeholder="Last Name"></asp:TextBox>
                                                                            </span>
                                                                        </div>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div id="Div6" class="form-group spicaldivin" runat="server" visible="false">
                                                                    <span class="name col-md-12">
                                                                        <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                                                    </span><span class="col-md-12">
                                                                        <asp:HiddenField ID="hndaddress" runat="server" />
                                                                        <asp:TextBox ID="txtStreetAddressline" runat="server" MaxLength="50" CssClass="form-control" onblur="getParsedAddress();"></asp:TextBox>
                                                                        <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company"
                                                                            ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass="comperror"
                                                                            ControlToValidate="txtStreetAddressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <asp:HiddenField ID="hndstreetno" runat="server" />
                                                                    <asp:HiddenField ID="hndstreetname" runat="server" />
                                                                    <asp:HiddenField ID="hndstreettype" runat="server" />
                                                                    <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                                    <asp:HiddenField ID="hndunittype" runat="server" />
                                                                    <asp:HiddenField ID="hndunitno" runat="server" />
                                                                    <div id="validaddressid" style="display: none">
                                                                        <img src="../../../images/check.png" alt="check">Address is valid.
                                                                    </div>
                                                                    <div id="invalidaddressid" style="display: none">
                                                                        <img src="../../../images/x.png" alt="cross">
                                                                        Address is invalid.
                                                                    </div>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                        <ContentTemplate>--%>


                                                                <div class="form-group spicaldivin">
                                                                    <span class="name col-md-12">
                                                                        <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                                                    </span><span class="col-md-12">
                                                                        <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                                            ControlToValidate="txtStreetAddress" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>



                                                                <div class="form-group spicaldivin">
                                                                    <span class="col-md-6">
                                                                        <label class="control-label">Unit No<span class="symbol required"></span> </label>
                                                                        <asp:TextBox ID="txtformbayUnitNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayUnitNo_TextChanged"></asp:TextBox>
                                                                    </span>
                                                                    <span class="col-md-6">
                                                                        <label class="control-label">Unit Type<span class="symbol required"></span> </label>

                                                                        <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnTextChanged="ddlformbayunittype_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>

                                                                    </span>
                                                                    <div class="clear"></div>

                                                                </div>
                                                                <div class="form-group spicaldivin">
                                                                    <span class="form-group col-md-4">
                                                                        <label class="control-label" style="width: 150px;">Street No</label>
                                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                                            ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <span class="col-md-4">
                                                                        <div class="autocompletedropdown">
                                                                            <label class="control-label" style="width: 150px;">Street Name<span class="symbol required"></span> </label>
                                                                            <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>

                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                            <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                                ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>

                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass="comperror"
                                                                                ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                                            <div id="Divvalidstreetname" style="display: none">
                                                                                <img src="../../../images/check.png" alt="check">Address is valid.
                                                                            </div>
                                                                            <div id="DivInvalidstreetname" style="display: none">
                                                                                <img src="../../../images/x.png" alt="cross">
                                                                                Address is invalid.
                                                                            </div>
                                                                        </div>
                                                                    </span>
                                                                    <span class="col-md-4">
                                                                        <div class="marginbtm15" id="divsttype" runat="server">
                                                                            <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                                            <div id="Div7" class="drpValidate" runat="server">
                                                                                <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" AutoPostBack="true" CssClass="myval" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                                                    ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="editdetail" InitialValue=""> </asp:RequiredFieldValidator>

                                                                            </div>
                                                                        </div>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>

                                                                <%--    </ContentTemplate>
                                                                    </asp:UpdatePanel>--%>

                                                                <div class="form-group spicaldivin">
                                                                    <span class="col-md-4">

                                                                        <label class="control-label" style="width: 150px;">Street City<span class="symbol required"></span> </label>

                                                                        <asp:TextBox ID="ddlStreetCity" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                            OnTextChanged="ddlStreetCity_TextChanged" MaxLength="50"></asp:TextBox>


                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="comperror"
                                                                            ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                    </span>

                                                                    <span class="col-md-4">

                                                                        <label class="control-label" style="width: 150px;">Street State<span class="symbol required"></span> </label>

                                                                        <asp:TextBox ID="txtStreetState" runat="server" MaxLength="10" CssClass="form-control"
                                                                            Enabled="false"></asp:TextBox>

                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                                    </span>

                                                                    <span class="col-md-4">
                                                                        <label class="control-label" style="width: 180px;">Street Post Code<span class="symbol required"></span> </label>
                                                                        <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="10" CssClass="form-control"
                                                                            Enabled="false"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                                            ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>

                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ValidationGroup="editdetail" ControlToValidate="txtStreetPostCode" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                    </span>
                                                                </div>
                                                                <div class="form-group spicaldivin">
                                                                    <span class="col-md-12">
                                                                        <label>
                                                                            Email:
                                                                        </label>
                                                                        <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                                            ValidationGroup="editdetail" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>

                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass="comperror"
                                                                            ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group spical">
                                                                    <span class="col-md-6">
                                                                        <label>
                                                                            Mobile:
                                                                        </label>
                                                                        <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile"
                                                                            ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>



                                                                    </span>
                                                                    <span class="col-md-6">
                                                                        <span>
                                                                            <label>Phone</label>
                                                                        </span>
                                                                        <span>
                                                                            <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                                                ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                                                ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>--%>

                                                                        </span>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>

                                                                <div class="form-group spical" style="margin-top: 10px; width: 100%;">
                                                                    <span class="col-md-6">
                                                                        <label style="width: 100%;">Solar Type</label>
                                                                        <div class="radio radio-info radio-inline">
                                                                            <label for="<%=rblResCom1.ClientID %>">
                                                                                <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                                                <span class="text">Res&nbsp;</span>
                                                                            </label>

                                                                            <label for="<%=rblResCom2.ClientID %>">
                                                                                <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                                                <span class="text">Com&nbsp;</span>
                                                                            </label>

                                                                        </div>
                                                                    </span>
                                                                    <span class="col-md-6">
                                                                        <label style="width: 100%;">Area</label>

                                                                        <div class="radio radio-info radio-inline">
                                                                            <label for="<%=rblArea1.ClientID %>">
                                                                                <asp:RadioButton runat="server" ID="rblArea1" GroupName="ar" />
                                                                                <span class="text">Metro&nbsp;</span>
                                                                            </label>

                                                                            <label for="<%=rblArea2.ClientID %>">
                                                                                <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" />
                                                                                <span class="text">Regional&nbsp;</span>
                                                                            </label>
                                                                        </div>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>

                                                                <div class="form-group marginleft center-text" style="margin-top: 10px;">
                                                                    <asp:Button ID="ibtnEditDetail" runat="server" Text="Update" OnClick="ibtnEditDetail_Click"
                                                                        CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="editdetail" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnNULLEdit" Style="display: none;" runat="server" />
                                        <asp:HiddenField ID="hndEditCustID" runat="server" />

                                        <div class="paddtopbottom20 textcenter" id="divMail" runat="server" visible="false">
                                            <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td>

                                                        <asp:Button ID="btnclear" runat="server" Text="Clearl All" CssClass="btn btn-primary" OnClick="btnclear_Click" Visible="false" />&nbsp;
                                                    </td>
                                                    <td align="left">
                                                        <asp:LinkButton class="btn btn-primary" ID="btnbulkemail" runat="server" CausesValidation="false">Bulk Email</asp:LinkButton>
                                                        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                                                            DropShadow="false" PopupControlID="divBulkemail" TargetControlID="btnbulkemail"
                                                            CancelControlID="btncanelemail">
                                                        </cc1:ModalPopupExtender>
                                                        <asp:LinkButton class="btn btn-primary" ID="btnbulksms" runat="server" CausesValidation="false">Bulk SMS</asp:LinkButton>
                                                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                                                            DropShadow="false" PopupControlID="divbulksms" TargetControlID="btnbulksms" CancelControlID="btncanelsms">
                                                        </cc1:ModalPopupExtender>
                                                        <asp:LinkButton class="btn btn-primary" ID="btnpromotracker" runat="server" CausesValidation="false"
                                                            OnClick="btnpromotracker_OnClick">Promo Tracker</asp:LinkButton>
                                                        <asp:LinkButton class="btn btn-primary" ID="lbshowtagemail" runat="server" CausesValidation="false" Visible="false"
                                                            OnClick="lbshowtagemail_OnClick">Show Tag Email</asp:LinkButton>
                                                        <asp:LinkButton class="btn btn-primary" ID="lblshowtagsms" runat="server" CausesValidation="false" Visible="false"
                                                            OnClick="lblshowtagsms_OnClick">Show Tag SMS</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div runat="server" id="divBulkemail" class="modalpopup" align="center" visible="false" style="display: none;">
                                            <div class="popupspecil">
                                                <div class="closbtn">
                                                    <asp:LinkButton ID="btncanelemail" CausesValidation="false" runat="server"><img src="../../../images/icon_close.png" width="22" height="22" /></asp:LinkButton>
                                                </div>
                                                <div>
                                                    <h1>Bulk Email</h1>
                                                </div>
                                            </div>
                                            <div style="height: 200px;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="formtableab"
                                                    summary="">
                                                    <tbody>
                                                        <tr id="tr1" runat="server">
                                                            <td valign="top">Select News
                                                            </td>
                                                            <td valign="top">
                                                                <asp:DropDownList ID="ddlNews" runat="server" AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="trpromoname" runat="server">
                                                            <td valign="top">Promo Name
                                                            </td>
                                                            <td valign="top">
                                                                <asp:TextBox ID="txtpromoname" MaxLength="100" runat="server" CssClass="form-control"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtpromoname"
                                                                    ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="qualification"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr id="trpromosent" runat="server">
                                                            <td valign="top">Promo Sent
                                                            </td>
                                                            <td valign="top" class="withpopupdate">
                                                                <asp:TextBox ID="txtpromosent" MaxLength="100" runat="server" Style="width: 40%;"
                                                                    CssClass="form-control"></asp:TextBox>
                                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="Image2"
                                                                    TargetControlID="txtpromosent" Format="dd/MM/yyyy">
                                                                </cc1:CalendarExtender>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99/99/9999"
                                                                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                    MaskType="date" TargetControlID="txtpromosent" CultureName="en-GB">
                                                                </cc1:MaskedEditExtender>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtpromosent"
                                                                    ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="qualification"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="btnaddqualificationtitle" runat="server" ImageUrl="~/admin/images/add.gif"
                                                                    OnClick="btnaddqualificationtitle_Onclick" ValidationGroup="qualification" />
                                                                <asp:ImageButton ID="btnqualificationcancel" runat="server" ImageUrl="~/admin/images/cancel.gif"
                                                                    OnClick="btnqualificationcancel_Onclick" CausesValidation="false" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div runat="server" id="divbulksms" class="modalpopup" align="center" visible="false">
                                            <div class="popupspecil">
                                                <div class="closbtn">
                                                    <asp:LinkButton ID="btncanelsms" runat="server"><img src="../../../images/icon_close.png" width="22" height="22" /></asp:LinkButton>
                                                </div>
                                                <div>
                                                    <h1>Bulk SMS</h1>
                                                </div>
                                            </div>
                                            <div style="height: 200px;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="formtableab"
                                                    summary="">
                                                    <tbody>
                                                        <tr>
                                                            <td>Save as Promo
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkpromosms" runat="server" AutoPostBack="true" OnCheckedChanged="chkpromosms_OnCheckedChanged" />
                                                                <label for="<%=chkpromosms.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                                <%-- <asp:TextBox ID="txtqualificationname" MaxLength="100" runat="server" Width="300"></asp:TextBox>
                                <br /><asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtqualificationname"
                                    ErrorMessage="*" ValidationGroup="qualification"></asp:RequiredFieldValidator>--%>
                                                            </td>
                                                        </tr>
                                                        <tr id="trpromonamesms" runat="server" visible="false">
                                                            <td>Promo Name
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtpromonamesms" MaxLength="100" runat="server" CssClass="form-control"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtpromonamesms"
                                                                    ErrorMessage="*" ValidationGroup="qualification"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr id="trpromodatesms" runat="server" visible="false" class="withpopupdate">
                                                            <td>Promo Sent
                                                            </td>
                                                            <td valign="top" class="withpopupdate">
                                                                <asp:TextBox ID="txtpromosentsms" MaxLength="100" runat="server" CssClass="form-control"
                                                                    Style="width: 40%;"></asp:TextBox>
                                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
                                                                    TargetControlID="txtpromosentsms" Format="dd/MM/yyyy">
                                                                </cc1:CalendarExtender>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                                                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                    MaskType="date" TargetControlID="txtpromosentsms" CultureName="en-GB">
                                                                </cc1:MaskedEditExtender>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtpromosentsms"
                                                                    ErrorMessage="*" ValidationGroup="qualification"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="btnbulksmsadd" runat="server" ImageUrl="~/admin/images/add.gif"
                                                                    OnClick="btnbulksmsadd_Onclick" ValidationGroup="qualification" />
                                                                <asp:ImageButton ID="btnbulksmscancel" runat="server" ImageUrl="~/admin/images/cancel.gif"
                                                                    OnClick="btnbulksmscancel_Onclick" CausesValidation="false" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                        <script type="text/javascript">

                                                        $(".dropdown dt a").on('click', function () {
                                                            $(".dropdown dd ul").slideToggle('fast');
                                                        });

                                                        $(".dropdown dd ul li a").on('click', function () {
                                                            $(".dropdown dd ul").hide();
                                                        });


                                                        $(document).bind('click', function (e) {
                                                            var $clicked = $(e.target);
                                                            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                                                        });

                                        </script>
                                        <asp:HiddenField runat="server" ID="hdncountdata" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <div class="finalgrid">
            <div class="padtopzero padrightzero">
                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div id="PanGrid" runat="server">
                        <div class="table-responsive xscroll">
                            <asp:GridView ID="GridView1" DataKeyNames="ContactID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDeleting="GridView1_RowDeleting1"
                                OnDataBound="GridView1_DataBound" OnRowDataBound="GridView1_RowDataBound1" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                <Columns>
                                    <%--<asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <center>
                                            <span class="euroradio">                                      
                                                <asp:CheckBox ID="chksmsheadertag" runat="server" AutoPostBack="true" OnCheckedChanged="chksmsheadertag_CheckedChanged" />
                                                <label for='<%# ((GridViewRow)Container).FindControl("chksmsheadertag").ClientID %>' runat="server" id="lblchk">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Width="75px">
                                                        <asp:CheckBox ID="chktagsms" runat="server" Visible='<%#Eval("SendSMS").ToString()=="True"?true:false %>' />
                                                        <label for='<%# ((GridViewRow)Container).FindControl("chktagsms").ClientID %>' runat="server" id="lblchk">
                                                            <span></span>
                                                        </label>
                                                        <asp:HiddenField ID="hndid" runat="server" Value='<%#Eval("ContactID") %>' />
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="fullname">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Width="150px">
                                                <asp:HiddenField ID="hndContactID" runat="server" Value='<%#Eval("ContactID") %>' />
                                                <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                <asp:Label ID="lblFullname" runat="server" Visible="false" style="color:#337ab7;"><%#Eval("fullname")%></asp:Label>
                                                <asp:HyperLink ID="lnkFullname" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=c&compid="+Eval("CustomerID")+"&contid="+ Eval("ContactID") %>'
                                                     ><%#Eval("fullname")%></asp:HyperLink>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Company" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="tdspecialclass"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="Customer" HeaderStyle-Width="80px" ItemStyle-Width="80px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomer" runat="server" Width="200px" CssClass="gridmainspan">
                                                  <asp:Label ID="lblCust" runat="server" Visible="false" style="color:#337ab7;"><%#Eval("Customer")%></asp:Label>
                                                <asp:HyperLink ID="lnkCustomer" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/CustomerNew.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%></asp:HyperLink>
                                                <div class="contacticonedit" style="display:none!important">
                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="EditDetail" CommandArgument='<%#Eval("CustomerID") %>' CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                    </asp:LinkButton>
                                                    <%--<asp:ImageButton ID="gvbtnUpdate" runat="server" CommandName="EditDetail" CommandArgument='<%#Eval("CustomerID") %>' ImageUrl="../../../images/icon_edit.png"
                                                                                    CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />--%>
                                                </div>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="salesrep">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsalesrep" runat="server" Width="100px"><%#Eval("salesrep")%></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Suburb" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="Suburb">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Width="100px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Suburb")%>'><%#Eval("Suburb")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="P/Cd" ItemStyle-HorizontalAlign="Center" SortExpression="PCd">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Width="35px"><%#Eval("PCd")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lead Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="leadtype">
                                        <ItemTemplate>
                                            <asp:Label ID="Label9" runat="server" Width="80px"><%#Eval("leadtype")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="projectstatus">
                                        <ItemTemplate>
                                            <asp:Label ID="Label16" runat="server" Width="100px" Text='<%#Eval("projectstatus")%>' data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("projectstatus")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Quote Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" SortExpression="QuoteDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuoteDate" runat="server" Width="90px"><%#Eval("QuoteDate", "{0:dd MMM yyyy}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NextFollowUp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" SortExpression="NextFollowUpDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNextFollowUpDate" runat="server" Width="90px"><%#Eval("NextFollowUpDate", "{0:dd MMM yyyy}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" SortExpression="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Width="90px" ToolTip='<%#Eval("Description")%>'><%#Eval("Description")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle />
                                <%--<PagerTemplate>
                                             <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                             <div class="pagination">
                                                 <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                 <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                 <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                 <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                 <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                 <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                 <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                 <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                 <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                 <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                 <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                             </div>
                                         </PagerTemplate>
                                         <PagerStyle CssClass="paginationGrid" />
                                         <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />--%>
                            </asp:GridView>

                        </div>
                        <div class="paginationnew1" runat="server" id="divnopage">
                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>


                                        <div class="pagination paginationGrid">
                                            <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                            <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                            <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                    <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                            <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnExport" />
                <asp:PostBackTrigger ControlID="btnSearch" />

            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <%-- <script type="text/javascript">
        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');
        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


    </script>--%>
</asp:Content>

