<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="streetname.aspx.cs" Inherits="admin_adminfiles_master_streetname" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                     $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $("[data-toggle=tooltip]").tooltip();
                    $(".myvalstreetname").select2({
                        //placeholder: "select",
                        allowclear: true,
						 minimumResultsForSearch: -1
                    });
                    
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
						 minimumResultsForSearch: -1
                    });

                    $('.redreq').click(function () {
                        formValidate();
                    });
                }
            </script>

            <script>



                function ComfirmDelete(event, ctl) {
                    event.preventDefault();
                    var defaultAction = $(ctl).prop("href");

                    swal({
                        title: "Are you sure you want to delete this Record?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },

                      function (isConfirm) {
                          if (isConfirm) {
                              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              eval(defaultAction);

                              //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              return true;
                          } else {
                              // swal("Cancelled", "Your imaginary file is safe :)", "error");
                              return false;
                          }
                      });
                }


            </script>
            
             <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Street Name</h5>
              <div id="hbreadcrumb" class="pull-right">
                                <ol class="hbreadcrumb breadcrumb fontsize16">
                                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                                </ol>
                            </div>
               </div>
               
               
               
             <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server" visible="false">
                 <div class="panel-body animate-panel padtopzero"> 
             	   <div class="well with-header with-footer addform">
                  	 <div class="header bordered-blue"><asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                Street Name
                      </div>
                      <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label">
                                                Street Name</asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtStreetName" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStreetName" ErrorMessage=""></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <asp:Button CssClass="btn btn-primary redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                    Text="Add" />
                                                <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                    Text="Save" Visible="false" />
                                                <asp:Button CssClass="btn btn-default btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                    CausesValidation="false" Text="Reset" />
                                                <asp:Button CssClass="btn btn-default btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />
                                            </div>
                                        </div>
                                                </div>
                      
                     </div>
                   </div>
                  </div>
                  </div>
                  

         <div class="page-body padtopzero">    
            <asp:Panel runat="server" ID="PanGridSearch">
                <div class="">
                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-success" id="PanSuccess" runat="server">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                   <div class="searchfinal">
                     <div class="widget-body shadownone brdrgray"> 
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                        <table border="0" cellspacing="0" width="100%" style="text-align: right;" cellpadding="0">
                                                        <tr>
                                                            <td class="left-text dataTables_length showdata padtopzero"><asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Street Name" CssClass="form-control m-b"></asp:TextBox>
                                                                    <%-- <span class="input-group-btn">
                                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />
                                                                </span>--%>
                                                                </div>

                                                                <div class="input-group">
                                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                        </div>
                      </div>
                     </div>
                     </div>
                   
                   
                    <div class="finalgrid">
                       <div class="table-responsive printArea">
                            <div id="PanGrid" runat="server">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView1" DataKeyNames="Id" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                    OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowDeleting="GridView1_RowDeleting" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Street Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("StreetName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="35px" HeaderStyle-Width="35px" HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <AlternatingRowStyle />

                                                    <PagerTemplate>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        <div class="pagination">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                        </div>
                                                    </PagerTemplate>
                                                    <PagerStyle CssClass="paginationGrid" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>
                                            </div>
                                            <div class="paginationnew1" runat="server" id="divnopage">
                                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
