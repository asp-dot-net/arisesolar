<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" 
    CodeFile="contactnote.aspx.cs" Inherits="admin_adminfiles_company_contactnote" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/contactmenu.ascx" TagName="contactmenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    
    <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Promo</h5>
              <div class="pull-right"> <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton></div>
     </div>
           
           
      <div class="page-body padtopzero">   
     <div class="searchfinal">  
                    <div class="widget-body shadownone brdrgray">                      
                         <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">                                           
                         	<div class="dataTables_filter">
                                <table border="0" cellspacing="0" width="100%" style="text-align: right; margin-bottom: 0px;" cellpadding="0">
                                                        
                                                            <tr>
                                                                <td class="left-text dataTables_length showdata" style="padding-top:0px!important;">
                                                                           
                                                                 <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                                
                                                                </td>
                                                                </tr>
                                                                </table>
                           </div>
                         </div>
                    </div>
            </div>
            
            <div class="contactsarea">
           
                <uc1:contactmenu ID="contactmenu1" runat="server" />
                <div class="messesgarea">
                    <asp:Panel ID="PanAlreadExists" runat="server" CssClass="information">
                        <i class="icon-info-sign"></i>
                        <asp:Label ID="lblalredyRecord" runat="server" Text="Company Location with this title already exists."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanSuccess" runat="server" CssClass="pansucess">
                        <i class="icon-ok-sign"></i>
                        <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanError" runat="server" CssClass="failure">
                        <i class="icon-remove-sign"></i>
                        <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanNoRecord" runat="server" CssClass="information">
                        <i class="icon-remove-sign"></i>
                        <div class="alert alert-info">
                        <asp:Label ID="lblNoRecord" runat="server" Text="There are no items to show in this view."></asp:Label>
                        </div>
                        <asp:Panel ID="PanSearch" runat="server">
                        </asp:Panel>
                    </asp:Panel>
                </div>
                
                <div class="contactbottomarea">
                    <div class="tableblack">
                        <div class="table-responsive noPagination" id="PanGrid" runat="server">
                            <asp:GridView ID="GridView1" DataKeyNames="ContNoteID" runat="server" AllowPaging="true"
                                PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" PageSize="10"
                                OnPageIndexChanging="GridView1_PageIndexChanging" PagerStyle-CssClass="gridpagination"
                                AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" CssClass="table table-bordered table-hover">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px" SortExpression="NoteSetname">
                                        <ItemTemplate>
                                            <%#Eval("ContNoteDate", "{0:dd MMM, yyyy}")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="300px" SortExpression="NoteSetname">
                                        <ItemTemplate>
                                            <%#Eval("NoteSetname")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="For" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="300px" SortExpression="employeename">
                                        <ItemTemplate>
                                            <%#Eval("employeename")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Note" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%#Eval("ContNote")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="40px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvbtnDelete" runat="server" CommandName="Delete" ImageUrl="~/admin/images/icons/icon_delet.png" CommandArgument='<%#Eval("ContNoteID")%>'
                                                CausesValidation="false" Visible='<%# Eval("usercheck").ToString() == "1" || Eval("ContNoteDone").ToString() == "True" ? false : true %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                    DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                    CancelControlID="Button1">
                </cc1:ModalPopupExtender>
                <div id="myModal" runat="server" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                 <div style="float: right">
                                <button id="Button1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                                     </div>
                                <h4 class="modal-title" id="myModalLabel">
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Add Contact Note</h4>
                            </div>
                            <asp:Panel ID="PanAddUpdate" runat="server">
                                <div class="modal-body paddnone">
                                    <div class="panel-body">
                                        <div class="formainline">
                                            <div class="form-group">
                                                <label>Note</label>
                                                <asp:TextBox ID="txtnote" runat="server" TextMode="MultiLine" Height="10%" CssClass="form-control"></asp:TextBox>
                                                <br /><asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtnote" Display="None"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="form-group  marginleft">
                                                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-danger" Text="Add" OnClick="btnAdd_Click" CausesValidation="true" />
                                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-dark" Text="Reset" OnClick="btnReset_Click" CausesValidation="false" />
                                                <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                                    CssClass="btn" Text=" OK " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
            </div>
            
        </div>
               
      <!--Danger Modal Templates-->
                    <asp:Button ID="btndelete" Style="display:none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog " style="margin-top:-300px">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>
                          
                                  
                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server" ></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align:center">
                    <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel"  runat="server"  class="btn"  data-dismiss="modal"  >Cancel</asp:LinkButton>
                </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />  
          
  
<!--End Danger Modal Templates-->
</asp:Content>
