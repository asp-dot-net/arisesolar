<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="emptitle.aspx.cs"  Inherits="admin_adminfiles_master_emptitle" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Emp Title</h5>
                            <div class="pull-right"> 
                            
                             <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" class="btn btn-default purple redreq btnaddicon"
                        OnClick="lnkAdd_Click">Add</asp:LinkButton>                        
               </div>
           </div>
         
      <div class="page-body padtopzero">
      <div class="messesgarea">
                    <div class="alert alert-success" id="PanSuccess" runat="server">
                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                    </div>
                    <div class="alert alert-danger" id="PanError" runat="server">
                        <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                            Text="Transaction Failed."></asp:Label></strong>
                    </div>
                    <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                    </div>
                    <div class="alert alert-info" id="PanNoRecord" runat="server">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>
                
      	 <div class="searchfinal">  
                    <div class="widget-body shadownone brdrgray">                      
                         <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">                                           
                         	<div class="dataTables_filter row">
                            <div class="col-md-6 col-sm-6">
                             <table border="0" cellspacing="0" width="100%" style="text-align: right; margin-bottom: 0px;" cellpadding="0">
                                                        
                                                            <tr>
                                                                <td class="left-text dataTables_length showdata" style="padding-top:0px!important;">
                                                                           
                                                                 <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                                
                                                                </td>
                                                               </tr>
                                                               </table>
                                                            </div>
                                             <div class="col-sm-6">
                                             <div class="pull-right">
                                               <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                        WatermarkText="Emp Title" />
                                    <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" class="btn btn-danger"
                                        CausesValidation="false" OnClick="btnSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                             	</div>
                                                </div>
                            </div>
                         </div>
                      </div>
                  </div>
                  
                  <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <h3 class="m-b-xs text-black"></h3>
            <div class="contactsarea">                
                <div class="contactbottomarea">
                    <div class="tableblack">
                        <div class="table-responsive noPagination" id="PanGrid" runat="server">
                            <asp:GridView ID="GridView1" DataKeyNames="EmpTitleID" runat="server" AllowPaging="true"
                                PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" PageSize="10" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                OnPageIndexChanging="GridView1_PageIndexChanging" PagerStyle-CssClass="gridpagination"
                                AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" CssClass="table table-bordered table-hover">
                                <Columns>
                                    <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server" Checked='<%# Eval("Active")%>' Enabled="false" />
                                        </ItemTemplate>
                                        <ItemStyle Width="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp Title" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="600px">
                                        <ItemTemplate>
                                            <%#Eval("EmpTitle")%>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="brdnoneleft" />
                                        <HeaderStyle CssClass="brdnoneleft" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandName="Select" ImageUrl="../../../images/icon_edit.png"
                                                CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="40px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                        <ItemTemplate>
                                            <!--DELETE Modal Templates-->

                                                <asp:LinkButton ID="gvbtnDelete" runat="server"  CssClass="btn btn-danger btn-xs" CausesValidation="false" 
                                                CommandName="Delete" CommandArgument='<%#Eval("EmpTitleID")%>' >
                                                <i class="fa fa-trash"></i> Delete
                                                </asp:LinkButton>

                                               
                                            <!--END DELETE Modal Templates-->
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
            CancelControlID="Button1">
        </cc1:ModalPopupExtender>
        <div id="myModal" runat="server">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                         <div style="float: right">
                        <button id="Button1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                             </div>
                        <h4 class="modal-title" id="myModalLabel">
                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                            Emp Title</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div class="formainline">
                                <div class="form-group">
                                    <label>Emp Title</label>
                                    <asp:TextBox ID="txtEmpTitle" runat="server" MaxLength="100" Width="200px" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="This value is required." CssClass="reqerror" ControlToValidate="txtEmpTitle"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group ">        
                                <div class="row">                          
                             <div class="col-sm-2 rightalign right-text">
                                                            <asp:Label ID="Label2" runat="server" class="control-label">
                                               Is Active?</asp:Label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div>
                                                            <label for="<%=chkActive.ClientID %>">
                                <asp:CheckBox ID="chkActive" runat="server" />
                                <span class="text">&nbsp;</span>
                            </label>
                                                               
                                                            </div>
                                    <div class="clear"></div>
                                </div>
                                </div>
                                <div class="form-group  marginleft" style="margin-top:15px;">
                                   <asp:Button CssClass="btn btn-default purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add" />
                                     <asp:Button CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click" CausesValidation="false" Text="Reset" />
                                    <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary savewhiteicon btnsaveicon" Text="Save" Visible="false" OnClick="btnUpdate_Click" CausesValidation="true" />
                                    <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                                CausesValidation="false" Text="Cancel" />
                                    <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                        CssClass="btn" Text=" OK " />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
    </section>
               </div>
      
   
           <asp:Button ID="btndelete" Style="display:none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog " style="margin-top:-300px">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>
                          
                                  
                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server" ></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align:center">
                    <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel"  runat="server"  class="btn"  data-dismiss="modal"  >Cancel</asp:LinkButton>
                </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />      
               
    
</asp:Content>


