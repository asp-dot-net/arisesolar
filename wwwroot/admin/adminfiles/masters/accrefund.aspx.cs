using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_master_accrefund : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropDown();
            BindGrid(0);
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Verification")))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}

            if ((Roles.IsUserInRole("Administrator")))
            {
                tdTeam.Visible = true;
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                tdsalerep.Visible = false;
            }
            BindScript();
        }
    }

    public void BindDropDown()
    {
        ddlOptionID.DataSource = ClstblRefundOptions.tblRefundOptions_SelectActice();
        ddlOptionID.DataMember = "OptionName";
        ddlOptionID.DataTextField = "OptionName";
        ddlOptionID.DataValueField = "OptionID";
        ddlOptionID.DataBind();

        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
        }

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            if (SalesTeam != string.Empty)
            {
                ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        ddlSalesRepSearch.DataMember = "fullname";
        ddlSalesRepSearch.DataTextField = "fullname";
        ddlSalesRepSearch.DataValueField = "EmployeeID";
        ddlSalesRepSearch.DataBind();
    }

    protected DataTable GetGridData()
    {
        string historic = "0";
        if (chkhist.Checked.ToString() == "True")
        {
            historic = "";
        }
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        DataTable dt = new DataTable();
        //Response.Write("Status=>" + historic + "<br/>project=>" + txtproject.Text + "<br/>optionid=>" + ddlOptionID.SelectedValue + "<br/>startdate=>" + txtStartDate.Text.Trim() + "<br/>enddate=>" + txtEndDate.Text.Trim() + "<br/>team=>" + ddlTeam.SelectedValue + "<br/>sales=>" + ddlSalesRepSearch.SelectedValue + "<br/>userid=>" + userid + "<br/>item=>" + selectedItem);
        //Response.End();
        dt = ClstblProjectRefund.tblProjectRefund_Search(historic, txtproject.Text, ddlOptionID.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlTeam.SelectedValue, ddlSalesRepSearch.SelectedValue, userid, selectedItem);
        //Response.Write(dt.Rows.Count);
        //Response.End();
        //Response.Write("Status=>" + historic + "<br/>project=>" + txtproject.Text + "<br/>optionid=>" + ddlOptionID.SelectedValue + "<br/>startdate=>" + txtStartDate.Text.Trim() + "<br/>enddate=>" + txtEndDate.Text.Trim() + "<br/>team=>" + ddlTeam.SelectedValue + "<br/>sales=>" + ddlSalesRepSearch.SelectedValue + "<br/>userid=>" + userid + "<br/>item=>" + selectedItem);
        //Response.End();
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            //Response.Write(dt.Rows.Count);
            //Response.End();
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "dodropdown();", true);
        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "refund")
        {
            string RefundID = e.CommandArgument.ToString();
            ModalPopupExtenderEdit.Show();



            ListItem item6 = new ListItem();
            item6.Text = "Select";
            item6.Value = "";
            ddlPayMethod.Items.Clear();
            ddlPayMethod.Items.Add(item6);

            ddlPayMethod.DataSource = ClsProjectSale.tblInvoicePayMethod_Select();
            ddlPayMethod.DataValueField = "FPTansID";
            ddlPayMethod.DataTextField = "InvoicePayMethodABB";
            ddlPayMethod.DataMember = "InvoicePayMethodABB";
            ddlPayMethod.DataBind();

            hndRefundID.Value = RefundID;
            SttblProjectRefund stref = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
            hdnProID.Value = stref.ProjectID;
        }
        if (e.CommandName.ToLower() == "revertrefund")
        {
            string RefundID = e.CommandArgument.ToString();
           
            hndRefundID.Value = RefundID;
            ModalPopupExtenderDelete.Show();

        }
        BindGrid(0);
    }

    protected void ibtnAddComment_Onclick(object sender, EventArgs e)
    {
        BindScript();
        string RefundID = hndRefundID.Value;
        //Response.Write(RefundID);
        //Response.End();
        string AccUserID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string PaymentMode = ddlPayMethod.SelectedValue;
        string PaidDate = txtPaidDate.Text;
        string Remarks = txtRemarks.Text;
        SttblProjectRefund stref = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
        decimal amount = Convert.ToDecimal(stref.Amount);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;
        string InvoicePayTotal = Convert.ToDecimal(amount * -1).ToString();
        string Refund = "False";
        string RefundType = "0";
        string ReceiptNumber = "";

        bool suc = ClstblProjectRefund.tblProjectRefund_Update(RefundID, PaymentMode, PaidDate, AccUserID, Remarks);

        decimal invoicepayGST = Convert.ToDecimal(amount) / 11;
        decimal invoicepayExGST = (Convert.ToDecimal(amount) / 11) * 10;
        string InvoicePayExGST = Convert.ToString(invoicepayExGST);
        string InvoicePayGST = Convert.ToString(invoicepayGST);

        //Response.Write(stref.OptionID);
        //Response.End();
        if (stref.OptionID == "4")
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(hdnProID.Value, "6");
            int success = ClstblInvoicePayments.tblInvoicePayments_Insert(hdnProID.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, PaidDate, PaymentMode, "", EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);
        }
        else //if(stref.OptionID == "3")
        {
            int success = ClstblInvoicePayments.tblInvoicePayments_Insert(hdnProID.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, PaidDate, PaymentMode, "", EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);
        }
       
        if (suc)
        {
            ModalPopupExtenderEdit.Hide();
            ddlPayMethod.SelectedValue = "";
            txtPaidDate.Text = string.Empty;
            txtRemarks.Text = string.Empty;
        }

        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtproject.Text = string.Empty;
        ddlOptionID.SelectedValue = "";
        txtEndDate.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        BindGrid(0);
    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        DataTable dt = ClstblProjectRefund.tblProjectRefund_Search1("False", txtproject.Text, ddlOptionID.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlTeam.SelectedValue, ddlSalesRepSearch.SelectedValue, userid, selectedItem);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Refund" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 1, 2, 3, 4, 5, 6, 9, 7 };
            string[] arrHeader = { "Project", "Date", "Amount", "Acc Name", "BSB No", "Acc No", "By", "Option" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal ltProjectStatus = (Literal)e.Row.FindControl("ltProjectStatus");
            HiddenField hdnProjectID = (HiddenField)e.Row.FindControl("hdnProjectID");
            //Response.Write(hdnProjectID.Value);
            //Response.End();
            try
            {
                SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);//783297
                if (st.ProjectStatusID != string.Empty)
                {
                    SttblProjectStatus ststatus = ClstblProjectStatus.tblProjectStatus_SelectByProjectStatusID(st.ProjectStatusID);
                    ltProjectStatus.Text = ststatus.ProjectStatus;

                }
            }
            catch
            {
            }
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        if (hdnID.Value == "2")
        {
            e.Item.Visible = false;
        }
    }


    protected void btnCancelEdit_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Hide();
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {

        if (hndRefundID.Value.ToString() != string.Empty)
        {
            string AccUserID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;
            string Refund = "False";
            string RefundType = "0";
            string ReceiptNumber = "";
            string RefundID = hndRefundID.Value;

            SttblProjectRefund stref = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
            hdnProID.Value = stref.ProjectID;

            ClstblProjectRefund.tblProjectRefund_Update(RefundID, "", "", AccUserID, "");
            bool suc = ClstblProjectRefund.tblProjectRefund_UpdateStatus(RefundID, false.ToString());

            decimal amount = Convert.ToDecimal(stref.Amount);
            decimal invoicepayGST = Convert.ToDecimal(amount) / 11;
            decimal invoicepayExGST = (Convert.ToDecimal(amount) / 11) * 10;
            string InvoicePayExGST = Convert.ToString(invoicepayExGST);
            string InvoicePayGST = Convert.ToString(invoicepayGST);
            string PaymentMode = ddlPayMethod.SelectedValue;
            string InvoicePayTotal = Convert.ToDecimal(amount * 1).ToString();
            
            //ClstblInvoicePayments
             int success = ClstblInvoicePayments.tblInvoicePayments_Insert(hdnProID.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, DateTime.Now.AddHours(14).ToString(), PaymentMode, "", EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);
            //int success = ClstblInvoicePayments.tblInvoicePayments_Insert(hdnProID.Value, "", "", "", DateTime.Now.AddHours(14).ToString(), "", "", EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);

            bool success_revert = ClsProjectSale.tblProjects_UpdateProjectStatusID(stref.ProjectID, stref.ProjectStatusID);
            if (success_revert)
            {
                SetAdd1();
                ModalPopupExtenderDelete.Hide();
            }
            else
            {
                SetError1();
            }
        }

        BindGrid(0);
    }
}