﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_master_empdetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           

            string EmployeeID = Request.QueryString["id"];
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByEmployeeID(EmployeeID);

            lblrole.Text = st.RoleName;
            //lblsalesteam.Text = st.TeamName;
            lblfirst.Text = st.EmpFirst;
            lbllast.Text = st.EmpLast;
            lbltitile.Text = st.EmpTitle;
            lblinitials.Text = st.EmpInitials;
            lblEmpEmail.Text = st.EmpEmail;
            lblmobilenum.Text = st.EmpMobile;
            lblphonenum.Text = st.EmpPhone;
            lblphoneextno.Text = st.EmpExtNo;
            lblnicname.Text = st.EmpNicName;

            if (st.LTeamOutDoor == "True")
            {
            
                lblteamoutdoor.Text = "Yes";
            }
            else
            {
                lblteamoutdoor.Text = "No";
            }
            //Response.Write(st.LTeamOutDoor);
            //Response.End();
            if (st.LTeamCloser == "True")
            {
                lblclosers.Text = "Yes";
            }
            else
            {
                lblclosers.Text = "No";
            }
            if (st.Include == "True")
            {
                lblincludeinlist.Text = "Yes";
            }
            else
            {
                lblincludeinlist.Text = "No";
            }
            if (st.ActiveEmp == "True")
            {
                lblactiveemployee.Text = "Yes";
            }
            else
            {
                lblactiveemployee.Text = "No";
            }
            if (st.showexcel == "True")
            {
                lblshowexcel.Text = "Yes";
            }
            else
            {
                lblshowexcel.Text = "No";
            }
           
           // lblrole.Text = st.
            if (st.EmpType == "1")
            {
                lblemptype.Text = "Arise Solar";
            }
            else
            {
                lblemptype.Text = "Door to Door";
            }
           
          //  lblstate.Text = st.StatusName;
            DataTable dtcat = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(EmployeeID);
            if (dtcat.Rows.Count > 0)
            {
                //Response.Write(rptTeam);
                //Response.End();
                rptTeam.DataSource = dtcat;
                rptTeam.DataBind();
            }
            lbllocation.Text = st.LocationName;
            lblusername.Text = st.username;
            lblstarttime.Text = string.Format("{0:HH:mm}", st.StartTime);
            lblendtime.Text = string.Format("{0:HH:mm}", st.EndTime);
            lblbreaktime.Text = string.Format("{0:HH:mm}", st.BreakTime);
             try
            {
                lblhireddate.Text = Convert.ToDateTime(st.HireDate).ToShortDateString();
            }
            catch { }

             lblinfo.Text = st.EmpInfo;
          

        }
    }
   
   
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/masters/employee.aspx");
    }
}