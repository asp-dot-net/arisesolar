<%@ Page Title="Arisesolar | Active Report" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Culture="en-GB" UICulture="en-GB" CodeFile="clickcustomer.aspx.cs" Inherits="admin_adminfiles_masters_clickcustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Click Customer </h5>
    </div>
    <%--    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                }
                function endrequesthandler(sender, args) {
                }
                function pageLoaded() {

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.redreq').click(function () {
                        formValidate();
                    });
                }
            </script>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }

            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="content animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>
                    <div class=" animate-panel padbtmzero padtopzero">
                        <div>
                            <div class="hpanel marbtmzero">
                                <div class="searchfinal">
                                    <div class="widget-body shadownone brdrgray">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="row">
                                                <div>
                                                    <div class="dataTables_filter col-sm-12 Responsive-search printorder">
                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                <tr>
                                                                    <td>

                                                                        <div class="form-group martop5">
                                                                            <asp:TextBox ID="txtproject" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtproject"
                                                                                WatermarkText="Project No" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtproject" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        </div>

                                                                        <div class="form-group martop5">
                                                                            <asp:TextBox ID="txtcustname" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtcustname"
                                                                                WatermarkText="Cust Name" />
                                                                        </div>
                                                                        <div class="form-group martop5">
                                                                            <asp:TextBox ID="txtemail" palceholder="Email" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtemail"
                                                                                WatermarkText="Email" />
                                                                        </div>
                                                                        <div class="form-group martop5">
                                                                            <asp:TextBox ID="txtphone" palceholder="Phone" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtphone"
                                                                                WatermarkText="Phone" />
                                                                        </div>
                                                                        <div class="form-group martop5">
                                                                            <asp:TextBox ID="txtmobile" palceholder="Mobile" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtmobile"
                                                                                WatermarkText="Mobile" />
                                                                        </div>
                                                                        <div class="form-group martop5">
                                                                            <asp:TextBox ID="txtaddress" palceholder="Address" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtaddress"
                                                                                WatermarkText="Address" />
                                                                        </div>
                                                                        <div class="form-group martop5" id="tdTeam" runat="server" style="width: 156px">
                                                                            <asp:DropDownList ID="ddlclickcustomer" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="">Is Click Customer</asp:ListItem>
                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class=" input-group col-sm-2" id="div2" runat="server">
                                                                            <asp:DropDownList ID="ddlSalesRep" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                                            </asp:DropDownList>                                                                            
                                                                        </div>
                                                                        <div class="input-group martop5">
                                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info redreq btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                        </div>
                                                                        <div class="form-group martop5">
                                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <%-- <div class="datashowbox">--%>
                                            <div class="row ">
                                                <div>
                                                    <div class="dataTables_length showdata col-sm-3 printorder">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                            <%--</div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panel1" runat="server" CssClass=" padleftzero padrightzero finalgrid">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div id="PanGrid" runat="server">
                            <div class="table-responsive  printArea">

                                <asp:GridView ID="GridView1" DataKeyNames="customerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Eval("CustomerName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Eval("CustomerAddress")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Eval("ContEmail")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Eval("ContMobile")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Eval("CustPhone")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <AlternatingRowStyle />

                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination printorder">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid printorder" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

