using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.Serialization.Json;
using System.Net;
using System.Dynamic;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using System.Web.Services;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Threading;
using System.Security.Principal;
using System.IO;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
//using Microsoft.AspNet.SignalR;
//using Microsoft.AspNet.SignalR.Infrastructure;

public partial class admin_adminfiles_upload_lead : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    //Google Sheet 
    //For Local
    //static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    //static readonly string ApplicationName = "Sync tblWebDownload";
    //static readonly string sheet = "Sheet1";
    //static readonly string SpreadsheetId = "1tNnJ0g2KWSIFoOx8BDmDnbLrnTZMODrlop-ab-RRN-I";
    //static SheetsService service;

    //For Live
    static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    static readonly string ApplicationName = "AriseSolar WebDownloadData";
    static readonly string sheet = "Sheet1";
    static readonly string SpreadsheetId = "19NxOhA3Z1srODefkdzVbb0NJr2bHwJ8roS7rrmk3bMM";
    static SheetsService service;
    //Gogle Sheet

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            HidePanel();

            custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
            ddlSearchState.DataMember = "State";
            ddlSearchState.DataTextField = "State";
            ddlSearchState.DataValueField = "State";
            ddlSearchState.DataBind();

            ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
            ddlTeam.DataMember = "SalesTeam";
            ddlTeam.DataTextField = "SalesTeam";
            ddlTeam.DataValueField = "SalesTeamID";
            ddlTeam.DataBind();

            ddlSource.DataSource = ClstblCustSourceSub.tblCustSourceSub_SelectActive();
            ddlSource.DataMember = "CustSourceSub";
            ddlSource.DataTextField = "CustSourceSub";
            ddlSource.DataValueField = "CustSourceSub";
            ddlSource.DataBind();

            if(!Roles.IsUserInRole("Administrator"))
            {
                ListItem removeItem = action.Items.FindByValue("3");
                action.Items.Remove(removeItem);
            }

            BindGrid(0);
        }
    }
    public void BindDataCount()
    {
        string data = "select count(WebDownloadID) as data from tblWebDownload s  left outer join (select min(webdownloadid) as contmwebdownloadid,ContMobile from tblWebDownload where ContMobile!='' group by ContMobile    having count(*) > 1) t on s.ContMobile = t.ContMobile left outer join (    select min(webdownloadid) as contewebdownloadid,ContEmail    from tblWebDownload where ContEmail!=''    group by ContEmail    having count(*) > 1) t2 on s.ContEmail = t2.ContEmail left outer join (    select min(webdownloadid) as contpwebdownloadid,CustPhone    from tblWebDownload where CustPhone!=''    group by CustPhone    having count(*) > 1) t3 on s.CustPhone = t3.CustPhone left outer join (    select min(ContactID) as MContactID,ContMobile    from tblContacts where ContMobile!=''    group by ContMobile    having count(*) > 1) c on s.ContMobile = c.ContMobile left outer join (    select min(ContactID) as EContactID,ContEmail    from tblContacts where ContEmail!=''    group by ContEmail    having count(*) > 1) c2 on s.ContEmail = c2.ContEmail left outer join (    select min(customerid) as Pcustomerid,CustPhone    from tblCustomers where CustPhone!=''    group by CustPhone    having count(*) > 1) c3 on s.CustPhone = c3.CustPhone left outer join tblEmployees E on E.EmployeeID=s.EmployeeID where Uploaded = 0 and Duplicate=0 and (((case when NotDuplicate=0 then (case when c.MContactID is not null then 1 else case when c2.EContactID is not null then 1 else case when c3.Pcustomerid is not null then 1 else 2 end end end) else 2 end)= '" + ddlDup.SelectedValue + "') or (isnull('" + ddlDup.SelectedValue + "',0) =0))and (((case when NotDuplicate=0 then (case when t.contmwebdownloadid is not null then 1 else case when t2.contewebdownloadid is not null then 1 else case when t3.contpwebdownloadid is not null then 1 else 2 end end end) else 2 end ) = '" + ddlwebdup.SelectedValue + "') or (isnull('" + ddlwebdup.SelectedValue + "',0) =0)) and   (upper(Customer) like '%'+'" + txtSearch.Text.Trim() + "'+'%'  or  '" + txtSearch.Text.Trim() + "'='') and ( E.SalesTeamID like '%'+'" + ddlTeam.SelectedValue + "'+'%'  or  '" + ddlTeam.SelectedValue + "'='') and  ( upper([State]) like '%'+'" + ddlSearchState.SelectedValue + "'+'%'  or  '" + ddlSearchState.SelectedValue + "'='') and ( [subsource] like '%'+ LTRIM(RTRIM('" + ddlSource.SelectedValue + "'))+'%'  or  '" + ddlSource.SelectedValue + "'='') and  ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= s.EntryDate or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=s.EntryDate  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='')";
        DataTable dt1 = ClstblCustomers.query_execute(data);
        if (dt1.Rows.Count > 0)
        {
            countdata = Convert.ToInt32(dt1.Rows[0]["data"]);

            hdncountdata.Value = countdata.ToString();
        }

        //rce.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtSearchPostCode.Text, "", txtSearchCompanyNo.Text, txtSerachCity.Text, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue);
        //Response.Write(data);
        //Response.End();

         #region MyRegionForPagination

        if (custompagesize != 0)
        {

            //Response.End();
            lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;

            if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
            {
                lastpageindex = lastpageindex + 1;
            }

            int pageindexcustom1 = Convert.ToInt32(hdncountdata.Value) / custompagesize;

            if (pageindexcustom1 > 0)
            {
                if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
                {
                    pageindexcustom1 = pageindexcustom1 + 1;
                }

                if (pageindexcustom1 >= custompageIndex)
                {

                }
                else
                {
                    custompageIndex = pageindexcustom1;
                }

                int PageIndex = 3;
                int StartPageIndex = custompageIndex - PageIndex;//10-3=7
                int EndPageIndex = custompageIndex + PageIndex;//10+3=13

                if (StartPageIndex < 1)
                {
                    EndPageIndex = EndPageIndex - StartPageIndex;
                }
                if (EndPageIndex > lastpageindex)
                {
                    StartPageIndex = (StartPageIndex - (EndPageIndex - lastpageindex)) + 1;
                }
                DataTable dt_page = new DataTable();
                dt_page.Columns.Add("ID");

                for (int i = 1; i <= lastpageindex; i++)
                {
                    if ((StartPageIndex < i && EndPageIndex > i))
                    {
                        DataRow dr = dt_page.NewRow();
                        dr["ID"] = i;
                        dt_page.Rows.Add(dr);

                    }
                }

                rptpage.DataSource = dt_page;
                rptpage.DataBind();

                if (custompageIndex == 1)
                {
                    lnkfirst.Visible = false;
                    lnkprevious.Visible = false;
                    lnknext.Visible = true;
                    lnklast.Visible = true;
                }
                if (custompageIndex > 1)
                {
                    lnkfirst.Visible = true;
                    lnkprevious.Visible = true;
                }
                if (custompageIndex == EndPageIndex)
                {
                    lnknext.Visible = false;
                    lnklast.Visible = false;
                }
                if (lastpageindex == custompageIndex)
                {
                    lnknext.Visible = false;
                    lnklast.Visible = false;
                }
                if (custompageIndex > 1 && custompageIndex < lastpageindex)
                {
                    lnkfirst.Visible = true;
                    lnkprevious.Visible = true;
                    lnknext.Visible = true;
                    lnklast.Visible = true;
                }
            }
            else
            {
                rptpage.DataSource = null;
                rptpage.DataBind();
                lnkfirst.Visible = false;
                lnkprevious.Visible = false;
                lnknext.Visible = false;
                lnklast.Visible = false;
            }
        }
        else
        {
            rptpage.DataSource = null;
            rptpage.DataBind();
            lnkfirst.Visible = false;
            lnkprevious.Visible = false;
            lnknext.Visible = false;
            lnklast.Visible = false;
        }
        #endregion
        
    }
    protected DataTable GetGridData()
    {
        BindDataCount();

        startindex = (custompageIndex - 1) * custompagesize + 1;


        // DataTable dt = ClstblWebDownload.tblWebDownload_GetDataBySearch1(txtSearch.Text.Trim(), ddlSearchState.SelectedValue, ddlTeam.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlSource.SelectedValue, ddlDup.SelectedValue, ddlwebdup.SelectedValue, startindex.ToString(), custompagesize.ToString());


        //Response.Write(startindex.ToString());
        //Response.End();
        string data = "select s.*,(case when NotDuplicate=0 then (case when c.ContMobile is not null then 1 else case when c2.ContEmail is not null then 1 else case when c3.CustPhone is not null then 1 else 0 end end end) else 0 end ) as duplicaterecord,(case when NotDuplicate=0 then (case when t.contmwebdownloadid is not null then 1 else case when t2.contewebdownloadid is not null then 1 else case when t3.contpwebdownloadid is not null then 1 else 0 end end end) else 0 end ) as duplicaterecord1 from tblWebDownload s left outer join (select min(webdownloadid) as contmwebdownloadid,ContMobile from tblWebDownload where ContMobile!='' and uploaded=0 and duplicate=0 group by ContMobile    having count(*) > 1) t on s.ContMobile = t.ContMobile left outer join (    select min(webdownloadid) as contewebdownloadid,ContEmail    from tblWebDownload where ContEmail!='' and uploaded=0  and duplicate=0  group by ContEmail    having count(*) > 1) t2 on s.ContEmail = t2.ContEmail left outer join (    select min(webdownloadid) as contpwebdownloadid,CustPhone    from tblWebDownload where CustPhone!='' and uploaded=0  and duplicate=0  group by CustPhone    having count(*) > 1) t3 on s.CustPhone = t3.CustPhone left outer join ( select ContMobile    from tblContacts where ContMobile!=''  group by ContMobile having count(*) > 0 ) c on s.ContMobile = c.ContMobile left outer join (    select ContEmail    from tblContacts where ContEmail!=''  group by ContEmail having count(*) > 0  ) c2 on s.ContEmail = c2.ContEmail left outer join (    select CustPhone    from tblCustomers where CustPhone!='' group by CustPhone having count(*) > 0  ) c3 on s.CustPhone = c3.CustPhone left outer join tblEmployees E on E.EmployeeID=s.EmployeeID where Uploaded = 0 and Duplicate=0 and (((case when NotDuplicate=0 then (case when c.ContMobile is not null then 1 else case when c2.ContEmail is not null then 1 else case when c3.CustPhone is not null then 1 else 2 end end end) else 2 end)= '" + ddlDup.SelectedValue + "') or (isnull('" + ddlDup.SelectedValue + "',0) =0))and (((case when NotDuplicate=0 then (case when t.contmwebdownloadid is not null then 1 else case when t2.contewebdownloadid is not null then 1 else case when t3.contpwebdownloadid is not null then 1 else 2 end end end) else 2 end ) = '" + ddlwebdup.SelectedValue + "') or (isnull('" + ddlwebdup.SelectedValue + "',0) =0)) and  (upper(Customer) like '%'+'" + txtSearch.Text.Trim() + "'+'%'  or  '" + txtSearch.Text.Trim() + "'='') and ( E.SalesTeamID like '%'+'" + ddlTeam.SelectedValue + "'+'%'  or  '" + ddlTeam.SelectedValue + "'='') and  ( upper([State]) like '%'+'" + ddlSearchState.SelectedValue + "'+'%'  or  '" + ddlSearchState.SelectedValue + "'='') and ( [subsource] like '%'+ LTRIM(RTRIM('" + ddlSource.SelectedValue + "'))+'%'  or  '" + ddlSource.SelectedValue + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= s.EntryDate or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=s.EntryDate  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') and ISNULL(s.IsDelete, 0) != 1 order by s.WebDownloadID DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(s.WebDownloadID) from tblWebDownload s where s.Uploaded = 0 ) end) ROWS ONLY ;";
        //  string data = "select s.*,(case when NotDuplicate=0 then (case when c.ContMobile is not null then 1 else case when c2.ContEmail is not null then 1 else case when c3.CustPhone is not null then 1 else 0 end end end) else 0 end ) as duplicaterecord,(case when NotDuplicate=0 then (case when t.contmwebdownloadid is not null then 1 else case when t2.contewebdownloadid is not null then 1 else case when t3.contpwebdownloadid is not null then 1 else 0 end end end) else 0 end ) as duplicaterecord1 from tblWebDownload s left outer join (select min(webdownloadid) as contmwebdownloadid,ContMobile from tblWebDownload where ContMobile!='' group by ContMobile    having count(*) > 1) t on s.ContMobile = t.ContMobile left outer join (    select min(webdownloadid) as contewebdownloadid,ContEmail    from tblWebDownload where ContEmail!=''    group by ContEmail    having count(*) > 1) t2 on s.ContEmail = t2.ContEmail left outer join (    select min(webdownloadid) as contpwebdownloadid,CustPhone    from tblWebDownload where CustPhone!=''    group by CustPhone    having count(*) > 1) t3 on s.CustPhone = t3.CustPhone left outer join ( select ContMobile    from tblContacts where ContMobile!='' ) c on s.ContMobile = c.ContMobile left outer join (    select ContEmail    from tblContacts where ContEmail!=''    ) c2 on s.ContEmail = c2.ContEmail left outer join (    select CustPhone    from tblCustomers where CustPhone!=''   ) c3 on s.CustPhone = c3.CustPhone left outer join tblEmployees E on E.EmployeeID=s.EmployeeID where Uploaded = 0 and Duplicate=0 and (((case when NotDuplicate=0 then (case when c.ContMobile is not null then 1 else case when c2.ContEmail is not null then 1 else case when c3.CustPhone is not null then 1 else 2 end end end) else 2 end)= '" + ddlDup.SelectedValue + "') or (isnull('" + ddlDup.SelectedValue + "',0) =0))and (((case when NotDuplicate=0 then (case when t.contmwebdownloadid is not null then 1 else case when t2.contewebdownloadid is not null then 1 else case when t3.contpwebdownloadid is not null then 1 else 2 end end end) else 2 end ) = '" + ddlwebdup.SelectedValue + "') or (isnull('" + ddlwebdup.SelectedValue + "',0) =0)) and  (upper(Customer) like '%'+'" + txtSearch.Text.Trim() + "'+'%'  or  '" + txtSearch.Text.Trim() + "'='') and ( E.SalesTeamID like '%'+'" + ddlTeam.SelectedValue + "'+'%'  or  '" + ddlTeam.SelectedValue + "'='') and  ( upper([State]) like '%'+'" + ddlSearchState.SelectedValue + "'+'%'  or  '" + ddlSearchState.SelectedValue + "'='') and ( [subsource] like '%'+ LTRIM(RTRIM('" + ddlSource.SelectedValue + "'))+'%'  or  '" + ddlSource.SelectedValue + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= s.EntryDate or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=s.EntryDate  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') order by s.WebDownloadID DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(s.WebDownloadID) from tblWebDownload s where s.Uploaded = 0 ) end) ROWS ONLY ;";
        // string data = "select s.*,(case when NotDuplicate=0 then (case when c.MContactID is not null then 1 else case when c2.EContactID is not null then 1 else case when c3.Pcustomerid is not null then 1 else 0 end end end) else 0 end ) as duplicaterecord,(case when NotDuplicate=0 then (case when t.contmwebdownloadid is not null then 1 else case when t2.contewebdownloadid is not null then 1 else case when t3.contpwebdownloadid is not null then 1 else 0 end end end) else 0 end ) as duplicaterecord1 from tblWebDownload s left outer join (select min(webdownloadid) as contmwebdownloadid,ContMobile from tblWebDownload where ContMobile!='' group by ContMobile    having count(*) > 1) t on s.ContMobile = t.ContMobile left outer join (    select min(webdownloadid) as contewebdownloadid,ContEmail    from tblWebDownload where ContEmail!=''    group by ContEmail    having count(*) > 1) t2 on s.ContEmail = t2.ContEmail left outer join (    select min(webdownloadid) as contpwebdownloadid,CustPhone    from tblWebDownload where CustPhone!=''    group by CustPhone    having count(*) > 1) t3 on s.CustPhone = t3.CustPhone left outer join (    select min(ContactID) as MContactID,ContMobile    from tblContacts where ContMobile!=''    group by ContMobile    having count(*) > 1) c on s.ContMobile = c.ContMobile left outer join (    select min(ContactID) as EContactID,ContEmail    from tblContacts where ContEmail!=''    group by ContEmail    having count(*) > 1) c2 on s.ContEmail = c2.ContEmail left outer join (    select min(customerid) as Pcustomerid,CustPhone    from tblCustomers where CustPhone!=''    group by CustPhone    having count(*) > 1) c3 on s.CustPhone = c3.CustPhone left outer join tblEmployees E on E.EmployeeID=s.EmployeeID where Uploaded = 0 and Duplicate=0 and (((case when NotDuplicate=0 then (case when c.MContactID is not null then 1 else case when c2.EContactID is not null then 1 else case when c3.Pcustomerid is not null then 1 else 2 end end end) else 2 end)= '" + ddlDup.SelectedValue + "') or (isnull('" + ddlDup.SelectedValue + "',0) =0))and (((case when NotDuplicate=0 then (case when t.contmwebdownloadid is not null then 1 else case when t2.contewebdownloadid is not null then 1 else case when t3.contpwebdownloadid is not null then 1 else 2 end end end) else 2 end ) = '" + ddlwebdup.SelectedValue + "') or (isnull('" + ddlwebdup.SelectedValue + "',0) =0)) and  (upper(Customer) like '%'+'" + txtSearch.Text.Trim() + "'+'%'  or  '" + txtSearch.Text.Trim() + "'='') and ( E.SalesTeamID like '%'+'" + ddlTeam.SelectedValue + "'+'%'  or  '" + ddlTeam.SelectedValue + "'='') and  ( upper([State]) like '%'+'" + ddlSearchState.SelectedValue + "'+'%'  or  '" + ddlSearchState.SelectedValue + "'='') and ( [subsource] like '%'+ LTRIM(RTRIM('" + ddlSource.SelectedValue + "'))+'%'  or  '" + ddlSource.SelectedValue + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "' <= s.EntryDate or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtStartDate.Text) + "'='')  and ('" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "' >=s.EntryDate  or '" + SiteConfiguration.ConveqrtToSqlDate_View(txtEndDate.Text) + "'='') order by s.WebDownloadID DESC OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(s.WebDownloadID) from tblWebDownload s where s.Uploaded = 0 ) end) ROWS ONLY ;";
        //Response.Write(data);
        //Response.End();

        DataTable dt = ClstblCustomers.query_execute(data);
        //Response.Write(dt.Rows.Count);
        //Response.End();

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {

            GridView1.DataSource = dt;
            GridView1.DataBind();
            //SetNoRecords();
            //   divnopage.Visible = true;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;

                }
                else
                {
                    foreach (RepeaterItem item in rptpage.Items)
                    {
                        LinkButton senderbtn = (LinkButton)item.FindControl("lnkpagebtn");
                        HiddenField postIdHidden = (HiddenField)item.FindControl("psotId");
                        LinkButton lnkpagebtn = (LinkButton)item.FindControl("lnkpagebtn");
                        if (Convert.ToInt32(postIdHidden.Value) == custompageIndex)
                        {
                            senderbtn.CssClass = "Linkbutton activepage";
                        }
                    }
                    divnopage.Visible = true;
                    int iTotalRecords = Convert.ToInt32(hdncountdata.Value);
                    int iEndRecord = GridView1.PageSize * (custompageIndex);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }

        BingGridDropDown();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            // custompagesize = 0;
        }

        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            // custompagesize = Convert.ToInt32(ddlSelectRecords.SelectedValue.ToString());
        }
        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        divnopage.Visible = false;
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        hdnWebDownloadID.Value = id;
        MPECustomerName.Show();
        GetDuplicate();
    }

    public void GetDuplicate()
    {
        SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(hdnWebDownloadID.Value);

        DataTable dt = ClstblCustomers.tblCustomers_SelectByCheckExists(st.Customer, st.CustPhone, st.ContEmail, st.ContMobile);
        DataList1.DataSource = null;
        DataList1.DataSource = dt;
        DataList1.DataBind();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
        BingGridDropDown();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    public void HidePanel()
    {
        
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int success = 0;
        Response.Expires = 400;
        Server.ScriptTimeout = 1200;
        if (FileUpload1.HasFile)
        {
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName);
            string connectionString = "";

            //if (FileUpload1.FileName.EndsWith(".csv"))
            //{
            //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
            //}
            // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

            //connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\Lead\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";


            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Lead\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (FileUpload1.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\Lead\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
            }


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    string employeeid = string.Empty;
                    string flag = "true";
                    using (DbDataReader dr1 = command.ExecuteReader())
                    {
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                if (dr1["Source"].ToString() != string.Empty)
                                {
                                    string Source = dr1["Source"].ToString();

                                    if (Source == string.Empty)
                                    {
                                        string First = "";
                                        string Last = "";
                                        string Email = "";
                                        string Phone = "";
                                        string Mobile = "";
                                        string Address = "";
                                        string City = "";
                                        string State = "";
                                        string PostCode = "";
                                        string System = "";
                                        string Roof = "";
                                        string Angle = "";
                                        string Story = "";
                                        string HouseAge = "";
                                        string Notes = "";
                                        string SubSource = "";

                                        First = dr1["First"].ToString();
                                        Last = dr1["Last"].ToString();
                                        Email = dr1["Email"].ToString();
                                        Phone = dr1["Phone"].ToString();
                                        Mobile = dr1["Mobile"].ToString();
                                        Address = dr1["Address"].ToString();
                                        City = dr1["City"].ToString();
                                        State = dr1["State"].ToString();
                                        PostCode = dr1["PostCode"].ToString();
                                        Source = dr1["Source"].ToString();
                                        System = dr1["System"].ToString();
                                        Roof = dr1["Roof"].ToString();
                                        Angle = dr1["Angle"].ToString();
                                        Story = dr1["Story"].ToString();
                                        HouseAge = dr1["HouseAge"].ToString();
                                        Notes = dr1["Notes"].ToString();
                                        SubSource = dr1["SubSource"].ToString();

                                        if ((First != string.Empty) || (Last != string.Empty) || (Email != string.Empty) || (Phone != string.Empty) || (Mobile != string.Empty) || (Address != string.Empty) || (City != string.Empty) || (State != string.Empty) || (PostCode != string.Empty) || (System != string.Empty) || (Roof != string.Empty) || (Angle != string.Empty) || (Story != string.Empty) || (HouseAge != string.Empty) || (Notes != string.Empty))
                                        {
                                            flag = "false";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            if (flag == "false")
                            {
                                PanEmpty.Visible = true;
                            }
                            else
                            {
                                while (dr.Read())
                                {
                                    string Customer = "";
                                    string First = "";
                                    string Last = "";
                                    string Email = "";
                                    string Phone = "";
                                    string Mobile = "";
                                    string Address = "";
                                    string City = "";
                                    string State = "";
                                    string PostCode = "";
                                    string Source = "";
                                    string System = "";
                                    string Roof = "";
                                    string Angle = "";
                                    string Story = "";
                                    string HouseAge = "";
                                    string Notes = "";
                                    string SubSource = "";

                                    First = dr["First"].ToString();
                                    Last = dr["Last"].ToString();
                                    Email = dr["Email"].ToString();
                                    Phone = dr["Phone"].ToString();
                                    Mobile = dr["Mobile"].ToString();
                                    Address = dr["Address"].ToString();
                                    City = dr["City"].ToString();
                                    State = dr["State"].ToString();
                                    PostCode = dr["PostCode"].ToString();
                                    Source = dr["Source"].ToString();
                                    System = dr["System"].ToString();
                                    Roof = dr["Roof"].ToString();
                                    Angle = dr["Angle"].ToString();
                                    Story = dr["Story"].ToString();
                                    HouseAge = dr["HouseAge"].ToString();
                                    Notes = dr["Notes"].ToString();
                                    SubSource = dr["SubSource"].ToString();

                                    if (dr["Mobile"].ToString() != string.Empty)
                                    {
                                        if (Mobile.Substring(0, 1) == "0")
                                        {
                                            Mobile = dr["Mobile"].ToString();
                                        }
                                        else
                                        {
                                            Mobile = "0" + dr["Mobile"].ToString();
                                        }
                                    }

                                    if (Source.Trim() == "Lead Source")
                                    {
                                        if (SubSource == string.Empty)
                                        {
                                            SubSource = "Q-Company";
                                        }
                                        else
                                        {
                                            DataTable dtSubSource = ClstblWebDownload.tblCustSourceSub_SelectBySourceSub(SubSource);
                                            if (dtSubSource.Rows.Count > 0)
                                            {
                                                SubSource = dtSubSource.Rows[0]["CustSourceSub"].ToString();
                                            }
                                            else
                                            {
                                                SubSource = "Q-Company";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        SubSource = "";
                                    }
                                    Customer = First.Trim() + " " + Last.Trim();
                                    if ((First != string.Empty) || (Last != string.Empty) || (Email != string.Empty) || (Phone != string.Empty) || (Mobile != string.Empty) || (Address != string.Empty) || (City != string.Empty) || (State != string.Empty) || (PostCode != string.Empty) || (System != string.Empty) || (Roof != string.Empty) || (Angle != string.Empty) || (Story != string.Empty) || (HouseAge != string.Empty) || (Notes != string.Empty))
                                    {
                                        //try
                                        //{

                                        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                                        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                        //Response.Write(Customer + "==>" + First + "==>" + Last + "==>" + Email + "==>" + Phone + "==>" + Mobile + "==>" + Address + "==>" + City + "==>" + State + "==>" + PostCode + "==>" + Source + "==>" + System + "==>" + Roof + "==>" + Angle + "==>" + Story + "==>" + HouseAge + "==>" + Notes + "==>" + SubSource + "==>" + stemp.EmployeeID);
                                        //Response.End();
                                        success = ClstblWebDownload.tblWebDownload_Insert(Customer, First, Last, Email, Phone, Mobile, Address, City, State, PostCode, Source, System, Roof, Angle, Story, HouseAge, Notes, SubSource, stemp.EmployeeID);
										bool sucess1 = ClstblWebDownload.tblWebDownload_IsDeleteflag(Convert.ToString(success), "0");
                                        //}
                                        //catch { }                                       
                                    }
                                    if (success > 0)
                                    {
                                        SetAdd1();
                                    }
                                    else
                                    {
                                        SetError1();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        BindGrid(0);
    }

    protected void chkNotduplicate_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chkNotduplicate = (CheckBox)row.FindControl("chkNotduplicate");
            HiddenField hdnWebDownloadID2 = (HiddenField)row.FindControl("hdnWebDownloadID2");
            if (chkNotduplicate.Checked == true)
            {
                ClstblWebDownload.tblWebDownload_Update_NotDuplicate(hdnWebDownloadID2.Value, Convert.ToString(true));
            }
        }
        BindGrid(0);
    }

    protected void chkDuplicateRecord_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chkDuplicateRecord = (CheckBox)row.FindControl("chkDuplicateRecord");
            HiddenField hdnWebDownloadID1 = (HiddenField)row.FindControl("hdnWebDownloadID1");
            if (chkDuplicateRecord.Checked == true)
            {
                ClstblWebDownload.tblWebDownload_Update_Duplicate(hdnWebDownloadID1.Value, Convert.ToString(true));
            }
        }
        BindGrid(0);
    }

    protected void btnAssidnLead_Click(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stemployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            CheckBox chksales = (CheckBox)gridRow.FindControl("chksales");
            HiddenField hdnSourceSub = (HiddenField)gridRow.FindControl("hdnSourceSub");
            HiddenField hdnSource = (HiddenField)gridRow.FindControl("hdnSource");

            string CustSourceSubID = "";
            string CustSourceID = "";

            if (ddlSalesRep.SelectedValue != string.Empty)
            {
                if (chksales.Checked)
                {
                    DataTable dtSource = ClstblWebDownload.tblCustSource_SelectBySource(hdnSource.Value);
                    if (dtSource.Rows.Count > 0)
                    {
                        CustSourceID = dtSource.Rows[0]["CustSourceID"].ToString();
                    }
                    else
                    {
                        CustSourceID = "18";
                    }

                    if (hdnSource.Value.Contains("Lead Source").ToString() == true.ToString())
                    {
                        string value = hdnSourceSub.Value;
                        string[] words = value.Split(',');
                        foreach (string word in words)
                        {
                            value = word.ToString();
                        }


                        DataTable dtSourceSub = ClstblWebDownload.tblCustSourceSub_SelectBySourceSub(value);

                        if (dtSourceSub.Rows.Count > 0)
                        {
                            CustSourceSubID = dtSourceSub.Rows[0]["CustSourceSubID"].ToString();
                        }
                        else
                        {
                            CustSourceSubID = "2";
                        }

                    }
                    else
                    {
                        CustSourceSubID = "";
                    }

                    //  if (ddlSalesRep.SelectedValue.ToString() != "")
                    // {
                    ClstblWebDownload.tblWebDownload_Update_Uploaded(id, Convert.ToString(true));
                    ClstblWebDownload.tblWebDownload_Update_ReadFlag(id, "1");

                    SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(id);
                    string BranchLocation = st.State + " - " + st.City;
                    // Response.Write(ddlSalesRep.SelectedValue.ToString());
                    int success = ClstblCustomers.tblCustomers_Insert("", ddlSalesRep.SelectedValue.ToString(), "False", "1", CustSourceID, CustSourceSubID, "", "", "", stemployeeid.EmployeeID, "", "", "", "", "", st.Customer, BranchLocation, "", st.Address, st.City, st.State, st.PostCode, st.Address, st.City, st.State, st.PostCode, "", st.CustPhone, "", "", st.CustPhone, "", st.PageUrl, "", "", "", "", "False", "", "", "", "", "", "", "", "");
                    //  Response.End();
                    int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), "", st.ContFirst, st.ContLast, st.ContMobile, st.ContEmail, ddlSalesRep.SelectedValue.ToString(), stemployeeid.EmployeeID);
                    int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), stemployeeid.EmployeeID, "1");

                    int sucno = ClstblCustomers.tblCompanyNumber_Insert(Convert.ToString(success));
                    DataTable dtCN = ClstblCustomers.tblCompanyNumber_Select(Convert.ToString(success));
                    string CompanyNumber = dtCN.Rows[0]["CompanyNumberID"].ToString();
                    bool sucCompNo = ClstblCustomers.tblCustomers_UpdateCompanyNumber(Convert.ToString(success), CompanyNumber);

                    ClstblWebDownload.tblWebDownload_Update_CustomerLinkID(id, success.ToString());

                    bool suc_EmailMobile = ClstblContacts.tblCustomers_Update_CustEmailMobile(Convert.ToString(success), st.ContEmail, st.ContMobile);

                    if (!string.IsNullOrEmpty(st.gclid))
                    {
                        string str = st.gclid;
                        bool updategcid = ClstblCustomers.tblCustomers_Updategclid(Convert.ToString(success), str);
                        if (str.Contains("?gclid="))
                        {
                            //int start = str.IndexOf("?gclid=") + ("?gclid=").Length;
                            //string value = str.Substring(start);
                            //bool updategcid = ClstblCustomers.tblCustomers_Updategclid(Convert.ToString(success), value);



                            ////GoogleSheet Code Start
                            //try
                            //{
                            //    GoogleCredential credential;
                            //    //Reading Credentials File...
                            //    //For Local
                            //    //string filepath = HttpContext.Current.Server.MapPath("app_client_secret.json");

                            //    //For Live
                            //    string filepath = HttpContext.Current.Server.MapPath("AriseSolar-692588a0321d.json");
                            //    using (var stream = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                            //    {
                            //        credential = GoogleCredential.FromStream(stream)
                            //            .CreateScoped(Scopes);
                            //    }

                            //    //Creating Google Sheets API service...
                            //    service = new SheetsService(new BaseClientService.Initializer()
                            //    {
                            //        HttpClientInitializer = credential,
                            //        ApplicationName = ApplicationName,
                            //    });

                            //    List<object> list = null;
                            //    //var range = $"{sheet}!A:S";
                            //    string range = "A:F";
                            //    var valueRange = new ValueRange();
                            //    DateTime dt = Convert.ToDateTime(st.EntryDate);
                            //    list = new List<object>() { value,"", dt.ToString("MM/dd/yyyy hh:mm:ss"),"","","" };

                            //    valueRange.Values = new List<IList<object>> { list };
                            //    var appendRequest = service.Spreadsheets.Values.Append(valueRange, SpreadsheetId, range);
                            //    appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
                            //    var appendReponse = appendRequest.Execute();

                            //}
                            //catch (Exception ex)
                            //{

                            //}
                            ////GoogleSheet Code End


                        }
                    }
                    //}

                    bool sucUserIp = ClstblCustomers.tblCustomers_userip(Convert.ToString(success), st.userIp);
                }
            }


        }
        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees stemployeeid = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        //int rowsCount = GridView1.Rows.Count;
        //GridViewRow gridRow;
        //DropDownList ddlSalesRep;
        //HiddenField hdnSourceSub;
        //HiddenField hdnSource;

        //string CustSourceSubID = "";
        //string CustSourceID = "";

        //for (int i = 0; i < rowsCount; i++)
        //{
        //    string id;
        //    gridRow = GridView1.Rows[i];
        //    id = GridView1.DataKeys[i].Value.ToString();

        //    ddlSalesRep = (DropDownList)gridRow.FindControl("ddlSalesRep");
        //    hdnSource = (HiddenField)gridRow.FindControl("hdnSource");
        //    hdnSourceSub = (HiddenField)gridRow.FindControl("hdnSourceSub");

        //    DataTable dtSource = ClstblWebDownload.tblCustSource_SelectBySource(hdnSource.Value);
        //    if (dtSource.Rows.Count > 0)
        //    {
        //        CustSourceID = dtSource.Rows[0]["CustSourceID"].ToString();
        //    }
        //    else
        //    {
        //        CustSourceID = "18";
        //    }

        //    if (hdnSource.Value.Contains("Lead Source").ToString() == true.ToString())
        //    {
        //        string value = hdnSourceSub.Value;
        //        string[] words = value.Split(',');
        //        foreach (string word in words)
        //        {
        //            value = word.ToString();
        //        }


        //        DataTable dtSourceSub = ClstblWebDownload.tblCustSourceSub_SelectBySourceSub(value);

        //        if (dtSourceSub.Rows.Count > 0)
        //        {
        //            CustSourceSubID = dtSourceSub.Rows[0]["CustSourceSubID"].ToString();
        //        }
        //        else
        //        {
        //            CustSourceSubID = "2";
        //        }

        //    }
        //    else
        //    {
        //        CustSourceSubID = "";
        //    }

        //    if (ddlSalesRep.SelectedValue.ToString() != "")
        //    {
        //        ClstblWebDownload.tblWebDownload_Update_Uploaded(id, Convert.ToString(true));
        //        SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(id);
        //        string BranchLocation = st.State + " - " + st.City;

        //        int success = ClstblCustomers.tblCustomers_Insert("", ddlSalesRep.SelectedValue.ToString(), "False", "3", CustSourceID, CustSourceSubID, "", "", "", stemployeeid.EmployeeID, "", "", "", "", "", st.Customer, BranchLocation, "", st.Address, st.City, st.State, st.PostCode, st.Address, st.City, st.State, st.PostCode, "", st.CustPhone, "", "", st.CustPhone, "", st.Notes, "", "", "", "", "False", "", "", "", "", "", "", "", "");
        //        int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), "", st.ContFirst, st.ContLast, st.ContMobile, st.ContEmail, ddlSalesRep.SelectedValue.ToString(), stemployeeid.EmployeeID);
        //        int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), stemployeeid.EmployeeID, "1");

        //        int sucno = ClstblCustomers.tblCompanyNumber_Insert(Convert.ToString(success));
        //        DataTable dtCN = ClstblCustomers.tblCompanyNumber_Select(Convert.ToString(success));
        //        string CompanyNumber = dtCN.Rows[0]["CompanyNumberID"].ToString();
        //        bool sucCompNo = ClstblCustomers.tblCustomers_UpdateCompanyNumber(Convert.ToString(success), CompanyNumber);
        //    }
        //}
        BindGrid(0);
        //SiteConfiguration.GetNotifications(ddlSalesRep.SelectedValue);
    }

    public void BingGridDropDown()
    {
        //foreach (GridViewRow row in GridView1.Rows)
        {
            //if (row.RowType == DataControlRowType.DataRow)
            {
                //LinkButton btnduplicate = (LinkButton)row.FindControl("btnduplicate");
                //DropDownList ddlSalesRep = (DropDownList)row.FindControl("ddlSalesRep");

                //if (btnduplicate.Visible == true)
                //{
                //    ddlSalesRep.Visible = false;
                //}
                ddlSalesRep.Items.Clear();
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectAll_include();
                ddlSalesRep.DataMember = "fullname";
                ddlSalesRep.DataTextField = "fullname";
                ddlSalesRep.DataValueField = "EmployeeID";
                ddlSalesRep.DataBind();
            }
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //bool sucess1 = ClstblWebDownload.tblWebDownload_Delete(id);

        //if (sucess1)
        //{
        //    PanSuccess.Visible = true;
        //}
        //else
        //{
        //    PanError.Visible = true;
        //}
        ////--- do not chage this code end------
        ////--- do not chage this code start------
        //GridView1.EditIndex = -1;
        //BindGrid(0);
        //BindGrid(1);
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlTeam.SelectedValue = "";
        ddlSource.SelectedValue = "";
        ddlDup.SelectedValue = "";

        BindGrid(0);
    }
    protected void DataList1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        MPECustomerName.Show();
        DataList1.PageIndex = e.NewPageIndex;
        GetDuplicate();
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        //GridViewRow gvrow = GridView1.BottomPagerRow;
        //Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

        //lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        //int[] page = new int[7];
        //page[0] = GridView1.PageIndex - 2;
        //page[1] = GridView1.PageIndex - 1;
        //page[2] = GridView1.PageIndex;
        //page[3] = GridView1.PageIndex + 1;
        //page[4] = GridView1.PageIndex + 2;
        //page[5] = GridView1.PageIndex + 3;
        //page[6] = GridView1.PageIndex + 4;

        //for (int i = 0; i < 7; i++)
        //{
        //    if (i != 3)
        //    {
        //        if (page[i] < 1 || page[i] > GridView1.PageCount)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Visible = false;
        //        }
        //        else
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //            lnkbtn.Text = Convert.ToString(page[i]);
        //            lnkbtn.CommandName = "PageNo";
        //            lnkbtn.CommandArgument = lnkbtn.Text;
        //        }
        //    }
        //}
        //if (GridView1.PageIndex == 0)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
        //    lnkbtn.Visible = false;
        //}
        //if (GridView1.PageIndex == GridView1.PageCount - 1)
        //{
        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
        //    lnkbtn.Visible = false;
        //    lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //    lnkbtn.Visible = false;
        //}
        //Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        //if (dv.ToTable().Rows.Count > 0)
        //{
        //    //Response.Write(dv.ToTable().Rows.Count);
        //    //Response.End();
        //    int iTotalRecords = dv.ToTable().Rows.Count;
        //    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //    if (iEndRecord > iTotalRecords)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    if (iStartsRecods == 0)
        //    {
        //        iStartsRecods = 1;
        //    }
        //    if (iEndRecord == 0)
        //    {
        //        iEndRecord = iTotalRecords;
        //    }
        //    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        //}
        //else
        //{
        //    ltrPage.Text = "";
        //}
        //BindScript();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.Pager)
        //{
        //    GridViewRow gvr = e.Row;
        //    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
         
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p1");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p2");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p4");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p5");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p6");
        //    lb.Command += new CommandEventHandler(lb_Command);
           
        //}
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    lnkpagebtn.CssClass = "active";
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        {
            lastpageindex = lastpageindex + 1;
        }
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    protected void chkheader_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkheader = (CheckBox)GridView1.HeaderRow.FindControl("chkheader");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk1 = (CheckBox)row.FindControl("chk1");

                if (chkheader.Checked == true)
                {
                    chk1.Checked = true;
                }
                else
                {
                    chk1.Checked = false;
                }
            }
        }
    }
    protected void btndup_Click(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            CheckBox chk1 = (CheckBox)gridRow.FindControl("chk1");
            HiddenField hdnWebDownloadID1 = (HiddenField)gridRow.FindControl("hdnWebDownloadID1");
            if (ddldupdrp.SelectedValue != string.Empty && hdnWebDownloadID1.Value != string.Empty)
            {
                if (chk1.Checked)
                {
                    if (ddldupdrp.SelectedValue == "1")
                    {
                        ClstblWebDownload.tblWebDownload_Update_Duplicate(hdnWebDownloadID1.Value, Convert.ToString(true));
                    }
                    if (ddldupdrp.SelectedValue == "2")
                    {
                        ClstblWebDownload.tblWebDownload_Update_NotDuplicate(hdnWebDownloadID1.Value, Convert.ToString(true));
                    }
                }
            }
        }
        BindGrid(0);
    }
    protected void chksalesheader_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chksalesheader = (CheckBox)GridView1.HeaderRow.FindControl("chksalesheader");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chksales = (CheckBox)row.FindControl("chksales");

                if (chksalesheader.Checked == true)
                {
                    chksales.Checked = true;
                }
                else
                {
                    chksales.Checked = false;
                }
            }
        }
    }
    protected void btndelete_Click(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();
            //Response.Write(id+"</br>");

            CheckBox chkdelete = (CheckBox)gridRow.FindControl("chkdelete");

            if (chkdelete.Checked)
            {

                bool sucess1 = ClstblWebDownload.tblWebDownload_Delete(id);

            }
        }
        //Response.End();
        BindGrid(0);
    }
    protected void chkdeletewhole_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkdeletewhole = (CheckBox)GridView1.HeaderRow.FindControl("chkdeletewhole");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkdelete = (CheckBox)row.FindControl("cbxDelete");

                if (chkdeletewhole.Checked == true)
                {
                    chkdelete.Checked = true;
                }
                else
                {
                    chkdelete.Checked = false;
                }
            }
        }
    }

    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtender1.Show();
        GridView2.PageIndex = e.NewPageIndex;
        GetDuplicate1();
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "webdup")
        {
            string id = e.CommandArgument.ToString();
            hdnwebdup.Value = id;
            ModalPopupExtender1.Show();
            GetDuplicate1();
        }
    }
    public void GetDuplicate1()
    {

        SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(hdnwebdup.Value);
        //Response.Write(st.Customer + "1=========" + st.CustPhone + "2=======" + st.ContEmail + "3=========" + st.ContMobile + "4=========" + st.Address);
        //Response.End();
        DataTable dt = ClstblCustomers.tblWebDownload_SelectByCheckExists(st.Customer, st.CustPhone, st.ContEmail, st.ContMobile, st.Address);
        GridView2.DataSource = null;
        GridView2.DataSource = dt;
        GridView2.DataBind();
    }
    protected void btn3quotes_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread t = new System.Threading.Thread(DoWork);
        //t.Start();
        DoWork();

    }
    private void DoWork()
    {
        string sWebServiceUrl = "https://client.3quotes.com.au/api/leads";

        // Create a Web service Request for the URL.           
        WebRequest objWebRequest = WebRequest.Create(sWebServiceUrl);

        //Create a proxy for the service request  
        objWebRequest.Proxy = new WebProxy();

        // set the credentials to authenticate request, if required by the server  
        objWebRequest.Credentials = new NetworkCredential("divya@eurosolar.com.au", "e6QsuLK09E");
        objWebRequest.Proxy.Credentials = new NetworkCredential("divya@eurosolar.com.au", "e6QsuLK09E");

        //Get the web service response.  
        HttpWebResponse objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();

        //get the contents return by the server in a stream and open the stream using a                                                                   -            StreamReader for easy access.  
        StreamReader objStreamReader = new StreamReader(objWebResponse.GetResponseStream());


        // Read the contents.  
        string sResponse = objStreamReader.ReadToEnd();


        Console.WriteLine(sResponse);
        Console.ReadLine();

        // Cleanup the streams and the response.  
        objStreamReader.Close();
        objWebResponse.Close();

        JavaScriptSerializer js = new JavaScriptSerializer();
        dynamic d = js.Deserialize<dynamic>(sResponse);
        int accepted = d["accepted"];
        string total = d["total"];
        dynamic[] leads = d["leads"];





        //for (int i = 0; i < leads.Length; i++)
        //{
        //    //Response.Write();

        //    //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        //    string name = leads[i]["First Name"].ToString() + " " + leads[i]["Last Name"].ToString();
        //    string leadid = "Q" + leads[i]["ID"].ToString();
        //    string PhoneNumber = leads[i]["Phone Number"].ToString();
        //    string Email = leads[i]["Email Address"].ToString();
        //    string Postcode = leads[i]["Postcode"].ToString();
        //    string State = leads[i]["State"].ToString();
        //    string TimeofRequest = leads[i]["Time of Request"].ToString();


        //    string notes = leads[i]["Recording"].ToString();
        //    string input = leads[i]["Property Address "].ToString();

        //    string[] splitString = input.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

        //    string addressresult = splitString[0].Trim();
        //    string city = "";
        //    if (splitString.Length >= 2)
        //    {
        //        city = splitString[1].ToString();
        //    }


        //    // Response.Write(input + "========" + addressresult + "=======" + splitString[1] + "</br>");//Butlers Road, Ferntree Gully, Victoria, Australia


        //    //   string city = Convert.ToString(splitString[1]);


        //    try
        //    {
        //        //    Response.Write("<br/>"+notes);
        //        int succ = ClstblCustomers.tblWebDownload_Insert_webapi(leadid, name, leads[i]["First Name"].ToString(), leads[i]["Last Name"].ToString(), PhoneNumber, Email, Postcode, State, addressresult, city, notes, TimeofRequest, "92");
        //    }
        //    catch
        //    {
        //    }

        //}
        //SqlConnection.ClearAllPools();
        //==========================================
        //Response.Write(leads.Length.ToString());

        ///////////////////////////////////////////////////////////////////
        DataTable dt = new DataTable();
        dt.Columns.AddRange(new DataColumn[29] { new DataColumn("Customer", typeof(string)),
                        new DataColumn("ContFirst", typeof(string)),
                        new DataColumn("ContLast",typeof(string)),
        new DataColumn("ContEmail",typeof(string)),
        new DataColumn("CustPhone",typeof(string)),
        new DataColumn("ContMobile",typeof(string)),
         new DataColumn("Address",typeof(string)),
         new DataColumn("City",typeof(string)),
         new DataColumn("State",typeof(string)),
         new DataColumn("PostCode",typeof(int)),
        new DataColumn("Source",typeof(string)),
    new DataColumn("System",typeof(string)),
        new DataColumn("Roof",typeof(string)),
        new DataColumn("Angle",typeof(string)),
        new DataColumn("Story",typeof(string)),
        new DataColumn("HouseAge",typeof(string)),
        new DataColumn("Notes",typeof(string)),
        new DataColumn("EntryDate",typeof(DateTime)),
        new DataColumn("PreferredTime",typeof(string)),
        new DataColumn("Uploaded",typeof(Boolean)),
        new DataColumn("Duplicate",typeof(Boolean)),
        new DataColumn("NotDuplicate",typeof(Boolean)),
          new DataColumn("SalesTeamID",typeof(int)),
          new DataColumn("EmployeeID",typeof(int)),
          new DataColumn("LinkID",typeof(string)),
          new DataColumn("FormatFixed",typeof(Boolean)),
         // new DataColumn("upsize_ts",typeof(string)),
          new DataColumn("SubSource",typeof(string)),
          new DataColumn("ReadFlag",typeof(int)),
          new DataColumn("CustomerLinkID",typeof(int))});//ContFirst


        DataTable dt_data = ClstblWebDownload.tblWebDownload_LeadService();

        for (int i = 0; i < leads.Length; i++)
        {
            if (dt_data.Select("linkid = '" + "Q" + leads[i]["ID"].ToString() + "'").Length == 0)
            {

                //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                string Customer = leads[i]["First Name"].ToString() + " " + leads[i]["Last Name"].ToString();//1    
                string ContFirst = leads[i]["First Name"].ToString();//2
                string ContLast = leads[i]["Last Name"].ToString();//3
                string ContEmail = leads[i]["Email Address"].ToString();//4
                string CustPhone = string.Empty;//6
                string ContMobile = leads[i]["Phone Number"].ToString();//5
                string input = leads[i]["Property Address "].ToString();
                string[] splitString = input.Split(',');
                string Address = splitString[0].Trim();//7
                string City = string.Empty;
                if (splitString.Length >= 2)
                {
                    City = splitString[1].ToString();
                }
                //string City = splitString[1].Trim();//8
                string State = leads[i]["State"].ToString();//9
                string Postcode = leads[i]["Postcode"].ToString();//10
                string Source = string.Empty;//11
                string System = string.Empty;//12
                string Roof = string.Empty;//13
                string Angle = string.Empty;//14
                string Story = string.Empty;//15
                string HouseAge = string.Empty;//16
                string Notes = leads[i]["Recording"].ToString();//17
                string EntryDate = leads[i]["Time of Request"].ToString();//18
                string PreferredTime = string.Empty;//19
                string Uploaded = false.ToString();//20
                string Duplicate = false.ToString();//21
                string NotDuplicate = false.ToString();//22
                string SalesTeamID = "0";//23
                string EmployeeID = "92";//24
                string LinkID = "Q" + leads[i]["ID"].ToString();//25
                string FormatFixed = false.ToString();//26
                //  string upsize_ts = string.Empty;
                string SubSource = "3Quotes";//27
                string ReadFlag = "0";//28
                string CustomerLinkID = "0";//29


                dt.Rows.Add(Customer, ContFirst, ContLast, ContEmail, CustPhone, ContMobile, Address, City, State, Postcode, Source, System, Roof, Angle, Story, HouseAge, Notes, EntryDate, PreferredTime, Uploaded, Duplicate, NotDuplicate, SalesTeamID, EmployeeID, LinkID, FormatFixed, SubSource, ReadFlag, CustomerLinkID);//EmployeeID, LinkID, FormatFixed, SubSource, ReadFlag, CustomerLinkID


            }

        }
        //  Response.Write(dt.Rows.Count);
        //    Response.End();
        if (dt.Rows.Count > 0)
        {
            string consString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {

                //var transaction = con.BeginTransaction();
                //  SqlTransaction transaction = con.BeginTransaction();
                ////using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.TableLock, transaction))
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {

                    //Set the database table name
                    sqlBulkCopy.DestinationTableName = "dbo.tblWebDownload";

                    //[OPTIONAL]: Map the DataTable columns with that of the database table
                    sqlBulkCopy.ColumnMappings.Add("Customer", "Customer");//1
                    sqlBulkCopy.ColumnMappings.Add("ContFirst", "ContFirst");//11
                    sqlBulkCopy.ColumnMappings.Add("ContLast", "ContLast");//12
                    sqlBulkCopy.ColumnMappings.Add("ContEmail", "ContEmail");//2
                    sqlBulkCopy.ColumnMappings.Add("CustPhone", "CustPhone");//3
                    sqlBulkCopy.ColumnMappings.Add("ContMobile", "ContMobile");//5
                    sqlBulkCopy.ColumnMappings.Add("Address", "Address");//6
                    sqlBulkCopy.ColumnMappings.Add("City", "City");//7
                    sqlBulkCopy.ColumnMappings.Add("State", "State");//8
                    sqlBulkCopy.ColumnMappings.Add("PostCode", "PostCode");//9
                    sqlBulkCopy.ColumnMappings.Add("Source", "Source");//10
                    sqlBulkCopy.ColumnMappings.Add("System", "System");//11
                    sqlBulkCopy.ColumnMappings.Add("Roof", "Roof");//12
                    sqlBulkCopy.ColumnMappings.Add("Angle", "Angle");//12
                    sqlBulkCopy.ColumnMappings.Add("Story", "Story");//12
                    sqlBulkCopy.ColumnMappings.Add("HouseAge", "HouseAge");//12
                    sqlBulkCopy.ColumnMappings.Add("Notes", "Notes");//12
                    sqlBulkCopy.ColumnMappings.Add("EntryDate", "EntryDate");//12
                    sqlBulkCopy.ColumnMappings.Add("PreferredTime", "PreferredTime");//12
                    sqlBulkCopy.ColumnMappings.Add("Uploaded", "Uploaded");//12
                    sqlBulkCopy.ColumnMappings.Add("Duplicate", "Duplicate");//12
                    sqlBulkCopy.ColumnMappings.Add("NotDuplicate", "NotDuplicate");//12
                    sqlBulkCopy.ColumnMappings.Add("SalesTeamID", "SalesTeamID");//12
                    sqlBulkCopy.ColumnMappings.Add("EmployeeID", "EmployeeID");//12
                    sqlBulkCopy.ColumnMappings.Add("LinkID", "LinkID");//12
                    sqlBulkCopy.ColumnMappings.Add("FormatFixed", "FormatFixed");//12
                    sqlBulkCopy.ColumnMappings.Add("SubSource", "SubSource");//12
                    sqlBulkCopy.ColumnMappings.Add("ReadFlag", "ReadFlag");//12
                    sqlBulkCopy.ColumnMappings.Add("CustomerLinkID", "CustomerLinkID");//12
                    // sqlBulkCopy.ColumnMappings.Add("ContLast", "ContLast");//12

                    con.Open();
                    //Response.Write(sqlBulkCopy);
                    //Response.End();
                    sqlBulkCopy.WriteToServer(dt);
                    con.Close();
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
       
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;

        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();
            //Response.Write(id);
            //Response.End();
            CheckBox cbxDelete = (CheckBox)gridRow.FindControl("cbxDelete");

            if (cbxDelete.Checked == true)
            {


                bool sucess1 = ClstblWebDownload.tblWebDownload_Delete(id);
                //Response.Write(sucess1);
                //Response.End();

            }
        }
        //Response.End();
        BindGrid(0);
    }



    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }




    protected void action_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (action.SelectedValue == "1")
        {
            int rowsCount = GridView1.Rows.Count;
            GridViewRow gridRow;

            for (int i = 0; i < rowsCount; i++)
            {
                string id;
                gridRow = GridView1.Rows[i];
                id = GridView1.DataKeys[i].Value.ToString();
                //Response.Write(id);
                //Response.End();
                CheckBox cbxDelete = (CheckBox)gridRow.FindControl("cbxDelete");

                if (cbxDelete.Checked == true)
                {
                    bool sucess1 = ClstblWebDownload.tblWebDownload_Delete(id);
                    //Response.Write(sucess1);
                    //Response.End();
                }
            }
            //Response.End();
            BindGrid(0);
        }

        if (action.SelectedValue == "3")
        {
            int rowsCount = GridView1.Rows.Count;
            GridViewRow gridRow;

            for (int i = 0; i < rowsCount; i++)
            {
                string id;
                gridRow = GridView1.Rows[i];
                id = GridView1.DataKeys[i].Value.ToString();
                //Response.Write(id);
                //Response.End();
                CheckBox cbxDelete = (CheckBox)gridRow.FindControl("cbxDelete");

                if (cbxDelete.Checked == true)
                {
                    bool sucess1 = ClstblWebDownload.tblWebDownload_Delete_forAdmin(id);
                    //Response.Write(sucess1);
                    //Response.End();
                }
            }
            //Response.End();
            BindGrid(0);
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        ModalPopupExtender5.Show();
    }
}