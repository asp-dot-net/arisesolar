using System;
using System.Web.UI;
using System.Data;
using System.Linq;
using System.IO;
using ClosedXML.Excel;

public partial class admin_adminfiles_upload_LeadReport : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            BindDropDown();
        }
    }

    protected void BindDropDown()
    {
        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC_Include();
        ddlEmployee.DataMember = "EmployeeID";
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();

        ddlSource.DataSource = ClstblCustSourceSub.tblCustSourceSub_SelectActive();
        ddlSource.DataMember = "CustSourceSubID";
        ddlSource.DataTextField = "CustSourceSub";
        ddlSource.DataValueField = "CustSourceSubID";
        ddlSource.DataBind();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    protected DataTable GetGridData()
    {
        DataTable dt = ClsLead.SP_GetLead_ByEmployee(txtStartDate.Text, txtEndDate.Text, ddlTeam.SelectedValue, ddlEmployee.SelectedValue);

        return dt;
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                .Select(x => x.ColumnName)
                                .ToArray();

            string FileName = "LeadReport_" + "" + ".xls";

            int[] ColList = { 1, 17, 2, 3, 4, 18, 5, 6, 7, 19, 8, 9, 10, 20, 11, 12, 13, 21, 14, 15, 16 };

            string[] arrHeader = { "Employee Name", "Actual Lead", "Current Lead", "Sold", "Ratio", "Actual TV", "TV", "TV Sold", "TV Ratio", "Actual Online", "Online", "Online Sold", "Online Ratio", "Actual Facebook", "Facebook", "Facebook Sold", "Facebook Ratio", "Actual Others", "Others", "Others Sold", "Others Ratio" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
            MsgError("Error while downloading Excel..");
        }
    }

    public DataSet GetDataSet()
    {
        DataSet ds = ClsLead.SP_GetLead_ByEmployeeV2(ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlTeam.SelectedValue, ddlEmployee.SelectedValue);

        return ds;
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        DataSet ds = GetDataSet();

        try
        {
            if (ds != null)
            {
                //Set Name of DataTables.
                ds.Tables[0].TableName = "TV Lead";
                ds.Tables[1].TableName = "Online Lead";
                ds.Tables[2].TableName = "FB Lead";
                ds.Tables[3].TableName = "Others Lead";
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                foreach (DataTable dt in ds.Tables)
                {
                    int SumActual = Convert.ToInt32(dt.Compute("SUM(Actual)", string.Empty));
                    int SumSold = Convert.ToInt32(dt.Compute("SUM(Sold)", string.Empty));

                    string Per = Convert.ToDecimal((SumSold * 100) / SumActual).ToString("F");

                    DataRow row = dt.NewRow();
                    row[1] = "Total";
                    row[2] = SumActual;
                    row[3] = SumSold;
                    row[4] = Per;
                    dt.Rows.Add(row);

                    wb.Worksheets.Add(dt);
                }

                string FileName = "LeadReport_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

        }
        catch (Exception ex)
        {
            MsgError(ex.Message);
        }
    }

    protected void lbtnSearch_Click(object sender, EventArgs e)
    {
        DataSet ds = GetDataSet();

        if (ds != null)
        {
            //Set Name of DataTables.
            ds.Tables[0].TableName = "TV Lead";
            ds.Tables[1].TableName = "Online Lead";
            ds.Tables[2].TableName = "FB Lead";
            ds.Tables[3].TableName = "Others Lead";
        }

        //foreach (DataTable dt in ds.Tables)
        //{
        //    int SumActual = Convert.ToInt32(dt.Compute("SUM(Actual)", string.Empty));
        //    int SumSold = Convert.ToInt32(dt.Compute("SUM(Sold)", string.Empty));

        //    string Per = Convert.ToDecimal((SumSold * 100) / SumActual).ToString("F");

        //    DataRow row = dt.NewRow();
        //    row[1] = "Total";
        //    row[2] = SumActual;
        //    row[3] = SumSold;
        //    row[4] = Per;
        //    dt.Rows.Add(row);
        //}

        //TV Lead
        DataTable dtTVLead = ds.Tables[0];
        int TVActual = Convert.ToInt32(dtTVLead.Compute("SUM(Actual)", string.Empty));
        int TVSold = Convert.ToInt32(dtTVLead.Compute("SUM(Sold)", string.Empty));
        string TVPer = Convert.ToDecimal((TVSold * 100) / TVActual).ToString("F");

        lblTVActual.Text = TVActual.ToString();
        lblTVSold.Text = TVSold.ToString();
        lblTVPer.Text = TVPer.ToString();

        //Online Lead
        DataTable dtOLLead = ds.Tables[1];
        int OLActual = Convert.ToInt32(dtOLLead.Compute("SUM(Actual)", string.Empty));
        int OLSold = Convert.ToInt32(dtOLLead.Compute("SUM(Sold)", string.Empty));
        string OLPer = Convert.ToDecimal((OLSold * 100) / OLActual).ToString("F");

        lblOLActual.Text = OLActual.ToString();
        lblOLSold.Text = OLSold.ToString();
        lblOLPer.Text = OLPer.ToString();

        //FB Lead
        DataTable dtFBLead = ds.Tables[2];
        int FBActual = Convert.ToInt32(dtFBLead.Compute("SUM(Actual)", string.Empty));
        int FBSold = Convert.ToInt32(dtFBLead.Compute("SUM(Sold)", string.Empty));
        string FBPer = Convert.ToDecimal((FBSold * 100) / FBActual).ToString("F");

        lblFBActual.Text = FBActual.ToString();
        lblFBSold.Text = FBSold.ToString();
        lblFBPer.Text = FBPer.ToString();

        //Others Lead
        DataTable dtOLead = ds.Tables[3];
        int OActual = Convert.ToInt32(dtOLead.Compute("SUM(Actual)", string.Empty));
        int OSold = Convert.ToInt32(dtOLead.Compute("SUM(Sold)", string.Empty));
        string OPer = Convert.ToDecimal((OSold * 100) / OActual).ToString("F");

        lblOActual.Text = OActual.ToString();
        lblOSold.Text = OSold.ToString();
        lblOPer.Text = OPer.ToString();
    }
}