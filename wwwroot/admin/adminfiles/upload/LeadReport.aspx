<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="LeadReport.aspx.cs" Inherits="admin_adminfiles_upload_LeadReport" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=lbtnExport.ClientID %>').click(function () {
                formValidate();
            });
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        } p
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

                function (isConfirm) {
                    if (isConfirm) {
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        eval(defaultAction);

                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        return true;
                    } else {
                        // swal("Cancelled", "Your imaginary file is safe :)", "error");
                        return false;
                    }
                });
        }
    </script>

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Lead Report</h5>

    </div>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('#datetimepicker7').datetimepicker({
                format: 'DD/MM/YYYY',
                useCurrent: false //Important! See issue #1075
            }).keypress(function (event) {
                if (event.keyCode != 8) {
                    event.preventDefault();
                }
            });

            var today = new Date();
            //var dd = String(today.getDate()).padStart(2, '0');
            //var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            //var yyyy = today.getFullYear();

            //today = dd + '/' + mm + '/' + yyyy;
            today = '19' + '/' + '09' + '/' + '2020';
            //document.write(today);
            $('#datetimepicker7').data("DateTimePicker").minDate('19/09/2020');

        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {

        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });


            $("[data-toggle=tooltip]").tooltip();
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="Panel1" runat="server">
                                        <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div class="inlineblock martop5">
                                                        <div>

                                                            <div class="input-group col-sm-1" id="div1" runat="server">
                                                                <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Team</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-2" runat="server">
                                                                <asp:DropDownList ID="ddlEmployee" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">SalesRep</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1" runat="server">
                                                                <asp:DropDownList ID="ddlSource" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Source</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1" runat="server">
                                                                <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="1">Created Date</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group date  col-sm-1" id="datetimepicker7">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtStartDate" ErrorMessage="" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="input-group date datetimepicker1 col-sm-1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>

                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                    Display="Dynamic" ValidationGroup="Search"></asp:CompareValidator>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtEndDate" ErrorMessage="" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" class="form-control btn btn-success btn-xs Excel"
                                                                    CausesValidation="true" ValidationGroup="Search" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:LinkButton ID="lbtnExportV2" runat="server" data-toggle="tooltip" class="form-control btn btn-success btn-xs Excel"
                                                                    CausesValidation="true" ValidationGroup="Search" OnClick="lbtnExportV2_Click"><i class="fa fa-file-excel-o"></i> Excel V2 </asp:LinkButton>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="lbtnSearch" runat="server" data-toggle="tooltip" class="form-control btn btn-success btn-xs"
                                                                    CausesValidation="true" ValidationGroup="Search" OnClick="lbtnSearch_Click"><i class="fa fa-search"></i> Serach </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel" runat="server">
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="printorder" style="font-size: medium; margin-bottom:10px;">
                                <b>TV Lead:-&nbsp;&nbsp;</b>
                                <b>Actual:&nbsp;</b><asp:Literal ID="lblTVActual" runat="server"></asp:Literal>&nbsp;&nbsp;
                                 <b>Sold:&nbsp;</b><asp:Literal ID="lblTVSold" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Percentage:&nbsp;</b><asp:Literal ID="lblTVPer" runat="server"></asp:Literal>
                            </div>
                            <div class="printorder" style="font-size: medium; margin-bottom:10px;">
                                <b>Online Lead:-&nbsp;&nbsp;</b>
                                <b>Actual:&nbsp;</b><asp:Literal ID="lblOLActual" runat="server"></asp:Literal>&nbsp;&nbsp;
                                 <b>Sold:&nbsp;</b><asp:Literal ID="lblOLSold" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Percentage:&nbsp;</b><asp:Literal ID="lblOLPer" runat="server"></asp:Literal>
                            </div>
                            <div class="printorder" style="font-size: medium; margin-bottom:10px;">
                                <b>FB Lead:-&nbsp;&nbsp;</b>
                                <b>Actual:&nbsp;</b><asp:Literal ID="lblFBActual" runat="server"></asp:Literal>&nbsp;&nbsp;
                                 <b>Sold:&nbsp;</b><asp:Literal ID="lblFBSold" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Percentage:&nbsp;</b><asp:Literal ID="lblFBPer" runat="server"></asp:Literal>
                            </div>
                            <div class="printorder" style="font-size: medium; margin-bottom:10px;">
                                <b>Others Lead:&nbsp;&nbsp;</b>
                                <b>Actual:&nbsp;</b><asp:Literal ID="lblOActual" runat="server"></asp:Literal>&nbsp;&nbsp;
                                 <b>Sold:&nbsp;</b><asp:Literal ID="lblOSold" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Percentage:&nbsp;</b><asp:Literal ID="lblOPer" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="lbtnExportV2" />
        </Triggers>
    </asp:UpdatePanel>



    <style type="text/css">
        .selected_row {
            bac grou - lor: #A1DCF2 !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=.] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView2] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=DataList1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=DataList1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

</asp:Content>
