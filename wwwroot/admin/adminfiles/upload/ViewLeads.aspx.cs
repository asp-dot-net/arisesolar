﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_upload_ViewLeads : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            HidePanel();

            //custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
            ddlSearchState.DataMember = "State";
            ddlSearchState.DataTextField = "State";
            ddlSearchState.DataValueField = "State";
            ddlSearchState.DataBind();

            ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
            ddlTeam.DataMember = "SalesTeam";
            ddlTeam.DataTextField = "SalesTeam";
            ddlTeam.DataValueField = "SalesTeamID";
            ddlTeam.DataBind();

            ddlSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
            ddlSource.DataMember = "CustSource";
            ddlSource.DataTextField = "CustSource";
            ddlSource.DataValueField = "CustSource";
            ddlSource.DataBind();

            ddlsubsource.DataSource = ClstblCustSourceSub.tblCustSourceSub_SelectActive();
            ddlsubsource.DataMember = "CustSourceSub";
            ddlsubsource.DataTextField = "CustSourceSub";
            ddlsubsource.DataValueField = "CustSourceSub";
            ddlsubsource.DataBind();

            txtStartDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
            txtEndDate.Text = DateTime.Now.AddHours(14).ToShortDateString();

            ListItem item126 = new ListItem();
            item126.Text = "Emp Category";
            item126.Value = "";

            ddlEmpCategory.DataSource = ClstblEmployees.get_EmpCategory();
            ddlEmpCategory.DataMember = "Emp_Category_Name";
            ddlEmpCategory.DataTextField = "Emp_Category_Name";
            ddlEmpCategory.DataValueField = "Emp_Cat_Id";
            ddlEmpCategory.DataBind();

            //BindGrid(0);
        }
    }

    protected DataTable GetGridData()
    {

        //DataTable dt = ClstblCustomers.getLeadData(ddlDup.SelectedValue, ddlwebdup.SelectedValue, txtSearch.Text, ddlSource.SelectedValue, ddlsubsource.SelectedValue, ddlSearchState.SelectedValue, ddlTeam.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlDelete.SelectedValue, ddlUpload.SelectedValue, "1");

        DataTable dt = ClsLead.tblWebDownload_GetLeadData(txtStartDate.Text, txtEndDate.Text, ddlSource.SelectedValue, ddlsubsource.SelectedValue, ddlDup.SelectedValue, ddlDelete.SelectedValue, ddlSearchState.SelectedValue);

        CountLeads(dt);
        return dt;
    }
    public void CountLeads(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            lblTotalPanels.Text = dt.Rows.Count.ToString();
            //DataRow[] dr = dt.Select("subsource= 'OnLineLeads'");
            //if (dr.Length > 0)
            //{
            //    Literal1.Text = Convert.ToString(dr.ToString());
            //}
            //else
            //{
            //    Literal1.Text = "0.00";
            //}
            DataRow[] dr1 = dt.Select("subsource in ('Facebook', 'Facebook Finance')");
            if (dr1.Length > 0)
            {
                lblfb.Text = Convert.ToString(dr1.Length.ToString());
            }
            else
            {
                lblfb.Text = "0.00";
            }
            DataRow[] dr12 = dt.Select("subsource='OnLineLeads'");
            if (dr12.Length > 0)
            {
                Literal1.Text = Convert.ToString(dr12.Length.ToString());
            }
            else
            {
                Literal1.Text = "0.00";
            }
            DataRow[] drprjCount1 = dt.Select("subsource  IN ('Lead 1','Lead 2','Lead 3','Lead 4','Lead 5','Lead 6')");
            if (drprjCount1.Length > 0)
            {
                Literal2.Text = Convert.ToString(drprjCount1.Length.ToString());
            }
            else
            {
                Literal2.Text = "0.00";
            }
        }
        else
        {
            Literal1.Text = "0.00";
            Literal2.Text = "0.00";
            lblfb.Text = "0.00";
            lblTotalPanels.Text = "0.00";
        }
    }

    public void BindGrid(int deleteFlag)
    {
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {

            GridView1.DataSource = dt;
            GridView1.DataBind();
            // PanNoRecord.Visible = false;

            divnopage.Visible = true;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        divnopage.Visible = false;
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        hdnWebDownloadID.Value = id;
        MPECustomerName.Show();
        GetDuplicate();
    }

    public void GetDuplicate()
    {
        SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(hdnWebDownloadID.Value);

        DataTable dt = ClstblCustomers.tblCustomers_SelectByCheckExists(st.Customer, st.CustPhone, st.ContEmail, st.ContMobile);
        DataList1.DataSource = null;
        DataList1.DataSource = dt;
        DataList1.DataBind();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
        BingGridDropDown();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    public void HidePanel()
    {

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }


    protected void chkNotduplicate_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chkNotduplicate = (CheckBox)row.FindControl("chkNotduplicate");
            HiddenField hdnWebDownloadID2 = (HiddenField)row.FindControl("hdnWebDownloadID2");
            if (chkNotduplicate.Checked == true)
            {
                ClstblWebDownload.tblWebDownload_Update_NotDuplicate(hdnWebDownloadID2.Value, Convert.ToString(true));
            }
        }
        BindGrid(0);
    }

    protected void chkDuplicateRecord_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            CheckBox chkDuplicateRecord = (CheckBox)row.FindControl("chkDuplicateRecord");
            HiddenField hdnWebDownloadID1 = (HiddenField)row.FindControl("hdnWebDownloadID1");
            if (chkDuplicateRecord.Checked == true)
            {
                ClstblWebDownload.tblWebDownload_Update_Duplicate(hdnWebDownloadID1.Value, Convert.ToString(true));
            }
        }
        BindGrid(0);
    }



    public void BingGridDropDown()
    {
        //foreach (GridViewRow row in GridView1.Rows)
        {
            //if (row.RowType == DataControlRowType.DataRow)
            {
                //LinkButton btnduplicate = (LinkButton)row.FindControl("btnduplicate");
                //DropDownList ddlSalesRep = (DropDownList)row.FindControl("ddlSalesRep");

                //if (btnduplicate.Visible == true)
                //{
                //    ddlSalesRep.Visible = false;
                //}

            }
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //bool sucess1 = ClstblWebDownload.tblWebDownload_Delete(id);

        //if (sucess1)
        //{
        //    PanSuccess.Visible = true;
        //}
        //else
        //{
        //    PanError.Visible = true;
        //}
        ////--- do not chage this code end------
        ////--- do not chage this code start------
        //GridView1.EditIndex = -1;
        //BindGrid(0);
        //BindGrid(1);
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlTeam.SelectedValue = "";
        ddlSource.SelectedValue = "";
        ddlDup.SelectedValue = "0";

        BindGrid(0);
    }
    protected void DataList1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        MPECustomerName.Show();
        DataList1.PageIndex = e.NewPageIndex;
        GetDuplicate();
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;

        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;
                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;
        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;
        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            //Response.Write(dv.ToTable().Rows.Count);
            //Response.End();
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        }
        else
        {
            ltrPage.Text = "";
        }
        ///BindScript();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");

            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);

        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    lnkpagebtn.CssClass = "active";
            //}
        }
    }
    public void PageClick(String id)
    {
        // custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {

    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        //custompageIndex = custompageIndex - 1;
        //PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        //custompageIndex = custompageIndex + 1;
        //PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        ////Response.Write("-->" + lastpageindex); Response.End();
        //custompageIndex = lastpageindex;
        ////ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        //PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    protected void chkheader_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkheader = (CheckBox)GridView1.HeaderRow.FindControl("chkheader");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk1 = (CheckBox)row.FindControl("chk1");

                if (chkheader.Checked == true)
                {
                    chk1.Checked = true;
                }
                else
                {
                    chk1.Checked = false;
                }
            }
        }
    }

    protected void chksalesheader_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chksalesheader = (CheckBox)GridView1.HeaderRow.FindControl("chksalesheader");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chksales = (CheckBox)row.FindControl("chksales");

                if (chksalesheader.Checked == true)
                {
                    chksales.Checked = true;
                }
                else
                {
                    chksales.Checked = false;
                }
            }
        }
    }
    protected void btndelete_Click(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();
            //Response.Write(id+"</br>");

            CheckBox chkdelete = (CheckBox)gridRow.FindControl("chkdelete");

            if (chkdelete.Checked)
            {

                bool sucess1 = ClstblWebDownload.tblWebDownload_Delete(id);

            }
        }
        //Response.End();
        BindGrid(0);
    }
    protected void chkdeletewhole_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkdeletewhole = (CheckBox)GridView1.HeaderRow.FindControl("chkdeletewhole");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkdelete = (CheckBox)row.FindControl("chkdelete");

                if (chkdeletewhole.Checked == true)
                {
                    chkdelete.Checked = true;
                }
                else
                {
                    chkdelete.Checked = false;
                }
            }
        }
    }

    //protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    ModalPopupExtender1.Show();
    //    GridView2.PageIndex = e.NewPageIndex;
    //    GetDuplicate1();
    //}
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewDup")
        {
            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
            string id = e.CommandArgument.ToString();

            if (id != "")
            {
                SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(id);
                //DataTable dt = ClstblCustomers.GetDuplicateData(st.ContEmail, st.ContMobile);
                DataTable dt = ClsLead.tblWebDownload_GetDuplicateByEmailMobile(st.ContMobile, st.ContEmail);
                rptLead.DataSource = dt;
                rptLead.DataBind();

                if (dt.Rows.Count > 5)
                {
                    divpl.Attributes.Add("style", "overflow-y: scroll; height: " + 300 + "px !important;");
                }
                else
                {
                    divpl.Attributes.Add("style", "");
                }

                ModalPopupExtender1.Show();
            }

        }

        if (e.CommandName == "UploadedDup")
        {
            string id = e.CommandArgument.ToString();

            if (id != "")
            {
                SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(id);
                DataTable dt = ClsLead.tblCustomers_GetDuplicateByEmailMobile(st.ContMobile, st.ContEmail);
                RptUploadedLead.DataSource = dt;
                RptUploadedLead.DataBind();

                if (dt.Rows.Count > 5)
                {
                    DivUploadedLead.Attributes.Add("style", "overflow-y: scroll; height: " + 300 + "px !important;");
                }
                else
                {
                    DivUploadedLead.Attributes.Add("style", "");
                }

                ModalPopupExtender2.Show();
            }

        }

    }
    public void GetDuplicate1()
    {
        SttblWebDownload st = ClstblWebDownload.tblWebDownload_SelectbyWebDownloadID(hdnwebdup.Value);
        //Response.Write(st.Customer + "1=========" + st.CustPhone + "2=======" + st.ContEmail + "3=========" + st.ContMobile + "4=========" + st.Address);
        //Response.End();
        DataTable dt = ClstblCustomers.GetDuplicateData(st.ContEmail, st.ContMobile);
        //GridView2.DataSource = null;
        //GridView2.DataSource = dt;
        //GridView2.DataBind();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        // string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        //DataTable dt = GetGridData();

        DataTable dt = ClsLead.tblWebDownload_GetLeadDataFoeExcel(txtStartDate.Text, txtEndDate.Text, ddlSource.SelectedValue, ddlsubsource.SelectedValue, ddlDup.SelectedValue, ddlDelete.SelectedValue, ddlSearchState.SelectedValue);

        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "ViewLeads" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();

            int[] ColList = { 1, 2, 4, 3, 5, 6, 7, 8, 9, 10, 11, 17, 18, 19, 20 };
            string[] arrHeader = { "CompanyName", "Email", "Phone", "Mobile", "Address", "City", "State", "PostCode", "Source", "SubSource", "LeadDate", "Dup.Count", "EmployeeName", "Project Status", "LeadDate" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }

    }
}
