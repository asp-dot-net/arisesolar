﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_account_InvoicePaid : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            
            bindExcelUploadDate();
            BindDropDown();
            BindGrid(0);
        }
    }
    
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    protected void bindExcelUploadDate()
    {
        string LastUploadOn = ClsInvoicePaid.tbl_BankInvoicePayments_LastExcelUploadDate();
        lblUploadMsg.Visible = string.IsNullOrEmpty(LastUploadOn) ? false : true;
        lblUploadMsg.Text = "Last Excel Upload on " + LastUploadOn;
    }

    public void BindDropDown()
    {
        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlpayby.DataSource = ClsProjectSale.tblFPTransType_getInv();
        ddlpayby.DataValueField = "FPTransTypeID";
        ddlpayby.DataTextField = "FPTransTypeAB";
        ddlpayby.DataMember = "FPTransTypeAB";
        ddlpayby.DataBind();
    }

    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        string invoiceNo = txtInvSearch.Text.Trim();
        string customer = txtSearchCustomer.Text.Trim();
        string state = ddlSearchState.SelectedValue;
        string payBy = ddlpayby.SelectedValue;
        string financeWith = ddlfinancewith.SelectedValue;
        string dateType = ddlDate.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        dt = ClsInvoicePaid.Sp_InvoicePaid(invoiceNo, customer, state, payBy, financeWith, dateType, startDate, endDate);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        panel.Visible = true;
        DataTable dt = GetGridData();

        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            PanGrid.Visible = false;
            panel.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            bindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {

                    divnopage.Visible = true;

                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void bindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            string amountPaid = dt.Compute("SUM(amountPaid)", String.Empty).ToString();
            lblTotalAmount.Text = SiteConfiguration.ChangeCurrency(amountPaid);

            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "TotalQuotePrice", "InvoiceNumber");
            string TotalQuotePrice = distinctValues.Compute("SUM(TotalQuotePrice)", String.Empty).ToString();
            lblInvTotal.Text = SiteConfiguration.ChangeCurrency(TotalQuotePrice);
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bindExcelUploadDate();
        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {

        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
        try
        {
            string[] ColumnName = dt.Columns.Cast<DataColumn>()
                                            .Select(x => x.ColumnName)
                                            .ToArray();

            Export oExport = new Export();
            string FileName = "Invice Paid_" + System.DateTime.Now.AddHours(15).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 3, 4, 5, 6, 7, 8, 9, 10, 11 };

            string[] arrHeader = { "Customer", "Inv No.", "Date Paid", "Inv Total", "Amnt Paid", "SSCharge", "Paid By", "Receipt No", "Purchase No" };
            //only change file extension to .xls for excel file

            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtInvSearch.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        ddlpayby.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtSearchCustomer.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        ddlfinancewith.SelectedValue = "";

        bindExcelUploadDate();
        BindGrid(0);
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void btnAddBankPayment_Click(object sender, EventArgs e)
    {
        int id = 0;
        try
        {
            int suc = 0;
            string createdOn = DateTime.Now.AddHours(15).ToString();
            string createdBy = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

            DataTable dtBankDetails = new DataTable();
            dtBankDetails.Columns.Add("projectNo", typeof(int));
            dtBankDetails.Columns.Add("payDate", typeof(DateTime));
            dtBankDetails.Columns.Add("payBy", typeof(string));
            dtBankDetails.Columns.Add("amountPaid", typeof(decimal));
            dtBankDetails.Columns.Add("sCharge", typeof(string));
            dtBankDetails.Columns.Add("receiptNo", typeof(string));
            dtBankDetails.Columns.Add("purchaseNo", typeof(string));
            dtBankDetails.Columns.Add("allocatedBy", typeof(int));
            dtBankDetails.Columns.Add("Description", typeof(string));
            dtBankDetails.Columns.Add("invoiceNotes", typeof(string));

            if (fuBankPayment.HasFile)
            {
                id = ClsInvoicePaid.tbl_BankInvoicePaymentsExcelLog_Insert(fuBankPayment.FileName, createdOn, createdBy);

                string fileName = id.ToString() + "_" + fuBankPayment.FileName;
                if (id > 0)
                {
                    fuBankPayment.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\BankPayment\\" + fileName);
                }
                
                string connectionString = "";
                if (fuBankPayment.FileName.EndsWith(".xls"))
                {
                    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\BankPayment\\" + fileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
                }
                else if (fuBankPayment.FileName.EndsWith(".xlsx"))
                {
                    connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\BankPayment\\" + fileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
                }

                DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
                using(DbConnection connection = factory.CreateConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();

                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT * FROM [Sheet1$]";
                        command.CommandType = CommandType.Text;

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if(dr.HasRows)
                            {
                                while(dr.Read())
                                {
                                    DataRow dataRow = dtBankDetails.NewRow();
                                    if(!String.IsNullOrEmpty(dr["Project No"].ToString()))
                                    {
                                        dataRow["projectNo"] = dr["Project No"].ToString();
                                        dataRow["payDate"] = dr["Date"].ToString();
                                        dataRow["payBy"] = dr["Pay By"].ToString();
                                        dataRow["amountPaid"] = dr["Amount Paid"].ToString();
                                        dataRow["sCharge"] = dr["SSCharge"].ToString();
                                        dataRow["receiptNo"] = dr["Receipt Number"].ToString();
                                        dataRow["purchaseNo"] = dr["Purchase Number"].ToString();
                                        dataRow["allocatedBy"] = dr["Allocated by"].ToString();
                                        dataRow["Description"] = dr["Description"].ToString();
                                        dataRow["invoiceNotes"] = dr["Invoice notes description"].ToString();
                                        dtBankDetails.Rows.Add(dataRow);
                                    }
                                }
                            }
                        }
                    }
                }

                suc = ClsInvoicePaid.tbl_BankInvoicePayments_Insert(dtBankDetails, createdOn, createdBy);
                if (suc > 0)
                {
                    SetAdd1();
                    BindGrid(0);
                    bindExcelUploadDate();
                }
                else
                {
                    SetError1();
                    SiteConfiguration.deleteimage(fuBankPayment.FileName, "BankPayment");
                    bool delExcelLog = ClsInvoicePaid.tbl_BankInvoicePaymentsExcelLog_Delete(id.ToString());
                }
            }
        }
        catch(Exception ex)
        {
            SetError1();
            SiteConfiguration.deleteimage(fuBankPayment.FileName, "BankPayment");
            bool delExcelLog = ClsInvoicePaid.tbl_BankInvoicePaymentsExcelLog_Delete(id.ToString());
        }
        //SiteConfiguration.deleteimage(fuBankPayment.FileName, "BankPayment");
    }
}