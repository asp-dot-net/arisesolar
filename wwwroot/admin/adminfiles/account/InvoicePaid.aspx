﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvoicePaid.aspx.cs" Inherits="admin_adminfiles_account_InvoicePaid"
    Culture="en-GB" UICulture="en-GB" MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>
    <style>
        .nowrap {
            white-space: normal !important;
        }

        .tooltip {
            z-index: 999999;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                    $('.loading-container').css('display', 'none');

                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });


                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });

                }
                function pageLoaded() {
                    //alert($(".search-select").attr("class"));
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    //alert($(".search-select").attr("class"));

                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true
                    });
                    $(".myvalinvoicepaid").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }
                    //gridviewScroll();

                    //  callMultiCheckbox();
                }
            </script>
            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Invoices Paid</h5>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="content animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>

                    <div class="searchfinal">
                        <asp:UpdatePanel ID="updatepanel2" runat="server" UpdateMode="Conditional" Visible='<%# Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager") ? true : false %>'>
                            <ContentTemplate>
                                <asp:Panel runat="server" ID="Panel3">
                                    <div class="topfileuploadbox marbtm15">
                                        <div class="widget-body shadownone brdrgray">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group" id="filediv">
                                                        <div>
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Bank Payment: Upload Excel File <span class="symbol required"></span>
                                                                </label>
                                                                <span class="">
                                                                    <asp:FileUpload ID="fuBankPayment" runat="server" Style="display: inline-block;" class="fileupdate" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="fuBankPayment"
                                                                        ValidationGroup="reqBankPayment" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                                        Display="Dynamic" ErrorMessage=".xls, xlsx only"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                        ControlToValidate="fuBankPayment" Display="Dynamic" ValidationGroup="reqBankPayment"></asp:RequiredFieldValidator>
                                                                </span>
                                                                &nbsp;
                                                            &nbsp; 
                                                            <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnAddBankPayment" runat="server" Text="Add"
                                                                OnClick="btnAddBankPayment_Click" ValidationGroup="reqBankPayment" Style="padding-left: 24px;" />

                                                                &nbsp;&nbsp;<asp:Label ID="lblUploadMsg" runat="server" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAddBankPayment" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <div class="dataTables_filter Responsive-search row">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="inlineblock">
                                                            <div class="col-sm-12">
                                                                <div class="input-group col-sm-1">
                                                                    <asp:TextBox ID="txtInvSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtInvSearch"
                                                                        WatermarkText="Invoice" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtInvSearch" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetInvoiceNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtInvSearch" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                </div>

                                                                <div class="input-group col-sm-1">
                                                                    <asp:TextBox ID="txtSearchCustomer" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchCustomer"
                                                                        WatermarkText="Customer" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtSearchCustomer" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                </div>
                                                                <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                                    <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid" Width="100%">
                                                                        <asp:ListItem Value="">State</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-1" id="div1" runat="server">
                                                                    <asp:DropDownList ID="ddlpayby" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                        <asp:ListItem Value="">Pay By</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <%--<div class="input-group col-sm-1" id="div3" runat="server">
                                                                    <asp:DropDownList ID="ddlIsVerified" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                        <asp:ListItem Value="2">Verified</asp:ListItem>
                                                                        <asp:ListItem Value="1">Only Verified</asp:ListItem>
                                                                        <asp:ListItem Value="0" Selected="True">Not Verified</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-1" id="div2" runat="server">
                                                                    <asp:DropDownList ID="ddlVerifiedBy" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                        <asp:ListItem Value="">Verified By</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>--%>

                                                                <div class="input-group col-sm-1" id="div5" runat="server">
                                                                    <asp:DropDownList ID="ddlfinancewith" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                        <asp:ListItem Value="">Finance Co.</asp:ListItem>
                                                                        <asp:ListItem Value="1">Cash</asp:ListItem>
                                                                        <asp:ListItem Value="2">Finance</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-1" id="div4" runat="server">
                                                                    <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                        <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                        <asp:ListItem Value="1">Bank Date</asp:ListItem>
                                                                        <asp:ListItem Value="2">Install Booking Date</asp:ListItem>
                                                                        <asp:ListItem Value="3">Upload Date</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group date datetimepicker1 col-sm-1">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtStartDate"
                                                                        WatermarkText="Start Date" />
                                                                </div>

                                                                <div class="input-group date datetimepicker1 col-sm-1">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control m-b"></asp:TextBox>
                                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                    <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                </div>

                                                                <div class="input-group">
                                                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                                <div class="input-group">
                                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanSearch" runat="server">
                                    <div class="row">
                                        <div class="dataTables_length showdata col-sm-6">
                                            <td class="padtopzero">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>

                                            <td class="padtopzero">
                                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            </td>
                                        </div>
                                        <div class="col-md-6">
                                            <%--<div id="Div8" class="pull-right" runat="server" style="padding-top:8px">
                                        <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                            CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                    </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="panel" runat="server" CssClass="hpanel marbtm15">
                        <div class="widget-body shadownone brdrgray">
                            <div class="panel-body padallzero">
                                <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="paddingleftright10" style="font-size: medium">
                                                        <b>Inv Total:&nbsp;</b><asp:Literal ID="lblInvTotal" runat="server"></asp:Literal>
                                                    </td>
                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                    <td class="paddingleftright10" style="font-size: medium">
                                                        <b>Total Amount Paid:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </asp:Panel>

                    <asp:Panel ID="panel1" runat="server" CssClass="hpanel padtopzero finalgrid">
                        <div id="PanGrid" runat="server">
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
                                    OnRowCreated="GridView1_RowCreated1" AllowSorting="true" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="20px">
                                            <ItemTemplate>

                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("id") %>','tr<%# Eval("id") %>');">
                                                    <img id='imgdiv<%# Eval("id") %>' src="../../../images/icon_plus.png" />
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdspecialclass spiclwithnew"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                            <ItemTemplate>
                                                <asp:Label ID="Label" runat="server" Width="175px" Style="display: initial">
                                                    <asp:HyperLink ID="lnkCustomer" CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/CustomerNew.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%></asp:HyperLink>

                                                    <div class="contacticonedit">
                                                        <asp:HyperLink ID="HyperLink11" runat="server" CssClass="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Project" Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                                       <i class="fa fa-link"></i> Detail
                                                        </asp:HyperLink>
                                                    </div>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Inv No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InvoiceNumber">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Width="60px">
                                                        <%#Eval("InvoiceNumber")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-HorizontalAlign="left" SortExpression="payDate">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Width="80px">
                                                        <%#Eval("payDate")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Inv Total" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" SortExpression="TotalQuotePrice">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Width="80px">
                                                    <asp:HiddenField ID="hndPrice" runat="server" Value='<%#Eval("TotalQuotePrice")%>' />
                                                    <%#Eval("TotalQuotePrice","{0:0.00}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Amnt Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" SortExpression="amountPaid">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Width="100px">
                                                    <%#Eval("amountPaid","{0:0.00}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="SS Charge" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" SortExpression="sCharge">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsCharge" runat="server" Width="100px">
                                                    <%#Eval("sCharge","{0:0.00}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Paid By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" SortExpression="FPTransType">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Width="100px">
                                                        <%#Eval("FPTransType")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Receipt No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" SortExpression="receiptNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblreceiptNo" runat="server" Width="100px">
                                                        <%#Eval("receiptNo")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Purchase No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" SortExpression="purchaseNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpurchaseNo" runat="server" Width="100px">
                                                        <%#Eval("purchaseNo")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Created On" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" SortExpression="createdOn">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedOn" runat="server">
                                                        <%#Eval("createdOn")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%--<asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderText=""
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="Label10" runat="server" Width="80px">

                                                    <asp:LinkButton ID="lbtnRevert" CommandName="Revert" Visible='<%#Eval("IsVerified").ToString()=="True"?true:false %>' CssClass="btn btn-maroon btn-xs"
                                                        CommandArgument='<%#Eval("Id") %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Revert">
                                                          <i class="fa fa-retweet"></i> Revert </asp:LinkButton>

                                                    <asp:HiddenField ID="hndID" runat="server" Value='<%#Eval("InvoicePaymentID")%>' />
                                                    <asp:LinkButton ID="lbtnVerified" CommandName="Comment" Visible='<%#Eval("IsVerified").ToString()=="False"?true:false %>' CssClass="btn btn-sky btn-xs"
                                                        CommandArgument='<%#Eval("Id") %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Verify">
                                                           <i class="fa fa-check-square"></i> Verify</asp:LinkButton>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                            <ItemTemplate>
                                                <tr id='tr<%# Eval("id") %>' style="display: none;" class="dataTable GridviewScrollItem">
                                                    <td colspan="98%" class="details">
                                                        <div id='div<%# Eval("id") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                            <%--<table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                <tr class="GridviewScrollItem">
                                                                    <td width="180px"><b>Verified</b></td>
                                                                    <td>
                                                                        <asp:Label ID="Label7" runat="server" Width="100px">
                                                        <%#Eval("Verified").ToString() == "" ? "-" : Eval("Verified","{0:dd MMM yyyy}")%></asp:Label>
                                                                    </td>
                                                                    <td width="180px">
                                                                        <b>Verified By</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label8" runat="server" Width="100px">
                                                        <%#Eval("VerifiedByName").ToString() == "" ? "-" : Eval("VerifiedByName")%></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <b>Notes</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label9" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("PaymentNote")%>' CssClass="tooltipwidth"
                                                                            Width="250px"><%#Eval("PaymentNote")%></asp:Label>
                                                                    </td>
                                                                </tr>

                                                            </table>--%>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>


                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </div>



            <script type="text/javascript">

                $(".dropdown dt a").on('click', function () {
                    $(".dropdown dd ul").slideToggle('fast');

                });

                $(".dropdown dd ul li a").on('click', function () {
                    $(".dropdown dd ul").hide();
                });
                $(document).bind('click', function (e) {
                    var $clicked = $(e.target);
                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                });


                $(document).ready(function () {
                    HighlightControlToValidate();
                });

                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
                function HighlightControlToValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            $('#' + Page_Validators[i].controltovalidate).blur(function () {
                                var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                                if (validatorctrl != null && !validatorctrl.isvalid) {
                                    $(this).css("border-color", "#FF5F5F");
                                }
                                else {
                                    $(this).css("border-color", "#B5B5B5");
                                }
                            });
                        }
                    }
                }
                function getValidatorUsingControl(controltovalidate) {
                    var length = Page_Validators.length;
                    for (var j = 0; j < length; j++) {
                        if (Page_Validators[j].controltovalidate == controltovalidate) {
                            return Page_Validators[j];
                        }
                    }
                    return null;
                }
            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                    // gridviewScroll();
                });
                $("#nav").on("click", "a", function () {
                    $('#content').animate({ opacity: 0 }, 500, function () {
                        //gridviewScroll();
                        $('#content').delay(250).animate({ opacity: 1 }, 500);
                    });
                });
                function gridviewScroll() {
                    $('#<%=GridView1.ClientID%>').gridviewScroll({
                        width: $("#content").width() - 40,
                        height: 6000,
                        freezesize: 0
                    });
                }
            </script>
            <asp:HiddenField ID="hndProInvID" runat="server" />
            <asp:HiddenField runat="server" ID="hdncountdata" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

