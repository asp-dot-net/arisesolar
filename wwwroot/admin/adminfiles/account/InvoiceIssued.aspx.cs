﻿using ClosedXML.Excel;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_account_InvoiceIssued : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            
            //txtInvoiceNo.Text = "821831";
            BindDropDown();
            BindGrid(0);
        }
    }
    
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    
    public void BindDropDown()
    {
        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlfinancewith.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlfinancewith.DataMember = "FinanceWith";
        ddlfinancewith.DataTextField = "FinanceWith";
        ddlfinancewith.DataValueField = "FinanceWithID";
        ddlfinancewith.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectByActive();
        lstSearchStatus.DataBind();

        ddlPaidStatus.DataSource = ClsProjectSale.tblInvoiceStatus_Select();
        ddlPaidStatus.DataValueField = "Status";
        ddlPaidStatus.DataMember = "InvoiceStatus";
        ddlPaidStatus.DataTextField = "InvoiceStatus";
        ddlPaidStatus.DataBind();

        try
        {
            ddlPaidStatus.SelectedValue = "3";
        }
        catch (Exception ex)
        { }

        ddlpayby.DataSource = ClsProjectSale.tblFPTransType_getInv();
        ddlpayby.DataValueField = "FPTransTypeID";
        ddlpayby.DataTextField = "FPTransTypeAB";
        ddlpayby.DataMember = "FPTransTypeAB";
        ddlpayby.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_GetInstallers();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataBind();
    }
    
    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        string invoiceNo = txtInvoiceNo.Text.Trim();
        string contact = txtContactSearch.Text.Trim();
        string suburb = txtsuburb.Text.Trim();
        string state = ddlSearchState.SelectedValue;
        string paidStatus = ddlPaidStatus.SelectedValue;
        string projectStatus = getProjectStatus();
        string financeWith = ddlfinancewith.SelectedValue;
        string payBy = ddlpayby.SelectedValue;
        string area = ddlArea.SelectedValue;
        string dateType = ddlSelectDate.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;
        string postCodeFrom = txtpcodefrom.Text;
        string postCodeTo = txtpcodeto.Text;
        string installer = ddlInstaller.SelectedValue;

        dt = ClsInvoiceIssued.Sp_InvoiceIssuedNew(invoiceNo, contact, suburb, state, paidStatus, projectStatus, financeWith, payBy, area, dateType, startDate, endDate, postCodeFrom, postCodeTo, installer);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        panel.Visible = true;
        DataTable dt = GetGridData();

        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            PanGrid.Visible = false;
            panel.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            bindTotal(dt);
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {

                    divnopage.Visible = true;

                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
    }

    protected void bindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            string TotalAmount = dt.Compute("SUM(TotalQuotePrice)", String.Empty).ToString();
            lblTotalAmount.Text = SiteConfiguration.ChangeCurrency(TotalAmount);

            string TotalDepRec = dt.Compute("SUM(InvoicePayTotal)", String.Empty).ToString();
            lblTotalDepRec.Text = SiteConfiguration.ChangeCurrency(TotalDepRec);

            string BalOwing = dt.Compute("SUM(BalOwing)", String.Empty).ToString();
            lblBalOwing.Text = SiteConfiguration.ChangeCurrency(BalOwing);

        }
        else
        {
            lblTotalAmount.Text = "0";
            lblTotalDepRec.Text = "0";
            lblBalOwing.Text = "0";
            lblTotalPanels.Text = "0";
        }
    }

    protected string getProjectStatus()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        return selectedItem;
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {
            hndProInvID.Value = e.CommandArgument.ToString();

            InvoicePayments1.Visible = true;
            InvoicePayments1.GetInvPayClickByProject(hndProInvID.Value);
        }
    }
    
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
        try
        {
            string[] ColumnName = dt.Columns.Cast<DataColumn>()
                                            .Select(x => x.ColumnName)
                                            .ToArray();

            Export oExport = new Export();
            string FileName = "Invice Issued" + System.DateTime.Now.AddHours(15).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            string[] arrHeader = { "Inv #", "State", "Finance", "Proj Status", "Contact", "Mobile", "Installer", "Installed"
                                , "Total", "Owing", "Date Paid", "Panels", "Dep Req", "Dep Rec", "Man Qte", "O/Due", "Email"
                                , "Invoice Notes", "New Invoice Notes" };
            //only change file extension to .xls for excel file
            
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtInvoiceNo.Text = string.Empty;
        
        ddlPaidStatus.SelectedValue = "";
        txtsuburb.Text = string.Empty;
        txtContactSearch.Text = string.Empty;

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        ddlfinancewith.SelectedValue = "";
        ddlpayby.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        ddlSelectDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtpcodefrom.Text = string.Empty;
        txtpcodeto.Text = string.Empty;
        ddlInstaller.SelectedValue = "";

        BindGrid(0);
    }
    
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }


    }
    
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.Pager)
        {
            Label lblcurrentpage = (Label)e.Row.FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)e.Row.FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)e.Row.FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)e.Row.FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)e.Row.FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void lbtnExportV2_Click(object sender, EventArgs e)
    {
        DataTable dtDepositReceived = getdtDepositReceived();
        
        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtDepositReceived, "Deposit Received");

                string FileName = "Deposit Received " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            SetError1();
        }
    }

    protected DataTable getdtDepositReceived()
    {
        DataTable returnDt = new DataTable();

        string invoiceNo = txtInvoiceNo.Text.Trim();
        string contact = txtContactSearch.Text.Trim();
        string suburb = txtsuburb.Text.Trim();
        string state = ddlSearchState.SelectedValue;
        string paidStatus = ddlPaidStatus.SelectedValue;
        string projectStatus = getProjectStatus();
        string financeWith = ddlfinancewith.SelectedValue;
        string payBy = ddlpayby.SelectedValue;
        string area = ddlArea.SelectedValue;
        string dateType = ddlSelectDate.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        returnDt = ClsInvoiceIssued.Sp_InvoiceIssuedNew_DepositReceived(invoiceNo, contact, suburb, state, paidStatus, projectStatus, financeWith, payBy, area, dateType, startDate, endDate);

        return returnDt;
    }

    protected DataTable getInvoiceIssued()
    {
        DataTable returnDt = new DataTable();

        string invoiceNo = txtInvoiceNo.Text.Trim();
        string contact = txtContactSearch.Text.Trim();
        string suburb = txtsuburb.Text.Trim();
        string state = ddlSearchState.SelectedValue;
        string paidStatus = ddlPaidStatus.SelectedValue;
        string projectStatus = getProjectStatus();
        string financeWith = ddlfinancewith.SelectedValue;
        string payBy = ddlpayby.SelectedValue;
        string area = ddlArea.SelectedValue;
        string dateType = ddlSelectDate.SelectedValue;
        string startDate = txtStartDate.Text;
        string endDate = txtEndDate.Text;

        returnDt = ClsInvoiceIssued.Sp_InvoiceIssuedNew_InoviceIssued(invoiceNo, contact, suburb, state, paidStatus, projectStatus, financeWith, payBy, area, dateType, startDate, endDate);

        return returnDt;
    }

    protected void lbtnExportInstall_Click(object sender, EventArgs e)
    {
        DataTable dtInvoiceIssued = getInvoiceIssued();

        try
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtInvoiceIssued, "Invoice Issued");

                string FileName = "Invoice Issue " + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xlsx";

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + "");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            SetError1();
        }
    }
}