<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Culture="en-GB" UICulture="en-GB" CodeFile="project.aspx.cs" Inherits="admin_adminfiles_view_project" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .tooltip{ min-width:200px;}
        .braktext{white-space: normal!important;}
    </style>
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none"; se
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
                //  divexpandcollapse("", "");
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {


            function doMyAction() {
                alert('1');
                $('#<%=ibtnAdd.ClientID %>').click(function (e) {
                    formValidate();

                });
            }


        });


        function doMyAction1() {

            HighlightControlToValidate();
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {

                callMultiCheckbox();
            });
        }
        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <%--<asp:Literal runat="server" ID="lt"></asp:Literal>--%>



    <%--        <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                function BeginRequestHandler(sender, args) {
                    $('.splash').css('display', 'block');
                }
                function EndRequestHandler(sender, args) {
                    //hide the modal popup - the update progress
                    //$('.splash').css('display', 'none');
                }
                prm.add_pageLoaded(pageLoaded);
                prm.add_beginRequest(BeginRequestHandler);
                prm.add_endRequest(EndRequestHandler);
                function pageLoaded() {

                    $('.splash').css('display', 'none');
                }

            </script>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        function pageLoaded() {
            $(".myvalproject").select2({
                //placeholder: "select",
                allowclear: true
            });

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            //  $('.i-checks').iCheck({
            //                        checkboxClass: 'icheckbox_square-green',
            //                        radioClass: 'iradio_square-green'
            //                    });


        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Projects</h5>

    </div>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
              </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="btnClearAll" />
            <%-- <asp:PostBackTrigger ControlID="GridView1" />--%>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="content animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <div class="row">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="inlineblock">
                                                            <div class="col-sm-12">
                                                                <div class="input-group col-sm-1 martop5" style="width: 224px">
                                                                    <asp:TextBox ID="txtClient" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtClient"
                                                                        WatermarkText="Client" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtClient" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                </div>

                                                                <div class="input-group col-sm-1 martop5">
                                                                    <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project Num"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtProjectNumber"
                                                                        WatermarkText="Project Num" />
                                                                    <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage=""
                                                                        ValidationExpression="^\d+$,"></asp:RegularExpressionValidator>--%>
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                                    <cc1:FilteredTextBoxExtender runat="server" FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtProjectNumber"></cc1:FilteredTextBoxExtender>

                                                                </div>

                                                                <div class="input-group col-sm-1 martop5">
                                                                    <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                                        WatermarkText="Manual Num" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                </div>

                                                                <div class="input-group col-sm-1 martop5">
                                                                    <asp:TextBox ID="txtSearchStreet" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStreet"
                                                                        WatermarkText="Street" />
                                                                </div>

                                                                <div class="input-group col-sm-1 martop5">
                                                                    <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                                        WatermarkText="City" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                    </cc1:AutoCompleteExtender>
                                                                </div>

                                                                <div class="input-group martop5" id="divCustomer" runat="server" style="width: 150px">
                                                                    <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject" Width="150px">
                                                                        <asp:ListItem Value="">State</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group col-sm-1 martop5">
                                                                    <div class=" groupalign ">
                                                                        <asp:TextBox ID="TextBox2" runat="server"
                                                                            Width="100%" CssClass="form-control" Text="PostCode:" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <%-- <div class="input-group wid100">
                                                                            <label>PostCode:</label>
                                                                        </div>--%>
                                                                <div class="input-group dislpyinlinediv martop5" style="width: 195px">

                                                                    <asp:TextBox ID="txtSearchPostCodeFrom" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtSearchPostCodeFrom"
                                                                        WatermarkText="From" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtSearchPostCodeFrom" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                    </cc1:AutoCompleteExtender>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtSearchPostCodeFrom" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                </div>

                                                                <div class="input-group dislpyinlinediv martop5" style="width: 190px">
                                                                    <asp:TextBox ID="txtSearchPostCodeTo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender14" runat="server" TargetControlID="txtSearchPostCodeTo"
                                                                        WatermarkText="To" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtSearchPostCodeTo" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                    </cc1:AutoCompleteExtender>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtSearchPostCodeTo" Display="Dynamic" ErrorMessage="Enter valid value"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                </div>


                                                            

                                                                <div class="input-group col-sm-1 martop5" id="Div1" runat="server" visible="false">
                                                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtSearch"
                                                                        WatermarkText="Project" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender8" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                </div>

                                                                <div class="form-group spical multiselect martop5" style="width: 142px">
                                                                    <dl class="dropdown">
                                                                        <dt>
                                                                            <a href="#">
                                                                                <span class="hida" id="spanselect">Select</span>
                                                                                <p class="multiSel"></p>
                                                                            </a>
                                                                        </dt>
                                                                        <dd id="ddproject" runat="server">
                                                                            <div class="mutliSelect" id="mutliSelect">
                                                                                <ul>
                                                                                    <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <li>
                                                                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                    <span></span>
                                                                                                </label>
                                                                                                <%-- </span>--%>
                                                                                                <label class="chkval">
                                                                                                    <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                                </label>
                                                                                            </li>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </ul>
                                                                            </div>
                                                                        </dd>
                                                                    </dl>
                                                                </div>

                                                                <div class="input-group col-sm-2 martop5" style="width: 140px">
                                                                    <asp:DropDownList ID="ddlSearchSource" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlSearchSource_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject" AutoPostBack="true">
                                                                        <asp:ListItem Value="">Source</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-2 martop5">
                                                                    <asp:DropDownList ID="ddlSerchSub" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">SubSource</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-2 martop5">
                                                                    <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">Select Team</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>



                                                                <div class="input-group col-sm-2 martop5" id="tdEmployee" runat="server">
                                                                    <asp:DropDownList ID="ddlSearchEmployee" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">Employee</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" id="div4" runat="server" style="width: 175px">
                                                                    <asp:DropDownList ID="ddlProjectTypeID" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="" Text="Project Type"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group martop5" id="div5" runat="server" style="width: 175px">
                                                                    <asp:DropDownList ID="ddlSearchCancel" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="" Text="Cancelled Status"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group col-sm-1 martop5" id="Div6" runat="server" visible="false" style="width: 106px">
                                                                    <asp:TextBox ID="txtCompanyNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtCompanyNo"
                                                                        WatermarkText="CompanyNumber" />
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtCompanyNo" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                </div>


                                                         

                                                                <div class="input-group col-sm-2 martop5">
                                                                    <asp:TextBox ID="txtPModel" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtPModel"
                                                                        WatermarkText="Panel Model" />
                                                                </div>

                                                                <div class="input-group col-sm-2 martop5">
                                                                    <asp:TextBox ID="txtIModel" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtIModel"
                                                                        WatermarkText="Inverter Model" />
                                                                </div>
                                                                <div class="input-group col-sm-1 martop5">
                                                                    <div class=" groupalign ">
                                                                        <asp:TextBox ID="TextBox1" runat="server"
                                                                            Width="100%" ValidationGroup="searchcontact" CssClass="form-control" Text="System Capacity" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="input-group col-sm-1 martop5">
                                                                    <asp:TextBox ID="txtSysCapacityFrom" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtSysCapacityFrom"
                                                                        WatermarkText="From" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtSysCapacityFrom" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d*\.?\d+$">
                                                                    </asp:RegularExpressionValidator>
                                                                </div>

                                                                <div class="input-group col-sm-1 martop5">
                                                                    <asp:TextBox ID="txtSysCapacityTo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender15" runat="server" TargetControlID="txtSysCapacityTo"
                                                                        WatermarkText="To" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="search"
                                                                        ControlToValidate="txtSysCapacityTo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d*\.?\d+$">
                                                                    </asp:RegularExpressionValidator>
                                                                </div>

                                                                <div class="input-group martop5" id="divpromo1" runat="server" style="width: 100px" visible="false">
                                                                    <asp:DropDownList ID="ddlPromoOffer1" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">PromoOffer1</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>


                                                                <div class="input-group col-sm-1 martop5" id="divpromo2" runat="server">
                                                                    <asp:DropDownList ID="ddlPromoOffer2" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">House Status</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group martop5" id="Div9" runat="server" style="width: 180px">
                                                                    <asp:DropDownList ID="ddlFinanceOption" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">FinanceOption</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group martop5" id="Div7" runat="server" style="width: 180px">
                                                                    <asp:DropDownList ID="ddlRoofType" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">RoofType</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group martop5" id="Div8" runat="server" style="width: 180px">
                                                                    <asp:DropDownList ID="ddlHouseType" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">HouseType</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group martop5" id="Div10" runat="server" style="width: 180px">
                                                                    <asp:DropDownList ID="ddlDistApprovedDate" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">Dist Approved Date</asp:ListItem>
                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                          

                                                                <div class="input-group martop5" id="Div11" runat="server">
                                                                    <asp:DropDownList ID="ddlInstallBookingDate" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">InstallBooking Date</asp:ListItem>
                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" id="Div2" runat="server">
                                                                    <asp:DropDownList ID="ddlmeterupgrade" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">Meter Upgrade</asp:ListItem>
                                                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                        <asp:ListItem Value="No">No</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group martop5" id="Div3" runat="server">
                                                                    <asp:DropDownList ID="ddlreadyactive" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="">Ready To Active</asp:ListItem>
                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>



                                                                <div class="input-group col-sm-1 martop5" style="width: 165px">
                                                                    <asp:TextBox ID="txtElecDistApprovelRef" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender16" runat="server" TargetControlID="txtElecDistApprovelRef"
                                                                        WatermarkText="ElecDistApprovelRef" />
                                                                </div>

                                                                <div class="input-group col-sm-1 martop5" style="width: 170px">
                                                                    <asp:TextBox ID="txtPurchaseNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender17" runat="server" TargetControlID="txtPurchaseNo"
                                                                        WatermarkText="PurchaseNo" />
                                                                </div>

                                                                <div class="input-group martop5" id="Div12" runat="server" style="width: 194px">
                                                                    <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                        <asp:ListItem Value="1" Selected="True">Project Opened</asp:ListItem>
                                                                        <asp:ListItem Value="2">First Deposite Date</asp:ListItem>
                                                                        <asp:ListItem Value="3">First Quote Date</asp:ListItem>
                                                                        <asp:ListItem Value="4">Quote Accepted</asp:ListItem>
                                                                        <asp:ListItem Value="5">Dep. Received</asp:ListItem>
                                                                        <asp:ListItem Value="6">Install Date</asp:ListItem>
                                                                        <asp:ListItem Value="7">Active Date</asp:ListItem>
                                                                        <asp:ListItem Value="8">FollowUp Date</asp:ListItem>
                                                                        <asp:ListItem Value="9">Install Comp</asp:ListItem>
                                                                        <asp:ListItem Value="10">Pickup List</asp:ListItem>
                                                                        <asp:ListItem Value="11">Stock Pickup</asp:ListItem>
                                                                         <asp:ListItem Value="12">Project Cancel Date</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 180px">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                </div>
                                                                <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 180px">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                                </div>

                                                                <div class="input-group martop5">
                                                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                                <div class="form-group martop5">
                                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="showdatabox martop5">
                                <div class="row">
                                    <div class="dataTables_length showdata col-sm-6">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="tdExport" class="pull-right" runat="server">
                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            <%-- </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div>
                                        <div>
                                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="row">

                                                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                                                        CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divFollowUp"
                                                        OkControlID="btnOK" TargetControlID="btnNULL">
                                                    </cc1:ModalPopupExtender>
                                                    <div runat="server" id="divFollowUp" style="display: none;" class="modal_popup">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="color-line"></div>
                                                                <div class="modal-header">
                                                                    <div style="float: right">
                                                                        <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                                                            Close
                                                                        </button>
                                                                    </div>

                                                                    <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                                                                        <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                                        <b>Add FollowUp</b></h4>
                                                                </div>
                                                                <div class="modal-body paddnone">
                                                                    <div>
                                                                        <div class="formainline ">
                                                                            <div class="row">
                                                                                <div class="form-group col-md-4">
                                                                                    <div class="row">
                                                                                        <asp:Label ID="Label14" runat="server" class="col-sm-12 control-label">
                                                <strong>Contact</strong></asp:Label>
                                                                                        <div class="col-sm-12">
                                                                                            <asp:DropDownList ID="ddlContact" runat="server" AppendDataBoundItems="true"
                                                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Eurosolar</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Door to Door</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                                                ControlToValidate="ddlContact" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group col-md-4">
                                                                                    <div class="row">
                                                                                        <asp:Label ID="Label3" runat="server" class="col-sm-12 control-label">
                                                <strong>Select</strong></asp:Label>
                                                                                        <div class="col-sm-12">
                                                                                            <asp:DropDownList ID="ddlManager" runat="server" OnSelectedIndexChanged="ddlManager_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true"
                                                                                                CssClass="myvalproject">
                                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Follow Up</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Manager</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass=""
                                                                                                ControlToValidate="ddlManager" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group col-md-4" id="divNextDate" runat="server" visible="false">
                                                                                    <div class="row">
                                                                                        <asp:Label ID="Label23" runat="server" class="col-md-12 control-label">
                                                <strong>Next Followup Date</strong></asp:Label>
                                                                                        <div>
                                                                                            <div class="col-sm-12">
                                                                                                <div class="input-group date datetimepicker1">
                                                                                                    <span class="input-group-addon">
                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                    </span>
                                                                                                    <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">

                                                                                                    </asp:TextBox>


                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNextFollowupDate"
                                                                                                        ValidationGroup="info" ErrorMessage="" CssClass="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">

                                                                                <div class="form-group col-md-12" style="margin-top: 6px; margin-bottom: 10px;">
                                                                                    <div class="row">
                                                                                        <div>
                                                                                            <asp:Label ID="Label22" runat="server" class="col-sm-12 control-label">
                                                <strong>Description</strong></asp:Label>
                                                                                            <div class="col-sm-12">
                                                                                                <asp:TextBox ID="txtDescription" runat="server" class="form-control modaltextbox wdth100"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                                                                    ValidationGroup="info" ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                            <div class="form-group  col-md-12  marbtm15 center-text">
                                                                                <!--<span class="name">
                                                                            <label class="control-label">&nbsp;</label>
                                                                        </span>-->
                                                                                <span>
                                                                                    <asp:Button ID="ibtnAdd" runat="server" Text="Add" OnClick="ibtnAdd_Click"
                                                                                        ValidationGroup="info" CssClass="btn btn-primary btnaddicon redreq addwhiteicon center-text" />
                                                                                    <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                                                                        CssClass="btn" Text=" OK " />
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seachfinal">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="tablescrolldiv xscroll">
                                                                                    <div id="divgrdFollowUp" runat="server" class="table-responsive xscroll">
                                                                                        <asp:GridView ID="grdFollowUp" DataKeyNames="CustInfoID" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                                                                                            AutoGenerateColumns="false" CssClass="tooltip-demo text-center GridviewScrollItem table table-striped table-bordered table-hover">
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                                    ItemStyle-HorizontalAlign="Left">
                                                                                                    <ItemTemplate>
                                                                                                        <%#Eval("Contact") %>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="150px" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Next FollowupDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                                                                    <ItemTemplate>
                                                                                                        <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle Width="150px" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                                    ItemStyle-HorizontalAlign="Left">
                                                                                                    <ItemTemplate>
                                                                                                        <%#Eval("Description") %>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                                                            <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                                                            <EmptyDataRowStyle Font-Bold="True" />
                                                                                            <RowStyle CssClass="GridviewScrollItem" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
                                                    <asp:HiddenField ID="hndCustomerID" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel" runat="server">
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="printorder" style="font-size: medium">
                                <b>Total Number of panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Total SystemCapKW:&nbsp;</b><asp:Literal ID="lblSystemCapKW" runat="server"></asp:Literal>&nbsp;&nbsp;
                                 <b>Price/kw :&nbsp;</b><asp:Literal ID="Literal1" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Install Ratio:&nbsp;</b><asp:Literal ID="Literal2" runat="server"></asp:Literal>

                            </div>                        
                        </div>
                    </div>

                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="hpanel panel-body padtopzero">
                    <div class="finalgrid">
                        <div class="row">
                            <div id="PanGrid" runat="server">
                                <div class="xscroll">
                                    <div class="table-responsive ">
                                        <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo  text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
                                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                            <%-- <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />--%>
                                                            <%--  <i id='imgdiv<%# Eval("ProjectID") %>' class="fa fa-plus"></i>--%>
                                                            <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="50px" HeaderText="Proj#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndprojectid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                        <asp:HiddenField ID="hndNumberPanels" runat="server" Value='<%# Eval("NumberPanels") %>' />
                                                        <asp:Label ID="lblProject9" runat="server">
                                                            <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                         <asp:HyperLink ID="hypDetail2" runat="server" data-toggle="tooltip" data-placement="top" Visible="false" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/ECompany.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                            </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject17" Width="70px" runat="server"><%#Eval("ProjectStatus")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="100px" HeaderText="Sales Rep Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject18" runat="server"><%#Eval("employeename")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sub Source" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="CustSourceSub">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSalesCommPaid" runat="server" Width="70px"><%#Eval("CustSourceSub")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Project">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndprojectid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                        <asp:HiddenField ID="hndNumberPanels" runat="server" Value='<%# Eval("NumberPanels") %>' />

                                                        <asp:Label ID="lblProject" runat="server" Width="300px">
                                                            <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("Project")%></asp:HyperLink>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="P/CD" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject15" runat="server" Width="40px"><%#Eval("InstallPostCode")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Customername">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject3" data-original-title='<%#Eval("Customername")%>' data-toggle="tooltip" data-placement="top" Width="100px" runat="server"><%#Eval("Customername")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="contactmobile"
                                                    ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject4" runat="server" Width="70px"><%#Eval("contactmobile")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Next FollowUp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="NextFollowupDate" ItemStyle-Width="200px">
                                                    <ItemTemplate>
                                                        <div style="width: 100%; display: block;">
                                                            <div style="text-align: right; padding-right: 2px; margin-top: 0px; display: block; float: right;">
                                                                <asp:LinkButton ID="lbtnFollowUpNote" CommandName="addfollowupnote" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-azure btn-xs"
                                                                    CausesValidation="false" runat="server" data-original-title="Add FollowUp" data-toggle="tooltip" data-placement="top">
                                                               <i class="fa fa-calendar"></i> Followup</asp:LinkButton>
                                                            </div>
                                                            <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%# Eval("CustomerID")%>' />
                                                            <asp:Label ID="lblFollowUpDate" runat="server"
                                                                Width="80px" CssClass="spandate"> <%# Eval("NextFollowupDate","{0:dd MMM yyyy}")%></asp:Label>&nbsp;&nbsp;
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Follow Up Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Description"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <span id="spnfollowup" runat="server" data-toggle="tooltip" data-placement="top">
                                                            <asp:Label ID="lblFollowUpNote" runat="server" data-original-title='<%# Eval("Description").ToString() %>' data-toggle="tooltip" data-placement="top"
                                                                Width="100px"><%# Eval("Description")%></asp:Label>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false" HeaderText="Quote" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="QuoteSent">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject2" runat="server" Width="80px"><%# DataBinder.Eval(Container.DataItem, "QuoteSent", "{0:dd MMM yyyy}")%> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField HeaderText="Project Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject7" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ProjectNotes")%>' CssClass="tooltipwidth"
                                                            Width="200px"><%#Eval("ProjectNotes")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <%--<asp:TemplateField HeaderText="Install Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstallNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>'
                                                            Width="200px"><%#Eval("InstallerNotes")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <%--<asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject9" runat="server" Width="70px"><%#Eval("ProjectNumber")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <%--<asp:TemplateField HeaderText="Man#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="ManualQuoteNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject10" runat="server" Width="80px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField Visible="false" HeaderText="Alert" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject8" runat="server" Width="40px"><%# (( Eval("FollowUp").ToString() != "" ) && (Convert.ToDateTime(Eval("FollowUp")) > Convert.ToDateTime(DateTime.Now.ToShortDateString()))) ? "" : "<span style='color:red'>XXX</span>"%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Manual Proj#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="ManualQuoteNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmanualproject" runat="server" Width="30px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Installation Booking Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="InstallBookingDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstallBookingDate" runat="server" Width="70px"><%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                


                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                            <td colspan="98%" class="details" style="overflow:visible">
                                                                <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                        <tr>
                                                                            <td><b>Project</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label2" runat="server" Width="80px"><%#Eval("Project")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Sys</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject11" runat="server" Width="50px"><%#Eval("SystemCapKW")%></asp:Label>
                                                                            </td>
                                                                            <td><b>System Details</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject12" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("SystemDetails")%>' CssClass="tooltipwidth braktext" 
                                                                                    Width="200px"><%#Eval("SystemDetails")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>House Type</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject13" runat="server" Width="100px"><%#Eval("housetype")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Roof Type</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProjectRoofType" runat="server" Width="100px"><%#Eval("RoofType")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Install Book Date</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject14" runat="server" Width="120px"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Install Complete Date</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblComplete" runat="server" Width="160px"><%# DataBinder.Eval(Container.DataItem, "InstallCompleted", "{0:dd MMM yyyy}")%></asp:Label>
                                                                            </td>

                                                                            <td width="150px"><b>Project Notes</b>
                                                                            </td>
                                                                            <td>

                                                                                <asp:Label ID="lblProject7" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ProjectNotes")%>' CssClass="tooltipwidth"
                                                                                    Width="200px"><%#Eval("ProjectNotes")%></asp:Label>
                                                                            </td>
                                                                            <td><b>State</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject16" runat="server" Width="80px"><%#Eval("InstallState")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="150px"><b>Install Notes</b>
                                                                            </td>
                                                                            <td>

                                                                                <asp:Label ID="lblInstallNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>'
                                                                                    Width="200px"><%#Eval("InstallerNotes")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Status </b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label1" runat="server" Width="130px"><%#Eval("ProjectStatus")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Dep Rec</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject22" runat="server" Width="80px"><%# DataBinder.Eval(Container.DataItem, "DepositReceived", "{0:dd MMM yyyy}")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Price </b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject33" runat="server" Width="70px"><%# Eval("TotalQuotePrice").ToString() == "" ? "" : ""+Math.Round(Convert.ToDecimal(Eval("TotalQuotePrice")), 2) %></asp:Label>
                                                                            </td>
                                                                            <td><b>FinanceWith</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProjectFW" runat="server" Width="110px"><%# Eval("FinanceWith") %></asp:Label>
                                                                            </td>
                                                                            <td><b>Man#</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject10" runat="server" Width="80px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Bal Owing</b></td>
                                                                            <td>
                                                                                <asp:Label ID="lblBalOwing" runat="server" Width="70px"></asp:Label>
                                                                            </td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>

                                    <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        
          
    <div class="loaderPopUP">
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
            }

            function pageLoadedpro() {

                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $("[data-toggle=tooltip]").tooltip();
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            }
        </script>
    </div>

    <script type="text/javascript">
        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');
        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


    </script>
    <asp:HiddenField runat="server" ID="hdncountdata" />
</asp:Content>
