using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_view_project : System.Web.UI.Page
{
    decimal totalpanels = 0;
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    //protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            //ddlInstallBookingDate.Visible = false;

          //  cmpNextDate.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            //if (PanNoRecord.Visible == true)
            //{
            //    ltrPage.Visible = false;
            //}
            //else
            //{
            //    ltrPage.Visible = true;
            //}
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            if ((Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("DSalesRep")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("CompanyManager")))
            {
                tdExport.Visible = false;
            }
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                tdEmployee.Visible = true;

                if (Roles.IsUserInRole("SalesRep"))
                {
                    ddlTeam.Enabled = false;
                    ddlSearchEmployee.Enabled = false;
                 //   divpromo1.Visible = false;
                    ddlPromoOffer2.Enabled = true;
                }

            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                GridView1.Columns[3].Visible = false;
            }
           
            BindSearchDropdown();
            BindGrid(0);
            BindScript();
        }
      
    }
  

    public void BindSearchDropdown()
    {
        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlHouseType.Items.Clear();
        ddlHouseType.Items.Add(item4);

        ddlHouseType.DataSource = ClstblHouseType.tblHouseType_SelectASC();
        ddlHouseType.DataValueField = "HouseTypeID";
        ddlHouseType.DataMember = "HouseType";
        ddlHouseType.DataTextField = "HouseType";
        ddlHouseType.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlRoofType.Items.Clear();
        ddlRoofType.Items.Add(item5);

        ddlRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        ddlRoofType.DataValueField = "RoofTypeID";
        ddlRoofType.DataMember = "RoofType";
        ddlRoofType.DataTextField = "RoofType";
        ddlRoofType.DataBind();


        ddlProjectTypeID.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectTypeID.DataMember = "ProjectType";
        ddlProjectTypeID.DataTextField = "ProjectType";
        ddlProjectTypeID.DataValueField = "ProjectTypeID";
        ddlProjectTypeID.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        ddlSearchCancel.DataSource = ClstblProjectCancel.tblProjectCancel_SelectASC();
        ddlSearchCancel.DataMember = "ProjectCancel";
        ddlSearchCancel.DataTextField = "ProjectCancel";
        ddlSearchCancel.DataValueField = "ProjectCancelID";
        ddlSearchCancel.DataBind();

        ddlSearchSource.DataSource = ClstblCustSource.tblCustSource_Select();
        ddlSearchSource.DataMember = "CustSource";
        ddlSearchSource.DataTextField = "CustSource";
        ddlSearchSource.DataValueField = "CustSourceID";
        ddlSearchSource.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            string SalesTeam = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlSearchEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSearchEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }

        ddlSearchEmployee.DataMember = "fullname";
        ddlSearchEmployee.DataTextField = "fullname";
        ddlSearchEmployee.DataValueField = "EmployeeID";
        ddlSearchEmployee.DataBind();

        ddlPromoOffer1.DataSource = ClstblPromoType.tblPromoOffer_SelectASC();
        ddlPromoOffer1.DataValueField = "PromoOfferID";
        ddlPromoOffer1.DataTextField = "PromoOffer";
        ddlPromoOffer1.DataMember = "PromoOffer";
        ddlPromoOffer1.DataBind();

        ddlPromoOffer2.DataSource = ClstblPromoType.tbl_pstatus_SelectASC();
        ddlPromoOffer2.DataValueField = "statusid";
        ddlPromoOffer2.DataTextField = "pstatus";
        ddlPromoOffer2.DataMember = "pstatus";
        ddlPromoOffer2.DataBind();

        ddlFinanceOption.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlFinanceOption.DataValueField = "FinanceWithID";
        ddlFinanceOption.DataTextField = "FinanceWith";
        ddlFinanceOption.DataMember = "FinanceWith";
        ddlFinanceOption.DataBind();
        BindScript();
    }
    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {
            string[] strids = e.CommandArgument.ToString().Split('|');
            string projectid = strids[0];
            string companyid = strids[1];
            //Profile.eurosolar.companyid = companyid;
            //Profile.eurosolar.projectid = projectid;
            //Profile.eurosolar.contactid = "";

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            string CSalesTeamID = stEmpC.SalesTeamID;
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(projectid);

            if (e.CommandName == "viewproject")
            {
                if (projectid != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    string SalesTeamID = stEmp.SalesTeamID;

                    if (Roles.IsUserInRole("Sales Manager"))
                    {
                        if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
                        {
                            Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
                    }
                }
            }
        }

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);

        if (e.CommandName.ToLower() == "addfollowupnote")
        {
            hndCustomerID.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();

            ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(hndCustomerID.Value);
            ddlContact.DataMember = "Contact";
            ddlContact.DataTextField = "Contact";
            ddlContact.DataValueField = "ContactID";
            ddlContact.DataBind();

            DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(hndCustomerID.Value);
            if (dt.Rows.Count > 0)
            {
                divgrdFollowUp.Visible = true;
                grdFollowUp.DataSource = dt;
                grdFollowUp.DataBind();

                try
                {
                    ddlContact.SelectedValue = dt.Rows[0]["ContactID"].ToString();
                }
                catch
                {
                }
            }
            else
            {
                divgrdFollowUp.Visible = false;
            }
            BindScript();
        }

        BindGrid(0);
    }
    public void BindDataCount()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("Finance"))
        {
            Employeeid = "";
        }
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //DataTable dt = new DataTable();
        string searchdate = ddlSearchDate.SelectedValue;
        if (ddlInstallBookingDate.SelectedValue != string.Empty)
        {
            searchdate = "";
        }
        string Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";

        DataTable dtcount = ClstblCustomers.query_execute(Count);
        if (dtcount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = "(E.EmpType=" + EmpType1 + ") and (Convert(nvarchar(200),ET.SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))) and ";

            }
        }
        //dt = ClstblProjects.tblProjectsGetDataBySearch(employeeId, , , , ,,, , ,, , , , , , , , txtClient.Text, , , , , , , , ddlInstallBookingDate.SelectedValue, ,, , , , );

        string data = "select count(ProjectID) as countdata, count(NumberPanels)as NumberPanels from tblProjects P join tblProjectStatus PS on P.ProjectStatusID=Ps.ProjectStatusID left outer join tblEmployees E on P.EmployeeID=E.EmployeeID outer apply (select top 1 SalesTeamID from tblEmployeeTeam where EmployeeID=E.EmployeeID order by EmployeeID desc) ET  left outer join tblCustomers C on P.CustomerID=C.CustomerID left outer Join tblCustSource CS on C.CustSourceID=CS.CustSourceID left outer join tblContacts CM on P.ContactID=CM.ContactID left outer join tblFinanceWith FW on P.FinanceWithID=FW.FinanceWithID left outer join tblProjects2 P2 on P.ProjectID=P2.ProjectLinkID left outer join tblSalesTeams ST on ST.SalesTeamID=ET.SalesTeamID left outer join tblCustSourceSub CSS on C.CustSourceSubID=CSS.CustSourceSubID where " + data1 + " (P.ProjectTypeID='" + ddlProjectTypeID.SelectedValue.ToString() + "' or '" + ddlProjectTypeID.SelectedValue.ToString() + "'=0) and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and (P.ProjectNumber='" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "' or '" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((upper(P.Project) like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='') and (P.ProjectCancelID='" + ddlSearchCancel.SelectedValue + "' or '" + ddlSearchCancel.SelectedValue + "'=0) and ((upper(P.InstallAddress) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and ((upper(P.InstallCity) like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue.ToString() + "'+'%') or '" + ddlSearchState.SelectedValue.ToString() + "'='') and (C.CustSourceID='" + ddlSearchSource.SelectedValue.ToString() + "' or '" + ddlSearchSource.SelectedValue.ToString() + "'=0) and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtCompanyNo.Text) + "'=0) and (ET.SalesTeamID='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'='') and ((upper(P.PanelModel) like '%'+'" + System.Security.SecurityElement.Escape(txtPModel.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtPModel.Text) + "'='') and ((upper(P.InverterModel) like '%'+'" + System.Security.SecurityElement.Escape(txtIModel.Text) + "'+'%')  or '" + System.Security.SecurityElement.Escape(txtIModel.Text) + "'='') and ((C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtClient.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtClient.Text) + "'='') and (C.CustSourceSubID='" + ddlSerchSub.SelectedValue + "' or '" + ddlSerchSub.SelectedValue + "'=0) and (P.EmployeeID='" + ddlSearchEmployee.SelectedValue + "' or '" + ddlSearchEmployee.SelectedValue + "'=0) and ((P.InstallPostCode >= '" + txtSearchPostCodeFrom.Text + "') or '" + txtSearchPostCodeFrom.Text + "'=0) and ((P.InstallPostCode<= '" + txtSearchPostCodeTo.Text + "') or '" + txtSearchPostCodeTo.Text + "'=0)  and (P.SystemCapKW>='" + txtSysCapacityFrom.Text + "' or '" + txtSysCapacityFrom.Text + "'=0) and (P.SystemCapKW<='" + txtSysCapacityTo.Text + "' or '" + txtSysCapacityTo.Text + "'=0) and ('" + txtStartDate.Text + "' <= CASE WHEN '" + searchdate + "'=1 THEN P.ProjectOpened else CASE WHEN '" + searchdate + "'=3 then P.QuoteSent else CASE WHEN '" + searchdate + "'=4 then P.QuoteAccepted else CASE WHEN '" + searchdate + "'=5 then P.DepositReceived else CASE WHEN '" + searchdate + "'=6 then P.InstallBookingDate else CASE WHEN '" + searchdate + "'=7 then P.ActiveDate else CASE WHEN '" + searchdate + "'=8 then P.FollowUp else CASE WHEN '" + searchdate + "'=9 then P.InstallCompleted else CASE WHEN '" + searchdate + "'=2 then (select top 1 InvoicePayDate from tblInvoicePayments where ProjectID=P.ProjectID Order by InvoicePayDate ASC) else P.ProjectOpened end end end end end end end end END) and (('" + txtEndDate.Text + "' >= CASE WHEN '" + searchdate + "'=1 THEN P.ProjectOpened else CASE WHEN '" + searchdate + "'=3 then P.QuoteSent else CASE WHEN '" + searchdate + "'=4 then P.QuoteAccepted else CASE WHEN '" + searchdate + "'=5 then P.DepositReceived else CASE WHEN '" + searchdate + "'=6 then P.InstallBookingDate else CASE WHEN '" + searchdate + "'=7 then P.ActiveDate else CASE WHEN '" + searchdate + "'=8 then P.FollowUp else CASE WHEN '" + searchdate + "'=9 then P.InstallCompleted else CASE WHEN '" + searchdate + "'=2 then (select top 1 InvoicePayDate from tblInvoicePayments where ProjectID=P.ProjectID Order by InvoicePayDate ASC) else '" + txtEndDate.Text + "' end end end end end end end end END) or '" + txtEndDate.Text + "'='' ) and (P2.Promo1ID='" + ddlPromoOffer1.SelectedValue + "' or '" + ddlPromoOffer1.SelectedValue + "'=0) and (P2.Promo2ID='" + ddlPromoOffer2.SelectedValue + "' or '" + ddlPromoOffer2.SelectedValue + "'=0) and (P.FinanceWithID='" + ddlFinanceOption.SelectedValue + "' or '" + ddlFinanceOption.SelectedValue + "'=0) and ((P.ElecDistApproved= (case when '" + ddlDistApprovedDate.SelectedValue + "' = 1 then P.ElecDistApproved else case when '" + ddlDistApprovedDate.SelectedValue + "' = 2 then '' else '" + ddlDistApprovedDate.SelectedValue + "' end end )) or '" + ddlDistApprovedDate.SelectedValue + "'='') and ((upper(P.ElecDistApprovelRef) like '%'+'" + System.Security.SecurityElement.Escape(txtElecDistApprovelRef.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtElecDistApprovelRef.Text) + "'='') and ((P.PurchaseNo like '%' +'" + System.Security.SecurityElement.Escape(txtPurchaseNo.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtPurchaseNo.Text) + "'='') and ((P.InstallBookingDate=(case when CONVERT(varchar(20),'" + ddlInstallBookingDate.SelectedValue + "') = 1 then InstallBookingDate else case when CONVERT(varchar(20),'" + ddlInstallBookingDate.SelectedValue + "') =0 then '' else '" + ddlInstallBookingDate.SelectedValue + "' end END)) or  '" + ddlInstallBookingDate.SelectedValue + "'='')";

        DataTable dt = ClstblCustomers.query_execute(data);
        if (dt.Rows.Count > 0)
        {
            countdata = Convert.ToInt32(dt.Rows[0]["countdata"]);
            hdncountdata.Value = countdata.ToString();
            lblTotalPanels.Text = Convert.ToInt32(dt.Rows[0]["NumberPanels"]).ToString();
        }
        //Response.Write(data);
        //Response.End();
        #region MyRegionForPagination

        //if (custompagesize != 0)
        //{

        //    lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;

        //    if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //    {
        //        lastpageindex = lastpageindex + 1;
        //    }

        //    int pageindexcustom1 = Convert.ToInt32(hdncountdata.Value) / custompagesize;

        //    if (pageindexcustom1 > 0)
        //    {
        //        if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //        {
        //            pageindexcustom1 = pageindexcustom1 + 1;
        //        }

        //        if (pageindexcustom1 >= custompageIndex)
        //        {

        //        }
        //        else
        //        {
        //            custompageIndex = pageindexcustom1;
        //        }

        //        int PageIndex = 3;
        //        int StartPageIndex = custompageIndex - PageIndex;//10-3=7
        //        int EndPageIndex = custompageIndex + PageIndex;//10+3=13

        //        if (StartPageIndex < 1)
        //        {
        //            EndPageIndex = EndPageIndex - StartPageIndex;
        //        }
        //        if (EndPageIndex > lastpageindex)
        //        {
        //            StartPageIndex = (StartPageIndex - (EndPageIndex - lastpageindex)) + 1;
        //        }
        //        DataTable dt_page = new DataTable();
        //        dt_page.Columns.Add("ID");

        //        for (int i = 1; i <= lastpageindex; i++)
        //        {
        //            if ((StartPageIndex < i && EndPageIndex > i))
        //            {
        //                DataRow dr = dt_page.NewRow();
        //                dr["ID"] = i;
        //                dt_page.Rows.Add(dr);

        //            }
        //        }

        //        rptpage.DataSource = dt_page;
        //        rptpage.DataBind();

        //        if (custompageIndex == 1)
        //        {
        //            lnkfirst.Visible = false;
        //            lnkprevious.Visible = false;
        //            lnknext.Visible = true;
        //            lnklast.Visible = true;
        //        }
        //        if (custompageIndex > 1)
        //        {
        //            lnkfirst.Visible = true;
        //            lnkprevious.Visible = true;
        //        }
        //        if (custompageIndex == EndPageIndex)
        //        {
        //            lnknext.Visible = false;
        //            lnklast.Visible = false;
        //        }
        //        if (lastpageindex == custompageIndex)
        //        {
        //            lnknext.Visible = false;
        //            lnklast.Visible = false;
        //        }
        //        if (custompageIndex > 1 && custompageIndex < lastpageindex)
        //        {
        //            lnkfirst.Visible = true;
        //            lnkprevious.Visible = true;
        //            lnknext.Visible = true;
        //            lnklast.Visible = true;
        //        }
        //    }
        //    else
        //    {
        //        rptpage.DataSource = null;
        //        rptpage.DataBind();
        //        lnkfirst.Visible = false;
        //        lnkprevious.Visible = false;
        //        lnknext.Visible = false;
        //        lnklast.Visible = false;
        //    }
        //}
        //else
        //{
        //    rptpage.DataSource = null;
        //    rptpage.DataBind();
        //    lnkfirst.Visible = false;
        //    lnkprevious.Visible = false;
        //    lnknext.Visible = false;
        //    lnklast.Visible = false;
        //}
        #endregion
    }
    protected DataTable GetGridData()
    {
        //BindDataCount();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        string Employeeid = st.EmployeeID;
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("Finance"))
        {
            Employeeid = "";
        }
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        DataTable dt = new DataTable();
        string searchdate = ddlSearchDate.SelectedValue;
        if (ddlInstallBookingDate.SelectedValue != string.Empty)
        {
            searchdate = "";
        }
       // startindex = (custompageIndex - 1) * custompagesize + 1;
        string datagrid = "", Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";
        
        DataTable dtcount = ClstblCustomers.query_execute(Count);

        if (dtcount.Rows.Count > 0)
        {

            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = "(E.EmpType=" + EmpType1 + ") and (Convert(nvarchar(200),ET.SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))) and ";

            }
        }




        if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Purchase Manager") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Maintenance"))
        {
            //Response.Write('2');
            //Response.End();
            dt = ClstblProjects.tblProjectsGetDataBySearchAllNotPlanned(Employeeid, ddlProjectTypeID.SelectedValue.ToString(), selectedItem, txtProjectNumber.Text, txtMQNsearch.Text, txtSearch.Text, ddlSearchCancel.SelectedValue, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue.ToString(), ddlSearchSource.SelectedValue.ToString(), ddlTeam.SelectedValue, txtCompanyNo.Text, txtPModel.Text, txtIModel.Text, txtStartDate.Text, txtEndDate.Text, txtClient.Text, ddlSerchSub.SelectedValue, txtSearchPostCodeFrom.Text, txtSearchPostCodeTo.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlSearchEmployee.SelectedValue, searchdate, ddlInstallBookingDate.SelectedValue.ToString(), ddlPromoOffer1.SelectedValue, ddlPromoOffer2.SelectedValue, ddlFinanceOption.SelectedValue, txtElecDistApprovelRef.Text, txtPurchaseNo.Text, ddlDistApprovedDate.SelectedValue, ddlmeterupgrade.SelectedValue,ddlreadyactive.SelectedValue, ddlHouseType.SelectedValue, ddlRoofType.SelectedValue);
           
        }
        else
        {
            //Response.Write( selectedItem+"=="+ txtProjectNumber.Text);
            //Response.Write(Employeeid + "==>" + ddlProjectTypeID.SelectedValue.ToString() + "==>" + selectedItem + "==>" + txtProjectNumber.Text + "==>" + txtMQNsearch.Text + "==>" + txtSearch.Text + "==>" + ddlSearchCancel.SelectedValue + "==>" + txtSearchStreet.Text + "==>" + txtSerachCity.Text + "==>" + ddlSearchState.SelectedValue.ToString() + "==>" + ddlSearchSource.SelectedValue.ToString() + "==>" + ddlTeam.SelectedValue + "==>" + txtCompanyNo.Text + "==>" + txtPModel.Text + "==>" + txtIModel.Text + "==>" + txtStartDate.Text + "==>" + txtEndDate.Text + "==>" + txtClient.Text + "==>" + ddlSerchSub.SelectedValue + "==>" + txtSearchPostCodeFrom.Text + "==>" + txtSearchPostCodeTo.Text + "==>" + txtSysCapacityFrom.Text + "==>" + txtSysCapacityTo.Text + "==>" + ddlSearchEmployee.SelectedValue + "==>" + searchdate + "==>" + ddlInstallBookingDate.SelectedValue + "==>" + ddlPromoOffer1.SelectedValue + "==>" + ddlPromoOffer2.SelectedValue + "==>" + ddlFinanceOption.SelectedValue + "==>" + txtElecDistApprovelRef.Text + "==>" + txtPurchaseNo.Text + "==>" + ddlDistApprovedDate.SelectedValue + "==>" + ddlmeterupgrade.SelectedValue);
            //Response.End();
         
            dt = ClstblProjects.tblProjectsGetDataBySearch(Employeeid, ddlProjectTypeID.SelectedValue.ToString(), selectedItem, txtProjectNumber.Text, txtMQNsearch.Text, txtSearch.Text, ddlSearchCancel.SelectedValue, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue.ToString(), ddlSearchSource.SelectedValue.ToString(), ddlTeam.SelectedValue, txtCompanyNo.Text, txtPModel.Text, txtIModel.Text, txtStartDate.Text, txtEndDate.Text, txtClient.Text, ddlSerchSub.SelectedValue, txtSearchPostCodeFrom.Text, txtSearchPostCodeTo.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlSearchEmployee.SelectedValue, searchdate, ddlInstallBookingDate.SelectedValue, ddlPromoOffer1.SelectedValue, ddlPromoOffer2.SelectedValue, ddlFinanceOption.SelectedValue, txtElecDistApprovelRef.Text, txtPurchaseNo.Text, ddlDistApprovedDate.SelectedValue, ddlmeterupgrade.SelectedValue, ddlreadyactive.SelectedValue,ddlRoofType.SelectedValue,ddlHouseType.SelectedValue);
           // Response.End();
           
        }
        //datagrid = "select P.SalesCommPaid, P.Project+' | '+ CAST(P.ProjectNumber AS varchar(10)) as ProjectFine,P.ProjectID,P.CustomerID,P.ContactID,P.Project,P.InstallPostCode,P.QuoteSent,P.FollowUp,P.ProjectNotes,P.InstallerNotes,P.ProjectNumber,P.ManualQuoteNumber,P.SystemCapKW,P.SystemDetails,P.InstallBookingDate,P.InstallCompleted,P.InstallState,P.DepositReceived,P.TotalQuotePrice,PS.ProjectStatus as ProjectStatus,(E.EmpFirst+' '+E.EmpLast) as employeename,C.Customer as Customername,CS.CustSource,CM.ContMobile as contactmobile,(select StockItem from tblStockItems where StockItemID=P.InverterDetailsID) as InverterName,(select HouseType from tblHouseType where HouseTypeID=P.HouseTypeID) as housetype,(select RoofType from tblRoofTypes where RoofTypeID=P.RoofTypeID) as RoofType,(CM.ContFirst+' '+CM.ContLast) as Contact,convert(varchar,P.QuoteSent,106) as QSDate,convert(varchar,P.InstallBookingDate,106) IBDate,convert(varchar,P.DepositReceived,106) as DRDate,P.NumberPanels,ST.SalesTeam,FW.FinanceWith, CSS.CustSourceSub,(select top 1 convert(varchar,InvoicePayDate,106) from tblInvoicePayments where ProjectID=P.ProjectID order by InvoicePayDate desc)as FDD,'' as NextFollowupDate, '' as [Description],(select top 1 convert(varchar,InvoicePayTotal,106) from tblInvoicePayments where ProjectID=P.ProjectID order by InvoicePayTotal desc)as FDA,(select ProjectStatus from tblProjectStatus where ProjectStatusID=P.ProjectStatusID) as ProjectStatus from tblProjects P join tblProjectStatus PS on P.ProjectStatusID=Ps.ProjectStatusID left outer join tblEmployees E on P.EmployeeID=E.EmployeeID outer apply (select top 1 SalesTeamID from tblEmployeeTeam where EmployeeID=E.EmployeeID order by EmployeeID desc) ET  left outer join tblCustomers C on P.CustomerID=C.CustomerID left outer Join tblCustSource CS on C.CustSourceID=CS.CustSourceID left outer join tblContacts CM on P.ContactID=CM.ContactID left outer join tblFinanceWith FW on P.FinanceWithID=FW.FinanceWithID left outer join tblProjects2 P2 on P.ProjectID=P2.ProjectLinkID left outer join tblSalesTeams ST on ST.SalesTeamID=ET.SalesTeamID left outer join tblCustSourceSub CSS on C.CustSourceSubID=CSS.CustSourceSubID where " + data1 + " (P.ProjectTypeID='" + ddlProjectTypeID.SelectedValue.ToString() + "' or '" + ddlProjectTypeID.SelectedValue.ToString() + "'=0) and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and (P.ProjectNumber='" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "' or '" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((upper(P.Project) like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='') and (P.ProjectCancelID='" + ddlSearchCancel.SelectedValue + "' or '" + ddlSearchCancel.SelectedValue + "'=0) and ((upper(P.InstallAddress) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and ((upper(P.InstallCity) like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue.ToString() + "'+'%') or '" + ddlSearchState.SelectedValue.ToString() + "'='') and (C.CustSourceID='" + ddlSearchSource.SelectedValue.ToString() + "' or '" + ddlSearchSource.SelectedValue.ToString() + "'=0) and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtCompanyNo.Text) + "'=0) and (ET.SalesTeamID='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'='') and ((upper(P.PanelModel) like '%'+'" + System.Security.SecurityElement.Escape(txtPModel.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtPModel.Text) + "'='') and ((upper(P.InverterModel) like '%'+'" + System.Security.SecurityElement.Escape(txtIModel.Text) + "'+'%')  or '" + System.Security.SecurityElement.Escape(txtIModel.Text) + "'='') and ((C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtClient.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtClient.Text) + "'='') and (C.CustSourceSubID='" + ddlSerchSub.SelectedValue + "' or '" + ddlSerchSub.SelectedValue + "'=0) and (P.EmployeeID='" + ddlSearchEmployee.SelectedValue + "' or '" + ddlSearchEmployee.SelectedValue + "'=0) and ((P.InstallPostCode >= '" + txtSearchPostCodeFrom.Text + "') or '" + txtSearchPostCodeFrom.Text + "'=0) and ((P.InstallPostCode<= '" + txtSearchPostCodeTo.Text + "') or '" + txtSearchPostCodeTo.Text + "'=0)  and (P.SystemCapKW>='" + txtSysCapacityFrom.Text + "' or '" + txtSysCapacityFrom.Text + "'=0) and (P.SystemCapKW<='" + txtSysCapacityTo.Text + "' or '" + txtSysCapacityTo.Text + "'=0) and ('" + txtStartDate.Text + "' <= CASE WHEN '" + searchdate + "'=1 THEN P.ProjectOpened else CASE WHEN '" + searchdate + "'=3 then P.QuoteSent else CASE WHEN '" + searchdate + "'=4 then P.QuoteAccepted else CASE WHEN '" + searchdate + "'=5 then P.DepositReceived else CASE WHEN '" + searchdate + "'=6 then P.InstallBookingDate else CASE WHEN '" + searchdate + "'=7 then P.ActiveDate else CASE WHEN '" + searchdate + "'=8 then P.FollowUp else CASE WHEN '" + searchdate + "'=9 then P.InstallCompleted else CASE WHEN '" + searchdate + "'=2 then (select top 1 InvoicePayDate from tblInvoicePayments where ProjectID=P.ProjectID Order by InvoicePayDate ASC) else P.ProjectOpened end end end end end end end end END) and (('" + txtEndDate.Text + "' >= CASE WHEN '" + searchdate + "'=1 THEN P.ProjectOpened else CASE WHEN '" + searchdate + "'=3 then P.QuoteSent else CASE WHEN '" + searchdate + "'=4 then P.QuoteAccepted else CASE WHEN '" + searchdate + "'=5 then P.DepositReceived else CASE WHEN '" + searchdate + "'=6 then P.InstallBookingDate else CASE WHEN '" + searchdate + "'=7 then P.ActiveDate else CASE WHEN '" + searchdate + "'=8 then P.FollowUp else CASE WHEN '" + searchdate + "'=9 then P.InstallCompleted else CASE WHEN '" + searchdate + "'=2 then (select top 1 InvoicePayDate from tblInvoicePayments where ProjectID=P.ProjectID Order by InvoicePayDate ASC) else '" + txtEndDate.Text + "' end end end end end end end end END) or '" + txtEndDate.Text + "'='' ) and (P2.Promo1ID='" + ddlPromoOffer1.SelectedValue + "' or '" + ddlPromoOffer1.SelectedValue + "'=0) and (P2.Promo2ID='" + ddlPromoOffer2.SelectedValue + "' or '" + ddlPromoOffer2.SelectedValue + "'=0) and (P.FinanceWithID='" + ddlFinanceOption.SelectedValue + "' or '" + ddlFinanceOption.SelectedValue + "'=0) and ((P.ElecDistApproved= (case when '" + ddlDistApprovedDate.SelectedValue + "' = 1 then P.ElecDistApproved else case when '" + ddlDistApprovedDate.SelectedValue + "' = 2 then '' else '" + ddlDistApprovedDate.SelectedValue + "' end end )) or '" + ddlDistApprovedDate.SelectedValue + "'='') and ((upper(P.ElecDistApprovelRef) like '%'+'" + System.Security.SecurityElement.Escape(txtElecDistApprovelRef.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtElecDistApprovelRef.Text) + "'='') and ((P.PurchaseNo like '%' +'" + System.Security.SecurityElement.Escape(txtPurchaseNo.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtPurchaseNo.Text) + "'='') and ((P.InstallBookingDate=(case when CONVERT(varchar(20),'" + ddlInstallBookingDate.SelectedValue + "') = 1 then InstallBookingDate else case when CONVERT(varchar(20),'" + ddlInstallBookingDate.SelectedValue + "') =0 then '' else '" + ddlInstallBookingDate.SelectedValue + "' end END)) or  '" + ddlInstallBookingDate.SelectedValue + "'='') order by P.ProjectOpened desc OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";

        //dt = ClstblCustomers.query_execute(datagrid);
        //Response.Write(datagrid);
        //Response.End();
        int noofpanel = 0;
        int noofSystemCapKW = 0;
        decimal QuotePrice = 0;
        if(dt.Rows.Count > 0)
        {
            QuotePrice = Convert.ToDecimal( dt.Compute("Sum(TotalQuotePrice)", "").ToString());
        }
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["NumberPanels"].ToString() != string.Empty)
            {
                noofpanel += Convert.ToInt32(dr["NumberPanels"]);
                noofSystemCapKW += Convert.ToInt32(dr["SystemCapKW"]);                
            }
        }
        try
        {
            lblTotalPanels.Text = noofpanel.ToString();
            lblSystemCapKW.Text = noofSystemCapKW.ToString();
            decimal syscap = Convert.ToDecimal(lblSystemCapKW.Text);
            decimal Ans = QuotePrice / syscap;
            decimal ans1 = Math.Round(Ans, 2);
            Literal1.Text = Convert.ToString(ans1);
            if(dt.Rows.Count > 0)
            {
                DataRow[] drprjCount1 = dt.Select("ProjectStatusID  IN ('5','10','19','11','12','13','14')");
                if (drprjCount1.Length > 0)
                {
                    decimal count = drprjCount1.Length;
                    Decimal dtCount = dt.Rows.Count;
                    decimal Ratio = Convert.ToDecimal(count / dtCount) * 100;
                    decimal ans2 = Math.Round(Ratio, 2);
                    Literal2.Text = ans2.ToString();
                }
                else
                {
                    Literal2.Text = "0.00";
                }

            }
        }
        catch(Exception ex)
        {

        }
       
        //int iTotalRecords = dt.Rows.Count;
        //int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //if (iEndRecord > iTotalRecords)
        //{
        //    iEndRecord = iTotalRecords;
        //}
        //if (iStartsRecods == 0)
        //{
        //    iStartsRecods = 1;
        //}
        //if (iEndRecord == 0)
        //{
        //    iEndRecord = iTotalRecords;
        //}
        //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";


        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
       
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            divnopage.Visible = true;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string employeeId = st.EmployeeID;

        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("Sales Manager"))
        {
            employeeId = "";
        }

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        //Response.Write(employeeId + "=>" + ddlProjectTypeID.SelectedValue.ToString() + "=>" + selectedItem + "=>" + txtProjectNumber.Text + "=>" + txtMQNsearch.Text + "=>" + txtSearch.Text + "=>" + ddlSearchCancel.SelectedValue + "=>" + txtSearchStreet.Text + "=>" + txtSerachCity.Text + "=>" + ddlSearchState.SelectedValue.ToString() + "=>" + ddlSearchSource.SelectedValue.ToString() + "=>" + ddlTeam.SelectedValue + "=>" + txtCompanyNo.Text + "=>" + txtPModel.Text + "=>" + txtIModel.Text + "=>" + txtStartDate.Text + "=>" + txtEndDate.Text + "=>" + txtClient.Text + "=>" + ddlSerchSub.SelectedValue + "=>" + txtSearchPostCodeFrom.Text + "=>" + txtSearchPostCodeTo.Text + "=>" + txtSysCapacityFrom.Text + "=>" + txtSysCapacityTo.Text + "=>" + ddlSearchEmployee.SelectedValue + "=>" + ddlSearchDate.SelectedValue + "=>" + ddlInstallBookingDate.SelectedValue + "=>" + ddlPromoOffer1.SelectedValue + "=>" + ddlPromoOffer2.SelectedValue + "=>" + ddlFinanceOption.SelectedValue + "=>" + ddlDistApprovedDate.SelectedValue);
        //Response.End();
    
        // DataTable dtCount = ClstblProjects.tblProjectsGetDataBySearchCount(employeeId, ddlProjectTypeID.SelectedValue.ToString(), selectedItem, txtProjectNumber.Text, txtMQNsearch.Text, txtSearch.Text, ddlSearchCancel.SelectedValue, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue.ToString(), ddlSearchSource.SelectedValue.ToString(), ddlTeam.SelectedValue, txtCompanyNo.Text, txtPModel.Text, txtIModel.Text, txtStartDate.Text, txtEndDate.Text, txtClient.Text, ddlSerchSub.SelectedValue, txtSearchPostCodeFrom.Text, txtSearchPostCodeTo.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlSearchEmployee.SelectedValue, ddlSearchDate.SelectedValue, ddlInstallBookingDate.SelectedValue, ddlPromoOffer1.SelectedValue, ddlPromoOffer2.SelectedValue, ddlFinanceOption.SelectedValue, ddlDistApprovedDate.SelectedValue);

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
        BindScript();
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
           // custompagesize = 0;
        }
        else if(Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else{
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindGrid(0);
        BindScript();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start

        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;

        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }
    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    ddlProjectTypeID.SelectedValue = string.Empty;
    //    txtProjectNumber.Text = string.Empty;
    //    txtMQNsearch.Text = string.Empty;
    //    txtSearch.Text = string.Empty;
    //    ddlSearchCancel.SelectedValue = string.Empty;
    //    ddlSearchSource.SelectedValue = string.Empty;
    //    ddlSerchSub.SelectedValue = string.Empty;
    //    ddlTeam.SelectedValue = string.Empty;
    //    txtSearchStreet.Text = string.Empty;
    //    txtSerachCity.Text = string.Empty;
    //    ddlSearchState.SelectedValue = string.Empty;
    //    txtCompanyNo.Text = string.Empty;
    //    txtPModel.Text = string.Empty;
    //    txtIModel.Text = string.Empty;
    //    ddlSearchDate.SelectedValue = string.Empty;
    //    txtClient.Text = string.Empty;
    //    ddlSearchEmployee.SelectedValue = string.Empty;
    //    txtSysCapacityFrom.Text = string.Empty;
    //    txtSysCapacityTo.Text = string.Empty;
    //    txtSearchPostCodeFrom.Text = string.Empty;
    //    txtSearchPostCodeTo.Text = string.Empty;
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    ddlPromoOffer1.SelectedValue = string.Empty;
    //    ddlPromoOffer2.SelectedValue = string.Empty;
    //    ddlFinanceOption.SelectedValue = string.Empty;
    //    ddlInstallBookingDate.SelectedValue = string.Empty;
    //    txtPurchaseNo.Text = string.Empty;

    //    foreach (RepeaterItem item in lstSearchStatus.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        chkselect.Checked = false;
    //    }

    //    BindGrid(0);
    //}
    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        ddlContact.SelectedValue = "";
        //txtFollowupDate.Text = string.Empty;
        txtNextFollowupDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        ddlManager.ClearSelection();
    }
    protected void ibtnAdd_Click(object sender, EventArgs e)
    {
        BindScript();
        string CustomerID = hndCustomerID.Value;
        string Description = txtDescription.Text;
        //string FollowupDate = txtFollowupDate.Text;
        string NextFollowupDate = txtNextFollowupDate.Text;

        if (ddlManager.SelectedValue == "1")
        {
            NextFollowupDate = txtNextFollowupDate.Text;
        }
        if (ddlManager.SelectedValue == "2")
        {
            NextFollowupDate = DateTime.Now.AddHours(14).ToShortDateString();
        }

        string ContactID = ddlContact.SelectedValue.ToString();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;

        int success = ClstblCustInfo.tblCustInfo_Insert(CustomerID, Description, NextFollowupDate, ContactID, CustInfoEnteredBy, ddlManager.SelectedValue);
        
        if (Convert.ToString(success) != string.Empty)
        {
            ModalPopupExtender2.Hide();
            ddlContact.SelectedValue = "";
            //txtFollowupDate.Text = string.Empty;
            txtNextFollowupDate.Text = string.Empty;
            txtDescription.Text = string.Empty;
        }
        ddlManager.ClearSelection();

        ddlContact.SelectedValue = "";
        //txtFollowupDate.Text = string.Empty;
        txtNextFollowupDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
     

        BindGrid(0);
    }
    protected void ddlManager_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        if (ddlManager.SelectedValue == "1")
        {
            divNextDate.Visible = true;
        }
        else
        {
            divNextDate.Visible = false;
        }
        BindScript();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //int nopanel = 0;
            HiddenField hndCustomerID = (HiddenField)e.Row.FindControl("hndCustomerID");
            Label lblFollowUpDate = (Label)e.Row.FindControl("lblFollowUpDate");
            Label lblFollowUpNote = (Label)e.Row.FindControl("lblFollowUpNote");
            HyperLink detail1 = (HyperLink)e.Row.FindControl("hypDetail");
            HyperLink detail2 = (HyperLink)e.Row.FindControl("hypDetail2");
            if (Roles.IsUserInRole("Lead Manager"))
            {
                detail1.Visible = false;
                detail2.Visible = true;
            }
            HtmlGenericControl spnfollowup = (HtmlGenericControl)e.Row.FindControl("spnfollowup");
            DataTable dtFollwUp = ClstblCustInfo.tblCustInfo_SelectTop1ByCustID(hndCustomerID.Value);
           
            if (dtFollwUp.Rows.Count > 0)
            {
                try
                {
                    lblFollowUpDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["NextFollowupDate"].ToString()));
                   
                }
                catch { }
                try
                {
                    lblFollowUpNote.Text = dtFollwUp.Rows[0]["Description"].ToString();
                    spnfollowup.Attributes.Add(" data-original-title", dtFollwUp.Rows[0]["Description"].ToString());
                }
                catch
                {
                }
            }
            //SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(hndprojectid.Value);
            //nopanel += Convert.ToInt32(hndNumberPanels.Value);
            //Response.Write(nopanel + "=");
            //lblTotalPanels.Text +=nopanel;
        }

    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        //string employeeId = st.EmployeeID;
        //if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("Sales Manager"))
        //{
        //    employeeId = "";
        //}
        //string selectedItem = "";
        //foreach (RepeaterItem item in lstSearchStatus.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

        //    if (chkselect.Checked == true)
        //    {
        //        selectedItem += "," + hdnID.Value.ToString();
        //    }
        //}
        //if (selectedItem != "")
        //{
        //    selectedItem = selectedItem.Substring(1);
        //}
        ////Response.Write('1');
        ////Response.End();
        ////DataTable dt = ClstblProjects.tblProjectsGetDataBySearch(employeeId, ddlProjectTypeID.SelectedValue.ToString(), selectedItem, txtProjectNumber.Text, txtMQNsearch.Text, txtSearch.Text, ddlSearchCancel.SelectedValue, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue.ToString(), ddlSearchSource.SelectedValue.ToString(), ddlTeam.SelectedValue, txtCompanyNo.Text, txtPModel.Text, txtIModel.Text, txtStartDate.Text, txtEndDate.Text, txtClient.Text, ddlSerchSub.SelectedValue, txtSearchPostCodeFrom.Text, txtSearchPostCodeTo.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlSearchEmployee.SelectedValue, ddlSearchDate.SelectedValue, ddlInstallBookingDate.SelectedValue, ddlPromoOffer1.SelectedValue, ddlPromoOffer2.SelectedValue, ddlFinanceOption.SelectedValue, txtElecDistApprovelRef.Text, txtPurchaseNo.Text, ddlDistApprovedDate.SelectedValue, ddlmeterupgrade.SelectedValue,ddlreadyactive.SelectedValue);
        //DataTable dt = ClstblProjects.tblProjectsGetDataBySearchForExcel2(employeeId, ddlProjectTypeID.SelectedValue.ToString(), selectedItem, txtProjectNumber.Text, txtMQNsearch.Text, txtSearch.Text, ddlSearchCancel.SelectedValue, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue.ToString(), ddlSearchSource.SelectedValue.ToString(), ddlTeam.SelectedValue, txtCompanyNo.Text, txtPModel.Text, txtIModel.Text, txtStartDate.Text, txtEndDate.Text, txtClient.Text, ddlSerchSub.SelectedValue, txtSearchPostCodeFrom.Text, txtSearchPostCodeTo.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlSearchEmployee.SelectedValue, ddlSearchDate.SelectedValue, ddlInstallBookingDate.SelectedValue, ddlPromoOffer1.SelectedValue, ddlPromoOffer2.SelectedValue, ddlFinanceOption.SelectedValue, txtElecDistApprovelRef.Text, txtPurchaseNo.Text, ddlDistApprovedDate.SelectedValue, ddlmeterupgrade.SelectedValue, ddlreadyactive.SelectedValue);        
        //Response.Clear();
        //int count=dt.Rows.Count;
        //try
        //{
        //    Export oExport = new Export();
        //    string FileName = "Projects" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        //    int[] ColList = { 5, 6, 29, 28,
        //                         24, 9, 11, 12,
        //                        13, 14,26, 15,
        //                        17, 20, 21, 18,
        //                        19, 27, 32, 25,
        //                          33, 34, 23, 35,
        //                          36, 39, 37, 38,
        //            42,43,44,45,46
        //        ,47,48,49
        //        };
        //    string[] arrHeader = { "Project", "P/CD", "Quote", "Contact",
        //                                 "Mobile", "Project Notes", "Project#", "Man#",
        //                                 "System Cap", "System Details", "House", "Install Book Date",
        //                                 "State", "Status", "Employee Name", "Dep Rec",
        //                                 "Price", "Roof Type", "Panels", "Inverter",
        //                                 "Team","FinanceWith", "Source", "Sub Source",
        //                                 "First Dep. Date", "First Dep. Amount", "NextFollowupDate", "Description",
        //            "InstallerName","PickList Created Date","Stock PickUp Date","Stock PickUp Installer","Stock Pickup Note"
        //            ,"Stock PickUp Return Date"
        //            ,"Stock Return PickUp Installer","Stock Return Pickup Note"
        //        };
        //    //only change file extension to .xls for excel file
        //    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        //}
        //catch (Exception Ex)
        //{
        //    //   lblError.Text = Ex.Message;
        //}
        EmportExcel();
    }

    public void EmportExcel()
    {
        // New Create by Kiran 30-11-2019
        DataTable dt = GetGridData();
        int cn = dt.Columns.Count;
        int count = dt.Rows.Count;
        //Notes:Add 54 For CustEmail inColList Array And UnComment 2 Times  (CM.ContEmail as ContEmail) above tblProjects P  
        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
            string FileName = "Projects" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            
                int[] ColList = { 5, 6, 55, 22,
                                 24, 9, 11, 12,
                                13, 14,26, 15,
                                17, 20, 21, 18,
                                19, 27, 32, 25,
                                  33, 34, 23, 35,
                                  36, 39, 37, 38,
                    42,43,44,45,46
                ,47,48,49,50,51,52,53,54,56,57,58,59,60,61,62,63,64,66
                };
            //}
            string[] arrHeader = { "Project", "P/CD", "Quote", "Contact",
                                         "Mobile", "Project Notes", "Project#", "Man#",
                                         "System Cap", "System Details", "House", "Install Book Date",
                                         "State", "Status", "Employee Name", "Dep Rec",
                                         "Price", "Roof Type", "Panels", "Inverter",
                                         "Team","FinanceWith", "Source", "Sub Source",
                                         "First Dep. Date", "First Dep. Amount", "NextFollowupDate", "Description",
                    "InstallerName","PickList Created Date","Stock PickUp Date","Stock PickUp Installer","Stock Pickup Note"
                    ,"Stock PickUp Return Date"
                    ,"Stock Return PickUp Installer","Stock Return Pickup Note","PanelModel","InverterModel","SecondInverterModel","ThirdInverterModel","ContactEmail","Leaddate","Proj.OpenDate","DiffInDays","Reason","CancelReason","HoldReason","ProjectCancelDate","Manager Discount","ManagerDiscountDate","MeterPhase"
                };
            //only change file extension to .xls for excel file11
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }

        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void ddlSearchSource_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSerchSub.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
        ddlSerchSub.Items.Add(lst);
        
        if (ddlSearchSource.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlSearchSource.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlSerchSub.DataSource = dt;
                ddlSerchSub.DataMember = "CustSourceSub";
                ddlSerchSub.DataTextField = "CustSourceSub";
                ddlSerchSub.DataValueField = "CustSourceSubID";
                ddlSerchSub.DataBind();
            }
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");

        if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Purchase Manager") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Maintenance"))
        {
            if (hdnID.Value == "2")
            {
                e.Item.Visible = false;
            }
        }

    }
    public void BindScript()
    {
      //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlProjectTypeID.SelectedValue = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtMQNsearch.Text = string.Empty;
        txtSearch.Text = string.Empty;
        ddlSearchCancel.SelectedValue = string.Empty;
        ddlSearchSource.SelectedValue = string.Empty;
        ddlSerchSub.SelectedValue = string.Empty;
        ddlTeam.SelectedValue = string.Empty;
        txtSearchStreet.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        ddlSearchState.SelectedValue = string.Empty;
        txtCompanyNo.Text = string.Empty;
        txtPModel.Text = string.Empty;
        txtIModel.Text = string.Empty;
        ddlSearchDate.SelectedValue = string.Empty;
        txtClient.Text = string.Empty;
        ddlSearchEmployee.SelectedValue = string.Empty;
        txtSysCapacityFrom.Text = string.Empty;
        txtSysCapacityTo.Text = string.Empty;
        txtSearchPostCodeFrom.Text = string.Empty;
        txtSearchPostCodeTo.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlPromoOffer1.SelectedValue = string.Empty;
        ddlPromoOffer2.SelectedValue = string.Empty;
        ddlFinanceOption.SelectedValue = string.Empty;
        ddlInstallBookingDate.SelectedValue = string.Empty;
        txtPurchaseNo.Text = string.Empty;
        

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    
}