using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_invoice_invoicespaid : System.Web.UI.Page
{
    static DataView dv;


    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            custompageIndex = 1;
            BindGrid(0);
            BindDropDown();




            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Verification")))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
        }
    }

    public void BindDropDown()
    {
        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        //ddlpayby.DataSource = ClstblInvoicePayments.tblInvoicePayMethod_GetdataByActive();
        //ddlpayby.DataMember = "InvoicePayMethod";
        //ddlpayby.DataTextField = "InvoicePayMethod";
        //ddlpayby.DataValueField = "InvoicePayMethodID";
        //ddlpayby.DataBind();

        ddlpayby.DataSource = ClsProjectSale.tblFPTransType_getInv();
        ddlpayby.DataValueField = "FPTransTypeID";
        ddlpayby.DataTextField = "FPTransTypeAB";
        ddlpayby.DataMember = "FPTransTypeAB";
        ddlpayby.DataBind();

        ddlVerifiedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlVerifiedBy.DataMember = "fullname";
        ddlVerifiedBy.DataTextField = "fullname";
        ddlVerifiedBy.DataValueField = "EmployeeID";
        ddlVerifiedBy.DataBind();
    }

    public void binddata()
    {
        string enddate = "";
        if (txtEndDate.Text != string.Empty)
        {
            enddate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtEndDate.Text));
        }
        string startdate = "";
        if (txtStartDate.Text != string.Empty)
        {
            startdate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtStartDate.Text));
        }
        string data = "select count(IP.InvoicePaymentID) as count1 from tblInvoicePayments IP join tblProjects P on P.ProjectID=IP.ProjectID join tblCustomers C on C.CustomerID=P.CustomerID left outer join tblFPTransType FTP on FTP.FPTransTypeID=IP.InvoicePayMethodID left outer join tblEmployees E on E.EmployeeID=IP.VerifiedBy left outer join tblProjectStatus PS on P.ProjectStatusID=Ps.ProjectStatusID where ((IP.InvoicePayMethodID='" + ddlpayby.SelectedValue.ToString() + "') or  '" + ddlpayby.SelectedValue.ToString() + "'=0) and ((IP.VerifiedBy = '" + ddlVerifiedBy.SelectedValue + "') or '" + ddlVerifiedBy.SelectedValue + "'=0) and ((P.InvoiceNumber = '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "')or '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "'=0) and ((upper(P.Project) like '%'+'" + System.Security.SecurityElement.Escape(txtproject.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtproject.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and ((upper(C.Customer) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchCustomer.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchCustomer.Text) + "'='') and isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') <=  CASE WHEN '" + ddlDate.SelectedValue + "'=1 THEN P.InstallBookingDate else  CASE WHEN '" + ddlDate.SelectedValue + "'=2 then IP.InvoicePayDate else  CASE WHEN '" + ddlDate.SelectedValue + "'=3 then IP.Verified else CASE WHEN '" + ddlDate.SelectedValue + "'=4 then IP.ActualPayDate else CASE WHEN '" + ddlDate.SelectedValue + "'=5 then P.InstallBookingDate else isnull(Convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'')end end end end END and ((convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "') >=CASE WHEN '" + ddlDate.SelectedValue + "'=1 THEN P.InstallCompleted else CASE WHEN '" + ddlDate.SelectedValue + "'=2 then IP.InvoicePayDate else CASE WHEN '" + ddlDate.SelectedValue + "'=3 then IP.Verified else CASE WHEN '" + ddlDate.SelectedValue + "'=4 then IP.ActualPayDate else CASE WHEN '" + ddlDate.SelectedValue + "'=5 then P.InstallBookingDate else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'') end end end end END) or (isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'')='')) and (IP.IsVerified='" + ddlIsVerified.SelectedValue + "' or (isnull('" + ddlIsVerified.SelectedValue + "',2)=2)) and ((Convert(nvarchar(200),P.FinanceWithID) in (select  * from Split('" + ddlfinancewith.SelectedValue + "',',')))or '" + ddlfinancewith.SelectedValue + "'='')";
        DataTable dt1 = ClstblCustomers.query_execute(data);
        if (dt1.Rows.Count > 0)
        {
            countdata = Convert.ToInt32(dt1.Rows[0]["count1"]);
            hdncountdata.Value = countdata.ToString();
        }
        // Response.Write(data);
        //Response.End();

    }
    protected DataTable GetGridData()
    {
        binddata();
        string fin = "";
        DataTable dtfin = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        if (dtfin.Rows.Count > 0)
        {
            if (ddlfinancewith.SelectedValue.ToString() == "1")
            {
                fin = "1";
            }
            if (ddlfinancewith.SelectedValue.ToString() == "2")
            {
                foreach (DataRow dr in dtfin.Rows)
                {
                    if (dr["FinanceWithID"].ToString() != "1")
                    {
                        fin += dr["FinanceWithID"] + ",";
                    }
                }
                fin = fin.Substring(0, fin.Length - 1);
            }
        }


        DataTable dt = new DataTable();
        //Response.Write("'" + txtInvSearch.Text + "','" + txtproject.Text + "','" + ddlpayby.SelectedValue.ToString() + "','" + ddlSearchState.SelectedValue.ToString() + "','" + txtStartDate.Text + "','" + txtEndDate.Text + "','" + ddlVerifiedBy.SelectedValue + "','" + txtSearchCustomer.Text + "','" + ddlIsVerified.SelectedValue + "','" + ddlDate.SelectedValue + "','" + fin);
        //dt = ClstblInvoicePayments.tblInvoicePayments_GetDataBySearch(txtInvSearch.Text, txtproject.Text, ddlpayby.SelectedValue.ToString(), ddlSearchState.SelectedValue.ToString(), txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlVerifiedBy.SelectedValue, txtSearchCustomer.Text, ddlIsVerified.SelectedValue, ddlDate.SelectedValue, fin);
        //Response.Write(dt.Rows.Count);
        //Response.End();
        startindex = (custompageIndex - 1) * custompagesize + 1;
        string enddate = "";
        if (txtEndDate.Text != string.Empty)
        {
            enddate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtEndDate.Text));
        }
        string startdate = "";
        if (txtStartDate.Text != string.Empty)
        {
            startdate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtStartDate.Text));
        }
        
        string data1 = "select  IP.InvoicePaymentID,IP.InvoicePayDate,IP.InvoicePayTotal,IP.Verified,IP.PaymentNote,IP.IsVerified,(select case when E.EmpLast is null then E.EmpFirst else E.EmpFirst+' '+E.EmpLast end) as VerifiedByName,P.ProjectID,P.TotalQuotePrice,P.Project,P.ProjectNumber,C.Customer ,C.CustomerID,FTP.FPTransType,convert(varchar(50),IP.InvoicePayDate,6) as IPDate,IP.ReceiptNumber,convert(varchar(50),IP.Verified,6) as VDate,convert(varchar (50),P.InstallBookingDate,6) as InstallBookingDate ,IP.ActualPayDate, IP.CCSurcharge, PS.ProjectStatus,P.InvoiceNumber, ISNULL(IP.CCSurcharge, 0.00) as CCSurcharge from tblInvoicePayments IP join tblProjects P on P.ProjectID=IP.ProjectID join tblCustomers C on C.CustomerID=P.CustomerID left outer join tblFPTransType FTP on FTP.FPTransTypeID=IP.InvoicePayMethodID left outer join tblEmployees E on E.EmployeeID=IP.VerifiedBy left outer join tblProjectStatus PS on P.ProjectStatusID=Ps.ProjectStatusID where ((IP.InvoicePayMethodID='" + ddlpayby.SelectedValue.ToString() + "') or  '" + ddlpayby.SelectedValue.ToString() + "'=0) and ((IP.VerifiedBy = '" + ddlVerifiedBy.SelectedValue + "') or '" + ddlVerifiedBy.SelectedValue + "'=0) and ((P.InvoiceNumber = '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "')or '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "'=0) and ((upper(P.Project) like '%'+'" + System.Security.SecurityElement.Escape(txtproject.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtproject.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and ((upper(C.Customer) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchCustomer.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchCustomer.Text) + "'='') and isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') <=  CASE WHEN '" + ddlDate.SelectedValue + "'=1 THEN P.InstallCompleted else  CASE WHEN '" + ddlDate.SelectedValue + "'=2 then IP.InvoicePayDate else  CASE WHEN '" + ddlDate.SelectedValue + "'=3 then IP.Verified else CASE WHEN '" + ddlDate.SelectedValue + "'=4 then IP.ActualPayDate else CASE WHEN '" + ddlDate.SelectedValue + "'=5 then P.InstallBookingDate else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'')end end end end END and ((convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "') >=CASE WHEN '" + ddlDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlDate.SelectedValue + "'=2 then IP.InvoicePayDate else CASE WHEN '" + ddlDate.SelectedValue + "'=3 then IP.Verified else CASE WHEN '" + ddlDate.SelectedValue + "'=4 then IP.ActualPayDate else CASE WHEN '" + ddlDate.SelectedValue + "'=5 then P.InstallBookingDate else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'') end end end end END) or (isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'')='')) and (IP.IsVerified='" + ddlIsVerified.SelectedValue + "' or (isnull('" + ddlIsVerified.SelectedValue + "',2)=2)) and ((Convert(nvarchar(200),P.FinanceWithID) in (select  * from Split('" + fin + "',',')))or '" + fin + "'='') order by IP.InvoicePayDate DESC";
        //OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(InvoicePaymentID) from tblInvoicePayments ) end) ROWS ONLY ;";
        //Response.Write(data1);
        //Response.End();
        dt = ClstblCustomers.query_execute(data1);
    
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_GetDataBySearchCount(txtInvSearch.Text, txtproject.Text, ddlpayby.SelectedValue.ToString(), ddlSearchState.SelectedValue.ToString(), txtStartDate.Text, txtEndDate.Text, ddlVerifiedBy.SelectedValue, txtSearchCustomer.Text, ddlIsVerified.SelectedValue, ddlDate.SelectedValue,fin);
        if (dtCount.Rows.Count > 0)
        {
            string InvTotal= dtCount.Rows[0]["TotalQuotePrice"].ToString();
            string TotalAmount= dtCount.Rows[0]["InvoicePayTotal"].ToString();
            if (!string.IsNullOrEmpty(InvTotal) || !string.IsNullOrEmpty(TotalAmount))
            {
                lblInvTotal.Text = InvTotal;
                lblTotalAmount.Text = TotalAmount;
            }
            else
            {
                lblInvTotal.Text = "0";
                lblTotalAmount.Text = "0";
            }
        }

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;

                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }


    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);

        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }


    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "comment")
        {
            hndinvoicepaymentid.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();
            divAddComment.Visible = true;
        }
        //if (e.CommandName.ToLower() == "paid")
        //{

        //    hndinvoicepaymentid.Value = e.CommandArgument.ToString();
        //    ModalPopupExtender1.Show();
        //    divPaiddate.Visible = true;
        //    divAddComment.Visible = true;
        //}

        if (e.CommandName.ToLower() == "revert")
        {
            ModalPopupExtenderDelete.Show();
            divAddComment.Visible = true;
            string InvoicePaymentID = e.CommandArgument.ToString();
            hdndelete.Value = InvoicePaymentID;

            ////bool suc_1 = ClstblInvoicePayments.tblInvoicePayments_UpdateVerified(InvoicePaymentID, stEmp.EmployeeID, "", "0");
        }
    }

    protected void ibtnAddComment_Onclick(object sender, EventArgs e)
    {

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string VerifiedBy = stEmp.EmployeeID;
        string Comment = txtComment.Text;
        string InvoicePaymentID = hndinvoicepaymentid.Value;
        bool suc = ClstblInvoicePayments.tblInvoicePayments_UpdateVerified(InvoicePaymentID, VerifiedBy, Comment, "1");

        ClstblInvoicePayments.tblInvoicePayments_UpdateActualPayDate(InvoicePaymentID, txtActualPayDate.Text);
        if (suc)
        {
            ModalPopupExtender2.Hide();
            divAddComment.Visible = false;
            txtComment.Text = string.Empty;
        }
        BindGrid(0);
        //BindScript();
    }
    protected void ibtnCancelComment_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        divAddComment.Visible = false;
    }
    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    txtInvSearch.Text = string.Empty;
    //    txtproject.Text = string.Empty;
    //    ddlSearchState.SelectedValue = "";
    //    ddlpayby.SelectedValue = "";
    //    ddlVerifiedBy.SelectedValue = "";
    //    ddlIsVerified.SelectedValue = "";
    //    txtSearchCustomer.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtStartDate.Text = string.Empty;

    //    BindGrid(0);
    //    //BindScript();
    //}
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string fin = "";
        DataTable dtfin = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        if (dtfin.Rows.Count > 0)
        {
            if (ddlfinancewith.SelectedValue.ToString() == "1")
            {
                fin = "1";
            }
            if (ddlfinancewith.SelectedValue.ToString() == "2")
            {
                foreach (DataRow dr in dtfin.Rows)
                {
                    if (dr["FinanceWithID"].ToString() != "1")
                    {
                        fin += dr["FinanceWithID"] + ",";
                    }
                }
            }
        }

        DataTable dt = ClstblInvoicePayments.tblInvoicePayments_GetDataBySearch(txtInvSearch.Text, txtproject.Text, ddlpayby.SelectedValue.ToString(), ddlSearchState.SelectedValue.ToString(), txtStartDate.Text, txtEndDate.Text, ddlVerifiedBy.SelectedValue, txtSearchCustomer.Text, ddlIsVerified.SelectedValue, ddlDate.SelectedValue, fin);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "InvoicePaid" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 11, 9, 23, 14, 18, 8, 2, 13, 16, 6, 4, 15, 17, 19, 20 ,21,24,22};
            string[] arrHeader = { "Customer", "Project", "Inv No.", "Date Paid", "Actual Pay Date",
                "Inv Total", "Amnt Paid", "Paid By", "Verified", "VerifiedByName", "Payment Notes",
                "ReceiptNumber", "Install Booking Date", "Surcharge", "Project Status","Install State"
            ,"Sales Rep","Sales Team"};
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }


    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtInvSearch.Text = string.Empty;
        txtproject.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        ddlpayby.SelectedValue = "";
        ddlIsVerified.SelectedValue = "";
        ddlVerifiedBy.SelectedValue = "";
        ddlIsVerified.SelectedValue = "";
        ddlDate.SelectedValue = "";
        txtSearchCustomer.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        ddlfinancewith.SelectedValue = "";
        ddlIsVerified.SelectedValue = "2";

        BindGrid(0);
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }

    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //HiddenField hdnInvoicePaymentID = (HiddenField)e.Row.FindControl("hdnInvoicePaymentID");
            Label ltSCharge = (Label)e.Row.FindControl("ltSCharge");
            Label ltTotal = (Label)e.Row.FindControl("ltTotal");
            HiddenField hndPayTotal = (HiddenField)e.Row.FindControl("hndPayTotal");
            HiddenField hndSChange = (HiddenField)e.Row.FindControl("hndSChange");
            //SttblInvoicePayments stpay = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(hdnInvoicePaymentID.Value);
            //decimal scharge = 0;
            //if (stpay.CCSurcharge != string.Empty)
            //{
            //    scharge = Convert.ToDecimal(stpay.CCSurcharge);
            //}
            //ltSCharge.Text = string.Format("{0:0.00}", Convert.ToDecimal(scharge));

            decimal totalamt = 0;
            if (hndPayTotal.Value != string.Empty)
            {
                totalamt = Convert.ToDecimal(hndPayTotal.Value);
            }
            decimal scharge = 0;
            if (hndSChange.Value != string.Empty)
            {
                scharge = Convert.ToDecimal(hndSChange.Value);
            }

            ltTotal.Text = string.Format("{0:0.00}", Convert.ToDecimal(totalamt + scharge));
        }
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string InvoicePaymentID = hdndelete.Value;

        SttblInvoicePayments stInvP = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(InvoicePaymentID);
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        bool suc = ClstblInvoicePayments.tblInvoicePayments_UpdateVerified(InvoicePaymentID, "", "", "0");
        ClstblInvoicePayments.tblInvoicePayments_UpdateVerifiedDate(InvoicePaymentID, "");//tblInvoicePayments_UpdateVerifiednull
        ClstblInvoicePayments.tblInvoicePayments_UpdateVerifiednull(InvoicePaymentID, "");
        BindGrid(0);
    }

    //protected void ibtnCancel_Click(object sender, EventArgs e)
    //{
    //    ModalPopupExtender1.Hide();
    //    divPaiddate.Visible = false;
    //}

    //protected void ibtnAddPAidDate_Click(object sender, EventArgs e)
    //{
    //    string InvoicePaymentID = hndinvoicepaymentid.Value;
    //    bool suc = ClstblInvoicePayments.tblInvoicePayments_UpdatePaidDate(InvoicePaymentID, txtpaiddate.Text, txtpaidcomment.Text);


    //    if (suc)
    //    {
    //        ModalPopupExtender1.Hide();
    //        divPaiddate.Visible = false;
    //        txtpaidcomment.Text = string.Empty;
    //        txtpaiddate.Text = string.Empty;
    //    }
    //    BindGrid(0);

    //}
}