using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_invoice_updateinvcomm : System.Web.UI.Page
{
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string ProjectID = Request.QueryString["id"];
            string InvType = Request.QueryString["type"];
            try
            {
                SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType(InvType, ProjectID);
                txtInstallerInvNo.Text = st.InvNo;
                txtInstallerAmnt.Text = SiteConfiguration.ChangeCurrencyVal(st.InvAmnt);
                txtInstallerInvDate.Text = Convert.ToDateTime(st.InvDate).ToShortDateString();
                txtInstallerPayDate.Text = Convert.ToDateTime(st.PayDate).ToShortDateString();
                txtElectricianInvoiceNotes.Text = st.Notes;

                if (st.InvDoc != string.Empty)
                {
                    lblInvDoc.Visible = true;
                    lblInvDoc.Text = st.InvDoc;
                   // lblInvDoc.NavigateUrl = "~/userfiles/commoninvdoc/" + st.InvDoc;
                    //lblInvDoc.NavigateUrl = pdfURL + "commoninvdoc/" + st.InvDoc;
                    lblInvDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("commoninvdoc", st.InvDoc);
                    RequiredFieldValidatorInvDoc.Visible = false;
                }
                else
                {
                    lblInvDoc.Visible = false;
                    RequiredFieldValidatorInvDoc.Visible = true;
                }

                ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
                ddlInstaller.DataValueField = "ContactID";
                ddlInstaller.DataTextField = "Contact";
                ddlInstaller.DataMember = "Contact";
                ddlInstaller.DataBind();
            }
            catch { }
        }
    }

    public void BindDoc()
    {
        string ProjectID = Request.QueryString["id"];
        string InvType = Request.QueryString["type"];
        SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType(InvType, ProjectID);

        lblInvDoc.Visible = true;
        lblInvDoc.Text = st.InvDoc;
        //lblInvDoc.NavigateUrl = "~/userfiles/commoninvdoc/" + st.InvDoc;
        //lblInvDoc.NavigateUrl = pdfURL + "commoninvdoc/" + st.InvDoc;
        lblInvDoc.NavigateUrl = SiteConfiguration.GetDocumnetPath("commoninvdoc", st.InvDoc);
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        RequiredFieldValidator4.Visible = false;
        string ProjectID = Request.QueryString["id"];
        string InvType = Request.QueryString["type"];

        SttblInvoiceCommon st = ClstblInvoiceCommon.tblInvoiceCommon_SelectByInvType(InvType, ProjectID);
        string InvNo = txtInstallerInvNo.Text;
        string InvAmnt = txtInstallerAmnt.Text;
        string InvDate = txtInstallerInvDate.Text;
        string PayDate = txtInstallerPayDate.Text;
        string Notes = txtElectricianInvoiceNotes.Text;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        if (st.InvDoc != string.Empty)
        {
            RequiredFieldValidatorInvDoc.Visible = false;
        }
        else
        {
            RequiredFieldValidatorInvDoc.Visible = true;
        }

        bool success = ClstblInvoiceCommon.tblInvoiceCommon_Update(st.InvoiceID, InvType, ProjectID, InvNo, InvAmnt, InvDate, PayDate, Notes, UpdatedBy, "", "", "", ddlInstaller.SelectedValue);
        if (fuInvDoc.HasFile)
        {
            SiteConfiguration.DeletePDFFile("commoninvdoc", st.InvDoc);
            string InvDoc = st.InvoiceID + fuInvDoc.FileName;
            fuInvDoc.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDoc);
            bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(st.InvoiceID, InvDoc);
         	SiteConfiguration.UploadPDFFile("commoninvdoc", InvDoc);
            SiteConfiguration.deleteimage(InvDoc, "commoninvdoc");
        }
        //else
        //{
        //    bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(st.InvoiceID, st.InvDoc);
        //}
        if (success)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
        Response.Redirect("/admin/adminfiles/company/mtcecall.aspx");
        BindDoc();
       
    }
    protected void lbtnBack_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["type"] == "4")
        {
            Response.Redirect("~/admin/adminfiles/company/mtcecall.aspx");
        }

        if (Request.QueryString["type"] == "5")
        {
            Response.Redirect("~/admin/adminfiles/company/casualmtce.aspx");
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}