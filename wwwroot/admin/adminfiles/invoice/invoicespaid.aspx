<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="invoicespaid.aspx.cs" Inherits="admin_adminfiles_invoice_invoicespaid" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>

  <%--  <script type="text/javascript">

        $(document).ready(function () {
            doMyAction();
        });



        function doMyAction() {
            HighlightControlToValidate();
            $('#<%=ibtnAddComment.ClientID %>').click(function () {
                formValidate();
            });

            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $(".myvalinvoicepaid").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });

        }
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }

    </script>--%>
     <style>
        .nowrap {
            white-space: normal!important;
        }

        .tooltip {
            z-index: 999999;
        }
    </style>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }

        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>



    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Invoices Paid</h5>
    </div>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="GridView1" />
        </Triggers>
    </asp:UpdatePanel>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                    //$('.loading-container').css('display', 'none');
                    //if (args.get_error() != undefined) {
                    //    args.set_errorhandled(wtrue);
                    //}

                }
                function pageLoaded() {

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    //alert($(".search-select").attr("class"));
                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();

                    $(".myvalinvoicepaid").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }
                }
            </script>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="content animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>


                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                            <tr>
                                                <td>

                                                    <div class="inlineblock martop5">
                                                        <div>
                                                            <div class="input-group col-sm-1">
                                                                <asp:TextBox ID="txtInvSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtInvSearch"
                                                                    WatermarkText="Invoice" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtInvSearch" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetInvoiceNumber"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                    ControlToValidate="txtInvSearch" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </div>

                                                            <div class="input-group col-sm-1">
                                                                <asp:TextBox ID="txtSearchCustomer" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchCustomer"
                                                                    WatermarkText="Customer" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtSearchCustomer" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            </div>
                                                            <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                                <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid" Width="100%">
                                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1" id="div1" runat="server">
                                                                <asp:DropDownList ID="ddlpayby" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                    <asp:ListItem Value="">Pay By</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1" id="div3" runat="server" >
                                                                <asp:DropDownList ID="ddlIsVerified" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                    <asp:ListItem Value="2">Verified</asp:ListItem>
                                                                    <asp:ListItem Value="1">Only Verified</asp:ListItem>
                                                                    <asp:ListItem Value="0" Selected="True">Not Verified</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1" id="div2" runat="server">
                                                                <asp:DropDownList ID="ddlVerifiedBy" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                    <asp:ListItem Value="">Verified By</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>


                                                            <div class="input-group col-sm-1" id="divproject" runat="server" visible="false">
                                                                <asp:TextBox ID="txtproject" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtproject"
                                                                    WatermarkText="Project Name" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtproject" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            </div>
                                                            <div class="input-group col-sm-1" id="div4" runat="server">
                                                                <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                    <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                    <asp:ListItem Value="1">Invoice Date</asp:ListItem>
                                                                    <asp:ListItem Value="2">DatePaid</asp:ListItem>
                                                                    <asp:ListItem Value="3">VerifiedDate</asp:ListItem>
                                                                    <asp:ListItem Value="4">Actual Date</asp:ListItem>
                                                                    <asp:ListItem Value="5">InstallBooking Date</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group date datetimepicker1 col-sm-1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtStartDate"
                                                                    WatermarkText="Start Date" />
                                                            </div>

                                                            <div class="input-group date datetimepicker1 col-sm-1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control m-b"></asp:TextBox>
                                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                            </div>


                                                            <div class="input-group col-sm-1" id="div5" runat="server">
                                                                <asp:DropDownList ID="ddlfinancewith" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinvoicepaid">
                                                                    <asp:ListItem Value="">Finance Co.</asp:ListItem>
                                                                    <asp:ListItem Value="1">Cash</asp:ListItem>
                                                                    <asp:ListItem Value="2">Finance</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group">
                                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                            </div>
                                                            <div class="input-group">
                                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>

                                                   <%-- <div class="inlineblock martop5">
                                                        <div>
                                                            
                                                            

                                                        </div>
                                                    </div>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="datashowbox">
                                <div class="row">
                                    <div class="dataTables_length showdata  col-sm-6">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="tdExport" class="pull-right" runat="server" style="padding-top:8px">
                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            <%-- </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
                <asp:Panel ID="panel" runat="server" CssClass="hpanel marbtm15">
                    <div class="widget-body shadownone brdrgray">
                        <div class="panel-body padallzero">
                            <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="paddingleftright10" style="font-size:medium">
                                                    <b>Inv Total:&nbsp;</b><asp:Literal ID="lblInvTotal" runat="server"></asp:Literal>
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;</td>
                                                <td class="paddingleftright10" style="font-size:medium">
                                                    <b>Total Amount Paid:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panel1" runat="server" CssClass="hpanel finalgrid">
                    <div class="">
                        <div id="PanGrid" runat="server">
                            <div>
                                <div class="table-responsive xscroll">
                                    <asp:GridView ID="GridView1" DataKeyNames="InvoicePaymentID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1"
                                        OnRowCommand="GridView1_RowCommand" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="20px">
                                                <ItemTemplate>

                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("InvoicePaymentID") %>','tr<%# Eval("InvoicePaymentID") %>');">
                                                        <img id='imgdiv<%# Eval("InvoicePaymentID") %>' src="../../../images/icon_plus.png" />
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdspecialclass spiclwithnew"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label" runat="server" Width="175px" Style="display:initial">
                                                        <asp:HyperLink ID="lnkCustomer" CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/company/CustomerNew.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%></asp:HyperLink>

                                                        <div class="contacticonedit">
                                                            <asp:HyperLink ID="HyperLink11" runat="server" CssClass="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Project" Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                                       <i class="fa fa-link"></i> Detail
                                                            </asp:HyperLink>
                                                        </div>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Project">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Width="250px">
                                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("Project")%></asp:HyperLink>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Inv No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="ProjectNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Width="60px">
                                                        <%#Eval("InvoiceNumber")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InvoicePayDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Width="80px">
                                                        <%#Eval("InvoicePayDate","{0:dd MMM yyyy}")%></asp:Label>
                                                 <%--   <asp:LinkButton ID="lbtnPaid" CommandName="Paid" CssClass="btn btn-sky btn-xs" Visible="false"
                                                        CommandArgument='<%#Eval("InvoicePaymentID") %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Paid Date">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>--%>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actual Pay" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="ActualPayDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label55" runat="server" Width="90px">
                                                               
                                                               <%#Eval("ActualPayDate","{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Inv Total" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                                ItemStyle-HorizontalAlign="Right" SortExpression="TotalQuotePrice">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="80px">
                                                        <asp:HiddenField ID="hndPrice" runat="server" Value='<%#Eval("TotalQuotePrice")%>' />
                                                        <%#Eval("TotalQuotePrice","{0:0.00}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amnt Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                                ItemStyle-HorizontalAlign="Right" SortExpression="InvoicePayTotal">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Width="100px">
                                                        <asp:HiddenField ID="hndPayTotal" runat="server" Value='<%#Eval("InvoicePayTotal")%>' />
                                                        <asp:HiddenField ID="hdnInvoicePaymentID" runat="server" Value='<%#Eval("InvoicePaymentID")%>' />
                                                        <%#Eval("InvoicePayTotal","{0:0.00}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="S/Chg" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right" SortExpression="ltSCharge"
                                                ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="ltSCharge" runat="server" Width="70px"><%#Eval("CCSurcharge","{0:0.00}")%>
                                                        <asp:HiddenField ID="hndSChange" runat="server" Value='<%#Eval("CCSurcharge")%>' />
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Amnt" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right" SortExpression="ltTotal"
                                                ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="ltTotal" runat="server" Width="80px">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Paid By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                                ItemStyle-HorizontalAlign="Right" SortExpression="FPTransType">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" Width="100px">
                                                        <%#Eval("FPTransType")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Verified" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SortExpression="Verified">
                                                        <ItemTemplate>
                                                            <%#Eval("Verified").ToString() == "" ? "-" : Eval("Verified","{0:dd MMM yyyy}")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Verified By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="VerifiedByName">
                                                        <ItemTemplate>
                                                            
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left" SortExpression="PaymentNote">
                                                        <ItemTemplate>
                                                             
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                            <%--<asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderText="Check"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                           
                                                            
                                                             <div class="form-group dateimgarea" id="divAdvDate" runat="server">
                                                            <span class="dateimg">
                                                            <asp:TextBox ID="txtActualPay" runat="server" CssClass="form-control" Width="100px" stype="border: none!important;"></asp:TextBox>
                                                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                    <cc1:calendarextender id="CalendarExtender5" runat="server" popupbuttonid="ImageButton2" targetcontrolid="txtActualPay" format="dd/MM/yyyy">
                                                                    </cc1:calendarextender>

                                                                    <asp:RegularExpressionValidator ValidationGroup="elecinv" ControlToValidate="txtActualPay" ID="RegularExpressionValidator13" runat="server" ErrorMessage="Enter valid date"
                                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                </span>
                                                                </div>
                                                       </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                            <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderText=""
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label10" runat="server" Width="80px">

                                                        <asp:LinkButton ID="lbtnRevert" CommandName="Revert" Visible='<%#Eval("IsVerified").ToString()=="True"?true:false %>' CssClass="btn btn-maroon btn-xs"
                                                            CommandArgument='<%#Eval("InvoicePaymentID") %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Revert">
                                                          <i class="fa fa-retweet"></i> Revert </asp:LinkButton>

                                                        <asp:HiddenField ID="hndID" runat="server" Value='<%#Eval("InvoicePaymentID")%>' />
                                                        <asp:LinkButton ID="lbtnVerified" CommandName="Comment" Visible='<%#Eval("IsVerified").ToString()=="False"?true:false %>' CssClass="btn btn-sky btn-xs"
                                                            CommandArgument='<%#Eval("InvoicePaymentID") %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Verify">
                                                           <i class="fa fa-check-square"></i> Verify</asp:LinkButton>



                                                    </asp:Label>

                                                    <%--   <asp:LinkButton ID="lbtnRevert" CommandName="Revert" Visible='<%#Eval("IsVerified").ToString()=="True"?true:false %>' CssClass="btn btn-maroon btn-xs"
                                                    CommandArgument='<%#Eval("InvoicePaymentID") %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Revert"
                                                    OnClientClick="return ComfirmDelete(event,this)">
                                                               <i class="fa fa-retweet"></i> Revert</asp:LinkButton>--%>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                <ItemTemplate>
                                                    <tr id='tr<%# Eval("InvoicePaymentID") %>' style="display: none;" class="dataTable GridviewScrollItem">
                                                        <td colspan="98%" class="details">
                                                            <div id='div<%# Eval("InvoicePaymentID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                    <tr class="GridviewScrollItem">
                                                                        <td width="180px"><b>Verified</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label7" runat="server" Width="100px">
                                                        <%#Eval("Verified").ToString() == "" ? "-" : Eval("Verified","{0:dd MMM yyyy}")%></asp:Label>
                                                                        </td>
                                                                        <td width="180px">
                                                                            <b>Verified By</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label8" runat="server" Width="100px">
                                                        <%#Eval("VerifiedByName").ToString() == "" ? "-" : Eval("VerifiedByName")%></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <b>Notes</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label9" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("PaymentNote")%>' CssClass="tooltipwidth"
                                                                                Width="250px"><%#Eval("PaymentNote")%></asp:Label>
                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle />
                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                                    </asp:GridView>


                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

            </div>

        
            

    <%--updatepanel--%>

    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="divAddComment" TargetControlID="btnNULLComment" CancelControlID="ibtnCancelComment">
    </cc1:ModalPopupExtender>

    <div id="divAddComment" runat="server" visible="false" style="display: none;" class="modal_popup">
        <div class="modal-dialog" style="width: 400px;">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                     <div style="float: right">
                    <asp:LinkButton ID="ibtnCancelComment" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal"
                        OnClick="ibtnCancelComment_Onclick">Close
                    </asp:LinkButton>
                         </div>
                    <h4 class="modal-title" id="myModalLabel">Add Comment</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline">

                            <div class="form-group marginbtm10 row">
                                <asp:Label ID="Label23" runat="server" class="col-sm-4 control-label" Style="padding: -right:0px;">
                                                <strong>Actual Date</strong></asp:Label>

                                <%-- <div class="input-group date datetimepicker1 col-sm-3">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                    <asp:TextBox ID="txtActualPayDate" runat="server" class="form-control" placeholder="Actual Pay Date">
                                    </asp:TextBox>
                                  
                                </div>--%>
                                <div class="col-sm-8">

                                    <div class="input-group date datetimepicker1 col-sm-12">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                        <asp:TextBox ID="txtActualPayDate" placeholder="Actual Date" runat="server" class="form-control"></asp:TextBox>
                                        <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtActualPayDate"
                                            WatermarkText="Actual Date" />--%>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group marginbtm10 row">
                                <asp:Label ID="Label16" runat="server" class="col-sm-4 control-label" Style="padding: -right:0px;">
                                                <strong>Comment</strong></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtComment" TextMode="MultiLine" Rows="3" Columns="5" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtComment"
                                        ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-12 center-text">
                                <asp:Button ID="ibtnAddComment" runat="server" Text="Update" OnClick="ibtnAddComment_Onclick"
                                    ValidationGroup="AddNotes" CssClass="btn btn-primary savewhiteicon btnsaveicon" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <asp:Button ID="btnNULLComment" Style="display: none;" runat="server" />



 

    <asp:HiddenField ID="hndinvoicepaymentid" runat="server" />


    <asp:Button ID="btndelete" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="modal_danger" CancelControlID="lnkcancel" TargetControlID="btndelete">
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message">

        <div class="modal-dialog" style="margin-top: -300px">
            <div class=" modal-content ">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>


                <div class="modal-title">Revert</div>
                <label id="ghh" runat="server"></label>
                <div class="modal-body ">Are you sure you want to Revert this Record?</div>
                <div class="modal-footer " style="text-align: center">
                    <asp:LinkButton ID="lnkdelete" runat="server" class="btn btn-danger" OnClick="lnkdelete_Click" CommandName="deleteRow">Yes</asp:LinkButton>
                    <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">No</asp:LinkButton>
                </div>
            </div>
        </div>

    </div>

    <asp:HiddenField runat="server" ID="hdncountdata" />
    <asp:HiddenField ID="hdndelete" runat="server" />

             
           
</asp:Content>


