<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="invoicesissued.aspx.cs" Inherits="admin_adminfiles_invoice_invoicesissued" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/InvoicePayments.ascx" TagName="InvoicePayments"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>
    <style>
        .nowrap {
            white-space: normal!important;
        }

        .tooltip {
            z-index: 999999;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
     
 

    <script>

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress

            $('.loading-container').css('display', 'none');

            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });


            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

        }
        function pageLoaded() {
            //alert($(".search-select").attr("class"));
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();



            //  callMultiCheckbox();


        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Invoices Issued</h5>
        <div id="tdExport" class="pull-right" runat="server">
        </div>
    </div>

    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="content animate-panel" style="padding-bottom: 0px!important;">
                <div class="messesgarea">
                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>
            </div>

            <div class="searchfinal">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <div class="dataTables_filter Responsive-search row">
                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                    <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div class="inlineblock">
                                                    <div class="col-sm-12">
                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtInvSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtInvSearch"
                                                                WatermarkText="Invoice" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtInvSearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetInvoiceNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtInvSearch" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtContactSearch"
                                                                WatermarkText="Contact" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtsuburb" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtsuburb"
                                                                WatermarkText="Suburb" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtsuburb" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>
                                                        <div class="input-group col-sm-1 martop5" id="divCustomer" runat="server">
                                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued" Width="150px">
                                                                <asp:ListItem Value="">State</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <div class=" groupalign ">
                                                                <asp:TextBox ID="TextBox1" runat="server" Width="100%" CssClass="form-control" Text=" Post Code" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtpcodefrom" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtpcodefrom"
                                                                WatermarkText="From" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtpcodefrom" ServicePath="~/Search.asmx" CompletionListCssClass="autocomplete_completionListElement"
                                                                ServiceMethod="GetPostCodeList" EnableCaching="false" CompletionInterval="10"
                                                                CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtpcodeto" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtpcodeto"
                                                                WatermarkText="To" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtpcodeto" ServicePath="~/Search.asmx" CompletionListCssClass="autocomplete_completionListElement"
                                                                ServiceMethod="GetPostCodeList" EnableCaching="false" CompletionInterval="10"
                                                                CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>


                                                        <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlMeterInstaller" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued">
                                                                <asp:ListItem Value="">Meter Installer</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtMQNsearch"
                                                                WatermarkText="Man Qte" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" id="div3" runat="server" style="width: 150px;">
                                                            <asp:DropDownList ID="ddlPaidStatus" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued">
                                                                <asp:ListItem Value="">Paid Status</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>


                                                   <%-- </div>
                                                </div>
                                                <div class="inlineblock">
                                                    <div class="col-sm-12">--%>

                                                        <div class="input-group col-sm-1 martop5" id="div2" runat="server">
                                                            <asp:DropDownList ID="ddlSystemInstaller" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued">
                                                                <asp:ListItem Value="">System Installer</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="form-group spical multiselect martop5" style="width: 142px">
                                                            <dl class="dropdown">
                                                                <dt>
                                                                    <a href="#">
                                                                        <span class="hida" id="spanselect">Select</span>
                                                                        <p class="multiSel"></p>
                                                                    </a>
                                                                </dt>
                                                                <dd id="ddproject" runat="server" class="martop5">
                                                                    <div class="mutliSelect" id="mutliSelect">
                                                                        <ul>
                                                                            <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li>
                                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                        <%--  <span class="checkbox-info checkbox">--%>
                                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                            <span></span>
                                                                                        </label>
                                                                                        <%-- </span>--%>
                                                                                        <label class="chkval">
                                                                                            <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                        </label>
                                                                                    </li>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </div>
                                                                </dd>
                                                            </dl>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" id="div6" runat="server">
                                                            <asp:DropDownList ID="ddlfinancewith" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued">
                                                                <asp:ListItem Value="">Finance Co.</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" id="div7" runat="server">
                                                            <asp:DropDownList ID="ddlpayby" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued">
                                                                <asp:ListItem Value="">Pay By</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        
                                                        <div class="input-group col-sm-1 martop5" runat="server">
                                                            <asp:DropDownList ID="ddlArea" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued">
                                                                <asp:ListItem Value="">Area</asp:ListItem>
                                                                <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                <asp:ListItem Value="2">Regional</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div id="Div4" class="input-group col-sm-1 martop5" runat="server" visible="false">
                                                            <asp:TextBox ID="txtSearchCompanyNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtSearchCompanyNo"
                                                                WatermarkText="CompanyNumber" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearchCompanyNo" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtSearchCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" id="div5" runat="server">
                                                            <asp:DropDownList ID="ddlSelectDate" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinvoiceissued">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">Invoice Date</asp:ListItem>
                                                                <asp:ListItem Value="5">Installed Date</asp:ListItem>
                                                                <asp:ListItem Value="2">Meter Installed</asp:ListItem>
                                                                <asp:ListItem Value="3">Deposit Received</asp:ListItem>
                                                                <asp:ListItem Value="4">Installation Complete</asp:ListItem>
                                                                
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control m-b "></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtStartDate"
                                                                WatermarkText="Start Date" />
                                                        </div>

                                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control m-b"></asp:TextBox>
                                                            <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                            <%--   <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                        </div>
                                                        <div class="input-group martop5">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn redreq btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="input-group martop5">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanSearch" runat="server">
                            <div class="row">
                                <div class="dataTables_length showdata col-sm-6">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="padtopzero">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div id="Div8" class="pull-right" runat="server" style="padding-top:8px">
                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                            CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                        <%-- </ol>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="panel" runat="server" CssClass="hpanel marbtm15">
            <div class="widget-body shadownone brdrgray">
                <div class="panel-body padallzero">
                    <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="paddingleftright10" style="font-size:medium">
                                            <b>Total Amount:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="paddingleftright10" style="font-size:medium">
                                            <b>Total Amount Received.:&nbsp;</b><asp:Literal ID="lblTotalDepRec" runat="server"></asp:Literal>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="paddingleftright10" style="font-size:medium">
                                            <b>Balance Owing:&nbsp;</b><asp:Literal ID="lblBalOwing" runat="server"></asp:Literal>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td id="Td1" runat="server" visible="false" style="font-size:medium">
                                            <b>Total Number of panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </asp:Panel>
        <asp:Panel ID="panel1" runat="server" CssClass="hpanel padtopzero finalgrid">
            <div id="PanGrid" runat="server">
                <div class="table-responsive">
                    <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                        OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowSorting="true" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                        <Columns>

                            <asp:TemplateField ItemStyle-Width="60px">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                        <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inv #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdspecialclass spiclwithnew" SortExpression="InvoiceNumber"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblProject" runat="server" Width="120px" Style="display:initial">
                                        <asp:LinkButton ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Invoice Payments"
                                            CausesValidation="false" CommandName="viewproject" CommandArgument='<%#Eval("ProjectID")%>'><%#Eval("InvoiceNumber")%></asp:LinkButton>
                                        
                                        <div class="contacticonedit martopminusnine">
                                            <div class="contacticonedit ">
                                                <asp:HyperLink ID="HyperLink11" runat="server" CssClass="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                                       <i class="fa fa-link"></i> Detail
                                                </asp:HyperLink>
                                            </div>
                                            <%-- <asp:HyperLink ID="HyperLink1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><img src="../../../images/icon_detail.png" /></asp:HyperLink>--%>
                                        </div>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="InstallState"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="LabelState" runat="server" Width="35px">
                                                <%#Eval("InstallState")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Man Qte" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Width="70px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Issued" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label3" runat="server" Width="90px"><%#Eval("InvoiceSent", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Finance" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="FinanceWith"
                                ItemStyle-HorizontalAlign="Left"> 
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Width="80px"><%#Eval("FinanceWith")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Proj Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ProjectStatus"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Width="100px"><%#Eval("ProjectStatus")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Contact"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Width="120px"><%#Eval("Contact")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="ContMobile"
                                ItemStyle-HorizontalAlign="left" >
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Width="80px"><%#Eval("ContMobile")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" SortExpression="InstallerName">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Width="120px"><%#Eval("InstallerName")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Installed" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallBookingDate"
                                ItemStyle-HorizontalAlign="left" >
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Width="80px">
                                                            <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="right-text"
                                ItemStyle-HorizontalAlign="Right" SortExpression="TotalQuotePrice">
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Width="60px"><%#Eval("TotalQuotePrice","{0:0.00}")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Owing" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="right-text" SortExpression="BalOwing"
                                ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="Labelbalo" runat="server" Width="50px">
                                                        <%#Eval("BalOwing","{0:0.00}")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="InvoicePaid"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Width="80px"><%#Eval("InvoicePaid", "{0:dd MMM yyyy}")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--  <asp:TemplateField HeaderText="O/Due" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelOdue" runat="server" Width="40px"><%#Eval("Odue")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>

                            <%--<asp:TemplateField HeaderText="Date Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label14" runat="server" Width="80px"><%#Eval("InvoicePaid", "{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%--<asp:TemplateField HeaderText="Follow Up" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%--  <asp:TemplateField HeaderText="Invoices Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label17" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InvoiceNotes")%>' CssClass="tooltipwidth"
                                                                Width="300px"><%#Eval("InvoiceNotes")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%--  <asp:TemplateField HeaderText="Installer Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label18" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>' CssClass="tooltipwidth"
                                                                Width="300px"><%#Eval("InstallerNotes")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%--<asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%--  <asp:TemplateField HeaderText="Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPanels" runat="server" Width="50px"><%#Eval("NumberPanels")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%--  <asp:TemplateField HeaderText="Inverter" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInverter" runat="server" Width="150px"><%#Eval("InverterDetailsName")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%-- <asp:TemplateField HeaderText="Dep Req" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Right" SortExpression="DepositRequired">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label10" runat="server" Width="80px"><%#Eval("DepositRequired", "{0:0.00}")%></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                            <%--<asp:TemplateField HeaderText="Dep Rec" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label11" runat="server" Width="90px"><%#Eval("DepositReceived","{0:dd MMM yyyy}")%></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>--%>

                            <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                <ItemTemplate>
                                    <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                        <td colspan="98%" class="details">
                                            <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                <table width="100%" class="table table-bordered table-hover">

                                                    <tr>
                                                        <td width="8%"><b>Invoices Notes</b>
                                                        </td>
                                                        <td width="24%">

                                                            <asp:Label ID="Label6" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InvoiceNotes")%>' CssClass="tooltipwidth nowrap"><%#Eval("InvoiceNotes")%></asp:Label>

                                                        </td>
                                                        <td width="8%"><b>Installer Notes</b>
                                                        </td>
                                                        <td width="24%">


                                                            <asp:Label ID="Label13" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>' CssClass="tooltipwidth nowrap"><%#Eval("InstallerNotes")%></asp:Label>
                                                        </td>
                                                        <td width="8%"><b>Panels</b>
                                                        </td>
                                                        <td width="24%">

                                                            <asp:Label ID="Label15" runat="server"><%#Eval("NumberPanels")%></asp:Label>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Inverter</b>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label19" runat="server"><%#Eval("InverterDetailsName")%></asp:Label>
                                                        </td>



                                                        <td><b>Dep Req</b>
                                                        </td>
                                                        <td>

                                                            <asp:Label ID="Label20" runat="server"><%#Eval("DepositRequired", "{0:0.00}")%></asp:Label>

                                                        </td>
                                                        <td><b>Dep Rec</b>
                                                        </td>
                                                        <td>


                                                            <asp:Label ID="Label21" runat="server"><%#Eval("DepositReceived","{0:dd MMM yyyy}")%></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><b>Man Qte</b>
                                                        </td>
                                                        <td>

                                                            <asp:Label ID="Label2" runat="server"><%#Eval("ManualQuoteNumber")%></asp:Label>

                                                        </td>
                                                        <td><b>O/Due</b>
                                                        </td>
                                                        <td>


                                                            <asp:Label ID="LabelOdue" runat="server"><%#Eval("Odue")%></asp:Label>
                                                        </td>


                                                        <td><b>Email</b></td>
                                                        <td>
                                                            <asp:Label ID="lblEmail" runat="server" Width="200px"><%#Eval("ContEmail")%></asp:Label></td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <b>Follow Up</b>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label16" runat="server" Width="90px"> <%#Eval("FollowUp","{0:dd MMM yyyy}")%></asp:Label>
                                                        </td>

                                                        <%--<td>
                                                                                    <b>Owing</b>
                                                                                </td>
                                                                                <td><asp:Label ID="Labelbalo" runat="server" Width="80px">
                                                        <%#Eval("BalOwing","{0:0.00}")%></asp:Label></td>
                                                                                <td>
                                                                                    <b>Date Paid</b>
                                                                                </td>
                                                                                <td><asp:Label ID="Label14" runat="server" Width="80px"><%#Eval("InvoicePaid", "{0:dd MMM yyyy}")%></asp:Label></td>--%>
                                                    </tr>

                                                    <%--<tr>
                                                                        
                                                                        <td>
                                                                         <asp:Label ID="Label17" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InvoiceNotes")%>' CssClass="tooltipwidth"
                                                                Width="102px"><%#Eval("InvoiceNotes")%></asp:Label>
                                                                          </td>
                                                                        <td>
                                                                         <asp:Label ID="Label18" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>' CssClass="tooltipwidth"
                                                                Width="69px"><%#Eval("InstallerNotes")%></asp:Label>
                                                                          </td>

                                                                        

                                                                          <td>
                                                                          <asp:Label ID="lblPanels" runat="server" Width="50px"><%#Eval("NumberPanels")%></asp:Label>
                                                                          </td>
                                                                          <td>
                                                                          
                                                                                 <asp:Label ID="lblInverter" runat="server" Width="107px"><%#Eval("InverterDetailsName")%></asp:Label>

                                                                             </td>
                                                                          <td>
                                                                          
                                                                                
                                                                             <asp:Label ID="Label10" runat="server" Width="40px"><%#Eval("DepositRequired", "{0:0.00}")%></asp:Label>
                                                                             </td>

                                                                        <td>
                                                                           <asp:Label ID="Label11" runat="server" Width="73px"><%#Eval("DepositReceived","{0:dd MMM yyyy}")%></asp:Label>
                                                                        </td>


                                                                        
                                                                        
                                                                    </tr>--%>
                                                </table>

                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <AlternatingRowStyle />
                        <PagerTemplate>
                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                            <div class="pagination">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                            </div>
                        </PagerTemplate>
                        <PagerStyle CssClass="paginationGrid" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                    </asp:GridView>


                </div>
                <div class="paginationnew1" runat="server" id="divnopage">
                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                        <tr>
                            <td>
                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>



    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            // gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });
        }
    </script>



    <asp:HiddenField ID="hndProInvID" runat="server" />
    <asp:HiddenField runat="server" ID="hdncountdata" />

    <uc1:InvoicePayments ID="InvoicePayments1" runat="server" Visible="false" />
  </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <%-- <asp:PostBackTrigger ControlID="btnSearch"/> 
          
             <asp:PostBackTrigger ControlID="btnClearAll" /> --%>
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

