﻿using System;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_invoice_invoicesissued : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            custompageIndex = 1;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PostInstaller")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            BindDropDown();
            BindGrid(0);
        }
         
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public void BindDropDown()
    {
        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlfinancewith.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlfinancewith.DataMember = "FinanceWith";
        ddlfinancewith.DataTextField = "FinanceWith";
        ddlfinancewith.DataValueField = "FinanceWithID";
        ddlfinancewith.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectByActive();
        lstSearchStatus.DataBind();

        ddlSystemInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlSystemInstaller.DataValueField = "ContactID";
        ddlSystemInstaller.DataMember = "Contact";
        ddlSystemInstaller.DataTextField = "Contact";
        ddlSystemInstaller.DataBind();

        ddlMeterInstaller.DataSource = ClstblContacts.tblContacts_SelectMeterElec();
        ddlMeterInstaller.DataValueField = "ContactID";
        ddlMeterInstaller.DataMember = "Contact";
        ddlMeterInstaller.DataTextField = "Contact";
        ddlMeterInstaller.DataBind();

        ddlPaidStatus.DataSource = ClsProjectSale.tblInvoiceStatus_Select();
        ddlPaidStatus.DataValueField = "Status";
        ddlPaidStatus.DataMember = "InvoiceStatus";
        ddlPaidStatus.DataTextField = "InvoiceStatus";
        ddlPaidStatus.DataBind();

        try
        {
            ddlPaidStatus.SelectedValue = "3";
        }
        catch(Exception ex)
        { }

        ddlpayby.DataSource = ClsProjectSale.tblFPTransType_getInv();
        ddlpayby.DataValueField = "FPTransTypeID";
        ddlpayby.DataTextField = "FPTransTypeAB";
        ddlpayby.DataMember = "FPTransTypeAB";
        ddlpayby.DataBind();
    }
    public void BindDataCount()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        string enddate = "";
        if (txtEndDate.Text != string.Empty)
        {
            enddate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtEndDate.Text));
        }
        string startdate = "";
        if (txtStartDate.Text != string.Empty)
        {
            startdate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtStartDate.Text));
        }
        string qry = "";
        if (ddlPaidStatus.SelectedValue == "1")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0) and ((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) < P.TotalQuotePrice))";
        }
        else if (ddlPaidStatus.SelectedValue == "2")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) = 0))";
        }
        else if (ddlPaidStatus.SelectedValue == "3")
        {
            qry = "and ((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0)";
        }
        else if (ddlPaidStatus.SelectedValue == "4")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) < 0))";
        }
        else
        {
            qry = "and ((P.InvoiceStatusID='" + ddlPaidStatus.SelectedValue + "') or '" + ddlPaidStatus.SelectedValue + "'=0) ";
        }
        if (!string.IsNullOrEmpty(ddlpayby.SelectedValue))
        {
            qry = qry + "and (P.ProjectID in (select ProjectID from tblInvoicePayments where (InvoicePayMethodID ='" + ddlpayby.SelectedValue + "' or isnull('" + ddlpayby.SelectedValue + "','')='')))";
        }
        string data = "select count(P.ProjectID) as countdata from tblProjects P join tblProjectStatus PS on PS.ProjectStatusID=P.ProjectStatusID join tblContacts C on C.ContactID=P.ContactID join tblContacts CT on CT.ContactID=P.Installer left outer join tblStockItems SI on SI.StockItemID=P.InverterDetailsID join tblFinanceWith F on F.FinanceWithID=P.FinanceWithID where P.InvoiceNumber is not null  and ((P.InvoiceNumber='" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "') or '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "'=0) and ((P.Installer='" + ddlSystemInstaller.SelectedValue + "') or '" + ddlSystemInstaller.SelectedValue + "'=0) and ((P.Electrician='" + ddlMeterInstaller.SelectedValue + "') or '" + ddlMeterInstaller.SelectedValue + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((P.InstallCity='" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "') or '" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'='') and (((upper(C.ContFirst+' '+C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContFirst) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%')) or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='') and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and ((P.FinanceWithID='" + ddlfinancewith.SelectedValue + "') or '" + ddlfinancewith.SelectedValue + "'=0) and ((P.InstallPostCode>='" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "'='') and ((P.InstallPostCode<='" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and (isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') <= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.InvoiceSent else isnull(Convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') end end END  ) and ((convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "') >= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.InvoiceSent else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'') end end END) or convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "')='') and P.CustomerID in(select CustomerID from tblCustomers where ((CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "') or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0)) " + qry;
        //string data = "select count(P.ProjectID) as countdata from tblProjects P join tblProjectStatus PS on PS.ProjectStatusID=P.ProjectStatusID join tblContacts C on C.ContactID=P.ContactID join tblContacts CT on CT.ContactID=P.Installer left outer join tblStockItems SI on SI.StockItemID=P.InverterDetailsID join tblFinanceWith F on F.FinanceWithID=P.FinanceWithID where P.InvoiceNumber is not null  and ((P.InvoiceNumber='" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "') or '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "'=0) and ((P.Installer='" + ddlSystemInstaller.SelectedValue + "') or '" + ddlSystemInstaller.SelectedValue + "'=0) and ((P.Electrician='" + ddlMeterInstaller.SelectedValue + "') or '" + ddlMeterInstaller.SelectedValue + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((P.InstallCity='" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "') or '" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'='') and (((upper(C.ContFirst+' '+C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContFirst) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%')) or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='') and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and ((P.FinanceWithID='" + ddlfinancewith.SelectedValue + "') or '" + ddlfinancewith.SelectedValue + "'=0) and ((P.InstallPostCode>='" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "'='') and ((P.InstallPostCode<='" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and (isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') <= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.InvoiceSent else isnull(Convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') end end END  ) and ((convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "') >= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.InvoiceSent else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'') end end END) or convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "')='') and P.CustomerID in(select CustomerID from tblCustomers where ((CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "') or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0)) " + qry;
        //Response.Write(data);
        //Response.End();
        DataTable dt = ClstblCustomers.query_execute(data);
        if (dt.Rows.Count > 0)
        {
            countdata = Convert.ToInt32(dt.Rows[0]["countdata"]);
            hdncountdata.Value = countdata.ToString();
        }
        


    }
    protected DataTable GetGridData()
    {
       // BindDataCount();
        //DataTable dt = ClstblProjects.tblProjects_SelectInvoiceIssued();
        DataTable dt = new DataTable();
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
      //  startindex = (custompageIndex - 1) * custompagesize + 1;
        string enddate = "";
        if (txtEndDate.Text != string.Empty)
        {
            enddate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtEndDate.Text));
        }
        string startdate = "";
        if (txtStartDate.Text != string.Empty)
        {
            startdate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtStartDate.Text));
        }
        string qry = "";
        if (ddlPaidStatus.SelectedValue == "1")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0) and ((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) < P.TotalQuotePrice))";
        }
        else if (ddlPaidStatus.SelectedValue == "2")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) = 0))";
        }
        else if (ddlPaidStatus.SelectedValue == "3")
        {
            qry = "and ((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0)";
        }
        else if (ddlPaidStatus.SelectedValue == "4")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) < 0))";
        }
        else
        {
            qry = "and ((P.InvoiceStatusID='" + ddlPaidStatus.SelectedValue + "') or '" + ddlPaidStatus.SelectedValue + "'=0) ";
        }
        if (!string.IsNullOrEmpty(ddlpayby.SelectedValue))
        {
            qry = qry + "and (P.ProjectID in (select ProjectID from tblInvoicePayments where (InvoicePayMethodID ='" + ddlpayby.SelectedValue + "' or isnull('" + ddlpayby.SelectedValue + "','')='')))";
        }
        if (!string.IsNullOrEmpty(ddlArea.SelectedValue))
        {
            qry = qry + "and ((Cust.Area='" + ddlArea.SelectedValue + "') or '" + ddlArea.SelectedValue + "'=0) ";
        }
        startindex = (custompageIndex - 1) * custompagesize + 1;
        string data = "select (select case when datediff(day,P.InstallBookingDate,GETDATE()) is null then 0 else datediff(day,P.InstallBookingDate,GETDATE()) end where (P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0)as Odue,(select P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID))as BalOwing, P.ProjectID,P.CustomerID,P.ContactID,P.InvoiceNumber,P.InstallBookingDate,P.ManualQuoteNumber,P.InvoiceSent,P.InvoiceTag,P.TotalQuotePrice,P.DepositRequired,P.DepositReceived,P.InvoicePaid,P.InvoiceTag,P.FollowUp,P.InvoiceNotes,P.InstallerNotes,P.NumberPanels,C.ContMobile,C.ContEmail,SI.StockItem as InverterDetailsName,PS.ProjectStatus,(select case when C.ContLast is null then C.ContFirst else C.ContFirst+' '+C.ContLast end) as Contact,(select case when CT.ContLast is null then CT.ContFirst else CT.ContFirst+' '+CT.ContLast end) as InstallerName,convert(varchar(50),P.InvoicePaid,6) IPaid,convert(varchar(50),P.InstallBookingDate,6) as IBDate,convert(varchar(50),P.InvoiceSent,6) as ISentDate,convert(varchar(50),P.DepositReceived,6) as DRDate,P.InstallState,F.FinanceWith,ST.SalesTeam as Team,(select case when TE.EmpLast is null then TE.EmpFirst else TE.EmpFirst+' '+TE.EmpLast end) as AssignedTo, ISNULL((select sum(InvoicePayTotal) from tblInvoicePayments where ProjectID = P.ProjectID), 0) as InvoicePayTotal from tblProjects P left join tblProjectStatus PS on PS.ProjectStatusID=P.ProjectStatusID left join tblContacts C on C.ContactID=P.ContactID left join tblContacts CT on CT.ContactID=P.Installer left join tblEmployees as TE on P.EmployeeID=TE.EmployeeID left join tblSalesTeams as ST on ST.SalesTeamID=TE.SalesTeamID left outer join tblStockItems SI on SI.StockItemID=P.InverterDetailsID left join tblFinanceWith F on F.FinanceWithID=P.FinanceWithID left join tblCustomers Cust on Cust.CustomerID = P.CustomerID where P.InvoiceNumber is not null and ((P.InvoiceNumber='" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "') or '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "'=0) and ((P.Installer='" + ddlSystemInstaller.SelectedValue + "') or '" + ddlSystemInstaller.SelectedValue + "'=0) and ((P.Electrician='" + ddlMeterInstaller.SelectedValue + "') or '" + ddlMeterInstaller.SelectedValue + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((P.InstallCity='" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "') or '" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'='') and (((upper(C.ContFirst+' '+C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContFirst) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%')) or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='') and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and ((P.FinanceWithID='" + ddlfinancewith.SelectedValue + "') or '" + ddlfinancewith.SelectedValue + "'=0) and ((P.InstallPostCode>='" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "'='') and ((P.InstallPostCode<='" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and (isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') <= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InvoiceSent else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.DepositReceived else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then P.InstallCompleted else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=5 then P.InstallBookingDate else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') end end end end END) and ((convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "') >= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InvoiceSent else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.DepositReceived else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then P.InstallCompleted else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=5 then P.InstallBookingDate else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'') end end end end END) or convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "')='') " + qry + " order by P.ProjectOpened desc";
        //OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";
        //Response.Write(data);
        //Response.End();

        dt = ClstblCustomers.query_execute(data);
       
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = GetGridData();

        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
           
                if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    
                    divnopage.Visible = true;
                  
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }

        }
        //string selectedItem = "";
        //foreach (RepeaterItem item in lstSearchStatus.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

        //    if (chkselect.Checked == true)
        //    {
        //        selectedItem += "," + hdnID.Value.ToString();
        //    }
        //}
        //if (selectedItem != "")
        //{
        //    selectedItem = selectedItem.Substring(1);
        //}

        //DataTable dtCount = ClstblProjects.tblProjects_getdateByInvoiceIssuedSearchCount(txtInvSearch.Text, ddlSystemInstaller.SelectedValue, ddlMeterInstaller.SelectedValue, txtMQNsearch.Text, ddlPaidStatus.SelectedValue, txtsuburb.Text, txtContactSearch.Text, selectedItem, ddlfinancewith.SelectedValue, txtpcodefrom.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSelectDate.SelectedValue, txtSearchCompanyNo.Text, txtpcodeto.Text);

        if (dt.Rows.Count > 0)
        {
            //if (dtCount.Rows[0]["TotalAmount"].ToString() != string.Empty)
            //{
            //    lblTotalAmount.Text = SiteConfiguration.ChangeCurrency(dtCount.Rows[0]["TotalAmount"].ToString());
            //}
            //if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            //{
            //    lblTotalDepRec.Text = SiteConfiguration.ChangeCurrency(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            //}
            //if (dtCount.Rows[0]["TotalAmount"].ToString() != string.Empty && dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            //{
            //    lblBalOwing.Text = SiteConfiguration.ChangeCurrency(Convert.ToString(Convert.ToDecimal(dtCount.Rows[0]["TotalAmount"].ToString()) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString())));
            //}
            //lblTotalPanels.Text = dtCount.Rows[0]["TotalPanels"].ToString();

            string TotalAmount = dt.Compute("SUM(TotalQuotePrice)", String.Empty).ToString();
            lblTotalAmount.Text = SiteConfiguration.ChangeCurrency(TotalAmount == "" ? "0" : TotalAmount);

            string TotalDepRec = dt.Compute("SUM(InvoicePayTotal)", String.Empty).ToString();
            lblTotalDepRec.Text = SiteConfiguration.ChangeCurrency(TotalDepRec == "" ? "0" : TotalDepRec);

            string BalOwing = dt.Compute("SUM(BalOwing)", String.Empty).ToString();
            lblBalOwing.Text = SiteConfiguration.ChangeCurrency(BalOwing == "" ? "0" : BalOwing);

        }
        else
        {
            lblTotalAmount.Text = "0";
            lblTotalDepRec.Text = "0";
            lblBalOwing.Text = "0";
            lblTotalPanels.Text = "0";
        }

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
      
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       // Response.End();
        if (e.CommandName == "viewproject")
        {
            //Response.Write(e.CommandName);
            //Response.End();
            hndProInvID.Value = e.CommandArgument.ToString();
           
            InvoicePayments1.Visible = true;
            InvoicePayments1.GetInvPayClickByProject(hndProInvID.Value);
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        //BindDataCount();
        //DataTable dt = ClstblProjects.tblProjects_SelectInvoiceIssued();
        DataTable dt = new DataTable();
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        startindex = (custompageIndex - 1) * custompagesize + 1;
        string enddate = "";
        if (txtEndDate.Text != string.Empty)
        {
            enddate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtEndDate.Text));
        }
        string startdate = "";
        if (txtStartDate.Text != string.Empty)
        {
            startdate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtStartDate.Text));
        }
        string qry = "";
        if (ddlPaidStatus.SelectedValue == "1")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0) and ((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) < P.TotalQuotePrice))";
        }
        else if (ddlPaidStatus.SelectedValue == "2")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) = 0))";
        }
        else if (ddlPaidStatus.SelectedValue == "3")
        {
            qry = "and ((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0)";
        }
        else if (ddlPaidStatus.SelectedValue == "4")
        {
            qry = "and (((P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) < 0))";
        }
        else
        {
            qry = "and ((P.InvoiceStatusID='" + ddlPaidStatus.SelectedValue + "') or '" + ddlPaidStatus.SelectedValue + "'=0) ";
        }
        if (!string.IsNullOrEmpty(ddlpayby.SelectedValue))
        {
            qry = qry + "and (P.ProjectID in (select ProjectID from tblInvoicePayments where (InvoicePayMethodID ='" + ddlpayby.SelectedValue + "' or isnull('" + ddlpayby.SelectedValue + "','')='')))";
        }
        //string data = "select (select case when datediff(day,P.InstallBookingDate,GETDATE()) is null then 0 else datediff(day,P.InstallBookingDate,GETDATE()) end where (P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0)as Odue,(select P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID))as BalOwing, P.ProjectID,P.CustomerID,P.ContactID,P.InvoiceNumber,P.InstallBookingDate,P.ManualQuoteNumber,P.InvoiceSent,P.InvoiceTag,P.TotalQuotePrice,P.DepositRequired,P.DepositReceived,P.InvoicePaid,P.InvoiceTag,P.FollowUp,P.InvoiceNotes,P.InstallerNotes,P.NumberPanels,C.ContMobile,C.ContEmail,SI.StockItem as InverterDetailsName,PS.ProjectStatus,(select case when C.ContLast is null then C.ContFirst else C.ContFirst+' '+C.ContLast end) as Contact,(select case when CT.ContLast is null then CT.ContFirst else CT.ContFirst+' '+CT.ContLast end) as InstallerName,convert(varchar(50),P.InvoicePaid,6) IPaid,convert(varchar(50),P.InstallBookingDate,6) as IBDate,convert(varchar(50),P.InvoiceSent,6) as ISentDate,convert(varchar(50),P.DepositReceived,6) as DRDate,P.InstallState,F.FinanceWith from tblProjects P join tblProjectStatus PS on PS.ProjectStatusID=P.ProjectStatusID join tblContacts C on C.ContactID=P.ContactID join tblContacts CT on CT.ContactID=P.Installer left outer join tblStockItems SI on SI.StockItemID=P.InverterDetailsID join tblFinanceWith F on F.FinanceWithID=P.FinanceWithID where P.InvoiceNumber is not null  and ((P.InvoiceNumber='" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "') or '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "'=0) and ((P.Installer='" + ddlSystemInstaller.SelectedValue + "') or '" + ddlSystemInstaller.SelectedValue + "'=0) and ((P.Electrician='" + ddlMeterInstaller.SelectedValue + "') or '" + ddlMeterInstaller.SelectedValue + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((P.InstallCity='" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "') or '" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'='') and (((upper(C.ContFirst+' '+C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContFirst) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%')) or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='') and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and ((P.FinanceWithID='" + ddlfinancewith.SelectedValue + "') or '" + ddlfinancewith.SelectedValue + "'=0) and ((P.InstallPostCode>='" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "'='') and ((P.InstallPostCode<='" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and (isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') <= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.InvoiceSent else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') end end END  ) and ((convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "') >= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.InvoiceSent else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'') end end END) or convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "')='') and P.CustomerID in(select CustomerID from tblCustomers where ((CompanyNumber='" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "') or '" + System.Security.SecurityElement.Escape(txtSearchCompanyNo.Text) + "'=0)) " + qry + " order by P.ProjectOpened desc ";
        string data = "select (select case when datediff(day,P.InstallBookingDate,GETDATE()) is null then 0 else datediff(day,P.InstallBookingDate,GETDATE()) end where (P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID)) > 0)as Odue,(select P.TotalQuotePrice - (select SUM(InvoicePayTotal) from tblInvoicePayments where ProjectID=P.ProjectID))as BalOwing, P.ProjectID,P.CustomerID,P.ContactID,P.InvoiceNumber, Convert(varchar, P.InstallBookingDate, 106) as InstallBookingDate, P.ManualQuoteNumber,P.InvoiceSent,P.InvoiceTag,P.TotalQuotePrice,P.DepositRequired,Convert(varchar, P.DepositReceived, 106) as DepositReceived, Convert(varchar, P.InvoicePaid, 106) as InvoicePaid, P.InvoiceTag,P.FollowUp,P.InvoiceNotes,P.InstallerNotes,P.NumberPanels,C.ContMobile,C.ContEmail,SI.StockItem as InverterDetailsName,PS.ProjectStatus,(select case when C.ContLast is null then C.ContFirst else C.ContFirst+' '+C.ContLast end) as Contact,(select case when CT.ContLast is null then CT.ContFirst else CT.ContFirst+' '+CT.ContLast end) as InstallerName,convert(varchar(50),P.InvoicePaid,6) IPaid,convert(varchar(50),P.InstallBookingDate,6) as IBDate,convert(varchar(50),P.InvoiceSent,6) as ISentDate,convert(varchar(50),P.DepositReceived,6) as DRDate,P.InstallState,F.FinanceWith,ST.SalesTeam as Team,(select case when TE.EmpLast is null then TE.EmpFirst else TE.EmpFirst+' '+TE.EmpLast end) as AssignedTo from tblProjects P join tblProjectStatus PS on PS.ProjectStatusID=P.ProjectStatusID join tblContacts C on C.ContactID=P.ContactID left join tblContacts CT on CT.ContactID=P.Installer join tblEmployees as TE on P.EmployeeID=TE.EmployeeID left  join tblSalesTeams as ST on ST.SalesTeamID=TE.SalesTeamID left outer join tblStockItems SI on SI.StockItemID=P.InverterDetailsID join tblFinanceWith F on F.FinanceWithID=P.FinanceWithID where P.InvoiceNumber is not null  and ((P.InvoiceNumber='" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "') or '" + System.Security.SecurityElement.Escape(txtInvSearch.Text) + "'=0) and ((P.Installer='" + ddlSystemInstaller.SelectedValue + "') or '" + ddlSystemInstaller.SelectedValue + "'=0) and ((P.Electrician='" + ddlMeterInstaller.SelectedValue + "') or '" + ddlMeterInstaller.SelectedValue + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((P.InstallCity='" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "') or '" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'='') and (((upper(C.ContFirst+' '+C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContFirst) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(C.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%')) or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='') and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and ((P.FinanceWithID='" + ddlfinancewith.SelectedValue + "') or '" + ddlfinancewith.SelectedValue + "'=0) and ((P.InstallPostCode>='" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodefrom.Text) + "'='') and ((P.InstallPostCode<='" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "') or '" + System.Security.SecurityElement.Escape(txtpcodeto.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and (isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') <= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.DepositReceived else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then P.InstallCompleted else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(startdate) + "'),'') end end end END  ) and ((convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "') >= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN P.InstallBookingDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then P.MeterAppliedDate else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then P.DepositReceived else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then P.InstallCompleted else isnull(convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "'),'') end end end END) or convert(datetime,'" + System.Security.SecurityElement.Escape(enddate) + "')='') " + qry + " order by P.ProjectOpened desc";
        //OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;
        //Response.Write(txtInvSearch.Text + "','" + ddlSystemInstaller.SelectedValue + "','" + ddlMeterInstaller.SelectedValue + "','" + txtMQNsearch.Text + "','" + ddlPaidStatus.SelectedValue + "','" + txtsuburb.Text + "','" + txtContactSearch.Text + "','" + selectedItem + "','" + ddlfinancewith.SelectedValue + "','" + txtpcodefrom.Text + "','" + ddlSearchState.SelectedValue + "','" + txtStartDate.Text + "','" + txtEndDate.Text + "','" + ddlSelectDate.SelectedValue + "','" + txtSearchCompanyNo.Text + "','" + txtpcodeto.Text);
        //Response.End();
        //Response.Write(data);
        //Response.End();
        
        
        //dt = ClstblCustomers.query_execute(data);
        

        dt = GetGridData();
        Response.Clear();
        try
        {
            string[] ColumnName = dt.Columns.Cast<DataColumn>()
                                            .Select(x => x.ColumnName)
                                            .ToArray();

            Export oExport = new Export();
            string FileName = "InviceIssued" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 5, 29, 30, 22, 23, 19, 24, 6,
                              10, 1, 13, 18, 21,
                              11, 12, 7, 0, 20, 15,
                              16, 17 }
            ;
            string[] arrHeader = { "Inv #", "State", "Finance", "Proj Status", "Contact", "Mobile", "Installer", "Installed"
                                , "Total", "Owing", "Date Paid", "Panels", "Inverter"
                                , "Dep Req", "Dep Rec", "Man Qte", "O/Due", "Email", "Follow Up"
                                , "Invoices Notes", "Installer Notes"
            };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void BindScript()
    {
       // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtInvSearch.Text = string.Empty;
        ddlSystemInstaller.SelectedValue = "";
        ddlMeterInstaller.SelectedValue = "";
        txtMQNsearch.Text = string.Empty;
        ddlPaidStatus.SelectedValue = "";
        txtsuburb.Text = string.Empty;
        txtContactSearch.Text = string.Empty;

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        ddlfinancewith.SelectedValue = "";
        ddlpayby.SelectedValue = "";
        txtpcodefrom.Text = string.Empty;
        txtpcodeto.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        ddlSelectDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
        BindScript();
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }


    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }

            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {

        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);

    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());

    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }

#endregion

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}