﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_NewSection_Projects_New : System.Web.UI.Page
{
    protected static string Siteurl;
    public DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindFilterDropdown();

            //try
            //{
            //    ddlSelectDate.SelectedValue = "1";
            //}
            //catch (Exception ex) { }
            //txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));
            //txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));

            if (Roles.IsUserInRole("Sales Manager"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = true;

                //GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }
            else if (Roles.IsUserInRole("SalesRep"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = false;
                //GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }

            if(Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Accountant"))
            {
                lbtnExport.Visible = true;
            }

            BindSalesRepDropdown();
            //BindGrid(0);
        }
    }

    public void BindSalesRepDropdown()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
        }

        if (SalesTeam != "")
        {
            ddlTeam.SelectedValue = SalesTeam;
        }

        if (Roles.IsUserInRole("Sales Manager"))
        {
            if (SalesTeam != string.Empty)
            {
                ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_SelectASC_Include();
        }

        ddlSalesRepSearch.DataMember = "fullname";
        ddlSalesRepSearch.DataTextField = "fullname";
        ddlSalesRepSearch.DataValueField = "EmployeeID";
        ddlSalesRepSearch.DataBind();

        if(Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {
            try
            {
                ddlSalesRepSearch.SelectedValue = st.EmployeeID;
            }
            catch (Exception ex) { }
        }
        
    }

    public void BindFilterDropdown()
    {
        ddlSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlSource.DataMember = "CustSource";
        ddlSource.DataTextField = "CustSource";
        ddlSource.DataValueField = "CustSourceID";
        ddlSource.DataBind();

        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        ddlProjectStatus.DataMember = "ProjectStatusID";
        ddlProjectStatus.DataTextField = "ProjectStatus";
        ddlProjectStatus.DataValueField = "ProjectStatusID";
        ddlProjectStatus.DataBind();

        ddlProjectType.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectType.DataMember = "ProjectType";
        ddlProjectType.DataTextField = "ProjectType";
        ddlProjectType.DataValueField = "ProjectTypeID";
        ddlProjectType.DataBind();

        ddlFinanceOption.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlFinanceOption.DataValueField = "FinanceWithID";
        ddlFinanceOption.DataTextField = "FinanceWith";
        ddlFinanceOption.DataMember = "FinanceWith";
        ddlFinanceOption.DataBind();

        ddlSubSource.DataSource = ClstblCustSourceSub.tblCustSourceSub_SelectActive();
        ddlSubSource.DataMember = "CustSourceSub";
        ddlSubSource.DataTextField = "CustSourceSub";
        ddlSubSource.DataValueField = "CustSourceSubID";
        ddlSubSource.DataBind();

        ddlState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlState.DataMember = "State";
        ddlState.DataTextField = "State";
        ddlState.DataValueField = "State";
        ddlState.DataBind();
    }

    #region BindGrid Code
    protected DataTable GetGridData()
    {
        string RoleName = "";
        string[] Role = Roles.GetRolesForUser();

        for (int i = 0; i < Role.Length; i++)
        {
            if (Role[i] != "crm_Customer")
            {
                RoleName = Role[i].ToString();
            }
        }


        //DataTable dt = ClstblSalesinfo.tblProject_GetData_Project();
        DataTable dt = ClstblSalesinfo.tblProject_GetData_Project_New_lts(RoleName, ddlSalesRepSearch.SelectedValue, ddlTeam.SelectedValue, txtProjectNumber.Text, txtCustomer.Text, txtManualNo.Text, ddlProjectStatus.SelectedValue, ddlProjectType.SelectedValue, ddlFinanceOption.SelectedValue, ddlReadyActive.SelectedValue, ddlSource.SelectedValue, ddlSubSource.SelectedValue, ddlState.SelectedValue, ddlSelectDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtPanelModel.Text, txtInverterModel.Text, txtSysKwFrom.Text, txtSysKwTo.Text, txtNMINo.Text);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanTotal.Visible = false;
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            PanTotal.Visible = true;
            //PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }

        if(e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvrow = e.Row;
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }

            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }

            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;

                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewDetails")
        {
            string ProjectID= e.CommandArgument.ToString();

            ClearDetails();
            BindDetails(ProjectID);

            ModalPopupExtenderFollowUpNew.Show();
            BindScript();
        }

        if(e.CommandName == "AddFollowupNote")
        {
            string CustomerID = e.CommandArgument.ToString();
            hndFollwCustomerID.Value = CustomerID;

            ResetFollowup();
            BindContact(CustomerID);

            DataTable dt_c = ClstblContacts.tblContacts_SelectByCustId(CustomerID);
            if (dt_c.Rows.Count > 0)
            {
                grdFollowUpBind(CustomerID);
            }

            hndMode.Value = "";
            ModalPopupExtenderFollowUp.Show();
            BindScript();
        }

    }

    #endregion

    #region Funcation
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    #endregion

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(GridView1.PageSize);
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindScript();
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtCustomer.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtManualNo.Text = string.Empty;
        ddlProjectStatus.SelectedValue = "";
        ddlSource.SelectedValue = "";
        ddlSubSource.SelectedValue = "";
        ddlProjectType.SelectedValue = "";
        ddlFinanceOption.SelectedValue = "";
        ddlReadyActive.SelectedValue = "";
        ddlState.SelectedValue = "";
        
        ddlSelectDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtNMINo.Text = string.Empty;

        BindScript();
        //BindGrid(0);
    }

    public void BindDetails(string ProjectID)
    {
        DataTable dt = ClstblSalesinfo.tblProjects_GetDataByProjectIDforNewProject(ProjectID);

        ltrHeader.Text = dt.Rows[0]["ProjectNumber"].ToString() + "-" + dt.Rows[0]["Project"].ToString();
        lblSys.Text = dt.Rows[0]["SystemCapKW"].ToString();
        lblSysDetails.Text = dt.Rows[0]["SystemDetails"].ToString();
        lblHouseType.Text = dt.Rows[0]["HouseType"].ToString();
        lblRoofType.Text = dt.Rows[0]["RoofType"].ToString();
        lblCompleteDate.Text = dt.Rows[0]["InstallCompleted"].ToString();
        lblProjectNotes.Text = dt.Rows[0]["ProjectNotes"].ToString();
        lblState.Text = dt.Rows[0]["InstallState"].ToString();
        lblInstallerNotes.Text = dt.Rows[0]["InstallerNotes"].ToString();
        lblStatus.Text = dt.Rows[0]["ProjectStatus"].ToString();
        lblDepRec.Text = dt.Rows[0]["DepositReceived"].ToString();
        lblPrice.Text = Convert.ToDecimal(dt.Rows[0]["TotalQuotePrice"]).ToString("F");
        lblFinanceWith.Text = dt.Rows[0]["FinanceWith"].ToString();
        lblManQNo.Text = dt.Rows[0]["ManualQuoteNumber"].ToString();
        lblEmail.Text = dt.Rows[0]["ContEmail"].ToString();

        //lblSysDetails.Attributes("", dt.Rows[0]["SystemDetails"].ToString());
        lblSysDetails.Attributes.Add("data-original-title", dt.Rows[0]["SystemDetails"].ToString());
        lblInstallerNotes.Attributes.Add("data-original-title", dt.Rows[0]["InstallerNotes"].ToString());
        lblProjectNotes.Attributes.Add("data-original-title", dt.Rows[0]["ProjectNotes"].ToString());
    }

    public void ClearDetails()
    {
        ltrHeader.Text = string.Empty;
        lblSys.Text = string.Empty;
        lblSysDetails.Text = string.Empty;
        lblHouseType.Text = string.Empty;
        lblRoofType.Text = string.Empty;
        lblCompleteDate.Text = string.Empty;
        lblProjectNotes.Text = string.Empty;
        lblState.Text = string.Empty;
        lblInstallerNotes.Text = string.Empty;
        lblStatus.Text = string.Empty;
        lblDepRec.Text = string.Empty;
        lblPrice.Text = string.Empty;
        lblFinanceWith.Text = string.Empty;
        lblManQNo.Text = string.Empty;
    }

    public void BindTotal(DataTable dt)
    {
        string NumberPanel = dt.Compute("SUM(NumberPanels)", string.Empty).ToString();
        lblTotalPanel.Text = NumberPanel != "" ? NumberPanel : "0";

        string SystemCapKW = dt.Compute("SUM(SystemCapKW)", string.Empty).ToString();
        lblSystemCapKW.Text = SystemCapKW != "" ? Convert.ToDecimal(SystemCapKW).ToString("F") : "0";

        string QuotePrice = dt.Compute("Sum(TotalQuotePrice)", string.Empty).ToString();
        if(SystemCapKW != "0" && SystemCapKW != "")
        {
            decimal ans = (Convert.ToDecimal(QuotePrice) / Convert.ToDecimal(SystemCapKW));
            lblPriceKV.Text = Math.Round(ans, 2).ToString();
        }
        else
        {
            lblPriceKV.Text = "0.00";
        }

        string Count = dt.Select("ProjectStatusID  IN ('5','10','19','11','12','13','14')").Length.ToString();
        string dtCount = dt.Rows.Count.ToString();
        if(Count != "0")
        {
            if(dtCount != "0")
            {
                decimal Ratio = (Convert.ToDecimal(Count) / Convert.ToDecimal(dtCount)) * 100;
                lblInstallRatio.Text = Math.Round(Ratio, 2).ToString();
            }
            else
            {
                lblInstallRatio.Text = "0.00";
            }
            
        }
        else
        {
            lblInstallRatio.Text = "0.00";
        }

        //string Active = dt.Select("ProjectStatusID  IN ('3')").Length.ToString();
        string Active = dt.AsEnumerable().Where(y => y.Field<int>("ProjectStatusID") == 3).Sum(x => x.Field<int>("NumberPanels")).ToString();
        lblActive.Text = Active;

        //string DepRec = dt.Select("ProjectStatusID  IN ('8')").Length.ToString();
        string DepRec = dt.AsEnumerable().Where(y => y.Field<int>("ProjectStatusID") == 8).Sum(x => x.Field<int>("NumberPanels")).ToString();
        lblDepReceived.Text = DepRec;

        string Days = dt.Compute("Avg(InstallDay)", string.Empty).ToString();
        lblAvgInstallDay.Text = Days.ToString();
    }

    #region FollowUp 
    public void BindContact(string CustomerID)
    {
        ListItem item1 = new ListItem();
        //item1.Text = "Select";
        //item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);
        ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(CustomerID);
        ddlContact.DataMember = "Contact";
        ddlContact.DataTextField = "Contact";
        ddlContact.DataValueField = "ContactID";
        ddlContact.DataBind();
    }

    public void ResetFollowup()
    {
        //ddlContact.SelectedValue = "";
        ddlManager.SelectedValue = "1";
        txtNextFollowupDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        ddlreject.SelectedValue = "2";
    }

    public void grdFollowUpBind(string CustomerID)
    {
        DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(CustomerID);
        //if (dt.Rows.Count > 0)
        //{
        //    divgrdFollowUp.Visible = true;
        //    grdFollowUp.DataSource = dt;
        //    grdFollowUp.DataBind();
        //}
        //else
        //{
        //    divgrdFollowUp.Visible = false;
        //}

        if (dt.Rows.Count > 0)
        {
            divFollowUpDetailsNew.Visible = true;
            rptFollowUpDetails.DataSource = dt;
            rptFollowUpDetails.DataBind();
            if (dt.Rows.Count > 5)
            {
                divFollowUpNew.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
            }
            else
            {
                divFollowUpNew.Attributes.Add("style", "overflow-y: scroll");
            }

        }
        else
        {
            divFollowUpDetails.Visible = false;
        }
    }

    protected void rptFollowUpDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string arg = e.CommandArgument.ToString();
        string[] Argument = arg.Split(';');

        string CustInfoID = Argument[0].ToString();
        string CustomerID = Argument[1].ToString();

        if (e.CommandName == "Select")
        {
            hndMode.Value = "Update";
            hndFollowEditCustInfoID.Value = CustInfoID;

            DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(CustomerID);

            if (dt.Rows[0]["ContactID"].ToString() != null && dt.Rows[0]["ContactID"].ToString() != "")
            {
                try
                {
                    ddlContact.SelectedValue = dt.Rows[0]["ContactID"].ToString();
                }
                catch (Exception ex) { }
            }

            if (dt.Rows[0]["FollowupType"].ToString() != null && dt.Rows[0]["FollowupType"].ToString() != "")
            {
                try
                {
                    ddlManager.SelectedValue = dt.Rows[0]["FollowupType"].ToString();
                }
                catch (Exception ex) { }
            }

            if (dt.Rows[0]["NextFollowupDate"].ToString() != null && dt.Rows[0]["NextFollowupDate"].ToString() != "")
            {
                try
                {
                    txtNextFollowupDate.Text = Convert.ToDateTime(dt.Rows[0]["NextFollowupDate"].ToString()).ToShortDateString();
                }
                catch (Exception ex) { }
            }

            if (dt.Rows[0]["Reject"].ToString() != null && dt.Rows[0]["Reject"].ToString() != "")
            {
                try
                {
                    ddlManager.SelectedValue = dt.Rows[0]["Reject"].ToString();
                }
                catch (Exception ex) { }
            }
            txtDescription.Text = dt.Rows[0]["Description"].ToString();

            ModalPopupExtenderFollowUp.Show();
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string CustomerID = hndFollwCustomerID.Value;
        string Description = txtDescription.Text;
        string Reject = ddlreject.SelectedValue;

        string NextFollowupDate = "";
        if (txtNextFollowupDate.Text != "")
        {
            NextFollowupDate = Convert.ToDateTime(txtNextFollowupDate.Text).AddHours(14).ToShortDateString();
        }

        string ContactID = ddlContact.SelectedValue.ToString();
        string Manager = ddlManager.SelectedValue;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;

        if (hndMode.Value != "Update")  // This Code is Add New Record
        {
            int success = ClstblCustInfo.tblCustInfo_Insert(CustomerID, Description, NextFollowupDate, ContactID, CustInfoEnteredBy, Manager);
            bool update = ClstblCustInfo.tblCustInfo_UpdateReject(success.ToString(), Reject);

            if (Convert.ToString(success) != string.Empty)
            {
                ResetFollowup();
                BindScript();
                SetAdd1();
                //grdFollowUpBind(CustomerID);
                BindGrid(0);
            }
            else
            {
                BindScript();
                grdFollowUpBind(CustomerID);
                SetError1();
                //BindGrid(0);
                ModalPopupExtenderFollowUp.Show();
            }
        }
        else // This Code is Update Record
        {
            string CustInfoID = hndFollowEditCustInfoID.Value;

            if (CustInfoID != "")
            {
                bool success = ClstblSalesinfo.tblCustInfo_UpdateFollowUp(ContactID, Description, NextFollowupDate, Manager, CustInfoID);
                bool update = ClstblCustInfo.tblCustInfo_UpdateReject(CustInfoID, Reject);

                if (success)
                {
                    ResetFollowup();
                    BindScript();
                    SetAdd1();
                    grdFollowUpBind(CustomerID);
                    //ModalPopupExtenderFollowUpNew.Show();
                    BindGrid(0);
                }
                else
                {
                    BindScript();
                    ResetFollowup();
                    grdFollowUpBind(CustomerID);
                    SetError1();
                    BindGrid(0);
                    //ModalPopupExtenderFollowUpNew.Show();
                }
            }
            else
            {
                SetError1();
            }
        }

        hndMode.Value = "";
    }
    #endregion

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExcelData();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                .Select(x => x.ColumnName)
                                .ToArray();

            string FileName = "ProjectNew_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 26, 27, 6, 7, 37, 30 , 11,
                            23, 5, 28, 31, 32, 3, 14, 13, 29, 41,
                            35, 36, 17, 18, 15, 21, 25, 8,
                            33, 34, 9, 10, 38, 39, 40, 22, 42, 43, 45 };

            string[] arrHeader = { "Project#", "Man#", "Project", "Contact", "Mobile", "ContactEmail", "Team", "Employee Name", "State", "P/CD", "Quote", "Source", "Sub Source", "Status", "System Cap", "Panels", "Inverter", "System Details", "PanelModel", "InverterModel", "House Type", "Roof Type", "Price", "Dep Rec", "FinanceWith", "Install Book Date", "First Dep. Date", "First Dep. Amount", "NextFollowupDate", "Description", "Leaddate", "Proj.OpenDate", "DiffInDays", "Project Notes", "Remove Panel", "Remove Qty", "Install Day" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
            MsgError("Error while downloading Excel..");
        }
    }

    protected DataTable GetExcelData()
    {
        string RoleName = "";
        string[] Role = Roles.GetRolesForUser();

        for (int i = 0; i < Role.Length; i++)
        {
            if (Role[i] != "crm_Customer")
            {
                RoleName = Role[i].ToString();
            }
        }


        //DataTable dt = ClstblSalesinfo.tblProject_GetData_Project();
        DataTable dt = ClstblSalesinfo.tblProject_GetData_Project_New_lts_Excel(RoleName, ddlSalesRepSearch.SelectedValue, ddlTeam.SelectedValue, txtProjectNumber.Text, txtCustomer.Text, txtManualNo.Text, ddlProjectStatus.SelectedValue, ddlProjectType.SelectedValue, ddlFinanceOption.SelectedValue, ddlReadyActive.SelectedValue, ddlSource.SelectedValue, ddlSubSource.SelectedValue, ddlState.SelectedValue, ddlSelectDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, txtPanelModel.Text, txtInverterModel.Text, txtSysKwFrom.Text, txtSysKwTo.Text, txtNMINo.Text);

        return dt;
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }
}