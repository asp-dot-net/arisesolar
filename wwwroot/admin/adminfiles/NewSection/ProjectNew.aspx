﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProjectNew.aspx.cs" Inherits="admin_adminfiles_NewSection_ProjectNew"
    MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/NewProject.ascx" TagPrefix="uc1" TagName="NewProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ui-autocomplete-loading {
            background: white url("../../../images/indicator.gif") right center no-repeat;
        }
    </style>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);
            //alert(divname + "--" + trname + "--" + 'img' + divname);
            // alert(div+"--"+img+"--"+tr);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>
    <script type="text/javascript">
        //$(document).ready(function () {
        //            doMyAction();
        //        });

        function doMyAction1() {

            HighlightControlToValidate();
            <%--$('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });--%>

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {

                callMultiCheckbox();
            });
        }

        function doMyAction() {
            HighlightControlToValidate();
            <%--$('#<%=ibtnUpdateStatus.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnCancelLead.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=ibtnEditDetail.ClientID %>').click(function () {
                formValidate();
                var drpdiv = document.getElementsByClassName("drpValidate");
                var flag = 0;

                for (i = 0; i < drpdiv.length; i++) {
                    if ($(drpdiv[i]).find(".myval").val() == "") {
                        $(drpdiv[i]).addClass("errormassage");
                        flag = flag + 1;
                    }
                }
                if (flag > 0) {
                    return false;
                }

            });--%>


            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            //gridviewScroll();
        }

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#b94a48");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>


    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Project</h5>
        <div id="fdfs" class="pull-right" runat="server">
            <%--<ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                <asp:HyperLink NavigateUrl="~/admin/adminfiles/company/Customer.aspx?Mode=AddCustomerFormLeadTracker" runat="server"
                    CssClass="btn btn-default purple" Target="_blank"> <i class="fa fa-plus"></i> Add Customer
                </asp:HyperLink>
            </ol>--%>
        </div>
    </div>


    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').removeClass('loading-inactive');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').addClass('loading-inactive');
        }

        function pageLoaded() {
            $('.loading-container').addClass('loading-inactive');
            //  CompanyAddress(); roshni
            // gridviewScroll();
            //alert($(".search-select").attr("class"));
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });


            $("[data-toggle=tooltip]").tooltip();
            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

           <%-- $("#<%=txtformbayUnitNo.ClientID %>").keyup(function () {
                //$('#lblName').text($('#txtName').val());
           
                document.getElementById("<%= txtStreetAddress.ClientID %>").value = $("#<%=txtformbayUnitNo.ClientID %>").val();
            });--%>
        }
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                        </div>
                    </div>

                    <div class="searchfinal" onkeypress="DefaultEnter(event);">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="paneldefault" runat="server" DefaultButton="btnSearch">
                                        <div class="row">
                                            <div class="inlineblock ">
                                                <div class="col-sm-12">
                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b" placeholder="Project No."></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtProjectNumber"
                                                            WatermarkText="Project No." />
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:TextBox ID="txtCustomer" runat="server" placeholder="Customer Name" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtCustomer"
                                                            WatermarkText="Customer Name" />
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtCustomer" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetCompanyList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:TextBox ID="txtManualNo" runat="server" CssClass="form-control m-b" placeholder="Manual No."></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtManualNo"
                                                            WatermarkText="Manual No." />
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:DropDownList ID="ddlProjectStatus" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Status</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div2" runat="server">
                                                        <asp:DropDownList ID="ddlSource" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Source</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div3" runat="server">
                                                        <asp:DropDownList ID="ddlSubSource" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="tdTeam" runat="server">
                                                        <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Team</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="tdsalerep" runat="server">
                                                        <asp:DropDownList ID="ddlSalesRepSearch" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-2 martop5">
                                                        <asp:DropDownList ID="ddlProjectType" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="" Text="Project Type"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:DropDownList ID="ddlFinanceOption" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Finance Option</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:DropDownList ID="ddlReadyActive" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Ready To Active</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5">
                                                        <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true" 
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1 martop5" id="div7" runat="server">
                                                        <asp:DropDownList ID="ddlSelectDate" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value = "">Select</asp:ListItem>
                                                            <asp:ListItem Value = "1" Selected="True">Project Opened</asp:ListItem>
                                                            <asp:ListItem Value = "2">First Deposite Date</asp:ListItem>
                                                            <%--<asp:ListItem Value="3">First Quote Date</asp:ListItem>
                                                            <asp:ListItem Value="4">Quote Accepted</asp:ListItem>--%>
                                                            <asp:ListItem Value = "5">Dep. Received</asp:ListItem>
                                                            <asp:ListItem Value = "7">Active Date</asp:ListItem>
                                                            <asp:ListItem Value = "8">FollowUp Date</asp:ListItem>
                                                            <asp:ListItem Value="6">Install Date</asp:ListItem>
                                                            <%--<asp:ListItem Value="9">Install Comp</asp:ListItem>
                                                            <asp:ListItem Value="10">Pickup List</asp:ListItem>
                                                            <asp:ListItem Value="11">Stock Pickup</asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>


                                                    <%--<div class="input-group col-sm-2 martop5" style="width: 70px;" id="div4" runat="server">
                                                        <asp:DropDownList ID="ddlRejectOrNot" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Reject</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>--%>

                                                    <div class="input-group martop5">
                                                        <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search"
                                                            OnClick="btnSearch_Click" />
                                                    </div>
                                                    <%--<div class="input-group martop5">
                                                        <asp:Button ID="btnSendMail" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="SendMail" />
                                                    </div>--%>
                                                    <div class="input-group martop5">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" CausesValidation="false" CssClass="btn btn-primary"
                                                            OnClick="btnClearAll_Click"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div class="datashowbox martop5">
                                <div class="row">
                                    <div class="dataTables_length showdata col-sm-6">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="tdExport" class="pull-right btnexelicon" runat="server">
                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                             <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                CausesValidation="false" OnClick="lbtnExport_Click" Visible="false"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            <%-- </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="PanTotal" Visible="false">
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="panelHeader" runat="server">
                                        <div class="row">
                                            <div class="inlineblock ">
                                                <div class="col-sm-12">
                                                    <div class=""  runat="server" >
                                                        <div class="input-group col-sm-12">
                                                            <b style="font-size:15px!important;">Total Number of Panels : </b>
                                                            <asp:Label ID="lblTotalPanel" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Total SystemCapKW : </b>
                                                            <asp:Label ID="lblSystemCapKW" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Price/kw : </b>
                                                            <asp:Label ID="lblPriceKV" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Install Ratio : </b>
                                                            <asp:Label ID="lblInstallRatio" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Active : </b>
                                                            <asp:Label ID="lblActive" runat="server" Font-Size="15px" />
                                                            &nbsp;&nbsp;
                                                            <b style="font-size:15px!important;">Dep. Rec : </b>
                                                            <asp:Label ID="lblDepReceived" runat="server" Font-Size="15px" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                    <div class="">
                        <div id="PanGrid" runat="server" class="leadtracker finalgrid">
                            <div class="table-responsive xscroll">
                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="20px">
                                            <ItemTemplate>
                                                <%--<a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                   
                                                    <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />
                                                </a>--%>
                                                <asp:LinkButton ID="gvlnkView" CommandName="ViewDetails" CommandArgument='<%#Eval("ProjectID")%>' CssClass="btn-sm"
                                                    CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="View Details">
                                                             <i class="fa fa-eye"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-Width="50px" HeaderText="Proj#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="InstallPostCode">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hndprojectid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                <asp:Label ID="lblProject9" runat="server">
                                                    <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="ProjectStatus">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProject17" Width="20px" runat="server"><%#Eval("ProjectStatus")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-Width="100px" HeaderText="Sales Rep Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="EmpName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProject18" runat="server"><%#Eval("EmpName")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Source" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Source">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSalesCommPaid" runat="server" Width="50px"><%#Eval("Source")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="P/CD" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="InstallPostCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProject15" runat="server" Width="20px"><%#Eval("InstallPostCode")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProject3" data-original-title='<%#Eval("Customer")%>' data-toggle="tooltip" data-placement="top" Width="100px" runat="server"><%#Eval("Customer")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="contactmobile"
                                            ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProject4" runat="server" Width="40px"><%#Eval("ContMobile")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Next FollowUp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-HorizontalAlign="left" SortExpression="NextFollowupDate" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <div style="width: 100%; display: block;">
                                                    <asp:Label ID="lblFollowUpDate" runat="server"
                                                        Width="80px" CssClass="spandate"> <%# Eval("NextFollowupDate","{0:dd MMM yyyy}")%></asp:Label>&nbsp;&nbsp;

                                                    <asp:LinkButton ID="lbtnFollowUpNote" CommandName="AddFollowupNote" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-azure btn-xs"
                                                            CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add FollowUp">
                                                              <i class="fa fa-calendar"></i></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Follow Up Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Description"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <span id="spnfollowup" runat="server" data-toggle="tooltip" data-placement="top">
                                                    <asp:Label ID="lblFollowUpNote" runat="server" data-original-title='<%# Eval("Description").ToString() %>' data-toggle="tooltip" data-placement="top"
                                                        Width="100px"><%# Eval("Description")%></asp:Label>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallBookingDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallBookingDate" runat="server" Width="70px"><%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <AlternatingRowStyle />
                                    <RowStyle />
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>




            <script type="text/javascript">

                function gridviewScroll() {
                    $('#<%=GridView1.ClientID%>').gridviewScroll({
                        width: $("#content").width() - 25,
                        height: 6000,
                        freezesize: 0
                    });
                }

            </script>

            <div class="loaderPopUP">
                <script>
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_pageLoaded(pageLoadedpro);
                    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(beginrequesthandler);
                    // raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(endrequesthandler);

                    function beginrequesthandler(sender, args) {
                        //shows the modal popup - the update progress
                        $('.loading-container').css('display', 'block');

                    }
                    function endrequesthandler(sender, args) {
                        //hide the modal popup - the update progress
                    }

                    function pageLoadedpro() {
                        $('.datetimepicker1').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                        $('.loading-container').css('display', 'none');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $("[data-toggle=tooltip]").tooltip();

                        $('#datetimepicker7').datetimepicker({
                            format: 'DD/MM/YYYY',
                            useCurrent: false //Important! See issue #1075
                        }).keypress(function (event) {
                            if (event.keyCode != 8) {
                                event.preventDefault();
                            }
                        });

                        var today = new Date();
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();

                        today = dd + '/' + mm + '/' + yyyy;
                        //document.write(today);
                        $('#datetimepicker7').data("DateTimePicker").minDate(today);
                    }


                </script>


                <script type="text/javascript">
                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });
                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });


                </script>
            </div>

            <!-- ModalPopup Add Folloup -->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderFollowUpNew" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelFollowUp" DropShadow="false" PopupControlID="divAddFollowUp"
                TargetControlID="btnNULLStatus">
            </cc1:ModalPopupExtender>
            <div id="divAddFollowUp" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog " style="margin-left: -300px">
                    <div class="modal-content  modal-lg" style="width: 900px; max-height: 500px; min-height: 300px;">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancelFollowUp" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                           Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                                <b>
                                    <asp:Literal runat="server" ID="ltrHeader" /></b></h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="row col-md-12">
                                    <div class="row finalgrid" id="divFollowUpDetails" runat="server">
                                        <div class="col-md-12" runat="server" id="divFollowUp">
                                            <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                <tr>
                                                    <td><b>Sys</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSys" runat="server"></asp:Label>
                                                    </td>
                                                    <td><b>System Details</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSysDetails" runat="server" Width="220px" data-toggle="tooltip" data-placement="top"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>House Type</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblHouseType" runat="server"></asp:Label>
                                                    </td>
                                                    <td><b>Roof Type</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRoofType" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Install Complete Date</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCompleteDate" runat="server" Width="160px"></asp:Label>
                                                    </td>

                                                    <td><b>Install Notes</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblInstallerNotes" runat="server" Width="220px" data-toggle="tooltip" data-placement="top"></asp:Label>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td><b>Dep Rec</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDepRec" runat="server"></asp:Label>
                                                    </td>

                                                    <td><b>Project Notes</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblProjectNotes" runat="server" Width="220px" data-toggle="tooltip" data-placement="top"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>State</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblState" runat="server" Width="80px"></asp:Label>
                                                    </td>
                                                    <td><b>Status </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Price </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPrice" runat="server" Width="70px"></asp:Label>
                                                    </td>
                                                    <td><b>FinanceWith</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFinanceWith" runat="server" Width="110px"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Man#</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblManQNo" runat="server" Width="80px"></asp:Label>
                                                    </td>
                                                    <td><b>Email</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndCustomerID" runat="server" />
            <!------------------------->

            <!-- ModalPopup Add Folloup -->
            <cc1:ModalPopupExtender ID="ModalPopupExtenderFollowUp" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="LinkButton5" DropShadow="false" PopupControlID="divFollowUpModal"
                TargetControlID="btnNULLFolloup">
            </cc1:ModalPopupExtender>
            <div id="divFollowUpModal" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog " style="margin-left: -300px">
                    <div class="modal-content  modal-lg" style="width: 900px; max-height: 500px; min-height: 300px;">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton5" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                           Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                <b>Add FollowUp</b></h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">

                                <div class="row col-md-12">
                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="Label8" runat="server" class="control-label">
                                                                Contact
                                            </asp:Label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlContact" runat="server"
                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                <%--<asp:ListItem Value="">Select</asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="Label3" runat="server" class="control-label">
                                                                Select
                                            </asp:Label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlManager" runat="server" AppendDataBoundItems="true"
                                                CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1" Selected="True">Follow Up</asp:ListItem>
                                                <asp:ListItem Value="2">Manager</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="LabelDate" runat="server" class="control-label">
                                                                Next Followup Date
                                            </asp:Label>
                                        </span><span>
                                            <div class="input-group date" id="datetimepicker7">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNextFollowupDate"
                                                    ValidationGroup="FollowUp" ErrorMessage="Required Date" CssClass="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                            </div>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-md-12">
                                    <div class="form-group col-md-8">
                                        <span class="name">
                                            <asp:Label ID="Label10" runat="server" class="control-label">
                                                                Description
                                            </asp:Label>
                                        </span><span>
                                            <asp:TextBox ID="txtDescription" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <span class="name">
                                            <asp:Label ID="Label19" runat="server" class="control-label">
                                                                Reject
                                            </asp:Label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlreject" runat="server" AppendDataBoundItems="true"
                                                CssClass="myval">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-md-12">
                                    <div class="form-group col-md-12 text-center">
                                        <asp:LinkButton class="btn btn-primary savewhiteicon btnsaveicon" ID="btnAdd" runat="server"
                                            ValidationGroup="FollowUp" Text="Save" OnClick="btnAdd_Click" />
                                    </div>
                                </div>

                                <asp:HiddenField ID="hndMode" runat="server" />
                                <asp:HiddenField ID="hndFollowEditCustInfoID" runat="server" />
                                <div class="row col-md-12">
                                    <div class="row finalgrid" id="divFollowUpDetailsNew" runat="server" visible="false">
                                        <div class="col-md-12" runat="server" id="divFollowUpNew">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 10%; text-align: center">#</th>
                                                        <th style="width: 25%; text-align: center">Contact</th>
                                                        <th style="width: 15%; text-align: center">Next Followup Date</th>
                                                        <th style="width: 30%; text-align: justify">Description</th>
                                                        <th style="width: 10%; text-align: center">Reject</th>
                                                        <th style="width: 10%; text-align: center">Action</th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="rptFollowUpDetails" runat="server" OnItemCommand="rptFollowUpDetails_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 10%; text-align: center">
                                                                <asp:Label ID="lblRowNumber" Text='<%# Container.ItemIndex + 1 %>' runat="server" />
                                                            </td>

                                                            <td style="text-align: center">
                                                                <%#Eval("Contact") %>
                                                            </td>

                                                            <td style="text-align: center">
                                                                <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                            </td>

                                                            <td style="width: 30%; text-align: justify;">
                                                                <%-- <asp:Label Text='<%#Eval("Description") %>' runat="server" 
                                                                data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>'/>--%>

                                                                <%#Eval("Description") %>
                                                            </td>

                                                            <td style="text-align: center;">
                                                                <%#Eval("Reject").ToString()=="1"?"Yes":Eval("Reject").ToString()=="0"?"No":"" %>
                                                            </td>

                                                            <td style="text-align: center;">
                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" Visible='<%#Eval("firstrecord").ToString()=="1"?true:false%>' CommandName="Select"
                                                                    CommandArgument='<%#Eval("CustInfoID") + ";" + Eval("CustomerID")%>' CausesValidation="false">
                                                                     <i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:Button ID="btnNULLFolloup" Style="display: none;" runat="server" />
            <asp:HiddenField ID="hndFollwCustomerID" runat="server" />
            <!------------------------->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
