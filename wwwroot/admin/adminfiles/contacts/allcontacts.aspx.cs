﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_contacts_allcontacts : System.Web.UI.Page
{
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            DataTable dtemp = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            string type = "";
            if (dtemp.Rows.Count > 0)
            {
                foreach (DataRow dr in dtemp.Rows)
                {
                    if (dr["SalesTeamID"].ToString() == "2" || dr["SalesTeamID"].ToString() == "5" || dr["SalesTeamID"].ToString() == "7" || dr["SalesTeamID"].ToString() == "8")
                    {
                        type = "internalteam";
                    }
                    //else
                    //{
                    //    type = "externalteam";
                    //}
                }
            }
            
            DataTable dtTeam = new DataTable();
            if (Roles.IsUserInRole("Administrator"))
            {
                dtTeam = ClstblSalesTeams.tblSalesTeams_SelectASC();
            }
            else
            {
                dtTeam = ClstblSalesTeams.tblSalesTeams_bySalesTeamType(type);
            }
            if (dtTeam.Rows.Count > 0)
            {
                rptteam.DataSource = dtTeam;
                rptteam.DataBind();
            }
            BindAllContacts();
        }
    }
    public void BindAllContacts()
    {
        foreach (RepeaterItem item1 in rptteam.Items)
        {
            LinkButton lnkteam = (LinkButton)item1.FindControl("lnkteam");
            lnkteam.CssClass = "list-group-item";
        }
        string name = "";
        if (txtSearch.Text != string.Empty)
        {
            name = txtSearch.Text;
        }

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        DataTable dtemp = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        string type = "";
        if (dtemp.Rows.Count > 0)
        {
            foreach (DataRow dr in dtemp.Rows)
            {
                if (dr["SalesTeamID"].ToString() == "2" || dr["SalesTeamID"].ToString() == "5" || dr["SalesTeamID"].ToString() == "7" || dr["SalesTeamID"].ToString() == "8")
                {
                    type = "internalteam";
                }
                //else
                //{
                //    type = "externalteam";
                //}
            }
        }


        DataTable dt = new DataTable();
        if (Roles.IsUserInRole("Administrator"))
        {
            dt = ClstblEmployees.tblEmployees_SearchContacts("", name);
        }
        else
        {
            dt = ClstblEmployees.tblEmployees_SearchContactsbyTeam("", name, type);
        }
        if (dt.Rows.Count > 0)
        {
            rptnames.DataSource = dt;
            rptnames.DataBind();
        }
        lnkallcontacts.CssClass = "list-group-item active";
    }
    protected void lnkallcontacts_Click(object sender, EventArgs e)
    {
        hdnindex.Value = string.Empty;
        divprofile.Visible = false;
        divprofile1.Visible = false;
        BindAllContacts();
    }
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*]{2})",
                        RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);

        return Convert.ToInt32(match.Value);
    }
    protected void lnkteam_Click(object sender, EventArgs e)
    {
        divprofile.Visible = false;
        divprofile1.Visible = false;
        int index = GetControlIndex(((LinkButton)sender).ClientID);
        RepeaterItem item = rptteam.Items[index];
        hdnindex.Value = index.ToString();
        foreach (RepeaterItem item1 in rptteam.Items)
        {
            LinkButton lnkteam = (LinkButton)item1.FindControl("lnkteam");
            lnkteam.CssClass = "list-group-item";
        }
        BindNames(item);
    }
    public void BindNames(RepeaterItem item)
    {
        HiddenField hdnid = (HiddenField)item.FindControl("hdnid");
        LinkButton lnkteam = (LinkButton)item.FindControl("lnkteam");
        string teamid = "";
        if (hdnid.Value != string.Empty)
        {
            teamid = hdnid.Value;
        }
        string name = "";
        if (txtSearch.Text != string.Empty)
        {
            name = txtSearch.Text;
        }
        DataTable dt = ClstblEmployees.tblEmployees_SearchContacts(teamid, name);
        
        if (dt.Rows.Count > 0)
        {
            rptnames.DataSource = dt;
            rptnames.DataBind();
        }
        else
        {
            rptnames.DataSource = null;
            rptnames.DataBind();
        }
        lnkteam.CssClass = "list-group-item active";
        
        lnkallcontacts.CssClass = "list-group-item";
    }
    protected void lnkname_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((LinkButton)sender).ClientID);
        RepeaterItem item = rptnames.Items[index];
        foreach (RepeaterItem item1 in rptnames.Items)
        {
            LinkButton lnkname = (LinkButton)item1.FindControl("lnkname");
            lnkname.CssClass = "list-group-item";
        }
        BindProfile(item);
    }
    public void BindProfile(RepeaterItem item)
    {
        HiddenField hdnempid = (HiddenField)item.FindControl("hdnempid");
        LinkButton lnkname = (LinkButton)item.FindControl("lnkname");
        if (hdnempid.Value != string.Empty)
        {
            divprofile.Visible = true;
            divprofile1.Visible = true;
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByEmployeeID(hdnempid.Value);
            int i = 0;
            if (st.EmpFirst != string.Empty && st.EmpLast != string.Empty)
            {
                lblfullname.Text = st.EmpFirst + " " + st.EmpLast;
                i = i + 2;
            }
            if (st.EmpTitle != string.Empty)
            {
                i++;
            }
            if (st.EmpInitials != string.Empty)
            {
                i++;
            }
            if (st.EmpNicName != string.Empty)
            {
                i++;
            }

            ltemail.Text = st.EmpEmail;
            ltname.Text = st.EmpFirst;
            if (st.EmpMobile != string.Empty)
            {
                ltcellphone.Text = st.EmpMobile;
                divcell.Visible = true;
                i++;
            }
            else
            {
                divcell.Visible = false;
            }
            lnkname.CssClass = "list-group-item active";

            if (st.EmpImage != string.Empty)
            {
                //EmpImage.ImageUrl = "~/userfiles/EmployeeProfile/" + st.EmpImage;
                //EmpImage.ImageUrl = pdfURL+"EmployeeProfile/" + st.EmpImage;
                EmpImage.ImageUrl = SiteConfiguration.GetDocumnetPath("EmployeeProfile", st.EmpImage);
                i++;
            }
            else
            {
                EmpImage.ImageUrl = "~/images/anynomous.png";
            }

            if (st.EmpBio != string.Empty)
            {
                ltbio.Text = st.EmpBio;
                divbio.Visible = true;
                i++;
            }
            else
            {
                divbio.Visible = false;
            }

            if (st.EmpAddress != string.Empty)
            {
                ltaddress.Text = st.EmpAddress;
                divaddress.Visible = true;
                i++;
            }
            else
            {
                divaddress.Visible = false;
            }

            if (st.EmpCity != string.Empty)
            {
                ltcity.Text = st.EmpCity;
                divcity.Visible = true;
                i++;
            }
            else
            {
                divcity.Visible = false;
            }

            if (st.EmpState != string.Empty)
            {
                ltstate.Text = st.EmpState;
                divstate.Visible = true;
                i++;
            }
            else
            {
                divstate.Visible = false;
            }

            if (st.EmpPostCode != string.Empty)
            {
                ltpostcode.Text = st.EmpPostCode;
                divpostcode.Visible = true;
                i++;
            }
            else
            {
                divpostcode.Visible = false;
            }

            if (st.HireDate != string.Empty)
            {
                ltdatehire.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(st.HireDate));
                divdatehire.Visible = true;
            }
            else
            {
                divdatehire.Visible = false;
            }

            if (st.LocationName != string.Empty)
            {
                ltlocation.Text = st.LocationName;
                divlocation.Visible = true;
            }
            else
            {
                divlocation.Visible = false;
            }

            if (st.TeamName != string.Empty)
            {
                ltteam.Text = st.TeamName;
                divteam.Visible = true;
            }
            else
            {
                divteam.Visible = false;
            }

            if (st.EmpType != string.Empty)
            {
                if (st.EmpType == "1")
                {
                    ltemptype.Text = "Arise Solar";
                }
                if (st.EmpType == "2")
                {
                    ltemptype.Text = "Door to Door";
                }
                divemptype.Visible = true;
            }
            else
            {
                divemptype.Visible = false;
            }

            decimal count = (100 * i) / 12;
            ltper.Text = count.ToString();
            divper.Attributes.Add("style", "width: " + count.ToString() + "%");
            divper.Attributes.Add("data-original-title", count.ToString() + "%");
        }
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        //int index = GetControlIndex(((Button)sender).ClientID);  //Convert.ToInt32(hdnsrchid.Value);
        if (hdnindex.Value != string.Empty)
        {
             
            int index = Convert.ToInt32(hdnindex.Value);

            RepeaterItem item = rptteam.Items[index];
            foreach (RepeaterItem item1 in rptteam.Items)
            {
                LinkButton lnkteam = (LinkButton)item1.FindControl("lnkteam");
                lnkteam.CssClass = "list-group-item";
            }
            BindNames(item);
        }
        else
        {
            
            string name = "";
            if (txtSearch.Text != string.Empty)
            {
                name = txtSearch.Text;
            }
            DataTable dt = ClstblEmployees.tblEmployees_SearchContacts("", name);
            
            if (dt.Rows.Count > 0)
            {
                rptnames.DataSource = dt;
                rptnames.DataBind();
            }
            else
            {
                rptnames.DataSource = null;
                rptnames.DataBind();
            }
        }
    }
}