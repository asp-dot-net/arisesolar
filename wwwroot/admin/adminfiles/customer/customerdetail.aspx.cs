﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_customer_customerdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string ProjectID = "2";//Request.QueryString["id"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            lblSystemDetails.Text = st.SystemDetails;
            try
            {
                lblInstallBookingDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.InstallBookingDate));
            }
            catch { }
            lblInstaller.Text = st.InstallerName;
            lblHouseType.Text = st.HouseType;
            lblRoofType.Text = st.RoofType;
            lblRoofAngle.Text = st.RoofAngle;
            lblElecDist.Text = st.ElecDistributor;
            lblElecRetailer.Text = st.ElecRetailer;
            lblApprovalRef.Text = st.ElecDistApprovelRef;
            lblNMINumber.Text = st.NMINumber;

            DataTable dt = ClstblProjects.tblCustomerFeedback_SelectByProjectID(ProjectID);
            if (dt.Rows.Count > 0)
            {
                divAddFeedback.Visible = false;
                txtFeedback.Enabled = false;
                txtFeedback.Text = dt.Rows[0]["Feedback"].ToString();
            }
            else
            {
                txtFeedback.Enabled = true;
                divAddFeedback.Visible = true;
            }

            DataTable dtQ = ClstblProjects.tblProjectQuotes_SelectTop1(ProjectID);
            if (dtQ.Rows.Count > 0)
            {
                divQuote.Visible = true;
                hypDoc.NavigateUrl = "~/userfiles/quotedoc/" + dtQ.Rows[0]["QuoteDoc"].ToString();
            }
            else
            {
                divQuote.Visible = false;
            }
            BindGrid(0);
        }
    }

    protected DataTable GetGridData()
    {
        string ProjectID = Request.QueryString["id"];
        DataTable dt = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
        if (dt.Rows.Count == 0)
        { }
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
                divPrint.Visible = false;
            }
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            PanNoRecord.Visible = false;
            divPrint.Visible = true;
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/customer/custproject.aspx");
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["id"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string CustomerID = st.CustomerID;
        string Feedback = txtFeedback.Text;

        int success = ClstblProjects.tblCustomerFeedback_Insert(CustomerID, ProjectID, Feedback);
        if (Convert.ToString(success) != string.Empty)
        {
            DataTable dt = ClstblProjects.tblCustomerFeedback_SelectByProjectID(ProjectID);
            if (dt.Rows.Count > 0)
            {
                txtFeedback.Enabled = false;
                divAddFeedback.Visible = false;
            }
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}