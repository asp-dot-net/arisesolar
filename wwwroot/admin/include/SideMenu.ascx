﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SideMenu.ascx.cs" Inherits="admin_include_SideMenu" %>



  <div class="page-sidebar " id="sidebar">
<aside id="menu" class="printorder">
    <div id="navigation" class="printorder" >
        <%--<div class="profile-picture">
         <%--   <a href="<%=Siteurl %>admin/adminfiles/dashboard.aspx">--%>
               <%-- <img src="<%=Siteurl %>admin/images/profile.jpg" class="img-circle m-b" alt="logo">--%>
<%--            </a>
            <div class="stats-label text-color">--%>
               <%-- <span class="font-extra-bold font-uppercase">
                    <asp:Label ID="lblRolename" runat="server"></asp:Label></span>--%>

              <%--  <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">--%>
                        <%--<small class="text-muted">
                           <%-- <asp:Label ID="lblUsserName" runat="server"></asp:Label><b class="caret"></b></small>--%>
                 <%-- </a>--%>
                   <%-- <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/admin/adminfiles/changepassword/Changepassword.aspx">Change Password</asp:HyperLink>
                        </li>
                        <li runat="server" visible="false"><a href="contacts.html">Contacts</a></li>
                        <li runat="server" visible="false"><a href="profile.html">Profile</a></li>
                        <li runat="server" visible="false"><a href="analytics.html">Analytics</a></li>
                        <li class="divider"></li>
                        <li>
                            <asp:LoginStatus ID="LoginStatus2" runat="server" LogoutText="Logout" Style="background: none; padding-right: 5px;"
                                OnLoggedOut="LoginStatus1_LoggedOut" LogoutAction="Redirect" />
                        </li>
                    </ul>--%>
           <%--     </div>--%>
               <%-- <div id="sparkline1" class="small-chart m-t-sm"></div>
                <div>
                    <h4 class="font-extra-bold m-b-xs">$260 104,200
                    </h4>
                    <small class="text-muted">Your income from the last year in sales product X.</small>
                </div>--%>
        <%--    </div>
        </div>--%>

        <ul class="nav sidebar-menu">
            <asp:Repeater runat="server" ID="menu1" OnItemDataBound="menu1_onitemdatabound" OnItemCommand="menu1_onitemcommand" >
                <ItemTemplate>
              <%--  class='<%# Eval("description").ToString().ToLower()==Utilities.GetPageName(Request.Url.ToString()).ToLower()?"active":"" %>'--%>
                    <li id="limain" runat="server" >
                         
                        <asp:HiddenField ID="hdndesc1" Value='<%# Eval("description").ToString()%>' runat="server" />

                           <asp:HiddenField ID="hdnmain"  runat="server" />
                        <asp:LinkButton ID="hdnheadermenu" CssClass="menu-dropdown" runat="server" CommandArgument='<%# Eval("url") %>' Target="_blank" href='<%# Eval("url") %>'
                            CausesValidation="false">
                            <i class='<%# Eval("description").ToString().Split(',').Count()>1?"menu-icon fa "+Eval("description").ToString().Split(',')[1]:""%>'></i> <span class="menu-text"><%# Eval("title")%></span>
                            <asp:Label CssClass="fa arrow" ID="lblarraow" runat="server" Visible="false"></asp:Label>
                        </asp:LinkButton>

                        </a>
                            <ul id="UL1" runat="server" class="submenu">
                                <asp:Repeater ID="repsubmenu" runat="server" OnItemDataBound="repsubmenu_OnItemDataBound">
                                    <ItemTemplate>
                                        <li id="lisub" runat="server">
                                            <asp:HyperLink ID="hypsubmenu" runat="server" NavigateUrl='<%# Eval("Url") %>' Target="_blank" Style="text-transform: capitalize;">
                                           <span class="menu-text"><%# Eval("title")%></span>
                                            </asp:HyperLink>

                                            <asp:HiddenField ID="hdndesc" Value='<%# Eval("description").ToString()%>' runat="server" />
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</aside>
</div>
