﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_include_Header : System.Web.UI.UserControl
{
    protected static string Siteurl;
    protected static string SiteName;
    protected static string noticount = "";
    //protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindHeader();
        }
    }

    public void BindHeader()
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        SiteName = st.sitename;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        stuser stu = ClsClientuser.ClientUserAspnetUserGetDataStructByUserId(userid);
        lbluseremail.Text = stu.email;
        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        lblusername.Text = stemp.EmpFirst + " " + stemp.EmpLast; ;
        noticount = "0";
        if (stemp.EmpImage != string.Empty)
        {            
            ////Image2.ImageUrl = "~/userfiles/EmployeeProfile/" + stemp.EmpImage;
            ////Image2.ImageUrl = pdfURL + "EmployeeProfile/" + stemp.EmpImage;
            //Image2.ImageUrl = SiteConfiguration.GetDocumnetPath("EmployeeProfile", stemp.EmpImage);
            //// panelImg.ImageUrl = "~/userfiles/EmployeeProfile/" + stemp.EmpImage;
            ////panelImg.ImageUrl = pdfURL + "EmployeeProfile/" + stemp.EmpImage;
            //panelImg.ImageUrl = SiteConfiguration.GetDocumnetPath("EmployeeProfile", stemp.EmpImage);

            Image2.ImageUrl = "~/images/defaultimage.jpg";
            panelImg.ImageUrl = "~/images/defaultimage.jpg";

        }
        else
        {
            Image2.ImageUrl = "~/images/defaultimage.jpg";
            panelImg.ImageUrl = "~/images/defaultimage.jpg";
        }
    }
    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        Session["userid"] = "";
        Session.Abandon();
        Response.Redirect("~/Default.aspx");
    }

    protected void panelImgLinkBtn_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderDetail.Show();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        if (stemp.EmpImage != string.Empty)
        {
            //Image1.ImageUrl = "~/userfiles/EmployeeProfile/" + stemp.EmpImage;
            //Image1.ImageUrl = pdfURL + "EmployeeProfile/" + stemp.EmpImage;
            Image1.ImageUrl = SiteConfiguration.GetDocumnetPath("EmployeeProfile", stemp.EmpImage);
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string profile = string.Empty;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        if (fuProfile.HasFile)
        {
            if ((System.IO.Path.GetExtension(fuProfile.FileName) == ".jpg") ||   (System.IO.Path.GetExtension(fuProfile.FileName) == ".jpeg")  ||   (System.IO.Path.GetExtension(fuProfile.FileName) == ".gif")  ||  (System.IO.Path.GetExtension(fuProfile.FileName) == ".png"))
            {              
                SiteConfiguration.DeletePDFFile("EmployeeProfile", stemp.EmpImage);
                profile = stemp.EmployeeID + fuProfile.FileName;                
                fuProfile.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\EmployeeProfile\\") + profile);
                bool sucMP = ClstblEmployees.tblEmployees_UpdateProfile(profile, userid);
                SiteConfiguration.UploadPDFFile("EmployeeProfile", profile);
                SiteConfiguration.deleteimage(profile, "EmployeeProfile");
                if (sucMP)
                {
                    ModalPopupExtenderDetail.Hide();
                    SetAdd1();
                    BindHeader();
                }
                else
                { SetError1(); }
            }
            else
            {
                ModalPopupExtenderDetail.Show();
                SetErrorImage();
            }
        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetErrorImage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunImageError();", true);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != string.Empty)
        {
            Response.Redirect("~/admin/adminfiles/searchdata.aspx?search=" + System.Web.HttpUtility.UrlEncode(txtSearch.Text));
        }
    }



    protected void singout_Click(object sender, EventArgs e)
    {
        Session["Uname"] = "";
        Session.Abandon();
        Response.Redirect("~/Default.aspx");
    }
}