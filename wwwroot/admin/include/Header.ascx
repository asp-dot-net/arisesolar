﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="admin_include_Header" %>
<!-- Navbar -->
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
<script src="<%=Siteurl %>admin/vendor/flot2/examples/shared/jquery-ui/jquery-ui.min.js"></script>

<style>
    .navbar .navbar-brand small img
    {
            height: 30px !important;
            width: 150px !important;
            padding-top: 6px !important;
    }
</style>

<script>
    function getimage(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#<%=Image1.ClientID %>')
                    .attr('src', e.target.result)
                    .width(128)
                    .height(128)
                    .title('');
            };
            reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<div class="navbar " id="header">
    <div class="navbar-inner">
        <div class="navbar-container">
            <!-- Navbar Barnd -->
            <div class="navbar-header pull-left" id="logo">
                <a href="#" class="navbar-brand">
                    <small>
                        <img src="<%=Siteurl %>admin/assets/img/logo_09-05-2018.png" alt="" />
                    </small>
                </a>
            </div>
           
            <div class="sidebar-collapse" id="sidebar-collapse">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>

            <div class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search" style="padding: 0px 80px;">
        <div class="form-group">
            <asp:Panel runat="server" ID="PanSearch" DefaultButton="btnSearch">
                <div class="input-group">
                    <span class="input-group-btn">
                        <asp:LinkButton runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></asp:LinkButton>
                        <%--<button type="submit" class="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></button>--%>
                    </span>
                    <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control input-sm no-border" placeholder="Search apps, projects..."></asp:TextBox>
                    <%--<input type="text" class="form-control input-sm no-border" placeholder="Search apps, projects...">--%>
                </div>
            </asp:Panel>
        </div>
    </div>

            <div class="navbar-header pull-right">

                <div class="navbar-account">
                    <ul class="account-area">


                        <li>

                            <a class="login-area dropdown-toggle" data-toggle="dropdown">
                                <div class="avatar" title="View your public profile" style="margin-right: 40px">
                                       <asp:Image runat="server" ID="Image2" class="avatar" Height="29px" Width="29px" />
                                </div>
                                <section>
                                    <h2><span class="profile"><span>
                                        <asp:Label ID="lblusername" runat="server" Text=""></asp:Label></span></span></h2>
                                </section>
                            </a>

                            <!--Login Area Dropdown-->
                            <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                                <li class="email" style="height:30px;text-align:center;padding-top:5px;color:black">
                                    <asp:Label  ID="lbluseremail" runat="server" Text=""></asp:Label></li>
                                <!--Avatar Area-->
                                <li>
                                    <div class="avatar-area">
                                        <asp:LinkButton ID="panelImgLinkBtn" runat="server" CommandArgument="Panel" OnClick="panelImgLinkBtn_Click">
                                            <asp:Image runat="server" ID="panelImg" class="avatar" />
                                            <span class="caption" style="margin-left: 2px;">Change Profile</span>
                                        </asp:LinkButton>
                                    </div>
                                </li>
                                <!--Avatar Area-->
                                <li class="edit">
                                        <a href="<%=Siteurl %>admin/Profile/Profile.aspx" class="pull-left">Profile</a>
                                        
                                    </li>
                                <li class="dropdown-footer">
                                    <asp:LinkButton ID="singout" runat="server" OnClick="singout_Click" > Sign out</asp:LinkButton>
                                    
                                </li>
                            </ul>
                            <!--/Login Area Dropdown-->
                        </li>
                        <!-- /Account Area -->
                        <!--Note: notice that setting div must start right after account area list.
                            no space must be between these elements-->
                        <!-- Settings -->
                    </ul>
                 
                </div>
            </div>
            <!-- /Account Area and Settings -->
        </div>
    </div>
</div>
<!-- /Navbar -->


<cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
    DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
    CancelControlID="lnkcancel">
</cc1:ModalPopupExtender>
<div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup">
    <div class="modal-dialog" style="width: 600px">
        <div class="modal-content">
            <div class="color-line printorder "></div>
            <div class="modal-header printorder">
                <div style="float: right">
                    <asp:LinkButton ID="lnkcancel" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                        Close
                    </asp:LinkButton>
                </div>

                <h4 class="modal-title" id="myModalLabel">Change Profile</h4>

            </div>

            <div class="modal-body paddnone" runat="server" id="divdetail">
                <div class="panel-body">
                    <div class="formainline">
                        <div class="">
                            <div class="" style="background: none!important;">
                                <div>
                                    <div class="col-md-6">
                                        <div class="form-group wd100">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                </label>
                                            </span><span>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table  quotetable">


                                                    <tr>
                                                        <td style="width: 20%; border-top: none">Change Profile
                                                        </td>
                                                        <td style="width: 70%; border-top: none">
                                                            <div class="form-group dateimgarea fileuploadmain col-md-6">
                                                                <span class="dateimg">
                                                                    <span class="file-input btn btn-azure btn-file">
                                                                        <asp:Image runat="server" ID="Image1" class="avatar" Style="width: 128px; height: 128px;" />
                                                                        <span class="caption">
                                                                            <asp:FileUpload ID="fuProfile" onchange="getimage(this)" runat="server" />

                                                                        </span>

                                                                    </span>
                                                                </span>

                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; border-top: none"></td>
                                                        <td style="width: 70%; border-top: none">
                                                            
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="fileis"
                                                                ControlToValidate="fuProfile" Display="Dynamic" ForeColor="Red" ErrorMessage="Upload a valid file"
                                                                ValidationExpression="^.+(.jpg|.JPG|.jpeg|.JPEG|.gif|.GIF|.png|.PNG)$"></asp:RegularExpressionValidator>
                                                            <span style="float: right;">&nbsp;&nbsp;<small><i> [Note:only jpg, jpeg, gif, png, bmp, ico]</i></small></span>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group marginleft text-center col-md-12">
                                        <asp:Button class="btn btn-primary addwhiteicon" ID="btnSave" ValidationGroup="fileis" CausesValidation="true" runat="server" OnClick="btnSave_Click" Text="Update" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:Button ID="btnNULL" Style="display: none;" runat="server" />