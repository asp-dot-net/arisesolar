<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="adminutilities.aspx.cs" Inherits="admin_adminfiles_utilities_adminutilities"
    Theme="admin" Culture="en-us" UICulture="en-us" EnableEventValidation="true"
    ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div id="breadCrumbs">
        <ul>
            <li class="breadCrumbLast" title="Utilities"><a href="#"><strong>Utilities</strong></a></li>
        </ul>
    </div>
    <div id="main">
        <div id="globalTabContent">
            <ul class="tabs-nav">
                <li class="tabs-selected"><a href="#tab-1"><span><strong>
                    <asp:Literal ID="lblAddUpdate" runat="server" Text=""></asp:Literal>
                    Utilities</strong></span></a></li>

            </ul>
            <div id="tab-1" class="tabs-container">
                <div class="curveTop">
                </div>

                <%-- START  : Error Panels --%>
                <div style="padding: 10px!important;">
                    <asp:Panel ID="PanSuccess" runat="server" CssClass="tick">
                        <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanError" runat="server" CssClass="cross">
                        <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanNoRecord" runat="server" CssClass="cross">
                        <asp:Label ID="lblNoRecord" runat="server" Text="There are no items to show in this view."></asp:Label>
                    </asp:Panel>
                </div>
                <%-- END    : Error Panels --%>

                <%-- START  : ADD / UPDATE Panels --%>
                <asp:Panel ID="PanAddUpdate" runat="server" Visible="false">
                    <div class="globalContainerCA">
                        <ul class="addNewPage" style="width: 100%;">
                            <li>
                                <label style="float: left;">
                                    <strong>Site Name</strong><span class="highlight"></span></label>
                                <asp:TextBox ID="tbsitename" runat="server" Width="200" MaxLength="100"></asp:TextBox>

                            </li>


                            <li>
                                <label style="float: left;">
                                    <strong>Site URL</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="tbsiteurl" runat="server" MaxLength="100" Width="200"></asp:TextBox>


                            </li>
                            <li>
                                <label style="float: left;">
                                    <strong>MailServer</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtMailServer" runat="server" MaxLength="100" Width="200"></asp:TextBox>


                            </li>

                            <li>

                                <label style="float: left;">
                                    <strong>From</strong><span class="highlight"></span></label>
                                <asp:TextBox ID="txtfrom" runat="server" MaxLength="100" Width="200"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter Valid Email Address"
                                    ControlToValidate="txtfrom" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>


                            </li>


                            <li>
                                <label style="float: left;">
                                    <strong>CC</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtcc" runat="server" MaxLength="500" Width="200"></asp:TextBox>


                            </li>
                            <li>
                                <label style="float: left;">
                                    <strong>Authenticate</strong><span class="highlight"></span></label>

                                <asp:DropDownList ID="ddlauthenticate" runat="server" AppendDataBoundItems="true"
                                    OnSelectedIndexChanged="ddlauthenticate_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">No Othenticate</asp:ListItem>
                                    <asp:ListItem Value="1">Simple Othenticate</asp:ListItem>
                                    <asp:ListItem Value="2">G-Mail Othenticate</asp:ListItem>
                                </asp:DropDownList>



                            </li>

                            <li runat="server" visible="false">
                                <label id="trusername" style="float: left;" runat="server" visible="false">
                                    <strong>username</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtusername" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>
                            <li runat="server" visible="false">
                                <label id="trpassword" style="float: left;" runat="server" visible="false">
                                    <strong>Password</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtpassword" runat="server" MaxLength="10" Width="200"></asp:TextBox>


                            </li>


                            <li runat="server" visible="false">

                                <label id="trsslportno" style="float: left;" runat="server" visible="false">
                                    <strong>EmailSSLPortNo</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtsslportno" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>

                            <li runat="server" visible="false">

                                <label id="trsslportvalue" style="float: left;" runat="server" visible="false">
                                    <strong>EmailSSLValue</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtsslvalue" runat="server" MaxLength="50" Width="200"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Order Subject</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtordersubject" runat="server" MaxLength="500" Width="300"></asp:TextBox>


                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Order Thanks Message</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtorderthanksmessage" runat="server" MaxLength="500" Width="300"
                                    TextMode="MultiLine" Height="100"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Contact us subject</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtcontactussubject" runat="server" MaxLength="500" Width="300"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Contact Us Thanks message</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtcontactusthenksmsg" runat="server" MaxLength="500" Width="300"
                                    TextMode="MultiLine" Height="100"></asp:TextBox>

                            </li>


                            <li>
                                <label style="float: left;">
                                    <strong>Regards Name</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtregardsname" runat="server" MaxLength="500" Width="300" TextMode="MultiLine"
                                    Height="100"></asp:TextBox>


                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Email Top</strong><span class="highlight"></span></label>

                                <asp:FileUpload ID="fulemailtop" runat="server" />
                                <asp:Image ID="imageemailtop" runat="server" Height="50px" Width="50px" />

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Email Bottom</strong><span class="highlight"></span></label>

                                <asp:FileUpload ID="fulemailbottom" runat="server" />
                                <asp:Image ID="imageemailbottom" runat="server" Height="50px" Width="50px" />


                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Email Body Color</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtemailbodycolor" runat="server" MaxLength="500" Width="200"></asp:TextBox>


                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Site Logo Upload</strong><span class="highlight"></span></label>

                                <asp:FileUpload ID="fulsitelogo" runat="server" />
                                <asp:Image ID="imagesitelogo" runat="server" Height="50px" Width="50px" />


                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Admin Footer</strong><span class="highlight"></span></label>

                                <%-- <FCKeditorV2:FCKeditor Height="250px" Width="550px" ID="txtadminfooter" runat="server" Value="">
                                        </FCKeditorV2:FCKeditor>
                                        <asp:CustomValidator runat="server" ID="CustomValidator1" ControlToValidate="txtadminfooter"
                                            SetFocusOnError="true" Display="dynamic" ErrorMessage="*" ClientValidationFunction="ValidateContentText"
                                            ValidateEmptyText="true"></asp:CustomValidator>
											 
                                        <asp:TextBox ID="txtadminfooter" runat="server" MaxLength="500" Width="200"></asp:TextBox>--%>
                                  
                               
                            </li>
                            <li>
                                <label style="float: left;">
                                    <strong>Paypal Account Name</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtpaypalaccountname" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>


                            <li>
                                <label style="float: left;">
                                    <strong>Paypal mode</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtpaypalmode" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Paypal Currency Symbol</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtcurrencysymbol" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Port</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtport" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>

  
                            <li>
                                <label style="float: left;">
                                    <strong>Pagenation</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtpagination" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                <cc1:filteredtextboxextender id="FilteredTextBoxExtender1" runat="server"
                                    enabled="True" filtertype="Numbers" targetcontrolid="txtpagination">
                                                                      </cc1:filteredtextboxextender>


                            </li>


                            <li>
                                <label style="float: left;">
                                    <strong>Site By Name</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtsitename" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Site By Url</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtsitebyurl" runat="server" MaxLength="500" Width="200"></asp:TextBox>

                            </li>

                            

                            <li>



                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/admin/images/save.gif"
                                        OnClick="btnUpdate_Click" Visible="true" />
                                </td>

                            </li>





                        </ul>
                        <%-- <ul class="addNewPageEditor" runat="server" visible="false">
                            <li class="floatLeft">

                                <p>
                                    <asp:ImageButton ID="btnUpdate1" runat="server" ImageUrl="~/admin/images/save.gif"
                                        OnClick="btnUpdate_Click" Visible="false" />
                                </p>
                               
                            </li>
                        </ul>--%>
                        <div class="clear">
                        </div>
                    </div>
                </asp:Panel>


                <div class="curveBottom">
                    <img height="8" width="8" src="../../images/bl.jpg" alt="" class="curveCorner" />
                </div>
            </div>
        </div>
    </div>


</asp:Content>
