using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Default : System.Web.UI.Page
{
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        if (Request.IsAuthenticated)
        {
            Response.Redirect("~/admin/adminfiles/dashboard.aspx");
        }
    }
    protected void SubmitButton_Click(object sender, EventArgs e)
    {

    }
    protected void VerifyingUser(object sender, System.Web.UI.WebControls.LoginCancelEventArgs e)
    {
        //   Response.End();

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/default.aspx");
    }
    protected void SubmitButton_Click1(object sender, EventArgs e)
    {
        string username = UserName.Text;
        int exist = ClsClientuser.forgorpassword_Username_Exists(username);
        if (exist == 1)
        {
            //  StCrm_Utilities St = ClsAdminUtilities.SpCrm_UtilitiesGetDataStructById("1");
            stuser st_User = ClsClientuser.ClientUserAspnetUserGetDataStructByUserName(username);
            string subject = "Forgot Password";
            string from = "bansi@meghtechnologies.com";

            string to = st_User.email;
            string password = st_User.password;
            string fullname = "";
            fullname = st_User.username;
            TextWriter txtWriter = new StringWriter() as TextWriter;
            Server.Execute("~/mailtemplate/forgotpassword.aspx?username=" + username + "&password=" + password + "&fullname=" + fullname, txtWriter);

            try
            {
                Utilities.SendMail(from, to, subject, txtWriter.ToString());
            }
            catch
            {
            }
            SetAdd1();
            //PanSuccess.Visible = true;
            Panfail.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MyAction", "MyfunSuccessforgotPassword()", true);
            tabform.Visible = true;
            UserName.Text = string.Empty;

        }
        else
        {
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "MyAction", "MyfunErrorforgotPassword()", true);
            Panfail.Visible = true;
            PanSuccess.Visible = false;
            tabform.Visible = true;
            UserName.Text = string.Empty;
        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}