namespace EurosolarReporting
{
    partial class STCForm
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(STCForm));
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup90 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup91 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup92 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup93 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup94 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup95 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup96 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup97 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup98 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup99 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup100 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup101 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup102 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup103 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup104 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup105 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup106 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup107 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup108 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup109 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup110 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup111 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup112 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup113 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup114 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup115 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup116 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup117 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup118 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup119 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup120 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup121 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup122 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup123 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup124 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup125 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup126 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup127 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup128 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup129 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup130 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup131 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup132 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup133 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup134 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup135 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup136 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup137 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup138 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup139 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup140 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup141 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup142 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup143 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup144 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup145 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup146 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup147 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup148 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup149 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup150 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup151 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup152 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup153 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup154 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup155 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup156 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup157 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup158 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup159 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup160 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup161 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup162 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup163 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup164 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup165 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup166 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup167 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup168 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup169 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup170 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup171 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup172 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup173 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.textBox172 = new Telerik.Reporting.TextBox();
            this.textBox175 = new Telerik.Reporting.TextBox();
            this.textBox170 = new Telerik.Reporting.TextBox();
            this.txtdate = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.ContFirst = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.ContLast = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.lblInstallBookingDate = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.chkBuilding = new Telerik.Reporting.CheckBox();
            this.chkResidential = new Telerik.Reporting.CheckBox();
            this.chkschool = new Telerik.Reporting.CheckBox();
            this.chkCommercial = new Telerik.Reporting.CheckBox();
            this.checkBox6 = new Telerik.Reporting.CheckBox();
            this.checkBox7 = new Telerik.Reporting.CheckBox();
            this.checkBox8 = new Telerik.Reporting.CheckBox();
            this.checkBox9 = new Telerik.Reporting.CheckBox();
            this.checkBox10 = new Telerik.Reporting.CheckBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.checkBox11 = new Telerik.Reporting.CheckBox();
            this.checkBox12 = new Telerik.Reporting.CheckBox();
            this.checkBox13 = new Telerik.Reporting.CheckBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.chkGround = new Telerik.Reporting.CheckBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.checkBox14 = new Telerik.Reporting.CheckBox();
            this.checkBox15 = new Telerik.Reporting.CheckBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.checkBox1 = new Telerik.Reporting.CheckBox();
            this.checkBox2 = new Telerik.Reporting.CheckBox();
            this.checkBox3 = new Telerik.Reporting.CheckBox();
            this.checkBox4 = new Telerik.Reporting.CheckBox();
            this.checkBox5 = new Telerik.Reporting.CheckBox();
            this.checkBox16 = new Telerik.Reporting.CheckBox();
            this.checkBox17 = new Telerik.Reporting.CheckBox();
            this.checkBox18 = new Telerik.Reporting.CheckBox();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox158 = new Telerik.Reporting.TextBox();
            this.textBox173 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox159 = new Telerik.Reporting.TextBox();
            this.textBox162 = new Telerik.Reporting.TextBox();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox169 = new Telerik.Reporting.TextBox();
            this.textBox171 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.table12 = new Telerik.Reporting.Table();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox161 = new Telerik.Reporting.TextBox();
            this.textBox163 = new Telerik.Reporting.TextBox();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table13 = new Telerik.Reporting.Table();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.table14 = new Telerik.Reporting.Table();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox177 = new Telerik.Reporting.TextBox();
            this.textBox179 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox178 = new Telerik.Reporting.TextBox();
            this.textBox180 = new Telerik.Reporting.TextBox();
            this.textBox181 = new Telerik.Reporting.TextBox();
            this.textBox182 = new Telerik.Reporting.TextBox();
            this.textBox183 = new Telerik.Reporting.TextBox();
            this.textBox184 = new Telerik.Reporting.TextBox();
            this.textBox185 = new Telerik.Reporting.TextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox187 = new Telerik.Reporting.TextBox();
            this.textBox188 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox193 = new Telerik.Reporting.TextBox();
            this.textBox194 = new Telerik.Reporting.TextBox();
            this.textBox195 = new Telerik.Reporting.TextBox();
            this.textBox196 = new Telerik.Reporting.TextBox();
            this.textBox197 = new Telerik.Reporting.TextBox();
            this.textBox198 = new Telerik.Reporting.TextBox();
            this.textBox199 = new Telerik.Reporting.TextBox();
            this.textBox200 = new Telerik.Reporting.TextBox();
            this.textBox215 = new Telerik.Reporting.TextBox();
            this.textBox210 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox202 = new Telerik.Reporting.TextBox();
            this.textBox201 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.txtpaymnetfrm = new Telerik.Reporting.HtmlTextBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.panel5 = new Telerik.Reporting.Panel();
            this.htmlTextBox9 = new Telerik.Reporting.HtmlTextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.lblProjectNumber4 = new Telerik.Reporting.TextBox();
            this.lblInstallerName7 = new Telerik.Reporting.TextBox();
            this.lblFinanceOption = new Telerik.Reporting.TextBox();
            this.lblProjectNumber5 = new Telerik.Reporting.TextBox();
            this.table15 = new Telerik.Reporting.Table();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox211 = new Telerik.Reporting.TextBox();
            this.litTitle = new Telerik.Reporting.TextBox();
            this.textBox216 = new Telerik.Reporting.TextBox();
            this.textBox217 = new Telerik.Reporting.TextBox();
            this.textBox218 = new Telerik.Reporting.TextBox();
            this.textBox222 = new Telerik.Reporting.TextBox();
            this.textBox226 = new Telerik.Reporting.TextBox();
            this.textBox230 = new Telerik.Reporting.TextBox();
            this.textBox232 = new Telerik.Reporting.TextBox();
            this.textBox233 = new Telerik.Reporting.TextBox();
            this.textBox234 = new Telerik.Reporting.TextBox();
            this.textBox227 = new Telerik.Reporting.TextBox();
            this.textBox219 = new Telerik.Reporting.TextBox();
            this.textBox207 = new Telerik.Reporting.TextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.textBox213 = new Telerik.Reporting.TextBox();
            this.textBox214 = new Telerik.Reporting.TextBox();
            this.textBox223 = new Telerik.Reporting.TextBox();
            this.textBox206 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            this.textBox204 = new Telerik.Reporting.TextBox();
            this.textBox224 = new Telerik.Reporting.TextBox();
            this.textBox225 = new Telerik.Reporting.TextBox();
            this.htmlTextBox11 = new Telerik.Reporting.HtmlTextBox();
            this.panel6 = new Telerik.Reporting.Panel();
            this.pictureBox8 = new Telerik.Reporting.PictureBox();
            this.htmlTextBox10 = new Telerik.Reporting.HtmlTextBox();
            this.pictureBox9 = new Telerik.Reporting.PictureBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.lblInverterModel2 = new Telerik.Reporting.TextBox();
            this.lblInverterBrand2 = new Telerik.Reporting.TextBox();
            this.lblInverterSeries2 = new Telerik.Reporting.TextBox();
            this.pictureBox10 = new Telerik.Reporting.PictureBox();
            this.ProjectNumber3 = new Telerik.Reporting.TextBox();
            this.litPanelDetails = new Telerik.Reporting.TextBox();
            this.lblMeterPhase = new Telerik.Reporting.TextBox();
            this.lblOffPeak = new Telerik.Reporting.TextBox();
            this.lblRetailer = new Telerik.Reporting.TextBox();
            this.lblPeakMeterNo = new Telerik.Reporting.TextBox();
            this.textBox221 = new Telerik.Reporting.TextBox();
            this.panel3 = new Telerik.Reporting.Panel();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.htmlTextBox8 = new Telerik.Reporting.HtmlTextBox();
            this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox228 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.228571891784668D), Telerik.Reporting.Drawing.Unit.Cm(0.34416663646698D));
            this.textBox8.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Arial";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "Is the System installed on or as";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.34416663646698D));
            this.textBox38.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Arial";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "Property Type";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.34416663646698D));
            this.textBox36.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Arial";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "System type";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4426338672637939D), Telerik.Reporting.Drawing.Unit.Cm(0.34416663646698D));
            this.textBox27.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Arial";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "System type";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6447982788085938D), Telerik.Reporting.Drawing.Unit.Cm(0.34416663646698D));
            this.textBox22.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Arial";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "STC Deeming Period:";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.39635789394378662D));
            this.textBox9.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Arial";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "CEC INSTALLER SECTION: (Here we need to know the installer, the designer and the " +
    "electricians details)";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.4235420823097229D));
            this.textBox56.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox56.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox56.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox56.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox56.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.Font.Name = "Arial";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.Value = "MANDATORY WRITTEN STATEMENT BY THE CEC INSTALLER AND DESIGNER";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox134.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox134.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox134.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox134.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox134.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox134.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox134.Style.Font.Bold = true;
            this.textBox134.Style.Font.Name = "Arial";
            this.textBox134.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox134.Value = "CUSTOMER GST DECLARATION";
            // 
            // textBox168
            // 
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.32187557220459D), Telerik.Reporting.Drawing.Unit.Cm(0.58204621076583862D));
            this.textBox168.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox168.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox168.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox168.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox168.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox168.Style.Font.Bold = true;
            this.textBox168.Style.Font.Name = "Arial";
            this.textBox168.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox168.Value = "Customer Acknowledgement";
            // 
            // textBox172
            // 
            this.textBox172.Name = "textBox172";
            this.textBox172.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2835731506347656D), Telerik.Reporting.Drawing.Unit.Cm(0.58204621076583862D));
            this.textBox172.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox172.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox172.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox172.Style.Font.Bold = true;
            this.textBox172.Style.Font.Name = "Arial";
            this.textBox172.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox172.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox172.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox172.Value = "Customer No : ";
            // 
            // textBox175
            // 
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3845508098602295D), Telerik.Reporting.Drawing.Unit.Cm(0.58204621076583862D));
            this.textBox175.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox175.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox175.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox175.Style.Font.Bold = true;
            this.textBox175.Style.Font.Name = "Arial";
            this.textBox175.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox175.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox175.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox175.StyleName = "";
            this.textBox175.Value = "=Fields.ProjectNumber";
            // 
            // textBox170
            // 
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989997863769531D), Telerik.Reporting.Drawing.Unit.Cm(0.608585774898529D));
            this.textBox170.Style.BorderColor.Left = System.Drawing.Color.White;
            this.textBox170.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox170.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox170.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox170.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox170.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox170.Style.Font.Name = "Arial";
            this.textBox170.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox170.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox170.Value = "QUALITY OF SERVICE (1=POOR, 2=AVERAGE, 3=GOOD, 4=VERY GOOD)";
            // 
            // txtdate
            // 
            this.txtdate.Height = Telerik.Reporting.Drawing.Unit.Inch(66.017471313476562D);
            this.txtdate.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table2,
            this.table3,
            this.table5,
            this.table6,
            this.table7,
            this.table8,
            this.textBox117,
            this.table9,
            this.table10,
            this.table11,
            this.table12,
            this.panel1,
            this.table13,
            this.table14,
            this.panel2,
            this.txtpaymnetfrm,
            this.pictureBox5,
            this.panel5,
            this.pictureBox7,
            this.lblProjectNumber4,
            this.lblInstallerName7,
            this.lblFinanceOption,
            this.lblProjectNumber5,
            this.table15,
            this.panel6,
            this.pictureBox9,
            this.textBox129,
            this.lblInverterModel2,
            this.lblInverterBrand2,
            this.lblInverterSeries2,
            this.pictureBox10,
            this.ProjectNumber3,
            this.litPanelDetails,
            this.lblMeterPhase,
            this.lblOffPeak,
            this.lblRetailer,
            this.lblPeakMeterNo,
            this.textBox221,
            this.panel3,
            this.textBox228});
            this.txtdate.Name = "txtdate";
            this.txtdate.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtdate.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtdate.Style.Font.Name = "Open Sans";
            this.txtdate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtdate.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtdate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.txtdate.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.txtdate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtdate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.19231653213501D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8785037994384766D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.1923174858093262D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8785037994384766D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8483579158782959D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.37000000476837158D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox3);
            this.table1.Body.SetCellContent(0, 1, this.textBox6);
            this.table1.Body.SetCellContent(0, 2, this.textBox93);
            this.table1.Body.SetCellContent(0, 3, this.textBox4);
            this.table1.Body.SetCellContent(0, 4, this.textBox120);
            tableGroup1.Name = "tableGroup";
            tableGroup2.Name = "group1";
            tableGroup3.Name = "tableGroup1";
            tableGroup4.Name = "group";
            tableGroup5.Name = "tableGroup2";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3,
            this.textBox6,
            this.textBox93,
            this.textBox4,
            this.textBox120});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010002215276472271D), Telerik.Reporting.Drawing.Unit.Cm(2.5232631983840292E-08D));
            this.table1.Name = "table1";
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup6.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup6);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.37000000476837158D));
            this.table1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.table1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.table1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.19231653213501D), Telerik.Reporting.Drawing.Unit.Cm(0.37000000476837158D));
            this.textBox3.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Arial";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "OWNER DETAILS";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8785040378570557D), Telerik.Reporting.Drawing.Unit.Cm(0.37000000476837158D));
            this.textBox6.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Arial";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "Quote No :";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1923174858093262D), Telerik.Reporting.Drawing.Unit.Cm(0.37000000476837158D));
            this.textBox93.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox93.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox93.Style.Font.Bold = true;
            this.textBox93.Style.Font.Name = "Arial";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.StyleName = "";
            this.textBox93.Value = "=Fields.ManualQuoteNumber";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8785040378570557D), Telerik.Reporting.Drawing.Unit.Cm(0.37000000476837158D));
            this.textBox4.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Arial";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "Customer No :";
            // 
            // textBox120
            // 
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8483579158782959D), Telerik.Reporting.Drawing.Unit.Cm(0.37000000476837158D));
            this.textBox120.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox120.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox120.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox120.Style.Font.Bold = true;
            this.textBox120.Style.Font.Name = "Arial";
            this.textBox120.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox120.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox120.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox120.StyleName = "";
            this.textBox120.Value = "=Fields.ProjectNumber";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5115234851837158D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8552079200744629D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2627663612365723D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.817946195602417D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8133386373519898D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4167793989181519D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9455229043960571D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6018387079238892D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.363906741142273D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.4011693000793457D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.42386737465858459D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34443184733390808D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.63272500038146973D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.36793652176856995D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox5);
            this.table2.Body.SetCellContent(0, 1, this.ContFirst);
            this.table2.Body.SetCellContent(0, 4, this.textBox14);
            this.table2.Body.SetCellContent(0, 3, this.ContLast);
            this.table2.Body.SetCellContent(0, 2, this.textBox18);
            this.table2.Body.SetCellContent(1, 0, this.textBox2);
            this.table2.Body.SetCellContent(1, 4, this.textBox13);
            this.table2.Body.SetCellContent(1, 5, this.textBox15);
            this.table2.Body.SetCellContent(1, 6, this.textBox17);
            this.table2.Body.SetCellContent(1, 7, this.textBox19);
            this.table2.Body.SetCellContent(1, 8, this.textBox21);
            this.table2.Body.SetCellContent(1, 9, this.textBox23);
            this.table2.Body.SetCellContent(3, 0, this.textBox25);
            this.table2.Body.SetCellContent(3, 4, this.textBox29);
            this.table2.Body.SetCellContent(3, 5, this.textBox30);
            this.table2.Body.SetCellContent(3, 6, this.textBox31);
            this.table2.Body.SetCellContent(3, 7, this.textBox32);
            this.table2.Body.SetCellContent(3, 8, this.textBox33);
            this.table2.Body.SetCellContent(3, 9, this.textBox34);
            this.table2.Body.SetCellContent(2, 0, this.textBox45);
            this.table2.Body.SetCellContent(2, 1, this.textBox46);
            this.table2.Body.SetCellContent(2, 2, this.textBox47);
            this.table2.Body.SetCellContent(2, 3, this.textBox48);
            this.table2.Body.SetCellContent(2, 4, this.textBox49);
            this.table2.Body.SetCellContent(4, 3, this.textBox68);
            this.table2.Body.SetCellContent(1, 1, this.textBox7, 1, 3);
            this.table2.Body.SetCellContent(0, 5, this.textBox12, 1, 5);
            this.table2.Body.SetCellContent(2, 5, this.textBox50, 1, 5);
            this.table2.Body.SetCellContent(3, 1, this.textBox26, 1, 3);
            this.table2.Body.SetCellContent(4, 8, this.textBox73, 1, 2);
            this.table2.Body.SetCellContent(4, 0, this.htmlTextBox2, 1, 3);
            this.table2.Body.SetCellContent(4, 4, this.htmlTextBox3, 1, 4);
            this.table2.Body.SetCellContent(5, 0, this.textBox55, 1, 4);
            this.table2.Body.SetCellContent(5, 4, this.textBox59, 1, 6);
            this.table2.Body.SetCellContent(6, 0, this.textBox35, 1, 10);
            tableGroup7.Name = "tableGroup3";
            tableGroup8.Name = "tableGroup4";
            tableGroup9.Name = "group5";
            tableGroup10.Name = "group4";
            tableGroup11.Name = "group3";
            tableGroup12.Name = "group2";
            tableGroup13.Name = "group8";
            tableGroup14.Name = "group7";
            tableGroup15.Name = "group6";
            tableGroup16.Name = "tableGroup5";
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.ColumnGroups.Add(tableGroup12);
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.ColumnGroups.Add(tableGroup14);
            this.table2.ColumnGroups.Add(tableGroup15);
            this.table2.ColumnGroups.Add(tableGroup16);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.ContFirst,
            this.textBox18,
            this.ContLast,
            this.textBox14,
            this.textBox12,
            this.textBox2,
            this.textBox7,
            this.textBox13,
            this.textBox15,
            this.textBox17,
            this.textBox19,
            this.textBox21,
            this.textBox23,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox25,
            this.textBox26,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.htmlTextBox2,
            this.textBox68,
            this.htmlTextBox3,
            this.textBox73,
            this.textBox55,
            this.textBox59,
            this.textBox35});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.3941670358181D));
            this.table2.Name = "table2";
            tableGroup18.Name = "group9";
            tableGroup19.Name = "group10";
            tableGroup20.Name = "group13";
            tableGroup21.Name = "group11";
            tableGroup22.Name = "group15";
            tableGroup23.Name = "group14";
            tableGroup24.Name = "group12";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.ChildGroups.Add(tableGroup19);
            tableGroup17.ChildGroups.Add(tableGroup20);
            tableGroup17.ChildGroups.Add(tableGroup21);
            tableGroup17.ChildGroups.Add(tableGroup22);
            tableGroup17.ChildGroups.Add(tableGroup23);
            tableGroup17.ChildGroups.Add(tableGroup24);
            tableGroup17.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup17.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup17);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(3.119999885559082D));
            this.table2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table2.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5115234851837158D), Telerik.Reporting.Drawing.Unit.Cm(0.42386746406555176D));
            this.textBox5.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox5.Style.Font.Name = "Arial";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Value = "First Name :";
            // 
            // ContFirst
            // 
            this.ContFirst.Name = "ContFirst";
            this.ContFirst.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8552083969116211D), Telerik.Reporting.Drawing.Unit.Cm(0.42386746406555176D));
            this.ContFirst.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.ContFirst.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.ContFirst.Style.Font.Name = "Arial";
            this.ContFirst.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ContFirst.Value = "=Fields.ContFirst";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8133386373519898D), Telerik.Reporting.Drawing.Unit.Cm(0.42386746406555176D));
            this.textBox14.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox14.Style.Font.Name = "Arial";
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "Company :";
            // 
            // ContLast
            // 
            this.ContLast.Name = "ContLast";
            this.ContLast.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8179464340209961D), Telerik.Reporting.Drawing.Unit.Cm(0.42386746406555176D));
            this.ContLast.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.ContLast.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.ContLast.Style.Font.Name = "Arial";
            this.ContLast.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ContLast.StyleName = "";
            this.ContLast.Value = "=Fields.ContLast";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2627663612365723D), Telerik.Reporting.Drawing.Unit.Cm(0.42386746406555176D));
            this.textBox18.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox18.Style.Font.Name = "Arial";
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "Last Name :";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5115234851837158D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox2.Style.Font.Name = "Arial";
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "Postal Address :";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8133386373519898D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox13.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox13.Style.Font.Name = "Arial";
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "State :";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4167795181274414D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox15.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.Font.Name = "Arial";
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "=Fields.PostalState";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9455230236053467D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox17.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox17.Style.Font.Name = "Arial";
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "Post Code :";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6018387079238892D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox19.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox19.Style.Font.Name = "Arial";
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "=Fields.PostalPostCode";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.363906741142273D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox21.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox21.Style.Font.Name = "Arial";
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "Country:";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4011693000793457D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox23.Style.Font.Name = "Arial";
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "Aus";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5115234851837158D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox25.Style.Font.Name = "Arial";
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "Install Address :";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8133386373519898D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox29.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.Style.Font.Name = "Arial";
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "State :";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4167795181274414D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox30.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.Font.Name = "Arial";
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox30.StyleName = "";
            this.textBox30.Value = "=Fields.InstallState";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9455230236053467D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.Font.Name = "Arial";
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox31.StyleName = "";
            this.textBox31.Value = "Post Code :";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6018387079238892D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.Font.Name = "Arial";
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.StyleName = "";
            this.textBox32.Value = "=Fields.InstallPostCode";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.363906741142273D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.Font.Name = "Arial";
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "Country:";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4011693000793457D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.Font.Name = "Arial";
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "Aus";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5115234851837158D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox45.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox45.Style.Font.Name = "Arial";
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "Contact No:";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8552083969116211D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox46.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox46.Style.Font.Name = "Arial";
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox46.StyleName = "";
            this.textBox46.TocText = "=Fields.CustPhone";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2627663612365723D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox47.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox47.Style.Font.Name = "Arial";
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox47.StyleName = "";
            this.textBox47.Value = "Fax No :";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8179464340209961D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox48.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox48.Style.Font.Name = "Arial";
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox48.StyleName = "";
            this.textBox48.Value = "=Fields.CustFax";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8133386373519898D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox49.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox49.Style.Font.Name = "Arial";
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox49.StyleName = "";
            this.textBox49.Value = "Email :";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8179464340209961D), Telerik.Reporting.Drawing.Unit.Cm(0.34443178772926331D));
            this.textBox68.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox68.Style.Font.Name = "Arial";
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox68.StyleName = "";
            this.textBox68.Value = "=IIf(Fields.AdditionalSystem=\"True\",\"Yes\",\"No\")";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9359211921691895D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox7.Style.Font.Name = "Arial";
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "=Fields.PostalAddress + \", \" + Fields.PostalCity";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7292170524597168D), Telerik.Reporting.Drawing.Unit.Cm(0.42386746406555176D));
            this.textBox12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox12.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.Font.Name = "Arial";
            this.textBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "=Fields.Customer";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7292170524597168D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox50.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox50.Style.Font.Name = "Arial";
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "=Fields.ContEmail";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9359211921691895D), Telerik.Reporting.Drawing.Unit.Cm(0.45034635066986084D));
            this.textBox26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.Font.Name = "Arial";
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "=Fields.InstallAddress + \", \" + Fields.InstallCity";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7650761604309082D), Telerik.Reporting.Drawing.Unit.Cm(0.34443178772926331D));
            this.textBox73.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox73.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox73.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox73.Style.Font.Name = "Arial";
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox73.StyleName = "";
            this.textBox73.Value = "= IIf(Fields.ProjectTypeID=\"2\",\"Yes\",\"No\")";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.62949800491333D), Telerik.Reporting.Drawing.Unit.Cm(0.34443178772926331D));
            this.htmlTextBox2.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.htmlTextBox2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.htmlTextBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox2.Style.Font.Name = "Arial";
            this.htmlTextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.htmlTextBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox2.StyleName = "";
            this.htmlTextBox2.Value = "<b><span style=\"font-size: 7.5pt; \">SYSTEM DETAILS: </span></b><span style=\"font-" +
    "size: 7.5pt; \">Is this system </span><b><span style=\"font-size: 7.5pt; \">ADDITIO" +
    "NAL?</span></b>";
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7774796485900879D), Telerik.Reporting.Drawing.Unit.Cm(0.34443178772926331D));
            this.htmlTextBox3.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.htmlTextBox3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox3.Style.Font.Name = "Arial";
            this.htmlTextBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox3.StyleName = "";
            this.htmlTextBox3.Value = "Are you installing a COMPLETE Unit?";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.447444915771484D), Telerik.Reporting.Drawing.Unit.Cm(0.63272511959075928D));
            this.textBox55.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox55.Style.Font.Name = "Arial";
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox55.StyleName = "";
            this.textBox55.Value = "(Are you adding extra capacity panels or completely own system which is in additi" +
    "on to an existing system for this address?)";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.5425558090209961D), Telerik.Reporting.Drawing.Unit.Cm(0.63272511959075928D));
            this.textBox59.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox59.Style.Font.Name = "Arial";
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox59.StyleName = "";
            this.textBox59.Value = "(Adding additional capacity panels to an existing system is NOT considered a comp" +
    "lete unit)";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.36793655157089233D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox35.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.Font.Name = "Arial";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox35.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "If YES,the system is additional, please specify where the panels or system is in " +
    "relation to the existing system:(e.g. additional 8 panels added to the end of ex" +
    "isting 8 panels)";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7331242561340332D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4954476356506348D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4954476356506348D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4426336288452148D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4954476356506348D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9743287563323975D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1750226020812988D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.44999980926513672D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.317708283662796D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34416651725769043D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.88219332695007324D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.67052698135375977D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.61760973930358887D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.69698464870452881D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.30010896921157837D)));
            this.table3.Body.SetCellContent(0, 6, this.textBox11);
            this.table3.Body.SetCellContent(0, 7, this.textBox20);
            this.table3.Body.SetCellContent(0, 5, this.textBox24);
            this.table3.Body.SetCellContent(2, 3, this.textBox51);
            this.table3.Body.SetCellContent(3, 0, this.textBox57);
            this.table3.Body.SetCellContent(3, 1, this.textBox58);
            this.table3.Body.SetCellContent(3, 2, this.textBox60);
            this.table3.Body.SetCellContent(3, 7, this.textBox65);
            this.table3.Body.SetCellContent(4, 0, this.textBox66);
            this.table3.Body.SetCellContent(4, 1, this.textBox67);
            this.table3.Body.SetCellContent(4, 2, this.textBox69);
            this.table3.Body.SetCellContent(4, 7, this.textBox75);
            this.table3.Body.SetCellContent(5, 0, this.textBox76);
            this.table3.Body.SetCellContent(5, 1, this.textBox77);
            this.table3.Body.SetCellContent(5, 2, this.textBox78);
            this.table3.Body.SetCellContent(5, 7, this.textBox83);
            this.table3.Body.SetCellContent(6, 0, this.textBox84);
            this.table3.Body.SetCellContent(6, 1, this.textBox85);
            this.table3.Body.SetCellContent(6, 7, this.textBox91);
            this.table3.Body.SetCellContent(7, 0, this.textBox92);
            this.table3.Body.SetCellContent(7, 1, this.lblInstallBookingDate);
            this.table3.Body.SetCellContent(7, 7, this.textBox99);
            this.table3.Body.SetCellContent(0, 0, this.chkBuilding, 1, 2);
            this.table3.Body.SetCellContent(0, 2, this.chkResidential);
            this.table3.Body.SetCellContent(1, 2, this.chkschool);
            this.table3.Body.SetCellContent(2, 2, this.chkCommercial);
            this.table3.Body.SetCellContent(0, 3, this.checkBox6);
            this.table3.Body.SetCellContent(1, 3, this.checkBox7);
            this.table3.Body.SetCellContent(0, 4, this.checkBox8);
            this.table3.Body.SetCellContent(1, 4, this.checkBox9);
            this.table3.Body.SetCellContent(2, 4, this.checkBox10);
            this.table3.Body.SetCellContent(1, 5, this.textBox102, 2, 3);
            this.table3.Body.SetCellContent(6, 2, this.textBox86, 2, 1);
            this.table3.Body.SetCellContent(3, 5, this.textBox63, 1, 2);
            this.table3.Body.SetCellContent(4, 5, this.textBox72, 1, 2);
            this.table3.Body.SetCellContent(5, 5, this.textBox81, 1, 2);
            this.table3.Body.SetCellContent(6, 5, this.textBox89, 1, 2);
            this.table3.Body.SetCellContent(7, 5, this.textBox97, 1, 2);
            this.table3.Body.SetCellContent(6, 3, this.table4, 2, 2);
            this.table3.Body.SetCellContent(3, 3, this.textBox61, 1, 2);
            this.table3.Body.SetCellContent(4, 3, this.textBox70, 1, 2);
            this.table3.Body.SetCellContent(5, 3, this.textBox79, 1, 2);
            this.table3.Body.SetCellContent(1, 0, this.chkGround, 2, 2);
            tableGroup31.Name = "group28";
            tableGroup32.Name = "group20";
            tableGroup30.ChildGroups.Add(tableGroup31);
            tableGroup30.ChildGroups.Add(tableGroup32);
            tableGroup30.Name = "tableGroup6";
            tableGroup30.ReportItem = this.textBox8;
            tableGroup33.Name = "group19";
            tableGroup33.ReportItem = this.textBox38;
            tableGroup34.Name = "group18";
            tableGroup34.ReportItem = this.textBox36;
            tableGroup35.Name = "group17";
            tableGroup35.ReportItem = this.textBox27;
            tableGroup37.Name = "group29";
            tableGroup38.Name = "tableGroup7";
            tableGroup39.Name = "tableGroup8";
            tableGroup36.ChildGroups.Add(tableGroup37);
            tableGroup36.ChildGroups.Add(tableGroup38);
            tableGroup36.ChildGroups.Add(tableGroup39);
            tableGroup36.Name = "group16";
            tableGroup36.ReportItem = this.textBox22;
            this.table3.ColumnGroups.Add(tableGroup30);
            this.table3.ColumnGroups.Add(tableGroup33);
            this.table3.ColumnGroups.Add(tableGroup34);
            this.table3.ColumnGroups.Add(tableGroup35);
            this.table3.ColumnGroups.Add(tableGroup36);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.chkBuilding,
            this.chkResidential,
            this.checkBox6,
            this.checkBox8,
            this.textBox24,
            this.textBox11,
            this.textBox20,
            this.chkGround,
            this.chkschool,
            this.checkBox7,
            this.checkBox9,
            this.textBox102,
            this.chkCommercial,
            this.textBox51,
            this.checkBox10,
            this.textBox57,
            this.textBox58,
            this.textBox60,
            this.textBox61,
            this.textBox63,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox69,
            this.textBox70,
            this.textBox72,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox81,
            this.textBox83,
            this.textBox84,
            this.textBox85,
            this.textBox86,
            this.table4,
            this.textBox89,
            this.textBox91,
            this.textBox92,
            this.lblInstallBookingDate,
            this.textBox97,
            this.textBox99,
            this.textBox8,
            this.textBox38,
            this.textBox36,
            this.textBox27,
            this.textBox22});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.8566653728485107D));
            this.table3.Name = "table3";
            tableGroup41.Name = "group21";
            tableGroup42.Name = "group30";
            tableGroup43.Name = "group22";
            tableGroup44.Name = "group23";
            tableGroup45.Name = "group24";
            tableGroup46.Name = "group25";
            tableGroup47.Name = "group26";
            tableGroup48.Name = "group27";
            tableGroup40.ChildGroups.Add(tableGroup41);
            tableGroup40.ChildGroups.Add(tableGroup42);
            tableGroup40.ChildGroups.Add(tableGroup43);
            tableGroup40.ChildGroups.Add(tableGroup44);
            tableGroup40.ChildGroups.Add(tableGroup45);
            tableGroup40.ChildGroups.Add(tableGroup46);
            tableGroup40.ChildGroups.Add(tableGroup47);
            tableGroup40.ChildGroups.Add(tableGroup48);
            tableGroup40.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup40.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup40);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(4.6234650611877441D));
            this.table3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.table3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9743285179138184D), Telerik.Reporting.Drawing.Unit.Cm(0.44999983906745911D));
            this.textBox11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox11.Style.Font.Name = "Arial";
            this.textBox11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.textBox11.Value = "5 Years";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1750226020812988D), Telerik.Reporting.Drawing.Unit.Cm(0.44999983906745911D));
            this.textBox20.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox20.Style.Font.Name = "Arial";
            this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.textBox20.Value = "15 Years";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.44999983906745911D));
            this.textBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox24.Style.Font.Name = "Arial";
            this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.textBox24.StyleName = "";
            this.textBox24.Value = "1 Year";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.34416654706001282D));
            this.textBox51.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox51.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox51.Style.Font.Name = "Arial";
            this.textBox51.StyleName = "";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7331244945526123D), Telerik.Reporting.Drawing.Unit.Cm(0.88219344615936279D));
            this.textBox57.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox57.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox57.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox57.Style.Font.Name = "Arial";
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox57.StyleName = "";
            this.textBox57.Value = "Panel Brand:";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.88219344615936279D));
            this.textBox58.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox58.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox58.Style.Font.Name = "Arial";
            this.textBox58.StyleName = "";
            this.textBox58.Value = "=Fields.PanelBrand";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.88219344615936279D));
            this.textBox60.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox60.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox60.Style.Font.Name = "Arial";
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox60.StyleName = "";
            this.textBox60.Value = "Inverter Brand:";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1750226020812988D), Telerik.Reporting.Drawing.Unit.Cm(0.88219344615936279D));
            this.textBox65.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox65.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox65.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox65.Style.Font.Name = "Arial";
            this.textBox65.StyleName = "";
            this.textBox65.Value = "= IIf(Fields.RebateApproved=\"True\",\"Yes\",\"No\")";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7331244945526123D), Telerik.Reporting.Drawing.Unit.Cm(0.67052698135375977D));
            this.textBox66.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox66.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox66.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox66.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox66.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox66.Style.Font.Name = "Arial";
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox66.StyleName = "";
            this.textBox66.Value = "Panel Model:";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.67052698135375977D));
            this.textBox67.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox67.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox67.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox67.Style.Font.Name = "Arial";
            this.textBox67.StyleName = "";
            this.textBox67.Value = "=Fields.PanelModel";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.67052698135375977D));
            this.textBox69.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox69.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox69.Style.Font.Name = "Arial";
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox69.StyleName = "";
            this.textBox69.Value = "Inverter\r\nSeries:";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1750226020812988D), Telerik.Reporting.Drawing.Unit.Cm(0.67052698135375977D));
            this.textBox75.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox75.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox75.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox75.Style.Font.Name = "Arial";
            this.textBox75.StyleName = "";
            this.textBox75.Value = "= IIf(Fields.ReceivedCredits=\"True\",\"Yes\",\"No\")";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7331244945526123D), Telerik.Reporting.Drawing.Unit.Cm(0.61760973930358887D));
            this.textBox76.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox76.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox76.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox76.Style.Font.Name = "Arial";
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "Rated Power Output (kw):";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.61760973930358887D));
            this.textBox77.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox77.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox77.Style.Font.Name = "Arial";
            this.textBox77.StyleName = "";
            this.textBox77.Value = "=Fields.SystemCapKW";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.61760973930358887D));
            this.textBox78.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox78.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox78.Style.Font.Name = "Arial";
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox78.StyleName = "";
            this.textBox78.Value = "Inverter Model:";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1750226020812988D), Telerik.Reporting.Drawing.Unit.Cm(0.61760973930358887D));
            this.textBox83.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox83.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox83.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox83.Style.Font.Name = "Arial";
            this.textBox83.StyleName = "";
            this.textBox83.Value = "= IIf(Fields.CreditEligible=\"True\",\"Yes\",\"No\")";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7331244945526123D), Telerik.Reporting.Drawing.Unit.Cm(0.69698464870452881D));
            this.textBox84.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox84.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox84.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox84.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox84.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox84.Style.Font.Name = "Arial";
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox84.StyleName = "";
            this.textBox84.Value = "No. of Panels:";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.69698464870452881D));
            this.textBox85.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox85.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox85.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox85.Style.Font.Name = "Arial";
            this.textBox85.StyleName = "";
            this.textBox85.Value = "=Fields.NumberPanels";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1750226020812988D), Telerik.Reporting.Drawing.Unit.Cm(0.69698464870452881D));
            this.textBox91.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox91.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox91.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox91.Style.Font.Name = "Arial";
            this.textBox91.StyleName = "";
            this.textBox91.Value = "= IIf(Fields.MoreThanOneInstall=\"True\",\"Yes\",\"No\")";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7331244945526123D), Telerik.Reporting.Drawing.Unit.Cm(0.30010896921157837D));
            this.textBox92.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox92.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox92.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox92.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox92.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox92.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox92.Style.Font.Name = "Arial";
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox92.StyleName = "";
            this.textBox92.Value = "Installation date:";
            // 
            // lblInstallBookingDate
            // 
            this.lblInstallBookingDate.Name = "lblInstallBookingDate";
            this.lblInstallBookingDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.30010896921157837D));
            this.lblInstallBookingDate.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblInstallBookingDate.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.lblInstallBookingDate.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblInstallBookingDate.Style.Font.Name = "Arial";
            this.lblInstallBookingDate.StyleName = "";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1750226020812988D), Telerik.Reporting.Drawing.Unit.Cm(0.30010896921157837D));
            this.textBox99.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox99.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox99.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox99.Style.Font.Name = "Arial";
            this.textBox99.StyleName = "";
            this.textBox99.Value = "= IIf(Fields.RequiredCompliancePaperwork=\"True\",\"Yes\",\"No\")";
            // 
            // chkBuilding
            // 
            this.chkBuilding.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkBuilding.CheckedImage = ((object)(resources.GetObject("chkBuilding.CheckedImage")));
            this.chkBuilding.FalseValue = "= False";
            this.chkBuilding.IndeterminateImage = ((object)(resources.GetObject("chkBuilding.IndeterminateImage")));
            this.chkBuilding.IndeterminateValue = "=1";
            this.chkBuilding.Name = "chkBuilding";
            this.chkBuilding.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.228571891784668D), Telerik.Reporting.Drawing.Unit.Cm(0.44999983906745911D));
            this.chkBuilding.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.chkBuilding.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.chkBuilding.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.chkBuilding.Style.Font.Name = "Arial";
            this.chkBuilding.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.chkBuilding.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.chkBuilding.StyleName = "";
            this.chkBuilding.Text = "Building or Structure";
            this.chkBuilding.TrueValue = "= True";
            this.chkBuilding.UncheckedImage = ((object)(resources.GetObject("chkBuilding.UncheckedImage")));
            this.chkBuilding.Value = "=iif(Fields.InstallBase=\"1\",\"True\",\"False\")";
            // 
            // chkResidential
            // 
            this.chkResidential.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkResidential.CheckedImage = ((object)(resources.GetObject("chkResidential.CheckedImage")));
            this.chkResidential.IndeterminateImage = ((object)(resources.GetObject("chkResidential.IndeterminateImage")));
            this.chkResidential.Name = "chkResidential";
            this.chkResidential.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.44999983906745911D));
            this.chkResidential.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.chkResidential.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.chkResidential.Style.Font.Name = "Arial";
            this.chkResidential.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.chkResidential.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.chkResidential.StyleName = "";
            this.chkResidential.Text = " Residential";
            this.chkResidential.UncheckedImage = ((object)(resources.GetObject("chkResidential.UncheckedImage")));
            this.chkResidential.Value = "=iif(Fields.ResCom=\"1\",\"True\",\"False\")";
            // 
            // chkschool
            // 
            this.chkschool.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkschool.CheckedImage = ((object)(resources.GetObject("chkschool.CheckedImage")));
            this.chkschool.IndeterminateImage = ((object)(resources.GetObject("chkschool.IndeterminateImage")));
            this.chkschool.Name = "chkschool";
            this.chkschool.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.317708283662796D));
            this.chkschool.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.chkschool.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.chkschool.Style.Font.Name = "Arial";
            this.chkschool.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.chkschool.StyleName = "";
            this.chkschool.Text = " School";
            this.chkschool.UncheckedImage = ((object)(resources.GetObject("chkschool.UncheckedImage")));
            this.chkschool.Value = "=False";
            // 
            // chkCommercial
            // 
            this.chkCommercial.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkCommercial.CheckedImage = ((object)(resources.GetObject("chkCommercial.CheckedImage")));
            this.chkCommercial.IndeterminateImage = ((object)(resources.GetObject("chkCommercial.IndeterminateImage")));
            this.chkCommercial.Name = "chkCommercial";
            this.chkCommercial.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.34416654706001282D));
            this.chkCommercial.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.chkCommercial.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.chkCommercial.Style.Font.Name = "Arial";
            this.chkCommercial.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.chkCommercial.StyleName = "";
            this.chkCommercial.Text = " Commercial";
            this.chkCommercial.UncheckedImage = ((object)(resources.GetObject("chkCommercial.UncheckedImage")));
            this.chkCommercial.Value = "=iif(Fields.ResCom=\"1\",\"False\",\"True\")";
            // 
            // checkBox6
            // 
            this.checkBox6.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox6.CheckedImage = ((object)(resources.GetObject("checkBox6.CheckedImage")));
            this.checkBox6.IndeterminateImage = ((object)(resources.GetObject("checkBox6.IndeterminateImage")));
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.44999983906745911D));
            this.checkBox6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox6.Style.Font.Name = "Arial";
            this.checkBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.checkBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox6.StyleName = "";
            this.checkBox6.Text = " Single";
            this.checkBox6.UncheckedImage = ((object)(resources.GetObject("checkBox6.UncheckedImage")));
            this.checkBox6.Value = "=False";
            // 
            // checkBox7
            // 
            this.checkBox7.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox7.CheckedImage = ((object)(resources.GetObject("checkBox7.CheckedImage")));
            this.checkBox7.IndeterminateImage = ((object)(resources.GetObject("checkBox7.IndeterminateImage")));
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4954473972320557D), Telerik.Reporting.Drawing.Unit.Cm(0.317708283662796D));
            this.checkBox7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox7.Style.Font.Name = "Arial";
            this.checkBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox7.StyleName = "";
            this.checkBox7.Text = " Multi";
            this.checkBox7.UncheckedImage = ((object)(resources.GetObject("checkBox7.UncheckedImage")));
            this.checkBox7.Value = "=False";
            // 
            // checkBox8
            // 
            this.checkBox8.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox8.CheckedImage = ((object)(resources.GetObject("checkBox8.CheckedImage")));
            this.checkBox8.IndeterminateImage = ((object)(resources.GetObject("checkBox8.IndeterminateImage")));
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4426338672637939D), Telerik.Reporting.Drawing.Unit.Cm(0.44999983906745911D));
            this.checkBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox8.Style.Font.Name = "Arial";
            this.checkBox8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.checkBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox8.StyleName = "";
            this.checkBox8.Text = " Solar Panel";
            this.checkBox8.UncheckedImage = ((object)(resources.GetObject("checkBox8.UncheckedImage")));
            this.checkBox8.Value = "=True";
            // 
            // checkBox9
            // 
            this.checkBox9.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox9.CheckedImage = ((object)(resources.GetObject("checkBox9.CheckedImage")));
            this.checkBox9.IndeterminateImage = ((object)(resources.GetObject("checkBox9.IndeterminateImage")));
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4426338672637939D), Telerik.Reporting.Drawing.Unit.Cm(0.317708283662796D));
            this.checkBox9.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox9.Style.Font.Name = "Arial";
            this.checkBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox9.StyleName = "";
            this.checkBox9.Text = " Wind";
            this.checkBox9.UncheckedImage = ((object)(resources.GetObject("checkBox9.UncheckedImage")));
            this.checkBox9.Value = "=False";
            // 
            // checkBox10
            // 
            this.checkBox10.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox10.CheckedImage = ((object)(resources.GetObject("checkBox10.CheckedImage")));
            this.checkBox10.IndeterminateImage = ((object)(resources.GetObject("checkBox10.IndeterminateImage")));
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4426338672637939D), Telerik.Reporting.Drawing.Unit.Cm(0.34416654706001282D));
            this.checkBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox10.Style.Font.Name = "Arial";
            this.checkBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox10.StyleName = "";
            this.checkBox10.Text = " Hydro";
            this.checkBox10.UncheckedImage = ((object)(resources.GetObject("checkBox10.UncheckedImage")));
            this.checkBox10.Value = "=False";
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6447982788085938D), Telerik.Reporting.Drawing.Unit.Cm(0.66187483072280884D));
            this.textBox102.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox102.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox102.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox102.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox102.Style.Font.Name = "Arial";
            this.textBox102.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox102.StyleName = "";
            this.textBox102.Value = "15 years deeming can only be created Panel if\r\ninstalled in the past 12 months.";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1785476207733154D), Telerik.Reporting.Drawing.Unit.Cm(0.99709361791610718D));
            this.textBox86.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox86.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox86.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox86.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox86.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox86.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox86.Style.Font.Name = "Arial";
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox86.StyleName = "";
            this.textBox86.Value = "Grid\r\nConnection";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4697756767272949D), Telerik.Reporting.Drawing.Unit.Cm(0.88219344615936279D));
            this.textBox63.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox63.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox63.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox63.Style.Font.Name = "Arial";
            this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox63.StyleName = "";
            this.textBox63.Value = "Have you received or been approved for a REBATE or financial assistance (includin" +
    "g Solar Credits) for any small generation unit at this address?";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4697756767272949D), Telerik.Reporting.Drawing.Unit.Cm(0.67052698135375977D));
            this.textBox72.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox72.Style.Font.Name = "Arial";
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox72.StyleName = "";
            this.textBox72.Value = "Have you received Solar Credits (multiplied STCs) for a small generation unit at " +
    "this premises/address?";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4697756767272949D), Telerik.Reporting.Drawing.Unit.Cm(0.61760973930358887D));
            this.textBox81.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox81.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox81.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox81.Style.Font.Name = "Arial";
            this.textBox81.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox81.StyleName = "";
            this.textBox81.Value = "Is this an ELIGIBLE premises for Solar Credits (STC Multiplier)?";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4697756767272949D), Telerik.Reporting.Drawing.Unit.Cm(0.69698464870452881D));
            this.textBox89.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox89.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox89.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox89.Style.Font.Name = "Arial";
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox89.StyleName = "";
            this.textBox89.Value = "Is there more than one Solar PV installation at this address?";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4697756767272949D), Telerik.Reporting.Drawing.Unit.Cm(0.30010896921157837D));
            this.textBox97.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox97.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox97.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox97.Style.Font.Name = "Arial";
            this.textBox97.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox97.StyleName = "";
            this.textBox97.Value = "Do you have all of the required compliance paper work?";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.35929191112518311D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.31890085339546204D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.31890085339546204D)));
            this.table4.Body.SetCellContent(0, 0, this.checkBox11);
            this.table4.Body.SetCellContent(1, 0, this.checkBox12);
            this.table4.Body.SetCellContent(2, 0, this.checkBox13);
            tableGroup25.Name = "tableGroup9";
            this.table4.ColumnGroups.Add(tableGroup25);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox11,
            this.checkBox12,
            this.checkBox13});
            this.table4.Name = "table4";
            tableGroup27.Name = "group31";
            tableGroup28.Name = "group33";
            tableGroup29.Name = "group32";
            tableGroup26.ChildGroups.Add(tableGroup27);
            tableGroup26.ChildGroups.Add(tableGroup28);
            tableGroup26.ChildGroups.Add(tableGroup29);
            tableGroup26.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup26.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup26);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D), Telerik.Reporting.Drawing.Unit.Cm(0.99709361791610718D));
            this.table4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.table4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table4.Style.Font.Name = "Arial";
            this.table4.StyleName = "";
            // 
            // checkBox11
            // 
            this.checkBox11.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox11.CheckedImage = ((object)(resources.GetObject("checkBox11.CheckedImage")));
            this.checkBox11.IndeterminateImage = ((object)(resources.GetObject("checkBox11.IndeterminateImage")));
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D), Telerik.Reporting.Drawing.Unit.Cm(0.35929191112518311D));
            this.checkBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.checkBox11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox11.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.4000000953674316D);
            this.checkBox11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox11.StyleName = "";
            this.checkBox11.Text = " Connected to an electricity grid without battery storage";
            this.checkBox11.UncheckedImage = ((object)(resources.GetObject("checkBox11.UncheckedImage")));
            this.checkBox11.Value = "=True";
            // 
            // checkBox12
            // 
            this.checkBox12.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox12.CheckedImage = ((object)(resources.GetObject("checkBox12.CheckedImage")));
            this.checkBox12.IndeterminateImage = ((object)(resources.GetObject("checkBox12.IndeterminateImage")));
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D), Telerik.Reporting.Drawing.Unit.Cm(0.31890082359313965D));
            this.checkBox12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox12.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.4000000953674316D);
            this.checkBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox12.StyleName = "";
            this.checkBox12.Text = " Connected to an electricity grid with battery storage";
            this.checkBox12.UncheckedImage = ((object)(resources.GetObject("checkBox12.UncheckedImage")));
            this.checkBox12.Value = "=False";
            // 
            // checkBox13
            // 
            this.checkBox13.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox13.CheckedImage = ((object)(resources.GetObject("checkBox13.CheckedImage")));
            this.checkBox13.IndeterminateImage = ((object)(resources.GetObject("checkBox13.IndeterminateImage")));
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D), Telerik.Reporting.Drawing.Unit.Cm(0.31890082359313965D));
            this.checkBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.checkBox13.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox13.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.checkBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.4000000953674316D);
            this.checkBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox13.StyleName = "";
            this.checkBox13.Text = " Stand-alone(not connected to an electricity)";
            this.checkBox13.UncheckedImage = ((object)(resources.GetObject("checkBox13.UncheckedImage")));
            this.checkBox13.Value = "=False";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D), Telerik.Reporting.Drawing.Unit.Cm(0.88219344615936279D));
            this.textBox61.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox61.Style.Font.Name = "Arial";
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox61.StyleName = "";
            this.textBox61.Value = "=Fields.InverterBrand";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D), Telerik.Reporting.Drawing.Unit.Cm(0.67052698135375977D));
            this.textBox70.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox70.Style.Font.Name = "Arial";
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox70.StyleName = "";
            this.textBox70.Value = "=Fields.InverterModel";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.93808126449585D), Telerik.Reporting.Drawing.Unit.Cm(0.61760973930358887D));
            this.textBox79.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox79.Style.Font.Name = "Arial";
            this.textBox79.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox79.StyleName = "";
            this.textBox79.Value = "=Fields.InverterModel";
            // 
            // chkGround
            // 
            this.chkGround.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkGround.CheckedImage = ((object)(resources.GetObject("chkGround.CheckedImage")));
            this.chkGround.FalseValue = "= False";
            this.chkGround.IndeterminateImage = ((object)(resources.GetObject("chkGround.IndeterminateImage")));
            this.chkGround.Name = "chkGround";
            this.chkGround.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.228571891784668D), Telerik.Reporting.Drawing.Unit.Cm(0.66187483072280884D));
            this.chkGround.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.chkGround.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.chkGround.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.chkGround.Style.Font.Name = "Arial";
            this.chkGround.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.chkGround.StyleName = "";
            this.chkGround.Text = " Ground Mounted or free Standing";
            this.chkGround.TrueValue = "= True";
            this.chkGround.UncheckedImage = ((object)(resources.GetObject("chkGround.UncheckedImage")));
            this.chkGround.Value = "=iif(Fields.InstallBase=\"2\",\"True\",\"False\")";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.6344311237335205D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9967118501663208D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9967118501663208D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9848768711090088D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9848768711090088D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.3923892974853516D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.35322916507720947D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox37);
            this.table5.Body.SetCellContent(0, 1, this.checkBox14);
            this.table5.Body.SetCellContent(0, 2, this.checkBox15);
            this.table5.Body.SetCellContent(0, 3, this.textBox10, 1, 3);
            tableGroup49.Name = "group36";
            tableGroup50.Name = "group35";
            tableGroup51.Name = "group34";
            tableGroup52.Name = "tableGroup10";
            tableGroup53.Name = "tableGroup11";
            tableGroup54.Name = "tableGroup12";
            this.table5.ColumnGroups.Add(tableGroup49);
            this.table5.ColumnGroups.Add(tableGroup50);
            this.table5.ColumnGroups.Add(tableGroup51);
            this.table5.ColumnGroups.Add(tableGroup52);
            this.table5.ColumnGroups.Add(tableGroup53);
            this.table5.ColumnGroups.Add(tableGroup54);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox37,
            this.checkBox14,
            this.checkBox15,
            this.textBox10});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(8.4872932434082031D));
            this.table5.Name = "table5";
            tableGroup55.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup55.Name = "detailTableGroup4";
            this.table5.RowGroups.Add(tableGroup55);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989997863769531D), Telerik.Reporting.Drawing.Unit.Cm(0.35322916507720947D));
            this.table5.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.table5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.table5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table5.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6344311237335205D), Telerik.Reporting.Drawing.Unit.Cm(0.35322916507720947D));
            this.textBox37.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox37.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox37.Style.Font.Name = "Arial";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox37.StyleName = "";
            this.textBox37.Value = "Additional comments:";
            // 
            // checkBox14
            // 
            this.checkBox14.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox14.CheckedImage = ((object)(resources.GetObject("checkBox14.CheckedImage")));
            this.checkBox14.IndeterminateImage = ((object)(resources.GetObject("checkBox14.IndeterminateImage")));
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9967120885849D), Telerik.Reporting.Drawing.Unit.Cm(0.35322916507720947D));
            this.checkBox14.Style.BackgroundColor = System.Drawing.Color.White;
            this.checkBox14.Style.Font.Name = "Arial";
            this.checkBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox14.StyleName = "";
            this.checkBox14.Text = " No";
            this.checkBox14.UncheckedImage = ((object)(resources.GetObject("checkBox14.UncheckedImage")));
            this.checkBox14.Value = "=False";
            // 
            // checkBox15
            // 
            this.checkBox15.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox15.CheckedImage = ((object)(resources.GetObject("checkBox15.CheckedImage")));
            this.checkBox15.IndeterminateImage = ((object)(resources.GetObject("checkBox15.IndeterminateImage")));
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9967120885849D), Telerik.Reporting.Drawing.Unit.Cm(0.35322916507720947D));
            this.checkBox15.Style.BackgroundColor = System.Drawing.Color.White;
            this.checkBox15.Style.Font.Name = "Arial";
            this.checkBox15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox15.StyleName = "";
            this.checkBox15.Text = " Yes";
            this.checkBox15.UncheckedImage = ((object)(resources.GetObject("checkBox15.UncheckedImage")));
            this.checkBox15.Value = "=False";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.362143516540527D), Telerik.Reporting.Drawing.Unit.Cm(0.35322916507720947D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.Font.Name = "Arial";
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = " \tIf Yes Provide Details:\r\n";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9872090816497803D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.0227365493774414D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.985480785369873D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9917535781860352D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34332972764968872D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34062647819519043D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.3142164945602417D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34062647819519043D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.314216673374176D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34062647819519043D)));
            this.table6.Body.SetCellContent(0, 1, this.textBox39);
            this.table6.Body.SetCellContent(0, 4, this.textBox41);
            this.table6.Body.SetCellContent(0, 3, this.textBox44);
            this.table6.Body.SetCellContent(0, 2, this.textBox53);
            this.table6.Body.SetCellContent(4, 1, this.textBox40);
            this.table6.Body.SetCellContent(4, 2, this.textBox43);
            this.table6.Body.SetCellContent(4, 3, this.textBox52);
            this.table6.Body.SetCellContent(4, 4, this.textBox54);
            this.table6.Body.SetCellContent(3, 1, this.textBox64);
            this.table6.Body.SetCellContent(3, 2, this.textBox74);
            this.table6.Body.SetCellContent(3, 3, this.textBox82);
            this.table6.Body.SetCellContent(3, 4, this.textBox87);
            this.table6.Body.SetCellContent(2, 1, this.textBox90);
            this.table6.Body.SetCellContent(2, 2, this.textBox94);
            this.table6.Body.SetCellContent(2, 3, this.textBox95);
            this.table6.Body.SetCellContent(2, 4, this.textBox96);
            this.table6.Body.SetCellContent(1, 1, this.textBox100);
            this.table6.Body.SetCellContent(1, 2, this.textBox101);
            this.table6.Body.SetCellContent(1, 3, this.textBox103);
            this.table6.Body.SetCellContent(1, 4, this.textBox104);
            this.table6.Body.SetCellContent(5, 1, this.textBox106);
            this.table6.Body.SetCellContent(5, 2, this.textBox107);
            this.table6.Body.SetCellContent(5, 3, this.textBox108);
            this.table6.Body.SetCellContent(5, 4, this.textBox109);
            this.table6.Body.SetCellContent(0, 0, this.textBox16, 2, 1);
            this.table6.Body.SetCellContent(2, 0, this.textBox88, 2, 1);
            this.table6.Body.SetCellContent(4, 0, this.textBox28, 2, 1);
            tableGroup57.Name = "group39";
            tableGroup58.Name = "tableGroup14";
            tableGroup59.Name = "group38";
            tableGroup60.Name = "group37";
            tableGroup61.Name = "tableGroup15";
            tableGroup56.ChildGroups.Add(tableGroup57);
            tableGroup56.ChildGroups.Add(tableGroup58);
            tableGroup56.ChildGroups.Add(tableGroup59);
            tableGroup56.ChildGroups.Add(tableGroup60);
            tableGroup56.ChildGroups.Add(tableGroup61);
            tableGroup56.Name = "tableGroup13";
            tableGroup56.ReportItem = this.textBox9;
            this.table6.ColumnGroups.Add(tableGroup56);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16,
            this.textBox39,
            this.textBox53,
            this.textBox44,
            this.textBox41,
            this.textBox100,
            this.textBox101,
            this.textBox103,
            this.textBox104,
            this.textBox88,
            this.textBox90,
            this.textBox94,
            this.textBox95,
            this.textBox96,
            this.textBox64,
            this.textBox74,
            this.textBox82,
            this.textBox87,
            this.textBox28,
            this.textBox40,
            this.textBox43,
            this.textBox52,
            this.textBox54,
            this.textBox106,
            this.textBox107,
            this.textBox108,
            this.textBox109,
            this.textBox9});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(8.82854175567627D));
            this.table6.Name = "table6";
            tableGroup63.Name = "group40";
            tableGroup64.Name = "group44";
            tableGroup65.Name = "group43";
            tableGroup66.Name = "group42";
            tableGroup67.Name = "group41";
            tableGroup68.Name = "group45";
            tableGroup62.ChildGroups.Add(tableGroup63);
            tableGroup62.ChildGroups.Add(tableGroup64);
            tableGroup62.ChildGroups.Add(tableGroup65);
            tableGroup62.ChildGroups.Add(tableGroup66);
            tableGroup62.ChildGroups.Add(tableGroup67);
            tableGroup62.ChildGroups.Add(tableGroup68);
            tableGroup62.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup62.Name = "detailTableGroup5";
            this.table6.RowGroups.Add(tableGroup62);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.4605557918548584D));
            this.table6.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table6.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table6.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0227365493774414D), Telerik.Reporting.Drawing.Unit.Cm(0.34332972764968872D));
            this.textBox39.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox39.Style.Font.Name = "Arial";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox39.Value = "=Fields.InstallerName";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9917535781860352D), Telerik.Reporting.Drawing.Unit.Cm(0.34332972764968872D));
            this.textBox41.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox41.Style.Font.Name = "Arial";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox41.Value = "=Fields.InstallerAccreditation";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854812622070312D), Telerik.Reporting.Drawing.Unit.Cm(0.34332972764968872D));
            this.textBox44.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox44.Style.Font.Name = "Arial";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox44.StyleName = "";
            this.textBox44.Value = "=Fields.Installeradd";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D), Telerik.Reporting.Drawing.Unit.Cm(0.34332972764968872D));
            this.textBox53.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox53.Style.Font.Name = "Arial";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox53.StyleName = "";
            this.textBox53.Value = "=Fields.InstallerMobile";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0227365493774414D), Telerik.Reporting.Drawing.Unit.Cm(0.31421664357185364D));
            this.textBox40.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox40.Style.Font.Name = "Arial";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox40.StyleName = "";
            this.textBox40.Value = "=Fields.ElectricianName";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D), Telerik.Reporting.Drawing.Unit.Cm(0.31421664357185364D));
            this.textBox43.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox43.Style.Font.Name = "Arial";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox43.StyleName = "";
            this.textBox43.Value = "=Fields.Electricianmolbile";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854812622070312D), Telerik.Reporting.Drawing.Unit.Cm(0.31421664357185364D));
            this.textBox52.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox52.Style.Font.Name = "Arial";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox52.StyleName = "";
            this.textBox52.Value = "=Fields.Electricianadd";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9917535781860352D), Telerik.Reporting.Drawing.Unit.Cm(0.31421664357185364D));
            this.textBox54.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox54.Style.Font.Name = "Arial";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox54.StyleName = "";
            this.textBox54.Value = "=Fields.ElectricianAccreditation";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0227365493774414D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox64.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox64.Style.Font.Name = "Arial";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox64.StyleName = "";
            this.textBox64.Value = "Full Name";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox74.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox74.Style.Font.Name = "Arial";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox74.StyleName = "";
            this.textBox74.Value = "Phone";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854812622070312D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox82.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox82.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox82.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox82.Style.Font.Name = "Arial";
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox82.StyleName = "";
            this.textBox82.Value = "Address";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9917535781860352D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox87.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox87.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox87.Style.Font.Name = "Arial";
            this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox87.StyleName = "";
            this.textBox87.Value = "Accreditation No.";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0227365493774414D), Telerik.Reporting.Drawing.Unit.Cm(0.31421646475791931D));
            this.textBox90.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox90.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox90.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox90.Style.Font.Name = "Arial";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox90.StyleName = "";
            this.textBox90.Value = "=Fields.DesignerName";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D), Telerik.Reporting.Drawing.Unit.Cm(0.31421646475791931D));
            this.textBox94.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox94.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox94.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox94.Style.Font.Name = "Arial";
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox94.StyleName = "";
            this.textBox94.Value = "=Fields.Designermobile";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854812622070312D), Telerik.Reporting.Drawing.Unit.Cm(0.31421646475791931D));
            this.textBox95.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox95.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox95.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox95.Style.Font.Name = "Arial";
            this.textBox95.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox95.StyleName = "";
            this.textBox95.Value = "=Fields.Designeradd";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9917535781860352D), Telerik.Reporting.Drawing.Unit.Cm(0.31421646475791931D));
            this.textBox96.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox96.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox96.Style.Font.Name = "Arial";
            this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox96.StyleName = "";
            this.textBox96.Value = "=Fields.DesignerAccreditation";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0227365493774414D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox100.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox100.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox100.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox100.Style.Font.Name = "Arial";
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox100.StyleName = "";
            this.textBox100.Value = "Full Name";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox101.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox101.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox101.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox101.Style.Font.Name = "Arial";
            this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox101.StyleName = "";
            this.textBox101.Value = "Phone";
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854812622070312D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox103.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox103.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox103.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox103.Style.Font.Name = "Arial";
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox103.StyleName = "";
            this.textBox103.Value = "Address";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9917535781860352D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox104.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox104.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox104.Style.Font.Name = "Arial";
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox104.StyleName = "";
            this.textBox104.Value = "Accreditation No.";
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0227365493774414D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox106.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox106.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox106.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox106.Style.Font.Name = "Arial";
            this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox106.StyleName = "";
            this.textBox106.Value = "Full Name";
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox107.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox107.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox107.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox107.Style.Font.Name = "Arial";
            this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox107.StyleName = "";
            this.textBox107.Value = "Phone";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854812622070312D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox108.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox108.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox108.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox108.Style.Font.Name = "Arial";
            this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox108.StyleName = "";
            this.textBox108.Value = "Address";
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9917535781860352D), Telerik.Reporting.Drawing.Unit.Cm(0.34062641859054565D));
            this.textBox109.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox109.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox109.Style.Font.Name = "Arial";
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox109.StyleName = "";
            this.textBox109.Value = "Accreditation No.";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872090816497803D), Telerik.Reporting.Drawing.Unit.Cm(0.68395614624023438D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Arial";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.Value = "Installer:";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872090816497803D), Telerik.Reporting.Drawing.Unit.Cm(0.65484285354614258D));
            this.textBox88.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox88.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox88.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox88.Style.Font.Bold = true;
            this.textBox88.Style.Font.Name = "Arial";
            this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox88.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.StyleName = "";
            this.textBox88.Value = "Designer:";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872090816497803D), Telerik.Reporting.Drawing.Unit.Cm(0.6548430323600769D));
            this.textBox28.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Arial";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "Electrician:";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.5992755889892578D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.1331033706665039D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.2576208114624023D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.58208256959915161D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.31479308009147644D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(2.0081238746643066D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1614583730697632D)));
            this.table7.Body.SetCellContent(2, 0, this.textBox105);
            this.table7.Body.SetCellContent(2, 1, this.textBox111);
            this.table7.Body.SetCellContent(2, 2, this.textBox113);
            this.table7.Body.SetCellContent(1, 0, this.textBox114);
            this.table7.Body.SetCellContent(1, 1, this.textBox115);
            this.table7.Body.SetCellContent(1, 2, this.textBox116);
            this.table7.Body.SetCellContent(0, 0, this.htmlTextBox4, 1, 3);
            this.table7.Body.SetCellContent(3, 0, this.textBox98, 1, 3);
            tableGroup70.Name = "group46";
            tableGroup71.Name = "tableGroup17";
            tableGroup72.Name = "tableGroup18";
            tableGroup69.ChildGroups.Add(tableGroup70);
            tableGroup69.ChildGroups.Add(tableGroup71);
            tableGroup69.ChildGroups.Add(tableGroup72);
            tableGroup69.Name = "tableGroup16";
            tableGroup69.ReportItem = this.textBox56;
            this.table7.ColumnGroups.Add(tableGroup69);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox4,
            this.textBox114,
            this.textBox115,
            this.textBox116,
            this.textBox105,
            this.textBox111,
            this.textBox113,
            this.textBox98,
            this.textBox56});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(11.23166561126709D));
            this.table7.Name = "table7";
            tableGroup74.Name = "group47";
            tableGroup75.Name = "group49";
            tableGroup76.Name = "group48";
            tableGroup77.Name = "group50";
            tableGroup73.ChildGroups.Add(tableGroup74);
            tableGroup73.ChildGroups.Add(tableGroup75);
            tableGroup73.ChildGroups.Add(tableGroup76);
            tableGroup73.ChildGroups.Add(tableGroup77);
            tableGroup73.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup73.Name = "detailTableGroup6";
            this.table7.RowGroups.Add(tableGroup73);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(4.4899997711181641D));
            this.table7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5992755889892578D), Telerik.Reporting.Drawing.Unit.Cm(2.0081238746643066D));
            this.textBox105.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox105.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox105.Style.Font.Name = "Arial";
            this.textBox105.StyleName = "";
            this.textBox105.Value = resources.GetString("textBox105.Value");
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1331038475036621D), Telerik.Reporting.Drawing.Unit.Cm(2.0081238746643066D));
            this.textBox111.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox111.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox111.Style.Font.Name = "Arial";
            this.textBox111.StyleName = "";
            this.textBox111.Value = "AS/NZS 3000:2007, Wiring Rules.\r\nAS 4777, this installation complies to this stan" +
    "dard \r\nAS/NZS1768:2007, Lightning Protection. \r\nAS 4777�2005, Grid connection of" +
    " energy systems via inverters";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2576217651367188D), Telerik.Reporting.Drawing.Unit.Cm(2.0081238746643066D));
            this.textBox113.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox113.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox113.Style.Font.Name = "Arial";
            this.textBox113.StyleName = "";
            this.textBox113.Value = resources.GetString("textBox113.Value");
            // 
            // textBox114
            // 
            this.textBox114.CanShrink = false;
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5992755889892578D), Telerik.Reporting.Drawing.Unit.Cm(0.31479313969612122D));
            this.textBox114.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox114.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox114.Style.Font.Bold = true;
            this.textBox114.Style.Font.Name = "Arial";
            this.textBox114.StyleName = "";
            this.textBox114.TextWrap = true;
            this.textBox114.Value = "PV & Inverter Standards";
            // 
            // textBox115
            // 
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1331038475036621D), Telerik.Reporting.Drawing.Unit.Cm(0.31479313969612122D));
            this.textBox115.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox115.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox115.Style.Font.Bold = true;
            this.textBox115.Style.Font.Name = "Arial";
            this.textBox115.StyleName = "";
            this.textBox115.Value = "Grid connected system\r\n";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2576217651367188D), Telerik.Reporting.Drawing.Unit.Cm(0.31479313969612122D));
            this.textBox116.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox116.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox116.Style.Font.Bold = true;
            this.textBox116.Style.Font.Name = "Arial";
            this.textBox116.StyleName = "";
            this.textBox116.Value = "Standalone systems";
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.58208262920379639D));
            this.htmlTextBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox4.Style.Font.Name = "Arial";
            this.htmlTextBox4.StyleName = "";
            this.htmlTextBox4.Value = resources.GetString("htmlTextBox4.Value");
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(1.1614581346511841D));
            this.textBox98.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox98.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox98.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox98.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox98.Style.Font.Name = "Arial";
            this.textBox98.StyleName = "";
            this.textBox98.Value = resources.GetString("textBox98.Value");
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9767329692840576D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5310153961181641D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.026740550994873D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.2144889831542969D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9007444381713867D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.3402779102325439D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.36748406291007996D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.32043153047561646D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.36748415231704712D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.31460022926330566D)));
            this.table8.Body.SetCellContent(2, 0, this.textBox133);
            this.table8.Body.SetCellContent(2, 3, this.textBox136);
            this.table8.Body.SetCellContent(0, 0, this.textBox139);
            this.table8.Body.SetCellContent(0, 3, this.textBox142);
            this.table8.Body.SetCellContent(1, 0, this.textBox112, 1, 2);
            this.table8.Body.SetCellContent(1, 3, this.textBox124, 1, 2);
            this.table8.Body.SetCellContent(0, 2, this.textBox141, 3, 1);
            this.table8.Body.SetCellContent(0, 5, this.textBox144, 3, 1);
            this.table8.Body.SetCellContent(3, 2, this.textBox118);
            this.table8.Body.SetCellContent(3, 5, this.textBox121);
            this.table8.Body.SetCellContent(3, 3, this.textBox119, 1, 2);
            this.table8.Body.SetCellContent(3, 0, this.textBox110, 1, 2);
            this.table8.Body.SetCellContent(0, 1, this.textBox62);
            this.table8.Body.SetCellContent(0, 4, this.textBox42);
            this.table8.Body.SetCellContent(2, 1, this.textBox71);
            this.table8.Body.SetCellContent(2, 4, this.textBox80);
            tableGroup78.Name = "tableGroup19";
            tableGroup79.Name = "tableGroup20";
            tableGroup80.Name = "group53";
            tableGroup81.Name = "group52";
            tableGroup82.Name = "group51";
            tableGroup83.Name = "tableGroup21";
            this.table8.ColumnGroups.Add(tableGroup78);
            this.table8.ColumnGroups.Add(tableGroup79);
            this.table8.ColumnGroups.Add(tableGroup80);
            this.table8.ColumnGroups.Add(tableGroup81);
            this.table8.ColumnGroups.Add(tableGroup82);
            this.table8.ColumnGroups.Add(tableGroup83);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox139,
            this.textBox62,
            this.textBox141,
            this.textBox142,
            this.textBox42,
            this.textBox144,
            this.textBox112,
            this.textBox124,
            this.textBox133,
            this.textBox71,
            this.textBox136,
            this.textBox80,
            this.textBox110,
            this.textBox118,
            this.textBox119,
            this.textBox121});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(15.72186279296875D));
            this.table8.Name = "table8";
            tableGroup85.Name = "group57";
            tableGroup86.Name = "group54";
            tableGroup87.Name = "group56";
            tableGroup88.Name = "group55";
            tableGroup84.ChildGroups.Add(tableGroup85);
            tableGroup84.ChildGroups.Add(tableGroup86);
            tableGroup84.ChildGroups.Add(tableGroup87);
            tableGroup84.ChildGroups.Add(tableGroup88);
            tableGroup84.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup84.Name = "detailTableGroup7";
            this.table8.RowGroups.Add(tableGroup84);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(1.3700000047683716D));
            this.table8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // textBox133
            // 
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9767334461212158D), Telerik.Reporting.Drawing.Unit.Cm(0.36748418211936951D));
            this.textBox133.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox133.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox133.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox133.Style.Font.Name = "Arial";
            this.textBox133.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox133.StyleName = "";
            this.textBox133.Value = "PRINT NAME";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.214489221572876D), Telerik.Reporting.Drawing.Unit.Cm(0.36748418211936951D));
            this.textBox136.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox136.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox136.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox136.Style.Font.Name = "Arial";
            this.textBox136.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox136.StyleName = "";
            this.textBox136.Value = "PRINT NAME";
            // 
            // textBox139
            // 
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9767334461212158D), Telerik.Reporting.Drawing.Unit.Cm(0.36748406291007996D));
            this.textBox139.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox139.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox139.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox139.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox139.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox139.Style.Font.Name = "Arial";
            this.textBox139.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox139.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox139.StyleName = "";
            this.textBox139.Value = "CEC NUMBER:";
            // 
            // textBox142
            // 
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.214489221572876D), Telerik.Reporting.Drawing.Unit.Cm(0.36748406291007996D));
            this.textBox142.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox142.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox142.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox142.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox142.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox142.Style.Font.Name = "Arial";
            this.textBox142.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox142.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox142.StyleName = "";
            this.textBox142.Value = "CEC NUMBER:";
            // 
            // textBox112
            // 
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5077486038208D), Telerik.Reporting.Drawing.Unit.Cm(0.32043153047561646D));
            this.textBox112.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox112.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox112.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox112.Style.Font.Name = "Arial";
            this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox112.Value = "Signature of the SGUs CEC Installer";
            // 
            // textBox124
            // 
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1152334213256836D), Telerik.Reporting.Drawing.Unit.Cm(0.32043153047561646D));
            this.textBox124.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox124.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox124.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox124.Style.Font.Name = "Arial";
            this.textBox124.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox124.StyleName = "";
            this.textBox124.Value = "Signature of the SGUs CEC Designer";
            // 
            // textBox141
            // 
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.026740550994873D), Telerik.Reporting.Drawing.Unit.Cm(1.0553997755050659D));
            this.textBox141.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox141.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox141.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox141.Style.Font.Name = "Arial";
            this.textBox141.StyleName = "";
            // 
            // textBox144
            // 
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3402779102325439D), Telerik.Reporting.Drawing.Unit.Cm(1.0553997755050659D));
            this.textBox144.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox144.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox144.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox144.Style.Font.Name = "Arial";
            this.textBox144.StyleName = "";
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.026740550994873D), Telerik.Reporting.Drawing.Unit.Cm(0.31460022926330566D));
            this.textBox118.Style.Font.Name = "Arial";
            this.textBox118.StyleName = "";
            // 
            // textBox121
            // 
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3402779102325439D), Telerik.Reporting.Drawing.Unit.Cm(0.31460022926330566D));
            this.textBox121.Style.Font.Name = "Arial";
            this.textBox121.StyleName = "";
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1152334213256836D), Telerik.Reporting.Drawing.Unit.Cm(0.31460022926330566D));
            this.textBox119.Style.Font.Bold = true;
            this.textBox119.Style.Font.Name = "Arial";
            this.textBox119.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(25D);
            this.textBox119.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox119.StyleName = "";
            this.textBox119.Value = "DATE:";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5077486038208D), Telerik.Reporting.Drawing.Unit.Cm(0.31460022926330566D));
            this.textBox110.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.textBox110.Style.Font.Bold = true;
            this.textBox110.Style.Font.Name = "Arial";
            this.textBox110.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(25D);
            this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox110.StyleName = "";
            this.textBox110.Value = "DATE:";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5310149192810059D), Telerik.Reporting.Drawing.Unit.Cm(0.36748406291007996D));
            this.textBox62.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox62.Style.Font.Name = "Arial";
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox62.Value = "=Fields.InstallerAccreditation";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9007441997528076D), Telerik.Reporting.Drawing.Unit.Cm(0.36748406291007996D));
            this.textBox42.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox42.Style.Font.Name = "Arial";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox42.StyleName = "";
            this.textBox42.Value = "=Fields.DesignerAccreditation";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5310149192810059D), Telerik.Reporting.Drawing.Unit.Cm(0.36748418211936951D));
            this.textBox71.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox71.Style.Font.Name = "Arial";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox71.Value = "=Fields.InstallerName";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9007441997528076D), Telerik.Reporting.Drawing.Unit.Cm(0.36748418211936951D));
            this.textBox80.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox80.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox80.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox80.Style.Font.Name = "Arial";
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox80.StyleName = "";
            this.textBox80.Value = "=Fields.DesignerName";
            // 
            // textBox117
            // 
            this.textBox117.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(17.091665267944336D));
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.textBox117.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox117.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox117.Style.Font.Bold = true;
            this.textBox117.Style.Font.Name = "Arial";
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox117.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox117.Value = "MANDATORY DECLARATION";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.2943296432495117D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.85174101591110229D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.82529979944229126D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.82529979944229126D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.56565523147583D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.791945219039917D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1839845180511475D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.82529979944229126D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.87818270921707153D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.85174131393432617D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.5689870119094849D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5275337696075439D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D)));
            this.table9.Body.SetCellContent(0, 6, this.textBox123);
            this.table9.Body.SetCellContent(0, 5, this.textBox122);
            this.table9.Body.SetCellContent(0, 0, this.textBox131);
            this.table9.Body.SetCellContent(0, 11, this.textBox135);
            this.table9.Body.SetCellContent(0, 1, this.checkBox1);
            this.table9.Body.SetCellContent(0, 2, this.checkBox2);
            this.table9.Body.SetCellContent(0, 3, this.checkBox3);
            this.table9.Body.SetCellContent(0, 4, this.checkBox4);
            this.table9.Body.SetCellContent(0, 7, this.checkBox5);
            this.table9.Body.SetCellContent(0, 8, this.checkBox16);
            this.table9.Body.SetCellContent(0, 9, this.checkBox17);
            this.table9.Body.SetCellContent(0, 10, this.checkBox18);
            tableGroup89.Name = "group63";
            tableGroup90.Name = "group62";
            tableGroup91.Name = "group61";
            tableGroup92.Name = "group60";
            tableGroup93.Name = "group59";
            tableGroup94.Name = "group58";
            tableGroup95.Name = "tableGroup22";
            tableGroup96.Name = "tableGroup23";
            tableGroup97.Name = "group65";
            tableGroup98.Name = "group64";
            tableGroup99.Name = "tableGroup24";
            tableGroup100.Name = "group66";
            this.table9.ColumnGroups.Add(tableGroup89);
            this.table9.ColumnGroups.Add(tableGroup90);
            this.table9.ColumnGroups.Add(tableGroup91);
            this.table9.ColumnGroups.Add(tableGroup92);
            this.table9.ColumnGroups.Add(tableGroup93);
            this.table9.ColumnGroups.Add(tableGroup94);
            this.table9.ColumnGroups.Add(tableGroup95);
            this.table9.ColumnGroups.Add(tableGroup96);
            this.table9.ColumnGroups.Add(tableGroup97);
            this.table9.ColumnGroups.Add(tableGroup98);
            this.table9.ColumnGroups.Add(tableGroup99);
            this.table9.ColumnGroups.Add(tableGroup100);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox131,
            this.checkBox1,
            this.checkBox2,
            this.checkBox3,
            this.checkBox4,
            this.textBox122,
            this.textBox123,
            this.checkBox5,
            this.checkBox16,
            this.checkBox17,
            this.checkBox18,
            this.textBox135});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.5241663455963135D));
            this.table9.Name = "table9";
            tableGroup101.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup101.Name = "detailTableGroup8";
            this.table9.RowGroups.Add(tableGroup101);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.table9.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table9.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table9.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table9.Style.Font.Name = "Arial";
            this.table9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // textBox123
            // 
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1839849948883057D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.textBox123.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox123.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox123.Value = "Old System Location:";
            // 
            // textBox122
            // 
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.791945219039917D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.textBox122.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox122.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox122.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox122.Style.Font.Bold = true;
            this.textBox122.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox122.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox122.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox122.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox122.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.25D);
            this.textBox122.StyleName = "";
            this.textBox122.Value = "---------------";
            // 
            // textBox131
            // 
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2943294048309326D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.textBox131.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox131.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox131.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox131.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox131.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox131.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox131.StyleName = "";
            this.textBox131.Value = "New System Location:";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5275337696075439D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.textBox135.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox135.Style.Font.Bold = true;
            this.textBox135.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox135.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox135.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox135.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox135.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.25D);
            this.textBox135.StyleName = "";
            this.textBox135.Value = "---------------";
            // 
            // checkBox1
            // 
            this.checkBox1.CheckedImage = ((object)(resources.GetObject("checkBox1.CheckedImage")));
            this.checkBox1.IndeterminateImage = ((object)(resources.GetObject("checkBox1.IndeterminateImage")));
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.85174107551574707D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox1.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox1.StyleName = "";
            this.checkBox1.Text = " E";
            this.checkBox1.UncheckedImage = ((object)(resources.GetObject("checkBox1.UncheckedImage")));
            this.checkBox1.Value = "=False";
            // 
            // checkBox2
            // 
            this.checkBox2.CheckedImage = ((object)(resources.GetObject("checkBox2.CheckedImage")));
            this.checkBox2.IndeterminateImage = ((object)(resources.GetObject("checkBox2.IndeterminateImage")));
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.82529973983764648D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox2.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox2.StyleName = "";
            this.checkBox2.Text = "N";
            this.checkBox2.UncheckedImage = ((object)(resources.GetObject("checkBox2.UncheckedImage")));
            this.checkBox2.Value = "=False";
            // 
            // checkBox3
            // 
            this.checkBox3.CheckedImage = ((object)(resources.GetObject("checkBox3.CheckedImage")));
            this.checkBox3.IndeterminateImage = ((object)(resources.GetObject("checkBox3.IndeterminateImage")));
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.82529973983764648D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox3.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox3.StyleName = "";
            this.checkBox3.Text = "W";
            this.checkBox3.UncheckedImage = ((object)(resources.GetObject("checkBox3.UncheckedImage")));
            this.checkBox3.Value = "=False";
            // 
            // checkBox4
            // 
            this.checkBox4.CheckedImage = ((object)(resources.GetObject("checkBox4.CheckedImage")));
            this.checkBox4.IndeterminateImage = ((object)(resources.GetObject("checkBox4.IndeterminateImage")));
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5656553506851196D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox4.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox4.StyleName = "";
            this.checkBox4.Text = "Others";
            this.checkBox4.UncheckedImage = ((object)(resources.GetObject("checkBox4.UncheckedImage")));
            this.checkBox4.Value = "=False";
            // 
            // checkBox5
            // 
            this.checkBox5.CheckedImage = ((object)(resources.GetObject("checkBox5.CheckedImage")));
            this.checkBox5.IndeterminateImage = ((object)(resources.GetObject("checkBox5.IndeterminateImage")));
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.82529973983764648D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox5.StyleName = "";
            this.checkBox5.Text = "E";
            this.checkBox5.UncheckedImage = ((object)(resources.GetObject("checkBox5.UncheckedImage")));
            this.checkBox5.Value = "=False";
            // 
            // checkBox16
            // 
            this.checkBox16.CheckedImage = ((object)(resources.GetObject("checkBox16.CheckedImage")));
            this.checkBox16.IndeterminateImage = ((object)(resources.GetObject("checkBox16.IndeterminateImage")));
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.87818264961242676D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox16.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox16.StyleName = "";
            this.checkBox16.Text = "N";
            this.checkBox16.UncheckedImage = ((object)(resources.GetObject("checkBox16.UncheckedImage")));
            this.checkBox16.Value = "=False";
            // 
            // checkBox17
            // 
            this.checkBox17.CheckedImage = ((object)(resources.GetObject("checkBox17.CheckedImage")));
            this.checkBox17.IndeterminateImage = ((object)(resources.GetObject("checkBox17.IndeterminateImage")));
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.851741373538971D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox17.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox17.StyleName = "";
            this.checkBox17.Text = "W";
            this.checkBox17.UncheckedImage = ((object)(resources.GetObject("checkBox17.UncheckedImage")));
            this.checkBox17.Value = "=False";
            // 
            // checkBox18
            // 
            this.checkBox18.CheckedImage = ((object)(resources.GetObject("checkBox18.CheckedImage")));
            this.checkBox18.IndeterminateImage = ((object)(resources.GetObject("checkBox18.IndeterminateImage")));
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5689868927001953D), Telerik.Reporting.Drawing.Unit.Cm(0.3177083432674408D));
            this.checkBox18.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox18.StyleName = "";
            this.checkBox18.Text = "Others";
            this.checkBox18.UncheckedImage = ((object)(resources.GetObject("checkBox18.UncheckedImage")));
            this.checkBox18.Value = "=False";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0694425106048584D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0790227651596069D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.83148735761642456D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4010958671569824D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1189517974853516D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49442791938781738D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.43797692656517029D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7917448878288269D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.40989449620246887D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.4372234046459198D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0627779960632324D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.46304652094841003D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.38256624341011047D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.3926331996917725D)));
            this.table10.Body.SetCellContent(0, 3, this.textBox128);
            this.table10.Body.SetCellContent(0, 4, this.textBox130);
            this.table10.Body.SetCellContent(0, 0, this.textBox126, 1, 3);
            this.table10.Body.SetCellContent(1, 0, this.textBox125, 1, 5);
            this.table10.Body.SetCellContent(2, 0, this.textBox157);
            this.table10.Body.SetCellContent(2, 1, this.textBox132, 1, 2);
            this.table10.Body.SetCellContent(2, 3, this.textBox137, 1, 2);
            this.table10.Body.SetCellContent(3, 4, this.textBox149);
            this.table10.Body.SetCellContent(5, 4, this.textBox158);
            this.table10.Body.SetCellContent(6, 4, this.textBox173);
            this.table10.Body.SetCellContent(3, 0, this.textBox143, 1, 4);
            this.table10.Body.SetCellContent(4, 0, this.textBox159, 1, 3);
            this.table10.Body.SetCellContent(4, 3, this.textBox162, 1, 2);
            this.table10.Body.SetCellContent(5, 2, this.textBox154, 1, 2);
            this.table10.Body.SetCellContent(5, 0, this.textBox151, 1, 2);
            this.table10.Body.SetCellContent(6, 0, this.textBox169, 1, 2);
            this.table10.Body.SetCellContent(6, 2, this.textBox171, 1, 2);
            this.table10.Body.SetCellContent(7, 0, this.textBox164, 1, 3);
            this.table10.Body.SetCellContent(7, 3, this.textBox167, 1, 2);
            this.table10.Body.SetCellContent(8, 0, this.textBox127, 1, 5);
            tableGroup102.Name = "tableGroup25";
            tableGroup103.Name = "group68";
            tableGroup104.Name = "group67";
            tableGroup105.Name = "tableGroup26";
            tableGroup106.Name = "tableGroup27";
            this.table10.ColumnGroups.Add(tableGroup102);
            this.table10.ColumnGroups.Add(tableGroup103);
            this.table10.ColumnGroups.Add(tableGroup104);
            this.table10.ColumnGroups.Add(tableGroup105);
            this.table10.ColumnGroups.Add(tableGroup106);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox126,
            this.textBox128,
            this.textBox130,
            this.textBox125,
            this.textBox157,
            this.textBox132,
            this.textBox137,
            this.textBox143,
            this.textBox149,
            this.textBox159,
            this.textBox162,
            this.textBox151,
            this.textBox154,
            this.textBox158,
            this.textBox169,
            this.textBox171,
            this.textBox173,
            this.textBox164,
            this.textBox167,
            this.textBox127});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.4899997711181641D), Telerik.Reporting.Drawing.Unit.Cm(17.50312614440918D));
            this.table10.Name = "table10";
            tableGroup108.Name = "group69";
            tableGroup109.Name = "group70";
            tableGroup110.Name = "group72";
            tableGroup111.Name = "group73";
            tableGroup112.Name = "group77";
            tableGroup113.Name = "group76";
            tableGroup114.Name = "group79";
            tableGroup115.Name = "group78";
            tableGroup116.Name = "group71";
            tableGroup107.ChildGroups.Add(tableGroup108);
            tableGroup107.ChildGroups.Add(tableGroup109);
            tableGroup107.ChildGroups.Add(tableGroup110);
            tableGroup107.ChildGroups.Add(tableGroup111);
            tableGroup107.ChildGroups.Add(tableGroup112);
            tableGroup107.ChildGroups.Add(tableGroup113);
            tableGroup107.ChildGroups.Add(tableGroup114);
            tableGroup107.ChildGroups.Add(tableGroup115);
            tableGroup107.ChildGroups.Add(tableGroup116);
            tableGroup107.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup107.Name = "detailTableGroup9";
            this.table10.RowGroups.Add(tableGroup107);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.5D), Telerik.Reporting.Drawing.Unit.Cm(5.8722915649414062D));
            this.table10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table10.Style.Font.Name = "Arial";
            this.table10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.table10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4010956287384033D), Telerik.Reporting.Drawing.Unit.Cm(0.49442803859710693D));
            this.textBox128.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox128.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox128.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox128.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox128.Value = "=Fields.STCNumber";
            // 
            // textBox130
            // 
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1189517974853516D), Telerik.Reporting.Drawing.Unit.Cm(0.49442803859710693D));
            this.textBox130.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox130.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox130.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox130.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox130.Value = "STCs and in exchange";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.979952335357666D), Telerik.Reporting.Drawing.Unit.Cm(0.49442803859710693D));
            this.textBox126.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox126.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox126.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox126.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.Value = "I understand that this system is eligible for";
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.5D), Telerik.Reporting.Drawing.Unit.Cm(0.43797701597213745D));
            this.textBox125.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox125.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox125.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox125.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox125.StyleName = "";
            this.textBox125.Value = "my right to create these STCs, I will receive from the installer/supplier;";
            // 
            // textBox157
            // 
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0694425106048584D), Telerik.Reporting.Drawing.Unit.Cm(0.79174482822418213D));
            this.textBox157.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox157.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox157.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox157.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox157.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox157.StyleName = "";
            this.textBox157.Value = "Total Costs for Installation";
            // 
            // textBox132
            // 
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9105099439620972D), Telerik.Reporting.Drawing.Unit.Cm(0.79174482822418213D));
            this.textBox132.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox132.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox132.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox132.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox132.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox132.StyleName = "";
            this.textBox132.Value = "$";
            // 
            // textBox137
            // 
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.520047664642334D), Telerik.Reporting.Drawing.Unit.Cm(0.79174482822418213D));
            this.textBox137.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox137.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox137.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox137.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox137.StyleName = "";
            this.textBox137.Value = "Connection Cost $ ______ , Any Other";
            // 
            // textBox149
            // 
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1189517974853516D), Telerik.Reporting.Drawing.Unit.Cm(0.40989464521408081D));
            this.textBox149.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox149.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox149.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox149.StyleName = "";
            this.textBox149.Value = "$";
            // 
            // textBox158
            // 
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1189517974853516D), Telerik.Reporting.Drawing.Unit.Cm(1.0627778768539429D));
            this.textBox158.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox158.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox158.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox158.StyleName = "";
            // 
            // textBox173
            // 
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1189517974853516D), Telerik.Reporting.Drawing.Unit.Cm(0.46304652094841003D));
            this.textBox173.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox173.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox173.Style.Font.Bold = true;
            this.textBox173.Style.Font.Name = "Arial";
            this.textBox173.StyleName = "";
            this.textBox173.Value = "Witness Signature";
            // 
            // textBox143
            // 
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.3810482025146484D), Telerik.Reporting.Drawing.Unit.Cm(0.40989464521408081D));
            this.textBox143.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox143.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox143.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox143.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.StyleName = "";
            this.textBox143.Value = "Costs $  ___ Deduct value of STCs, any rebates or benefits";
            // 
            // textBox159
            // 
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.979952335357666D), Telerik.Reporting.Drawing.Unit.Cm(0.437223345041275D));
            this.textBox159.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox159.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox159.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox159.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox159.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox159.StyleName = "";
            this.textBox159.Value = "Out of Pocket Expense";
            // 
            // textBox162
            // 
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.520047664642334D), Telerik.Reporting.Drawing.Unit.Cm(0.437223345041275D));
            this.textBox162.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox162.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox162.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox162.StyleName = "";
            this.textBox162.Value = "$";
            // 
            // textBox154
            // 
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2325830459594727D), Telerik.Reporting.Drawing.Unit.Cm(1.0627778768539429D));
            this.textBox154.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox154.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox154.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox154.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox154.StyleName = "";
            // 
            // textBox151
            // 
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1484651565551758D), Telerik.Reporting.Drawing.Unit.Cm(1.0627778768539429D));
            this.textBox151.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox151.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox151.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox151.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox151.StyleName = "";
            // 
            // textBox169
            // 
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1484651565551758D), Telerik.Reporting.Drawing.Unit.Cm(0.46304652094841003D));
            this.textBox169.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox169.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox169.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox169.Style.Font.Bold = true;
            this.textBox169.Style.Font.Name = "Arial";
            this.textBox169.StyleName = "";
            this.textBox169.Value = "Owner Signature";
            // 
            // textBox171
            // 
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2325830459594727D), Telerik.Reporting.Drawing.Unit.Cm(0.46304652094841003D));
            this.textBox171.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox171.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox171.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox171.Style.Font.Bold = true;
            this.textBox171.Style.Font.Name = "Arial";
            this.textBox171.StyleName = "";
            this.textBox171.Value = "\tElectrician Signature";
            // 
            // textBox164
            // 
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.979952335357666D), Telerik.Reporting.Drawing.Unit.Cm(0.38256621360778809D));
            this.textBox164.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox164.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox164.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox164.Style.Font.Bold = true;
            this.textBox164.Style.Font.Name = "Arial";
            this.textBox164.StyleName = "";
            this.textBox164.Value = "Date:";
            // 
            // textBox167
            // 
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.520047664642334D), Telerik.Reporting.Drawing.Unit.Cm(0.38256621360778809D));
            this.textBox167.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox167.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox167.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox167.StyleName = "";
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.5D), Telerik.Reporting.Drawing.Unit.Cm(1.3926331996917725D));
            this.textBox127.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox127.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox127.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox127.StyleName = "";
            this.textBox127.Value = resources.GetString("textBox127.Value");
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.299293041229248D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1574411392211914D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.6469206809997559D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.658899188041687D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5195322036743164D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7079136371612549D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.39687499403953552D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458320021629333D)));
            this.table11.Body.SetCellContent(0, 5, this.textBox147);
            this.table11.Body.SetCellContent(1, 5, this.textBox160);
            this.table11.Body.SetCellContent(0, 0, this.textBox138, 2, 1);
            this.table11.Body.SetCellContent(0, 2, this.textBox156, 2, 1);
            this.table11.Body.SetCellContent(0, 4, this.textBox150, 2, 1);
            this.table11.Body.SetCellContent(0, 1, this.textBox140, 2, 1);
            this.table11.Body.SetCellContent(0, 3, this.textBox145, 2, 1);
            tableGroup118.Name = "group81";
            tableGroup119.Name = "tableGroup29";
            tableGroup120.Name = "group80";
            tableGroup121.Name = "group75";
            tableGroup122.Name = "group74";
            tableGroup123.Name = "tableGroup30";
            tableGroup117.ChildGroups.Add(tableGroup118);
            tableGroup117.ChildGroups.Add(tableGroup119);
            tableGroup117.ChildGroups.Add(tableGroup120);
            tableGroup117.ChildGroups.Add(tableGroup121);
            tableGroup117.ChildGroups.Add(tableGroup122);
            tableGroup117.ChildGroups.Add(tableGroup123);
            tableGroup117.Name = "tableGroup28";
            tableGroup117.ReportItem = this.textBox134;
            this.table11.ColumnGroups.Add(tableGroup117);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox138,
            this.textBox140,
            this.textBox156,
            this.textBox145,
            this.textBox150,
            this.textBox147,
            this.textBox160,
            this.textBox134});
            this.table11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(23.382501602172852D));
            this.table11.Name = "table11";
            tableGroup125.Name = "group82";
            tableGroup126.Name = "group83";
            tableGroup124.ChildGroups.Add(tableGroup125);
            tableGroup124.ChildGroups.Add(tableGroup126);
            tableGroup124.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup124.Name = "detailTableGroup10";
            this.table11.RowGroups.Add(tableGroup124);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(1.0614582300186157D));
            this.table11.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table11.Style.Font.Name = "Arial";
            this.table11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // textBox147
            // 
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7079137563705444D), Telerik.Reporting.Drawing.Unit.Cm(0.39687499403953552D));
            this.textBox147.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox147.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox147.Style.Font.Name = "Arial";
            this.textBox147.Value = "N/A\t";
            // 
            // textBox160
            // 
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7079137563705444D), Telerik.Reporting.Drawing.Unit.Cm(0.26458320021629333D));
            this.textBox160.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox160.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox160.Style.Font.Name = "Arial";
            this.textBox160.StyleName = "";
            this.textBox160.Value = "N/A";
            // 
            // textBox138
            // 
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.299293041229248D), Telerik.Reporting.Drawing.Unit.Cm(0.66145819425582886D));
            this.textBox138.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox138.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox138.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox138.Style.Font.Name = "Arial";
            this.textBox138.Value = "Is this SGU Used for Commercial or Domestic use? ";
            // 
            // textBox156
            // 
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6469202041625977D), Telerik.Reporting.Drawing.Unit.Cm(0.66145819425582886D));
            this.textBox156.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox156.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox156.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox156.Style.Font.Name = "Arial";
            this.textBox156.StyleName = "";
            this.textBox156.Value = "Is the owner of the SGU registered for GST? ";
            // 
            // textBox150
            // 
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5195326805114746D), Telerik.Reporting.Drawing.Unit.Cm(0.66145819425582886D));
            this.textBox150.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox150.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox150.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox150.Style.Font.Name = "Arial";
            this.textBox150.StyleName = "";
            this.textBox150.Value = "If so, please provide full Business Name and ABN N/A N/A ";
            // 
            // textBox140
            // 
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1574409008026123D), Telerik.Reporting.Drawing.Unit.Cm(0.66145819425582886D));
            this.textBox140.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox140.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox140.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox140.Style.Font.Name = "Arial";
            this.textBox140.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox140.Value = "=iif(Fields.ResCom=\"1\",\"Residential\",\"Commercial\")";
            // 
            // textBox145
            // 
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6588990688323975D), Telerik.Reporting.Drawing.Unit.Cm(0.66145819425582886D));
            this.textBox145.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox145.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox145.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox145.Style.Font.Name = "Arial";
            this.textBox145.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox145.Value = "=iif(Fields.OwnerGSTRegistered=\"True\",\"Yes\",\"No\")";
            // 
            // table12
            // 
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.87773591279983521D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.58243441581726074D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5824350118637085D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5824350118637085D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60903280973434448D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.87501430511474609D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.316454142332077D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.58243471384048462D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.84785670042037964D)));
            this.table12.Body.SetCellContent(0, 0, this.textBox148);
            this.table12.Body.SetCellContent(4, 0, this.textBox146);
            this.table12.Body.SetCellContent(3, 0, this.textBox152);
            this.table12.Body.SetCellContent(2, 0, this.textBox153);
            this.table12.Body.SetCellContent(1, 0, this.textBox155);
            this.table12.Body.SetCellContent(8, 0, this.textBox161);
            this.table12.Body.SetCellContent(5, 0, this.textBox163);
            this.table12.Body.SetCellContent(7, 0, this.textBox165);
            this.table12.Body.SetCellContent(6, 0, this.textBox166);
            tableGroup127.Name = "tableGroup31";
            this.table12.ColumnGroups.Add(tableGroup127);
            this.table12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox148,
            this.textBox155,
            this.textBox153,
            this.textBox152,
            this.textBox146,
            this.textBox163,
            this.textBox166,
            this.textBox165,
            this.textBox161});
            this.table12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(17.504167556762695D));
            this.table12.Name = "table12";
            tableGroup129.Name = "group84";
            tableGroup130.Name = "group88";
            tableGroup131.Name = "group87";
            tableGroup132.Name = "group86";
            tableGroup133.Name = "group85";
            tableGroup134.Name = "group90";
            tableGroup135.Name = "group92";
            tableGroup136.Name = "group91";
            tableGroup137.Name = "group89";
            tableGroup128.ChildGroups.Add(tableGroup129);
            tableGroup128.ChildGroups.Add(tableGroup130);
            tableGroup128.ChildGroups.Add(tableGroup131);
            tableGroup128.ChildGroups.Add(tableGroup132);
            tableGroup128.ChildGroups.Add(tableGroup133);
            tableGroup128.ChildGroups.Add(tableGroup134);
            tableGroup128.ChildGroups.Add(tableGroup135);
            tableGroup128.ChildGroups.Add(tableGroup136);
            tableGroup128.ChildGroups.Add(tableGroup137);
            tableGroup128.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup128.Name = "detailTableGroup11";
            this.table12.RowGroups.Add(tableGroup128);
            this.table12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(5.8558330535888672D));
            this.table12.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table12.Style.Font.Name = "Arial";
            this.table12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // textBox148
            // 
            this.textBox148.Format = "{0}";
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.87773585319519043D));
            this.textBox148.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox148.Style.BackgroundImage.ImageData")));
            this.textBox148.Style.BackgroundImage.MimeType = "image/png";
            this.textBox148.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox148.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox148.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox148.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox148.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox148.Style.Font.Name = "Arial";
            this.textBox148.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox148.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox148.Value = resources.GetString("textBox148.Value");
            // 
            // textBox146
            // 
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.609032928943634D));
            this.textBox146.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox146.Style.BackgroundImage.ImageData")));
            this.textBox146.Style.BackgroundImage.MimeType = "image/png";
            this.textBox146.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox146.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox146.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox146.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox146.Style.Font.Name = "Arial";
            this.textBox146.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox146.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox146.StyleName = "";
            this.textBox146.Value = "I agree to repay the STC payment to **ACHIEVERS ENERGY GROUP PTY LTD** should my " +
    "assignment be invalid";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.5824350118637085D));
            this.textBox152.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox152.Style.BackgroundImage.ImageData")));
            this.textBox152.Style.BackgroundImage.MimeType = "image/png";
            this.textBox152.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox152.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox152.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox152.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox152.Style.Font.Name = "Arial";
            this.textBox152.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox152.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox152.StyleName = "";
            this.textBox152.Value = "I understand I am under no obligation to assign STCs to **ACHIEVERS\r\nENERGY GROUP" +
    " PTY LTD**";
            // 
            // textBox153
            // 
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.5824350118637085D));
            this.textBox153.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox153.Style.BackgroundImage.ImageData")));
            this.textBox153.Style.BackgroundImage.MimeType = "image/png";
            this.textBox153.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox153.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox153.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox153.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox153.Style.Font.Name = "Arial";
            this.textBox153.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox153.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox153.StyleName = "";
            this.textBox153.Value = "To claim 15 years deeming for SGU, STCs must be registered within 12 months of in" +
    "stallation.";
            // 
            // textBox155
            // 
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.58243447542190552D));
            this.textBox155.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox155.Style.BackgroundImage.ImageData")));
            this.textBox155.Style.BackgroundImage.MimeType = "image/png";
            this.textBox155.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox155.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox155.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox155.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox155.Style.Font.Name = "Arial";
            this.textBox155.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox155.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox155.StyleName = "";
            this.textBox155.Value = "I have not previously assigned or created any STCs for this system within this pe" +
    "riod.";
            // 
            // textBox161
            // 
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.84785681962966919D));
            this.textBox161.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox161.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox161.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox161.Style.Font.Name = "Arial";
            this.textBox161.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox161.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox161.StyleName = "";
            this.textBox161.Value = "I further declare that the accredited CEC installer named on this form physically" +
    "\r\nattended the installation of the unit";
            // 
            // textBox163
            // 
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.87501436471939087D));
            this.textBox163.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox163.Style.BackgroundImage.ImageData")));
            this.textBox163.Style.BackgroundImage.MimeType = "image/png";
            this.textBox163.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox163.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox163.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox163.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox163.Style.Font.Name = "Arial";
            this.textBox163.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox163.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox163.StyleName = "";
            this.textBox163.Value = "I understand that an agent of the Office of the Clean Energy Regulator or **ACHIE" +
    "VERS ENERGY GROUP PTY LTD** may wish to inspect the SGU\r\nwithin the first years " +
    "of certificate redemption.";
            // 
            // textBox165
            // 
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.58243477344512939D));
            this.textBox165.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox165.Style.BackgroundImage.ImageData")));
            this.textBox165.Style.BackgroundImage.MimeType = "image/png";
            this.textBox165.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox165.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox165.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox165.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox165.Style.Font.Name = "Arial";
            this.textBox165.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox165.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox165.StyleName = "";
            this.textBox165.Value = "I am aware that penalties can be applied for providing misleading information in\r" +
    "\nthis form under the Renewable Energy (Electricity) Act 2000.";
            // 
            // textBox166
            // 
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.4877166748046875D), Telerik.Reporting.Drawing.Unit.Cm(0.31645408272743225D));
            this.textBox166.Style.BackgroundImage.ImageData = ((System.Drawing.Image)(resources.GetObject("textBox166.Style.BackgroundImage.ImageData")));
            this.textBox166.Style.BackgroundImage.MimeType = "image/png";
            this.textBox166.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox166.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox166.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox166.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox166.Style.Font.Name = "Arial";
            this.textBox166.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox166.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox166.StyleName = "";
            this.textBox166.Value = "I must retain receipts and proof of the installation date for the life of the STC" +
    "s.";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox5,
            this.pictureBox1});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(24.675830841064453D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.0999999046325684D));
            this.panel1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.58096981048584D), Telerik.Reporting.Drawing.Unit.Cm(-1.0093052793536117E-07D));
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.htmlTextBox5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox5.Value = resources.GetString("htmlTextBox5.Value");
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.38679406046867371D), Telerik.Reporting.Drawing.Unit.Cm(0.10583332926034927D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.Font.Name = "Open Sans";
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table13
            // 
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(12.321876525878906D)));
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2835726737976074D)));
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3845508098602295D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(10.34999942779541D)));
            this.table13.Body.SetCellContent(0, 0, this.htmlTextBox6, 1, 3);
            tableGroup138.Name = "tableGroup32";
            tableGroup138.ReportItem = this.textBox168;
            tableGroup139.Name = "tableGroup33";
            tableGroup139.ReportItem = this.textBox172;
            tableGroup140.Name = "tableGroup34";
            tableGroup140.ReportItem = this.textBox175;
            this.table13.ColumnGroups.Add(tableGroup138);
            this.table13.ColumnGroups.Add(tableGroup139);
            this.table13.ColumnGroups.Add(tableGroup140);
            this.table13.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox6,
            this.textBox168,
            this.textBox172,
            this.textBox175});
            this.table13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(26.776033401489258D));
            this.table13.Name = "table13";
            tableGroup141.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup141.Name = "detailTableGroup12";
            this.table13.RowGroups.Add(tableGroup141);
            this.table13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(10.932045936584473D));
            this.table13.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table13.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(10.34999942779541D));
            this.htmlTextBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.htmlTextBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox6.Style.Font.Name = "Microsoft Sans Serif";
            this.htmlTextBox6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.htmlTextBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.htmlTextBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox6.StyleName = "";
            this.htmlTextBox6.Value = resources.GetString("htmlTextBox6.Value");
            // 
            // table14
            // 
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(10.019036293029785D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9872090816497803D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9982702732086182D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9982702732086182D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9872090816497803D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52920562028884888D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50003612041473389D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50003612041473389D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50003612041473389D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50003612041473389D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0292410850524902D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.57941699028015137D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(6.4536676406860352D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.81753617525100708D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4261659383773804D)));
            this.table14.Body.SetCellContent(0, 0, this.textBox174);
            this.table14.Body.SetCellContent(0, 1, this.textBox177);
            this.table14.Body.SetCellContent(0, 4, this.textBox179);
            this.table14.Body.SetCellContent(0, 3, this.textBox176);
            this.table14.Body.SetCellContent(0, 2, this.textBox178);
            this.table14.Body.SetCellContent(4, 0, this.textBox180);
            this.table14.Body.SetCellContent(4, 1, this.textBox181);
            this.table14.Body.SetCellContent(4, 2, this.textBox182);
            this.table14.Body.SetCellContent(4, 3, this.textBox183);
            this.table14.Body.SetCellContent(4, 4, this.textBox184);
            this.table14.Body.SetCellContent(3, 0, this.textBox185);
            this.table14.Body.SetCellContent(3, 1, this.textBox186);
            this.table14.Body.SetCellContent(3, 2, this.textBox187);
            this.table14.Body.SetCellContent(3, 3, this.textBox188);
            this.table14.Body.SetCellContent(3, 4, this.textBox189);
            this.table14.Body.SetCellContent(2, 0, this.textBox190);
            this.table14.Body.SetCellContent(2, 1, this.textBox191);
            this.table14.Body.SetCellContent(2, 2, this.textBox192);
            this.table14.Body.SetCellContent(2, 3, this.textBox193);
            this.table14.Body.SetCellContent(2, 4, this.textBox194);
            this.table14.Body.SetCellContent(1, 0, this.textBox195);
            this.table14.Body.SetCellContent(1, 1, this.textBox196);
            this.table14.Body.SetCellContent(1, 2, this.textBox197);
            this.table14.Body.SetCellContent(1, 3, this.textBox198);
            this.table14.Body.SetCellContent(1, 4, this.textBox199);
            this.table14.Body.SetCellContent(9, 0, this.textBox200);
            this.table14.Body.SetCellContent(6, 0, this.textBox215, 1, 5);
            this.table14.Body.SetCellContent(7, 0, this.textBox210, 1, 5);
            this.table14.Body.SetCellContent(8, 0, this.textBox205, 1, 5);
            this.table14.Body.SetCellContent(9, 3, this.textBox203, 1, 2);
            this.table14.Body.SetCellContent(9, 1, this.textBox202, 1, 2);
            this.table14.Body.SetCellContent(5, 0, this.textBox201, 1, 5);
            tableGroup143.Name = "group93";
            tableGroup144.Name = "tableGroup36";
            tableGroup145.Name = "group95";
            tableGroup146.Name = "group94";
            tableGroup147.Name = "tableGroup37";
            tableGroup142.ChildGroups.Add(tableGroup143);
            tableGroup142.ChildGroups.Add(tableGroup144);
            tableGroup142.ChildGroups.Add(tableGroup145);
            tableGroup142.ChildGroups.Add(tableGroup146);
            tableGroup142.ChildGroups.Add(tableGroup147);
            tableGroup142.Name = "tableGroup35";
            tableGroup142.ReportItem = this.textBox170;
            this.table14.ColumnGroups.Add(tableGroup142);
            this.table14.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox174,
            this.textBox177,
            this.textBox178,
            this.textBox176,
            this.textBox179,
            this.textBox195,
            this.textBox196,
            this.textBox197,
            this.textBox198,
            this.textBox199,
            this.textBox190,
            this.textBox191,
            this.textBox192,
            this.textBox193,
            this.textBox194,
            this.textBox185,
            this.textBox186,
            this.textBox187,
            this.textBox188,
            this.textBox189,
            this.textBox180,
            this.textBox181,
            this.textBox182,
            this.textBox183,
            this.textBox184,
            this.textBox201,
            this.textBox215,
            this.textBox210,
            this.textBox205,
            this.textBox200,
            this.textBox202,
            this.textBox203,
            this.textBox170});
            this.table14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(37.716960906982422D));
            this.table14.Name = "table14";
            tableGroup149.Name = "group96";
            tableGroup150.Name = "group100";
            tableGroup151.Name = "group99";
            tableGroup152.Name = "group98";
            tableGroup153.Name = "group97";
            tableGroup154.Name = "group105";
            tableGroup155.Name = "group104";
            tableGroup156.Name = "group103";
            tableGroup157.Name = "group102";
            tableGroup158.Name = "group101";
            tableGroup148.ChildGroups.Add(tableGroup149);
            tableGroup148.ChildGroups.Add(tableGroup150);
            tableGroup148.ChildGroups.Add(tableGroup151);
            tableGroup148.ChildGroups.Add(tableGroup152);
            tableGroup148.ChildGroups.Add(tableGroup153);
            tableGroup148.ChildGroups.Add(tableGroup154);
            tableGroup148.ChildGroups.Add(tableGroup155);
            tableGroup148.ChildGroups.Add(tableGroup156);
            tableGroup148.ChildGroups.Add(tableGroup157);
            tableGroup148.ChildGroups.Add(tableGroup158);
            tableGroup148.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup148.Name = "detailTableGroup13";
            this.table14.RowGroups.Add(tableGroup148);
            this.table14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.9899959564209D), Telerik.Reporting.Drawing.Unit.Cm(13.443963050842285D));
            this.table14.Style.Font.Name = "Arial";
            this.table14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            // 
            // textBox174
            // 
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.529205322265625D));
            this.textBox174.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox174.Style.Font.Name = "Arial";
            this.textBox174.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox174.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox174.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox174.Value = "Overall Impression";
            // 
            // textBox177
            // 
            this.textBox177.Name = "textBox177";
            this.textBox177.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.529205322265625D));
            this.textBox177.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox177.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox177.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox177.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox177.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox177.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox177.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox177.Value = "1";
            // 
            // textBox179
            // 
            this.textBox179.Name = "textBox179";
            this.textBox179.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.529205322265625D));
            this.textBox179.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox179.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox179.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox179.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox179.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox179.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox179.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox179.Value = "4";
            // 
            // textBox176
            // 
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.529205322265625D));
            this.textBox176.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox176.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox176.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox176.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox176.StyleName = "";
            this.textBox176.Value = "3";
            // 
            // textBox178
            // 
            this.textBox178.Name = "textBox178";
            this.textBox178.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.529205322265625D));
            this.textBox178.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox178.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox178.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox178.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox178.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox178.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox178.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox178.StyleName = "";
            this.textBox178.Value = "2";
            // 
            // textBox180
            // 
            this.textBox180.Name = "textBox180";
            this.textBox180.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox180.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox180.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox180.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox180.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox180.Style.Font.Name = "Arial";
            this.textBox180.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox180.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox180.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox180.StyleName = "";
            this.textBox180.Value = "Personal View :";
            // 
            // textBox181
            // 
            this.textBox181.Name = "textBox181";
            this.textBox181.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox181.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox181.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox181.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox181.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox181.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox181.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox181.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox181.StyleName = "";
            this.textBox181.Value = "1";
            // 
            // textBox182
            // 
            this.textBox182.Name = "textBox182";
            this.textBox182.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox182.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox182.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox182.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox182.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox182.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox182.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox182.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox182.StyleName = "";
            this.textBox182.Value = "2";
            // 
            // textBox183
            // 
            this.textBox183.Name = "textBox183";
            this.textBox183.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox183.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox183.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox183.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox183.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox183.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox183.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox183.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox183.StyleName = "";
            this.textBox183.Value = "3";
            // 
            // textBox184
            // 
            this.textBox184.Name = "textBox184";
            this.textBox184.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox184.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox184.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox184.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox184.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox184.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox184.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox184.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox184.StyleName = "";
            this.textBox184.Value = "4";
            // 
            // textBox185
            // 
            this.textBox185.Name = "textBox185";
            this.textBox185.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox185.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox185.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox185.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox185.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox185.Style.Font.Name = "Arial";
            this.textBox185.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox185.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox185.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox185.StyleName = "";
            this.textBox185.Value = "Your Experience";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox186.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox186.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox186.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox186.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox186.StyleName = "";
            this.textBox186.Value = "1";
            // 
            // textBox187
            // 
            this.textBox187.Name = "textBox187";
            this.textBox187.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox187.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox187.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox187.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox187.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox187.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox187.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox187.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox187.StyleName = "";
            this.textBox187.Value = "2";
            // 
            // textBox188
            // 
            this.textBox188.Name = "textBox188";
            this.textBox188.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox188.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox188.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox188.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox188.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox188.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox188.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox188.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox188.StyleName = "";
            this.textBox188.Value = "3";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox189.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox189.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.StyleName = "";
            this.textBox189.Value = "4";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox190.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox190.Style.Font.Name = "Arial";
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox190.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox190.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox190.StyleName = "";
            this.textBox190.Value = "Installation";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox191.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox191.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.StyleName = "";
            this.textBox191.Value = "1";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox192.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox192.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.StyleName = "";
            this.textBox192.Value = "2";
            // 
            // textBox193
            // 
            this.textBox193.Name = "textBox193";
            this.textBox193.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox193.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox193.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox193.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox193.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox193.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox193.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox193.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox193.StyleName = "";
            this.textBox193.Value = "3";
            // 
            // textBox194
            // 
            this.textBox194.Name = "textBox194";
            this.textBox194.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox194.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox194.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox194.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox194.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox194.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox194.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox194.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox194.StyleName = "";
            this.textBox194.Value = "4";
            // 
            // textBox195
            // 
            this.textBox195.Name = "textBox195";
            this.textBox195.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox195.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox195.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox195.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox195.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox195.Style.Font.Name = "Arial";
            this.textBox195.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox195.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox195.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox195.StyleName = "";
            this.textBox195.Value = "Services / Technical Support";
            // 
            // textBox196
            // 
            this.textBox196.Name = "textBox196";
            this.textBox196.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox196.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox196.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox196.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox196.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox196.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox196.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox196.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox196.StyleName = "";
            this.textBox196.Value = "1";
            // 
            // textBox197
            // 
            this.textBox197.Name = "textBox197";
            this.textBox197.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox197.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox197.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox197.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox197.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox197.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox197.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox197.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox197.StyleName = "";
            this.textBox197.Value = "2";
            // 
            // textBox198
            // 
            this.textBox198.Name = "textBox198";
            this.textBox198.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982705116271973D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox198.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox198.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox198.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox198.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox198.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox198.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox198.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox198.StyleName = "";
            this.textBox198.Value = "3";
            // 
            // textBox199
            // 
            this.textBox199.Name = "textBox199";
            this.textBox199.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox199.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox199.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox199.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox199.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox199.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox199.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox199.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox199.StyleName = "";
            this.textBox199.Value = "4";
            // 
            // textBox200
            // 
            this.textBox200.Name = "textBox200";
            this.textBox200.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019038200378418D), Telerik.Reporting.Drawing.Unit.Cm(1.4261646270751953D));
            this.textBox200.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox200.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox200.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox200.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox200.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.textBox200.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox200.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox200.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox200.StyleName = "";
            this.textBox200.Value = "Name_______________________________";
            // 
            // textBox215
            // 
            this.textBox215.Name = "textBox215";
            this.textBox215.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989997863769531D), Telerik.Reporting.Drawing.Unit.Cm(0.57941687107086182D));
            this.textBox215.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox215.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox215.Style.Font.Bold = true;
            this.textBox215.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox215.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox215.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox215.StyleName = "";
            this.textBox215.Value = "Installation Sketch";
            // 
            // textBox210
            // 
            this.textBox210.Name = "textBox210";
            this.textBox210.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.9899959564209D), Telerik.Reporting.Drawing.Unit.Cm(6.4536662101745605D));
            this.textBox210.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox210.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox210.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox210.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox210.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox210.StyleName = "";
            // 
            // textBox205
            // 
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.9899959564209D), Telerik.Reporting.Drawing.Unit.Cm(0.8175361156463623D));
            this.textBox205.Style.BorderColor.Left = System.Drawing.Color.White;
            this.textBox205.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox205.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox205.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox205.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox205.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox205.StyleName = "";
            this.textBox205.Value = "Signed (\"The Customer\", or authorised agent):";
            // 
            // textBox203
            // 
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854793548583984D), Telerik.Reporting.Drawing.Unit.Cm(1.4261646270751953D));
            this.textBox203.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox203.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox203.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.textBox203.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox203.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox203.StyleName = "";
            this.textBox203.Value = "Date________________";
            // 
            // textBox202
            // 
            this.textBox202.Name = "textBox202";
            this.textBox202.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9854793548583984D), Telerik.Reporting.Drawing.Unit.Cm(1.4261646270751953D));
            this.textBox202.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox202.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox202.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox202.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.textBox202.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox202.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox202.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox202.StyleName = "";
            this.textBox202.Value = "Signature________________";
            // 
            // textBox201
            // 
            this.textBox201.Name = "textBox201";
            this.textBox201.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989997863769531D), Telerik.Reporting.Drawing.Unit.Cm(1.0292408466339111D));
            this.textBox201.Style.BorderColor.Left = System.Drawing.Color.White;
            this.textBox201.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox201.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox201.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox201.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox201.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox201.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox201.StyleName = "";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox3,
            this.htmlTextBox7});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(52.765628814697266D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.1000969409942627D));
            this.panel2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.37531298398971558D), Telerik.Reporting.Drawing.Unit.Cm(0.053013559430837631D));
            this.pictureBox3.MimeType = "image/png";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Style.Font.Name = "Open Sans";
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.486427307128906D), Telerik.Reporting.Drawing.Unit.Cm(9.6893309091683477E-05D));
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.htmlTextBox7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox7.Value = resources.GetString("htmlTextBox7.Value");
            // 
            // txtpaymnetfrm
            // 
            this.txtpaymnetfrm.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(84.498123168945312D));
            this.txtpaymnetfrm.Name = "txtpaymnetfrm";
            this.txtpaymnetfrm.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.4234929084777832D), Telerik.Reporting.Drawing.Unit.Cm(1.7937499284744263D));
            this.txtpaymnetfrm.Style.Font.Name = "Arial";
            this.txtpaymnetfrm.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtpaymnetfrm.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtpaymnetfrm.Value = "";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(86.429580688476562D));
            this.pictureBox5.MimeType = "image/png";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20D), Telerik.Reporting.Drawing.Unit.Cm(17.727083206176758D));
            this.pictureBox5.Value = ((object)(resources.GetObject("pictureBox5.Value")));
            // 
            // panel5
            // 
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox9,
            this.pictureBox6});
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(109.91561126708984D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.2116665840148926D));
            this.panel5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // htmlTextBox9
            // 
            this.htmlTextBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.58096981048584D), Telerik.Reporting.Drawing.Unit.Cm(-1.0093052793536117E-07D));
            this.htmlTextBox9.Name = "htmlTextBox9";
            this.htmlTextBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.htmlTextBox9.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox9.Value = resources.GetString("htmlTextBox9.Value");
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.37744545936584473D), Telerik.Reporting.Drawing.Unit.Cm(0.21688325703144074D));
            this.pictureBox6.MimeType = "image/png";
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox6.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox6.Style.Font.Name = "Open Sans";
            this.pictureBox6.Value = ((object)(resources.GetObject("pictureBox6.Value")));
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.009999995119869709D), Telerik.Reporting.Drawing.Unit.Cm(112.54395294189453D));
            this.pictureBox7.MimeType = "image/png";
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(15.901457786560059D));
            this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.pictureBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.pictureBox7.Value = ((object)(resources.GetObject("pictureBox7.Value")));
            // 
            // lblProjectNumber4
            // 
            this.lblProjectNumber4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.0268754959106445D), Telerik.Reporting.Drawing.Unit.Cm(116.35395050048828D));
            this.lblProjectNumber4.Name = "lblProjectNumber4";
            this.lblProjectNumber4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.6982126235961914D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblProjectNumber4.Style.Font.Name = "Arial";
            this.lblProjectNumber4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblProjectNumber4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblProjectNumber4.Value = "";
            // 
            // lblInstallerName7
            // 
            this.lblInstallerName7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.0016183853149414D), Telerik.Reporting.Drawing.Unit.Cm(117.16583251953125D));
            this.lblInstallerName7.Name = "lblInstallerName7";
            this.lblInstallerName7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.883880615234375D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblInstallerName7.Style.Font.Name = "Arial";
            this.lblInstallerName7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblInstallerName7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblInstallerName7.Value = "";
            // 
            // lblFinanceOption
            // 
            this.lblFinanceOption.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.621368408203125D), Telerik.Reporting.Drawing.Unit.Cm(124.70061492919922D));
            this.lblFinanceOption.Name = "lblFinanceOption";
            this.lblFinanceOption.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0226359367370605D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblFinanceOption.Style.Font.Name = "Arial";
            this.lblFinanceOption.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblFinanceOption.Value = "";
            // 
            // lblProjectNumber5
            // 
            this.lblProjectNumber5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.70245361328125D), Telerik.Reporting.Drawing.Unit.Cm(113.05310821533203D));
            this.lblProjectNumber5.Name = "lblProjectNumber5";
            this.lblProjectNumber5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0535123348236084D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblProjectNumber5.Style.Font.Name = "Arial";
            this.lblProjectNumber5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblProjectNumber5.Value = "";
            // 
            // table15
            // 
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.7161273956298828D)));
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2506437301635742D)));
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D)));
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.85573214292526245D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.66580349206924438D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.71928685903549194D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.93321800231933594D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(3.9015390872955322D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(8.6015462875366211D)));
            this.table15.Body.SetCellContent(1, 2, this.textBox208);
            this.table15.Body.SetCellContent(1, 3, this.textBox211);
            this.table15.Body.SetCellContent(1, 1, this.litTitle);
            this.table15.Body.SetCellContent(6, 1, this.textBox216);
            this.table15.Body.SetCellContent(6, 2, this.textBox217);
            this.table15.Body.SetCellContent(6, 3, this.textBox218);
            this.table15.Body.SetCellContent(5, 3, this.textBox222);
            this.table15.Body.SetCellContent(4, 3, this.textBox226);
            this.table15.Body.SetCellContent(3, 3, this.textBox230);
            this.table15.Body.SetCellContent(2, 1, this.textBox232);
            this.table15.Body.SetCellContent(2, 2, this.textBox233);
            this.table15.Body.SetCellContent(2, 3, this.textBox234);
            this.table15.Body.SetCellContent(3, 0, this.textBox227, 2, 1);
            this.table15.Body.SetCellContent(5, 0, this.textBox219, 2, 1);
            this.table15.Body.SetCellContent(7, 0, this.textBox207, 1, 4);
            this.table15.Body.SetCellContent(0, 0, this.textBox212);
            this.table15.Body.SetCellContent(0, 1, this.textBox213);
            this.table15.Body.SetCellContent(0, 2, this.textBox214);
            this.table15.Body.SetCellContent(0, 3, this.textBox223);
            this.table15.Body.SetCellContent(1, 0, this.textBox206, 2, 1);
            this.table15.Body.SetCellContent(5, 1, this.textBox209);
            this.table15.Body.SetCellContent(5, 2, this.textBox220);
            this.table15.Body.SetCellContent(3, 1, this.textBox204, 1, 2);
            this.table15.Body.SetCellContent(4, 1, this.textBox224, 1, 2);
            this.table15.Body.SetCellContent(8, 0, this.textBox225, 1, 4);
            this.table15.Body.SetCellContent(9, 0, this.htmlTextBox11, 1, 4);
            tableGroup159.Name = "tableGroup38";
            tableGroup160.Name = "group106";
            tableGroup161.Name = "tableGroup39";
            tableGroup162.Name = "tableGroup40";
            this.table15.ColumnGroups.Add(tableGroup159);
            this.table15.ColumnGroups.Add(tableGroup160);
            this.table15.ColumnGroups.Add(tableGroup161);
            this.table15.ColumnGroups.Add(tableGroup162);
            this.table15.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox212,
            this.textBox213,
            this.textBox214,
            this.textBox223,
            this.textBox206,
            this.litTitle,
            this.textBox208,
            this.textBox211,
            this.textBox232,
            this.textBox233,
            this.textBox234,
            this.textBox227,
            this.textBox204,
            this.textBox230,
            this.textBox224,
            this.textBox226,
            this.textBox219,
            this.textBox209,
            this.textBox220,
            this.textBox222,
            this.textBox216,
            this.textBox217,
            this.textBox218,
            this.textBox207,
            this.textBox225,
            this.htmlTextBox11});
            this.table15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(144.42208862304688D));
            this.table15.Name = "table15";
            tableGroup164.Name = "group114";
            tableGroup165.Name = "group107";
            tableGroup166.Name = "group113";
            tableGroup167.Name = "group112";
            tableGroup168.Name = "group111";
            tableGroup169.Name = "group110";
            tableGroup170.Name = "group109";
            tableGroup171.Name = "group108";
            tableGroup172.Name = "group115";
            tableGroup173.Name = "group116";
            tableGroup163.ChildGroups.Add(tableGroup164);
            tableGroup163.ChildGroups.Add(tableGroup165);
            tableGroup163.ChildGroups.Add(tableGroup166);
            tableGroup163.ChildGroups.Add(tableGroup167);
            tableGroup163.ChildGroups.Add(tableGroup168);
            tableGroup163.ChildGroups.Add(tableGroup169);
            tableGroup163.ChildGroups.Add(tableGroup170);
            tableGroup163.ChildGroups.Add(tableGroup171);
            tableGroup163.ChildGroups.Add(tableGroup172);
            tableGroup163.ChildGroups.Add(tableGroup173);
            tableGroup163.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup163.Name = "detailTableGroup14";
            this.table15.RowGroups.Add(tableGroup163);
            this.table15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20D), Telerik.Reporting.Drawing.Unit.Cm(17.698541641235352D));
            this.table15.Style.Font.Name = "Arial";
            this.table15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.table15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // textBox208
            // 
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.85573208332061768D));
            this.textBox208.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox208.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox208.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D);
            this.textBox208.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox208.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox208.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox208.Value = "=Fields.ContFirst";
            // 
            // textBox211
            // 
            this.textBox211.Name = "textBox211";
            this.textBox211.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.85573208332061768D));
            this.textBox211.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox211.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox211.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D);
            this.textBox211.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox211.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox211.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox211.Value = "=Fields.ContLast";
            // 
            // litTitle
            // 
            this.litTitle.Name = "litTitle";
            this.litTitle.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2506442070007324D), Telerik.Reporting.Drawing.Unit.Cm(0.85573208332061768D));
            this.litTitle.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.litTitle.Style.Font.Name = "Microsoft Sans Serif";
            this.litTitle.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D);
            this.litTitle.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.litTitle.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.litTitle.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.litTitle.StyleName = "";
            this.litTitle.Value = "=Fields.ContMr";
            // 
            // textBox216
            // 
            this.textBox216.Name = "textBox216";
            this.textBox216.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2506442070007324D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox216.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox216.StyleName = "";
            this.textBox216.Value = "Suburb/Town ";
            // 
            // textBox217
            // 
            this.textBox217.Name = "textBox217";
            this.textBox217.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox217.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox217.StyleName = "";
            this.textBox217.Value = "Postcode ";
            // 
            // textBox218
            // 
            this.textBox218.Name = "textBox218";
            this.textBox218.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox218.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox218.StyleName = "";
            // 
            // textBox222
            // 
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox222.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox222.StyleName = "";
            // 
            // textBox226
            // 
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.933218240737915D));
            this.textBox226.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox226.StyleName = "";
            // 
            // textBox230
            // 
            this.textBox230.Name = "textBox230";
            this.textBox230.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.71928638219833374D));
            this.textBox230.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox230.StyleName = "";
            // 
            // textBox232
            // 
            this.textBox232.Name = "textBox232";
            this.textBox232.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2506442070007324D), Telerik.Reporting.Drawing.Unit.Cm(0.66580319404602051D));
            this.textBox232.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox232.StyleName = "";
            this.textBox232.Value = "Title";
            // 
            // textBox233
            // 
            this.textBox233.Name = "textBox233";
            this.textBox233.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.66580319404602051D));
            this.textBox233.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox233.StyleName = "";
            this.textBox233.Value = "Given name/s";
            // 
            // textBox234
            // 
            this.textBox234.Name = "textBox234";
            this.textBox234.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.66580319404602051D));
            this.textBox234.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox234.StyleName = "";
            this.textBox234.Value = "Surname";
            // 
            // textBox227
            // 
            this.textBox227.Name = "textBox227";
            this.textBox227.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.7161269187927246D), Telerik.Reporting.Drawing.Unit.Cm(1.652504563331604D));
            this.textBox227.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox227.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox227.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox227.StyleName = "";
            this.textBox227.Value = "*Address";
            // 
            // textBox219
            // 
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.7161269187927246D), Telerik.Reporting.Drawing.Unit.Cm(1.0107073783874512D));
            this.textBox219.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox219.StyleName = "";
            // 
            // textBox207
            // 
            this.textBox207.Name = "textBox207";
            this.textBox207.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox207.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox207.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox207.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox207.StyleName = "";
            this.textBox207.Value = "* Electrical installation/equipment tested (please include site address for elect" +
    "rical installation work if different from above):";
            // 
            // textBox212
            // 
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.7161269187927246D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox212.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox212.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox212.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox212.StyleName = "";
            this.textBox212.Value = "* Work performed for:";
            // 
            // textBox213
            // 
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2506442070007324D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox213.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox213.StyleName = "";
            // 
            // textBox214
            // 
            this.textBox214.Name = "textBox214";
            this.textBox214.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox214.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox214.StyleName = "";
            // 
            // textBox223
            // 
            this.textBox223.Name = "textBox223";
            this.textBox223.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox223.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox223.StyleName = "";
            // 
            // textBox206
            // 
            this.textBox206.Name = "textBox206";
            this.textBox206.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.7161269187927246D), Telerik.Reporting.Drawing.Unit.Cm(1.5215352773666382D));
            this.textBox206.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox206.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox206.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox206.Value = "*Name";
            // 
            // textBox209
            // 
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2506442070007324D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox209.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox209.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox209.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D);
            this.textBox209.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox209.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox209.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox209.StyleName = "";
            this.textBox209.Value = "=Fields.StreetCity";
            // 
            // textBox220
            // 
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5166144371032715D), Telerik.Reporting.Drawing.Unit.Cm(0.50535368919372559D));
            this.textBox220.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox220.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox220.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D);
            this.textBox220.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox220.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox220.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox220.StyleName = "";
            this.textBox220.Value = "=Fields.StreetPostCode";
            // 
            // textBox204
            // 
            this.textBox204.Name = "textBox204";
            this.textBox204.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.7672586441040039D), Telerik.Reporting.Drawing.Unit.Cm(0.71928638219833374D));
            this.textBox204.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox204.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox204.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D);
            this.textBox204.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox204.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox204.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox204.StyleName = "";
            this.textBox204.Value = "=Fields.StreetAddress";
            // 
            // textBox224
            // 
            this.textBox224.Name = "textBox224";
            this.textBox224.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.7672586441040039D), Telerik.Reporting.Drawing.Unit.Cm(0.933218240737915D));
            this.textBox224.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox224.StyleName = "";
            this.textBox224.Value = "Street ";
            // 
            // textBox225
            // 
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20D), Telerik.Reporting.Drawing.Unit.Cm(3.9015398025512695D));
            this.textBox225.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox225.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox225.StyleName = "";
            // 
            // htmlTextBox11
            // 
            this.htmlTextBox11.Name = "htmlTextBox11";
            this.htmlTextBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20D), Telerik.Reporting.Drawing.Unit.Cm(8.6015462875366211D));
            this.htmlTextBox11.Style.Font.Name = "Microsoft Sans Serif";
            this.htmlTextBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox11.StyleName = "";
            this.htmlTextBox11.Value = resources.GetString("htmlTextBox11.Value");
            // 
            // panel6
            // 
            this.panel6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox8,
            this.htmlTextBox10});
            this.panel6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(137.34771728515625D));
            this.panel6.Name = "panel6";
            this.panel6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.0999999046325684D));
            this.panel6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.37644058465957642D), Telerik.Reporting.Drawing.Unit.Cm(0.21616251766681671D));
            this.pictureBox8.MimeType = "image/png";
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox8.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox8.Style.Font.Name = "Open Sans";
            this.pictureBox8.Value = ((object)(resources.GetObject("pictureBox8.Value")));
            // 
            // htmlTextBox10
            // 
            this.htmlTextBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.58096981048584D), Telerik.Reporting.Drawing.Unit.Cm(0.00010335286060580984D));
            this.htmlTextBox10.Name = "htmlTextBox10";
            this.htmlTextBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.htmlTextBox10.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox10.Value = resources.GetString("htmlTextBox10.Value");
            // 
            // pictureBox9
            // 
            this.pictureBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.021941490471363068D), Telerik.Reporting.Drawing.Unit.Cm(139.55377197265625D));
            this.pictureBox9.MimeType = "image/png";
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.734025955200195D), Telerik.Reporting.Drawing.Unit.Cm(4.8499999046325684D));
            this.pictureBox9.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox9.Value = ((object)(resources.GetObject("pictureBox9.Value")));
            // 
            // textBox129
            // 
            this.textBox129.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(24.444160461425781D));
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20D), Telerik.Reporting.Drawing.Unit.Cm(0.20687906444072723D));
            this.textBox129.Style.Font.Name = "Arial";
            this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox129.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox129.Value = "STC Paper Approved on 28 August 2014.";
            // 
            // lblInverterModel2
            // 
            this.lblInverterModel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.87075138092041D), Telerik.Reporting.Drawing.Unit.Cm(62.014114379882812D));
            this.lblInverterModel2.Name = "lblInverterModel2";
            this.lblInverterModel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.81871509552002D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.lblInverterModel2.Style.Font.Bold = true;
            this.lblInverterModel2.Style.Font.Name = "Arial";
            this.lblInverterModel2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblInverterModel2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblInverterModel2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblInverterModel2.Value = "";
            // 
            // lblInverterBrand2
            // 
            this.lblInverterBrand2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.87075138092041D), Telerik.Reporting.Drawing.Unit.Cm(61.135154724121094D));
            this.lblInverterBrand2.Name = "lblInverterBrand2";
            this.lblInverterBrand2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.115382194519043D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.lblInverterBrand2.Style.Font.Bold = true;
            this.lblInverterBrand2.Style.Font.Name = "Arial";
            this.lblInverterBrand2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblInverterBrand2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblInverterBrand2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblInverterBrand2.Value = "";
            // 
            // lblInverterSeries2
            // 
            this.lblInverterSeries2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.87075138092041D), Telerik.Reporting.Drawing.Unit.Cm(62.854946136474609D));
            this.lblInverterSeries2.Name = "lblInverterSeries2";
            this.lblInverterSeries2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.52568244934082D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.lblInverterSeries2.Style.Font.Bold = true;
            this.lblInverterSeries2.Style.Font.Name = "Arial";
            this.lblInverterSeries2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblInverterSeries2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblInverterSeries2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblInverterSeries2.Value = "";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(55.109779357910156D));
            this.pictureBox10.MimeType = "image/png";
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(23.309791564941406D));
            this.pictureBox10.Value = ((object)(resources.GetObject("pictureBox10.Value")));
            // 
            // ProjectNumber3
            // 
            this.ProjectNumber3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.92235803604126D), Telerik.Reporting.Drawing.Unit.Cm(55.189155578613281D));
            this.ProjectNumber3.Name = "ProjectNumber3";
            this.ProjectNumber3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0444128513336182D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.ProjectNumber3.Style.Font.Bold = true;
            this.ProjectNumber3.Style.Font.Name = "Arial";
            this.ProjectNumber3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.ProjectNumber3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.ProjectNumber3.Value = "=Fields.ProjectNumber";
            // 
            // litPanelDetails
            // 
            this.litPanelDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.29706555604934692D), Telerik.Reporting.Drawing.Unit.Cm(55.815814971923828D));
            this.litPanelDetails.Name = "litPanelDetails";
            this.litPanelDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9855208396911621D), Telerik.Reporting.Drawing.Unit.Cm(0.49416670203208923D));
            this.litPanelDetails.Style.Font.Bold = true;
            this.litPanelDetails.Style.Font.Name = "Arial";
            this.litPanelDetails.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.litPanelDetails.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.litPanelDetails.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.litPanelDetails.Value = "=Fields.PanelDetails";
            // 
            // lblMeterPhase
            // 
            this.lblMeterPhase.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.04173755645752D), Telerik.Reporting.Drawing.Unit.Cm(65.60687255859375D));
            this.lblMeterPhase.Name = "lblMeterPhase";
            this.lblMeterPhase.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblMeterPhase.Style.Font.Bold = true;
            this.lblMeterPhase.Style.Font.Name = "Arial";
            this.lblMeterPhase.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.lblMeterPhase.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblMeterPhase.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblMeterPhase.Value = "";
            // 
            // lblOffPeak
            // 
            this.lblOffPeak.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.004517555236816D), Telerik.Reporting.Drawing.Unit.Cm(66.447708129882812D));
            this.lblOffPeak.Name = "lblOffPeak";
            this.lblOffPeak.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblOffPeak.Style.Font.Bold = true;
            this.lblOffPeak.Style.Font.Name = "Arial";
            this.lblOffPeak.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.lblOffPeak.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblOffPeak.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblOffPeak.Value = "";
            // 
            // lblRetailer
            // 
            this.lblRetailer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.469951629638672D), Telerik.Reporting.Drawing.Unit.Cm(67.233108520507812D));
            this.lblRetailer.Name = "lblRetailer";
            this.lblRetailer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblRetailer.Style.Font.Bold = true;
            this.lblRetailer.Style.Font.Name = "Arial";
            this.lblRetailer.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.lblRetailer.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblRetailer.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblRetailer.Value = "";
            // 
            // lblPeakMeterNo
            // 
            this.lblPeakMeterNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.004517555236816D), Telerik.Reporting.Drawing.Unit.Cm(69.793731689453125D));
            this.lblPeakMeterNo.Name = "lblPeakMeterNo";
            this.lblPeakMeterNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D));
            this.lblPeakMeterNo.Style.Font.Bold = true;
            this.lblPeakMeterNo.Style.Font.Name = "Arial";
            this.lblPeakMeterNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.lblPeakMeterNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblPeakMeterNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblPeakMeterNo.Value = "";
            // 
            // textBox221
            // 
            this.textBox221.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1775276660919189D), Telerik.Reporting.Drawing.Unit.Inch(45.214893341064453D));
            this.textBox221.Name = "textBox221";
            this.textBox221.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5591205358505249D), Telerik.Reporting.Drawing.Unit.Inch(0.18413715064525604D));
            this.textBox221.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox221.Style.Font.Bold = false;
            this.textBox221.Style.Font.Name = "Arial Rounded MT Bold";
            this.textBox221.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox221.Value = "ARISE SOLAR";
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox4,
            this.htmlTextBox8});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.0061542424373329E-05D), Telerik.Reporting.Drawing.Unit.Cm(82.174774169921875D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.1000969409942627D));
            this.panel3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.37531298398971558D), Telerik.Reporting.Drawing.Unit.Cm(0.053013559430837631D));
            this.pictureBox4.MimeType = "image/png";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox4.Style.Font.Name = "Open Sans";
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // htmlTextBox8
            // 
            this.htmlTextBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4222412109375D), Telerik.Reporting.Drawing.Unit.Cm(4.8345722461817786E-05D));
            this.htmlTextBox8.Name = "htmlTextBox8";
            this.htmlTextBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.htmlTextBox8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox8.Value = resources.GetString("htmlTextBox8.Value");
            // 
            // reportHeaderSection1
            // 
            this.reportHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D);
            this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.htmlTextBox1,
            this.pictureBox2});
            this.reportHeaderSection1.Name = "reportHeaderSection1";
            this.reportHeaderSection1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.reportHeaderSection1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.reportHeaderSection1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.reportHeaderSection1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.reportHeaderSection1.Style.Font.Name = "Arial";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.8923616409301758D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Arial";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "STC Assignment Form\r\nArise Solar\r\n";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.426736831665039D), Telerik.Reporting.Drawing.Unit.Cm(5.0465263967680585E-08D));
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.htmlTextBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox1.Value = resources.GetString("htmlTextBox1.Value");
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.383556604385376D), Telerik.Reporting.Drawing.Unit.Cm(0.13669182360172272D));
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Style.Font.Name = "Open Sans";
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // textBox228
            // 
            this.textBox228.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.1320090293884277D), Telerik.Reporting.Drawing.Unit.Inch(50.308643341064453D));
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72507065534591675D), Telerik.Reporting.Drawing.Unit.Inch(0.19455380737781525D));
            this.textBox228.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox228.Style.Font.Bold = false;
            this.textBox228.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox228.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox228.Value = "Arise Solar";
            // 
            // STCForm
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtdate,
            this.reportHeaderSection1});
            this.Name = "STCForm";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.Style.Font.Name = "open sp";
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20.009998321533203D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection txtdate;
        private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox ContFirst;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox ContLast;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox lblInstallBookingDate;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.CheckBox chkBuilding;
        private Telerik.Reporting.CheckBox chkGround;
        private Telerik.Reporting.CheckBox chkResidential;
        private Telerik.Reporting.CheckBox chkschool;
        private Telerik.Reporting.CheckBox chkCommercial;
        private Telerik.Reporting.CheckBox checkBox6;
        private Telerik.Reporting.CheckBox checkBox7;
        private Telerik.Reporting.CheckBox checkBox8;
        private Telerik.Reporting.CheckBox checkBox9;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.CheckBox checkBox10;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.CheckBox checkBox11;
        private Telerik.Reporting.CheckBox checkBox12;
        private Telerik.Reporting.CheckBox checkBox13;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.CheckBox checkBox14;
        private Telerik.Reporting.CheckBox checkBox15;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox123;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.CheckBox checkBox1;
        private Telerik.Reporting.CheckBox checkBox2;
        private Telerik.Reporting.CheckBox checkBox3;
        private Telerik.Reporting.CheckBox checkBox4;
        private Telerik.Reporting.CheckBox checkBox5;
        private Telerik.Reporting.CheckBox checkBox16;
        private Telerik.Reporting.CheckBox checkBox17;
        private Telerik.Reporting.CheckBox checkBox18;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.TextBox textBox158;
        private Telerik.Reporting.TextBox textBox173;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.TextBox textBox159;
        private Telerik.Reporting.TextBox textBox162;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.TextBox textBox169;
        private Telerik.Reporting.TextBox textBox171;
        private Telerik.Reporting.TextBox textBox164;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.TextBox textBox138;
        private Telerik.Reporting.TextBox textBox147;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.Table table12;
        private Telerik.Reporting.TextBox textBox148;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox161;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.TextBox textBox166;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
        private Telerik.Reporting.Table table13;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox172;
        private Telerik.Reporting.TextBox textBox175;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.Table table14;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.TextBox textBox177;
        private Telerik.Reporting.TextBox textBox179;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox178;
        private Telerik.Reporting.TextBox textBox180;
        private Telerik.Reporting.TextBox textBox181;
        private Telerik.Reporting.TextBox textBox182;
        private Telerik.Reporting.TextBox textBox183;
        private Telerik.Reporting.TextBox textBox184;
        private Telerik.Reporting.TextBox textBox185;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox187;
        private Telerik.Reporting.TextBox textBox188;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox193;
        private Telerik.Reporting.TextBox textBox194;
        private Telerik.Reporting.TextBox textBox195;
        private Telerik.Reporting.TextBox textBox196;
        private Telerik.Reporting.TextBox textBox197;
        private Telerik.Reporting.TextBox textBox198;
        private Telerik.Reporting.TextBox textBox199;
        private Telerik.Reporting.TextBox textBox170;
        private Telerik.Reporting.TextBox textBox200;
        private Telerik.Reporting.TextBox textBox215;
        private Telerik.Reporting.TextBox textBox210;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox202;
        private Telerik.Reporting.TextBox textBox201;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.HtmlTextBox txtpaymnetfrm;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.Panel panel5;
        private Telerik.Reporting.HtmlTextBox htmlTextBox9;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.TextBox lblProjectNumber4;
        private Telerik.Reporting.TextBox lblInstallerName7;
        private Telerik.Reporting.TextBox lblFinanceOption;
        private Telerik.Reporting.TextBox lblProjectNumber5;
        private Telerik.Reporting.Table table15;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox211;
        private Telerik.Reporting.TextBox litTitle;
        private Telerik.Reporting.TextBox textBox216;
        private Telerik.Reporting.TextBox textBox217;
        private Telerik.Reporting.TextBox textBox218;
        private Telerik.Reporting.TextBox textBox222;
        private Telerik.Reporting.TextBox textBox224;
        private Telerik.Reporting.TextBox textBox226;
        private Telerik.Reporting.TextBox textBox230;
        private Telerik.Reporting.TextBox textBox232;
        private Telerik.Reporting.TextBox textBox233;
        private Telerik.Reporting.TextBox textBox234;
        private Telerik.Reporting.TextBox textBox206;
        private Telerik.Reporting.TextBox textBox227;
        private Telerik.Reporting.TextBox textBox219;
        private Telerik.Reporting.TextBox textBox207;
        private Telerik.Reporting.Panel panel6;
        private Telerik.Reporting.PictureBox pictureBox9;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.TextBox textBox213;
        private Telerik.Reporting.TextBox textBox214;
        private Telerik.Reporting.TextBox textBox223;
        private Telerik.Reporting.TextBox textBox204;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox220;
        private Telerik.Reporting.TextBox textBox225;
        private Telerik.Reporting.HtmlTextBox htmlTextBox11;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox lblInverterModel2;
        private Telerik.Reporting.TextBox lblInverterBrand2;
        private Telerik.Reporting.TextBox lblInverterSeries2;
        private Telerik.Reporting.PictureBox pictureBox10;
        private Telerik.Reporting.TextBox ProjectNumber3;
        private Telerik.Reporting.TextBox litPanelDetails;
        private Telerik.Reporting.TextBox lblMeterPhase;
        private Telerik.Reporting.TextBox lblOffPeak;
        private Telerik.Reporting.TextBox lblRetailer;
        private Telerik.Reporting.TextBox lblPeakMeterNo;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox8;
        private Telerik.Reporting.TextBox textBox221;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.HtmlTextBox htmlTextBox10;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.HtmlTextBox htmlTextBox8;
        private Telerik.Reporting.TextBox textBox228;
    }
}