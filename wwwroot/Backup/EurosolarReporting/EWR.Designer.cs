using System;
namespace EurosolarReporting
{
    partial class EWR
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EWR));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.txtcustname = new Telerik.Reporting.TextBox();
            this.txtmobile = new Telerik.Reporting.TextBox();
            this.txtstreetno = new Telerik.Reporting.TextBox();
            this.txtaddress = new Telerik.Reporting.TextBox();
            this.txtsuburb = new Telerik.Reporting.TextBox();
            this.txtday = new Telerik.Reporting.TextBox();
            this.txtyear = new Telerik.Reporting.TextBox();
            this.txtmonth = new Telerik.Reporting.TextBox();
            this.txtyear2 = new Telerik.Reporting.TextBox();
            this.txtmonth2 = new Telerik.Reporting.TextBox();
            this.txtday2 = new Telerik.Reporting.TextBox();
            this.txtinverter = new Telerik.Reporting.TextBox();
            this.txtsolarpanel = new Telerik.Reporting.TextBox();
            this.txtinstaller = new Telerik.Reporting.TextBox();
            this.txtaddress2 = new Telerik.Reporting.TextBox();
            this.txtcust2 = new Telerik.Reporting.TextBox();
            this.txtmobile2 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.txt_elec_licence = new Telerik.Reporting.TextBox();
            this.txtprojectno = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297.00003051757812D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.txtcustname,
            this.txtmobile,
            this.txtstreetno,
            this.txtaddress,
            this.txtsuburb,
            this.txtday,
            this.txtyear,
            this.txtmonth,
            this.txtyear2,
            this.txtmonth2,
            this.txtday2,
            this.txtinverter,
            this.txtsolarpanel,
            this.txtinstaller,
            this.txtaddress2,
            this.txtcust2,
            this.txtmobile2,
            this.textBox18,
            this.txt_elec_licence,
            this.txtprojectno});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "Calibri";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.pictureBox1.Style.Font.Name = "Times New Roman";
            this.pictureBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.pictureBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // txtcustname
            // 
            this.txtcustname.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.5D), Telerik.Reporting.Drawing.Unit.Cm(6.4499998092651367D));
            this.txtcustname.Name = "txtcustname";
            this.txtcustname.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.7000007629394531D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.txtcustname.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtcustname.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtcustname.Value = "";
            // 
            // txtmobile
            // 
            this.txtmobile.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.100000381469727D), Telerik.Reporting.Drawing.Unit.Cm(6.4499998092651367D));
            this.txtmobile.Name = "txtmobile";
            this.txtmobile.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.txtmobile.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtmobile.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtmobile.Value = "";
            // 
            // txtstreetno
            // 
            this.txtstreetno.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.4999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(8.1499996185302734D));
            this.txtstreetno.Name = "txtstreetno";
            this.txtstreetno.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6000000238418579D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtstreetno.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtstreetno.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtstreetno.Value = "";
            // 
            // txtaddress
            // 
            this.txtaddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(8.1499996185302734D));
            this.txtaddress.Name = "txtaddress";
            this.txtaddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.90000057220459D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtaddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtaddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtaddress.Value = "";
            // 
            // txtsuburb
            // 
            this.txtsuburb.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(9.25D));
            this.txtsuburb.Name = "txtsuburb";
            this.txtsuburb.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.8999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.txtsuburb.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtsuburb.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtsuburb.Value = "";
            // 
            // txtday
            // 
            this.txtday.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(24.80000114440918D));
            this.txtday.Name = "txtday";
            this.txtday.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtday.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtday.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtday.Value = "";
            // 
            // txtyear
            // 
            this.txtyear.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(24.780000686645508D));
            this.txtyear.Name = "txtyear";
            this.txtyear.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0000003576278687D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtyear.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtyear.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtyear.Value = "";
            // 
            // txtmonth
            // 
            this.txtmonth.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.5D), Telerik.Reporting.Drawing.Unit.Cm(24.80000114440918D));
            this.txtmonth.Name = "txtmonth";
            this.txtmonth.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtmonth.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtmonth.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtmonth.Value = "";
            // 
            // txtyear2
            // 
            this.txtyear2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(27.19999885559082D));
            this.txtyear2.Name = "txtyear2";
            this.txtyear2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtyear2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtyear2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtyear2.Value = "";
            // 
            // txtmonth2
            // 
            this.txtmonth2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(27.19999885559082D));
            this.txtmonth2.Name = "txtmonth2";
            this.txtmonth2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtmonth2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtmonth2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtmonth2.Value = "";
            // 
            // txtday2
            // 
            this.txtday2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(27.19999885559082D));
            this.txtday2.Name = "txtday2";
            this.txtday2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtday2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtday2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtday2.Value = "";
            // 
            // txtinverter
            // 
            this.txtinverter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.6000008583068848D), Telerik.Reporting.Drawing.Unit.Cm(16.549999237060547D));
            this.txtinverter.Name = "txtinverter";
            this.txtinverter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0999990701675415D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtinverter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtinverter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtinverter.Value = "";
            // 
            // txtsolarpanel
            // 
            this.txtsolarpanel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.6000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(16.149999618530273D));
            this.txtsolarpanel.Name = "txtsolarpanel";
            this.txtsolarpanel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtsolarpanel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtsolarpanel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtsolarpanel.Value = "";
            // 
            // txtinstaller
            // 
            this.txtinstaller.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(25.25D));
            this.txtinstaller.Name = "txtinstaller";
            this.txtinstaller.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3999981880187988D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtinstaller.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtinstaller.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtinstaller.Value = "";
            // 
            // txtaddress2
            // 
            this.txtaddress2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D), Telerik.Reporting.Drawing.Unit.Cm(25.700000762939453D));
            this.txtaddress2.Name = "txtaddress2";
            this.txtaddress2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7999992370605469D), Telerik.Reporting.Drawing.Unit.Cm(0.39999818801879883D));
            this.txtaddress2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtaddress2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtaddress2.Value = "";
            // 
            // txtcust2
            // 
            this.txtcust2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(26.350000381469727D));
            this.txtcust2.Name = "txtcust2";
            this.txtcust2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.4000014066696167D));
            this.txtcust2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtcust2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtcust2.Value = "";
            // 
            // txtmobile2
            // 
            this.txtmobile2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.700000762939453D), Telerik.Reporting.Drawing.Unit.Cm(26.19999885559082D));
            this.txtmobile2.Name = "txtmobile2";
            this.txtmobile2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.4000014066696167D));
            this.txtmobile2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtmobile2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtmobile2.Value = "";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.700000762939453D), Telerik.Reporting.Drawing.Unit.Cm(25.700000762939453D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999984264373779D), Telerik.Reporting.Drawing.Unit.Cm(0.4000014066696167D));
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox18.Value = "";
            // 
            // txt_elec_licence
            // 
            this.txt_elec_licence.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(25.25D));
            this.txt_elec_licence.Name = "txt_elec_licence";
            this.txt_elec_licence.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.4000014066696167D));
            this.txt_elec_licence.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txt_elec_licence.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txt_elec_licence.Value = "";
            // 
            // txtprojectno
            // 
            this.txtprojectno.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(9.8000001907348633D));
            this.txtprojectno.Name = "txtprojectno";
            this.txtprojectno.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.txtprojectno.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtprojectno.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtprojectno.Value = "";
            // 
            // EWR
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "EWR";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox txtcustname;
        private Telerik.Reporting.TextBox txtmobile;
        private Telerik.Reporting.TextBox txtstreetno;
        private Telerik.Reporting.TextBox txtaddress;
        private Telerik.Reporting.TextBox txtsuburb;
        private Telerik.Reporting.TextBox txtday;
        private Telerik.Reporting.TextBox txtyear;
        private Telerik.Reporting.TextBox txtmonth;
        private Telerik.Reporting.TextBox txtyear2;
        private Telerik.Reporting.TextBox txtmonth2;
        private Telerik.Reporting.TextBox txtday2;
        private Telerik.Reporting.TextBox txtinverter;
        private Telerik.Reporting.TextBox txtsolarpanel;
        private Telerik.Reporting.TextBox txtinstaller;
        private Telerik.Reporting.TextBox txtaddress2;
        private Telerik.Reporting.TextBox txtcust2;
        private Telerik.Reporting.TextBox txtmobile2;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox txt_elec_licence;
        private Telerik.Reporting.TextBox txtprojectno;
    }
}