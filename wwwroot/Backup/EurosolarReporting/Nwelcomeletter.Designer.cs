namespace EurosolarReporting
{
    partial class Nwelcomeletter
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nwelcomeletter));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.txtbasiccost = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.txtmeter = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.txtswitch = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.txtmeterphase = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.txtInverterdetail1 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.txtpaymentplan = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox214 = new Telerik.Reporting.TextBox();
            this.txtotherdetail = new Telerik.Reporting.TextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.txtreq = new Telerik.Reporting.TextBox();
            this.txtcompl = new Telerik.Reporting.TextBox();
            this.txtextra = new Telerik.Reporting.TextBox();
            this.txtfullretail = new Telerik.Reporting.TextBox();
            this.txtdiscount2 = new Telerik.Reporting.TextBox();
            this.txtstc = new Telerik.Reporting.TextBox();
            this.txtgst = new Telerik.Reporting.TextBox();
            this.txtdeposit = new Telerik.Reporting.TextBox();
            this.txtlessdeposit = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox8 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox9 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox10 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox11 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox12 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox13 = new Telerik.Reporting.HtmlTextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0799994468688965D), Telerik.Reporting.Drawing.Unit.Cm(0.60854166746139526D));
            this.textBox10.Style.Font.Bold = false;
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "=TrimStart(Fields.ProjectNotes) ";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.55000007152557373D));
            this.textBox70.Style.Color = System.Drawing.Color.White;
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox70.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox70.Value = "System & Installation";
            // 
            // txtbasiccost
            // 
            this.txtbasiccost.Name = "txtbasiccost";
            this.txtbasiccost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.55000007152557373D));
            this.txtbasiccost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtbasiccost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtbasiccost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtbasiccost.Value = "=Fields.lblbasiccost";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(76.000007629394531D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox1,
            this.htmlTextBox2,
            this.htmlTextBox3,
            this.htmlTextBox4,
            this.htmlTextBox5,
            this.htmlTextBox6,
            this.table7,
            this.table3,
            this.table5,
            this.table6,
            this.table8,
            this.table11,
            this.table9,
            this.table10,
            this.htmlTextBox7,
            this.htmlTextBox8,
            this.htmlTextBox9,
            this.htmlTextBox10,
            this.htmlTextBox11,
            this.htmlTextBox12,
            this.htmlTextBox13});
            this.detail.Name = "detail";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9000002145767212D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
            this.htmlTextBox1.Value = "<b><span style=\"font-size: 18px; font-family: raleway-bold\"><span style=\"font-siz" +
    "e: 18px; font-family: raleway-bold\"><p>Customer</p></span></span></b>";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999997854232788D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
            this.htmlTextBox2.Value = "<b><span style=\"font-size: 18px; font-family: raleway-bold\"><span style=\"font-siz" +
    "e: 18px; font-family: raleway-bold\"><p>Sales</p></span></span></b>";
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.90000009536743164D));
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
            this.htmlTextBox3.Value = resources.GetString("htmlTextBox3.Value");
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.90000009536743164D));
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(1.5000001192092896D));
            this.htmlTextBox4.Value = resources.GetString("htmlTextBox4.Value");
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(10.700000762939453D));
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(2.0999996662139893D));
            this.htmlTextBox5.Value = resources.GetString("htmlTextBox5.Value");
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(13.40000057220459D));
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(2.6000010967254639D));
            this.htmlTextBox6.Value = resources.GetString("htmlTextBox6.Value");
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1353828907012939D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7226181030273438D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.8731149435043335D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.94960647821426392D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.302100419998169D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.65874958038330078D)));
            this.table7.Body.SetCellContent(0, 1, this.textBox6);
            this.table7.Body.SetCellContent(1, 1, this.textBox7);
            this.table7.Body.SetCellContent(2, 1, this.txtmeter);
            this.table7.Body.SetCellContent(0, 0, this.textBox38);
            this.table7.Body.SetCellContent(1, 0, this.textBox39);
            this.table7.Body.SetCellContent(2, 0, this.textBox40);
            this.table7.Body.SetCellContent(3, 0, this.textBox41);
            this.table7.Body.SetCellContent(3, 1, this.txtswitch);
            tableGroup1.Name = "group16";
            tableGroup2.Name = "tableGroup";
            this.table7.ColumnGroups.Add(tableGroup1);
            this.table7.ColumnGroups.Add(tableGroup2);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox38,
            this.textBox6,
            this.textBox39,
            this.textBox7,
            this.textBox40,
            this.txtmeter,
            this.textBox41,
            this.txtswitch});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.25399994850158691D), Telerik.Reporting.Drawing.Unit.Cm(85.291099548339844D));
            this.table7.Name = "table7";
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup";
            tableGroup4.Name = "group";
            tableGroup5.Name = "group1";
            tableGroup6.Name = "group82";
            this.table7.RowGroups.Add(tableGroup3);
            this.table7.RowGroups.Add(tableGroup4);
            this.table7.RowGroups.Add(tableGroup5);
            this.table7.RowGroups.Add(tableGroup6);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8580007553100586D), Telerik.Reporting.Drawing.Unit.Cm(3.7835714817047119D));
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7226178646087646D), Telerik.Reporting.Drawing.Unit.Cm(0.8731149435043335D));
            this.textBox6.Style.Color = System.Drawing.Color.White;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "=Fields.ElecRetailer";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7226178646087646D), Telerik.Reporting.Drawing.Unit.Cm(0.94960647821426392D));
            this.textBox7.Style.Color = System.Drawing.Color.White;
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "=Fields.ElecDistributor";
            // 
            // txtmeter
            // 
            this.txtmeter.Name = "txtmeter";
            this.txtmeter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7226178646087646D), Telerik.Reporting.Drawing.Unit.Cm(1.302100419998169D));
            this.txtmeter.Style.Color = System.Drawing.Color.White;
            this.txtmeter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtmeter.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtmeter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtmeter.StyleName = "";
            this.txtmeter.Value = "";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1353826522827148D), Telerik.Reporting.Drawing.Unit.Cm(0.8731149435043335D));
            this.textBox38.Style.Color = System.Drawing.Color.White;
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox38.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "ELEC. RETAILER";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1353826522827148D), Telerik.Reporting.Drawing.Unit.Cm(0.94960647821426392D));
            this.textBox39.Style.Color = System.Drawing.Color.White;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            this.textBox39.Value = "ELEC. DISTRIBUTOR";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1353826522827148D), Telerik.Reporting.Drawing.Unit.Cm(1.302100419998169D));
            this.textBox40.Style.Color = System.Drawing.Color.White;
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.StyleName = "";
            this.textBox40.Value = "COMPLIANCE & SAFETY";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1353826522827148D), Telerik.Reporting.Drawing.Unit.Cm(0.65874958038330078D));
            this.textBox41.Style.Color = System.Drawing.Color.White;
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            // 
            // txtswitch
            // 
            this.txtswitch.Name = "txtswitch";
            this.txtswitch.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7226178646087646D), Telerik.Reporting.Drawing.Unit.Cm(0.65874958038330078D));
            this.txtswitch.Style.Color = System.Drawing.Color.White;
            this.txtswitch.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtswitch.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtswitch.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtswitch.StyleName = "";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.4807741641998291D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.82020723819732666D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.55291587114334106D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.65875029563903809D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.57937586307525635D)));
            this.table3.Body.SetCellContent(0, 1, this.txtmeterphase);
            this.table3.Body.SetCellContent(1, 1, this.textBox11);
            this.table3.Body.SetCellContent(2, 1, this.textBox12);
            this.table3.Body.SetCellContent(3, 1, this.textBox13);
            this.table3.Body.SetCellContent(0, 0, this.textBox14);
            this.table3.Body.SetCellContent(1, 0, this.textBox42);
            this.table3.Body.SetCellContent(2, 0, this.textBox43);
            this.table3.Body.SetCellContent(3, 0, this.textBox44);
            tableGroup7.Name = "group4";
            tableGroup8.Name = "tableGroup";
            this.table3.ColumnGroups.Add(tableGroup7);
            this.table3.ColumnGroups.Add(tableGroup8);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14,
            this.txtmeterphase,
            this.textBox42,
            this.textBox11,
            this.textBox43,
            this.textBox12,
            this.textBox44,
            this.textBox13});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.1280002593994141D), Telerik.Reporting.Drawing.Unit.Cm(85.344001770019531D));
            this.table3.Name = "table3";
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "detailTableGroup";
            tableGroup10.Name = "group";
            tableGroup11.Name = "group1";
            tableGroup12.Name = "group2";
            this.table3.RowGroups.Add(tableGroup9);
            this.table3.RowGroups.Add(tableGroup10);
            this.table3.RowGroups.Add(tableGroup11);
            this.table3.RowGroups.Add(tableGroup12);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8807740211486816D), Telerik.Reporting.Drawing.Unit.Cm(2.6112492084503174D));
            this.table3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtmeterphase
            // 
            this.txtmeterphase.Name = "txtmeterphase";
            this.txtmeterphase.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4807741641998291D), Telerik.Reporting.Drawing.Unit.Cm(0.820207417011261D));
            this.txtmeterphase.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtmeterphase.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtmeterphase.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtmeterphase.Value = "=Fields.MeterPhase";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4807741641998291D), Telerik.Reporting.Drawing.Unit.Cm(0.55291599035263062D));
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "=Fields.RoofType";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4807741641998291D), Telerik.Reporting.Drawing.Unit.Cm(0.65875035524368286D));
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "=Fields.RoofAngle";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4807741641998291D), Telerik.Reporting.Drawing.Unit.Cm(0.57937586307525635D));
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "=Fields.HouseType";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D), Telerik.Reporting.Drawing.Unit.Cm(0.820207417011261D));
            this.textBox14.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D), Telerik.Reporting.Drawing.Unit.Cm(0.55291599035263062D));
            this.textBox42.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox42.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D), Telerik.Reporting.Drawing.Unit.Cm(0.65875035524368286D));
            this.textBox43.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D), Telerik.Reporting.Drawing.Unit.Cm(0.57937586307525635D));
            this.textBox44.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox44.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.StyleName = "";
            this.textBox44.Value = "";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7428314685821533D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.1471681594848633D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.3520825207233429D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.41458246111869812D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.5187491774559021D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999908804893494D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.5610438585281372D)));
            this.table5.Body.SetCellContent(4, 1, this.textBox18);
            this.table5.Body.SetCellContent(0, 1, this.textBox17);
            this.table5.Body.SetCellContent(2, 1, this.txtInverterdetail1);
            this.table5.Body.SetCellContent(1, 1, this.textBox21);
            this.table5.Body.SetCellContent(3, 1, this.textBox23);
            this.table5.Body.SetCellContent(0, 0, this.textBox45);
            this.table5.Body.SetCellContent(1, 0, this.textBox46);
            this.table5.Body.SetCellContent(2, 0, this.textBox48);
            this.table5.Body.SetCellContent(3, 0, this.textBox50);
            this.table5.Body.SetCellContent(4, 0, this.textBox72);
            tableGroup13.Name = "group17";
            tableGroup14.Name = "tableGroup1";
            this.table5.ColumnGroups.Add(tableGroup13);
            this.table5.ColumnGroups.Add(tableGroup14);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox45,
            this.textBox17,
            this.textBox46,
            this.textBox21,
            this.textBox48,
            this.txtInverterdetail1,
            this.textBox50,
            this.textBox23,
            this.textBox72,
            this.textBox18});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.25399994850158691D), Telerik.Reporting.Drawing.Unit.Cm(89.408004760742188D));
            this.table5.Name = "table5";
            tableGroup15.Name = "group6";
            tableGroup16.Name = "group10";
            tableGroup17.Name = "group8";
            tableGroup18.Name = "group12";
            tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup19.Name = "detailTableGroup1";
            this.table5.RowGroups.Add(tableGroup15);
            this.table5.RowGroups.Add(tableGroup16);
            this.table5.RowGroups.Add(tableGroup17);
            this.table5.RowGroups.Add(tableGroup18);
            this.table5.RowGroups.Add(tableGroup19);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.8899993896484375D), Telerik.Reporting.Drawing.Unit.Cm(5.5879936218261719D));
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1471686363220215D), Telerik.Reporting.Drawing.Unit.Cm(1.5610438585281372D));
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox18.Value = "- Installation by CEC Accredited Solar Installer\r\n- CEC Approved, Mounts, & Brack" +
    "ets\r\n- Standard Admin. & Grid-Connection";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1471686363220215D), Telerik.Reporting.Drawing.Unit.Inch(0.3520825207233429D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.SystemCapKW +\" KW\" ";
            // 
            // txtInverterdetail1
            // 
            this.txtInverterdetail1.Name = "txtInverterdetail1";
            this.txtInverterdetail1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1471686363220215D), Telerik.Reporting.Drawing.Unit.Inch(0.51874911785125732D));
            this.txtInverterdetail1.Style.Font.Bold = false;
            this.txtInverterdetail1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtInverterdetail1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtInverterdetail1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtInverterdetail1.StyleName = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1471686363220215D), Telerik.Reporting.Drawing.Unit.Inch(0.41458243131637573D));
            this.textBox21.Style.Font.Bold = false;
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "=Fields.PanelDetails";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1471686363220215D), Telerik.Reporting.Drawing.Unit.Inch(0.29999908804893494D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "..... Through AriseSolar";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7428314685821533D), Telerik.Reporting.Drawing.Unit.Inch(0.3520825207233429D));
            this.textBox45.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox45.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7428314685821533D), Telerik.Reporting.Drawing.Unit.Inch(0.41458243131637573D));
            this.textBox46.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox46.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox46.StyleName = "";
            this.textBox46.Value = "";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7428314685821533D), Telerik.Reporting.Drawing.Unit.Inch(0.51874911785125732D));
            this.textBox48.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox48.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox48.StyleName = "";
            this.textBox48.Value = "";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7428314685821533D), Telerik.Reporting.Drawing.Unit.Inch(0.29999908804893494D));
            this.textBox50.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox50.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7428314685821533D), Telerik.Reporting.Drawing.Unit.Cm(1.5610438585281372D));
            this.textBox72.Style.BackgroundImage.MimeType = "image/png";
            this.textBox72.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox72.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox72.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox72.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            this.textBox72.Value = "";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(14.732000350952148D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854160785675049D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox8);
            tableGroup20.Name = "tableGroup12";
            this.table6.ColumnGroups.Add(tableGroup20);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox8});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.25399994850158691D), Telerik.Reporting.Drawing.Unit.Cm(83.311996459960938D));
            this.table6.Name = "table6";
            tableGroup22.Name = "group26";
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "detailTableGroup5";
            this.table6.RowGroups.Add(tableGroup21);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.732000350952148D), Telerik.Reporting.Drawing.Unit.Cm(0.60854160785675049D));
            this.table6.Style.Font.Name = "Calibri";
            this.table6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.732000350952148D), Telerik.Reporting.Drawing.Unit.Cm(0.60854160785675049D));
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12.5D);
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox8.Value = "=TrimStart(Fields.PostalAddress) +\",\"+TrimStart(Fields.PostalCity) +\", \"+TrimStar" +
    "t(Fields.PostalState)+\", \"+TrimStart(Fields.PostalPostCode)";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.0799994468688965D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.76458323001861572D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854202508926392D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.976250410079956D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999961256980896D)));
            this.table8.Body.SetCellContent(1, 0, this.txtpaymentplan);
            this.table8.Body.SetCellContent(0, 0, this.textBox55);
            this.table8.Body.SetCellContent(2, 0, this.textBox214);
            this.table8.Body.SetCellContent(3, 0, this.txtotherdetail);
            tableGroup23.Name = "tableGroup4";
            tableGroup23.ReportItem = this.textBox10;
            this.table8.ColumnGroups.Add(tableGroup23);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox55,
            this.txtpaymentplan,
            this.textBox214,
            this.txtotherdetail,
            this.textBox10});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9060001373291016D), Telerik.Reporting.Drawing.Unit.Cm(89.408004760742188D));
            this.table8.Name = "table8";
            tableGroup25.Name = "group96";
            tableGroup26.Name = "group95";
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.ChildGroups.Add(tableGroup26);
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup24.Name = "detailTableGroup3";
            tableGroup28.Name = "group98";
            tableGroup27.ChildGroups.Add(tableGroup28);
            tableGroup27.Name = "group97";
            tableGroup30.Name = "group100";
            tableGroup29.ChildGroups.Add(tableGroup30);
            tableGroup29.Name = "group99";
            this.table8.RowGroups.Add(tableGroup24);
            this.table8.RowGroups.Add(tableGroup27);
            this.table8.RowGroups.Add(tableGroup29);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0799994468688965D), Telerik.Reporting.Drawing.Unit.Cm(3.4579169750213623D));
            // 
            // txtpaymentplan
            // 
            this.txtpaymentplan.Name = "txtpaymentplan";
            this.txtpaymentplan.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0799994468688965D), Telerik.Reporting.Drawing.Unit.Cm(0.60854202508926392D));
            this.txtpaymentplan.Style.Font.Bold = false;
            this.txtpaymentplan.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtpaymentplan.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtpaymentplan.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtpaymentplan.StyleName = "";
            this.txtpaymentplan.Value = "";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0799994468688965D), Telerik.Reporting.Drawing.Unit.Cm(0.76458323001861572D));
            this.textBox55.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(70)))), ((int)(((byte)(110)))));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox55.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox55.StyleName = "";
            this.textBox55.Value = "PROJECT PLAN";
            // 
            // textBox214
            // 
            this.textBox214.Name = "textBox214";
            this.textBox214.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0799994468688965D), Telerik.Reporting.Drawing.Unit.Cm(0.976250410079956D));
            this.textBox214.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(70)))), ((int)(((byte)(110)))));
            this.textBox214.Style.Font.Bold = true;
            this.textBox214.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox214.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox214.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox214.StyleName = "";
            this.textBox214.Value = "Others";
            // 
            // txtotherdetail
            // 
            this.txtotherdetail.Name = "txtotherdetail";
            this.txtotherdetail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0799994468688965D), Telerik.Reporting.Drawing.Unit.Cm(0.49999961256980896D));
            this.txtotherdetail.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(70)))), ((int)(((byte)(110)))));
            this.txtotherdetail.Style.Font.Bold = false;
            this.txtotherdetail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtotherdetail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtotherdetail.StyleName = "";
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.8145831823348999D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.68229168653488159D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.76166611909866333D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60291653871536255D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.76166671514511108D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.8674999475479126D)));
            this.table11.Body.SetCellContent(0, 0, this.textBox71);
            this.table11.Body.SetCellContent(4, 0, this.textBox74);
            this.table11.Body.SetCellContent(7, 0, this.textBox76);
            this.table11.Body.SetCellContent(6, 0, this.textBox78);
            this.table11.Body.SetCellContent(5, 0, this.textBox80);
            this.table11.Body.SetCellContent(1, 0, this.textBox82);
            this.table11.Body.SetCellContent(3, 0, this.textBox9);
            this.table11.Body.SetCellContent(2, 0, this.textBox84);
            this.table11.Body.SetCellContent(8, 0, this.textBox86);
            this.table11.Body.SetCellContent(0, 1, this.txtreq);
            this.table11.Body.SetCellContent(1, 1, this.txtcompl);
            this.table11.Body.SetCellContent(2, 1, this.txtextra);
            this.table11.Body.SetCellContent(3, 1, this.txtfullretail);
            this.table11.Body.SetCellContent(4, 1, this.txtdiscount2);
            this.table11.Body.SetCellContent(5, 1, this.txtstc);
            this.table11.Body.SetCellContent(6, 1, this.txtgst);
            this.table11.Body.SetCellContent(7, 1, this.txtdeposit);
            this.table11.Body.SetCellContent(8, 1, this.txtlessdeposit);
            tableGroup31.Name = "tableGroup8";
            tableGroup31.ReportItem = this.textBox70;
            tableGroup32.Name = "tableGroup9";
            tableGroup32.ReportItem = this.txtbasiccost;
            this.table11.ColumnGroups.Add(tableGroup31);
            this.table11.ColumnGroups.Add(tableGroup32);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.txtreq,
            this.textBox82,
            this.txtcompl,
            this.textBox84,
            this.txtextra,
            this.textBox9,
            this.txtfullretail,
            this.textBox74,
            this.txtdiscount2,
            this.textBox80,
            this.txtstc,
            this.textBox78,
            this.txtgst,
            this.textBox76,
            this.txtdeposit,
            this.textBox86,
            this.txtlessdeposit,
            this.textBox70,
            this.txtbasiccost});
            this.table11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.25399994850158691D), Telerik.Reporting.Drawing.Unit.Cm(95.210807800292969D));
            this.table11.Name = "table11";
            tableGroup34.Name = "group31";
            tableGroup35.Name = "group37";
            tableGroup36.Name = "group38";
            tableGroup37.Name = "group32";
            tableGroup38.Name = "group33";
            tableGroup39.Name = "group36";
            tableGroup40.Name = "group35";
            tableGroup41.Name = "group34";
            tableGroup42.Name = "group39";
            tableGroup33.ChildGroups.Add(tableGroup34);
            tableGroup33.ChildGroups.Add(tableGroup35);
            tableGroup33.ChildGroups.Add(tableGroup36);
            tableGroup33.ChildGroups.Add(tableGroup37);
            tableGroup33.ChildGroups.Add(tableGroup38);
            tableGroup33.ChildGroups.Add(tableGroup39);
            tableGroup33.ChildGroups.Add(tableGroup40);
            tableGroup33.ChildGroups.Add(tableGroup41);
            tableGroup33.ChildGroups.Add(tableGroup42);
            tableGroup33.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup33.Name = "detailTableGroup7";
            this.table11.RowGroups.Add(tableGroup33);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.8899993896484375D), Telerik.Reporting.Drawing.Unit.Cm(7.4049992561340332D));
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.8145831823348999D));
            this.textBox71.Style.Color = System.Drawing.Color.White;
            this.textBox71.Style.Font.Bold = true;
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox71.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox71.Value = "Site Requirements";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D));
            this.textBox74.Style.Color = System.Drawing.Color.White;
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox74.StyleName = "";
            this.textBox74.Value = "Less Discounts";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D));
            this.textBox76.Style.Color = System.Drawing.Color.White;
            this.textBox76.Style.Font.Bold = true;
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "Deposit Amount";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.76166671514511108D));
            this.textBox78.Style.Color = System.Drawing.Color.White;
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox78.StyleName = "";
            this.textBox78.Value = "Total Cost inc. GST";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.60291653871536255D));
            this.textBox80.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.textBox80.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox80.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox80.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox80.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox80.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox80.Style.Color = System.Drawing.Color.White;
            this.textBox80.Style.Font.Bold = true;
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox80.StyleName = "";
            this.textBox80.Value = "Less Govt. Rebate";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.68229168653488159D));
            this.textBox82.Style.Color = System.Drawing.Color.White;
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox82.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox82.StyleName = "";
            this.textBox82.Value = "Compliance & Safety";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.76166611909866333D));
            this.textBox9.Style.Color = System.Drawing.Color.White;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "Full Retail Price";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D));
            this.textBox84.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.textBox84.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox84.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.textBox84.Style.Color = System.Drawing.Color.White;
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox84.StyleName = "";
            this.textBox84.Value = "Extras, Upgrades, & Other";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.338646411895752D), Telerik.Reporting.Drawing.Unit.Cm(0.8674999475479126D));
            this.textBox86.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.textBox86.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox86.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.textBox86.Style.Color = System.Drawing.Color.White;
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox86.StyleName = "";
            this.textBox86.Value = "Total less Deposit";
            // 
            // txtreq
            // 
            this.txtreq.Name = "txtreq";
            this.txtreq.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.8145831823348999D));
            this.txtreq.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtreq.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtreq.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtreq.Value = "=Fields.lblreq";
            // 
            // txtcompl
            // 
            this.txtcompl.Name = "txtcompl";
            this.txtcompl.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.68229168653488159D));
            this.txtcompl.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtcompl.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtcompl.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtcompl.Value = "=Fields.lblcompl";
            // 
            // txtextra
            // 
            this.txtextra.Name = "txtextra";
            this.txtextra.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D));
            this.txtextra.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.txtextra.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtextra.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.txtextra.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtextra.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtextra.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtextra.Value = "=Fields.lblextra";
            // 
            // txtfullretail
            // 
            this.txtfullretail.Name = "txtfullretail";
            this.txtfullretail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.76166611909866333D));
            this.txtfullretail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtfullretail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtfullretail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtfullretail.Value = "=Fields.lblfullretail";
            // 
            // txtdiscount2
            // 
            this.txtdiscount2.Name = "txtdiscount2";
            this.txtdiscount2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D));
            this.txtdiscount2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtdiscount2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtdiscount2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtdiscount2.Value = "=Fields.lbldiscount2";
            // 
            // txtstc
            // 
            this.txtstc.Name = "txtstc";
            this.txtstc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.60291653871536255D));
            this.txtstc.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.txtstc.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.txtstc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtstc.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtstc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtstc.Value = "=Fields.lblstc";
            // 
            // txtgst
            // 
            this.txtgst.Name = "txtgst";
            this.txtgst.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.76166671514511108D));
            this.txtgst.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtgst.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtgst.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtgst.Value = "=Fields.lblgst";
            // 
            // txtdeposit
            // 
            this.txtdeposit.Name = "txtdeposit";
            this.txtdeposit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.78812497854232788D));
            this.txtdeposit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtdeposit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtdeposit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtdeposit.Value = "=Fields.lbldeposit";
            // 
            // txtlessdeposit
            // 
            this.txtlessdeposit.Name = "txtlessdeposit";
            this.txtlessdeposit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5513532161712646D), Telerik.Reporting.Drawing.Unit.Cm(0.8674999475479126D));
            this.txtlessdeposit.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.txtlessdeposit.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtlessdeposit.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.txtlessdeposit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtlessdeposit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtlessdeposit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtlessdeposit.Value = "=Fields.lbllessdeposit";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8101297616958618D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8894526958465576D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53976178169250488D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.56549698114395142D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.59195548295974731D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.4861220121383667D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox57);
            this.table9.Body.SetCellContent(0, 1, this.textBox59);
            this.table9.Body.SetCellContent(3, 0, this.textBox58);
            this.table9.Body.SetCellContent(3, 1, this.textBox60);
            this.table9.Body.SetCellContent(1, 0, this.textBox61);
            this.table9.Body.SetCellContent(1, 1, this.textBox62);
            this.table9.Body.SetCellContent(2, 0, this.textBox63);
            this.table9.Body.SetCellContent(2, 1, this.textBox64);
            tableGroup43.Name = "group20";
            tableGroup44.Name = "tableGroup6";
            this.table9.ColumnGroups.Add(tableGroup43);
            this.table9.ColumnGroups.Add(tableGroup44);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox57,
            this.textBox59,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox58,
            this.textBox60});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9060001373291016D), Telerik.Reporting.Drawing.Unit.Cm(96.152420043945312D));
            this.table9.Name = "table9";
            tableGroup46.Name = "group21";
            tableGroup47.Name = "group23";
            tableGroup48.Name = "group24";
            tableGroup49.Name = "group22";
            tableGroup45.ChildGroups.Add(tableGroup46);
            tableGroup45.ChildGroups.Add(tableGroup47);
            tableGroup45.ChildGroups.Add(tableGroup48);
            tableGroup45.ChildGroups.Add(tableGroup49);
            tableGroup45.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup45.Name = "detailTableGroup4";
            this.table9.RowGroups.Add(tableGroup45);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6995828151702881D), Telerik.Reporting.Drawing.Unit.Cm(2.1833362579345703D));
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8101304769515991D), Telerik.Reporting.Drawing.Unit.Cm(0.53976172208786011D));
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox57.Value = "";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.889452338218689D), Telerik.Reporting.Drawing.Unit.Cm(0.53976172208786011D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.Value = "Westpac";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8101304769515991D), Telerik.Reporting.Drawing.Unit.Cm(0.48612198233604431D));
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox58.StyleName = "";
            this.textBox58.Value = "";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.889452338218689D), Telerik.Reporting.Drawing.Unit.Cm(0.48612198233604431D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox60.StyleName = "";
            this.textBox60.Value = "Arise Solar";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8101304769515991D), Telerik.Reporting.Drawing.Unit.Cm(0.56549704074859619D));
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox61.StyleName = "";
            this.textBox61.Value = "";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.889452338218689D), Telerik.Reporting.Drawing.Unit.Cm(0.56549704074859619D));
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            this.textBox62.Value = "034-001";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8101304769515991D), Telerik.Reporting.Drawing.Unit.Cm(0.59195542335510254D));
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox63.StyleName = "";
            this.textBox63.Value = "";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.889452338218689D), Telerik.Reporting.Drawing.Unit.Cm(0.59195542335510254D));
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "504046";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0341989994049072D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0615999698638916D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.880000114440918D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox66);
            this.table10.Body.SetCellContent(0, 1, this.textBox20);
            tableGroup50.Name = "tableGroup7";
            tableGroup51.Name = "group3";
            this.table10.ColumnGroups.Add(tableGroup50);
            this.table10.ColumnGroups.Add(tableGroup51);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox66,
            this.textBox20});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.144200325012207D), Telerik.Reporting.Drawing.Unit.Cm(100.19850921630859D));
            this.table10.Name = "table10";
            tableGroup53.Name = "group25";
            tableGroup52.ChildGroups.Add(tableGroup53);
            tableGroup52.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup52.Name = "detailTableGroup6";
            this.table10.RowGroups.Add(tableGroup52);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0957989692687988D), Telerik.Reporting.Drawing.Unit.Cm(1.880000114440918D));
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0341992378234863D), Telerik.Reporting.Drawing.Unit.Cm(1.880000114440918D));
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox66.Value = "Balance payments via\r\nCredit Card will incur a 1%\r\nsurcharge.";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0615999698638916D), Telerik.Reporting.Drawing.Unit.Cm(1.880000114440918D));
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "Payable to \"Arise Solar\" and mail it to:\r\nPO Box 3079 \r\nSunnybank South,QLD 4109";
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(44D));
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9000003337860107D), Telerik.Reporting.Drawing.Unit.Inch(1.4000040292739868D));
            this.htmlTextBox7.Value = resources.GetString("htmlTextBox7.Value");
            // 
            // htmlTextBox8
            // 
            this.htmlTextBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(46.300003051757812D));
            this.htmlTextBox8.Name = "htmlTextBox8";
            this.htmlTextBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox8.Value = resources.GetString("htmlTextBox8.Value");
            // 
            // htmlTextBox9
            // 
            this.htmlTextBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(46.300003051757812D));
            this.htmlTextBox9.Name = "htmlTextBox9";
            this.htmlTextBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5999996662139893D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox9.Value = resources.GetString("htmlTextBox9.Value");
            // 
            // htmlTextBox10
            // 
            this.htmlTextBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(47D));
            this.htmlTextBox10.Name = "htmlTextBox10";
            this.htmlTextBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox10.Value = resources.GetString("htmlTextBox10.Value");
            // 
            // htmlTextBox11
            // 
            this.htmlTextBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(47D));
            this.htmlTextBox11.Name = "htmlTextBox11";
            this.htmlTextBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5999996662139893D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox11.Value = resources.GetString("htmlTextBox11.Value");
            // 
            // htmlTextBox12
            // 
            this.htmlTextBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(47.700000762939453D));
            this.htmlTextBox12.Name = "htmlTextBox12";
            this.htmlTextBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7999997138977051D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox12.Value = resources.GetString("htmlTextBox12.Value");
            // 
            // htmlTextBox13
            // 
            this.htmlTextBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(47.700000762939453D));
            this.htmlTextBox13.Name = "htmlTextBox13";
            this.htmlTextBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5999996662139893D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox13.Value = resources.GetString("htmlTextBox13.Value");
            // 
            // Nwelcomeletter
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Nwelcomeletter";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.2677168846130371D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox txtmeter;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox txtswitch;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox txtmeterphase;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox txtInverterdetail1;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox txtpaymentplan;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox214;
        private Telerik.Reporting.TextBox txtotherdetail;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox txtreq;
        private Telerik.Reporting.TextBox txtcompl;
        private Telerik.Reporting.TextBox txtextra;
        private Telerik.Reporting.TextBox txtfullretail;
        private Telerik.Reporting.TextBox txtdiscount2;
        private Telerik.Reporting.TextBox txtstc;
        private Telerik.Reporting.TextBox txtgst;
        private Telerik.Reporting.TextBox txtdeposit;
        private Telerik.Reporting.TextBox txtlessdeposit;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox txtbasiccost;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.HtmlTextBox htmlTextBox8;
        private Telerik.Reporting.HtmlTextBox htmlTextBox9;
        private Telerik.Reporting.HtmlTextBox htmlTextBox10;
        private Telerik.Reporting.HtmlTextBox htmlTextBox11;
        private Telerik.Reporting.HtmlTextBox htmlTextBox12;
        private Telerik.Reporting.HtmlTextBox htmlTextBox13;
    }
}