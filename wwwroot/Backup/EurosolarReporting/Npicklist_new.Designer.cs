using System;

namespace EurosolarReporting
{
    partial class Npicklist_new
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Npicklist_new));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox158 = new Telerik.Reporting.TextBox();
            this.textBox159 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.lblPanDesc = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.table4 = new Telerik.Reporting.Table();
            this.lblqty1 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.table5 = new Telerik.Reporting.Table();
            this.lblqty2 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.txrintadd1 = new Telerik.Reporting.TextBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.txrintadd1_1 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.lblqty21 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.lblqty11 = new Telerik.Reporting.TextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.lblPanDesc1 = new Telerik.Reporting.TextBox();
            this.pictureBox8 = new Telerik.Reporting.PictureBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.18386709690094D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Value = "Installer Date";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458367705345154D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Calibri";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox50.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox50.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox50.StyleName = "";
            this.textBox50.Value = ":";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4962732791900635D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Value = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1170282363891602D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Calibri";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox21.StyleName = "";
            this.textBox21.Value = "Installer Name";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458358764648438D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Name = "Calibri";
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox55.StyleName = "";
            this.textBox55.Value = ":";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4242727756500244D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox31.Style.Font.Bold = false;
            this.textBox31.Style.Font.Name = "Calibri";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox31.StyleName = "";
            this.textBox31.Value = "=Fields.Picinstallername";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2382138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Calibri";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "Installer Mobile:";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.37595838308334351D));
            this.textBox5.Style.Font.Bold = false;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox5.StyleName = "";
            this.textBox5.Value = "=Fields.InstallerMobile";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73785006999969482D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox26.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Calibri";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "Qty";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8258787393569946D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Calibri";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "Item";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D), Telerik.Reporting.Drawing.Unit.Inch(0.43185952305793762D));
            this.textBox38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox38.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Calibri";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "Model";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0100903511047363D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox36.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Calibri";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "Description";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D), Telerik.Reporting.Drawing.Unit.Inch(0.43185952305793762D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Calibri";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "Done";
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1767926216125488D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox111.Style.Font.Bold = true;
            this.textBox111.Style.Font.Name = "Calibri";
            this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox111.Value = "Installer Date";
            // 
            // textBox112
            // 
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox112.Style.Font.Bold = true;
            this.textBox112.Style.Font.Name = "Calibri";
            this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox112.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox112.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox112.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox112.StyleName = "";
            this.textBox112.Value = ":";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox113.Style.Font.Bold = false;
            this.textBox113.Style.Font.Name = "Calibri";
            this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox113.Value = "";
            // 
            // textBox114
            // 
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.119999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox114.Style.Font.Bold = true;
            this.textBox114.Style.Font.Name = "Calibri";
            this.textBox114.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox114.StyleName = "";
            this.textBox114.Value = "Installer Name";
            // 
            // textBox115
            // 
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox115.Style.Font.Bold = true;
            this.textBox115.Style.Font.Name = "Calibri";
            this.textBox115.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox115.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox115.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox115.StyleName = "";
            this.textBox115.Value = ":";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox116.Style.Font.Bold = false;
            this.textBox116.Style.Font.Name = "Calibri";
            this.textBox116.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox116.StyleName = "";
            this.textBox116.Value = "=Fields.Picinstallername";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2400000095367432D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox117.Style.Font.Bold = true;
            this.textBox117.Style.Font.Name = "Calibri";
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox117.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox117.StyleName = "";
            this.textBox117.Value = "Installer Mobile:";
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox118.Style.Font.Bold = false;
            this.textBox118.Style.Font.Name = "Calibri";
            this.textBox118.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox118.StyleName = "";
            this.textBox118.Value = "=Fields.InstallerMobile";
            // 
            // textBox155
            // 
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73785006999969482D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox155.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox155.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox155.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox155.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox155.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox155.Style.Font.Bold = true;
            this.textBox155.Style.Font.Name = "Calibri";
            this.textBox155.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox155.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox155.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox155.Value = "Qty";
            // 
            // textBox156
            // 
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8258787393569946D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox156.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox156.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox156.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox156.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox156.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox156.Style.Font.Bold = true;
            this.textBox156.Style.Font.Name = "Calibri";
            this.textBox156.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox156.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox156.Value = "Item";
            // 
            // textBox157
            // 
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D), Telerik.Reporting.Drawing.Unit.Inch(0.43185952305793762D));
            this.textBox157.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox157.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox157.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox157.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox157.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox157.Style.Font.Bold = true;
            this.textBox157.Style.Font.Name = "Calibri";
            this.textBox157.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox157.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox157.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox157.StyleName = "";
            this.textBox157.Value = "Model";
            // 
            // textBox158
            // 
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0100908279418945D), Telerik.Reporting.Drawing.Unit.Inch(0.43185952305793762D));
            this.textBox158.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox158.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox158.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox158.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox158.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox158.Style.Font.Bold = true;
            this.textBox158.Style.Font.Name = "Calibri";
            this.textBox158.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox158.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox158.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox158.StyleName = "";
            this.textBox158.Value = "Description";
            // 
            // textBox159
            // 
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D), Telerik.Reporting.Drawing.Unit.Inch(0.43185952305793762D));
            this.textBox159.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox159.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox159.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox159.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox159.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox159.Style.Font.Bold = true;
            this.textBox159.Style.Font.Name = "Calibri";
            this.textBox159.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox159.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox159.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox159.Value = "Done";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(59.400001525878906D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table1,
            this.textBox12,
            this.table2,
            this.table4,
            this.table5,
            this.textBox60,
            this.textBox33,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox47,
            this.textBox45,
            this.textBox46,
            this.textBox48,
            this.textBox61,
            this.txrintadd1,
            this.pictureBox5,
            this.table3,
            this.textBox121,
            this.txrintadd1_1,
            this.textBox122,
            this.textBox123,
            this.textBox124,
            this.textBox125,
            this.textBox126,
            this.textBox127,
            this.textBox128,
            this.textBox129,
            this.textBox130,
            this.textBox131,
            this.textBox132,
            this.table6,
            this.table7,
            this.table8,
            this.textBox62,
            this.textBox63,
            this.textBox119});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.pictureBox1.Style.Visible = true;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.18386709690094D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26458436250686646D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4962731599807739D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1170279979705811D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26458409428596497D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.424272894859314D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2382137775421143D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4217612743377686D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.39201390743255615D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.37118059396743774D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox2);
            this.table1.Body.SetCellContent(0, 2, this.textBox4);
            this.table1.Body.SetCellContent(3, 0, this.textBox7);
            this.table1.Body.SetCellContent(3, 2, this.textBox8);
            this.table1.Body.SetCellContent(2, 0, this.textBox10);
            this.table1.Body.SetCellContent(2, 2, this.textBox11);
            this.table1.Body.SetCellContent(1, 0, this.textBox13);
            this.table1.Body.SetCellContent(1, 2, this.textBox14);
            this.table1.Body.SetCellContent(0, 6, this.textBox17);
            this.table1.Body.SetCellContent(1, 6, this.textBox18);
            this.table1.Body.SetCellContent(3, 6, this.textBox20);
            this.table1.Body.SetCellContent(0, 3, this.textBox22);
            this.table1.Body.SetCellContent(1, 3, this.textBox23);
            this.table1.Body.SetCellContent(2, 3, this.textBox24);
            this.table1.Body.SetCellContent(3, 3, this.textBox25);
            this.table1.Body.SetCellContent(0, 5, this.textBox27);
            this.table1.Body.SetCellContent(1, 5, this.textBox28);
            this.table1.Body.SetCellContent(2, 5, this.textBox29);
            this.table1.Body.SetCellContent(3, 5, this.textBox30);
            this.table1.Body.SetCellContent(0, 7, this.textBox6);
            this.table1.Body.SetCellContent(1, 7, this.textBox9);
            this.table1.Body.SetCellContent(3, 7, this.textBox15);
            this.table1.Body.SetCellContent(2, 6, this.textBox19);
            this.table1.Body.SetCellContent(2, 7, this.textBox70);
            this.table1.Body.SetCellContent(0, 1, this.textBox51);
            this.table1.Body.SetCellContent(1, 1, this.textBox52);
            this.table1.Body.SetCellContent(2, 1, this.textBox53);
            this.table1.Body.SetCellContent(3, 1, this.textBox54);
            this.table1.Body.SetCellContent(0, 4, this.textBox56);
            this.table1.Body.SetCellContent(1, 4, this.textBox57);
            this.table1.Body.SetCellContent(2, 4, this.textBox58);
            this.table1.Body.SetCellContent(3, 4, this.textBox59);
            tableGroup1.Name = "tableGroup";
            tableGroup1.ReportItem = this.textBox1;
            tableGroup2.Name = "group10";
            tableGroup2.ReportItem = this.textBox50;
            tableGroup3.Name = "tableGroup1";
            tableGroup3.ReportItem = this.textBox3;
            tableGroup4.Name = "group4";
            tableGroup4.ReportItem = this.textBox21;
            tableGroup5.Name = "group11";
            tableGroup5.ReportItem = this.textBox55;
            tableGroup6.Name = "group5";
            tableGroup6.ReportItem = this.textBox31;
            tableGroup7.Name = "group3";
            tableGroup7.ReportItem = this.textBox16;
            tableGroup8.Name = "group6";
            tableGroup8.ReportItem = this.textBox5;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox51,
            this.textBox4,
            this.textBox22,
            this.textBox56,
            this.textBox27,
            this.textBox17,
            this.textBox6,
            this.textBox13,
            this.textBox52,
            this.textBox14,
            this.textBox23,
            this.textBox57,
            this.textBox28,
            this.textBox18,
            this.textBox9,
            this.textBox10,
            this.textBox53,
            this.textBox11,
            this.textBox24,
            this.textBox58,
            this.textBox29,
            this.textBox19,
            this.textBox70,
            this.textBox7,
            this.textBox54,
            this.textBox8,
            this.textBox25,
            this.textBox59,
            this.textBox30,
            this.textBox20,
            this.textBox15,
            this.textBox1,
            this.textBox50,
            this.textBox3,
            this.textBox21,
            this.textBox55,
            this.textBox31,
            this.textBox16,
            this.textBox5});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.11811026185750961D), Telerik.Reporting.Drawing.Unit.Inch(2.5984251499176025D));
            this.table1.Name = "table1";
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "detailTableGroup";
            tableGroup10.Name = "group2";
            tableGroup11.Name = "group1";
            tableGroup12.Name = "group";
            this.table1.RowGroups.Add(tableGroup9);
            this.table1.RowGroups.Add(tableGroup10);
            this.table1.RowGroups.Add(tableGroup11);
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.1453056335449219D), Telerik.Reporting.Drawing.Unit.Inch(1.9522640705108643D));
            this.table1.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.Color = System.Drawing.Color.White;
            this.table1.Style.Font.Bold = false;
            this.table1.Style.Font.Name = "Calibri";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.table1.Style.LineColor = System.Drawing.Color.Transparent;
            this.table1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.18386709690094D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox2.Value = "Customer Name\t";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4962732791900635D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox4.Value = "=Fields.Customer";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.18386709690094D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox7.StyleName = "";
            this.textBox7.Value = "Store";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.496273398399353D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox8.StyleName = "";
            this.textBox8.Value = "=Fields.StoreName";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.18386709690094D), Telerik.Reporting.Drawing.Unit.Inch(0.39201390743255615D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox10.StyleName = "";
            this.textBox10.Value = "Manual Quote";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.496273398399353D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox11.StyleName = "";
            this.textBox11.Value = "=Fields.ManualQuoteNumber";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.18386709690094D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox13.StyleName = "";
            this.textBox13.Value = "House Type";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.496273398399353D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox14.StyleName = "";
            this.textBox14.Value = "=Fields.HouseType";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2382138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "Mob:";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2382138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "Angle:";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2382138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox20.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1170282363891602D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox22.StyleName = "";
            this.textBox22.Value = "Phone";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1170282363891602D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox23.StyleName = "";
            this.textBox23.Value = "Roof Type";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1170282363891602D), Telerik.Reporting.Drawing.Unit.Inch(0.39201390743255615D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Calibri";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox24.StyleName = "";
            this.textBox24.Value = "Project No.";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1170282363891602D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox25.Style.Font.Name = "Calibri";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox25.StyleName = "";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4242727756500244D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox27.Style.Font.Name = "Calibri";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox27.StyleName = "";
            this.textBox27.Value = "=Fields.CustPhone";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.424272894859314D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox28.Style.Font.Name = "Calibri";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox28.StyleName = "";
            this.textBox28.Value = "=Fields.RoofType";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.424272894859314D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox29.Style.Font.Name = "Calibri";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox29.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.424272894859314D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox30.Style.Font.Name = "Calibri";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox30.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.37595838308334351D));
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox6.StyleName = "";
            this.textBox6.Value = "=Fields.ContMobile";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.38159725069999695D));
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.RoofAngle";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox15.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2382138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.39201390743255615D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox19.StyleName = "";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.39201390743255615D));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox70.StyleName = "";
            this.textBox70.Value = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Name = "Calibri";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox51.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox51.StyleName = "";
            this.textBox51.Value = ":";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458367705345154D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Name = "Calibri";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox52.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox52.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox52.StyleName = "";
            this.textBox52.Value = ":";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Name = "Calibri";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox53.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox53.StyleName = "";
            this.textBox53.Value = ":";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Font.Name = "Calibri";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox54.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox54.StyleName = "";
            this.textBox54.Value = ":";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458364725112915D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.Font.Name = "Calibri";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.StyleName = "";
            this.textBox56.Value = ":";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458361744880676D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Name = "Calibri";
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.StyleName = "";
            this.textBox57.Value = ":";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458364725112915D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.Font.Name = "Calibri";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox58.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox58.StyleName = "";
            this.textBox58.Value = ":";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458367705345154D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Name = "Calibri";
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Cm(11.926457405090332D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox12.Style.Color = System.Drawing.Color.Black;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox12.Value = "SITE ADDRESS :";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.7378503680229187D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8258785009384155D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.0100905895233154D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox35);
            this.table2.Body.SetCellContent(0, 1, this.textBox40);
            this.table2.Body.SetCellContent(0, 2, this.textBox49);
            this.table2.Body.SetCellContent(0, 3, this.lblPanDesc);
            this.table2.Body.SetCellContent(0, 4, this.pictureBox3);
            tableGroup13.Name = "tableGroup2";
            tableGroup13.ReportItem = this.textBox26;
            tableGroup14.Name = "tableGroup3";
            tableGroup14.ReportItem = this.textBox32;
            tableGroup15.Name = "group8";
            tableGroup15.ReportItem = this.textBox38;
            tableGroup16.Name = "group7";
            tableGroup16.ReportItem = this.textBox36;
            tableGroup17.Name = "tableGroup4";
            tableGroup17.ReportItem = this.textBox34;
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.ColumnGroups.Add(tableGroup14);
            this.table2.ColumnGroups.Add(tableGroup15);
            this.table2.ColumnGroups.Add(tableGroup16);
            this.table2.ColumnGroups.Add(tableGroup17);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox35,
            this.textBox40,
            this.textBox49,
            this.lblPanDesc,
            this.pictureBox3,
            this.textBox26,
            this.textBox32,
            this.textBox38,
            this.textBox36,
            this.textBox34});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.029325485229492188D), Telerik.Reporting.Drawing.Unit.Inch(5.7086615562438965D));
            this.table2.Name = "table2";
            tableGroup19.Name = "group9";
            tableGroup18.ChildGroups.Add(tableGroup19);
            tableGroup18.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup18.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup18);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2306051254272461D), Telerik.Reporting.Drawing.Unit.Inch(0.90551191568374634D));
            this.table2.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.Color = System.Drawing.Color.White;
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73785006999969482D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox35.Style.Font.Name = "Calibri";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "=Fields.NumberPanels";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8258787393569946D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.textBox40.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox40.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox40.Style.Font.Name = "Calibri";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.Value = "=Fields.PanelBrandName";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D), Telerik.Reporting.Drawing.Unit.Inch(0.47365239262580872D));
            this.textBox49.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox49.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox49.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox49.Style.Font.Name = "Calibri";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox49.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "=Fields.PanelModel";
            // 
            // lblPanDesc
            // 
            this.lblPanDesc.Name = "lblPanDesc";
            this.lblPanDesc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0100903511047363D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.lblPanDesc.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblPanDesc.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.lblPanDesc.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.lblPanDesc.Style.Font.Name = "Calibri";
            this.lblPanDesc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblPanDesc.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.lblPanDesc.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblPanDesc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblPanDesc.Value = "=Fields.Paneldesc";
            // 
            // pictureBox3
            // 
            this.pictureBox3.MimeType = "image/png";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.pictureBox3.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox3.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.pictureBox3.Style.Font.Name = "Calibri";
            this.pictureBox3.Style.LineColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.pictureBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox3.StyleName = "";
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.71738910675048828D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8437259197235107D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.626807451248169D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.9986953735351562D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0231516361236572D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D)));
            this.table4.Body.SetCellContent(0, 0, this.lblqty1);
            this.table4.Body.SetCellContent(0, 1, this.textBox39);
            this.table4.Body.SetCellContent(0, 2, this.textBox71);
            this.table4.Body.SetCellContent(0, 3, this.textBox75);
            this.table4.Body.SetCellContent(0, 4, this.pictureBox4);
            tableGroup20.Name = "tableGroup8";
            tableGroup21.Name = "tableGroup9";
            tableGroup22.Name = "group15";
            tableGroup23.Name = "group16";
            tableGroup24.Name = "tableGroup10";
            this.table4.ColumnGroups.Add(tableGroup20);
            this.table4.ColumnGroups.Add(tableGroup21);
            this.table4.ColumnGroups.Add(tableGroup22);
            this.table4.ColumnGroups.Add(tableGroup23);
            this.table4.ColumnGroups.Add(tableGroup24);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblqty1,
            this.textBox39,
            this.textBox71,
            this.textBox75,
            this.pictureBox4});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.039370059967041016D), Telerik.Reporting.Drawing.Unit.Inch(6.6142520904541016D));
            this.table4.Name = "table4";
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup25.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup25);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2097692489624023D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.table4.Style.Color = System.Drawing.Color.White;
            // 
            // lblqty1
            // 
            this.lblqty1.Name = "lblqty1";
            this.lblqty1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71738874912261963D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.lblqty1.Style.Font.Name = "Calibri";
            this.lblqty1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblqty1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblqty1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblqty1.Value = "=Fields.inverterqty";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8437256813049316D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox39.Style.Font.Name = "Calibri";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "=Fields.InverterDetailsName";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6268069744110107D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox71.Style.Font.Name = "Calibri";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.Value = "=Fields.Invertermodel";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.9986953735351562D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox75.Style.Font.Name = "Calibri";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.Value = "=Fields.Inverterdesc";
            // 
            // pictureBox4
            // 
            this.pictureBox4.MimeType = "image/png";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0231518745422363D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.pictureBox4.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Style.Font.Name = "Calibri";
            this.pictureBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.pictureBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox4.StyleName = "";
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.72739428281784058D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8235548734664917D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6248607635498047D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.0102112293243408D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0137065649032593D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.511810839176178D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0027086734771729D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.68520843982696533D)));
            this.table5.Body.SetCellContent(0, 0, this.lblqty2);
            this.table5.Body.SetCellContent(0, 1, this.textBox73);
            this.table5.Body.SetCellContent(0, 2, this.textBox37);
            this.table5.Body.SetCellContent(0, 3, this.textBox69);
            this.table5.Body.SetCellContent(0, 4, this.pictureBox2);
            this.table5.Body.SetCellContent(1, 0, this.textBox64);
            this.table5.Body.SetCellContent(1, 1, this.textBox65);
            this.table5.Body.SetCellContent(1, 2, this.textBox66);
            this.table5.Body.SetCellContent(1, 3, this.textBox67);
            this.table5.Body.SetCellContent(1, 4, this.textBox68);
            this.table5.Body.SetCellContent(2, 0, this.textBox72);
            this.table5.Body.SetCellContent(2, 1, this.textBox74);
            this.table5.Body.SetCellContent(2, 2, this.textBox76);
            this.table5.Body.SetCellContent(2, 3, this.textBox77);
            this.table5.Body.SetCellContent(2, 4, this.textBox78);
            tableGroup26.Name = "group18";
            tableGroup27.Name = "group17";
            tableGroup28.Name = "tableGroup11";
            tableGroup29.Name = "tableGroup12";
            tableGroup30.Name = "tableGroup13";
            this.table5.ColumnGroups.Add(tableGroup26);
            this.table5.ColumnGroups.Add(tableGroup27);
            this.table5.ColumnGroups.Add(tableGroup28);
            this.table5.ColumnGroups.Add(tableGroup29);
            this.table5.ColumnGroups.Add(tableGroup30);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblqty2,
            this.textBox73,
            this.textBox37,
            this.textBox69,
            this.pictureBox2,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox72,
            this.textBox74,
            this.textBox76,
            this.textBox77,
            this.textBox78});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.039370059967041016D), Telerik.Reporting.Drawing.Unit.Inch(7.0078740119934082D));
            this.table5.Name = "table5";
            tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup31.Name = "detailTableGroup4";
            tableGroup32.Name = "group12";
            tableGroup33.Name = "group13";
            this.table5.RowGroups.Add(tableGroup31);
            this.table5.RowGroups.Add(tableGroup32);
            this.table5.RowGroups.Add(tableGroup33);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.1997280120849609D), Telerik.Reporting.Drawing.Unit.Inch(1.1763451099395752D));
            this.table5.Style.Color = System.Drawing.Color.White;
            // 
            // lblqty2
            // 
            this.lblqty2.Name = "lblqty2";
            this.lblqty2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72739404439926147D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.lblqty2.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblqty2.Style.Font.Name = "Calibri";
            this.lblqty2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblqty2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblqty2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblqty2.Value = "=Fields.inverterqty2";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.823554277420044D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox73.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox73.Style.Font.Name = "Calibri";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.Value = "=Fields.SecondInverterDetails";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248600482940674D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox37.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox37.Style.Font.Name = "Calibri";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "=Fields.SecondInvertermodel";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox69.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox69.Style.Font.Name = "Calibri";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = "=Fields.SecondInverterDetailsdesc";
            // 
            // pictureBox2
            // 
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0137063264846802D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.pictureBox2.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Style.Font.Name = "Calibri";
            this.pictureBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.pictureBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox2.StyleName = "";
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.727394163608551D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox64.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox64.Style.Font.Name = "Calibri";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.StyleName = "";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8235546350479126D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox65.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox65.Style.Font.Name = "Calibri";
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.StyleName = "";
            this.textBox65.Value = "Rail";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248606443405151D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox66.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox66.Style.Font.Name = "Calibri";
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.StyleName = "";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox67.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox67.Style.Font.Name = "Calibri";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.StyleName = "";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0137064456939697D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox68.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox68.Style.Font.Name = "Calibri";
            this.textBox68.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.textBox68.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.StyleName = "";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72739428281784058D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox72.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox72.Style.Font.Name = "Calibri";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8235548734664917D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox74.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox74.Style.Font.Name = "Calibri";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.StyleName = "";
            this.textBox74.Value = "Mounting";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248607635498047D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox76.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox76.Style.Font.Name = "Calibri";
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox77.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox77.Style.Font.Name = "Calibri";
            this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.StyleName = "";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0137065649032593D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox78.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox78.Style.Font.Name = "Calibri";
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.textBox78.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.StyleName = "";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2125983238220215D), Telerik.Reporting.Drawing.Unit.Inch(10.603428840637207D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2653465270996094D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Name = "Calibri";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox60.Value = "Installer Notes:";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.35433068871498108D), Telerik.Reporting.Drawing.Unit.Inch(8.4645671844482422D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3385038375854492D), Telerik.Reporting.Drawing.Unit.Inch(0.19684982299804688D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Calibri";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox33.Value = "Installer Notes:";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(22.69999885559082D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0576328039169312D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Name = "Calibri";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox41.Value = "PICKED BY:";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(23.899999618530273D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59055131673812866D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Name = "Calibri";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox42.Value = "DATE:";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(25.200000762939453D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.550000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox43.Value = "SIGN:";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(21.5D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox44.Style.Font.Name = "Calibri";
            this.textBox44.Value = "_________________________";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5D), Telerik.Reporting.Drawing.Unit.Cm(25.200000762939453D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox47.Style.Font.Name = "Calibri";
            this.textBox47.Value = "_________________________";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(22.674304962158203D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox45.Style.Font.Name = "Calibri";
            this.textBox45.Value = "_________________________";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(23.899999618530273D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox46.Style.Font.Name = "Calibri";
            this.textBox46.Value = "_________________________";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.72645837068557739D), Telerik.Reporting.Drawing.Unit.Cm(27.8856258392334D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.413614273071289D), Telerik.Reporting.Drawing.Unit.Cm(1.1000006198883057D));
            this.textBox48.Style.Font.Name = "Calibri";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox48.Value = "ABN 32168697907 | Phone : 1300 274 737\r\nAddress : Level 6/140 Creek Street, Birsb" +
    "ane QLD 4019 | www.arisesolar.com.au | info@arisesolar.com.au";
            // 
            // textBox61
            // 
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2125983238220215D), Telerik.Reporting.Drawing.Unit.Inch(8.3622865676879883D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8410310745239258D), Telerik.Reporting.Drawing.Unit.Inch(2.149442195892334D));
            this.textBox61.Style.Font.Bold = false;
            this.textBox61.Style.Font.Name = "Calibri";
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox61.Value = "=Fields.InstallerNotes";
            // 
            // txrintadd1
            // 
            this.txrintadd1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.16770859062671661D), Telerik.Reporting.Drawing.Unit.Cm(12.773124694824219D));
            this.txrintadd1.Name = "txrintadd1";
            this.txrintadd1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.911041259765625D), Telerik.Reporting.Drawing.Unit.Cm(0.65291672945022583D));
            this.txrintadd1.Style.Font.Bold = true;
            this.txrintadd1.Style.Font.Name = "Calibri";
            this.txrintadd1.Value = "";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.7002010345459D));
            this.pictureBox5.MimeType = "image/jpeg";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.pictureBox5.Value = ((object)(resources.GetObject("pictureBox5.Value")));
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1767923831939697D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26458343863487244D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5000001192092896D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1199996471405029D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4199999570846558D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2399998903274536D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4199999570846558D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.37118059396743774D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox79);
            this.table3.Body.SetCellContent(0, 2, this.textBox80);
            this.table3.Body.SetCellContent(3, 0, this.textBox81);
            this.table3.Body.SetCellContent(3, 2, this.textBox82);
            this.table3.Body.SetCellContent(2, 0, this.textBox83);
            this.table3.Body.SetCellContent(2, 2, this.textBox84);
            this.table3.Body.SetCellContent(1, 0, this.textBox85);
            this.table3.Body.SetCellContent(1, 2, this.textBox86);
            this.table3.Body.SetCellContent(0, 6, this.textBox87);
            this.table3.Body.SetCellContent(1, 6, this.textBox88);
            this.table3.Body.SetCellContent(3, 6, this.textBox89);
            this.table3.Body.SetCellContent(0, 3, this.textBox90);
            this.table3.Body.SetCellContent(1, 3, this.textBox91);
            this.table3.Body.SetCellContent(2, 3, this.textBox92);
            this.table3.Body.SetCellContent(3, 3, this.textBox93);
            this.table3.Body.SetCellContent(0, 5, this.textBox94);
            this.table3.Body.SetCellContent(1, 5, this.textBox95);
            this.table3.Body.SetCellContent(2, 5, this.textBox96);
            this.table3.Body.SetCellContent(3, 5, this.textBox97);
            this.table3.Body.SetCellContent(0, 7, this.textBox98);
            this.table3.Body.SetCellContent(1, 7, this.textBox99);
            this.table3.Body.SetCellContent(3, 7, this.textBox100);
            this.table3.Body.SetCellContent(2, 6, this.textBox101);
            this.table3.Body.SetCellContent(2, 7, this.textBox102);
            this.table3.Body.SetCellContent(0, 1, this.textBox103);
            this.table3.Body.SetCellContent(1, 1, this.textBox104);
            this.table3.Body.SetCellContent(2, 1, this.textBox105);
            this.table3.Body.SetCellContent(3, 1, this.textBox106);
            this.table3.Body.SetCellContent(0, 4, this.textBox107);
            this.table3.Body.SetCellContent(1, 4, this.textBox108);
            this.table3.Body.SetCellContent(2, 4, this.textBox109);
            this.table3.Body.SetCellContent(3, 4, this.textBox110);
            tableGroup34.Name = "tableGroup";
            tableGroup34.ReportItem = this.textBox111;
            tableGroup35.Name = "group10";
            tableGroup35.ReportItem = this.textBox112;
            tableGroup36.Name = "tableGroup1";
            tableGroup36.ReportItem = this.textBox113;
            tableGroup37.Name = "group4";
            tableGroup37.ReportItem = this.textBox114;
            tableGroup38.Name = "group11";
            tableGroup38.ReportItem = this.textBox115;
            tableGroup39.Name = "group5";
            tableGroup39.ReportItem = this.textBox116;
            tableGroup40.Name = "group3";
            tableGroup40.ReportItem = this.textBox117;
            tableGroup41.Name = "group6";
            tableGroup41.ReportItem = this.textBox118;
            this.table3.ColumnGroups.Add(tableGroup34);
            this.table3.ColumnGroups.Add(tableGroup35);
            this.table3.ColumnGroups.Add(tableGroup36);
            this.table3.ColumnGroups.Add(tableGroup37);
            this.table3.ColumnGroups.Add(tableGroup38);
            this.table3.ColumnGroups.Add(tableGroup39);
            this.table3.ColumnGroups.Add(tableGroup40);
            this.table3.ColumnGroups.Add(tableGroup41);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox79,
            this.textBox103,
            this.textBox80,
            this.textBox90,
            this.textBox107,
            this.textBox94,
            this.textBox87,
            this.textBox98,
            this.textBox85,
            this.textBox104,
            this.textBox86,
            this.textBox91,
            this.textBox108,
            this.textBox95,
            this.textBox88,
            this.textBox99,
            this.textBox83,
            this.textBox105,
            this.textBox84,
            this.textBox92,
            this.textBox109,
            this.textBox96,
            this.textBox101,
            this.textBox102,
            this.textBox81,
            this.textBox106,
            this.textBox82,
            this.textBox93,
            this.textBox110,
            this.textBox97,
            this.textBox89,
            this.textBox100,
            this.textBox111,
            this.textBox112,
            this.textBox113,
            this.textBox114,
            this.textBox115,
            this.textBox116,
            this.textBox117,
            this.textBox118});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.09375D), Telerik.Reporting.Drawing.Unit.Inch(14.253196716308594D));
            this.table3.Name = "table3";
            tableGroup42.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup42.Name = "detailTableGroup";
            tableGroup43.Name = "group2";
            tableGroup44.Name = "group1";
            tableGroup45.Name = "group";
            this.table3.RowGroups.Add(tableGroup42);
            this.table3.RowGroups.Add(tableGroup43);
            this.table3.RowGroups.Add(tableGroup44);
            this.table3.RowGroups.Add(tableGroup45);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.140681266784668D), Telerik.Reporting.Drawing.Unit.Inch(1.9522640705108643D));
            this.table3.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.Color = System.Drawing.Color.White;
            this.table3.Style.Font.Bold = false;
            this.table3.Style.Font.Name = "Calibri";
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.table3.Style.LineColor = System.Drawing.Color.Transparent;
            this.table3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1767926216125488D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox79.Style.Font.Bold = true;
            this.textBox79.Style.Font.Name = "Calibri";
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox79.Value = "Customer Name\t";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox80.Style.Font.Name = "Calibri";
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox80.Value = "=Fields.Customer";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1767926216125488D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox81.Style.Font.Bold = true;
            this.textBox81.Style.Font.Name = "Calibri";
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox81.StyleName = "";
            this.textBox81.Value = "Store";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox82.Style.Font.Name = "Calibri";
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox82.StyleName = "";
            this.textBox82.Value = "=Fields.StoreName";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1767926216125488D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Name = "Calibri";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox83.StyleName = "";
            this.textBox83.Value = "Manual Quote";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox84.Style.Font.Name = "Calibri";
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox84.StyleName = "";
            this.textBox84.Value = "=Fields.ManualQuoteNumber";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1767926216125488D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox85.Style.Font.Bold = true;
            this.textBox85.Style.Font.Name = "Calibri";
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox85.StyleName = "";
            this.textBox85.Value = "House Type";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox86.Style.Font.Name = "Calibri";
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox86.StyleName = "";
            this.textBox86.Value = "=Fields.HouseType";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2400000095367432D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.Font.Name = "Calibri";
            this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox87.StyleName = "";
            this.textBox87.Value = "Mob:";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2400000095367432D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox88.Style.Font.Bold = true;
            this.textBox88.Style.Font.Name = "Calibri";
            this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox88.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox88.StyleName = "";
            this.textBox88.Value = "Angle:";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2400000095367432D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox89.Style.Font.Name = "Calibri";
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox89.StyleName = "";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.119999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Name = "Calibri";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox90.StyleName = "";
            this.textBox90.Value = "Phone";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.119999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox91.Style.Font.Bold = true;
            this.textBox91.Style.Font.Name = "Calibri";
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox91.StyleName = "";
            this.textBox91.Value = "Roof Type";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.119999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox92.Style.Font.Bold = true;
            this.textBox92.Style.Font.Name = "Calibri";
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox92.StyleName = "";
            this.textBox92.Value = "Project No.";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.119999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox93.Style.Font.Name = "Calibri";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox93.StyleName = "";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox94.Style.Font.Name = "Calibri";
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox94.StyleName = "";
            this.textBox94.Value = "=Fields.CustPhone";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox95.Style.Font.Name = "Calibri";
            this.textBox95.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox95.StyleName = "";
            this.textBox95.Value = "=Fields.RoofType";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox96.Style.Font.Name = "Calibri";
            this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox96.StyleName = "";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox97.Style.Font.Name = "Calibri";
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox97.StyleName = "";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox98.Style.Font.Name = "Calibri";
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox98.StyleName = "";
            this.textBox98.Value = "=Fields.ContMobile";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox99.Style.Font.Name = "Calibri";
            this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox99.StyleName = "";
            this.textBox99.Value = "=Fields.RoofAngle";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox100.StyleName = "";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2400000095367432D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox101.Style.Font.Bold = true;
            this.textBox101.Style.Font.Name = "Calibri";
            this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox101.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox101.StyleName = "";
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox102.Style.Font.Bold = true;
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox102.StyleName = "";
            this.textBox102.Value = "";
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox103.Style.Font.Bold = true;
            this.textBox103.Style.Font.Name = "Calibri";
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox103.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox103.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox103.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox103.StyleName = "";
            this.textBox103.Value = ":";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox104.Style.Font.Bold = true;
            this.textBox104.Style.Font.Name = "Calibri";
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox104.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox104.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox104.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox104.StyleName = "";
            this.textBox104.Value = ":";
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox105.Style.Font.Bold = true;
            this.textBox105.Style.Font.Name = "Calibri";
            this.textBox105.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox105.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox105.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox105.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox105.StyleName = "";
            this.textBox105.Value = ":";
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox106.Style.Font.Bold = true;
            this.textBox106.Style.Font.Name = "Calibri";
            this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox106.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox106.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox106.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox106.StyleName = "";
            this.textBox106.Value = ":";
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.37595841288566589D));
            this.textBox107.Style.Font.Bold = true;
            this.textBox107.Style.Font.Name = "Calibri";
            this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox107.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox107.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox107.StyleName = "";
            this.textBox107.Value = ":";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.38159716129302979D));
            this.textBox108.Style.Font.Bold = true;
            this.textBox108.Style.Font.Name = "Calibri";
            this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox108.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox108.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox108.StyleName = "";
            this.textBox108.Value = ":";
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.39201393723487854D));
            this.textBox109.Style.Font.Bold = true;
            this.textBox109.Style.Font.Name = "Calibri";
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox109.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox109.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox109.StyleName = "";
            this.textBox109.Value = ":";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox110.Style.Font.Bold = true;
            this.textBox110.Style.Font.Name = "Calibri";
            this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox110.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox110.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox110.StyleName = "";
            // 
            // textBox121
            // 
            this.textBox121.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.38638749718666077D), Telerik.Reporting.Drawing.Unit.Cm(41.6270751953125D));
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox121.Style.Color = System.Drawing.Color.Black;
            this.textBox121.Style.Font.Bold = true;
            this.textBox121.Style.Font.Name = "Calibri";
            this.textBox121.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox121.Value = "SITE ADDRESS :";
            // 
            // txrintadd1_1
            // 
            this.txrintadd1_1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.25409603118896484D), Telerik.Reporting.Drawing.Unit.Cm(42.502288818359375D));
            this.txrintadd1_1.Name = "txrintadd1_1";
            this.txrintadd1_1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.158143043518066D), Telerik.Reporting.Drawing.Unit.Cm(0.56916660070419312D));
            this.txrintadd1_1.Style.Font.Bold = true;
            this.txrintadd1_1.Style.Font.Name = "Calibri";
            this.txrintadd1_1.Value = "";
            // 
            // textBox122
            // 
            this.textBox122.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2125978469848633D), Telerik.Reporting.Drawing.Unit.Inch(20.044862747192383D));
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8410310745239258D), Telerik.Reporting.Drawing.Unit.Inch(2.149442195892334D));
            this.textBox122.Style.Font.Bold = false;
            this.textBox122.Style.Font.Name = "Calibri";
            this.textBox122.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox122.Value = "=Fields.InstallerNotes";
            // 
            // textBox123
            // 
            this.textBox123.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.72645759582519531D), Telerik.Reporting.Drawing.Unit.Cm(57.559364318847656D));
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.413614273071289D), Telerik.Reporting.Drawing.Unit.Cm(1.1000006198883057D));
            this.textBox123.Style.Font.Name = "Calibri";
            this.textBox123.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox123.Value = "ABN 32168697907 | Phone : 1300 274 737\r\nAddress : Level 6/140 Creek Street, Birsb" +
    "ane QLD 4019 | www.arisesolar.com.au | info@arisesolar.com.au";
            // 
            // textBox124
            // 
            this.textBox124.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999987125396729D), Telerik.Reporting.Drawing.Unit.Cm(53.573745727539062D));
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox124.Style.Font.Name = "Calibri";
            this.textBox124.Value = "_________________________";
            // 
            // textBox125
            // 
            this.textBox125.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.7999989986419678D), Telerik.Reporting.Drawing.Unit.Cm(52.348049163818359D));
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox125.Style.Font.Name = "Calibri";
            this.textBox125.Value = "_________________________";
            // 
            // textBox126
            // 
            this.textBox126.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4999990463256836D), Telerik.Reporting.Drawing.Unit.Cm(54.873741149902344D));
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox126.Style.Font.Name = "Calibri";
            this.textBox126.Value = "_________________________";
            // 
            // textBox127
            // 
            this.textBox127.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999992370605469D), Telerik.Reporting.Drawing.Unit.Cm(51.173740386962891D));
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox127.Style.Font.Name = "Calibri";
            this.textBox127.Value = "_________________________";
            // 
            // textBox128
            // 
            this.textBox128.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999830722808838D), Telerik.Reporting.Drawing.Unit.Cm(54.873741149902344D));
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.550000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox128.Style.Font.Bold = true;
            this.textBox128.Style.Font.Name = "Calibri";
            this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox128.Value = "SIGN:";
            // 
            // textBox129
            // 
            this.textBox129.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999830722808838D), Telerik.Reporting.Drawing.Unit.Cm(53.573745727539062D));
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59055131673812866D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox129.Style.Font.Bold = true;
            this.textBox129.Style.Font.Name = "Calibri";
            this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox129.Value = "DATE:";
            // 
            // textBox130
            // 
            this.textBox130.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999830722808838D), Telerik.Reporting.Drawing.Unit.Cm(52.373741149902344D));
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0576328039169312D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox130.Style.Font.Bold = true;
            this.textBox130.Style.Font.Name = "Calibri";
            this.textBox130.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox130.Value = "PICKED BY:";
            // 
            // textBox131
            // 
            this.textBox131.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.35433006286621094D), Telerik.Reporting.Drawing.Unit.Inch(20.14714241027832D));
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3385038375854492D), Telerik.Reporting.Drawing.Unit.Inch(0.19684982299804688D));
            this.textBox131.Style.Font.Bold = true;
            this.textBox131.Style.Font.Name = "Calibri";
            this.textBox131.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox131.Value = "Installer Notes:";
            // 
            // textBox132
            // 
            this.textBox132.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2125978469848633D), Telerik.Reporting.Drawing.Unit.Inch(22.2860050201416D));
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2653465270996094D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox132.Style.Font.Bold = true;
            this.textBox132.Style.Font.Name = "Calibri";
            this.textBox132.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox132.Value = "Installer Notes:";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.72739428281784058D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8235547542572022D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6248607635498047D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0449564456939697D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.511810839176178D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0027086734771729D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.68520843982696533D)));
            this.table6.Body.SetCellContent(0, 0, this.lblqty21);
            this.table6.Body.SetCellContent(0, 1, this.textBox134);
            this.table6.Body.SetCellContent(0, 2, this.textBox135);
            this.table6.Body.SetCellContent(0, 3, this.textBox136);
            this.table6.Body.SetCellContent(0, 4, this.pictureBox6);
            this.table6.Body.SetCellContent(1, 0, this.textBox137);
            this.table6.Body.SetCellContent(1, 1, this.textBox138);
            this.table6.Body.SetCellContent(1, 2, this.textBox139);
            this.table6.Body.SetCellContent(1, 3, this.textBox140);
            this.table6.Body.SetCellContent(1, 4, this.textBox141);
            this.table6.Body.SetCellContent(2, 0, this.textBox142);
            this.table6.Body.SetCellContent(2, 1, this.textBox143);
            this.table6.Body.SetCellContent(2, 2, this.textBox144);
            this.table6.Body.SetCellContent(2, 3, this.textBox145);
            this.table6.Body.SetCellContent(2, 4, this.textBox146);
            tableGroup46.Name = "group18";
            tableGroup47.Name = "group17";
            tableGroup48.Name = "tableGroup11";
            tableGroup49.Name = "tableGroup12";
            tableGroup50.Name = "tableGroup13";
            this.table6.ColumnGroups.Add(tableGroup46);
            this.table6.ColumnGroups.Add(tableGroup47);
            this.table6.ColumnGroups.Add(tableGroup48);
            this.table6.ColumnGroups.Add(tableGroup49);
            this.table6.ColumnGroups.Add(tableGroup50);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblqty21,
            this.textBox134,
            this.textBox135,
            this.textBox136,
            this.pictureBox6,
            this.textBox137,
            this.textBox138,
            this.textBox139,
            this.textBox140,
            this.textBox141,
            this.textBox142,
            this.textBox143,
            this.textBox144,
            this.textBox145,
            this.textBox146});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.029325485229492188D), Telerik.Reporting.Drawing.Unit.Inch(18.701112747192383D));
            this.table6.Name = "table6";
            tableGroup51.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup51.Name = "detailTableGroup4";
            tableGroup52.Name = "group12";
            tableGroup53.Name = "group13";
            this.table6.RowGroups.Add(tableGroup51);
            this.table6.RowGroups.Add(tableGroup52);
            this.table6.RowGroups.Add(tableGroup53);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2309780120849609D), Telerik.Reporting.Drawing.Unit.Inch(1.1763451099395752D));
            this.table6.Style.Color = System.Drawing.Color.White;
            // 
            // lblqty21
            // 
            this.lblqty21.Name = "lblqty21";
            this.lblqty21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72739404439926147D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.lblqty21.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblqty21.Style.Font.Name = "Calibri";
            this.lblqty21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblqty21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblqty21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblqty21.Value = "=Fields.inverterqty2";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.823554277420044D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox134.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox134.Style.Font.Name = "Calibri";
            this.textBox134.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox134.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox134.Value = "=Fields.SecondInverterDetails";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248600482940674D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox135.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox135.Style.Font.Name = "Calibri";
            this.textBox135.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox135.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox135.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox135.Value = "=Fields.SecondInvertermodel";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox136.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox136.Style.Font.Name = "Calibri";
            this.textBox136.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox136.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox136.Value = "=Fields.SecondInverterDetailsdesc";
            // 
            // pictureBox6
            // 
            this.pictureBox6.MimeType = "image/png";
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0449564456939697D), Telerik.Reporting.Drawing.Unit.Inch(0.511810839176178D));
            this.pictureBox6.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Style.Font.Name = "Calibri";
            this.pictureBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.pictureBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox6.StyleName = "";
            this.pictureBox6.Value = ((object)(resources.GetObject("pictureBox6.Value")));
            // 
            // textBox137
            // 
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.727394163608551D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox137.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox137.Style.Font.Name = "Calibri";
            this.textBox137.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox137.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox137.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox137.StyleName = "";
            // 
            // textBox138
            // 
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8235546350479126D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox138.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox138.Style.Font.Name = "Calibri";
            this.textBox138.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox138.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox138.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox138.StyleName = "";
            this.textBox138.Value = "Rail";
            // 
            // textBox139
            // 
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248606443405151D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox139.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox139.Style.Font.Name = "Calibri";
            this.textBox139.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox139.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox139.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox139.StyleName = "";
            // 
            // textBox140
            // 
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox140.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox140.Style.Font.Name = "Calibri";
            this.textBox140.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox140.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox140.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox140.StyleName = "";
            // 
            // textBox141
            // 
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0449564456939697D), Telerik.Reporting.Drawing.Unit.Cm(1.0027086734771729D));
            this.textBox141.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox141.Style.Font.Name = "Calibri";
            this.textBox141.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.textBox141.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.textBox141.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox141.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox141.StyleName = "";
            // 
            // textBox142
            // 
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72739428281784058D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox142.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox142.Style.Font.Name = "Calibri";
            this.textBox142.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox142.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox142.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox142.StyleName = "";
            // 
            // textBox143
            // 
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8235548734664917D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox143.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox143.Style.Font.Name = "Calibri";
            this.textBox143.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox143.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.StyleName = "";
            this.textBox143.Value = "Mounting";
            // 
            // textBox144
            // 
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248607635498047D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox144.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox144.Style.Font.Name = "Calibri";
            this.textBox144.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox144.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox144.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox144.StyleName = "";
            // 
            // textBox145
            // 
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox145.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox145.Style.Font.Name = "Calibri";
            this.textBox145.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox145.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox145.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox145.StyleName = "";
            // 
            // textBox146
            // 
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0449564456939697D), Telerik.Reporting.Drawing.Unit.Cm(0.68520849943161011D));
            this.textBox146.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox146.Style.Font.Name = "Calibri";
            this.textBox146.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.textBox146.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.textBox146.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox146.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox146.StyleName = "";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.71738898754119873D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8437256813049316D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6268073320388794D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.019528865814209D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0231516361236572D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D)));
            this.table7.Body.SetCellContent(0, 0, this.lblqty11);
            this.table7.Body.SetCellContent(0, 1, this.textBox148);
            this.table7.Body.SetCellContent(0, 2, this.textBox149);
            this.table7.Body.SetCellContent(0, 3, this.textBox150);
            this.table7.Body.SetCellContent(0, 4, this.pictureBox7);
            tableGroup54.Name = "tableGroup8";
            tableGroup55.Name = "tableGroup9";
            tableGroup56.Name = "group15";
            tableGroup57.Name = "group16";
            tableGroup58.Name = "tableGroup10";
            this.table7.ColumnGroups.Add(tableGroup54);
            this.table7.ColumnGroups.Add(tableGroup55);
            this.table7.ColumnGroups.Add(tableGroup56);
            this.table7.ColumnGroups.Add(tableGroup57);
            this.table7.ColumnGroups.Add(tableGroup58);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblqty11,
            this.textBox148,
            this.textBox149,
            this.textBox150,
            this.pictureBox7});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(18.307413101196289D));
            this.table7.Name = "table7";
            tableGroup59.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup59.Name = "detailTableGroup3";
            this.table7.RowGroups.Add(tableGroup59);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2306022644042969D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.table7.Style.Color = System.Drawing.Color.White;
            // 
            // lblqty11
            // 
            this.lblqty11.Name = "lblqty11";
            this.lblqty11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71738874912261963D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.lblqty11.Style.Font.Name = "Calibri";
            this.lblqty11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblqty11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblqty11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblqty11.Value = "=Fields.inverterqty";
            // 
            // textBox148
            // 
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8437256813049316D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox148.Style.Font.Name = "Calibri";
            this.textBox148.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox148.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox148.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox148.Value = "=Fields.InverterDetailsName";
            // 
            // textBox149
            // 
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6268069744110107D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox149.Style.Font.Name = "Calibri";
            this.textBox149.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox149.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox149.Value = "=Fields.Invertermodel";
            // 
            // textBox150
            // 
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.019528865814209D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox150.Style.Font.Name = "Calibri";
            this.textBox150.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox150.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox150.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox150.Value = "=Fields.Inverterdesc";
            // 
            // pictureBox7
            // 
            this.pictureBox7.MimeType = "image/png";
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0231518745422363D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.pictureBox7.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox7.Style.Font.Name = "Calibri";
            this.pictureBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.pictureBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox7.StyleName = "";
            this.pictureBox7.Value = ((object)(resources.GetObject("pictureBox7.Value")));
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.73785030841827393D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.825878381729126D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.0100908279418945D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox151);
            this.table8.Body.SetCellContent(0, 1, this.textBox152);
            this.table8.Body.SetCellContent(0, 2, this.textBox153);
            this.table8.Body.SetCellContent(0, 3, this.lblPanDesc1);
            this.table8.Body.SetCellContent(0, 4, this.pictureBox8);
            tableGroup60.Name = "tableGroup2";
            tableGroup60.ReportItem = this.textBox155;
            tableGroup61.Name = "tableGroup3";
            tableGroup61.ReportItem = this.textBox156;
            tableGroup62.Name = "group8";
            tableGroup62.ReportItem = this.textBox157;
            tableGroup63.Name = "group7";
            tableGroup63.ReportItem = this.textBox158;
            tableGroup64.Name = "tableGroup4";
            tableGroup64.ReportItem = this.textBox159;
            this.table8.ColumnGroups.Add(tableGroup60);
            this.table8.ColumnGroups.Add(tableGroup61);
            this.table8.ColumnGroups.Add(tableGroup62);
            this.table8.ColumnGroups.Add(tableGroup63);
            this.table8.ColumnGroups.Add(tableGroup64);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox151,
            this.textBox152,
            this.textBox153,
            this.lblPanDesc1,
            this.pictureBox8,
            this.textBox155,
            this.textBox156,
            this.textBox157,
            this.textBox158,
            this.textBox159});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.00849334429949522D), Telerik.Reporting.Drawing.Unit.Inch(17.401821136474609D));
            this.table8.Name = "table8";
            tableGroup66.Name = "group9";
            tableGroup65.ChildGroups.Add(tableGroup66);
            tableGroup65.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup65.Name = "detailTableGroup1";
            this.table8.RowGroups.Add(tableGroup65);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2306051254272461D), Telerik.Reporting.Drawing.Unit.Inch(0.90551191568374634D));
            this.table8.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.table8.Style.Color = System.Drawing.Color.White;
            // 
            // textBox151
            // 
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73785006999969482D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.textBox151.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox151.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox151.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox151.Style.Font.Name = "Calibri";
            this.textBox151.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox151.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox151.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox151.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox151.Value = "=Fields.NumberPanels";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8258787393569946D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.textBox152.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox152.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox152.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox152.Style.Font.Name = "Calibri";
            this.textBox152.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox152.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox152.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox152.Value = "=Fields.PanelBrandName";
            // 
            // textBox153
            // 
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D), Telerik.Reporting.Drawing.Unit.Inch(0.47365239262580872D));
            this.textBox153.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox153.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox153.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox153.Style.Font.Name = "Calibri";
            this.textBox153.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox153.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox153.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox153.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox153.Value = "=Fields.PanelModel";
            // 
            // lblPanDesc1
            // 
            this.lblPanDesc1.Name = "lblPanDesc1";
            this.lblPanDesc1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0100908279418945D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.lblPanDesc1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblPanDesc1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.lblPanDesc1.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.lblPanDesc1.Style.Font.Name = "Calibri";
            this.lblPanDesc1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblPanDesc1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.lblPanDesc1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblPanDesc1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblPanDesc1.Value = "=Fields.Paneldesc";
            // 
            // pictureBox8
            // 
            this.pictureBox8.MimeType = "image/png";
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.pictureBox8.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox8.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.pictureBox8.Style.Font.Name = "Calibri";
            this.pictureBox8.Style.LineColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.pictureBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox8.StyleName = "";
            this.pictureBox8.Value = ((object)(resources.GetObject("pictureBox8.Value")));
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.514737129211426D), Telerik.Reporting.Drawing.Unit.Cm(35.030834197998047D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Name = "Calibri";
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox62.Value = "RETURN";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.38638749718666077D), Telerik.Reporting.Drawing.Unit.Cm(43.202377319335938D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.158143043518066D), Telerik.Reporting.Drawing.Unit.Cm(0.56916660070419312D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.Font.Name = "Calibri";
            this.textBox63.Value = "";
            // 
            // textBox119
            // 
            this.textBox119.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Cm(13.599582672119141D));
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.911041259765625D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox119.Style.Font.Bold = true;
            this.textBox119.Style.Font.Name = "Calibri";
            this.textBox119.Value = "textBox119";
            // 
            // Npicklist_new
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "picklist";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210.38018798828125D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox lblPanDesc;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox lblqty1;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox lblqty2;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox txrintadd1;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox txrintadd1_1;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox123;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox lblqty21;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox138;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox lblqty11;
        private Telerik.Reporting.TextBox textBox148;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox lblPanDesc1;
        private Telerik.Reporting.PictureBox pictureBox8;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox158;
        private Telerik.Reporting.TextBox textBox159;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox119;
    }
}