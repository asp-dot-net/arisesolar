namespace EurosolarReporting
{
    partial class customeracknowledgement
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(customeracknowledgement));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.textBox172 = new Telerik.Reporting.TextBox();
            this.textBox175 = new Telerik.Reporting.TextBox();
            this.textBox170 = new Telerik.Reporting.TextBox();
            this.txtdate = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.table14 = new Telerik.Reporting.Table();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox177 = new Telerik.Reporting.TextBox();
            this.textBox179 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox178 = new Telerik.Reporting.TextBox();
            this.textBox180 = new Telerik.Reporting.TextBox();
            this.textBox181 = new Telerik.Reporting.TextBox();
            this.textBox182 = new Telerik.Reporting.TextBox();
            this.textBox183 = new Telerik.Reporting.TextBox();
            this.textBox184 = new Telerik.Reporting.TextBox();
            this.textBox185 = new Telerik.Reporting.TextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox187 = new Telerik.Reporting.TextBox();
            this.textBox188 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox193 = new Telerik.Reporting.TextBox();
            this.textBox194 = new Telerik.Reporting.TextBox();
            this.textBox195 = new Telerik.Reporting.TextBox();
            this.textBox196 = new Telerik.Reporting.TextBox();
            this.textBox197 = new Telerik.Reporting.TextBox();
            this.textBox198 = new Telerik.Reporting.TextBox();
            this.textBox199 = new Telerik.Reporting.TextBox();
            this.textBox200 = new Telerik.Reporting.TextBox();
            this.textBox215 = new Telerik.Reporting.TextBox();
            this.textBox210 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox202 = new Telerik.Reporting.TextBox();
            this.textBox201 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox168
            // 
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.32187557220459D), Telerik.Reporting.Drawing.Unit.Cm(0.58204621076583862D));
            this.textBox168.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox168.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox168.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox168.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox168.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox168.Style.Font.Bold = true;
            this.textBox168.Style.Font.Name = "Arial";
            this.textBox168.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox168.Value = "Customer Acknowledgement";
            // 
            // textBox172
            // 
            this.textBox172.Name = "textBox172";
            this.textBox172.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2835731506347656D), Telerik.Reporting.Drawing.Unit.Cm(0.58204621076583862D));
            this.textBox172.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox172.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox172.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox172.Style.Font.Bold = true;
            this.textBox172.Style.Font.Name = "Arial";
            this.textBox172.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox172.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox172.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox172.Value = "Customer No : ";
            // 
            // textBox175
            // 
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3845508098602295D), Telerik.Reporting.Drawing.Unit.Cm(0.58204621076583862D));
            this.textBox175.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox175.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox175.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox175.Style.Font.Bold = true;
            this.textBox175.Style.Font.Name = "Arial";
            this.textBox175.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox175.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox175.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox175.StyleName = "";
            this.textBox175.Value = "=Fields.ProjectNumber";
            // 
            // textBox170
            // 
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989992141723633D), Telerik.Reporting.Drawing.Unit.Cm(0.608585774898529D));
            this.textBox170.Style.BorderColor.Left = System.Drawing.Color.White;
            this.textBox170.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox170.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox170.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox170.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox170.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox170.Style.Font.Name = "Arial";
            this.textBox170.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox170.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox170.Value = "QUALITY OF SERVICE (1=POOR, 2=AVERAGE, 3=GOOD, 4=VERY GOOD)";
            // 
            // txtdate
            // 
            this.txtdate.Height = Telerik.Reporting.Drawing.Unit.Inch(10.511811256408691D);
            this.txtdate.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1,
            this.table1,
            this.table14});
            this.txtdate.Name = "txtdate";
            this.txtdate.Style.Font.Name = "Open Sans";
            this.txtdate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox5,
            this.pictureBox2,
            this.textBox2});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.3263990429950354E-09D), Telerik.Reporting.Drawing.Unit.Cm(-4.3138864924685549E-08D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(2.0999999046325684D));
            this.panel1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.58096981048584D), Telerik.Reporting.Drawing.Unit.Cm(-1.0093052793536117E-07D));
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.htmlTextBox5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox5.Value = resources.GetString("htmlTextBox5.Value");
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.088541708886623383D), Telerik.Reporting.Drawing.Unit.Cm(0.1081332340836525D));
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Style.Font.Name = "Open Sans";
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(12.321876525878906D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2835726737976074D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3845508098602295D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(10.34999942779541D)));
            this.table1.Body.SetCellContent(0, 0, this.htmlTextBox6, 1, 3);
            tableGroup1.Name = "tableGroup32";
            tableGroup1.ReportItem = this.textBox168;
            tableGroup2.Name = "tableGroup33";
            tableGroup2.ReportItem = this.textBox172;
            tableGroup3.Name = "tableGroup34";
            tableGroup3.ReportItem = this.textBox175;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox6,
            this.textBox168,
            this.textBox172,
            this.textBox175});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.3263990429950354E-09D), Telerik.Reporting.Drawing.Unit.Cm(2.100200891494751D));
            this.table1.Name = "table1";
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "detailTableGroup12";
            this.table1.RowGroups.Add(tableGroup4);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(10.932045936584473D));
            this.table1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(10.34999942779541D));
            this.htmlTextBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.htmlTextBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox6.Style.Font.Name = "Microsoft Sans Serif";
            this.htmlTextBox6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.htmlTextBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.htmlTextBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.htmlTextBox6.StyleName = "";
            this.htmlTextBox6.Value = resources.GetString("htmlTextBox6.Value");
            // 
            // table14
            // 
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(10.019034385681152D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6432504653930664D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2363958358764648D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1041038036346436D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9872095584869385D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52920562028884888D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0292410850524902D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.57941699028015137D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(6.4536705017089844D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.81753653287887573D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4261659383773804D)));
            this.table14.Body.SetCellContent(0, 0, this.textBox174);
            this.table14.Body.SetCellContent(0, 1, this.textBox177);
            this.table14.Body.SetCellContent(0, 4, this.textBox179);
            this.table14.Body.SetCellContent(0, 3, this.textBox176);
            this.table14.Body.SetCellContent(0, 2, this.textBox178);
            this.table14.Body.SetCellContent(4, 0, this.textBox180);
            this.table14.Body.SetCellContent(4, 1, this.textBox181);
            this.table14.Body.SetCellContent(4, 2, this.textBox182);
            this.table14.Body.SetCellContent(4, 3, this.textBox183);
            this.table14.Body.SetCellContent(4, 4, this.textBox184);
            this.table14.Body.SetCellContent(3, 0, this.textBox185);
            this.table14.Body.SetCellContent(3, 1, this.textBox186);
            this.table14.Body.SetCellContent(3, 2, this.textBox187);
            this.table14.Body.SetCellContent(3, 3, this.textBox188);
            this.table14.Body.SetCellContent(3, 4, this.textBox189);
            this.table14.Body.SetCellContent(2, 0, this.textBox190);
            this.table14.Body.SetCellContent(2, 1, this.textBox191);
            this.table14.Body.SetCellContent(2, 2, this.textBox192);
            this.table14.Body.SetCellContent(2, 3, this.textBox193);
            this.table14.Body.SetCellContent(2, 4, this.textBox194);
            this.table14.Body.SetCellContent(1, 0, this.textBox195);
            this.table14.Body.SetCellContent(1, 1, this.textBox196);
            this.table14.Body.SetCellContent(1, 2, this.textBox197);
            this.table14.Body.SetCellContent(1, 3, this.textBox198);
            this.table14.Body.SetCellContent(1, 4, this.textBox199);
            this.table14.Body.SetCellContent(9, 0, this.textBox200);
            this.table14.Body.SetCellContent(6, 0, this.textBox215, 1, 5);
            this.table14.Body.SetCellContent(7, 0, this.textBox210, 1, 5);
            this.table14.Body.SetCellContent(8, 0, this.textBox205, 1, 5);
            this.table14.Body.SetCellContent(9, 3, this.textBox203, 1, 2);
            this.table14.Body.SetCellContent(9, 1, this.textBox202, 1, 2);
            this.table14.Body.SetCellContent(5, 0, this.textBox201, 1, 5);
            tableGroup6.Name = "group93";
            tableGroup7.Name = "tableGroup36";
            tableGroup8.Name = "group95";
            tableGroup9.Name = "group94";
            tableGroup10.Name = "tableGroup37";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ChildGroups.Add(tableGroup7);
            tableGroup5.ChildGroups.Add(tableGroup8);
            tableGroup5.ChildGroups.Add(tableGroup9);
            tableGroup5.ChildGroups.Add(tableGroup10);
            tableGroup5.Name = "tableGroup35";
            tableGroup5.ReportItem = this.textBox170;
            this.table14.ColumnGroups.Add(tableGroup5);
            this.table14.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox174,
            this.textBox177,
            this.textBox178,
            this.textBox176,
            this.textBox179,
            this.textBox195,
            this.textBox196,
            this.textBox197,
            this.textBox198,
            this.textBox199,
            this.textBox190,
            this.textBox191,
            this.textBox192,
            this.textBox193,
            this.textBox194,
            this.textBox185,
            this.textBox186,
            this.textBox187,
            this.textBox188,
            this.textBox189,
            this.textBox180,
            this.textBox181,
            this.textBox182,
            this.textBox183,
            this.textBox184,
            this.textBox201,
            this.textBox215,
            this.textBox210,
            this.textBox205,
            this.textBox200,
            this.textBox202,
            this.textBox203,
            this.textBox170});
            this.table14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.3263990429950354E-09D), Telerik.Reporting.Drawing.Unit.Cm(13.04112720489502D));
            this.table14.Name = "table14";
            tableGroup12.Name = "group96";
            tableGroup13.Name = "group100";
            tableGroup14.Name = "group99";
            tableGroup15.Name = "group98";
            tableGroup16.Name = "group97";
            tableGroup17.Name = "group105";
            tableGroup18.Name = "group104";
            tableGroup19.Name = "group103";
            tableGroup20.Name = "group102";
            tableGroup21.Name = "group101";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.ChildGroups.Add(tableGroup13);
            tableGroup11.ChildGroups.Add(tableGroup14);
            tableGroup11.ChildGroups.Add(tableGroup15);
            tableGroup11.ChildGroups.Add(tableGroup16);
            tableGroup11.ChildGroups.Add(tableGroup17);
            tableGroup11.ChildGroups.Add(tableGroup18);
            tableGroup11.ChildGroups.Add(tableGroup19);
            tableGroup11.ChildGroups.Add(tableGroup20);
            tableGroup11.ChildGroups.Add(tableGroup21);
            tableGroup11.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup11.Name = "detailTableGroup13";
            this.table14.RowGroups.Add(tableGroup11);
            this.table14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989994049072266D), Telerik.Reporting.Drawing.Unit.Cm(13.443964004516602D));
            this.table14.Style.Font.Name = "Arial";
            this.table14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            // 
            // textBox174
            // 
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.529205322265625D));
            this.textBox174.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox174.Style.Font.Name = "Arial";
            this.textBox174.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox174.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox174.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox174.Value = "Overall Impression";
            // 
            // textBox177
            // 
            this.textBox177.Name = "textBox177";
            this.textBox177.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6432507038116455D), Telerik.Reporting.Drawing.Unit.Cm(0.52920573949813843D));
            this.textBox177.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox177.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox177.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox177.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox177.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox177.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox177.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox177.Value = "1";
            // 
            // textBox179
            // 
            this.textBox179.Name = "textBox179";
            this.textBox179.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.529205322265625D));
            this.textBox179.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox179.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox179.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox179.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox179.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox179.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox179.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox179.Value = "4";
            // 
            // textBox176
            // 
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1041038036346436D), Telerik.Reporting.Drawing.Unit.Cm(0.52920562028884888D));
            this.textBox176.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox176.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox176.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox176.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox176.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox176.StyleName = "";
            this.textBox176.Value = "3";
            // 
            // textBox178
            // 
            this.textBox178.Name = "textBox178";
            this.textBox178.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2363953590393066D), Telerik.Reporting.Drawing.Unit.Cm(0.52920567989349365D));
            this.textBox178.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox178.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox178.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox178.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox178.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox178.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox178.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox178.StyleName = "";
            this.textBox178.Value = "2";
            // 
            // textBox180
            // 
            this.textBox180.Name = "textBox180";
            this.textBox180.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox180.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox180.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox180.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox180.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox180.Style.Font.Name = "Arial";
            this.textBox180.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox180.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox180.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox180.StyleName = "";
            this.textBox180.Value = "Personal View :";
            // 
            // textBox181
            // 
            this.textBox181.Name = "textBox181";
            this.textBox181.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6432507038116455D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox181.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox181.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox181.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox181.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox181.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox181.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox181.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox181.StyleName = "";
            this.textBox181.Value = "1";
            // 
            // textBox182
            // 
            this.textBox182.Name = "textBox182";
            this.textBox182.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2363953590393066D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox182.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox182.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox182.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox182.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox182.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox182.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox182.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox182.StyleName = "";
            this.textBox182.Value = "2";
            // 
            // textBox183
            // 
            this.textBox183.Name = "textBox183";
            this.textBox183.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1041038036346436D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox183.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox183.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox183.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox183.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox183.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox183.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox183.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox183.StyleName = "";
            this.textBox183.Value = "3";
            // 
            // textBox184
            // 
            this.textBox184.Name = "textBox184";
            this.textBox184.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox184.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox184.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox184.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox184.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox184.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox184.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox184.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox184.StyleName = "";
            this.textBox184.Value = "4";
            // 
            // textBox185
            // 
            this.textBox185.Name = "textBox185";
            this.textBox185.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox185.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox185.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox185.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox185.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox185.Style.Font.Name = "Arial";
            this.textBox185.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox185.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox185.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox185.StyleName = "";
            this.textBox185.Value = "Your Experience";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6432507038116455D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox186.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox186.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox186.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox186.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox186.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox186.StyleName = "";
            this.textBox186.Value = "1";
            // 
            // textBox187
            // 
            this.textBox187.Name = "textBox187";
            this.textBox187.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2363953590393066D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox187.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox187.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox187.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox187.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox187.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox187.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox187.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox187.StyleName = "";
            this.textBox187.Value = "2";
            // 
            // textBox188
            // 
            this.textBox188.Name = "textBox188";
            this.textBox188.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1041038036346436D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox188.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox188.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox188.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox188.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox188.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox188.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox188.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox188.StyleName = "";
            this.textBox188.Value = "3";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox189.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox189.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox189.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.StyleName = "";
            this.textBox189.Value = "4";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox190.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox190.Style.Font.Name = "Arial";
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox190.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox190.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox190.StyleName = "";
            this.textBox190.Value = "Installation";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6432507038116455D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox191.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox191.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox191.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.StyleName = "";
            this.textBox191.Value = "1";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2363953590393066D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox192.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox192.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox192.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.StyleName = "";
            this.textBox192.Value = "2";
            // 
            // textBox193
            // 
            this.textBox193.Name = "textBox193";
            this.textBox193.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1041038036346436D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox193.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox193.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox193.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox193.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox193.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox193.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox193.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox193.StyleName = "";
            this.textBox193.Value = "3";
            // 
            // textBox194
            // 
            this.textBox194.Name = "textBox194";
            this.textBox194.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox194.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox194.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox194.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox194.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox194.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox194.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox194.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox194.StyleName = "";
            this.textBox194.Value = "4";
            // 
            // textBox195
            // 
            this.textBox195.Name = "textBox195";
            this.textBox195.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019040107727051D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox195.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox195.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox195.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox195.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox195.Style.Font.Name = "Arial";
            this.textBox195.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox195.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox195.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox195.StyleName = "";
            this.textBox195.Value = "Services / Technical Support";
            // 
            // textBox196
            // 
            this.textBox196.Name = "textBox196";
            this.textBox196.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6432507038116455D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox196.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox196.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox196.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox196.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox196.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox196.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox196.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox196.StyleName = "";
            this.textBox196.Value = "1";
            // 
            // textBox197
            // 
            this.textBox197.Name = "textBox197";
            this.textBox197.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2363953590393066D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox197.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox197.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox197.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox197.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox197.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox197.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox197.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox197.StyleName = "";
            this.textBox197.Value = "2";
            // 
            // textBox198
            // 
            this.textBox198.Name = "textBox198";
            this.textBox198.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1041038036346436D), Telerik.Reporting.Drawing.Unit.Cm(0.500035285949707D));
            this.textBox198.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox198.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox198.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox198.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox198.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox198.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox198.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox198.StyleName = "";
            this.textBox198.Value = "3";
            // 
            // textBox199
            // 
            this.textBox199.Name = "textBox199";
            this.textBox199.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9872088432312012D), Telerik.Reporting.Drawing.Unit.Cm(0.50003641843795776D));
            this.textBox199.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox199.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox199.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox199.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox199.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox199.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox199.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox199.StyleName = "";
            this.textBox199.Value = "4";
            // 
            // textBox200
            // 
            this.textBox200.Name = "textBox200";
            this.textBox200.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.019038200378418D), Telerik.Reporting.Drawing.Unit.Cm(1.4261646270751953D));
            this.textBox200.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox200.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox200.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox200.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox200.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.textBox200.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox200.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox200.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox200.StyleName = "";
            this.textBox200.Value = "Name_______________________________";
            // 
            // textBox215
            // 
            this.textBox215.Name = "textBox215";
            this.textBox215.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989992141723633D), Telerik.Reporting.Drawing.Unit.Cm(0.57941699028015137D));
            this.textBox215.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox215.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox215.Style.Font.Bold = true;
            this.textBox215.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox215.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox215.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox215.StyleName = "";
            this.textBox215.Value = "Installation Sketch";
            // 
            // textBox210
            // 
            this.textBox210.Name = "textBox210";
            this.textBox210.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989992141723633D), Telerik.Reporting.Drawing.Unit.Cm(6.4536705017089844D));
            this.textBox210.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox210.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox210.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox210.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox210.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox210.StyleName = "";
            // 
            // textBox205
            // 
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989992141723633D), Telerik.Reporting.Drawing.Unit.Cm(0.817536473274231D));
            this.textBox205.Style.BorderColor.Left = System.Drawing.Color.White;
            this.textBox205.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox205.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox205.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox205.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox205.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox205.StyleName = "";
            this.textBox205.Value = "Signed (\"The Customer\", or authorised agent):";
            // 
            // textBox203
            // 
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.091313362121582D), Telerik.Reporting.Drawing.Unit.Cm(1.4261659383773804D));
            this.textBox203.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox203.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox203.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.textBox203.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox203.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox203.StyleName = "";
            this.textBox203.Value = "Date________________";
            // 
            // textBox202
            // 
            this.textBox202.Name = "textBox202";
            this.textBox202.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.879645824432373D), Telerik.Reporting.Drawing.Unit.Cm(1.42616605758667D));
            this.textBox202.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox202.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox202.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox202.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.textBox202.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox202.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox202.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox202.StyleName = "";
            this.textBox202.Value = "Signature________________";
            // 
            // textBox201
            // 
            this.textBox201.Name = "textBox201";
            this.textBox201.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989992141723633D), Telerik.Reporting.Drawing.Unit.Cm(1.0292410850524902D));
            this.textBox201.Style.BorderColor.Left = System.Drawing.Color.White;
            this.textBox201.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox201.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox201.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox201.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox201.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox201.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox201.StyleName = "";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.7999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Arial";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "STC Assignment Form\r\nArise Solar\r\n";
            // 
            // customeracknowledgement
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtdate});
            this.Name = "customeracknowledgement";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.Font.Name = "open sp";
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection txtdate;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox172;
        private Telerik.Reporting.TextBox textBox175;
        private Telerik.Reporting.Table table14;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.TextBox textBox177;
        private Telerik.Reporting.TextBox textBox179;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox178;
        private Telerik.Reporting.TextBox textBox180;
        private Telerik.Reporting.TextBox textBox181;
        private Telerik.Reporting.TextBox textBox182;
        private Telerik.Reporting.TextBox textBox183;
        private Telerik.Reporting.TextBox textBox184;
        private Telerik.Reporting.TextBox textBox185;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox187;
        private Telerik.Reporting.TextBox textBox188;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox193;
        private Telerik.Reporting.TextBox textBox194;
        private Telerik.Reporting.TextBox textBox195;
        private Telerik.Reporting.TextBox textBox196;
        private Telerik.Reporting.TextBox textBox197;
        private Telerik.Reporting.TextBox textBox198;
        private Telerik.Reporting.TextBox textBox199;
        private Telerik.Reporting.TextBox textBox200;
        private Telerik.Reporting.TextBox textBox215;
        private Telerik.Reporting.TextBox textBox210;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox202;
        private Telerik.Reporting.TextBox textBox201;
        private Telerik.Reporting.TextBox textBox170;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.TextBox textBox2;
    }
}