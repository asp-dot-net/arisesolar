namespace EurosolarReporting
{
    partial class NewPicklist
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewPicklist));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.Picklist = new Telerik.Reporting.DetailSection();
            this.pic1 = new Telerik.Reporting.PictureBox();
            this.txtInstallerName = new Telerik.Reporting.TextBox();
            this.txtInstallDate = new Telerik.Reporting.TextBox();
            this.txtSiteAddress1 = new Telerik.Reporting.TextBox();
            this.txtSiteAddress2 = new Telerik.Reporting.TextBox();
            this.tblItemDetails = new Telerik.Reporting.Table();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.txtInstallerNotes = new Telerik.Reporting.TextBox();
            this.txtCustName = new Telerik.Reporting.TextBox();
            this.txtCustMobile = new Telerik.Reporting.TextBox();
            this.txtCustEmail = new Telerik.Reporting.TextBox();
            this.txtCustAdd = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Picklist
            // 
            this.Picklist.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.Picklist.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pic1,
            this.txtInstallerName,
            this.txtInstallDate,
            this.txtSiteAddress1,
            this.txtSiteAddress2,
            this.tblItemDetails,
            this.txtInstallerNotes,
            this.txtCustName,
            this.txtCustMobile,
            this.txtCustEmail,
            this.txtCustAdd});
            this.Picklist.Name = "Picklist";
            // 
            // pic1
            // 
            this.pic1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pic1.MimeType = "image/png";
            this.pic1.Name = "pic1";
            this.pic1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pic1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pic1.Value = ((object)(resources.GetObject("pic1.Value")));
            // 
            // txtInstallerName
            // 
            this.txtInstallerName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(5D));
            this.txtInstallerName.Name = "txtInstallerName";
            this.txtInstallerName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtInstallerName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtInstallerName.Value = "";
            // 
            // txtInstallDate
            // 
            this.txtInstallDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17D), Telerik.Reporting.Drawing.Unit.Cm(5D));
            this.txtInstallDate.Name = "txtInstallDate";
            this.txtInstallDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtInstallDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtInstallDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtInstallDate.Value = "";
            // 
            // txtSiteAddress1
            // 
            this.txtSiteAddress1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(8.1D));
            this.txtSiteAddress1.Name = "txtSiteAddress1";
            this.txtSiteAddress1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.4D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtSiteAddress1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtSiteAddress1.Value = "";
            // 
            // txtSiteAddress2
            // 
            this.txtSiteAddress2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(8.8D));
            this.txtSiteAddress2.Name = "txtSiteAddress2";
            this.txtSiteAddress2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.4D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtSiteAddress2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtSiteAddress2.Value = "";
            // 
            // tblItemDetails
            // 
            this.tblItemDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.185D)));
            this.tblItemDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.376D)));
            this.tblItemDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.281D)));
            this.tblItemDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.853D)));
            this.tblItemDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1D)));
            this.tblItemDetails.Body.SetCellContent(0, 1, this.textBox7);
            this.tblItemDetails.Body.SetCellContent(0, 2, this.textBox8);
            this.tblItemDetails.Body.SetCellContent(0, 3, this.textBox9);
            this.tblItemDetails.Body.SetCellContent(0, 0, this.textBox1);
            tableGroup1.Name = "group";
            tableGroup2.Name = "tableGroup";
            tableGroup3.Name = "tableGroup1";
            tableGroup4.Name = "tableGroup2";
            this.tblItemDetails.ColumnGroups.Add(tableGroup1);
            this.tblItemDetails.ColumnGroups.Add(tableGroup2);
            this.tblItemDetails.ColumnGroups.Add(tableGroup3);
            this.tblItemDetails.ColumnGroups.Add(tableGroup4);
            this.tblItemDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox7,
            this.textBox8,
            this.textBox9});
            this.tblItemDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(12.7D));
            this.tblItemDetails.Name = "tblItemDetails";
            tableGroup6.Name = "group1";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup";
            this.tblItemDetails.RowGroups.Add(tableGroup5);
            this.tblItemDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.695D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.376D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.TextWrap = false;
            this.textBox7.Value = "=Fields.PicklistItem";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.281D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.TextWrap = false;
            this.textBox8.Value = "=Fields.Model";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.853D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.TextWrap = false;
            this.textBox9.Value = "=Fields.Stockdesc";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.185D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            this.textBox1.TextWrap = false;
            this.textBox1.Value = "=Fields.OrderQuantity";
            // 
            // txtInstallerNotes
            // 
            this.txtInstallerNotes.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.5D), Telerik.Reporting.Drawing.Unit.Cm(22.1D));
            this.txtInstallerNotes.Name = "txtInstallerNotes";
            this.txtInstallerNotes.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(3.4D));
            this.txtInstallerNotes.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtInstallerNotes.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInstallerNotes.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.txtInstallerNotes.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInstallerNotes.Value = "";
            // 
            // txtCustName
            // 
            this.txtCustName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(5.4D));
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtCustName.Style.Color = System.Drawing.Color.White;
            this.txtCustName.Style.Font.Bold = true;
            this.txtCustName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtCustName.Value = "";
            // 
            // txtCustMobile
            // 
            this.txtCustMobile.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(6.1D));
            this.txtCustMobile.Name = "txtCustMobile";
            this.txtCustMobile.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtCustMobile.Style.Color = System.Drawing.Color.White;
            this.txtCustMobile.Style.Font.Bold = false;
            this.txtCustMobile.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtCustMobile.Value = "";
            // 
            // txtCustEmail
            // 
            this.txtCustEmail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(6.8D));
            this.txtCustEmail.Name = "txtCustEmail";
            this.txtCustEmail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtCustEmail.Style.Color = System.Drawing.Color.White;
            this.txtCustEmail.Style.Font.Bold = false;
            this.txtCustEmail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtCustEmail.Value = "";
            // 
            // txtCustAdd
            // 
            this.txtCustAdd.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(7.5D));
            this.txtCustAdd.Name = "txtCustAdd";
            this.txtCustAdd.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtCustAdd.Style.Color = System.Drawing.Color.White;
            this.txtCustAdd.Style.Font.Bold = false;
            this.txtCustAdd.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtCustAdd.Value = "";
            // 
            // NewPicklist
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Picklist});
            this.Name = "NewPicklist";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection Picklist;
        private Telerik.Reporting.PictureBox pic1;
        private Telerik.Reporting.TextBox txtInstallerName;
        private Telerik.Reporting.TextBox txtInstallDate;
        private Telerik.Reporting.TextBox txtSiteAddress1;
        private Telerik.Reporting.TextBox txtSiteAddress2;
        private Telerik.Reporting.Table tblItemDetails;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox txtInstallerNotes;
        private Telerik.Reporting.TextBox txtCustName;
        private Telerik.Reporting.TextBox txtCustMobile;
        private Telerik.Reporting.TextBox txtCustEmail;
        private Telerik.Reporting.TextBox txtCustAdd;
    }
}