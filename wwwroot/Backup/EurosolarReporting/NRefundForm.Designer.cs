using System;
namespace EurosolarReporting
{
    partial class NRefundForm
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NRefundForm));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.txtProjectNumber = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.txtSTATUS = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.txtDEPOSITDATE = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.txtENTRYDATE = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.txtNOOFPANELS = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.txtProjectNumber1 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.txtBANKNAME = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.txtACCOUNTNAME = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.txtBSBNO = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.txtACCNO = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.txtAMOUNT = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.txtSIGNATURE = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.txtREFUNDDATE = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table1,
            this.table2,
            this.table4,
            this.textBox18});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "Calibri";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.999799728393555D), Telerik.Reporting.Drawing.Unit.Cm(29.699800491333008D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.27340272068977356D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(11.013297080993652D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0000004768371582D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0000004768371582D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox2);
            this.table1.Body.SetCellContent(0, 2, this.txtProjectNumber);
            this.table1.Body.SetCellContent(1, 0, this.textBox1);
            this.table1.Body.SetCellContent(1, 2, this.textBox5);
            this.table1.Body.SetCellContent(2, 0, this.textBox7);
            this.table1.Body.SetCellContent(2, 2, this.txtSTATUS);
            this.table1.Body.SetCellContent(3, 0, this.textBox10);
            this.table1.Body.SetCellContent(3, 2, this.textBox12);
            this.table1.Body.SetCellContent(4, 0, this.textBox13);
            this.table1.Body.SetCellContent(4, 2, this.txtDEPOSITDATE);
            this.table1.Body.SetCellContent(5, 0, this.textBox16);
            this.table1.Body.SetCellContent(5, 2, this.txtENTRYDATE);
            this.table1.Body.SetCellContent(6, 0, this.textBox19);
            this.table1.Body.SetCellContent(6, 2, this.textBox21);
            this.table1.Body.SetCellContent(7, 0, this.textBox22);
            this.table1.Body.SetCellContent(7, 2, this.txtNOOFPANELS);
            this.table1.Body.SetCellContent(0, 1, this.textBox20);
            this.table1.Body.SetCellContent(1, 1, this.textBox23);
            this.table1.Body.SetCellContent(2, 1, this.textBox24);
            this.table1.Body.SetCellContent(3, 1, this.textBox27);
            this.table1.Body.SetCellContent(4, 1, this.textBox28);
            this.table1.Body.SetCellContent(5, 1, this.textBox29);
            this.table1.Body.SetCellContent(6, 1, this.textBox30);
            this.table1.Body.SetCellContent(7, 1, this.textBox31);
            tableGroup1.Name = "tableGroup";
            tableGroup2.Name = "group11";
            tableGroup3.Name = "tableGroup2";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox20,
            this.txtProjectNumber,
            this.textBox1,
            this.textBox23,
            this.textBox5,
            this.textBox7,
            this.textBox24,
            this.txtSTATUS,
            this.textBox10,
            this.textBox27,
            this.textBox12,
            this.textBox13,
            this.textBox28,
            this.txtDEPOSITDATE,
            this.textBox16,
            this.textBox29,
            this.txtENTRYDATE,
            this.textBox19,
            this.textBox30,
            this.textBox21,
            this.textBox22,
            this.textBox31,
            this.txtNOOFPANELS});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684D));
            this.table1.Name = "table1";
            tableGroup5.Name = "group";
            tableGroup6.Name = "group1";
            tableGroup7.Name = "group2";
            tableGroup8.Name = "group3";
            tableGroup9.Name = "group4";
            tableGroup10.Name = "group5";
            tableGroup11.Name = "group6";
            tableGroup12.Name = "group7";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ChildGroups.Add(tableGroup6);
            tableGroup4.ChildGroups.Add(tableGroup7);
            tableGroup4.ChildGroups.Add(tableGroup8);
            tableGroup4.ChildGroups.Add(tableGroup9);
            tableGroup4.ChildGroups.Add(tableGroup10);
            tableGroup4.ChildGroups.Add(tableGroup11);
            tableGroup4.ChildGroups.Add(tableGroup12);
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup4);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.60319709777832D), Telerik.Reporting.Drawing.Unit.Cm(8.0000009536743164D));
            this.table1.Style.Color = System.Drawing.Color.White;
            this.table1.Style.Font.Name = "Arial";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.table1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D), Telerik.Reporting.Drawing.Unit.Cm(0.99999994039535522D));
            this.textBox2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "Project Number";
            // 
            // txtProjectNumber
            // 
            this.txtProjectNumber.Name = "txtProjectNumber";
            this.txtProjectNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013297080993652D), Telerik.Reporting.Drawing.Unit.Cm(0.99999994039535522D));
            this.txtProjectNumber.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtProjectNumber.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNumber.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            this.textBox1.Value = "Customer Name";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013296127319336D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "=Fields.ContFirst +\" \" + Fields.ContLast";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox7.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "Status";
            // 
            // txtSTATUS
            // 
            this.txtSTATUS.Name = "txtSTATUS";
            this.txtSTATUS.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013296127319336D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtSTATUS.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtSTATUS.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSTATUS.StyleName = "";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox10.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "Sales Person";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013296127319336D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox12.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "=Fields.SalesRepName";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox13.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "Deposit Date";
            // 
            // txtDEPOSITDATE
            // 
            this.txtDEPOSITDATE.Name = "txtDEPOSITDATE";
            this.txtDEPOSITDATE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013296127319336D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtDEPOSITDATE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtDEPOSITDATE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDEPOSITDATE.StyleName = "";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164963722229D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox16.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "Entry Date";
            // 
            // txtENTRYDATE
            // 
            this.txtENTRYDATE.Name = "txtENTRYDATE";
            this.txtENTRYDATE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013296127319336D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtENTRYDATE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtENTRYDATE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtENTRYDATE.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox19.Style.Font.Bold = false;
            this.textBox19.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "Manager Name";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013297080993652D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox21.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3164968490600586D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "No of Panel";
            // 
            // txtNOOFPANELS
            // 
            this.txtNOOFPANELS.Name = "txtNOOFPANELS";
            this.txtNOOFPANELS.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.013297080993652D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtNOOFPANELS.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtNOOFPANELS.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtNOOFPANELS.StyleName = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340278029441833D), Telerik.Reporting.Drawing.Unit.Cm(0.99999994039535522D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = ":";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340278029441833D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = ":";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340278029441833D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = ":";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340278029441833D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            this.textBox27.Value = ":";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340278029441833D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = ":";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340278029441833D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = ":";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340275049209595D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            this.textBox30.Value = ":";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27340272068977356D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.StyleName = "";
            this.textBox31.Value = ":";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(16.329792022705078D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0332456827163696D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(2.8887007236480713D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox26);
            this.table2.Body.SetCellContent(1, 0, this.textBox25);
            tableGroup13.Name = "tableGroup3";
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox26,
            this.textBox25});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(12.278055191040039D));
            this.table2.Name = "table2";
            tableGroup15.Name = "group8";
            tableGroup16.Name = "group9";
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.ChildGroups.Add(tableGroup16);
            tableGroup14.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup14.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup14);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.329792022705078D), Telerik.Reporting.Drawing.Unit.Cm(3.9219465255737305D));
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.329792022705078D), Telerik.Reporting.Drawing.Unit.Cm(1.0332456827163696D));
            this.textBox26.Style.Color = System.Drawing.Color.White;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Lucida Sans";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(19D);
            this.textBox26.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "REASON";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.329792022705078D), Telerik.Reporting.Drawing.Unit.Cm(2.8887007236480713D));
            this.textBox25.Style.Color = System.Drawing.Color.White;
            this.textBox25.Style.Font.Name = "Arial";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox25.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox25.StyleName = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.1792311668396D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.27681124210357666D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1222219467163086D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox34);
            this.table4.Body.SetCellContent(0, 2, this.txtProjectNumber1);
            this.table4.Body.SetCellContent(1, 0, this.textBox33);
            this.table4.Body.SetCellContent(1, 2, this.textBox37);
            this.table4.Body.SetCellContent(2, 0, this.textBox39);
            this.table4.Body.SetCellContent(2, 2, this.txtBANKNAME);
            this.table4.Body.SetCellContent(3, 0, this.textBox42);
            this.table4.Body.SetCellContent(3, 2, this.txtACCOUNTNAME);
            this.table4.Body.SetCellContent(4, 0, this.textBox45);
            this.table4.Body.SetCellContent(4, 2, this.txtBSBNO);
            this.table4.Body.SetCellContent(5, 0, this.textBox48);
            this.table4.Body.SetCellContent(5, 2, this.txtACCNO);
            this.table4.Body.SetCellContent(6, 0, this.textBox51);
            this.table4.Body.SetCellContent(6, 2, this.txtAMOUNT);
            this.table4.Body.SetCellContent(7, 0, this.textBox54);
            this.table4.Body.SetCellContent(7, 2, this.txtSIGNATURE);
            this.table4.Body.SetCellContent(8, 0, this.textBox57);
            this.table4.Body.SetCellContent(8, 2, this.txtREFUNDDATE);
            this.table4.Body.SetCellContent(0, 1, this.textBox3);
            this.table4.Body.SetCellContent(1, 1, this.textBox4);
            this.table4.Body.SetCellContent(2, 1, this.textBox6);
            this.table4.Body.SetCellContent(3, 1, this.textBox8);
            this.table4.Body.SetCellContent(4, 1, this.textBox9);
            this.table4.Body.SetCellContent(5, 1, this.textBox11);
            this.table4.Body.SetCellContent(6, 1, this.textBox14);
            this.table4.Body.SetCellContent(7, 1, this.textBox15);
            this.table4.Body.SetCellContent(8, 1, this.textBox17);
            tableGroup17.Name = "tableGroup6";
            tableGroup18.Name = "group10";
            tableGroup19.Name = "tableGroup8";
            this.table4.ColumnGroups.Add(tableGroup17);
            this.table4.ColumnGroups.Add(tableGroup18);
            this.table4.ColumnGroups.Add(tableGroup19);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox34,
            this.textBox3,
            this.txtProjectNumber1,
            this.textBox33,
            this.textBox4,
            this.textBox37,
            this.textBox39,
            this.textBox6,
            this.txtBANKNAME,
            this.textBox42,
            this.textBox8,
            this.txtACCOUNTNAME,
            this.textBox45,
            this.textBox9,
            this.txtBSBNO,
            this.textBox48,
            this.textBox11,
            this.txtACCNO,
            this.textBox51,
            this.textBox14,
            this.txtAMOUNT,
            this.textBox54,
            this.textBox15,
            this.txtSIGNATURE,
            this.textBox57,
            this.textBox17,
            this.txtREFUNDDATE});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Cm(17.799999237060547D));
            this.table4.Name = "table4";
            tableGroup21.Name = "group13";
            tableGroup22.Name = "group14";
            tableGroup23.Name = "group15";
            tableGroup24.Name = "group16";
            tableGroup25.Name = "group17";
            tableGroup26.Name = "group18";
            tableGroup27.Name = "group19";
            tableGroup28.Name = "group20";
            tableGroup29.Name = "group21";
            tableGroup20.ChildGroups.Add(tableGroup21);
            tableGroup20.ChildGroups.Add(tableGroup22);
            tableGroup20.ChildGroups.Add(tableGroup23);
            tableGroup20.ChildGroups.Add(tableGroup24);
            tableGroup20.ChildGroups.Add(tableGroup25);
            tableGroup20.ChildGroups.Add(tableGroup26);
            tableGroup20.ChildGroups.Add(tableGroup27);
            tableGroup20.ChildGroups.Add(tableGroup28);
            tableGroup20.ChildGroups.Add(tableGroup29);
            tableGroup20.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup20.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup20);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.24256706237793D), Telerik.Reporting.Drawing.Unit.Cm(10.099997520446777D));
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox34.Style.Color = System.Drawing.Color.White;
            this.textBox34.Style.Font.Name = "Arial";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox34.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "Project Number";
            // 
            // txtProjectNumber1
            // 
            this.txtProjectNumber1.Name = "txtProjectNumber1";
            this.txtProjectNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtProjectNumber1.Style.Color = System.Drawing.Color.White;
            this.txtProjectNumber1.Style.Font.Name = "Arial";
            this.txtProjectNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtProjectNumber1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtProjectNumber1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNumber1.Value = "";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox33.Style.Color = System.Drawing.Color.White;
            this.textBox33.Style.Font.Name = "Arial";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox33.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "Customer";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox37.Style.Color = System.Drawing.Color.White;
            this.textBox37.Style.Font.Name = "Arial";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox37.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "=Fields.ContFirst +\" \" + Fields.ContLast";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox39.Style.Color = System.Drawing.Color.White;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Name = "Arial";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox39.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            this.textBox39.Value = "Bank Name";
            // 
            // txtBANKNAME
            // 
            this.txtBANKNAME.Name = "txtBANKNAME";
            this.txtBANKNAME.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtBANKNAME.Style.Color = System.Drawing.Color.White;
            this.txtBANKNAME.Style.Font.Name = "Arial";
            this.txtBANKNAME.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtBANKNAME.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtBANKNAME.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBANKNAME.StyleName = "";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox42.Style.Color = System.Drawing.Color.White;
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Name = "Arial";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox42.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "A/C Name";
            // 
            // txtACCOUNTNAME
            // 
            this.txtACCOUNTNAME.Name = "txtACCOUNTNAME";
            this.txtACCOUNTNAME.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtACCOUNTNAME.Style.Color = System.Drawing.Color.White;
            this.txtACCOUNTNAME.Style.Font.Name = "Arial";
            this.txtACCOUNTNAME.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtACCOUNTNAME.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtACCOUNTNAME.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtACCOUNTNAME.StyleName = "";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox45.Style.Color = System.Drawing.Color.White;
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Arial";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox45.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "BSB NO";
            // 
            // txtBSBNO
            // 
            this.txtBSBNO.Name = "txtBSBNO";
            this.txtBSBNO.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtBSBNO.Style.Color = System.Drawing.Color.White;
            this.txtBSBNO.Style.Font.Name = "Arial";
            this.txtBSBNO.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtBSBNO.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtBSBNO.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBSBNO.StyleName = "";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox48.Style.Color = System.Drawing.Color.White;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Arial";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox48.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.StyleName = "";
            this.textBox48.Value = "A/C NO";
            // 
            // txtACCNO
            // 
            this.txtACCNO.Name = "txtACCNO";
            this.txtACCNO.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtACCNO.Style.Color = System.Drawing.Color.White;
            this.txtACCNO.Style.Font.Name = "Arial";
            this.txtACCNO.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtACCNO.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtACCNO.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtACCNO.StyleName = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox51.Style.Color = System.Drawing.Color.White;
            this.textBox51.Style.Font.Bold = false;
            this.textBox51.Style.Font.Name = "Arial";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox51.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.StyleName = "";
            this.textBox51.Value = "Amount";
            // 
            // txtAMOUNT
            // 
            this.txtAMOUNT.Name = "txtAMOUNT";
            this.txtAMOUNT.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtAMOUNT.Style.Color = System.Drawing.Color.White;
            this.txtAMOUNT.Style.Font.Name = "Arial";
            this.txtAMOUNT.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtAMOUNT.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtAMOUNT.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAMOUNT.StyleName = "";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox54.Style.Color = System.Drawing.Color.White;
            this.textBox54.Style.Font.Name = "Arial";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox54.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.StyleName = "";
            this.textBox54.Value = "Signature";
            // 
            // txtSIGNATURE
            // 
            this.txtSIGNATURE.Name = "txtSIGNATURE";
            this.txtSIGNATURE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtSIGNATURE.Style.Color = System.Drawing.Color.White;
            this.txtSIGNATURE.Style.Font.Name = "Arial";
            this.txtSIGNATURE.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtSIGNATURE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtSIGNATURE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSIGNATURE.StyleName = "";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1792316436767578D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox57.Style.Color = System.Drawing.Color.White;
            this.textBox57.Style.Font.Name = "Arial";
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox57.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.StyleName = "";
            this.textBox57.Value = "Refund Date";
            // 
            // txtREFUNDDATE
            // 
            this.txtREFUNDDATE.Name = "txtREFUNDDATE";
            this.txtREFUNDDATE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.786524772644043D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.txtREFUNDDATE.Style.Color = System.Drawing.Color.White;
            this.txtREFUNDDATE.Style.Font.Name = "Arial";
            this.txtREFUNDDATE.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtREFUNDDATE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtREFUNDDATE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtREFUNDDATE.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox3.Style.Color = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Arial";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            this.textBox3.Value = ":";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox4.Style.Color = System.Drawing.Color.White;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Arial";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = ":";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox6.Style.Color = System.Drawing.Color.White;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Arial";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox6.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.StyleName = "";
            this.textBox6.Value = ":";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox8.Style.Color = System.Drawing.Color.White;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Arial";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox8.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = ":";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox9.Style.Color = System.Drawing.Color.White;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Arial";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox9.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = ":";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox11.Style.Color = System.Drawing.Color.White;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Arial";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox11.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = ":";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox14.Style.Color = System.Drawing.Color.White;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Arial";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox14.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = ":";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox15.Style.Color = System.Drawing.Color.White;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Arial";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox15.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.StyleName = "";
            this.textBox15.Value = ":";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27681127190589905D), Telerik.Reporting.Drawing.Unit.Cm(1.122221827507019D));
            this.textBox17.Style.Color = System.Drawing.Color.White;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Arial";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            this.textBox17.Value = ":";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Cm(28.899999618530273D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(0.69999927282333374D));
            this.textBox18.Style.Color = System.Drawing.Color.White;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.Value = "ABN 32168697907 | Phone : 1300 274 737 | www.arisesolar.com.au | info@arisesolar." +
    "com.au";
            // 
            // NRefundForm
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "NRefundForm";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox txtProjectNumber;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox txtSTATUS;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox txtDEPOSITDATE;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox txtENTRYDATE;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox txtNOOFPANELS;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox txtProjectNumber1;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox txtBANKNAME;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox txtACCOUNTNAME;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox txtBSBNO;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox txtACCNO;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox txtAMOUNT;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox txtSIGNATURE;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox txtREFUNDDATE;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
    }
}